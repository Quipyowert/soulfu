Menu
----

Character Creation
------------------

Game
----

Non-proximate damage:
- EXPECTED:
    Weapon swinging should not damage the player unless done nearby.
- OBSERVED:
    In the boxing ring room, damage happens to the player when they are not
    beside anyone doing damage.  The act of moving closer to the ring causes
    the application of damage.
- REPRODUCTION CASE:
    1) Move from the village to the castle foyer.
    2) Move from the castle foyer to the boxing ring room (RHS of entrance).
    3) Move towards LHS corner of the boxing ring closest to the entrance.
    4) Damage will happen to the player despite combating NPCs being too far
       away / inside the ring.

Right control behaviour:
- EXPECTED: Should do a weapon swing and do damage.
- OBSERVED: In the opening area the character does the "gets hit" animation.
    Elsewhere the character does the proper animation (i.e. castle foyer).
- REPRODUCTION CASE:
    1) Move to be directly before and facing a crate.
    2) Do a weapon swing and see the wrong action performed (and no damage).

Phantom doors:
- EXPECTED: Doors should not appear where they should not be.
- OBSERVED: Entering a room, a door can be seen on the far side where no
   door would have been authored.  On sight, it moves upwards above the
   height of the character.
- REPRODUCTION:
  1) Move from the village to the castle foyer.
  4) Move back into the village from the castle foyer.
  5) You should see a door hovering beside the platform you started on.
