// The "Programmer X Scripting System" by Programmer X, aka Aaron Bishop
//
// This code was abstracted into a library from the scripting system in SoulFu and
// is licensed to be distributed solely with that application, under Aaron's "Be Nice" license.
//
// file : pxss_run.c

// <ZZ> This file contains functions to run scripts after they've been compiled
//      _pxss_run_script                  - The main function to run a script

#include "soulfu_config.h"
#include "pxss_lib.h"
#include "logfile.h"

#include "pxss_run.inl"
#include "pxss_compile.inl"
#include "soulfu_math.inl"
#include "soulfu_endian.inl"

#include <assert.h>

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

char pxss_run_string[PXSS_STRING_COUNT][PXSS_STRING_SIZE];
char pxss_number_string[MAX_STRING_SIZE];

char * pxss_fast_function_name[MAX_FAST_FUNCTION];

bool_t pxss_looking_for_fast_run = kfalse;
bool_t pxss_fast_run_found       = kfalse;
Uint16 pxss_fast_run_offset      = 0;
int    pxss_debug_level          = 1;

//-----------------------------------------------------------------------------------------------
// ---- the most basic level of the scripting system
// ---- This level of the most basic requirements of
// ---- a script system: flow control, basic operators/math,
// ---- defining and calling user defined functions, etc.

_INLINE bool_t _pxss_run_setup_basic( PBaseGlobalScriptContext ppgs );

_INLINE PBaseScriptContext _pxss_script_spawn_basic(PBaseGlobalScriptContext pbgs, PBaseScriptContext pbs);
_INLINE PBaseScriptContext _pxss_script_init_basic(PBaseScriptContext pbs, Uint8 *file_start);
_INLINE bool_t             _pxss_run_opcode_basic(PBaseScriptContext pbs, OPCODE opcode);

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// ---- the "plain jane" level of the scripting system
// ---- This level of the scripting system corresponds to
// ---- what is available in most scripting systems with
// ---- additional libraries: file access, string manipulation, etc.

_INLINE bool_t _pxss_run_opcode_soulfu(PScriptContext ps, OPCODE_SF opcode);
_INLINE bool_t _pxss_run_opcode_library(PScriptContext ps, OPCODE_LIB opcode);
_INLINE bool_t _pxss_run_script(PScriptContext ps);

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
_INLINE bool_t _pxss_run_setup_basic( PBaseGlobalScriptContext pbgs )
{
    if (NULL == pbgs) return kfalse;

    // set the really important stuff
    pbgs->stack_head = 0;

    return ktrue;
};


//-----------------------------------------------------------------------------------------------
_INLINE void _pxss_run_script_begin_basic(PBaseScriptContext pbs)
{
    PBaseGlobalScriptContext pbgs;

    // for convenience
    pbgs = pbs->pbgs;
    pbs->link = pbgs->stack_head;

    // debugging info
    //pbs->file_name = pxss_find_file_name(pbs->file_start);
    //fprintf(stdout, "++++ _pxss_run_script_begin_basic() - %s.%s()\n", pbs->file_name, pbs->function_name);

    // initialize the token stream
    sdf_stream_open_mem(&pbs->tokens, pbs->function_start, 0);

    // Keep going until we hit a return...
    pbs->running = ktrue;

}

//-----------------------------------------------------------------------------------------------
_INLINE OPCODE _pxss_run_script_loop_begin_basic(PBaseScriptContext pbs)
{
    OPCODE opcode;
    size_t offset;

    // Read the opcode
    offset = CAST(size_t, pbs->tokens.read - pbs->file_start);
    opcode = _receive_uint08(&pbs->tokens);

    // Show debug info
    // show_int_stack(pbgs);
    // show_flt_stack(pbgs);
    log_info(pxss_debug_level, "\t0x%06d: (0x%02x) %s ", offset, opcode, opcode_name[opcode]);

    return opcode;
}

//-----------------------------------------------------------------------------------------------
_INLINE bool_t _pxss_run_script_loop_end_basic(PBaseScriptContext pbs, OPCODE opcode)
{
    bool_t handled = kfalse;

    handled = _pxss_run_opcode_basic(pbs, opcode);

    if (!handled)
    {
        // Invalid opcode
        log_error(0, "Invalid opcode at 0x%04x", opcode, pbs->tokens.read - pbs->file_start);
        pbs->running = kfalse;
    }

    return handled;
}

//-----------------------------------------------------------------------------------------------
_INLINE void _pxss_run_script_end_basic(PBaseScriptContext pbs)
{
    PBaseGlobalScriptContext pbgs = pbs->pbgs;

    //fprintf(stdout, "---- _pxss_run_script_end_basic() - %s.%s()\n", pbs->file_name, pbs->function_name);

    // Reset the stack...
    assert(pbgs->stack_head >= pbs->link);
    pbgs->stack_head = pbs->link;
}

//-----------------------------------------------------------------------------------------------
_INLINE bool_t _pxss_run_script_basic(PBaseScriptContext pbs)
{
    // <ZZ> This function runs a script, starting with whatever opcode is at pbs->function_start.

    OPCODE opcode;

    _pxss_run_script_begin_basic(pbs);

    while ( pbs->running )
    {
        opcode = _pxss_run_script_loop_begin_basic(pbs);

        _pxss_run_script_loop_end_basic(pbs, opcode);
    };

    _pxss_run_script_end_basic(pbs);

    // upload changes to the shared base global variables
    _update_global_basic(pbs->pbgs, pbs);

    return ktrue;
};

//-----------------------------------------------------------------------------------------------
void pxss_run_script_begin(PScriptContext ps)
{
    _pxss_run_script_begin_basic( &(ps->base) );
}

//-----------------------------------------------------------------------------------------------
OPCODE pxss_run_script_loop_begin(PScriptContext ps)
{
    return _pxss_run_script_loop_begin_basic( &(ps->base) );
}

//-----------------------------------------------------------------------------------------------
bool_t pxss_run_script_loop_end(PScriptContext ps, OPCODE opcode)
{
    bool_t handled = kfalse;

    // ---- handle the opcodes
    // ---- do it in "reverse" order so that you can override
    // ---- default behavior of the opcodes

    // handle soulfu_script functions
    handled = _pxss_run_opcode_soulfu(ps, opcode);

    if (!handled)
    {
        // handle soulfu_script functions
        handled = _pxss_run_opcode_library(ps, opcode);
    }

    if (!handled)
    {
        handled = _pxss_run_script_loop_end_basic( &(ps->base), opcode );
    }


    return handled;
}

//-----------------------------------------------------------------------------------------------
void pxss_run_script_end(PScriptContext ps)
{
    _pxss_run_script_end_basic( &(ps->base) );
}


//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
bool_t pxss_run_setup( PGlobalScriptContext pgs )
{
    if ( !_pxss_run_setup_basic( &(pgs->base) ) )
    {
        return kfalse;
    };

    return ktrue;
};


//-----------------------------------------------------------------------------------------------
PScriptContext pxss_script_clone(PScriptContext pold, PScriptContext pnew)
{
    if (NULL == pold || NULL == pnew) return NULL;

    // copy everything
    memcpy(pnew, pold, sizeof(ScriptContext));

    return pnew;
};

//-----------------------------------------------------------------------------------------------
PScriptContext pxss_script_spawn(PGlobalScriptContext pgs, PScriptContext ps)
{
    if (NULL == _pxss_script_spawn_basic(&(pgs->base), &(ps->base))) return NULL;

    ps->pgs = pgs;

    return ps;
};


//-----------------------------------------------------------------------------------------------
PScriptContext pxss_script_init(PScriptContext ps, Uint8 *file_start)
{
    if (NULL == ps) return NULL;

    if (NULL == _pxss_script_init_basic( &(ps->base), file_start)) return NULL;

    return ps;
};

//-----------------------------------------------------------------------------------------------
bool_t pxss_fast_rerun_script(PScriptContext ps, Uint16 fast_function)
{
    if (NULL == pxss_prepare_fast_run(ps, fast_function))
    {
        return kfalse;
    };

    return _pxss_run_script(ps);
};

//-----------------------------------------------------------------------------------------------
PBaseScriptContext _pxss_script_clone_basic(PBaseScriptContext pold, PBaseScriptContext pnew)
{
    if (NULL == pold || NULL == pnew) return NULL;

    // copy everything
    memcpy(pnew, pold, sizeof(BaseScriptContext));

    return pnew;
};

//-----------------------------------------------------------------------------------------------
PBaseScriptContext _pxss_script_spawn_basic(PBaseGlobalScriptContext pbgs, PBaseScriptContext pbs)
{
    if (NULL == pbs || NULL == pbgs) return NULL;

    memset(pbs, 0, sizeof(BaseScriptContext));

    //---- store a reference to the GlobalScriptContext
    pbs->pbgs = pbgs;

    //---- initialize the basic info ----
    pbs->num_int_vars = 0;
    pbs->num_flt_vars = 0;

    return pbs;
};

//-----------------------------------------------------------------------------------------------
_INLINE PBaseScriptContext _pxss_script_init_basic(PBaseScriptContext pbs, Uint8 *file_start)
{
    if (NULL == pbs) return NULL;

    // initialize the debug info
    pbs->function_name  = "";
    pbs->function_start = NULL;
    pbs->file_start     = file_start;
    pbs->file_name      = (NULL == file_start) ? "UNKNOWN" : pxss_find_file_name(file_start);

    return pbs;
};




//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

_INLINE PBaseScriptContext _pxss_init_callfunction_basic(PBaseScriptContext pold, PBaseScriptContext pnew)
{
    // spawn a script in the same global context as pold
    if (NULL == _pxss_script_clone_basic(pold, pnew)) return NULL;

    // grab the function parameters from the input stream
    if (NULL == _pxss_read_callfunction_params(pnew)) return NULL;

    return pnew;
};

_INLINE bool_t _pxss_run_opcode_basic(PBaseScriptContext pbs, OPCODE opcode)
{
    bool_t handled;

    SINT i;

    FLOT  f;
    FLOT  e;

    Uint8 ui8;

    Uint8* ptmp_a;
    Uint8* ptmp_b;

    size_t offset;

    PBaseGlobalScriptContext pbgs = pbs->pbgs;

    // Do these two opcodes as a special case, outside the switch
    // This is useful for debugging line-by-line
    if (OPCODE_DEBUG_LINE_NUMBER == opcode)
    {
        pbgs->stack_head = pbs->link;

        pbs->line_number = _receive_uint16(&pbs->tokens);
        return ktrue;
    }
    else if ( OPCODE_DEBUG_LINE_POINTER ==  opcode)
    {
        pbs->line_pointer = (char *)_receive_uint32(&pbs->tokens);

        //log_info(pxss_debug_level, "");
        //log_info(pxss_debug_level, "%s(%d) - %s\n", pbs->file_name, pbs->line_number, pbs->line_pointer);
        //if (pxss_debug_level == 1)
        //{
        //    fprintf(stdout, "\n%s(%d) - %s\n", pbs->file_name, pbs->line_number, pbs->line_pointer);
        //}

        return ktrue;
    };

    // Handle what it does....
    handled = ktrue;

    switch (opcode)
    {
        case OPCODE_CALLFUNCTION:
            {
                BaseScriptContext loc_sc;

                // Call function with information about the arguments it was given...
                if (NULL == _pxss_init_callfunction_basic(pbs, &loc_sc))
                {
                    // Not the best way to handle an error...
                    push_int_stack(pbs, kfalse);
                }
                else
                {
                    _pxss_run_script_basic(&loc_sc);

                    switch (loc_sc.return_var.type)
                    {
                        case PXSS_VAR_INT:
                            push_int_stack(pbs, loc_sc.return_var.i);
                            break;

                        case PXSS_VAR_FLT:
                            push_flt_stack(pbs, loc_sc.return_var.f);
                            break;

                        case PXSS_VAR_PTR:
                            push_int_stack(pbs, loc_sc.return_var.i);
                            break;

                        default:
                            assert(kfalse);
                            break;
                    }

                }

                // Now jump to our next opcode
                pbs->tokens.read = pbs->return_address;
            }
            break;

        case OPCODE_ASSIGN:
            // Put top value from int stack into the given variable (1 byte extension)
            i = pop_int_stack(pbs);
            opcode = _receive_uint08(&pbs->tokens);

            log_info(pxss_debug_level, "\t\t0x%06d: (0x%02x) %s ", CAST(size_t, pbs->tokens.read - pbs->tokens.read_beg), opcode, opcode_name[opcode]);

            // Put the data into one of the variables...
            if (opcode >= OPCODE_IREG00 && opcode <= OPCODE_IREG1F)
            {
                // Left operand is an integer variable...
                pbs->int_variable[opcode - OPCODE_IREG00] = i;
                push_int_stack(pbs, i);
            }
            else if (opcode >= OPCODE_FREG00 && opcode <= OPCODE_FREG1F)
            {
                // Left operand is an integer variable...
                pbs->flt_variable[opcode - OPCODE_FREG00] = (FLOT)i;
                push_int_stack(pbs, i);
            }
            else if (opcode >= OPCODE_PROP00 && opcode <= OPCODE_PROP1F)
            {
                // It's a property...  Get our memory pointer from the variable...
                ptmp_b = (Uint8*) pbs->int_variable[opcode - OPCODE_PROP00];

                // Then get the property number from the extension...
                ui8 = _receive_uint08(&pbs->tokens);

                // Offset the ptmp_b to the correct memory location...
                ptmp_b += properties[ui8].offset;

                switch (properties[ui8].type)
                {
                    case VAR_INT:
                        // Change the integer value at ptmp_b...
                        (DEREF( SINT, ptmp_b )) = (SINT) i;
                        push_int_stack(pbs, (SINT) i);
                        break;

                    case VAR_FLT:
                        // Change the FLOT value at ptmp_b...
                        (DEREF( FLOT, ptmp_b )) = (FLOT) i;
                        push_int_stack(pbs, i);
                        break;

                    case VAR_BYTE:
                        // Change the byte value at ptmp_b...
                        (DEREF( Sint8, ptmp_b )) = (Uint8) i;
                        push_int_stack(pbs, (Uint8)i);
                        break;

                    case VAR_WORD:
                        // Change the word value at ptmp_b...
                        (DEREF( Uint16, ptmp_b )) = (Uint16) i;
                        push_int_stack(pbs, (Uint16)i);
                        break;

                    case VAR_STRING:
                        // Copy up to 15 bytes from a string token/variable
                        ptmp_a = (Uint8*) i;
                        push_int_stack(pbs, i);

                        // Now read the values...
                        i = 0;

                        while (i < 15 && *ptmp_a != 0)
                        {
                            *ptmp_b = *ptmp_a;
                            ptmp_b++;
                            ptmp_a++;
                            i++;
                        }

                        *ptmp_b = 0;
                        break;

                    case VAR_TEXT:
                        // Copy up to 63 bytes from a string token/variable
                        ptmp_a = (Uint8*) i;

                        push_int_stack(pbs, i);

                        // Now read the values...
                        i = 0;

                        while (i < 63 && *ptmp_a != 0)
                        {
                            *ptmp_b = *ptmp_a;
                            ptmp_b++;
                            ptmp_a++;
                            i++;
                        }

                        *ptmp_b = 0;
                        break;

                }
            }
            else
            {
                // It shouldn't ever get here...
                log_error(0, "Invalid variable %d for on left-hand-side of '='", opcode);
            }

            break;

        case OPCODE_ADD:
            // Add the top value on the stack to the next to top value
            i = pop_int_stack(pbs);
            operate_int_stack(+, i);
            break;

        case OPCODE_SUBTRACT:
            // Subtract the top value on the stack from the next to top value
            i = pop_int_stack(pbs);
            operate_int_stack(-, i);
            break;

        case OPCODE_MULTIPLY:
            // Multiply the top value on the stack with the next to top value
            i = pop_int_stack(pbs);
            operate_int_stack(*, i);
            break;

        case OPCODE_DIVIDE:
            // Divide the next to top value on the stack by the top value
            i = pop_int_stack(pbs);

            if (i != 0)
            {
                operate_int_stack( / , i);
            }

            break;

        case OPCODE_INCREMENT:
            // Increment a variable...  1 byte extension follows...
            opcode = _receive_uint08(&pbs->tokens);

            offset = CAST(size_t, pbs->tokens.read - pbs->tokens.read_beg);
            log_info(pxss_debug_level, "\t\t0x%06d: (0x%02x) %s ", offset, opcode, opcode_name[opcode]);

            // Put the data into one of the variables...
            if (opcode >= OPCODE_IREG00 && opcode <= OPCODE_IREG1F)
            {
                i = (pbs->int_variable[opcode - OPCODE_IREG00]++);
            }
            else if (opcode >= OPCODE_FREG00 && opcode <= OPCODE_FREG1F)
            {
                i = (SINT) (pbs->flt_variable[opcode - OPCODE_FREG00]++);
            }
            else if (opcode >= OPCODE_PROP00 && opcode <= OPCODE_PROP1F)
            {
                // It's a property...  Get our memory pointer from the variable...
                ptmp_b = (Uint8*) pbs->int_variable[opcode - OPCODE_PROP00];

                // Then get the property number from the extension...
                ui8 = _receive_uint08(&pbs->tokens);

                // Offset the ptmp_b to the correct memory location...
                ptmp_b += properties[ui8].offset;

                switch (properties[ui8].type)
                {
                    case VAR_INT:
                        // Change the integer value at ptmp_b...
                        i = ((DEREF( SINT, ptmp_b ))++);
                        break;

                    case VAR_FLT:
                        // Change the FLOT value at ptmp_b...
                        i = (SINT) ((DEREF( FLOT, ptmp_b ))++);
                        break;

                    case VAR_BYTE:
                        // Change the byte value at ptmp_b...
                        i = (SINT) ((DEREF( Uint8, ptmp_b ))++);
                        break;

                    case VAR_WORD:
                        // Change the word value at ptmp_b...
                        i = (SINT) ((DEREF( Uint16, ptmp_b ))++);
                        break;

                    default:
                        // No effect on strings and text...
                        log_error(0, "Invalid variable for increment command");
                        i = 0;
                        break;
                }
            }
            else
            {
                // It shouldn't ever get here...
                log_error(0, "Invalid variable for increment command");
                i = 0;
            }

            push_int_stack(pbs, i);
            break;

        case OPCODE_DECREMENT:
            // Decrement a variable...  1 byte extension follows...
            opcode = _receive_uint08(&pbs->tokens);

            offset = CAST(size_t, pbs->tokens.read - pbs->tokens.read_beg);
            log_info(pxss_debug_level, "\t\t0x%06d: (0x%02x) %s ", offset, opcode, opcode_name[opcode]);

            // Put the data into one of the variables...
            if (opcode >= OPCODE_IREG00 && opcode <= OPCODE_IREG1F)
            {
                i = (pbs->int_variable[opcode - OPCODE_IREG00]--);
            }
            else if (opcode >= OPCODE_FREG00 && opcode <= OPCODE_FREG1F)
            {
                i = (SINT) (pbs->flt_variable[opcode - OPCODE_FREG00]--);
            }
            else if (opcode >= OPCODE_PROP00 && opcode <= OPCODE_PROP1F)
            {
                ptmp_b = (Uint8*) pbs->int_variable[opcode - OPCODE_PROP00];

                // Then get the property number from the extension...
                ui8 = _receive_uint08(&pbs->tokens);

                // Offset the ptmp_b to the correct memory location...
                ptmp_b += properties[ui8].offset;

                switch (properties[ui8].type)
                {
                    case VAR_INT:
                        // Change the integer value at ptmp_b...
                        i = ((DEREF( SINT, ptmp_b ))--);
                        break;

                    case VAR_FLT:
                        // Change the FLOT value at ptmp_b...
                        i = (SINT) ((DEREF( FLOT, ptmp_b ))--);
                        break;

                    case VAR_BYTE:
                        // Change the byte value at ptmp_b...
                        i = (SINT) ((DEREF( Uint8, ptmp_b ))--);
                        break;

                    case VAR_WORD:
                        // Change the word value at ptmp_b...
                        i = (SINT) ((DEREF( Uint16, ptmp_b ))--);
                        break;

                    default:
                        // No effect on strings and text...
                        log_error(0, "Invalid variable for decrement command");
                        i = 0;
                        break;

                }
            }
            else
            {
                // It shouldn't ever get here...
                log_error(0, "Invalid variable for decrement command");
                i = 0;
            }

            push_int_stack(pbs, i);
            break;

        case OPCODE_ISEQUAL:
            // Leave a one on the stack if the top two values are equal
            i = pop_int_stack(pbs);
            operate_int_stack( == , i);
            break;

        case OPCODE_ISNOTEQUAL:
            // Leave a one on the stack if the top two values are not equal
            i = pop_int_stack(pbs);
            operate_int_stack( != , i);
            break;

        case OPCODE_ISGREATEREQUAL:
            // Leave a one on the stack if the next to top >= top value on the stack
            i = pop_int_stack(pbs);
            operate_int_stack( >= , i);
            break;

        case OPCODE_ISLESSEREQUAL:
            // Leave a one on the stack if the next to top <= top value on the stack
            i = pop_int_stack(pbs);
            operate_int_stack( <= , i);
            break;

        case OPCODE_ISGREATER:
            // Leave a one on the stack if the next to top > top value on the stack
            i = pop_int_stack(pbs);
            operate_int_stack( > , i);
            break;

        case OPCODE_ISLESSER:
            // Leave a one on the stack if the next to top < top value on the stack
            i = pop_int_stack(pbs);
            operate_int_stack( < , i);
            break;

        case OPCODE_LOGICALAND:
            // Leave a one on the stack if both of the values are positive
            i = pop_int_stack(pbs);
            operate_int_stack( && , i);
            break;

        case OPCODE_LOGICALOR:
            // Leave a one on the stack if either of the values are positive
            i = pop_int_stack(pbs);
            operate_int_stack( || , i);
            break;

        case OPCODE_LOGICALNOT:
            // Perform a logical not of the top value on the stack
            preoperate_int_stack(!);
            break;

        case OPCODE_NEGATE:
            // Negates the top value on the stack
            preoperate_int_stack(-);
            break;

        case OPCODE_BITWISEAND:
            // AND the top value on the stack with the next to top value
            i = pop_int_stack(pbs);
            operate_int_stack(&, i);
            break;

        case OPCODE_BITWISEOR:
            // OR the top value on the stack with the next to top value
            i = pop_int_stack(pbs);
            operate_int_stack( | , i);
            break;

        case OPCODE_BITWISELEFT:
            // Shift left the next to top value on the stack by so many places
            i = pop_int_stack(pbs);
            operate_int_stack( << , i);
            break;

        case OPCODE_BITWISERIGHT:
            // Shift left the next to top value on the stack by so many places
            i = pop_int_stack(pbs);
            operate_int_stack( >> , i);
            break;

        case OPCODE_MODULUS:
            // Divide the next to top value on the stack by the top value, Keep the remainder
            i = pop_int_stack(pbs);

            if (i != 0)
            {
                operate_int_stack( % , i);
            }

            break;

        case OPCODE_TOFLOAT:
            // Transfer a value from the int stack to the FLOT stack
            f = pop_int_stack_cast(pbs, FLOT);
            push_flt_stack(pbs, f);
            break;

        case OPCODE_TOINT:
            // Transfer a value from the FLOT stack to the int stack
            i = pop_flt_stack_cast(pbs, SINT);
            push_int_stack(pbs, i);
            break;

        case OPCODE_F_ASSIGN:
            // Put top value from FLOT stack into the given variable (1 byte extension)
            f = pop_flt_stack(pbs);
            opcode = _receive_uint08(&pbs->tokens);

            offset = CAST(size_t, pbs->tokens.read - pbs->tokens.read_beg);
            log_info(pxss_debug_level, "\t\t0x%06d: (0x%02x) %s ", offset, opcode, opcode_name[opcode]);

            // Put the data into one of the variables...
            if (opcode >= OPCODE_IREG00 && opcode <= OPCODE_IREG1F)
            {
                pbs->int_variable[opcode - OPCODE_IREG00] = (SINT) f;
                push_flt_stack(pbs, f);
            }
            else if (opcode >= OPCODE_FREG00 && opcode <= OPCODE_FREG1F)
            {
                pbs->flt_variable[opcode - OPCODE_FREG00] = f;
                push_flt_stack(pbs, f);
            }
            else if (opcode >= OPCODE_PROP00 && opcode <= OPCODE_PROP1F)
            {
                ptmp_b = (Uint8*) pbs->int_variable[opcode - OPCODE_PROP00];

                // Then get the property number from the extension...
                ui8 = _receive_uint08(&pbs->tokens);

                // Offset the ptmp_b to the correct memory location...
                ptmp_b += properties[ui8].offset;

                switch (properties[ui8].type)
                {
                    case VAR_INT:
                        // Change the integer value at ptmp_b...
                        (DEREF( SINT, ptmp_b )) = (SINT) f;
                        push_flt_stack(pbs, f);
                        break;

                    case VAR_FLT:
                        // Change the FLOT value at ptmp_b...
                        (DEREF( FLOT, ptmp_b )) = f;
                        push_flt_stack(pbs, f);
                        break;

                    case VAR_BYTE:
                        // Change the byte value at ptmp_b...
                        (DEREF( Uint8, ptmp_b )) = (Sint8) f;
                        push_flt_stack(pbs, f);
                        break;

                    case VAR_WORD:
                        // Change the word value at ptmp_b...
                        (DEREF( Uint16, ptmp_b )) = (Sint16) f;
                        push_flt_stack(pbs, f);
                        break;

                    default:
                        // Strings and text can't get a FLOT copied into 'em...
                        log_error(0, "Invalid property %d-%c for f_equals command", ui8, properties[ui8].type);
                        break;

                }
            }
            else
            {
                // It shouldn't ever get here...
                log_error(0, "Invalid variable %d for f_equals command", opcode);
            }

            break;

        case OPCODE_F_ADD:
            // Add the top value on the stack to the next to top value
            f = pop_flt_stack(pbs);
            operate_flt_stack(+, f);
            break;

        case OPCODE_F_SUBTRACT:
            // Subtract the top value on the stack from the next to top value
            f = pop_flt_stack(pbs);
            operate_flt_stack(-, f);
            break;

        case OPCODE_F_MULTIPLY:
            // Multiply the top value on the stack with the next to top value
            f = pop_flt_stack(pbs);
            operate_flt_stack(*, f);
            break;

        case OPCODE_F_DIVIDE:
            // Divide the next to top value on the stack by the top value
            f = pop_flt_stack(pbs);

            if (f > 0.0001 || f < -0.0001)
            {
                operate_flt_stack( / , f);
            }

            break;

        case OPCODE_F_INCREMENT:
            // Decrement a variable...  1 byte extension follows...
            opcode = _receive_uint08(&pbs->tokens);

            offset = CAST(size_t, pbs->tokens.read - pbs->tokens.read_beg);
            log_info(pxss_debug_level, "\t\t0x%06d: (0x%02x) %s ", offset, opcode, opcode_name[opcode]);

            // Put the data into one of the variables...
            if (opcode >= OPCODE_IREG00 && opcode <= OPCODE_IREG1F)
            {
                f = (FLOT) (pbs->int_variable[opcode - OPCODE_IREG00]++);
            }
            else if (opcode >= OPCODE_FREG00 && opcode <= OPCODE_FREG1F)
            {
                f = (pbs->flt_variable[opcode - OPCODE_FREG00]++);
            }
            else if (opcode >= OPCODE_PROP00 && opcode <= OPCODE_PROP1F)
            {
                ptmp_b = (Uint8*) pbs->int_variable[opcode - OPCODE_PROP00];

                // Then get the property number from the extension...
                ui8 = _receive_uint08(&pbs->tokens);

                // Offset the ptmp_b to the correct memory location...
                ptmp_b += properties[ui8].offset;

                switch (properties[ui8].type)
                {
                    case VAR_INT:
                        // Change the integer value at ptmp_b...
                        f = (FLOT) ((DEREF( SINT, ptmp_b ))++);
                        break;

                    case VAR_FLT:
                        // Change the FLOT value at ptmp_b...
                        f = ((DEREF( FLOT, ptmp_b ))++);
                        break;

                    case VAR_BYTE:
                        // Change the byte value at ptmp_b...
                        f = (FLOT) ((DEREF( Uint8, ptmp_b ))++);
                        break;

                    case VAR_WORD:
                        // Change the word value at ptmp_b...
                        f = (FLOT) ((DEREF( Uint16, ptmp_b ))++);
                        break;

                    default:
                        // No effect on strings and text...
                        log_error(0, "Invalid variable for f_increment command");
                        f = 0.0f;
                        break;

                }
            }
            else
            {
                // It shouldn't ever get here...
                log_error(0, "Invalid variable for f_increment command");
                f = 0.0f;
            }

            push_flt_stack(pbs, f);
            break;

        case OPCODE_F_DECREMENT:
            // Decrement a variable...  1 byte extension follows...
            opcode = _receive_uint08(&pbs->tokens);

            offset = CAST(size_t, pbs->tokens.read - pbs->tokens.read_beg);
            log_info(pxss_debug_level, "\t\t0x%06d: (0x%02x) %s ", offset, opcode, opcode_name[opcode]);

            // Put the data into one of the variables...
            if (opcode >= OPCODE_IREG00 && opcode <= OPCODE_IREG1F)
            {
                f = (FLOT) (pbs->int_variable[opcode - OPCODE_IREG00]--);
            }
            else if (opcode >= OPCODE_FREG00 && opcode <= OPCODE_FREG1F)
            {
                f = (pbs->flt_variable[opcode - OPCODE_FREG00]--);
            }
            else if (opcode >= OPCODE_PROP00 && opcode <= OPCODE_PROP1F)
            {
                ptmp_b = (Uint8*) pbs->int_variable[opcode - OPCODE_PROP00];

                // Then get the property number from the extension...
                ui8 = _receive_uint08(&pbs->tokens);

                // Offset the ptmp_b to the correct memory location...
                ptmp_b += properties[ui8].offset;

                switch (properties[ui8].type)
                {
                    case VAR_INT:
                        // Change the integer value at ptmp_b...
                        f = (FLOT) ((DEREF( SINT, ptmp_b ))--);
                        break;

                    case VAR_FLT:
                        // Change the FLOT value at ptmp_b...
                        f = ((DEREF( FLOT, ptmp_b ))--);
                        break;

                    case VAR_BYTE:
                        // Change the byte value at ptmp_b...
                        f = (FLOT) ((DEREF( Uint8, ptmp_b ))--);
                        break;

                    case VAR_WORD:
                        // Change the word value at ptmp_b...
                        f = (FLOT) ((DEREF( Uint16, ptmp_b ))--);
                        break;

                    default:
                        // No effect on strings and text...
                        log_error(0, "Invalid variable for f_decrement command");
                        f = 0.0f;
                        break;

                }
            }
            else
            {
                // It shouldn't ever get here...
                log_error(0, "Invalid variable for f_decrement command");
                f = 0.0f;
            }

            push_flt_stack(pbs, f);
            break;

        case OPCODE_F_ISEQUAL:
            // Leave a one on the stack if the top two values are equal
            f = pop_flt_stack(pbs);
            e = pop_flt_stack(pbs);
            push_int_stack(pbs, e == f);
            break;

        case OPCODE_F_ISNOTEQUAL:
            // Leave a one on the stack if the top two values are not equal
            f = pop_flt_stack(pbs);
            e = pop_flt_stack(pbs);
            push_int_stack(pbs, e != f);
            break;

        case OPCODE_F_ISGREATEREQUAL:
            // Leave a one on the stack if the next to top >= top value on the stack
            f = pop_flt_stack(pbs);
            e = pop_flt_stack(pbs);
            push_int_stack(pbs, e >= f);
            break;

        case OPCODE_F_ISLESSEREQUAL:
            // Leave a one on the stack if the next to top <= top value on the stack
            f = pop_flt_stack(pbs);
            e = pop_flt_stack(pbs);
            push_int_stack(pbs, e <= f);
            break;

        case OPCODE_F_ISGREATER:
            // Leave a one on the stack if the next to top > top value on the stack
            f = pop_flt_stack(pbs);
            e = pop_flt_stack(pbs);
            push_int_stack(pbs, e > f);
            break;

        case OPCODE_F_ISLESSER:
            // Leave a one on the stack if the next to top < top value on the stack
            f = pop_flt_stack(pbs);
            e = pop_flt_stack(pbs);
            push_int_stack(pbs, e < f);
            break;

        case OPCODE_F_LOGICALAND:
            // Leave a one on the stack if both of the values are positive
            f = pop_flt_stack(pbs);
            e = pop_flt_stack(pbs);
            push_int_stack(pbs, e && f);
            break;

        case OPCODE_F_LOGICALOR:
            // Leave a one on the stack if either of the values are positive
            f = pop_flt_stack(pbs);
            e = pop_flt_stack(pbs);
            push_int_stack(pbs, e || f);
            break;

        case OPCODE_F_LOGICALNOT:
            // Perform a logical not of the top value on the stack
            f = pop_flt_stack(pbs);
            push_int_stack(pbs, !f);
            break;

        case OPCODE_F_NEGATE:
            // Negates the top value on the stack
            preoperate_flt_stack(-);
            break;

        case OPCODE_RETURNINT:
            // End of the script, but may return values for recursion
            pxss_var_set_int( &(pbs->return_var), pop_int_stack(pbs));
            pbs->running = kfalse;
            break;

        case OPCODE_RETURNFLOAT:
            // End of the script, but may return values for recursion

            pxss_var_set_flt( &(pbs->return_var), pop_flt_stack(pbs));
            pbs->running = kfalse;
            break;

        case OPCODE_IFFALSEJUMP:
            // Go to a new pbs->tokens position if the top integer isn't ktrue...
            i = pop_int_stack(pbs);

            if (i)
            {
                // Stay where we are...
                _receive_uint16(&pbs->tokens);
            }
            else
            {
                // Skip to where the jump says to go
                pbs->tokens.read = pbs->file_start + _receive_uint16(&pbs->tokens);
            }

            break;


        case OPCODE_JUMP:
            // Go to a new pbs->tokens position...
            pbs->tokens.read = pbs->file_start + _receive_uint16(&pbs->tokens);
            break;


        case OPCODE_SQRT:
            // Return the square root of the top FLOT value...
            preoperate_flt_stack(SQRT);
            break;

        case OPCODE_SIN:
            e = pop_flt_stack(pbs);  // angle from 0 to 360...
            e = sine_table[((Uint16)(e*182.04444f))>>4];
            push_flt_stack(pbs, e);
            break;

        case OPCODE_NIL:
            break;

        default:
            // If it got here, the ui8 is an operand value of some type...
            // Figure out what type and push it to the appropriate stack...

            if (opcode >= OPCODE_IREG00 && opcode <= OPCODE_IREG1F)
            {
                push_int_stack(pbs, pbs->int_variable[opcode - OPCODE_IREG00]);
            }
            else if (opcode >= OPCODE_FREG00 && opcode <= OPCODE_FREG1F)
            {
                push_flt_stack(pbs, pbs->flt_variable[opcode - OPCODE_FREG00]);
            }
            else if (opcode >= OPCODE_PROP00 && opcode <= OPCODE_PROP1F)
            {
                ptmp_b = (Uint8*) pbs->int_variable[opcode - OPCODE_PROP00];

                // Then get the property number from the extension...
                ui8 = _receive_uint08(&pbs->tokens);

                // Offset the ptmp_b to the correct memory location...
                ptmp_b += properties[ui8].offset;

                switch (properties[ui8].type)
                {
                    case VAR_INT:
                        // Push the integer value at ptmp_b...
                        push_int_stack(pbs,  DEREF( SINT, ptmp_b ) );
                        break;

                    case VAR_FLT:
                        // Push the FLOT value at ptmp_b...
                        push_flt_stack(pbs,  DEREF( FLOT, ptmp_b ) );
                        break;

                    case VAR_BYTE:
                        // Push the byte value at ptmp_b...
                        push_int_stack(pbs,  DEREF( Uint8, ptmp_b ) );
                        break;

                    case VAR_WORD:
                        // Push the word value at ptmp_b...
                        push_int_stack(pbs,  DEREF( Uint16, ptmp_b ) );
                        break;

                    case VAR_STRING:
                    case VAR_TEXT:
                        // Strings and text are put on stack as a pointer...  Can be used with FileWriteByte...
                        push_int_stack(pbs,  (SINT) ptmp_b );
                        break;

                    default:
                        // Invalid ui8
                        log_error(0, "Invalid property type %c at 0x%04x", properties[ui8].type, pbs->tokens.read - pbs->file_start);
                        break;

                }
            }
            else
            {
                switch (opcode)
                {

                    case OPCODE_ZERO:
                        // Integer 0
                        push_int_stack(pbs, 0);
                        break;

                    case OPCODE_INT_ONE:
                        // Integer 1
                        push_int_stack(pbs, 1);
                        break;

                    case OPCODE_FLOAT_ONE:
                        // Float 1.0
                        push_flt_stack(pbs, 1.0f);
                        break;

                    case OPCODE_INT08:
                        // Integer token, 1 byte signed
                        i = (Sint8)_receive_uint08(&pbs->tokens);
                        push_int_stack(pbs, i);
                        break;

                    case OPCODE_INT16:
                        // Integer token, 2 byte signed
                        i = (Sint16)_receive_uint16(&pbs->tokens);
                        push_int_stack(pbs, i);
                        break;

                    case OPCODE_INT32:
                        {
                            // Integer token, 4 byte signed
                            i = (SINT) _receive_uint32(&pbs->tokens);
                            push_int_stack(pbs, i);
                        }
                        break;

                    case OPCODE_FLOAT:
                        // Float token, 4 byte IEEE
                        f = _receive_float(&pbs->tokens);
                        push_flt_stack(pbs, f);
                        break;

                    case OPCODE_ZSTRING:
                        {
                            // String token, Zero terminated
                            char* string;

                            string = pbs->file_start + _receive_uint16(&pbs->tokens);

                            push_int_stack(pbs,  (UINT)string );
                        }
                        break;

                    default:
                        handled = kfalse;

                }

            }

            break;

    }

    return handled;
};

//-----------------------------------------------------------------------------------------------
_INLINE PScriptContext _pxss_init_callfunction_soulfu(PScriptContext pold, PScriptContext pnew)
{
    // spawn a script in the same global context as pold
    if (NULL == pxss_script_clone(pold, pnew)) return NULL;

    // grab the function parameters from the input stream
    if (NULL == _pxss_read_callfunction_params( &(pnew->base))) return NULL;

    return pnew;
}

//-----------------------------------------------------------------------------------------------
_INLINE bool_t _pxss_run_opcode_soulfu(PScriptContext ps, OPCODE_SF opcode)
{
    bool_t handled = ktrue;

    SINT i;
    SINT j;
    SINT k;
    SINT m;

    // for convenience
    PBaseScriptContext       pbs  = &(ps->base);

    switch (opcode)
    {
        case OPCODE_SF_SYSTEMSET:
            // Write data to special system values
            m = pop_int_stack(pbs);  // New value...
            k = pop_int_stack(pbs);  // SubNumber...  like Button 5
            j = pop_int_stack(pbs);  // Number...  like Player 2
            i = pop_int_stack(pbs);  // Type...

            push_int_stack(pbs, system_set(ps, i, j, k, m));
            break;

        case OPCODE_SF_SYSTEMGET:
            // Returns a requested value
            k = pop_int_stack(pbs);  // SubNumber...  like Button 5
            j = pop_int_stack(pbs);  // Number...  like Cursor 2
            i = pop_int_stack(pbs);  // Type...

            push_int_stack(pbs, system_get(ps, i, j, k));
            break;

            // override for CallFunction
        case OPCODE_SF_CALLFUNCTION:
            {
                ScriptContext loc_sc;

                // Call function with information about the arguments it was given...
                if (NULL == _pxss_init_callfunction_soulfu(ps, &loc_sc))
                {
                    // Not the best way to handle an error...
                    push_int_stack(pbs, 0);
                }
                else
                {
                    _pxss_run_script(&loc_sc);

                    switch (loc_sc.base.return_var.type)
                    {
                        case PXSS_VAR_INT:
                            push_int_stack(pbs, loc_sc.base.return_var.i);
                            break;

                        case PXSS_VAR_FLT:
                            push_flt_stack(pbs, loc_sc.base.return_var.f);
                            break;

                        case PXSS_VAR_PTR:
                            push_int_stack(pbs, loc_sc.base.return_var.i);
                            break;

                        default:
                            assert(kfalse);
                            break;
                    }

                }

                // Now jump to our next opcode
                pbs->tokens.read = loc_sc.base.return_address;
            }
            break;

        default:
            handled = kfalse;
    }

    return handled;
}

//-----------------------------------------------------------------------------------------------
_INLINE bool_t _pxss_run_script(PScriptContext ps)
{
    // <ZZ> This function runs a script, starting with whatever opcode is at pbs->function_start.

    PBaseScriptContext pbs;

    OPCODE opcode;

    // for convenience
    pbs  = &(ps->base);

    pxss_run_script_begin(ps);

    while ( pbs->running )
    {
        opcode = pxss_run_script_loop_begin(ps);

        pxss_run_script_loop_end(ps, opcode);
    };

    pxss_run_script_end(ps);

    // upload changes to the shared base global variables
    _update_global(ps->pgs, ps);

    return ktrue;
};

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
#include "sdf_archive.h"
_INLINE bool_t _pxss_run_opcode_library(PScriptContext ps, OPCODE_LIB opcode)
{
    bool_t handled = ktrue;

    SINT i;
    SINT j;
    SINT k;

    Uint8 ui8;

    Uint8* ptmp_a;
    Uint8* ptmp_b;

    SDF_PHEADER phdr_a;

    // for convenience
    PBaseScriptContext       pbs  = &(ps->base);

    switch (opcode)
    {
        case OPCODE_LIB_STRING:
            // Returns a pointer to a string...
            ui8 = pop_int_stack_cast(pbs, Uint8);

            if (ui8 > MAX_STRING)
            {
                push_int_stack(pbs, 0);
            }
            else
            {
                push_int_stack(pbs, (SINT)CAST(Uint8*, pxss_run_string[ui8]) );
            }

            break;

        case OPCODE_LIB_STRINGGETNUMBER:
            // Returns the first decimal value found in a string...
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);

            if (NULL == ptmp_a)
            {
                push_int_stack(pbs, 0);
            }
            else
            {
                sscanf(ptmp_a, "%d", &i);
                push_int_stack(pbs, i);
            }

            break;

        case OPCODE_LIB_STRINGCLEAR:
            // Clears a given string...  Should not be used with token strings...
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);

            if (NULL == ptmp_a)
            {
                push_int_stack(pbs, kfalse);
            }
            else
            {
                *ptmp_a = EOS;
                push_int_stack(pbs, ktrue);
            }


            break;

        case OPCODE_LIB_STRINGCLEARALL:
            // Clears all of the string variables...
            repeat(i, MAX_STRING) { pxss_run_string[i][0] = EOS; }
            push_int_stack(pbs, ktrue);
            break;

        case OPCODE_LIB_STRINGAPPEND:
            // Appends some data to a string variable...
            k = pop_int_stack(pbs);                                       // max destination size
            ptmp_b = pop_int_stack_cast(pbs, Uint8*);       // source
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // destination

            if (NULL == ptmp_a || NULL == ptmp_b)
            {
                push_int_stack(pbs, kfalse);
            }
            else
            {
                size_t szlen_a = strlen(ptmp_a);
                strncat(ptmp_a, ptmp_b, MAX(0, k - szlen_a));
                push_int_stack(pbs, ktrue);
            }

            break;

        case OPCODE_LIB_STRINGCOMPARE:
            // Returns ktrue if the strings match
            ptmp_b = pop_int_stack_cast(pbs, Uint8*);       // String 2
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // String 1

            if (NULL == ptmp_a && NULL == ptmp_b)
            {
                push_int_stack(pbs, ktrue);
            }
            else if (NULL == ptmp_a || NULL == ptmp_b)
            {
                push_int_stack(pbs, kfalse);
            }
            else
            {
                // Now compare the values...
                while (*ptmp_b == *ptmp_a && *ptmp_b != EOS)
                {
                    ptmp_b++;
                    ptmp_a++;
                }

                push_int_stack(pbs, (*ptmp_b == EOS) && (*ptmp_a == EOS));
            }

            break;

        case OPCODE_LIB_STRINGLENGTH:
            // Returns the length of the string
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // String

            if (NULL == ptmp_a)
            {
                push_int_stack(pbs, 0);
            }
            else
            {
                push_int_stack(pbs, strlen(ptmp_a));
            }

            break;

        case OPCODE_LIB_STRINGCHOPLEFT:
            // Chops some characters off the left side of a string
            j      = pop_int_stack(pbs);                    // Amount to chop
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // String

            if (NULL == ptmp_a)
            {
                push_int_stack(pbs, kfalse);
            }
            else
            {
                // Stick a null terminator at the end...
                i = strlen(ptmp_a);

                if (i > j)
                {
                    // Copy data from further ahead back to the start of the string...
                    ptmp_b = ptmp_a + j;

                    while (*ptmp_b != EOS)
                    {
                        *ptmp_a = *ptmp_b;
                        ptmp_b++;
                        ptmp_a++;
                    }
                }

                *ptmp_a = EOS;


                push_int_stack(pbs, ktrue);
            }

            break;

        case OPCODE_LIB_STRINGCHOPRIGHT:
            // Chops some characters off the right side of a string
            j = pop_int_stack(pbs);                         // Amount to chop
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // String

            if (NULL == ptmp_a)
            {
                push_int_stack(pbs, kfalse);
            }
            else
            {
                // Stick a null terminator at the end...
                i = strlen(ptmp_a);

                if (i > j)
                {
                    *(ptmp_a + i - j) = EOS;
                }
                else
                {
                    *ptmp_a = EOS;
                };

                push_int_stack(pbs, ktrue);
            }

            break;


        case OPCODE_LIB_STRINGUPPERCASE:
            // Makes a string uppercase
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // String

            if (NULL == ptmp_a)
            {
                push_int_stack(pbs, kfalse);
            }
            else
            {
                make_uppercase(ptmp_a);
                push_int_stack(pbs, ktrue);
            };

            break;

        case OPCODE_LIB_STRINGAPPENDNUMBER:
            // Appends a number to the end of a string...
            k = pop_int_stack(pbs);                         // String length

            j = pop_int_stack(pbs);                         // Number to append

            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // String

            if (NULL == ptmp_a)
            {
                push_int_stack(pbs, kfalse);
            }
            else
            {
                i = snprintf(pxss_number_string, strlen(ptmp_a) + (k - 1), "%s%d", ptmp_a, j);
                assert(i > -1);
                strncpy(ptmp_a, pxss_number_string, i);
                ptmp_a[i] = EOS;

                push_int_stack(pbs, ktrue);
            }

            break;

        case OPCODE_LIB_FILEOPEN:
            // Returns the starting location of the given file...
            // Converts "TEST.TXT" into "FILE:TEST.TXT"
            // Returns NULL if not found...
            // Used for dynamic file opening (for tools)...
            // Very slow...  Should not be used for game things...

            i = pop_int_stack(pbs);                    // mode
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);  // Filename string

            if (i == FILE_EXPORT)
            {
                sdf_archive_export_file(ptmp_a, NULL);
            }
            else if (i == FILE_IMPORT)
            {
                sdf_archive_add_file(ptmp_a, NULL);
            }
            else if (i == FILE_DELETE)
            {
                sdf_archive_delete_file(ptmp_a, SDF_FLAG_ALL);
            }
            else if (i == FILE_MAKENEW)
            {
                sdf_archive_add_new_file(ptmp_a);
            }

            phdr_a = sdf_archive_find_header(ptmp_a);

            if (phdr_a != NULL)
            {
                if (i == FILE_SIZE)
                {
                    ptmp_a = (Uint8*) sdf_file_get_size(phdr_a);
                }
                else
                {
                    ptmp_a = sdf_file_get_data(phdr_a);
                }
			} else
				ptmp_a = NULL;

            push_int_stack(pbs, (SINT) ptmp_a);
            break;


        case OPCODE_LIB_FILEREADBYTE:
            // Returns a value from a file...  File must exist and be big enough...
            j = pop_int_stack(pbs);                         // Offset
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // File/String start

            if (NULL == ptmp_a)
            {
                push_int_stack(pbs, EOS);
            }
            else
            {
                // Figure where we're reading from...
                push_int_stack(pbs, *(ptmp_a + j));
            }

            break;


        case OPCODE_LIB_FILEWRITEBYTE:
            // Writes a value to a file...  File must exist and be big enough...
            ui8 = pop_int_stack_cast(pbs, Uint8);           // Value
            i = pop_int_stack(pbs);                         // Offset
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // File/String start

            if (NULL == ptmp_a)
            {
                push_int_stack(pbs, kfalse);
            }
            else
            {
                ptmp_a += i;
                *ptmp_a = ui8;
                push_int_stack(pbs, ktrue);
            }

            break;


        case OPCODE_LIB_FILEINSERT:
            // Inserts or removes data from a file (not a string)

#ifdef DEVTOOL
            j = pop_int_stack(pbs);                         // Bytes to insert
            ptmp_b = pop_int_stack_cast(pbs, Uint8*);       // Data to insert
            i = pop_int_stack(pbs);                         // Offset into file
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // File data

            if (NULL == ptmp_a)
            {
                push_int_stack(pbs, kfalse);
            }
            else
            {
                ptmp_a += i;
                push_int_stack(pbs, sdf_insert_data(ptmp_a, ptmp_b, j));
            };

#endif
            break;

        default:
            handled = kfalse;

            break;
    }

    return handled;
}

