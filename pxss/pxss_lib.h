#pragma once

#include <SDL_types.h>

#define PXSS_STRING_COUNT 16                           // Global string variables for string functions...
#define PXSS_STRING_SIZE  0x0100                     //

extern char pxss_run_string[PXSS_STRING_COUNT][PXSS_STRING_SIZE];
extern char pxss_number_string[PXSS_STRING_SIZE];

// these functions find the memory locations to various types of data
extern Uint8* pxss_find_data(const char * filename);
extern Uint8* pxss_find_run_data(const char * dataname);
extern Uint8* pxss_find_int_data(const char * dataname);
extern Uint8* pxss_find_src_data(const char * dataname);
extern Uint8* pxss_find_txt_data(const char * dataname);
extern char * pxss_find_file_name(Uint8 * data_start);

#include "pxss_compile.h"
#include "pxss_run.h"
