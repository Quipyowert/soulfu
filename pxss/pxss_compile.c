// The "Programmer X Scripting System" by Programmer X, aka Aaron Bishop
//
// This code was abstracted into a library from the scripting system in SoulFu and
// is licensed to be distributed solely with that application, under Aaron's "Be Nice" license.
//
// file : pxss_compile.c

// <ZZ> This file contains functions to compile SRC (script source) files
//      pxss_close_jumps             - Fills in all of the previous jump locations
//      pxss_add_return_opcode       - Puts a return opcode in the compiled code
//      pxss_rpn_set_priority            - Helper for pxss_rpn_find_order
//      pxss_rpn_find_order           - Generates the RPN order for a line of code, Recursive
//      pxss_read_token              - Reads the next word-like thing of a file
//      pxss_defines_setup            - Reads global defines from "DEFINE.TXT"
//      pxss_emit_opcodes        - Writes the opcodes for a line
//      pxss_rpn_figure_variable_types   - Determines any type casting that needs to be done
//      pxss_find_function_entry     - Returns an offset to the start of a function's header, or 0
//      pxss_find_string_entry       - Returns an offset to the start of a string's header, or 0
//      pxss_mega_find_function      - Better than pxss_find_function_entry...
//      pxss_tokenize_line             - Helper for pxss_rpn_find_order, sets up data...
//      pxss_loadize             - Fill in all of the function jumps for a RUN file (helper)
//      pxss_compilerize             - The main function to compile an SRC file
//      pxss_headerize               - Generate header data before compiling an SRC file
//      pxss_stage_compile           - Wrapper for headerize, compilerize, and functionize

//      define_find              - Returns an pxss_header to the first matching define, or -1
//      define_add              - #define's a token and it's replacement value
//      define_pop_to_scope          - Gets rid of temporary defines (used for variable names...)

#include "pxss_lib.h"
#include "pxss_compile.inl"
#include "pxss_run.inl"

#include "soulfu_endian.inl"
#include "logfile.h"

#include <ctype.h>
#include <assert.h>

#define PXSS_REQUEST_ENTRY       0   // Used for mega_find...  Return pointer to function header
#define PXSS_REQUEST_OFFSET      1   // Tells us where a CallFunction should take us
#define PXSS_REQUEST_ARGUMENTS   2   // Return pointer to a function's argument/returncode string
#define PXSS_REQUEST_FILESTART   3   // Return pointer to the start of the function's run file data

#define SCOPE_GLOBAL        0   // Define is permanent
#define SCOPE_FILE          1   // Define goes away when done with file
#define SCOPE_FUNCTION      2   // Define goes away when done with function

typedef enum pxss_jump_type_e
{
    PXSS_JUMP_INVALID = 0,        // Marked as unused...
    PXSS_JUMP_IF,                 // Used when an if is found
    PXSS_JUMP_ELSE,               // Used when an else is found
    PXSS_JUMP_WHILE               // Used when a while is found
} PXSS_JUMP_TYPE;

//-----------------------------------------------------------------------------------------------
// shortcuts
char arg_list_none[] = "";                  // Argument lists for basic functions
char arg_list_i[] = "I";                  //
char arg_list_ii[] = "II";             //
char arg_list_f[] = "F";                //
char arg_list_ff[] = "FF";         //
char arg_list_iii[] = "III";                //
char arg_list_iif[] = "IIF";                //
char arg_list_iiii[] = "IIII";              //
char arg_list_iffi[] = "IFFI";              //
char arg_list_ffii[] = "FFII";              //
char arg_list_iiiii[] = "IIIII";            //
char arg_list_ifffi[] = "IFFFI";            //
char arg_list_ffiii[] = "FFIII";            //
char arg_list_iiiiii[] = "IIIIII";          //
char arg_list_iffiii[] = "IFFIII";          //
char arg_list_ffiiii[] = "FFIIII";          //
char arg_list_ffiiiii[] = "FFIIIII";        //
char arg_list_fffi[] = "FFFI";              //
char arg_list_fffiiii[] = "FFFIIII";        //
char arg_list_ffffi[] = "FFFFI";            //
char arg_list_ffffiii[] = "FFFFIII";        //
char arg_list_ffffiiiiii[] = "FFFFIIIIII";  //
char arg_list_ffffffffffffiii[] = "FFFFFFFFFFFFIII";

//-----------------------------------------------------------------------------------------------
static int   pxss_token_index = 0;
static TOKEN pxss_token_list[PXSS_MAX_TOKEN_REGISTER];

static void pxss_register_tokens_basic();
static void pxss_register_opcodes_basic();

static void pxss_register_tokens_lib();
static void pxss_register_opcodes_lib();

//-----------------------------------------------------------------------------------------------
static Uint16          put_jump_offset_here[PXSS_MAX_INDENT];     // For figurin' out indentation jumps
static Uint16          while_jumps_back_to_here[PXSS_MAX_INDENT]; // For figurin' out indentation jumps
static PXSS_JUMP_TYPE  last_jump_type_found[PXSS_MAX_INDENT];     // Not used, If, Else, While...

static bool_t line_is_a_conditional;                              // While, If, or Else on line...
static bool_t line_was_a_conditional;
static bool_t last_function_returns_integer;
static Uint8  last_return_type;
static bool_t next_token_may_be_negative;     // For reading -5 as negative 5 instead of minus 5

//-----------------------------------------------------------------------------------------------
// debug stuff...
char * opcode_name[256];

bool_t pxss_file_found_error = kfalse;                    // For compiler errors
bool_t pxss_compiler_error = kfalse;
static const char * pxss_file_pointer = "";
static char * pxss_line_pointer = "";

//-----------------------------------------------------------------------------------------------
// RPN stuff...
typedef struct rpn_info_t
{
    TOKEN   * ptok;
    bool_t    is_destroyed;                // For variable type conversions
} RPN_INFO;

static int      rpn_stack_size;                  // For determining RPN order/priority...
static RPN_INFO rpn_stack[PXSS_MAX_TOKEN];        // List of tokens in order...

static void   pxss_rpn_set_priority(int start, int i, int end, Sint8 any_type);
static bool_t pxss_rpn_figure_variable_types(SDF_PSTREAM pstream);
static void   pxss_rpn_find_order(SDF_PSTREAM pstream, int start, int end);

//-----------------------------------------------------------------------------------------------
// Token stuff...

static int   token_count;
static TOKEN tokens[PXSS_MAX_TOKEN];

//-----------------------------------------------------------------------------------------------
// Define stuff...
#define PXSS_MAX_DEFINE           2048   // The maximum number of defines
typedef struct define_t
{
    char tag[PXSS_TAG_SIZE];     // ex. "ktrue"
    char value[PXSS_TAG_SIZE];   // ex. "1"
    int  scope;                       // 0 is global, 1 is file, 2 is function
} DEFINE;

static int    define_count = 0;         // The number of defined values...  May be higher than actual...
static DEFINE defines[PXSS_MAX_DEFINE];

int    define_get_free(void);

int   define_find(char* token);
int   define_find_scope(char* token, int min_scope);
void  define_pop_to_scope(int scope);
void  define_add_string(char* token, char * value, char scope, bool_t allow_redefine);
void  define_add(SDF_PSTREAM pstream, char* token, char scope, bool_t allow_redefine);

//-----------------------------------------------------------------------------------------------
// Property stuff...
int      property_count;                        // The number of registered properties
PROPERTY properties[MAX_PROPERTY];


//-----------------------------------------------------------------------------------------------
// compile buffer
#define PXSS_BUFFER_SIZE 0x00100000           // Buffer size is 1 Meg
static Uint8 _pxss_buffer[PXSS_BUFFER_SIZE];  // Stick the INT or RUN file here while building it...

//-----------------------------------------------------------------------------------------------
// variable info
typedef struct pxss_variable_info_t
{
    bool_t set;
    int    scope;
} PXSS_VARIABLE_INFO;

static PXSS_VARIABLE_INFO flt_variable[MAX_VARIABLE];  // Make sure variables are set before they're
static PXSS_VARIABLE_INFO int_variable[MAX_VARIABLE];  // used in expressions... x = 0 before y = x...

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// forward declaration of functions

static int    _pxss_parse_line(SDF_PSTREAM pstream, bool_t expand_properties);
static bool_t _pxss_write_tokens(SDF_PSTREAM pstream);
static void   _pxss_dump_tokens();

static void   pxss_close_jumps(SDF_PSTREAM cstream, int indent, int last_indent);
static void   pxss_add_return_opcode(SDF_PSTREAM cstream);
static size_t pxss_emit_opcodes(SDF_PSTREAM cstream, SDF_PSTREAM pstream, int indent);
static int    pxss_find_function_entry(Uint8* filedata, char* functionname);
static int    pxss_find_string_entry(Uint8* filedata, char* stringname);
static bool_t pxss_tokenize_line(SDF_PSTREAM cstream, SDF_PSTREAM pstream, const char* filename);
static Sint8  pxss_stage_compile(Uint8 stage, Uint8 mask);



//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
bool_t token_is_valid_rvalue(TOKEN * ptoken)
{
    bool_t retval = ktrue;

    // Variables being used...
    if (ptoken->opcode >= OPCODE_IREG00 && ptoken->opcode <= OPCODE_IREG1F)
    {
        retval = int_variable[ptoken->opcode-OPCODE_IREG00].set;
    }
    else if (ptoken->opcode >= OPCODE_PROP00 && ptoken->opcode <= OPCODE_PROP1F)
    {
        retval = int_variable[ptoken->opcode-OPCODE_PROP00].set;
    }
    else if (ptoken->opcode >= OPCODE_FREG00 && ptoken->opcode <= OPCODE_FREG1F)
    {
        retval = flt_variable[ptoken->opcode-OPCODE_FREG00].set;
    }

    return retval;
};

//-----------------------------------------------------------------------------------------------
bool_t token_is_valid_lvalue(TOKEN * ptoken)
{
    return (ptoken->opcode >= OPCODE_IREG00 && ptoken->opcode <= OPCODE_FREG1F);
};

//-----------------------------------------------------------------------------------------------
bool_t token_set_valid_lvalue(TOKEN * ptoken)
{
    bool_t retval = ktrue;

    // Variables being set...
    if (ptoken->opcode >= OPCODE_IREG00 && ptoken->opcode <= OPCODE_IREG1F)
    {
        // Integer variable has been set...
        int_variable[ptoken->opcode-OPCODE_IREG00].set = ktrue;
        //fprintf(stdout, "\tset int variable I%02X\n", ptoken->opcode-OPCODE_IREG00);
    }
    else if (ptoken->opcode >= OPCODE_FREG00 && ptoken->opcode <= OPCODE_FREG1F)
    {
        // Float variable has been set...
        flt_variable[ptoken->opcode-OPCODE_FREG00].set = ktrue;
        //fprintf(stdout, "\tset flt variable F%02X\n", ptoken->opcode-OPCODE_FREG00);
    }
    else if (ptoken->opcode < OPCODE_PROP00 || ptoken->opcode > OPCODE_PROP1F)
    {
        retval = kfalse;
    }

    return retval;
}



//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
void   _pxss_dump_tokens()
{
    int i;
    char string[1024] = EMPTY_STRING, *ptr;

    if (0 == token_count) return;

    ptr = string;
    repeat(i, token_count - 1)
    {
        if (VAR_STRING == tokens[i].variable_type)
        {
            ptr += sprintf(ptr, "\"%s\" ", tokens[i].tag);
        }
        else
        {
            ptr += sprintf(ptr, "%s ", tokens[i].tag);
        }
    };

    if (VAR_STRING == tokens[i].variable_type)
    {
        ptr += sprintf(ptr, "\"%s\"", tokens[i].tag);
    }
    else
    {
        ptr += sprintf(ptr, "%s", tokens[i].tag);
    }

    log_special(0, "TOKENS", "%s", string);
};

//-----------------------------------------------------------------------------------------------
bool_t _pxss_write_tokens(SDF_PSTREAM pstream)
{
    int i;

    if (NULL == pstream || 0 == token_count) return kfalse;

    repeat(i, token_count - 1)
    {
        if (VAR_STRING == tokens[i].variable_type)
        {
            pstream->read += sprintf(CAST(char*,pstream->read), "\"%s\" ", tokens[i].tag);
        }
        else
        {
            pstream->read += sprintf(CAST(char*,pstream->read), "%s ", tokens[i].tag);
        }

    };

    if (VAR_STRING == tokens[i].variable_type)
    {
        pstream->read += sprintf(CAST(char*,pstream->read), "\"%s\"", tokens[i].tag);
    }
    else
    {
        pstream->read += sprintf(CAST(char*,pstream->read), "%s", tokens[i].tag);
    }

    return sdf_stream_eos(pstream);
};

//-----------------------------------------------------------------------------------------------
int   _pxss_parse_line(SDF_PSTREAM pstream, bool_t expand_properties)
{
    SDF_STREAM pxss_stream;

    Uint8* tempptr;

    int i;
    Sint8 tempnext;

    int define, property;
    bool_t expanded;

    // declare a stream to hold the line while we are expanding it
    sdf_stream_open_mem(&pxss_stream, _pxss_buffer, PXSS_BUFFER_SIZE);

    //copy the line to the subbuffer
    strcpy(pxss_stream.read_beg, pstream->read);
    pstream->read += strlen(pxss_stream.read_beg);

    // here's the basic "resursive" loop to expand all tokens
    // keeps looping until no expansions are made
    // may want to limit the expansion, just in case there are recursive #define's
    do
    {
        // reset the expansion flag
        expanded = kfalse;

        //restart the source buffer
        sdf_stream_rewind(&pxss_stream);


        // !!!! do this HERE instead of in pxss_tokenize_line() because
        // !!!! we can get some very useful tokenizing info out of the
        // !!!! parser and I don't want to erase it when we enter pxss_tokenize_line()
        repeat(i, PXSS_MAX_TOKEN)
        {
            memset(tokens + i, 0, sizeof(TOKEN));

            tokens[i].opcode            = OPCODE_NIL;
            tokens[i].extension         = -1;
            tokens[i].number_to_destroy = -1;
            tokens[i].variable_type     = VAR_UNKNOWN;
        }

        // grab tokens from the source buffer
        token_count = 0;

        while (token_count < PXSS_MAX_TOKEN && pxss_read_token(&pxss_stream, tokens + token_count))
        {
            // See if the token needs to be expanded
            // !!!! do not expand literal strings
            define = -1;

            if (TOKEN_UNKNOWN == tokens[token_count].type)
            {
                define = define_find(tokens[token_count].tag);
            }
            else
            {
                define = -1;
            }

            if (define < 0)
            {
                token_count++;
            }
            else
            {
                SDF_STREAM tmp_stream;

                // Save the read line stuff...
                tempnext = next_token_may_be_negative;

                // Read in all of the tokens in the #define...
                next_token_may_be_negative = ktrue;

                sdf_stream_open_mem(&tmp_stream, defines[define].value, PXSS_TAG_SIZE);

                while (token_count < PXSS_MAX_TOKEN && pxss_read_token(&tmp_stream, tokens + token_count))
                {
                    token_count++;
                }

                // Restore the read line stuff...
                next_token_may_be_negative = tempnext;

                expanded = ktrue;
            }

        };

        // trap errors
        if (token_count >= PXSS_MAX_TOKEN)
        {
            log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
            log_message(0, "Token overflow");
            log_message(0, "\"%s\"", pxss_line_pointer);
            _pxss_dump_tokens();
            log_message(0, "");

            pxss_file_found_error = ktrue;
            return token_count;
        }

        // write the tokens back into the source stream
        if (expanded)
        {
            sdf_stream_rewind(&pxss_stream);
            _pxss_write_tokens(&pxss_stream);
        };

    }
    while (expanded);

    if (expand_properties)
    {
        // translate any properties
        repeat(i, token_count)
        {
            // literal tokens like strings should not be touched
            if (tokens[i].variable_type == VAR_STRING) continue;

            // See if the token has a dot style property
            tempptr = strpbrk(tokens[i].tag, ".");

            if (NULL == tempptr) continue;

            if ('.' != tokens[i].tag[0] && !isdigit(tokens[i].tag[0]))
            {
                // Looks like it is...  Need to break it into two parts...
                // First find the property extension...
                *tempptr = EOS;
                property = property_find(tempptr + 1);
                define = define_find(tokens[i].tag);

                if (property > -1 && define > -1)
                {
                    // Okay, we found the property and variable...  Now reguritate 'em
                    // If we didn't find anything, it may be an external function...

                    if (VAR_INT != defines[define].value[0])
                    {
                        log_error(0, "Invalid property definition");
                    }
                    else
                    {
                        int j;
                        sscanf(defines[define].value + 1, "%02d", &j);

                        sprintf(tokens[i].tag, "%s.%d", defines[define].value, property);

                        tokens[i].tag[0]        = (char)VAR_PTR;
                        tokens[i].opcode        = OPCODE_PROP00 + j;
                        tokens[i].variable_type = properties[property].type;
                        tokens[i].extension     = property;
                    }
                }
                else
                {
                    // Must be an external function call...  Replace the period we removed...
                    *tempptr = '.';
                }
            }
        }

        sdf_stream_rewind(&pxss_stream);
        _pxss_write_tokens(&pxss_stream);
    }

    return token_count;

};





//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

#define REGISTER_OPCODE_NAME(OP) register_opcode_name(opcode_name, (OPCODE)OPCODE_##OP, #OP);

void pxss_register_opcodes_basic()
{
    REGISTER_OPCODE_NAME(ASSIGN);
    REGISTER_OPCODE_NAME(ADD);
    REGISTER_OPCODE_NAME(SUBTRACT);
    REGISTER_OPCODE_NAME(MULTIPLY);
    REGISTER_OPCODE_NAME(DIVIDE);
    REGISTER_OPCODE_NAME(INCREMENT);
    REGISTER_OPCODE_NAME(DECREMENT);
    REGISTER_OPCODE_NAME(ISEQUAL);
    REGISTER_OPCODE_NAME(ISNOTEQUAL);
    REGISTER_OPCODE_NAME(ISGREATEREQUAL);
    REGISTER_OPCODE_NAME(ISLESSEREQUAL);
    REGISTER_OPCODE_NAME(ISGREATER);
    REGISTER_OPCODE_NAME(ISLESSER);
    REGISTER_OPCODE_NAME(LOGICALAND);
    REGISTER_OPCODE_NAME(LOGICALOR);
    REGISTER_OPCODE_NAME(LOGICALNOT);
    REGISTER_OPCODE_NAME(NEGATE);
    REGISTER_OPCODE_NAME(BITWISEXOR);
    REGISTER_OPCODE_NAME(BITWISEINVERT);
    REGISTER_OPCODE_NAME(BITWISEAND);
    REGISTER_OPCODE_NAME(BITWISEOR);
    REGISTER_OPCODE_NAME(BITWISELEFT);
    REGISTER_OPCODE_NAME(BITWISERIGHT);
    REGISTER_OPCODE_NAME(MODULUS);
    REGISTER_OPCODE_NAME(UNUSED_24);
    REGISTER_OPCODE_NAME(UNUSED_25);
    REGISTER_OPCODE_NAME(UNUSED_26);
    REGISTER_OPCODE_NAME(UNUSED_27);
    REGISTER_OPCODE_NAME(UNUSED_28);
    REGISTER_OPCODE_NAME(UNUSED_29);
    REGISTER_OPCODE_NAME(TOFLOAT);
    REGISTER_OPCODE_NAME(TOINT);
    REGISTER_OPCODE_NAME(F_ASSIGN);
    REGISTER_OPCODE_NAME(F_ADD);
    REGISTER_OPCODE_NAME(F_SUBTRACT);
    REGISTER_OPCODE_NAME(F_MULTIPLY);
    REGISTER_OPCODE_NAME(F_DIVIDE);
    REGISTER_OPCODE_NAME(F_INCREMENT);
    REGISTER_OPCODE_NAME(F_DECREMENT);
    REGISTER_OPCODE_NAME(F_ISEQUAL);
    REGISTER_OPCODE_NAME(F_ISNOTEQUAL);
    REGISTER_OPCODE_NAME(F_ISGREATEREQUAL);
    REGISTER_OPCODE_NAME(F_ISLESSEREQUAL);
    REGISTER_OPCODE_NAME(F_ISGREATER);
    REGISTER_OPCODE_NAME(F_ISLESSER);
    REGISTER_OPCODE_NAME(F_LOGICALAND);
    REGISTER_OPCODE_NAME(F_LOGICALOR);
    REGISTER_OPCODE_NAME(F_LOGICALNOT);
    REGISTER_OPCODE_NAME(F_NEGATE);
    REGISTER_OPCODE_NAME(UNUSED_49);
    REGISTER_OPCODE_NAME(UNUSED_50);
    REGISTER_OPCODE_NAME(UNUSED_51);
    REGISTER_OPCODE_NAME(UNUSED_52);
    REGISTER_OPCODE_NAME(UNUSED_53);
    REGISTER_OPCODE_NAME(UNUSED_54);
    REGISTER_OPCODE_NAME(UNUSED_55);
    REGISTER_OPCODE_NAME(UNUSED_56);
    REGISTER_OPCODE_NAME(UNUSED_57);
    REGISTER_OPCODE_NAME(UNUSED_58);
    REGISTER_OPCODE_NAME(UNUSED_59);
    REGISTER_OPCODE_NAME(UNUSED_60);
    REGISTER_OPCODE_NAME(UNUSED_61);
    REGISTER_OPCODE_NAME(UNUSED_62);
    REGISTER_OPCODE_NAME(UNUSED_63);
    REGISTER_OPCODE_NAME(CALLFUNCTION);
    REGISTER_OPCODE_NAME(RETURNINT);
    REGISTER_OPCODE_NAME(RETURNFLOAT);
    REGISTER_OPCODE_NAME(IFFALSEJUMP);
    REGISTER_OPCODE_NAME(JUMP);
    REGISTER_OPCODE_NAME(SQRT);
    REGISTER_OPCODE_NAME(UNUSED_70);
    REGISTER_OPCODE_NAME(UNUSED_71);
    REGISTER_OPCODE_NAME(UNUSED_72);
    REGISTER_OPCODE_NAME(UNUSED_73);
    REGISTER_OPCODE_NAME(UNUSED_74);
    REGISTER_OPCODE_NAME(UNUSED_75);
    REGISTER_OPCODE_NAME(UNUSED_76);
    REGISTER_OPCODE_NAME(UNUSED_77);
    REGISTER_OPCODE_NAME(UNUSED_78);
    REGISTER_OPCODE_NAME(UNUSED_79);
    REGISTER_OPCODE_NAME(UNUSED_80);
    REGISTER_OPCODE_NAME(UNUSED_81);
    REGISTER_OPCODE_NAME(UNUSED_82);
    REGISTER_OPCODE_NAME(SIN);
    REGISTER_OPCODE_NAME(UNUSED_84);
    REGISTER_OPCODE_NAME(UNUSED_85);
    REGISTER_OPCODE_NAME(UNUSED_86);
    REGISTER_OPCODE_NAME(UNUSED_87);
    REGISTER_OPCODE_NAME(UNUSED_88);
    REGISTER_OPCODE_NAME(NIL);
    REGISTER_OPCODE_NAME(TOBYTE);
    REGISTER_OPCODE_NAME(TOWORD);
    REGISTER_OPCODE_NAME(UNUSED_92);
    REGISTER_OPCODE_NAME(UNUSED_93);
    REGISTER_OPCODE_NAME(UNUSED_94);
    REGISTER_OPCODE_NAME(UNUSED_95);
    REGISTER_OPCODE_NAME(UNUSED_96);
    REGISTER_OPCODE_NAME(UNUSED_97);
    REGISTER_OPCODE_NAME(UNUSED_98);
    REGISTER_OPCODE_NAME(UNUSED_99);
    REGISTER_OPCODE_NAME(UNUSED_100);
    REGISTER_OPCODE_NAME(UNUSED_101);
    REGISTER_OPCODE_NAME(UNUSED_102);
    REGISTER_OPCODE_NAME(UNUSED_103);
    REGISTER_OPCODE_NAME(UNUSED_104);
    REGISTER_OPCODE_NAME(UNUSED_105);
    REGISTER_OPCODE_NAME(UNUSED_106);
    REGISTER_OPCODE_NAME(UNUSED_107);
    REGISTER_OPCODE_NAME(UNUSED_108);
    REGISTER_OPCODE_NAME(UNUSED_109);
    REGISTER_OPCODE_NAME(UNUSED_110);
    REGISTER_OPCODE_NAME(UNUSED_111);
    REGISTER_OPCODE_NAME(UNUSED_112);
    REGISTER_OPCODE_NAME(UNUSED_113);
    REGISTER_OPCODE_NAME(UNUSED_114);
    REGISTER_OPCODE_NAME(UNUSED_115);
    REGISTER_OPCODE_NAME(UNUSED_116);
    REGISTER_OPCODE_NAME(UNUSED_117);
    REGISTER_OPCODE_NAME(UNUSED_118);
    REGISTER_OPCODE_NAME(UNUSED_119);
    REGISTER_OPCODE_NAME(UNUSED_120);
    REGISTER_OPCODE_NAME(UNUSED_121);
    REGISTER_OPCODE_NAME(UNUSED_122);
    REGISTER_OPCODE_NAME(UNUSED_123);
    REGISTER_OPCODE_NAME(UNUSED_124);
    REGISTER_OPCODE_NAME(UNUSED_125);
    REGISTER_OPCODE_NAME(UNUSED_126);

    // Last basic function is 0x7F ...
    REGISTER_OPCODE_NAME(LAST_BASIC);

    // Opcodes for integer registers
    REGISTER_OPCODE_NAME(IREG00);
    REGISTER_OPCODE_NAME(IREG01);
    REGISTER_OPCODE_NAME(IREG02);
    REGISTER_OPCODE_NAME(IREG03);
    REGISTER_OPCODE_NAME(IREG04);
    REGISTER_OPCODE_NAME(IREG05);
    REGISTER_OPCODE_NAME(IREG06);
    REGISTER_OPCODE_NAME(IREG07);
    REGISTER_OPCODE_NAME(IREG08);
    REGISTER_OPCODE_NAME(IREG09);
    REGISTER_OPCODE_NAME(IREG0A);
    REGISTER_OPCODE_NAME(IREG0B);
    REGISTER_OPCODE_NAME(IREG0C);
    REGISTER_OPCODE_NAME(IREG0D);
    REGISTER_OPCODE_NAME(IREG0E);
    REGISTER_OPCODE_NAME(IREG0F);
    REGISTER_OPCODE_NAME(IREG10);
    REGISTER_OPCODE_NAME(IREG11);
    REGISTER_OPCODE_NAME(IREG12);
    REGISTER_OPCODE_NAME(IREG13);
    REGISTER_OPCODE_NAME(IREG14);
    REGISTER_OPCODE_NAME(IREG15);
    REGISTER_OPCODE_NAME(IREG16);
    REGISTER_OPCODE_NAME(IREG17);
    REGISTER_OPCODE_NAME(IREG18);
    REGISTER_OPCODE_NAME(IREG19);
    REGISTER_OPCODE_NAME(IREG1A);
    REGISTER_OPCODE_NAME(IREG1B);
    REGISTER_OPCODE_NAME(IREG1C);
    REGISTER_OPCODE_NAME(IREG1D);
    REGISTER_OPCODE_NAME(IREG1E);
    REGISTER_OPCODE_NAME(IREG1F);

    // Opcodes for properties derived from integer registers
    REGISTER_OPCODE_NAME(PROP00);
    REGISTER_OPCODE_NAME(PROP01);
    REGISTER_OPCODE_NAME(PROP02);
    REGISTER_OPCODE_NAME(PROP03);
    REGISTER_OPCODE_NAME(PROP04);
    REGISTER_OPCODE_NAME(PROP05);
    REGISTER_OPCODE_NAME(PROP06);
    REGISTER_OPCODE_NAME(PROP07);
    REGISTER_OPCODE_NAME(PROP08);
    REGISTER_OPCODE_NAME(PROP09);
    REGISTER_OPCODE_NAME(PROP0A);
    REGISTER_OPCODE_NAME(PROP0B);
    REGISTER_OPCODE_NAME(PROP0C);
    REGISTER_OPCODE_NAME(PROP0D);
    REGISTER_OPCODE_NAME(PROP0E);
    REGISTER_OPCODE_NAME(PROP0F);
    REGISTER_OPCODE_NAME(PROP10);
    REGISTER_OPCODE_NAME(PROP11);
    REGISTER_OPCODE_NAME(PROP12);
    REGISTER_OPCODE_NAME(PROP13);
    REGISTER_OPCODE_NAME(PROP14);
    REGISTER_OPCODE_NAME(PROP15);
    REGISTER_OPCODE_NAME(PROP16);
    REGISTER_OPCODE_NAME(PROP17);
    REGISTER_OPCODE_NAME(PROP18);
    REGISTER_OPCODE_NAME(PROP19);
    REGISTER_OPCODE_NAME(PROP1A);
    REGISTER_OPCODE_NAME(PROP1B);
    REGISTER_OPCODE_NAME(PROP1C);
    REGISTER_OPCODE_NAME(PROP1D);
    REGISTER_OPCODE_NAME(PROP1E);
    REGISTER_OPCODE_NAME(PROP1F);

    // Opcodes for float registers
    REGISTER_OPCODE_NAME(FREG00);
    REGISTER_OPCODE_NAME(FREG01);
    REGISTER_OPCODE_NAME(FREG02);
    REGISTER_OPCODE_NAME(FREG03);
    REGISTER_OPCODE_NAME(FREG04);
    REGISTER_OPCODE_NAME(FREG05);
    REGISTER_OPCODE_NAME(FREG06);
    REGISTER_OPCODE_NAME(FREG07);
    REGISTER_OPCODE_NAME(FREG08);
    REGISTER_OPCODE_NAME(FREG09);
    REGISTER_OPCODE_NAME(FREG0A);
    REGISTER_OPCODE_NAME(FREG0B);
    REGISTER_OPCODE_NAME(FREG0C);
    REGISTER_OPCODE_NAME(FREG0D);
    REGISTER_OPCODE_NAME(FREG0E);
    REGISTER_OPCODE_NAME(FREG0F);
    REGISTER_OPCODE_NAME(FREG10);
    REGISTER_OPCODE_NAME(FREG11);
    REGISTER_OPCODE_NAME(FREG12);
    REGISTER_OPCODE_NAME(FREG13);
    REGISTER_OPCODE_NAME(FREG14);
    REGISTER_OPCODE_NAME(FREG15);
    REGISTER_OPCODE_NAME(FREG16);
    REGISTER_OPCODE_NAME(FREG17);
    REGISTER_OPCODE_NAME(FREG18);
    REGISTER_OPCODE_NAME(FREG19);
    REGISTER_OPCODE_NAME(FREG1A);
    REGISTER_OPCODE_NAME(FREG1B);
    REGISTER_OPCODE_NAME(FREG1C);
    REGISTER_OPCODE_NAME(FREG1D);
    REGISTER_OPCODE_NAME(FREG1E);
    REGISTER_OPCODE_NAME(FREG1F);

    // opcodes for constant values
    REGISTER_OPCODE_NAME(ZERO);
    REGISTER_OPCODE_NAME(INT_ONE);
    REGISTER_OPCODE_NAME(FLOAT_ONE);
    REGISTER_OPCODE_NAME(INT08);
    REGISTER_OPCODE_NAME(INT16);
    REGISTER_OPCODE_NAME(INT32);
    REGISTER_OPCODE_NAME(FLOAT);
    REGISTER_OPCODE_NAME(ZSTRING);

    REGISTER_OPCODE_NAME(DEBUG_LINE_NUMBER);
    REGISTER_OPCODE_NAME(DEBUG_LINE_POINTER);

    REGISTER_OPCODE_NAME(COUNT);

    REGISTER_OPCODE_NAME(INTERNAL_BEGIN);

    // markers that help with parsing/tokenizing the line
    REGISTER_OPCODE_NAME(INT_COMMA);
    REGISTER_OPCODE_NAME(INT_LPAREN);
    REGISTER_OPCODE_NAME(INT_RPAREN);
    REGISTER_OPCODE_NAME(INT_LBRACKET);
    REGISTER_OPCODE_NAME(INT_RBRACKET);
};


void pxss_compile_setup_basic()
{
    pxss_compiler_error = kfalse;
    pxss_file_pointer = "";
    pxss_line_pointer = "";

    pxss_register_tokens_basic();
    pxss_register_opcodes_basic();
}


#define REGISTER_OPCODE_NAME_LIB(OP) register_opcode_name(opcode_name, (OPCODE)OPCODE_LIB_##OP, #OP);


void pxss_register_opcodes_lib()
{
    REGISTER_OPCODE_NAME_LIB(STRING);
    REGISTER_OPCODE_NAME_LIB(STRINGGETNUMBER);
    REGISTER_OPCODE_NAME_LIB(STRINGCLEAR);
    REGISTER_OPCODE_NAME_LIB(STRINGCLEARALL);
    REGISTER_OPCODE_NAME_LIB(STRINGAPPEND);
    REGISTER_OPCODE_NAME_LIB(STRINGCOMPARE);
    REGISTER_OPCODE_NAME_LIB(STRINGLENGTH);
    REGISTER_OPCODE_NAME_LIB(STRINGCHOPLEFT);
    REGISTER_OPCODE_NAME_LIB(STRINGCHOPRIGHT);
    REGISTER_OPCODE_NAME_LIB(STRINGUPPERCASE);
    REGISTER_OPCODE_NAME_LIB(STRINGAPPENDNUMBER);

    REGISTER_OPCODE_NAME_LIB(FILEOPEN);
    REGISTER_OPCODE_NAME_LIB(FILEREADBYTE);
    REGISTER_OPCODE_NAME_LIB(FILEWRITEBYTE);
    REGISTER_OPCODE_NAME_LIB(FILEINSERT);
};

//-----------------------------------------------------------------------------------------------
void pxss_compile_setup_lib()
{
    pxss_register_tokens_lib();
    pxss_register_opcodes_lib();
};

//-----------------------------------------------------------------------------------------------
void pxss_compile_setup()
{
    pxss_compile_setup_basic();
    pxss_compile_setup_lib();
};

//-----------------------------------------------------------------------------------------------
void pxss_close_jumps(SDF_PSTREAM cstream, int indent, int last_indent)
{
    // <ZZ> This function closes all of the if's and else's and while's down to the given
    //      indent level.
    int i, j;

    int max_indent = last_indent;
    int min_indent = MAX(1, indent);


    for (i = max_indent; i >= min_indent; i--)
    {
        if (last_jump_type_found[i] == PXSS_JUMP_INVALID) continue;

        if (PXSS_JUMP_ELSE  == last_jump_type_found[i])
        {
            assert( UINT16_MAX == endian_read_mem_int16(cstream->read_beg + put_jump_offset_here[i]) );

            log_info(1, "ELSE SKIPPER JUMPS TO HERE 0x%x", CAST(size_t, cstream->read - cstream->read_beg));
            endian_write_mem_int16(cstream->read_beg + put_jump_offset_here[i], CAST(size_t, cstream->read - cstream->read_beg));

            put_jump_offset_here[i] = UINT16_MAX;
            last_jump_type_found[i] = PXSS_JUMP_INVALID;
        }
        else if (PXSS_JUMP_IF    == last_jump_type_found[i])
        {
            // Insert implied jumps in order to skip over elses
            j = put_jump_offset_here[i];

            assert( OPCODE_IFFALSEJUMP == DEREF(Uint8, cstream->read_beg + j - 1) );
            assert( UINT16_MAX == endian_read_mem_int16(cstream->read_beg + j) );

            if (OPCODE_INT_ELSE == tokens[0].opcode && i == indent)
            {
                log_info(1, "OPC:  0x%02x", OPCODE_JUMP);
                _emit_uint08(cstream, OPCODE_JUMP);

                put_jump_offset_here[i] = CAST(size_t, cstream->read - cstream->read_beg);
                last_jump_type_found[i] = PXSS_JUMP_ELSE;

                log_info(1, "0x%04x", UINT16_MAX);
                _emit_uint16(cstream, UINT16_MAX);
            }
            else
            {
                put_jump_offset_here[i] = UINT16_MAX;
                last_jump_type_found[i] = PXSS_JUMP_INVALID;
            }

            log_info(1, "FAILED IF JUMPS TO HERE 0x%x", CAST(size_t, cstream->read - cstream->read_beg));
            endian_write_mem_int16(cstream->read_beg + j, CAST(size_t, cstream->read - cstream->read_beg));
        }
        else if (PXSS_JUMP_WHILE == last_jump_type_found[i])
        {
            log_info(1, "JUMP BACK TO START OF WHILE 0x%x", while_jumps_back_to_here[i]);

            log_info(1, "OPC:  0x%02x", OPCODE_JUMP);
            _emit_uint08(cstream, OPCODE_JUMP);

            log_info(1, "0x%04x", 0);
            _emit_uint16(cstream, while_jumps_back_to_here[i]);

            assert( OPCODE_IFFALSEJUMP == DEREF(Uint8, cstream->read_beg + put_jump_offset_here[i] - 1) );
            assert( UINT16_MAX == endian_read_mem_int16(cstream->read_beg + put_jump_offset_here[i]) );

            log_info(1, "FAILED WHILE JUMPS TO HERE 0x%x", CAST(size_t, cstream->read - cstream->read_beg));
            endian_write_mem_int16(cstream->read_beg + put_jump_offset_here[i], CAST(size_t, cstream->read - cstream->read_beg));

            put_jump_offset_here[i] = UINT16_MAX;
            last_jump_type_found[i] = PXSS_JUMP_INVALID;
        }
        else
        {
            last_jump_type_found[i] = PXSS_JUMP_INVALID;
        }
    }

    // pop any defines that have moved out of scope
    define_pop_to_scope(SCOPE_FUNCTION + indent);
}

//-----------------------------------------------------------------------------------------------
void pxss_add_return_opcode(SDF_PSTREAM cstream)
{
    // <ZZ> This function appends a return opcode to the compile_stream.  Done to make sure
    //      function calls go back to where they came from.
    if (last_function_returns_integer)
    {
        log_info(1, "OPC:  0x%02x", OPCODE_INT_ONE);         // Opcode for integer 1 is 225
        _emit_uint08(cstream, OPCODE_INT_ONE);

        log_info(1, "OPC:  0x%02x", OPCODE_RETURNINT);
        _emit_uint08(cstream, OPCODE_RETURNINT);
    }
    else
    {
        log_info(1, "OPC:  0x%02x", OPCODE_FLOAT_ONE);         // Opcode for float 1.0 is 226
        log_info(1, "OPC:  0x%02x", OPCODE_RETURNFLOAT);

        _emit_uint08(cstream, OPCODE_FLOAT_ONE);
        _emit_uint08(cstream, OPCODE_RETURNFLOAT);
    }
}

//-----------------------------------------------------------------------------------------------
bool_t pxss_read_token(SDF_PSTREAM pstream, TOKEN * ptok)
{
    // <ZZ> This function reads the next token (like a word) in a file that has been
    //      sdf_open()'d and puts that token in buffer.  The token can't be any longer than
    //      PXSS_TAG_SIZE.  It returns ktrue if there was a token to read, or kfalse if
    //      not.  next_token_may_be_negative is also important for reading negative numbers
    //      correctly.
    int count;
    int value;

    ptok->opcode        = OPCODE_NIL;
    ptok->variable_type = VAR_UNKNOWN;
    ptok->type          = TOKEN_UNKNOWN;

    // Skip any whitespace...
    while ( isspace(*pstream->read) && !sdf_stream_eos(pstream))
    {
        pstream->read++;
    }

    if (EOS == *(pstream->read) || sdf_stream_eos(pstream))
    {
        // ignore empty lines
        return kfalse;
    }

    if (*(pstream->read) == '/' && *(pstream->read + 1) == '/')
    {
        // Ignore comments
        return kfalse;
    }

    // specialized tokens
    count = 0;

    if (*(pstream->read) == ',')
    {
        ptok->tag[count] = *(pstream->read);
        pstream->read++;
        count++;

        ptok->tag[count++] = EOS;

        ptok->opcode = OPCODE_INT_COMMA;
        ptok->type   = TOKEN_DELIMITER;
        next_token_may_be_negative = ktrue;
    }
    else if (*(pstream->read) == '(')
    {
        ptok->tag[count] = *(pstream->read);
        pstream->read++;
        count++;

        ptok->tag[count++] = EOS;

        ptok->opcode = OPCODE_INT_LPAREN;
        ptok->type   = TOKEN_DELIMITER;
        next_token_may_be_negative = ktrue;
    }
    else if (*(pstream->read) == ')')
    {
        ptok->tag[count] = *(pstream->read);
        pstream->read++;
        count++;

        ptok->tag[count++] = EOS;

        ptok->opcode = OPCODE_INT_RPAREN;
        ptok->type   = TOKEN_DELIMITER;
        next_token_may_be_negative = kfalse;
    }

    if (count > 0) return ktrue;

    // general tokens
    ptok->type = TOKEN_OPERAND;

    if (*(pstream->read) == '\'' && (*(pstream->read + 2)) == '\'')
    {
        sprintf(ptok->tag, "%03d", *(pstream->read + 1));
        pstream->read += 3;
        count += 3;

        ptok->variable_type = VAR_INT;
    }
    else if (*(pstream->read) == '\"')
    {
        // Read the string token
        pstream->read++;

        while (count < PXSS_TAG_SIZE - 2 && !sdf_stream_eos(pstream) && *(pstream->read) != '\"' && *(pstream->read) != EOS)
        {
            ptok->tag[count] = *(pstream->read);
            pstream->read++;

            if (ptok->tag[count] == '\\' && isdigit(*pstream->read))
            {
                // It's an escape sequence, so read in the value...
                value = 0;

                while (isdigit(*pstream->read))
                {
                    value = (value * 10) + *(pstream->read) - '0';
                    pstream->read++;
                }

                ptok->tag[count] = value;
            }

            count++;
        }

        if ('\"' == *(pstream->read)) pstream->read++;

        ptok->tag[count++] = EOS;

        ptok->variable_type = VAR_STRING;

        next_token_may_be_negative = kfalse;
    }
    else if (  next_token_may_be_negative && *(pstream->read) == '-' && (isdigit(*(pstream->read + 1)) || *(pstream->read + 1) == '.') )
    {
        int dot_count = 0;

        if ('.' == *(pstream->read)) dot_count++;

        ptok->tag[count] = *(pstream->read);
        pstream->read++;
        count++;

        while (count < PXSS_TAG_SIZE - 1 && !sdf_stream_eos(pstream) && (*(pstream->read) == '.' || isdigit(*pstream->read)) )
        {
            if (*(pstream->read) == '.') dot_count++;

            if (dot_count > 1) break;

            ptok->tag[count] = *(pstream->read);
            pstream->read++;
            count++;
        }

        ptok->tag[count++] = EOS;

        ptok->variable_type = (dot_count == 0) ? VAR_INT      : VAR_FLT;
        ptok->opcode        = (dot_count == 0) ? OPCODE_INT32 : OPCODE_FLOAT;

        next_token_may_be_negative = kfalse;
    }
    else if (  isdigit(*pstream->read) || *(pstream->read) == '.' )
    {
        int dot_count = 0;

        if ('.' == *(pstream->read)) dot_count++;

        ptok->tag[count] = *(pstream->read);
        pstream->read++;
        count++;

        while (count < PXSS_TAG_SIZE - 1 && !sdf_stream_eos(pstream) && (*(pstream->read) == '.' || isdigit(*pstream->read)) )
        {
            if (*(pstream->read) == '.') dot_count++;

            if (dot_count > 1) break;

            ptok->tag[count] = *(pstream->read);
            pstream->read++;
            count++;
        }

        ptok->tag[count++] = EOS;

        ptok->variable_type = (dot_count == 0) ? VAR_INT      : VAR_FLT;
        ptok->opcode        = (dot_count == 0) ? OPCODE_INT32 : OPCODE_FLOAT;

        next_token_may_be_negative = kfalse;
    }
    else if (VAR_INT == *(pstream->read) )
    {
        int j;

        // grab the register offset
        sscanf(pstream->read + 1, "%02d", &j);

        // grab float the variable identifiers
        memcpy(ptok->tag, pstream->read, 3);
        pstream->read += 3;
        count         += 3;
        ptok->tag[count++] = EOS;

        // set the token info
        ptok->variable_type = VAR_INT;
        ptok->opcode        = (OPCODE)(OPCODE_IREG00 + j);
    }
    else if (VAR_FLT == *(pstream->read) )
    {
        int j;

        // grab the register offset
        sscanf(pstream->read + 1, "%02d", &j);

        // grab float the variable identifiers
        memcpy(ptok->tag, pstream->read, 3);
        pstream->read += 3;
        count         += 3;
        ptok->tag[count++] = EOS;

        // set the token info
        ptok->variable_type = VAR_FLT;
        ptok->opcode        = (OPCODE)(OPCODE_FREG00 + j);
    }
    else if (VAR_PTR == *(pstream->read) )
    {
        // grab integer the variable identifiers
        int j, property_type;

        assert('.' == *(pstream->read + 2));

        // grab the register offset
        sscanf(pstream->read + 1, "%02d", &j);

        memcpy(ptok->tag, pstream->read, 3);
        pstream->read += 3;
        count = 3;

        // grab the property
        sscanf(pstream->read, "%d", &property_type);

        while ( count < PXSS_TAG_SIZE - 1 && !sdf_stream_eos(pstream) && isdigit(*(pstream->read)) )
        {
            ptok->tag[count] = *(pstream->read);
            pstream->read++;
            count++;
        }

        ptok->tag[count++] = EOS;

        ptok->variable_type = properties[property_type].type;
        ptok->opcode        = (OPCODE)(OPCODE_PROP00 + j);
    }
    else if (isalpha(*pstream->read) || *(pstream->read) == '_' )
    {
        // Read the identifier

        ptok->tag[count] = *(pstream->read);
        pstream->read++;
        count++;

        while (count < PXSS_TAG_SIZE - 1 && !sdf_stream_eos(pstream) && (isalpha(*pstream->read) || *(pstream->read) == '_' || *(pstream->read) == '.' || isdigit(*pstream->read)) )
        {
            ptok->tag[count] = *(pstream->read);
            pstream->read++;
            count++;
        }

        ptok->tag[count++] = EOS;
        make_uppercase(ptok->tag);
        next_token_may_be_negative = kfalse;

        ptok->type          = TOKEN_UNKNOWN;
    }
    else
    {
        // must be some kind of operator

        next_token_may_be_negative = ktrue;

        ptok->tag[count] = *(pstream->read);
        pstream->read++;
        count++;

        // Do we need another?
        if ((ptok->tag[0] == '>' && *(pstream->read) == '>') ||
                (ptok->tag[0] == '<' && *(pstream->read) == '<') ||
                (ptok->tag[0] == '&' && *(pstream->read) == '&') ||
                (ptok->tag[0] == '|' && *(pstream->read) == '|') ||
                (ptok->tag[0] == '>' && *(pstream->read) == '=') ||
                (ptok->tag[0] == '<' && *(pstream->read) == '=') ||
                (ptok->tag[0] == '=' && *(pstream->read) == '=') ||
                (ptok->tag[0] == '+' && *(pstream->read) == '+') ||
                (ptok->tag[0] == '-' && *(pstream->read) == '-') ||
                (ptok->tag[0] == '/' && *(pstream->read) == '/') ||
                (ptok->tag[0] == '!' && *(pstream->read) == '='))
        {
            ptok->tag[count] = *(pstream->read);
            pstream->read++;
            count++;
        }

        ptok->tag[count++] = EOS;

        // Figure out if the next one can be negative...
        if (0 == strcmp(ptok->tag, "--")) next_token_may_be_negative = kfalse;
        else if (0 == strcmp(ptok->tag, "++")) next_token_may_be_negative = kfalse;

        ptok->type = TOKEN_OPERATOR;
    }

    return (count > 0);
}

//-----------------------------------------------------------------------------------------------
bool_t pxss_defines_setup(SDF_PSTREAM define_stream)
{
    // <ZZ> This function looks at "DEFINE.TXT" to see what global #define's we've got,
    //      including ID strings.  It returns ktrue if it worked okay, or kfalse if not.
    Uint8 spaces, indent;

    if (NULL == define_stream || sdf_stream_eos(define_stream)) return kfalse;

    log_info(0, "Setting up the global #defines...");

    property_reset_all();
    define_count = 0;
    next_token_may_be_negative = ktrue;

    while (sdf_stream_read_line(define_stream))
    {
        // Count the indentation (by 2's), and skip over any whitespace...
        pxss_line_pointer = define_stream->read;
        spaces = count_indentation(define_stream);
        indent = spaces >> 1;

        // Skip empty lines
        if ( *(define_stream->read) == EOS ) continue;

        // Skip comments
        if ( *(define_stream->read) == '/' &&  *(define_stream->read + 1) == '/') continue;

        // Check for a #define...
        if ( 0 == strncmp(define_stream->read, "#define", 7) )
        {

            define_stream->read += 7;

            if (pxss_read_token(define_stream, tokens + 0))
            {
                define_add(define_stream, tokens[0].tag, SCOPE_GLOBAL, ktrue);
            }
        }
        else if ( 0 == strncmp(define_stream->read, "#proper", 7) )
        {
            define_stream->read += 7;

            if (pxss_read_token(define_stream, tokens + 0))  // tag
            {
                if (pxss_read_token(define_stream, tokens + 1)) // type
                {
                    if (pxss_read_token(define_stream, tokens + 2)) // offset
                    {
                        property_add(tokens[0].tag, tokens[1].tag[0], tokens[2].tag);
                    }
                }
            }
        }
    }

    log_info(0, "%d of %d properties used", property_count, MAX_PROPERTY);
    log_info(0, "%d of %d defines used", define_count, PXSS_MAX_DEFINE);

    return ktrue;

}


//-----------------------------------------------------------------------------------------------
size_t pxss_emit_opcodes(SDF_PSTREAM cstream, SDF_PSTREAM pstream, int indent)
{
    // <ZZ> This function writes the finalized opcodes for a single line of a SRC file.  The
    //      opcodes are put in the compile_stream.  Token_count is the number of RPN symbols found
    //      previously, which doesn't include tokens like '(' and ','.
    int     i;
    TOKEN * ptoken;
    Uint32  tempint;
    float   tempflt;
    bool_t  last_token_was_equals;
    size_t  return_offset = UINT32_MAX;
    OPCODE  opcode;
    size_t  offset;

    // Remember where while's start, because we need to jump back to 'em...
    if (OPCODE_INT_WHILE == tokens[0].opcode)
    {
        log_info(1, "WHILE JUMPS TO BACK TO HERE 0x%x", CAST(size_t, cstream->read - cstream->read_beg));
        while_jumps_back_to_here[indent] = CAST(size_t, cstream->read - cstream->read_beg);
    }

    //log_info(0, "\t:%s", pxss_line_pointer);

    /* We need the line numbers, as that ensures the stack is reset after
       each script line (some expressions push values to the stack, which
       are only used if the expression was not a statement e.g. INCREMENT)
       */

    // write the line number
    offset = CAST(size_t, cstream->read - cstream->read_beg);
    //log_info(1, "\t\t0x%06d: (0x%02x) %s ", offset, OPCODE_DEBUG_LINE_NUMBER, opcode_name[OPCODE_DEBUG_LINE_NUMBER]);
    //log_info(1, "\t\t\t0x%04d", pstream->line);

    _emit_uint08(cstream, OPCODE_DEBUG_LINE_NUMBER);
    _emit_uint16(cstream, pstream->line);

#if defined(_DEBUG) && defined(DEVTOOL)
    // write the offset to the source file
    // !!!!this will not survive saving and reloading!!!!
    offset = CAST(size_t, cstream->read - cstream->read_beg);
    //log_info(1, "\t\t0x%06d: (0x%02x) %s ", offset, OPCODE_DEBUG_LINE_POINTER, opcode_name[OPCODE_DEBUG_LINE_POINTER]);
    //log_info(1, "\t\t\t0x%08p", pxss_line_pointer);

    _emit_uint08(cstream, OPCODE_DEBUG_LINE_POINTER);
    _emit_uint32(cstream, (Uint32)pxss_line_pointer);

#endif


    last_token_was_equals = kfalse;
    repeat(i, rpn_stack_size)
    {
        // Write out the opcode and any extended data...
        ptoken = rpn_stack[i].ptok;

        offset = CAST(size_t, cstream->read - cstream->read_beg);

        // localize the opcode
        opcode = (OPCODE)ptoken->opcode;

        switch (opcode)
        {
            case OPCODE_INT_WHILE:
            case OPCODE_INT_IF:
                opcode = OPCODE_IFFALSEJUMP;
                break;

            case OPCODE_INT_RETURN:
                assert(kfalse);
                opcode = OPCODE_NIL;
                break;

            case OPCODE_INT_COMMA:
            case OPCODE_INT_LPAREN:
            case OPCODE_INT_RPAREN:
            case OPCODE_INT_LBRACKET:
            case OPCODE_INT_RBRACKET:
            case OPCODE_INT_ELSE:
                opcode = OPCODE_NIL;
                break;
        }

        // emit the basic opcode
        //log_info(1, "\t\t0x%06d: (0x%02x) %s ", offset, opcode, opcode_name[opcode]);
        _emit_uint08(cstream, opcode);

        // do not further process NIL opcodes
        if (OPCODE_NIL == opcode)
            continue;

        // Look for variables that are used before being set...
        if (last_token_was_equals)
        {
            if (!token_set_valid_lvalue(ptoken))
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Can only set/assign variables");
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
            }
        }
        else
        {
            if (!token_is_valid_rvalue(ptoken))
            {
                // Variable has been used before being set...
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Variable %s used before being set", opcode_name[ptoken->opcode]);
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
            }

        }

        last_token_was_equals = kfalse;

        if (ptoken->opcode == OPCODE_ASSIGN || ptoken->opcode == OPCODE_F_ASSIGN)
        {
            last_token_was_equals = ktrue;
        }
        else if (OPCODE_CALLFUNCTION == ptoken->opcode)
        {
            // Call function...  4 byte jump location, 4 byte return
            // position, 4 byte argument list pointer, 4 byte file start address,
            // then the zero terminated function name

            log_info(1, ":  0x00000000 (jump)");
            _emit_uint32(cstream, 0);

            log_info(1, ":  0x00000000 (return)");
            _emit_uint32(cstream, 0);

            log_info(1, ":  0x00000000 (arg)");
            _emit_uint32(cstream, 0);

            log_info(1, ":  0x00000000 (filestart)");
            _emit_uint32(cstream, 0);

            log_info(1, ":  \"%s\"", ptoken->tag);
            _emit_string(cstream, ptoken->tag);
        }
        else if (OPCODE_INT_IF       == ptoken->opcode)
        {
            // Jump function (if)...  2 byte address...  Filled in later...

            log_info(1, "STARTED A NEW IF STATEMENT");

            return_offset = CAST(size_t, cstream->read - cstream->read_beg);
            put_jump_offset_here[indent] = return_offset;
            last_jump_type_found[indent] = PXSS_JUMP_IF;

            log_info(1, ":  0xFFFF");
            _emit_uint16(cstream, 0xFFFF);
        }
        else if (OPCODE_INT_WHILE    == ptoken->opcode)
        {
            // Jump function (while)...  2 byte address...  Filled in later...

            log_info(1, "STARTED A NEW WHILE STATEMENT");

            return_offset = CAST(size_t, cstream->read - cstream->read_beg);
            put_jump_offset_here[indent] = return_offset;
            last_jump_type_found[indent] = PXSS_JUMP_WHILE;

            log_info(1, ":  0xFFFF");
            _emit_uint16(cstream, 0xFFFF);
        }
        else if (ptoken->opcode >= OPCODE_PROP00 && ptoken->opcode <= OPCODE_PROP1F)
        {
            // 101xxxxx...  Integer variable with dot parameter...  One byte pxss_header follows
            tempint = CAST(Uint8, DEREF(Uint32, &ptoken->extension) & 0xFF);

            log_info(1, ":  0x%02x", tempint);
            _emit_uint08(cstream, tempint);
        }
        else if (ptoken->opcode == OPCODE_INT08)
        {
            // 227...  Integer numeric token....  1 byte data follows (signed)
            tempint = CAST(Uint8, DEREF(Uint32, &ptoken->extension) & 0xFF);

            log_info(1, ":  0x%02x", tempint);

            _emit_uint08(cstream, tempint);
        }
        else if (ptoken->opcode == OPCODE_INT16)
        {
            // 228 ... Integer numeric token...  2 byte data follows (signed)...
            tempint = CAST(Uint16, DEREF(Uint32, &ptoken->extension) & 0xFFFF);

            log_info(1, ":  0x%04x", tempint);

            _emit_uint16(cstream, tempint);
        }
        else if (ptoken->opcode == OPCODE_INT32)
        {
            // 229...  Integer numeric token or pointer to a string...  4 byte data follows (signed)...
            tempint = DEREF(Uint32, &ptoken->extension);

            if (0 == strncmp(ptoken->tag, "FILE:", 5))
            {
                // This is a pointer to an external file.
                // We need to set the offset in the header so that loadize can
                // fix the pointer to the string when it loads the .run file.

                log_info(1, "Weird string fill-in-later thing.... \"%s\"", cstream->read_beg + ptoken->extension);
                endian_write_mem_int16( (cstream->read_beg + ptoken->extension) - 2, CAST(size_t, cstream->read - cstream->read_beg) );

                tempint = 0xFFFFFFFF;
            }

            log_info(1, ":  0x%08x", 0);

            _emit_uint32(cstream, tempint);
        }
        else if (ptoken->opcode == OPCODE_FLOAT)
        {
            // 230...  32 bit IEEE Float numeric token...  4 byte data follows (signed)...
            tempflt = DEREF(float, &ptoken->extension);

            log_info(1, ":  %f", tempflt);

            _emit_float(cstream, tempflt);
        }
        else if (ptoken->opcode == OPCODE_ZSTRING)
        {
            tempint = DEREF(Uint32, &ptoken->extension);

            // This is a string literal.  It is stored at a given offset from
            // the beginning of the file.

            // !!!! There is no benefit to store an actual pointer in the RUN file
            // !!!! here, so we will just store the offset

            log_info(1, ":  0x%04x", ptoken->extension);
            _emit_uint16(cstream, ptoken->extension);
        };

        // Now insert variable type conversions...
        if ( !last_token_was_equals && VAR_UNKNOWN != ptoken->change_type && (ptoken->change_type != ptoken->variable_type) )
        {
            if ( VAR_FLT != ptoken->change_type && VAR_FLT == ptoken->variable_type)
            {
                //log_info(0, "OPC:  0x%02x (to int)", OPCODE_TOINT);
                _emit_uint08(cstream, OPCODE_TOINT);
            }
            else if (VAR_FLT == ptoken->change_type && VAR_FLT != ptoken->variable_type)
            {
                //log_info(0, "OPC:  0x%02x (to float)", OPCODE_TOFLOAT);
                _emit_uint08(cstream, OPCODE_TOFLOAT);
            }
        }


    }
    log_info(1, "");

    return return_offset;
}

//-----------------------------------------------------------------------------------------------
int pxss_find_function_entry(Uint8* filedata, char* functionname)
{
    // <ZZ> This function searches a .RUN file's header for a given function, and returns
    //      the function's entry (offset in the header) if it finds a match.  If there weren't
    //      any matches, it returns 0.  Functionname should be all uppercase...
    int number_of_functions;
    int i;
    Uint8* filedatastart;



    // For each function in the header...
    filedatastart = filedata;
    filedata += MAX_FAST_FUNCTION << 1;  // Skip the fast function lookups...
    number_of_functions = endian_read_mem_int16(filedata);
    filedata += 4;
    repeat(i, number_of_functions)
    {
        // Check for a match...
        if (0 == strcmp(functionname, (filedata + 2)))
        {
            return (filedata - filedatastart);
        }

        // Go to the next entry
        filedata += 2;                    // Skip the address
        filedata += strlen(filedata) + 1; // Skip the name and 0
        filedata += strlen(filedata) + 1; // Skip the return and any arguments and 0
    }
    // Didn't find a match
    return 0;
}

//-----------------------------------------------------------------------------------------------
int pxss_find_string_entry(Uint8* filedata, char* stringname)
{
    // <ZZ> This function searches a .RUN file's header for a given string, and returns
    //      the string's entry (offset in the header) if it finds a match.  If there weren't
    //      any matches, it returns 0.
    int number_of_functions;
    int number_of_strings;
    int i;
    Uint16 j;
    Uint8* filedatastart;


    log_info(1, "Looking for string %s...", stringname);


    // For each function in the header...
    filedatastart = filedata;
    filedata += MAX_FAST_FUNCTION << 1;  // Skip the fast function lookups...
    number_of_functions = ENDIAN_READ_STREAM_16( filedata );
    number_of_strings   = ENDIAN_READ_STREAM_16( filedata );
    repeat(i, number_of_functions)
    {
        // Go to the next entry
        filedata += 2;                    // Skip the address
        filedata += strlen(filedata) + 1; // Skip the name and 0
        filedata += strlen(filedata) + 1; // Skip the return and any arguments and 0
    }

    repeat(i, number_of_strings)
    {
        // !!!! In the case of multiple "FILE:BLAH.BLAH" strings, we need to keep a
        // !!!! seperate reference to EACH occurrence of the string so the loader can
        // !!!! adjust all of the dangling external pointers

        j = endian_read_mem_int16(filedata);

        if (j == UINT16_MAX)
        {
            // Check for a match...
            if (0 == strcmp(stringname, filedata + 2))
            {
                log_info(1, "Found string \"%s\" at 0x%04x", stringname, (filedata - filedatastart));
                endian_write_mem_int16(filedata, 0);
                return (filedata - filedatastart);
            }
        }

        // Go to the next entry
        filedata += 2;                    // Skip the address
        filedata += strlen(filedata) + 1; // Skip the name and 0
    }
    // Didn't find a match
    return 0;
}

//-----------------------------------------------------------------------------------------------
Uint8* pxss_mega_find_function(Uint8* functionstring, const char* filename, Uint8 request)
{
    // <ZZ> This function may only be called after a complete headerize has been performed.  It
    //      determines the header entry location of a function based on a "file.function" type
    //      string or just a "function" string, and then returns a bit of requested information
    //      about that function.  Filename is the name of the file in which the string was found,
    //      not necessarily the file the function is found in.  It returns NULL if no matching
    //      function could be found.
    Uint8* functionname;
    Uint8  must_fix_dot;
    Uint8* data;
    Uint32 entry_offset;
    Uint8* entry;


    // Split a two part string into two parts by figuring out where the dot is...
    must_fix_dot = kfalse;
    functionname = strpbrk(functionstring, ".");

    if (functionname != NULL)
    {
        // Found a dot, so we should have an explicit filename...
        filename = functionstring;
        *functionname = 0;    // Split into two strings...
        must_fix_dot = ktrue;  // ...But remember to put the dot back later...
        functionname++;
    }
    else
    {
        // No dot, so it's just a function name, no file name
        functionname = functionstring;
    }


    // Open the file that we think it's in...
    entry = NULL;

    // scan THIS file for the function
    data = pxss_find_int_data(filename);

    if (NULL == data)
    {
        data = pxss_find_run_data(filename);
    }

    // Found the data, so now look for the function
    entry_offset = pxss_find_function_entry(data, functionname);

    if (UINT16_MAX != entry_offset && 0x0000 != entry_offset)
    {
        entry = data + entry_offset;
    }


    if (entry == NULL && !must_fix_dot)
    {
        // Didn't find the function in this file
        // scan the GENERIC file for the function

        data = pxss_find_int_data("GENERIC");

        if (NULL == data)
        {
            data = pxss_find_run_data("GENERIC");
        }

        // Found the file, so now look for the function
        entry_offset = pxss_find_function_entry(data, functionname);

        if (UINT16_MAX != entry_offset && 0x0000 != entry_offset)
        {
            entry = data + entry_offset;
        }
    }


    // Put the dot back if we took it out...
    if (must_fix_dot)
    {
        *(functionname - 1) = '.';
    }

    // return NULL on an error
    if (NULL == entry)
        return NULL;


    // ---- FUNCTION FOUND ----
    // Log it...
    log_info(1, "Mega_found function %s", functionstring);

    // Return whatever was requested...
    switch (request)
    {
        case PXSS_REQUEST_ENTRY:
            return entry;
            break;

        case PXSS_REQUEST_OFFSET:
            return (data + endian_read_mem_int16(entry));
            break;

        case PXSS_REQUEST_ARGUMENTS:
            entry += 2;               // Skip the offset
            entry += strlen(entry) + 1; // Skip the function name
            return entry;             // Return a pointer to the arguments
            break;

        case PXSS_REQUEST_FILESTART:
            return data;
            break;
    };


    return NULL;
}

//-----------------------------------------------------------------------------------------------
void pxss_register_tokens_basic()
{
    TOKEN tmp_tok;

    // register operators
    tmp_tok.tag[0]            = '\0';
    tmp_tok.opcode            = OPCODE_NIL;
    tmp_tok.type              = TOKEN_UNKNOWN;
    tmp_tok.arg_list          = arg_list_none;
    tmp_tok.variable_type     = VAR_UNKNOWN;
    tmp_tok.number_to_destroy = -1;
    tmp_tok.extension         = -1;
    tmp_tok.is_in_rpn         = kfalse;
    tmp_tok.change_type       = VAR_UNKNOWN;
    tmp_tok.fix_bits          = FIX_INFIX | FIX_INFIX | FIX_POSTFIX;


    // register the postfix assignmant operators
    tmp_tok.type          = TOKEN_OPERATOR;
    tmp_tok.fix_bits      = FIX_POSTFIX | FIX_ASSIGN;
    tmp_tok.variable_type = VAR_UNKNOWN;
    { strcpy(tmp_tok.tag, "++");  tmp_tok.opcode =  OPCODE_INCREMENT;      tmp_tok.number_to_destroy = 1; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "--");  tmp_tok.opcode =  OPCODE_DECREMENT;      tmp_tok.number_to_destroy = 1; pxss_register_token(&tmp_tok); }

    // register the infix assignmant operators
    tmp_tok.type          = TOKEN_OPERATOR;
    tmp_tok.fix_bits      = FIX_INFIX | FIX_ASSIGN;
    tmp_tok.variable_type = VAR_UNKNOWN;
    { strcpy(tmp_tok.tag, "=" );  tmp_tok.opcode =  OPCODE_ASSIGN;         tmp_tok.number_to_destroy = 1; pxss_register_token(&tmp_tok); }


    // register the prefix operators
    tmp_tok.type          = TOKEN_OPERATOR;
    tmp_tok.fix_bits      = FIX_PREFIX;
    tmp_tok.variable_type = VAR_UNKNOWN;
    { strcpy(tmp_tok.tag, "!" );  tmp_tok.opcode =  OPCODE_LOGICALNOT;     tmp_tok.number_to_destroy = 1; pxss_register_token(&tmp_tok); }

    // register the infix operators
    tmp_tok.type          = TOKEN_OPERATOR;
    tmp_tok.fix_bits      = FIX_INFIX;
    tmp_tok.variable_type = VAR_UNKNOWN;
    { strcpy(tmp_tok.tag, "*" );  tmp_tok.opcode =  OPCODE_MULTIPLY;       tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "/" );  tmp_tok.opcode =  OPCODE_DIVIDE;         tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "==");  tmp_tok.opcode =  OPCODE_ISEQUAL;        tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "!=");  tmp_tok.opcode =  OPCODE_ISNOTEQUAL;     tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, ">=");  tmp_tok.opcode =  OPCODE_ISGREATEREQUAL; tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "<=");  tmp_tok.opcode =  OPCODE_ISLESSEREQUAL;  tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, ">" );  tmp_tok.opcode =  OPCODE_ISGREATER;      tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "<" );  tmp_tok.opcode =  OPCODE_ISLESSER;       tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }

    // int valued operators infix operators
    tmp_tok.type          = TOKEN_OPERATOR;
    tmp_tok.fix_bits      = FIX_INFIX;
    tmp_tok.variable_type = VAR_INT;
    { strcpy(tmp_tok.tag, "&&");  tmp_tok.opcode =  OPCODE_LOGICALAND;     tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok);  }
    { strcpy(tmp_tok.tag, "||");  tmp_tok.opcode =  OPCODE_LOGICALOR;      tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "&" );  tmp_tok.opcode =  OPCODE_BITWISEAND;     tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "|" );  tmp_tok.opcode =  OPCODE_BITWISEOR;      tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "<<");  tmp_tok.opcode =  OPCODE_BITWISELEFT;    tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, ">>");  tmp_tok.opcode =  OPCODE_BITWISERIGHT;   tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "%" );  tmp_tok.opcode =  OPCODE_MODULUS;        tmp_tok.number_to_destroy = 2; pxss_register_token(&tmp_tok); }

    // register in-determinate operators
    tmp_tok.type          = TOKEN_OPERATOR;
    tmp_tok.fix_bits      = FIX_PREFIX | FIX_INFIX;
    tmp_tok.variable_type = VAR_UNKNOWN;
    { strcpy(tmp_tok.tag, "+" );  tmp_tok.opcode =  OPCODE_INT_PLUS;  pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "-" );  tmp_tok.opcode =  OPCODE_INT_MINUS; pxss_register_token(&tmp_tok); }

    // register basic functions
    tmp_tok.type          = TOKEN_FUNCTION;
    tmp_tok.fix_bits      = FIX_PREFIX;
    tmp_tok.variable_type = VAR_UNKNOWN;
    { strcpy(tmp_tok.tag, "IF");        tmp_tok.opcode = OPCODE_INT_IF;       tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT;       pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WHILE");     tmp_tok.opcode = OPCODE_INT_WHILE;    tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT;       pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "ELSE");      tmp_tok.opcode = OPCODE_INT_ELSE;     tmp_tok.number_to_destroy = 0; tmp_tok.arg_list = arg_list_none;     tmp_tok.variable_type = VAR_INT;     pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "RETURN");    tmp_tok.opcode = OPCODE_INT_RETURN;   tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_none;     tmp_tok.variable_type = VAR_UNKNOWN; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "TOINT");     tmp_tok.opcode = OPCODE_TOINT;        tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_f;    tmp_tok.variable_type = VAR_INT;         pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "TOFLOAT");   tmp_tok.opcode = OPCODE_TOFLOAT;      tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_FLT;       pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "SQRT");      tmp_tok.opcode = OPCODE_SQRT;         tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_f;    tmp_tok.variable_type = VAR_FLT;         pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "SIN");       tmp_tok.opcode = OPCODE_SIN;          tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_f;    tmp_tok.variable_type = VAR_FLT;         pxss_register_token(&tmp_tok); }
};

//-----------------------------------------------------------------------------------------------
bool_t pxss_tokenize_line(SDF_PSTREAM cstream, SDF_PSTREAM pstream, const char* filename)
{
    // <ZZ> This function helps the RPN code figure out what a given token is.  It breaks tokens
    //      into operands, functions, and operators (filling in the corresponding flag arrays).

    int i, j;
    bool_t found_error, found;

    // clear out any RPN data from the token
    // make sure to keep variable_type and is_operator
    // since they are determined already in _pxss_parse_line()
    repeat(i, token_count)
    {
        tokens[i].is_in_rpn         = kfalse;
        tokens[i].change_type       = VAR_UNKNOWN;
    }


    line_was_a_conditional = line_is_a_conditional;
    line_is_a_conditional  = kfalse;

    // Figure out the opcode for each token, and what type of token it is...
    found_error = kfalse;
    repeat(i, token_count)
    {
        //---- no need to process some tokens ----

        if (TOKEN_DELIMITER == tokens[i].type)
        {
            // deal with special tokens
            tokens[i].variable_type     = VAR_UNKNOWN;
            tokens[i].number_to_destroy = 0;
            continue;
        }

        //---- begin handling tokens that need further processing ----

        found = kfalse;

        if (VAR_PTR == tokens[i].tag[0])
        {
            // a property variable
            tokens[i].number_to_destroy = -1;
            tokens[i].type = TOKEN_OPERAND;
            continue;
        }
        else if (VAR_INT == tokens[i].variable_type)
        {
            if (tokens[i].opcode == OPCODE_INT32)
            {
                // this is a literal integer.
                // read the value into the extensinand optimize the opcode
                sscanf(tokens[i].tag, "%d", &tokens[i].extension);

                if (tokens[i].extension >= -128 && tokens[i].extension <= 127) tokens[i].opcode = OPCODE_INT08;
                else if (tokens[i].extension >= -32768 && tokens[i].extension <= 32767) tokens[i].opcode = OPCODE_INT16;
                else if (tokens[i].extension == 0)  tokens[i].opcode = OPCODE_ZERO;
                else if (tokens[i].extension == 1)  tokens[i].opcode = OPCODE_INT_ONE;
            }


            tokens[i].number_to_destroy = -1;
            tokens[i].type = TOKEN_OPERAND;
            continue;
        }
        else if (VAR_FLT == tokens[i].variable_type)
        {
            if (tokens[i].opcode == OPCODE_FLOAT)
            {
                // this is a literal number.
                // ??? if it got this far and we think it's a float, we dont't really care
                // if it is actually an integer???

                sscanf(tokens[i].tag, "%f", CAST(float *, &(tokens[i].extension)));

                if ( 1.0f == DEREF(float, &(tokens[i].extension)) )  tokens[i].opcode = OPCODE_FLOAT_ONE;
            }

            tokens[i].number_to_destroy = -1;
            tokens[i].type = TOKEN_OPERAND;
            continue;
        }
        else if (VAR_STRING == tokens[i].variable_type)
        {
            // this is a literal string

            OPCODE loc_opcode = OPCODE_NIL;

            if (0 == strncmp(tokens[i].tag, "FILE:", 5))
            {
                loc_opcode = OPCODE_INT32;
            }
            else
            {
                loc_opcode = OPCODE_ZSTRING;
            }


            // Figure out the offset of the string in memory...  2 to skip address in header...
            tokens[i].extension = pxss_find_string_entry(cstream->read_beg, tokens[i].tag);
            tokens[i].extension += 2;  // Skip the offset

            // Need to go back through these during functionize...
            // ...But in order to do that, we need to remember where the opcode is...
            // ...But we don't know where it is yet, so we'll have to wait 'til later...
            tokens[i].opcode        = loc_opcode;
            tokens[i].variable_type = VAR_STRING;

            tokens[i].number_to_destroy = -1;
            tokens[i].type = TOKEN_OPERAND;
            continue;
        }
        else if (TOKEN_OPERATOR == tokens[i].type)
        {
            bool_t found_token;

            repeat(j, pxss_token_index)
            {
                if (tokens[i].type != pxss_token_list[j].type) continue;

                if ( 0 == strcmp(tokens[i].tag, pxss_token_list[j].tag) )
                {
                    // need to copy everything but the extension.
                    Sint32 extension = tokens[i].extension;
                    memcpy(tokens + i, pxss_token_list + j, sizeof(TOKEN));
                    tokens[i].extension = extension;

                    break;
                }
            }

            found_token = (OPCODE_NIL != tokens[i].opcode);

            // Subtract and Negate use '-' and Unary Plus and Add use '+', check local conditions...
            if (OPCODE_INT_MINUS == tokens[i].opcode)
            {
                // Check the value to the left...
                if (i > 0)
                {
                    if ( (TOKEN_OPERAND != tokens[i-1].type) && OPCODE_INT_RPAREN != tokens[i-1].opcode)
                    {
                        // It's a negate...
                        tokens[i].opcode            = OPCODE_NEGATE;
                        tokens[i].number_to_destroy = 1;
                        tokens[i].fix_bits          = FIX_PREFIX;
                    }
                    else
                    {
                        // It's a subtract...
                        tokens[i].opcode            = OPCODE_SUBTRACT;
                        tokens[i].number_to_destroy = 2;
                        tokens[i].fix_bits          = FIX_INFIX;
                    }
                }
                else
                {
                    // It's a negate...
                    tokens[i].opcode            = OPCODE_NEGATE;
                    tokens[i].number_to_destroy = 1;
                    tokens[i].fix_bits          = FIX_PREFIX;
                }
            }

            else if (OPCODE_INT_PLUS == tokens[i].opcode)
            {
                // Check the value to the left...
                if (i > 0)
                {
                    if ( (TOKEN_OPERAND != tokens[i-1].type) && OPCODE_INT_RPAREN != tokens[i-1].opcode)
                    {
                        // It's a unary plus, which has no effect...
                        tokens[i].opcode            = OPCODE_NIL;
                        tokens[i].number_to_destroy = 0;
                        tokens[i].fix_bits          = FIX_PREFIX;
                    }
                    else
                    {
                        // It's a subtract...
                        tokens[i].opcode            = OPCODE_ADD;
                        tokens[i].number_to_destroy = 2;
                        tokens[i].fix_bits          = FIX_INFIX;
                    }
                }
                else
                {
                    // It's a unary plus, which has no effect...
                    tokens[i].opcode            = OPCODE_NIL;
                    tokens[i].number_to_destroy = 0;
                    tokens[i].fix_bits          = FIX_PREFIX;
                }
            }

            if (found_token) continue;
        }

        // test for all pre-defined functions
        {

            repeat(j, pxss_token_index)
            {
                if (TOKEN_OPERATOR == tokens[i].type || TOKEN_DELIMITER == tokens[i].type) continue;

                if ( 0 == strcmp(tokens[i].tag, pxss_token_list[j].tag) )
                {
                    // need to copy everything but the extension.
                    Sint32 extension = tokens[i].extension;
                    memcpy(tokens + i, pxss_token_list + j, sizeof(TOKEN));
                    tokens[i].extension = extension;

                    break;
                }
            }

            if (OPCODE_INT_IF == tokens[i].opcode || OPCODE_INT_WHILE == tokens[i].opcode || OPCODE_INT_ELSE == tokens[i].opcode)
            {
                // conditional function
                line_is_a_conditional = ktrue;
            }
            else if (OPCODE_INT_RETURN == tokens[i].opcode)
            {
                // Return function...
                if (last_function_returns_integer) { tokens[i].opcode = OPCODE_RETURNINT;   tokens[i].arg_list = arg_list_i; tokens[i].variable_type = VAR_INT; }
                else                               { tokens[i].opcode = OPCODE_RETURNFLOAT; tokens[i].arg_list = arg_list_f; tokens[i].variable_type = VAR_FLT; }
            }

            // !!!! More basic functions !!!!
            if (tokens[i].opcode != OPCODE_NIL)
            {
                tokens[i].type = TOKEN_FUNCTION;
                continue;
            }
        }

        // Search for extended functions
        //if( pxss_extended_find_function(tokens + i) ) continue;

        // Search for a user-defined function
        tokens[i].arg_list = pxss_mega_find_function(tokens[i].tag, filename, PXSS_REQUEST_ARGUMENTS);

        if (NULL != tokens[i].arg_list)
        {
            tokens[i].opcode        = OPCODE_CALLFUNCTION;
            tokens[i].variable_type = CHAR_TO_VAR(tokens[i].arg_list[0]);
            tokens[i].arg_list++;  // Skip the return code
            tokens[i].number_to_destroy = strlen(tokens[i].arg_list);

            tokens[i].type = TOKEN_FUNCTION;
            continue;
        }


        // !!!! if it gets to here, it must be a user function that is improperly referenced,
        // !!!! or an operand that we forgot to define, or an operator that is not defined
        // !!!! in any case it is a bad token...

        log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
        log_message(0, "Unrecognized symbol %s found", tokens[i].tag);
        log_message(0, "\"%s\"", pxss_line_pointer);
        _pxss_dump_tokens();
        log_message(0, "");

        pxss_file_found_error = ktrue;
        break;
    };

    pxss_file_found_error = found_error || pxss_file_found_error;
    return found_error;
}


//-----------------------------------------------------------------------------------------------
bool_t pxss_headerize(SDF_PSTREAM src_stream, SDF_PSTREAM int_stream, char * filename)
{
    // <ZZ> This function runs through an SRC file that has been stored in memory and finds
    //      function information to store in the header.  This must be done for every file
    //      before compilation can occur.  Index is a pointer to the start of the file's
    //      pxss_header in the sdf_archive, and can be gotten from sdf_archive_find_index_from_filename.  If the function
    //      works okay, it should create a new RUN file in the pxss_header and return ktrue.  If
    //      it fails it should return kfalse.

    Uint8* offset_begin;

    Sint8 keepgoing;
    Uint8 spaces, indent, indent_last;
    Uint16 number_of_functions;
    Uint16 number_of_strings;

    int i;
    int token_count;
    char returncode;

    // save the file name for debugging
    pxss_file_pointer = filename;

    // Don't create the RUN file if we find any errors...
    pxss_file_found_error = kfalse;


    // Undefine any variables and defines used by the last file
    define_pop_to_scope(SCOPE_GLOBAL);

    // Pad the start of the header with fast function offsets...
    sdf_stream_rewind(int_stream);
    repeat(i, MAX_FAST_FUNCTION)
    {
        // 2 bytes each...
        _emit_uint16(int_stream, UINT16_MAX);
    }
    // Something like this in the int_stream->..
    //   0000 0000 0000 0000 0000 0000 0000 0000 ....


    // Make a list of all the functions in this file...  Figure out offsets in another pass...
    offset_begin = int_stream->read;
    _emit_uint16(int_stream, 0x0000);  // Skip a word for the function count...  Filled in after counting...
    _emit_uint16(int_stream, 0x0000);  // Skip a word for the string   count...  Filled in after counting...

    log_info(1, "Headerizing functions...");

    sdf_stream_rewind(src_stream);
    number_of_functions = 0;
    next_token_may_be_negative = ktrue;

    while (sdf_stream_read_line(src_stream))
    {
        // Count the indentation (by 2's), and skip over any whitespace...
        pxss_line_pointer = src_stream->read;
        spaces = count_indentation(src_stream);
        indent = spaces >> 1;

        // Skip empty lines
        if ( *(src_stream->read) == EOS ) continue;

        // Skip comments
        if ( *(src_stream->read) == '/' &&  *(src_stream->read + 1) == '/') continue;

        // Skip #defines, we're only interested in functions...
        if (*(src_stream->read) == '#' || indent > 0) continue;

        // we're looking for functions!
        if (indent > 0) continue;

        // Read all of the tokens on this line...
        token_count = 0;
        keepgoing = ktrue;

        while (token_count < PXSS_MAX_TOKEN && pxss_read_token(src_stream, tokens + token_count))
        {
            log_info(1, "%s (line %d) - %s", pxss_file_pointer, src_stream->line, tokens[token_count].tag);
            log_info(1, "");
            token_count++;
        }

        if (token_count == 0) continue;


        // Get the return type for the function...
        i = 0;
        returncode = VAR_INT;

        if (0 == strcmp(tokens[0].tag, "INT"  ))  { returncode = VAR_INT;  i++; }
        else if (0 == strcmp(tokens[0].tag, "FLOAT"))  { returncode = VAR_FLT;  i++; }


        // Make sure the function has parentheses after it
        if (OPCODE_INT_LPAREN == tokens[i+1].opcode)
        {
            // Now stick the function address and name in our RUN file...
            log_info(1, "function...  %s()", tokens[i].tag);

            _emit_uint16(int_stream, UINT16_MAX);
            _emit_string(int_stream, tokens[i].tag);

            // Now store the return code type...
            _emit_uint08(int_stream, VAR_TO_CHAR(returncode));

            // Now store any arguments types...
            i = i + 2;

            while (i < token_count)
            {
                if (0 == strcmp(tokens[i].tag, "INT"))
                {
                    _emit_uint08(int_stream, VAR_TO_CHAR(VAR_INT));
                }
                else if (0 == strcmp(tokens[i].tag, "FLOAT"))
                {
                    _emit_uint08(int_stream, VAR_TO_CHAR(VAR_FLT));
                }

                i++;
            }

            // And zero terminate the string...
            _emit_uint08(int_stream, EOS);

            number_of_functions++;
        }

    }

    endian_write_mem_int16(offset_begin, number_of_functions);
    // Something like this in the int_stream->..
    //   00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
    //   00 03 00 00
    //   0  0  D  E  A  T  H  00 I  F  F  F  I  00
    //   0  0  B  L  A  H  00 V  00
    //   0  0  A  T  T  A  C  K  00 V  I  00
    //   Address     FunctionName      ReturnAndArgumentTypes


    // Check for errors too...
    if (number_of_functions == 0)
    {
        log_error(0, "No functions found");
        goto pxss_headerize_fail;
    }


    // Now search for strings to append to the header...  Just like above...
    log_info(1, "Headerizing strings...");

    sdf_stream_rewind(src_stream);
    next_token_may_be_negative = ktrue;
    number_of_strings = 0;
    indent_last = 0;

    while (sdf_stream_read_line(src_stream))
    {
        // Count the indentation (by 2's), and skip over any whitespace...
        pxss_line_pointer = src_stream->read;
        spaces = count_indentation(src_stream);
        indent = spaces >> 1;

        // Skip empty lines
        if ( *(src_stream->read) == EOS ) continue;

        // Skip comments
        if ( *(src_stream->read) == '/' &&  *(src_stream->read + 1) == '/') continue;

        // Check for a #define...
        if ( 0 == strncmp(src_stream->read, "#define", 7) )
        {
            src_stream->read += 7;

            if (pxss_read_token(src_stream, tokens + 0))
            {
                define_add(src_stream, tokens[0].tag, SCOPE_FILE, ktrue);
            }

            continue;
        }

        // Read all of the tokens on this line...
        token_count = _pxss_parse_line(src_stream, kfalse);

#ifdef COMPILE_VERBOSE
        _pxss_dump_tokens();
#endif

        // Look through all of the tokens for a string token...
        repeat(i, token_count)
        {
            if (VAR_STRING != tokens[i].variable_type) continue;

            // Found a string, so put it in the header
            _emit_uint16(int_stream, UINT16_MAX);

            _emit_string(int_stream, tokens[i].tag);

            number_of_strings++;
        }
    }

    endian_write_mem_int16(offset_begin + 2, number_of_strings);
    // Something like this in the int_stream->..
    //   0000 0000 0000 0000 0000 0000 0000 0000
    //   0003 0002
    //   FFFF  D  E  A  T  H  00 I  F  F  F  I  00
    //   FFFF  B  L  A  H  00 V  00
    //   FFFF  A  T  T  A  C  K  00 V  I  00
    //   FFFF  B  i  g     B  o  b     i  s     c  o  o  l  00
    //   FFFF T  e  s  t     s  t  r  i  n  g  00
    //   FFFF F  I  L  E  :  T  E  S  T  .  T  X  T  00
    //   String address           String

    // done with any defines from this file. Get rid of them.
    define_pop_to_scope(SCOPE_GLOBAL);

    // Now copy the header pxss_data into a (temporary) RUN file
    if (pxss_file_found_error)
    {
        goto pxss_headerize_fail;
    };

    return ktrue;


pxss_headerize_fail:

    pxss_compiler_error = ktrue;

    return kfalse;
}
//-----------------------------------------------------------------------------------------------
bool_t pxss_compilerize(SDF_PSTREAM src_stream, SDF_PSTREAM compile_stream, const char* filename)
{
    // <ZZ> This function compiles an SRC file that has been stored in memory.  Index is a
    //      pointer to the start of the file's src_header in the sdf_archive, and can be gotten from
    //      sdf_archive_find_index_from_filename.  If the function works okay, it should create a new RUN file in the
    //      src_header and return ktrue.  It might also delete the original file to save space, but
    //      that's a compile time option.  If it fails it should return kfalse.

    char   tempstring[16];

    Uint8 spaces, indent;
    Uint8 last_indent;
    int i;

    Uint16 number_of_functions;
    int function_offset;
    int num_int_variables, num_flt_variables;

    // save the file name for debugging
    pxss_file_pointer = filename;


    // Don't create the RUN file if we find any errors...
    pxss_file_found_error = kfalse;


    // Log what we're doing
    log_info(1, "Decoding %s.SRC to %s.RUN", filename, filename);


    // Undefine any variables and defines used by the last file
    define_pop_to_scope(SCOPE_GLOBAL);


    // Clear out our jump int_data...
    repeat(i, PXSS_MAX_INDENT)
    {
        last_jump_type_found[i] = PXSS_JUMP_INVALID;
    }
    last_indent = 0;
    last_function_returns_integer = ktrue;


    // Remember what variables have been set (for generating errors)
    num_int_variables = 0;
    num_flt_variables = 0;
    repeat(i, MAX_VARIABLE)
    {
        flt_variable[i].set = kfalse;
        int_variable[i].set = kfalse;
    }





    // Make a list of all the functions in this file...  Figure out offsets in another pass...





    // For each line in the file...
    next_token_may_be_negative = ktrue;
    last_indent = indent   = 0;
    number_of_functions    = 0;
    line_was_a_conditional =
        line_is_a_conditional  = kfalse;

    while (sdf_stream_read_line(src_stream))
    {
        // Count the indentation (by 2's), and skip over any whitespace...
        pxss_line_pointer = src_stream->read;        // Remember the line text for debugging
        spaces = count_indentation(src_stream);

        // Skip empty lines
        if ( *(src_stream->read) == EOS ) continue;

        // Skip comments
        if ( *(src_stream->read) == '/' &&  *(src_stream->read + 1) == '/') continue;

        if (0 != (spaces & 0x01))
        {
            log_warning(0, "%s (line %d)", pxss_file_pointer, src_stream->line);
            log_message(0, "Odd number of spaces at beginning of line. Possible typo.");
            log_message(0, "\"%s\"", pxss_line_pointer);
            log_message(0, "");
        };

        // Check for a #define...
        if ( 0 == strncmp(src_stream->read, "#define", 7) )
        {
            src_stream->read += 7;

            if (pxss_read_token(src_stream, tokens + 0))
            {
                define_add(src_stream, tokens[0].tag, SCOPE_FILE, ktrue);
            }

            continue;
        }

        // Skip other #proper and any other preprocessor commands
        if ( '#' == *(src_stream->read) ) continue;

        // !!!! make sure that we scope all defines before parsing the line
        // !!!! otherwise, a function definition like "ModelXXXX(int self)",
        // !!!! followed by a function definition like "SpawnXXXX(int self)",
        // !!!! will parse the SpawnXXX() definition as "SpawnXXXX(int I00)))"
        if (spaces == 0)
        {
            define_pop_to_scope(SCOPE_FILE);

            repeat(i, MAX_VARIABLE)
            {
                if (int_variable[i].scope > SCOPE_FILE)
                {
                    int_variable[i].set   = kfalse;
                    int_variable[i].scope = -1;
                }

                if (flt_variable[i].scope > SCOPE_FILE)
                {
                    flt_variable[i].set = kfalse;
                    flt_variable[i].scope = -1;
                }
            }
        }
        else
        {
            int local_scope = SCOPE_FUNCTION + (spaces >> 1);

            define_pop_to_scope(local_scope);

            repeat(i, MAX_VARIABLE)
            {
                if (int_variable[i].scope > local_scope)
                {
                    int_variable[i].set   = kfalse;
                    int_variable[i].scope = -1;
                }

                if (flt_variable[i].scope > local_scope)
                {
                    flt_variable[i].set = kfalse;
                    flt_variable[i].scope = -1;
                }
            }
        }

        token_count = _pxss_parse_line(src_stream, ktrue);

        // Show me all of the tokens...
#ifdef VERBOSE_COMPILE
        //fprintf(stdout, "%s\n", pxss_line_pointer);
        _pxss_dump_tokens();
#endif

        // Did we actually read anything on this line?
        if (token_count <= 0) continue;

        // only worry about the indention for potentially valid lines
        last_indent      = indent;
        indent           = spaces >> 1;

        // Must have a conditional to change indentation levels...
        if (indent > last_indent && last_indent != 0)
        {
            if (PXSS_JUMP_INVALID == last_jump_type_found[last_indent])
            {
                // Throw an error
                log_warning(0, "%s (line %d)", pxss_file_pointer, src_stream->line);
                log_message(0, "Indents only valid with statements and function definitions.\n");
            }
        }

        // Is this line the start of a function?
        if (indent == 0)
        {
            line_was_a_conditional =
                line_is_a_conditional  = kfalse;

            if (last_indent > indent)
            {
                // Slap in a return 1, just to be on the safe side...
                pxss_close_jumps(compile_stream, indent, last_indent);
                pxss_add_return_opcode(compile_stream);
                last_indent = 0;
            }

            // Undefine any variables used by the last function
            //fprintf(stdout, "****reset all variables****\n");
            num_int_variables = 0;
            num_flt_variables = 0;
            repeat(i, MAX_VARIABLE)
            {
                flt_variable[i].set = kfalse;
                int_variable[i].set = kfalse;
            }

            // Skip over the return value...
            i = 0;
            last_function_returns_integer = ktrue;

            if (0 == strcmp(tokens[i].tag, "INT")) { last_function_returns_integer = ktrue; i++; }
            else if (0 == strcmp(tokens[i].tag, "FLOAT")) { last_function_returns_integer = kfalse; i++; }



            // Find the function in the RUN header...
            function_offset = pxss_find_function_entry(compile_stream->read_beg, tokens[i].tag);

            if (0 == function_offset)
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, src_stream->line);
                log_message(0, "Function offset not found (corrupt header?)", tokens[i].tag);
                log_message(0, "");
                pxss_file_found_error = ktrue;
                goto pxss_compilerize_fail;
            }


            // Then write down the offset...
            endian_write_mem_int16(compile_stream->read_beg + function_offset, CAST(size_t, compile_stream->read - compile_stream->read_beg));
            number_of_functions++;

            // Skip the function name
            i++;

            // Skip the parentheses
            if (OPCODE_INT_LPAREN == tokens[i].opcode)
            {
                i++;
            }
            else
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, src_stream->line);
                log_message(0, "Function argument list must begin with '('\n");
                pxss_file_found_error = ktrue;
            }

            // Now define any arguments...  #define value i0
            while (i < token_count)
            {
                if (0 == strcmp(tokens[i].tag, "INT"))
                {
                    i++;

                    if (num_int_variables >= MAX_VARIABLE)
                    {
                        log_error(0, "%s (line %d)", pxss_file_pointer, src_stream->line);
                        log_message(0, "Float variable overflow defining %s. Too many floats.\n", tokens[i].tag);
                        pxss_file_found_error = ktrue;
                        goto pxss_compilerize_fail;
                    }

                    if (i < token_count)
                    {
                        sprintf(tempstring, "%c%02d", VAR_INT, num_int_variables);
                        define_add_string(tokens[i].tag, tempstring, SCOPE_FUNCTION + indent + 1, kfalse);
                        int_variable[num_int_variables].set   = ktrue;
                        int_variable[num_int_variables].scope = SCOPE_FUNCTION + indent + 1;
                        num_int_variables++;
                        i++;
                    }
                }
                else if (0 == strcmp(tokens[i].tag, "FLOAT"))
                {
                    i++;

                    if (num_int_variables >= MAX_VARIABLE)
                    {
                        log_error(0, "%s (line %d)", pxss_file_pointer, src_stream->line);
                        log_message(0, "Float variable overflow defining %s. Too many floats.\n", tokens[i].tag);
                        pxss_file_found_error = ktrue;
                        goto pxss_compilerize_fail;
                    }


                    if (i < token_count)
                    {
                        sprintf(tempstring, "%c%02d", VAR_FLT, num_flt_variables);
                        define_add_string(tokens[i].tag, tempstring, SCOPE_FUNCTION + indent + 1, kfalse);
                        flt_variable[num_flt_variables].set   = ktrue;
                        flt_variable[num_flt_variables].scope = SCOPE_FUNCTION + indent + 1;
                        num_flt_variables++;
                        i++;
                    }
                }
                else if (OPCODE_INT_COMMA == tokens[i].opcode)
                {
                    i++;
                }
                else if (OPCODE_INT_RPAREN == tokens[i].opcode)
                {
                    break;
                }
                else
                {
                    log_error(0, "%s (line %d)", pxss_file_pointer, src_stream->line);
                    log_message(0, "Unexpected token \"%s\" found in function definition\n", tokens[i].tag);
                    pxss_file_found_error = ktrue;
                }
            }

            // Skip the parentheses
            if (OPCODE_INT_RPAREN == tokens[i].opcode)
            {
                i++;
            }
            else
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, src_stream->line);
                log_message(0, "Function argument list must end with ')'\n");
                pxss_file_found_error = ktrue;
            }

            continue;
        }


        // Check for local int and float declarations...  Use temporary defines... I0-I31, F0-F31
        if (indent >= 1)
        {
            if (0 == strcmp(tokens[0].tag, "INT"))
            {
                i = 1;

                while (i < token_count)
                {
                    if (OPCODE_INT_COMMA == tokens[i].opcode)
                    {
                        i++;
                    }
                    else if (num_int_variables >= MAX_VARIABLE)
                    {
                        log_error(0, "%s (line %d)", pxss_file_pointer, src_stream->line);
                        log_message(0, "Int variable overflow defining %s. Too many ints.\n", tokens[i].tag);
                        pxss_file_found_error = ktrue;
                        goto pxss_compilerize_fail;
                    }
                    else
                    {
                        sprintf(tempstring, "%c%02d", VAR_INT, num_int_variables);
                        define_add_string(tokens[i].tag, tempstring, SCOPE_FUNCTION + indent, kfalse);
                        int_variable[num_int_variables].scope = SCOPE_FUNCTION + indent;
                        num_int_variables++;
                        i++;
                    }

                }

                log_info(1, "");

                continue;
            }
            else if (0 == strcmp(tokens[0].tag, "FLOAT"))
            {
                i = 1;

                while (i < token_count)
                {
                    if (OPCODE_INT_COMMA == tokens[i].opcode)
                    {
                        i++;
                    }
                    else if (num_flt_variables >= MAX_VARIABLE)
                    {
                        log_error(0, "%s (line %d)", pxss_file_pointer, src_stream->line);
                        log_message(0, "Float variable overflow defining %s. Too many floats.\n", tokens[i].tag);
                        pxss_file_found_error = ktrue;
                        goto pxss_compilerize_fail;
                    }
                    else
                    {
                        sprintf(tempstring, "%c%02d", VAR_FLT, num_flt_variables);
                        define_add_string(tokens[i].tag, tempstring, SCOPE_FUNCTION + indent, kfalse);
                        flt_variable[num_flt_variables].scope = SCOPE_FUNCTION + indent;
                        num_flt_variables++;
                        i++;
                    }
                }

                log_info(1, "");

                continue;
            }
        }


        // Clear out the RPN int_data and fill in some of the helper arrays...
        pxss_tokenize_line(compile_stream, src_stream, filename);


        // Check for errors with if and else and while with nothing after...
        // !!!! must go AFTER pxss_tokenize_line() because we cannot know the values of
        // !!!! line_is_a_conditional and line_was_a_conditional until then
        if (line_was_a_conditional && indent <= last_indent)
        {
            log_error(0, "%s (line %d)", pxss_file_pointer, src_stream->line);
            log_message(0, "Conditional without a nest\n");
            pxss_file_found_error = ktrue;
            goto pxss_compilerize_fail;
        }


        // finish out any jumps
        if (last_indent > indent)
        {
            // Close out all jumps...
            pxss_close_jumps(compile_stream, indent, last_indent);
        }

        // Figure out the RPN order...
        rpn_stack_size = 0;
        pxss_rpn_find_order(src_stream, 0, token_count - 1);


        // Debug stuff
#ifdef VERBOSE_COMPILE
        repeat(i, rpn_stack_size)
        {
            log_info(1, "RPN:  %s", rpn_stack[i].ptok->tag);
        }
        log_info(1, "");
#endif


        // Figure out type casting based on RPN int_data...
        // If there is a syntax error, set a global compiler error and skip to the next line
        if (!pxss_rpn_figure_variable_types(src_stream))
            continue;


        // Write out the opcode int_data...
        pxss_emit_opcodes(compile_stream, src_stream, indent);
    }


    // Clean up any unresolved jumps...
    pxss_close_jumps(compile_stream, 0, PXSS_MAX_INDENT - 1);


    // Slap in a return 1, just to be on the safe side...
    pxss_add_return_opcode(compile_stream);

    // done with any defines from this file. Get rid of them.
    define_pop_to_scope(SCOPE_GLOBAL);

    return !pxss_file_found_error;

pxss_compilerize_fail:

    return kfalse;
}

//-----------------------------------------------------------------------------------------------
bool_t pxss_loadize(SDF_PSTREAM run_stream, const char* filename)
{
    // <ZZ> This function fills in the CallFunction addresses for a single file.  If it finds
    //      an error, the file is deleted...  This is the last stage of SRC compilation, sorta
    //      like a linker stage...  Returns kfalse if it had trouble, ktrue if not.

    char *   str;
    Uint8*   calladdress;
    Uint8*   addresslocation;
    Uint16   number_of_functions;
    Uint16   number_of_strings;
    OPCODE   opcode;
    Uint16   offset;
    int      i, j;

    // Don't create the RUN file there is an error
    pxss_file_found_error = kfalse;

    // save the file name for debugging
    pxss_file_pointer = filename;

    // Skip over the header run_stream->read for this RUN file...
    run_stream->read += MAX_FAST_FUNCTION << 1;  // Skip the fast function lookups...
    number_of_functions = ENDIAN_READ_STREAM_16( run_stream->read );
    number_of_strings   = ENDIAN_READ_STREAM_16( run_stream->read );
    repeat(i, number_of_functions)
    {
        // Check the name to see if it's one of the fast functions...
        repeat(j, MAX_FAST_FUNCTION)
        {
            if (0 == strcmp(run_stream->read + 2, pxss_fast_function_name[j]))
            {
                // It is a fast function, so fill in the look up table with it's entry point...
                Uint16 offset = endian_read_mem_int16(run_stream->read);
                endian_write_mem_int16(run_stream->read_beg + (j << 1), offset);
                break;
            }
        }

        // Go to the next entry
        run_stream->read += 2;                // Skip over the address
        run_stream->read += strlen(run_stream->read) + 1; // Skip the name and 0
        run_stream->read += strlen(run_stream->read) + 1; // Skip the return and any arguments and 0
    }

    repeat(i, number_of_strings)
    {
        // Check each string...

        offset = endian_read_mem_int16(run_stream->read);
        str    = run_stream->read + 2;

        if (0 == offset || UINT16_MAX == offset)
        {
            if ( 0 == strncmp(str, "FILE:", 5) )
            {
                log_warning(0, "%s - string \"%s\" at 0x%p", pxss_file_pointer, str, str);
            }
            else
            {
                // do nothing, the string is either corrupt or it is being handled another way
                log_message(1, "%s - string \"%s\" at 0x%p", pxss_file_pointer, str, str);
            }
        }
        else if ( 0 == strncmp(str, "FILE:", 5) )
        {
            // It's a weird "FILE:BLAH.BLA" type of string

            // rely on the externally defined function psxx_find_run_file_data()
            // to give us a pointer to the data we're looking for
            char  * tmp_str = str + 5;
            Uint8 * pdata   = pxss_find_data(tmp_str);

            if (NULL != pdata)
            {
                log_info(1, "Found file %s at 0x%x", tmp_str, pdata);
            }
            else
            {
                log_warning(0, "Writing a NULL value for file %s", tmp_str);
            }

            endian_write_mem_int32(run_stream->read_beg + offset, CAST(Uint32, pdata));
        }
        else
        {
            // It's a normal "BLAH BLAH BLAH" type of string
            // that is handled in the old way
            char * string = NULL;

            endian_write_mem_int32(run_stream->read_beg + offset, (Uint32) str);
            string = (char *)endian_read_mem_int32(run_stream->read_beg + offset);

            log_message(1, "%s - string \"%s\" at 0x%p", pxss_file_pointer, str, str);
        }


        // Go to the next entry
        run_stream->read += 2;                // Skip the address
        run_stream->read += strlen(run_stream->read) + 1; // Skip the name and 0
    }


    // Now we're at the start of the opcodes...   Go through every opcode, looking for
    // OPCODE_CALLFUNCTION, and stick in the correct address...
    log_info(1, "FUNC-OP:  %s", filename );

    while (run_stream->read < run_stream->read_end && !pxss_file_found_error)
    {
        // Read the opcode
        opcode = *run_stream->read;
        //log_info(1, "\t0x%06x:  0x%02x %s", run_stream->read-run_stream->read_beg, opcode, opcode_name[opcode] );
        run_stream->read++;

        switch (opcode)
        {
                //---- Skip over the extensions of the boring non-function opcodes ----

            case OPCODE_INT08:       run_stream->read += 1; break;                // 1 byte integer extension
            case OPCODE_JUMP:
            case OPCODE_INT_IF:
            case OPCODE_INT_WHILE:
            case OPCODE_IFFALSEJUMP: run_stream->read += 2; break;                // relative jump - unsigned 16-bit integer
            case OPCODE_INT16:       run_stream->read += 2; break;                // 2 byte integer extension
            case OPCODE_ZSTRING:     run_stream->read += 2; break;                // 2 byte offset of the string
            case OPCODE_INT32:       run_stream->read += 4; break;                // 4 byte integer extension
            case OPCODE_FLOAT:       run_stream->read += 4; break;                // 4 byte float   extension


            case OPCODE_DEBUG_LINE_NUMBER:  run_stream->read += 2; break;
            case OPCODE_DEBUG_LINE_POINTER: run_stream->read += 4; break;

            case  OPCODE_CALLFUNCTION:

                // Make sure there's still run_stream->read left to read, before reading the extension...
                if (run_stream->read >= run_stream->read_end - 19)
                {
                    log_error(0, "Ran out of run_stream->read while parsing %s.RUN", filename);
                    pxss_file_found_error = ktrue;
                    goto pxss_loadize_fail;
                }

                // Check for errors in the extension...
                addresslocation = run_stream->read;

                // Spit out debug info...
                log_info(1, "Function \"%s\" at 0x%04x", run_stream->read + 16, run_stream->read - run_stream->read_beg + 16);
                log_info(1, "Address    at: 0x%04x", run_stream->read - run_stream->read_beg +  0);
                log_info(1, "Return     at: 0x%04x", run_stream->read - run_stream->read_beg +  4);
                log_info(1, "Arguments  at: 0x%04x", run_stream->read - run_stream->read_beg +  8);
                log_info(1, "File Start at: 0x%04x", run_stream->read - run_stream->read_beg + 12);

                // Skip the call address, return address, argument address, and filestart address...
                // Data should now be at the function name
                run_stream->read += 16;


                // Fill in the call address...
                calladdress = pxss_mega_find_function(run_stream->read, filename, PXSS_REQUEST_OFFSET);
                endian_write_mem_int32(addresslocation, (Uint32) calladdress);


                // Error check...
                if (calladdress == NULL)
                {
                    log_info(1, "Couldn't find function %s", run_stream->read);
                    // Don't destroy file, because a null address should just be ignored...
                }

                // Fill in the argument address...   +1 to ignore return value...
                calladdress = pxss_mega_find_function(run_stream->read, filename, PXSS_REQUEST_ARGUMENTS) + 1;
                endian_write_mem_int32(addresslocation + 8, (Uint32) calladdress);

                // Fill in the filestart address...
                calladdress = pxss_mega_find_function(run_stream->read, filename, PXSS_REQUEST_FILESTART);
                endian_write_mem_int32(addresslocation + 12, (Uint32) calladdress);

                // Skip the function name and the trailing zero
                run_stream->read += strlen(run_stream->read) + 1;

                // Fill in the return address...
                endian_write_mem_int32(addresslocation + 4, (Uint32) run_stream->read);
                break;


            default:

                if (opcode >= OPCODE_PROP00 && opcode <= OPCODE_PROP1F)
                {
                    run_stream->read += 1;             // property - 1 byte extension
                };
        }

        i++;
    }

    return ktrue;

pxss_loadize_fail:


    return kfalse;
}



//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
void pxss_rpn_set_priority(int start, int i, int end, Sint8 any_type)
{
    // <ZZ> This function is a helper for the RPN code.  It sets the given token to be the
    //      next most important token.


    if (i >= start && i <= end)
    {
        if (!tokens[i].is_in_rpn && TOKEN_DELIMITER != tokens[i].type )
        {
            if ( (TOKEN_OPERAND == tokens[i].type) || any_type)
            {
                rpn_stack[rpn_stack_size].ptok         = tokens + i;
                rpn_stack[rpn_stack_size].is_destroyed = kfalse;

                tokens[i].is_in_rpn = ktrue;
                rpn_stack_size++;
            }
        }
    }
}


//-----------------------------------------------------------------------------------------------
bool_t pxss_rpn_simulate_stack(SDF_PSTREAM pstream)
{
    int i, j, k;
    TOKEN *ptoken, *other_ptoken, *temp_ptoken;
    int  stack_depth = 0;
    TOKEN * stack_vars[2*MAX_STACK];

    repeat(i, rpn_stack_size)
    {
        ptoken = rpn_stack[i].ptok;

        if (ptoken->number_to_destroy <= 0)
        {
            stack_vars[stack_depth++] = ptoken;
        }
        else if (OPCODE_ASSIGN == ptoken->opcode)
        {
            // special in-fix operator

            VAR_TYPE temp_type;

            // pop the right-hand-side token
            if (stack_depth <= 0)
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Missing value to right of \'=\'");
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            temp_ptoken  = stack_vars[--stack_depth];

            // get left-hand-side token
            i++;

            if (i >= rpn_stack_size)
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Missing value to left of \'=\'");
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            other_ptoken = rpn_stack[i].ptok;

            // make sure the left-hand-side token is left-valued token
            if (!token_is_valid_lvalue(other_ptoken))
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Must have a l-valued token on the left-hand side of \'=\'");
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            temp_type = (VAR_TYPE)temp_ptoken->variable_type;

            if (VAR_UNKNOWN != temp_ptoken->change_type)
            {
                temp_type = temp_ptoken->change_type;
            }

            ptoken->variable_type = temp_type;
            ptoken->change_type   = other_ptoken->variable_type;

            // push the return value
            stack_vars[stack_depth++] = ptoken;
        }
        else if (ptoken->number_to_destroy == 1 && (OPCODE_INCREMENT == ptoken->opcode || OPCODE_DECREMENT == ptoken->opcode))
        {
            // special unary operators

            VAR_TYPE other_type;

            // get left-hand-side token
            i++;

            if (i >= rpn_stack_size)
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Missing value to left of \'%s\'", ptoken->tag);
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            other_ptoken = rpn_stack[i].ptok;


            // make sure the left-hand-side token is left-valued token
            if (!token_is_valid_rvalue(other_ptoken))
            {

                // Variable has been used before being set...
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Variable %s used before being set", opcode_name[other_ptoken->opcode]);
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            // make sure the left-hand-side token is left-valued token
            if (!token_is_valid_lvalue(other_ptoken))
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Must have a l-valued token on the left-hand side of \'%s\'", ptoken->tag);
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            if (VAR_UNKNOWN == ptoken->variable_type)
            {
                other_type = (VAR_TYPE)other_ptoken->variable_type;

                if (VAR_UNKNOWN != other_ptoken->change_type)
                {
                    other_type = other_ptoken->change_type;
                }

                ptoken->variable_type = other_type;
            }

            // coerce the variable
            other_ptoken->change_type = ptoken->variable_type;

            // push the return value
            stack_vars[stack_depth++] = ptoken;
        }
        else if (ptoken->number_to_destroy == 1 && TOKEN_OPERATOR == ptoken->type)
        {
            // unary operators, logical not, negate, ...

            // get right-hand-side token
            if (stack_depth <= 0)
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Missing value to right of \'%s\'", ptoken->tag);
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            other_ptoken = stack_vars[--stack_depth];

            if (!token_is_valid_rvalue(other_ptoken))
            {
                // Variable has been used before being set...
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Variable %s used before being set", opcode_name[other_ptoken->opcode]);
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            if (VAR_UNKNOWN == ptoken->variable_type)
            {
                // get the variable type from a compromise type of the two operands
                VAR_TYPE other_type;

                other_type = (VAR_TYPE)other_ptoken->variable_type;

                if (VAR_UNKNOWN != other_ptoken->change_type)
                {
                    other_type = other_ptoken->change_type;
                }

                ptoken->variable_type = other_type;
            }

            // coerce the operand types to the compromise type
            other_ptoken->change_type  = ptoken->variable_type;

            // push the return value
            stack_vars[stack_depth++] = ptoken;
        }
        else if (ptoken->number_to_destroy == 2 && TOKEN_OPERATOR == ptoken->type)
        {
            // non-special in-fix binary operators

            // get right-hand-side token
            if (stack_depth <= 0)
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Missing value to right of \'%s\'", ptoken->tag);
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            other_ptoken = stack_vars[--stack_depth];

            if (!token_is_valid_rvalue(other_ptoken))
            {
                // Variable has been used before being set...
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Variable %s used before being set", opcode_name[other_ptoken->opcode]);
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            // get left-hand-side token
            if (stack_depth <= 0)
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Missing value to left of \'%s\'", ptoken->tag);
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            temp_ptoken = stack_vars[--stack_depth];

            if (!token_is_valid_rvalue(temp_ptoken))
            {

                // Variable has been used before being set...
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Variable %s used before being set", opcode_name[temp_ptoken->opcode]);
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            if (VAR_UNKNOWN == ptoken->variable_type)
            {
                // get the variable type from a compromise type of the two operands
                VAR_TYPE other_type, temp_type, compromise_type;

                other_type = (VAR_TYPE)other_ptoken->variable_type;

                if (VAR_UNKNOWN != other_ptoken->change_type)
                {
                    other_type = other_ptoken->change_type;
                }

                temp_type = (VAR_TYPE)temp_ptoken->variable_type;

                if (VAR_UNKNOWN != temp_ptoken->change_type)
                {
                    temp_type = temp_ptoken->change_type;
                }

                compromise_type = VAR_INT;

                if (VAR_FLT == other_type || VAR_FLT == temp_type)
                {
                    compromise_type = VAR_FLT;
                }

                ptoken->variable_type = compromise_type;
            }

            // coerce the operand types to the compromise type
            other_ptoken->change_type = ptoken->variable_type;
            temp_ptoken->change_type  = ptoken->variable_type;

            // push the return value
            stack_vars[stack_depth++] = ptoken;
        }
        else if (TOKEN_FUNCTION == ptoken->type)
        {
            // an ordinary function (system-defined or user-defined)

            // make sure there are enough tokens on the stack
            if (ptoken->number_to_destroy > stack_depth)
            {
                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
                log_message(0, "Missing arguments to function \'%s\'", ptoken->tag);
                log_message(0, "\"%s\"", pxss_line_pointer);
                _pxss_dump_tokens();
                log_message(0, "");

                pxss_file_found_error = ktrue;
                return kfalse;
            }

            k = stack_depth - ptoken->number_to_destroy;
            repeat(j, ptoken->number_to_destroy)
            {
                if (VAR_UNKNOWN == stack_vars[k+j]->variable_type)
                {
                    stack_vars[k+j]->variable_type = ptoken->arg_list[j];
                }
                else
                {
                    stack_vars[k+j]->change_type = CHAR_TO_VAR(ptoken->arg_list[j]);
                }
            }

            // pop the stack
            stack_depth -= ptoken->number_to_destroy;

            // push the return value
            stack_vars[stack_depth++] = ptoken;
        }
        else
        {
            assert(kfalse);
            return kfalse;
        }
    }

    return ktrue;
}

//-----------------------------------------------------------------------------------------------
bool_t pxss_rpn_figure_variable_types(SDF_PSTREAM pstream)
{
    // <ZZ> This function determines where to place the ToInt and ToFloat opcodes for a line.
    //      Token_count is the number of RPN symbols found previously, which doesn't include
    //      tokens like '(' and ','.

    int i;
    TOKEN *ptoken;

    if ( !pxss_rpn_simulate_stack(pstream) ) return kfalse;

    // finish assigning the types
    repeat(i, rpn_stack_size)
    {
        ptoken = rpn_stack[i].ptok;

        if (VAR_UNKNOWN == ptoken->variable_type)
        {
            ptoken->variable_type = ptoken->change_type;
        }

        assert(VAR_UNKNOWN != ptoken->variable_type);
    };


    // optimize some numerical operators
    repeat(i, rpn_stack_size)
    {
        ptoken = rpn_stack[i].ptok;

        if (VAR_FLT == ptoken->variable_type && ptoken->opcode >= OPCODE_ASSIGN && ptoken->opcode <= OPCODE_NEGATE)
        {
            ptoken->opcode += OPCODE_F_ASSIGN - OPCODE_ASSIGN;
        }
        else if (VAR_INT == ptoken->variable_type && ptoken->opcode >= OPCODE_F_ASSIGN && ptoken->opcode <= OPCODE_F_NEGATE)
        {
            ptoken->opcode -= OPCODE_F_ASSIGN - OPCODE_ASSIGN;
        };
    }

    return ktrue;



    //last_return_type = VAR_UNKNOWN;
    //repeat(i, rpn_stack_size)
    //{
    //    // Look for tokens in order of their priority...
    //    rpn_stack[i].is_destroyed = kfalse;

    //    ptoken = rpn_stack[i].ptok;

    //    // Look through previous tokens to find arguments for functions/operations
    //    type_needed = VAR_INT;
    //    if (ptoken->number_to_destroy > 0)
    //    {
    //        // Look for errors with equals, increment, and decrement
    //        if (ptoken->variable_type == VAR_UNKNOWN)
    //        {
    //            if (ptoken->opcode == OPCODE_ASSIGN || ptoken->opcode == OPCODE_INCREMENT || ptoken->opcode == OPCODE_DECREMENT)
    //            {
    //                // Need to know what type of variable is being set...  Check it...
    //                if (i < rpn_stack_size)
    //                {
    //                    // Make sure the ptoken to the left is a valid data storage ptoken...
    //                    other_ptoken = rpn_stack[i+1].ptok;
    //                    if (other_ptoken->opcode >= OPCODE_IREG00 && other_ptoken->opcode < OPCODE_FREG1F )
    //                    {
    //                        if (ptoken->opcode == OPCODE_ASSIGN)
    //                        {
    //                            // Check to the right for equals, now that we've checked for errors...
    //                            if (i > 0)
    //                            {
    //                                other_ptoken = rpn_stack[i-1].ptok;
    //                                type_needed = other_ptoken->variable_type;
    //                            }
    //                            else
    //                            {
    //                                log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
    //                                log_message(0, "Missing value to right of \'=\'");
    //                                log_message(0, "\"%s\"", pxss_line_pointer);
    //                                _pxss_dump_tokens();
    //                                log_message(0, "");

    //                                pxss_file_found_error = ktrue;
    //                            }
    //                        }
    //                        else
    //                        {
    //                            type_needed = other_ptoken->variable_type;
    //                        }
    //                    }
    //                    else
    //                    {
    //                        log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
    //                        log_message(0, "Invalid storage variable to left of \'=\', \'++\', or \'--\'");
    //                        log_message(0, "\"%s\"", pxss_line_pointer);
    //                        _pxss_dump_tokens();
    //                        log_message(0, "");

    //                        pxss_file_found_error = ktrue;
    //                    }
    //                }
    //            }
    //        }

    //        // Look for any floating point arguments...  This is needed for basic functions
    //        // and operations that can handle either float or integer...  If an Add has a float
    //        // and an int, the int should be changed to float and a FloatAdd performed...
    //        if (ptoken->arg_list == NULL)
    //        {
    //            destroyed = 0;
    //            j = i;
    //            while (j > 0 && destroyed < ptoken->number_to_destroy)
    //            {
    //                // Go to the last ptoken and see if we can use it...
    //                j--;
    //                other_ptoken = rpn_stack[j].ptok;
    //                if (!rpn_stack[j].is_destroyed)
    //                {
    //                    // Okay, we can use it...  Check if it's a float...
    //                    if (other_ptoken->variable_type == VAR_FLT) type_needed = VAR_FLT;
    //                    destroyed++;
    //                }
    //            }


    //            // Now change the opcodes from IntAdd to FloatAdd...  Others too...
    //            if (type_needed == VAR_FLT)
    //            {
    //                if (ptoken->opcode >= OPCODE_ASSIGN && ptoken->opcode <= OPCODE_NEGATE)
    //                {
    //                    ptoken->opcode += (OPCODE_F_ASSIGN - OPCODE_ASSIGN);  // Float operations start with opcode 32...
    //                }
    //            }
    //        }

    //        // Fill in the correct variable type...
    //        if (ptoken->variable_type == VAR_UNKNOWN)
    //        {
    //            ptoken->variable_type = type_needed;
    //            // Some floating point functions return integers...
    //            if (ptoken->opcode >= OPCODE_F_ISEQUAL && ptoken->opcode <= OPCODE_F_LOGICALNOT)
    //            {
    //                ptoken->variable_type = VAR_INT;
    //            }
    //        }




    //        // Now type cast any of those arguments we found...  Do the search again...
    //        destroyed = 0;
    //        j = i;
    //        while (j > 0 && destroyed < ptoken->number_to_destroy)
    //        {
    //            // Go to the last ptoken and see if we can use it...
    //            j--;

    //            other_ptoken = rpn_stack[j].ptok;
    //            if (!rpn_stack[j].is_destroyed)
    //            {
    //                // Remember that we ate it up, so other functions don't try to use it too...
    //                rpn_stack[j].is_destroyed = ktrue;
    //                destroyed++;

    //                // Okay, we can use it...  Do we need to change all ints to float?
    //                if (ptoken->arg_list != NULL)
    //                {
    //                    // Looks like a function with a set argument list, so just cast any
    //                    // argument we need to
    //                    type_needed = CHAR_TO_VAR(*(ptoken->arg_list + ptoken->number_to_destroy - destroyed));
    //                }
    //                if (other_ptoken->variable_type != type_needed)
    //                {
    //                    // Don't insert ToInt or ToFloat for equates...
    //                    if (ptoken->opcode != OPCODE_ASSIGN && ptoken->opcode != OPCODE_F_ASSIGN)
    //                    {
    //                        other_ptoken->change_type = type_needed;
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    last_return_type = ptoken->variable_type;
    //}



    //// Make sure we had the correct number of arguments
    //total_destroyed = 0;
    //repeat(i, rpn_stack_size)
    //{
    //    ptoken = rpn_stack[i].ptok;

    //    if (ptoken->opcode == OPCODE_ASSIGN || ptoken->opcode == OPCODE_F_ASSIGN)
    //    {
    //        ptoken->number_to_destroy++;
    //    }

    //    if (ptoken->number_to_destroy >= 0)
    //    {
    //        ptoken->number_to_destroy--;
    //    };

    //    total_destroyed += ptoken->number_to_destroy;
    //    log_info(1, "RPN:  %s  DES:  %d", ptoken->tag, ptoken->number_to_destroy);
    //}
    //log_info(1, "");

    //if (total_destroyed != -1)
    //{
    //    log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
    //    log_message(0, "Wrong number of arguments (%d to %d)", total_destroyed, -1);
    //    log_message(0, "\"%s\"", pxss_line_pointer);
    //    _pxss_dump_tokens();
    //    log_message(0, "");

    //    pxss_file_found_error = ktrue;
    //}


    // Debug stuff...
#ifdef VERBOSE_COMPILE
    repeat(i, rpn_stack_size)
    {
        log_info(1, "TYP:  %c  CHG:  %c  RPN:  %s", rpn_stack[i].ptok->variable_type, rpn_stack[i].ptok->change_type, rpn_stack[i].ptok->tag);
    }
    log_info(1, "");
#endif
}

//-----------------------------------------------------------------------------------------------
// handle true operators

//operator precedence as given in c++
// Operator            - Name or Meaning                  - Associativity
//-----------------------------------------------------------------------
//   ::                - Scope resolution                 - Left to right
//   .                 - Member selection (object)        - Left to right
//   �>                - Member selection (pointer)       - Left to right
//   [ ]               - Array subscript                  - Left to right
//   ( )               - Function call                    - Left to right
//   ( )               - member initialization            - Left to right
//   ++                - Postfix increment                - Left to right
//   ��                - Postfix decrement                - Left to right
//   typeid( )         - type name                        - Left to right
//   const_cast        - Type cast (conversion)           - Left to right
//   dynamic_cast      - Type cast (conversion)           - Left to right
//   reinterpret_cast  - Type cast (conversion)           - Left to right
//   static_cast       - Type cast (conversion)           - Right to left
//   sizeof            - Size of object or type           - Right to left
//   ++                - Prefix increment                 - Right to left
//   ��                - Prefix decrement                 - Right to left
//   ~                 - One's complement                 - Right to left
//   !                 - Logical not                      - Right to left
//   �                 - Unary minus                      - Right to left
//   +                 - Unary plus                       - Right to left
//   &                 - Address-of                       - Right to left
//   *                 - Indirection                      - Right to left
//   new               - Create object                    - Right to left
//   delete            - Destroy object                   - Right to left
//   ()                - Cast                             - Left to right
//   .*                - Pointer-to-member (objects)      - Left to right
//   �>*               - Pointer-to-member (pointers)     - Left to right
//   *                 - Multiplication                   - Left to right
//   /                 - Division                         - Left to right
//   %                 - Modulus                          - Left to right
//   +                 - Addition                         - Left to right
//   �                 - Subtraction                      - Left to right
//   <<                - Left shift                       - Left to right
//   >>                - Right shift                      - Left to right
//   <                 - Less than                        - Left to right
//   >                 - Greater than                     - Left to right
//   <=                - Less than or equal�to            - Left to right
//   >=                - Greater than or equal to         - Left to right
//   ==                - Equality                         - Left to right
//   !=                - Inequality                       - Left to right
//   &                 - Bitwise AND                      - Left to right
//   ^                 - Bitwise exclusive OR             - Left to right
//   |                 - Bitwise inclusive OR             - Left to right
//   &&                - Logical AND                      - Left to right
//   ||                - Logical OR                       - Right to left
//   e1?e2:e3          - Conditional                      - Right to left
//   =                 - Assignment                       - Right to left
//   *=                - Multiplication assignment        - Right to left
//   /=                - Division assignment              - Right to left
//   %=                - Modulus assignment               - Right to left
//   +=                - Addition assignment              - Right to left
//   �=                - Subtraction assignment           - Right to left
//   <<=               - Left-shift assignment            - Right to left
//   >>=               - Right-shift assignment           - Right to left
//   &=                - Bitwise AND assignment           - Right to left
//   |=                - Bitwise inclusive OR assignment  - Right to left
//   ^=                - Bitwise exclusive OR assignment  - Right to left
//   throw expr        - throw expression                 - Left to right
//   ,                 - Comma                            - Left to right

//operator precedence for SoulFu scripting
//    Operator            - Name or Meaning                  - Associativity
//-----------------------------------------------------------------------
// 28                       - constants
// 27                       - functions
// 26     .                 - Member selection (object)        - Left to right
// 25     ++                - Postfix increment                - Left to right
// 24     ��                - Postfix decrement                - Left to right
// 23     ~                 - One's complement                 - Right to left
// 22     !                 - Logical not                      - Right to left
// 21     �                 - Unary minus                      - Right to left
// 20     +                 - Unary plus                       - Right to left
// 19     *                 - Multiplication                   - Left to right
// 18     /                 - Division                         - Left to right
// 17     %                 - Modulus                          - Left to right
// 16     +                 - Addition                         - Left to right
// 15     �                 - Subtraction                      - Left to right
// 14     <<                - Left shift                       - Left to right
// 13     >>                - Right shift                      - Left to right
// 12     <                 - Less than                        - Left to right
// 11     >                 - Greater than                     - Left to right
// 10     <=                - Less than or equal�to            - Left to right
//  9     >=                - Greater than or equal to         - Left to right
//  8     ==                - Equality                         - Left to right
//  7     !=                - Inequality                       - Left to right
//  6     &                 - Bitwise AND                      - Left to right
//  5     ^                 - Bitwise exclusive OR             - Left to right
//  4     |                 - Bitwise inclusive OR             - Left to right
//  3     &&                - Logical AND                      - Left to right
//  2     ||                - Logical OR                       - Right to left
//  1     =                 - Assignment                       - Right to left
//  0     ,                 - Comma                            - Left to right
// -1     ( and ) delimiters

static int _pxss_get_token_priority(TOKEN * ptok)
{
    int priority = -1;

    if (NULL == ptok) return priority;

    if (TOKEN_OPERAND == ptok->type)
    {
        priority = 27;
    }
    else if (TOKEN_FUNCTION == ptok->type)
    {
        priority = 26;
    }
    else if (OPCODE_INCREMENT == ptok->opcode)
    {
        priority = 9;
    }
    else if (OPCODE_DECREMENT == ptok->opcode)
    {
        priority = 9;
    }
    else if (OPCODE_BITWISEINVERT == ptok->opcode)
    {
        priority = 8;
    }
    else if (OPCODE_LOGICALNOT == ptok->opcode)
    {
        priority = 8;
    }
    else if (OPCODE_NEGATE == ptok->opcode || OPCODE_F_NEGATE == ptok->opcode)
    {
        priority = 8;
    }
    else if (OPCODE_MULTIPLY == ptok->opcode || OPCODE_F_MULTIPLY == ptok->opcode)
    {
        priority = 6;
    }
    else if (OPCODE_DIVIDE == ptok->opcode || OPCODE_F_DIVIDE == ptok->opcode)
    {
        priority = 6;
    }
    else if (OPCODE_MODULUS == ptok->opcode)
    {
        priority = 6;
    }
    else if (OPCODE_ADD == ptok->opcode || OPCODE_F_ADD == ptok->opcode)
    {
        priority = 5;
    }
    else if (OPCODE_SUBTRACT == ptok->opcode || OPCODE_F_SUBTRACT == ptok->opcode)
    {
        priority = 5;
    }
    else if (OPCODE_BITWISELEFT == ptok->opcode)
    {
        priority = 4;
    }
    else if (OPCODE_BITWISERIGHT == ptok->opcode)
    {
        priority = 4;
    }
    else if (OPCODE_BITWISEAND == ptok->opcode)
    {
        priority = 4;
    }
    else if (OPCODE_BITWISEXOR == ptok->opcode)
    {
        priority = 4;
    }
    else if (OPCODE_BITWISEOR == ptok->opcode)
    {
        priority = 4;
    }
    else if (OPCODE_ISLESSER == ptok->opcode)
    {
        priority = 3;
    }
    else if (OPCODE_ISGREATER == ptok->opcode)
    {
        priority = 3;
    }
    else if (OPCODE_ISLESSEREQUAL == ptok->opcode)
    {
        priority = 3;
    }
    else if (OPCODE_ISGREATEREQUAL == ptok->opcode)
    {
        priority = 3;
    }
    else if (OPCODE_ISEQUAL == ptok->opcode)
    {
        priority = 3;
    }
    else if (OPCODE_ISNOTEQUAL == ptok->opcode)
    {
        priority = 3;
    }
    else if (OPCODE_LOGICALAND == ptok->opcode)
    {
        priority = 2;
    }
    else if (OPCODE_LOGICALOR == ptok->opcode)
    {
        priority = 2;
    }
    else if (TOKEN_DELIMITER == ptok->type)
    {
        priority = 1;
    }
    else if (OPCODE_ASSIGN == ptok->opcode || OPCODE_F_ASSIGN == ptok->opcode)
    {
        priority = 0;
    }


    return priority;
}

//-----------------------------------------------------------------------------------------------
static int _token_scan(int begin, int end, int our_nest_level, int our_priority)
{
    int i;
    int priority;
    int lowest_token;
    int nest_level;
    bool_t found = kfalse;


    found = kfalse;
    lowest_token = -1;
    nest_level = 0;
    i = begin;

    while (i <= end)
    {
        // Check for nest level changes...
        if (OPCODE_INT_LPAREN == tokens[i].opcode)
        {
            nest_level++;
        }
        else if (OPCODE_INT_RPAREN == tokens[i].opcode)
        {
            nest_level--;
        }
        else if (nest_level == our_nest_level)
        {
            // Only handle tokens at our_nest_level...

            priority = _pxss_get_token_priority(tokens + i);

            // Now remember the priority...
            if (priority == our_priority) return i;
        }

        i++;
    }

    return -1;
}

//-----------------------------------------------------------------------------------------------
static void _pxss_rpn_find_order_rec(SDF_PSTREAM pstream, int start, int end)
{
    // <ZZ> This function looks at the tokens in token_buffer to determine the reverse polish
    //      ordering for the tokens.  Start is the first token to be examined, end is the last.
    //      If the expression is complex (as it usually is), it is cut in half at the lowest
    //      priority operator, and each half is fed back into the function recursively.  If the
    //      expression is simple enough to figure out, the priorities are assigned.

    int i;
    int priority, priority_count;
    int lowest_token;
    int lowest_priority = 0;
    int highest_priority = 0;
    int nest_level;
    int our_nest_level;
    int delim_begin, delim_end;   // delimiters like parentheses or commas
    bool_t found = kfalse;

    // Just in case something weird happens...
    if (end < start || start < 0 || end < 0 || start >= PXSS_MAX_TOKEN || end >= PXSS_MAX_TOKEN)
    {
        return;
    }

    // check for the special case of one token
    if ( end == start )
    {
        pxss_rpn_set_priority(start, start, start, ktrue);
        return;
    };

    // split commas
    i = start;

    nest_level = 0;

    delim_begin = start;

    delim_end   = -1;

    while (i <= end)
    {
        // Check for nest level changes...
        if (OPCODE_INT_LPAREN == tokens[i].opcode)
        {
            nest_level++;
        }
        else if (OPCODE_INT_RPAREN == tokens[i].opcode)
        {
            nest_level--;
        }
        else if (0 == nest_level)
        {
            if (-1 == delim_end && OPCODE_INT_COMMA == tokens[i].opcode)
            {
                delim_end = i;
            }
        }

        i++;
    }

    if (-1 != delim_begin && -1 != delim_end)
    {
        // attack things to the the left of the comma, first
        _pxss_rpn_find_order_rec(pstream, delim_begin, delim_end - 1);

        // then to the right of the comma
        _pxss_rpn_find_order_rec(pstream, delim_end + 1, end);

        return;
    }

    // split parentheses
    i = start;
    nest_level = 0;
    delim_begin = delim_end = -1;

    if (OPCODE_INT_LPAREN == tokens[i].opcode)
    {
        delim_begin = start;
        nest_level++;
        i++;

        while (i <= end)
        {
            // Check for nest level changes...
            if (OPCODE_INT_LPAREN == tokens[i].opcode)
            {
                nest_level++;
            }
            else if (OPCODE_INT_RPAREN == tokens[i].opcode)
            {
                nest_level--;

                if (-1 == delim_end && -1 != delim_begin && 0 == nest_level)
                {
                    delim_end = i;
                }
            }

            i++;
        }

        if (-1 != delim_end)
        {
            // attack things in parentheses first
            _pxss_rpn_find_order_rec(pstream, delim_begin + 1, delim_end - 1);

            // then to the left of parentheses
            _pxss_rpn_find_order_rec(pstream, start, delim_begin - 1);

            // then to the right of parentheses
            _pxss_rpn_find_order_rec(pstream, delim_end + 1, end);

            return;
        }
        else
        {
            assert(kfalse);
        }
    }


    log_info(1, "Checking priority from %d to %d", start, end);


    // Find the lowest nest level...
    our_nest_level = 0;
    nest_level = 0;
    i = start;

    while (i <= end)
    {
        // Check for nest level changes...
        if (OPCODE_INT_LPAREN == tokens[i].opcode)
        {
            nest_level++;
        }
        else if (OPCODE_INT_RPAREN == tokens[i].opcode)
        {
            nest_level--;
        }
        else
        {
            // Find the first useable token...
            our_nest_level = nest_level;
            break;
        }

        i++;
    }

    while (i <= end)
    {
        // Check for nest level changes...
        if (OPCODE_INT_LPAREN == tokens[i].opcode)
        {
            nest_level++;
        }
        else if (OPCODE_INT_RPAREN == tokens[i].opcode)
        {
            nest_level--;
        }
        else
        {
            // Only take nest levels that actually have tokens...
            if (nest_level < our_nest_level)  our_nest_level = nest_level;
        }

        i++;
    }


    // Search for the lowest and highest priority tokens
    found = kfalse;
    lowest_token = -1;
    nest_level = 0;
    i = start;
    priority_count = 0;

    while (i <= end)
    {
        // Check for nest level changes...
        if (OPCODE_INT_LPAREN == tokens[i].opcode)
        {
            nest_level++;
        }
        else if (OPCODE_INT_RPAREN == tokens[i].opcode)
        {
            nest_level--;
        }
        else if (nest_level == our_nest_level)
        {
            // Only handle tokens at our_nest_level...

            priority = _pxss_get_token_priority(tokens + i);

            // Now remember the priority...
            if (priority > -1)
            {
                if (!found)
                {
                    found            = ktrue;

                    highest_priority = priority;

                    lowest_token     = i;
                    lowest_priority  = priority;

                    priority_count   = (priority < 26) ? 1 : 0;
                }
                else if (priority > highest_priority)
                {
                    highest_priority = priority;
                }
                else if (priority < lowest_priority)
                {
                    lowest_token    = i;
                    lowest_priority = priority;
                    priority_count  = (priority < 26) ? 1 : 0;
                }
                else if (priority < 26 && priority == lowest_priority)
                {
                    priority_count++;
                }
            }
        }

        i++;
    }

    if (priority_count > 1)
    {
        int tmp_tok, tmp_tok_last, tmp_end, tmp_stt;

        tmp_stt = start;
        tmp_tok = _token_scan(start, end, our_nest_level, lowest_priority);
        tmp_end = tmp_tok - 1;

        // grab the left hand side of the operator
        _pxss_rpn_find_order_rec(pstream, tmp_stt, tmp_end);

        for (i = 1; i < priority_count; i++)
        {
            tmp_tok_last = tmp_tok;
            tmp_stt     = tmp_tok + 1;
            tmp_tok     = _token_scan(tmp_stt, end, our_nest_level, lowest_priority);
            tmp_end     = tmp_tok - 1;

            // grab the next right hand side of the operator
            _pxss_rpn_find_order_rec(pstream, tmp_stt, tmp_end);

            // Middle
            pxss_rpn_set_priority(start, tmp_tok_last, end, ktrue);
        };

        tmp_tok_last = tmp_tok;

        tmp_stt     = tmp_tok + 1;

        tmp_end     = end;

        // grab the next right hand side of the operator
        _pxss_rpn_find_order_rec(pstream, tmp_stt, tmp_end);

        // Middle
        pxss_rpn_set_priority(start, tmp_tok_last, end, ktrue);

    }
    else
    {
        if (TOKEN_OPERAND == tokens[lowest_token].type)
        {
            // Left side
            assert(start > lowest_token - 1);

            // Middle
            pxss_rpn_set_priority(start, lowest_token, end, ktrue);

            // Right side
            _pxss_rpn_find_order_rec(pstream, lowest_token + 1, end);
        }
        else if (TOKEN_FUNCTION == tokens[lowest_token].type)
        {
            // Left side
            assert(start > lowest_token - 1);

            // Right side
            _pxss_rpn_find_order_rec(pstream, lowest_token + 1, end);

            // Middle
            pxss_rpn_set_priority(start, lowest_token, end, ktrue);
        }
        else if (TOKEN_OPERATOR == tokens[lowest_token].type)
        {
            // Too complex, so split it in two and try again...
            if (FIX_PREFIX == (tokens[lowest_token].fix_bits & FIX_PREFIX))
            {
                // Left side should not really exist
                assert(start > lowest_token - 1);

                // Right side
                _pxss_rpn_find_order_rec(pstream, lowest_token + 1, end);

                // Handle operator
                pxss_rpn_set_priority(start, lowest_token, end, ktrue);
            }
            else if (FIX_POSTFIX == (tokens[lowest_token].fix_bits & FIX_PREFIX))
            {
                // Handle operand(s) to the left
                _pxss_rpn_find_order_rec(pstream, start, lowest_token - 1);

                // Handle self
                pxss_rpn_set_priority(start, lowest_token, end, ktrue);

                // Handle operand(s) to the right
                _pxss_rpn_find_order_rec(pstream, lowest_token + 1, end);
            }
            else if ((FIX_INFIX | FIX_ASSIGN) == (tokens[lowest_token].fix_bits & (FIX_INFIX | FIX_ASSIGN)))
            {
                // this should be OPCODE_ASSIGN and similar opcodes

                // Right side
                _pxss_rpn_find_order_rec(pstream, lowest_token + 1, end);

                // Middle
                pxss_rpn_set_priority(start, lowest_token, end, ktrue);

                // Left side
                _pxss_rpn_find_order_rec(pstream, start, lowest_token - 1);
            }
            else if ((FIX_POSTFIX | FIX_ASSIGN) == (tokens[lowest_token].fix_bits & (FIX_POSTFIX | FIX_ASSIGN)))
            {
                // this should be '++' and '--' and similar postfix assign operators

                // Handle self
                pxss_rpn_set_priority(start, lowest_token, end, ktrue);

                // Handle operand(s) to the left
                _pxss_rpn_find_order_rec(pstream, start, lowest_token - 1);

                // Handle operand(s) to the right
                _pxss_rpn_find_order_rec(pstream, lowest_token + 1, end);
            }
            else if (FIX_INFIX == (tokens[lowest_token].fix_bits & FIX_INFIX))
            {
                // Left side
                _pxss_rpn_find_order_rec(pstream, start, lowest_token - 1);

                // Right side
                _pxss_rpn_find_order_rec(pstream, lowest_token + 1, end);

                // Middle
                pxss_rpn_set_priority(start, lowest_token, end, ktrue);
            }
            else
            {
                assert(kfalse);
            }
        }
        else
        {
            assert(kfalse);
        };
    }

    return;
}



//-----------------------------------------------------------------------------------------------
void pxss_rpn_find_order(SDF_PSTREAM pstream, int start, int end)
{
    // <ZZ> This function looks at the tokens in token_buffer to determine the reverse polish
    //      ordering for the tokens.  Start is the first token to be examined, end is the last.
    //      If the expression is complex (as it usually is), it is cut in half at the lowest
    //      priority operator, and each half is fed back into the function recursively.  If the
    //      expression is simple enough to figure out, the priorities are assigned.

    int i;
    int nest_level;


    // Just in case something weird happens...
    if (end < start || start < 0 || end < 0 || start >= PXSS_MAX_TOKEN || end >= PXSS_MAX_TOKEN)
    {
        return;
    }

    nest_level = 0;
    i = start;

    while (i <= end)
    {
        if (OPCODE_INT_LPAREN == tokens[i].opcode)
        {
            nest_level++;
        }
        else if (OPCODE_INT_RPAREN == tokens[i].opcode)
        {
            nest_level--;
        }

        i++;
    }

    // Check for errors...
    if (nest_level != 0)
    {
        log_error(0, "%s (line %d)", pxss_file_pointer, pstream->line);
        log_message(0, "Unbalanced parentheses\n");
        pxss_file_found_error = ktrue;
        return;
    }

    _pxss_rpn_find_order_rec(pstream, start, end);
}

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
void property_reset_all(void)
{
    // <ZZ> This function clears out our properties
    property_count = 0;
}

//-----------------------------------------------------------------------------------------------
void property_add(const char* tag, const char type, const char* offset)
{
    // <ZZ> This function registers a new property index...  The x in window.x for example.
    //      Tag is the name of the property, type is F or I or something, offset is the
    //      location of the data for this property relative to the start of the object's
    //      data (and is stored in ASCII text... "202" not the number 202).

    int i, index;

    // !!!!watch out for duplicate tags!!!!
    index = property_find(tag);

    if (index < 0)
    {
        if (property_count >= MAX_PROPERTY) return;

        index = property_count;
        property_count++ ;
    };

    // Copy the name
    strncpy(properties[index].tag, tag, 8);
    properties[index].tag[7] = 0;

    // Copy the type
    properties[index].type = CHAR_TO_VAR(type);

    // Copy the offset
    sscanf(offset, "%d", &i);
    properties[index].offset = i;

    log_info(1, "Added property %s as number %d...  Type %c, Offset %d", properties[index].tag, index, properties[index].type, properties[index].offset);
}

//-----------------------------------------------------------------------------------------------
int property_find(const char* tag)
{
    // <ZZ> This function returns the index of the first property that matches tag.  If there
    //      aren't any matches, it returns -1.
    int i;

    // Check each property...
    repeat(i, property_count)
    {
        if (0 == strcmp(tag, properties[i].tag)) return i;
    }
    return -1;
}

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
int define_find_scope(char* token, int min_scope)
{
    // <BB> This finction finds a defined value whose scope is greater than or equal to min_scope

    int i;
    int max_scope = -1;
    int max_index = -1;

    // Check each define...
    for (i = define_count - 1; i >= 0; i--)
    {
        if (defines[i].scope > min_scope && defines[i].scope > max_scope)
        {
            if (0 == strcmp(token, defines[i].tag))
            {

                max_scope = defines[i].scope;
                max_index = i;
            }
        }
    }

    return max_index;
}

//-----------------------------------------------------------------------------------------------
int define_find(char* token)
{
    // <ZZ> This function returns the pxss_header of the first #define that matches token.  If there
    //      aren't any matches, it returns -1.
    int i;
    int max_scope = -1;
    int max_index = -1;

    // Check each define...
    for (i = define_count - 1; i >= 0; i--)
    {
        if (0 == strcmp(token, defines[i].tag))
        {
            if (defines[i].scope > max_scope)
            {
                max_scope = defines[i].scope;
                max_index = i;
            }
        };
    }

    return max_index;
}

//-----------------------------------------------------------------------------------------------
int define_get_free()
{
    // <BB> get a free define from defines[]. returns the indes of the free define or -1 on failure

    int i, index;

    // start at the top and go down, 'cause that's where the empty ones are likely to be
    index = -1;

    for (i = define_count - 1; i >= 0; i--)
    {
        if (EOS == defines[i].tag[0])
        {
            index = i;
            break;
        }
    }

    if (-1 == index)
    {
        if (define_count < PXSS_MAX_DEFINE)
        {
            index = define_count;
            define_count++;
        }
    }

    return index;
}

//-----------------------------------------------------------------------------------------------
void define_add(SDF_PSTREAM pstream, char* tag, char scope, bool_t allow_redefine)
{
    // <ZZ> This function registers a new #define'd pstream->read.  Temporary_level determines if the
    //      define is removed after a file or function is done being parsed.
    TOKEN temptoken;

    int i, j;
    bool_t found;

    SDF_STREAM loc_stream;
    DEFINE *   pdef;

    int define_to_use;

    // re-use the define if it is allowed
    define_to_use = define_find_scope(tag, scope);

    if (define_to_use < 0)
    {
        define_to_use = define_get_free();
    }
    else if (!allow_redefine)
    {
        log_error(0, "Attempted to redefine \"#define %s %s\" to \"%s\" at scope %d", defines[define_to_use].tag, defines[define_to_use].value, tag, scope);
        pxss_file_found_error = ktrue;
    };

    if (define_to_use < 0)
    {
        log_error(0, "Couldn't define %s...  Out of tokens to use...", tag);
        pxss_file_found_error = ktrue;
    }

    // shorten "defines[define_to_use]." to "pdef->"
    pdef = defines + define_to_use;

    // Skip whitespace...
    while ((*tag) == ' ')  tag++;

    while (*(pstream->read) == ' ')  pstream->read++;

    // Copy the tag...
    strncpy(pdef->tag, tag, PXSS_TAG_SIZE);
    pdef->tag[PXSS_TAG_SIZE-1] = EOS;

    // Set the scope
    pdef->scope = scope;

    // Now search through the pstream->read, looking for previously #defined tokens...
    i = 0;
    sdf_stream_open_mem(&loc_stream, pstream->read, PXSS_TAG_SIZE);
    next_token_may_be_negative = ktrue;

    while (pxss_read_token(&loc_stream, &temptoken))
    {
        found = -1;

        if (TOKEN_UNKNOWN == temptoken.type)
        {
            found = define_find(temptoken.tag);
        }
        else
        {
            found = -1;
        }

        if (found > -1)
        {
            // Copy the pstream->read of the old tag into the new one...
            j = 0;

            while (defines[found].value[j] != 0 && i < PXSS_TAG_SIZE - 1)
            {
                pdef->value[i] = defines[found].value[j];
                i++;
                j++;
            }

            if (i < PXSS_TAG_SIZE - 1)
            {
                pdef->value[i] = ' ';
                i++;
            }
        }
        else
        {
            // Copy the tag into the new define...
            j = 0;

            while (temptoken.tag[j] != EOS && i < PXSS_TAG_SIZE - 1)
            {
                pdef->value[i] = temptoken.tag[j];
                i++;
                j++;
            }

            if (i < PXSS_TAG_SIZE - 1)
            {
                pdef->value[i] = ' ';
                i++;
            }
        }
    }

    if (i < PXSS_TAG_SIZE)
    {
        pdef->value[i] = 0;
        i--;
    }

    if (i >= 0)
    {
        if (pdef->value[i] == ' ')
        {
            pdef->value[i] = 0;
        }
    }

    log_info(1, "Defined...  %s == %s", pdef->tag, pdef->value);

    // advance the stream pointer of the parent stream
    pstream->read = loc_stream.read;


}

//-----------------------------------------------------------------------------------------------
void define_add_string(char* tag, char * value, char scope, bool_t allow_redefine)
{
    SDF_STREAM loc_stream;

    sdf_stream_open_mem(&loc_stream, value, sizeof(value) + 1);

    define_add(&loc_stream, tag, scope, allow_redefine);
};

//-----------------------------------------------------------------------------------------------
void define_pop_to_scope(int scope)
{
    // <ZZ> This function gets rid of any defines that we don't need
    int i;

    repeat(i, define_count)
    {
        if (defines[i].scope > scope)
        {
            defines[i].tag[0]   =  EOS;
            defines[i].value[0] =  EOS;
            defines[i].scope    = -1;
        }
    }
}

//-----------------------------------------------------------------------------------------------
int pxss_register_token(TOKEN * ptok)
{
    assert(pxss_token_index < PXSS_MAX_TOKEN_REGISTER - 1);
    memcpy(pxss_token_list + pxss_token_index, ptok, sizeof(TOKEN));
    pxss_token_index++;

    return pxss_token_index;
}

//-----------------------------------------------------------------------------------------------
void pxss_register_tokens_lib()
{
    TOKEN tmp_tok;

    tmp_tok.tag[0]            = '\0';
    tmp_tok.opcode            = OPCODE_NIL;
    tmp_tok.type              = TOKEN_UNKNOWN;
    tmp_tok.arg_list          = arg_list_none;
    tmp_tok.variable_type     = VAR_UNKNOWN;
    tmp_tok.number_to_destroy = -1;
    tmp_tok.extension         = -1;
    tmp_tok.is_in_rpn         = kfalse;
    tmp_tok.change_type       = VAR_UNKNOWN;

    // File functions
    tmp_tok.type     = TOKEN_FUNCTION;
    tmp_tok.fix_bits = FIX_PREFIX;
    { strcpy(tmp_tok.tag, "FILEOPEN");         tmp_tok.opcode = OPCODE_LIB_FILEOPEN;     tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ii;    tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FILEREADBYTE");     tmp_tok.opcode = OPCODE_LIB_FILEREADBYTE; tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ii;    tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FILEWRITEBYTE");    tmp_tok.opcode = OPCODE_LIB_FILEWRITEBYTE; tmp_tok.number_to_destroy = 3; tmp_tok.arg_list = arg_list_iii;   tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FILEINSERT");       tmp_tok.opcode = OPCODE_LIB_FILEINSERT;   tmp_tok.number_to_destroy = 4; tmp_tok.arg_list = arg_list_iiii;  tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }

    // String functions...
    tmp_tok.type     = TOKEN_FUNCTION;
    tmp_tok.fix_bits = FIX_PREFIX;
    { strcpy(tmp_tok.tag, "STRING");            tmp_tok.opcode = OPCODE_LIB_STRING;             tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGGETNUMBER");   tmp_tok.opcode = OPCODE_LIB_STRINGGETNUMBER;    tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGCLEAR");       tmp_tok.opcode = OPCODE_LIB_STRINGCLEAR;        tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGCLEARALL");    tmp_tok.opcode = OPCODE_LIB_STRINGCLEARALL;     tmp_tok.number_to_destroy = 0; tmp_tok.arg_list = arg_list_none;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGAPPEND");      tmp_tok.opcode = OPCODE_LIB_STRINGAPPEND;       tmp_tok.number_to_destroy = 3; tmp_tok.arg_list = arg_list_iii;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGCOMPARE");     tmp_tok.opcode = OPCODE_LIB_STRINGCOMPARE;      tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ii;  tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGLENGTH");      tmp_tok.opcode = OPCODE_LIB_STRINGLENGTH;       tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGCHOPLEFT");    tmp_tok.opcode = OPCODE_LIB_STRINGCHOPLEFT;     tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ii;  tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGCHOPRIGHT");   tmp_tok.opcode = OPCODE_LIB_STRINGCHOPRIGHT;    tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ii;  tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGGETVALUE");    tmp_tok.opcode = OPCODE_LIB_FILEREADBYTE;       tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ii;  tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGSETVALUE");    tmp_tok.opcode = OPCODE_LIB_FILEWRITEBYTE;      tmp_tok.number_to_destroy = 3; tmp_tok.arg_list = arg_list_iii;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }

    // get/put Functions
    tmp_tok.type     = TOKEN_FUNCTION;
    tmp_tok.fix_bits = FIX_PREFIX;
    { strcpy(tmp_tok.tag, "SYSTEMSET");        tmp_tok.opcode = OPCODE_SF_SYSTEMSET;    tmp_tok.number_to_destroy = 4; tmp_tok.arg_list = arg_list_iiii;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "SYSTEMGET");        tmp_tok.opcode = OPCODE_SF_SYSTEMGET;    tmp_tok.number_to_destroy = 3; tmp_tok.arg_list = arg_list_iii;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
};