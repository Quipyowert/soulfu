// The "Programmer X Scripting System" by Programmer X, aka Aaron Bishop
//
// This code was abstracted into a library from the scripting system in SoulFu and
// is licensed to be distributed solely with that application, under Aaron's "Be Nice" license.
//
// file : pxss_run.inl

#pragma once

#include "pxss_run.h"
#include "logfile.h"

#include <assert.h>

_INLINE PBaseScriptContext _pxss_read_callfunction_params(PBaseScriptContext pnew);
_INLINE void pxss_script_clear_base( PBaseScriptContext pbs );
_INLINE void pxss_script_clear( PScriptContext ps );

_INLINE void _update_global(PGlobalScriptContext pgs, PScriptContext ps);
_INLINE void _update_global_basic(PBaseGlobalScriptContext pbgs, PBaseScriptContext pbs);

//-----------------------------------------------------------------------------------------------
// basic functions for parsing the token stream

_INLINE Uint8  _receive_uint08(SDF_PSTREAM ps);
_INLINE Uint16 _receive_uint16(SDF_PSTREAM ps);
_INLINE Uint32 _receive_uint32(SDF_PSTREAM ps);
_INLINE float  _receive_float (SDF_PSTREAM ps);
_INLINE char * _receive_string(SDF_PSTREAM ps);

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
_INLINE bool_t pxss_var_clear(PXSS_VAR * pvar)
{
    if (NULL == pvar) return kfalse;

    pvar->type = PXSS_VAR_UNKNOWN;

    return ktrue;
}

_INLINE bool_t pxss_var_set_int(PXSS_VAR * pvar, SINT val)
{
    if (NULL == pvar) return kfalse;

    pvar->type = PXSS_VAR_INT;
    pvar->i    = val;

    return ktrue;
}

_INLINE bool_t pxss_var_set_flt(PXSS_VAR * pvar, FLOT val)
{
    if (NULL == pvar) return kfalse;

    pvar->type = PXSS_VAR_FLT;
    pvar->f    = val;

    return ktrue;
}

_INLINE bool_t pxss_var_set_ptr(PXSS_VAR * pvar, Uint8* val)
{
    if (NULL == pvar) return kfalse;

    pvar->type = PXSS_VAR_PTR;
    pvar->p    = val;

    return ktrue;
}

_INLINE SINT pxss_var_get_int(PXSS_VAR * pvar)
{
    SINT retval = 0;
    bool_t error = kfalse;

    switch (pvar->type)
    {
        case PXSS_VAR_INT:
            retval = pvar->i;
            break;

        case PXSS_VAR_FLT:
            retval = (SINT)pvar->f;
            break;

        default:
        case PXSS_VAR_PTR:
        case PXSS_VAR_UNKNOWN:
            error = ktrue;
            break;
    }

    if (error)
    {
        assert(kfalse);
    }

    return retval;
}

_INLINE FLOT pxss_var_get_flt(PXSS_VAR * pvar)
{
    FLOT retval = 0;
    bool_t error = kfalse;

    switch (pvar->type)
    {
        case PXSS_VAR_INT:
            retval = (FLOT)pvar->i;
            break;

        case PXSS_VAR_FLT:
            retval = pvar->f;
            break;

        default:
        case PXSS_VAR_PTR:
        case PXSS_VAR_UNKNOWN:
            error = ktrue;
            break;
    }

    if (error)
    {
        assert(kfalse);
    }

    return retval;
}

_INLINE Uint8 * pxss_var_get_ptr(PXSS_VAR * pvar)
{
    Uint8 * retval = 0;
    bool_t error = kfalse;

    switch (pvar->type)
    {
        case PXSS_VAR_INT:
            retval = (Uint8*)(pvar->i);
            break;

        case PXSS_VAR_PTR:
            retval = pvar->p;
            break;

        default:
        case PXSS_VAR_FLT:
        case PXSS_VAR_UNKNOWN:
            error = ktrue;
            break;
    }

    if (error)
    {
        assert(kfalse);
    }

    return retval;
}

//-----------------------------------------------------------------------------------------------
_INLINE Uint8  _receive_uint08(SDF_PSTREAM ps)
{
    Uint8 retval = *(ps->read);
    ps->read += sizeof(Uint8);
    return retval;
};

_INLINE Uint16 _receive_uint16(SDF_PSTREAM ps)
{
    Uint16 retval = endian_read_mem_int16(ps->read);
    ps->read += sizeof(Uint16);
    return retval;
};

_INLINE Uint32 _receive_uint32(SDF_PSTREAM ps)
{
    Uint32 retval = endian_read_mem_int32(ps->read);
    ps->read += sizeof(Uint32);
    return retval;
};

_INLINE float  _receive_float(SDF_PSTREAM ps)
{
    float retval = endian_read_mem_float(ps->read);
    ps->read += sizeof(float);
    return retval;
};

_INLINE char * _receive_string(SDF_PSTREAM ps)
{
    char * retval = ps->read;

    ps->read += strlen(retval) + 1;

    return retval;
};

//-----------------------------------------------------------------------------------------------
// <ZZ> These macros are for controlling the stacks...
//-----------------------------------------------------------------------------------------------
_INLINE void show_stack(PBaseScriptContext ps)
{
    PBaseGlobalScriptContext loc_pbgs;
    PXSS_VAR * pvar;
    int i;

    assert(NULL != ps);

    loc_pbgs = ps->pbgs;
    assert(NULL != loc_pbgs);

    log_info(0, "Current integer stack...");
    repeat(i, loc_pbgs->stack_head)
    {
        pvar = loc_pbgs->stack + loc_pbgs->stack_head - i - 1;

        switch (pvar->type)
        {
            case PXSS_VAR_INT:
                log_info(0, "\t\t\tSTK: 0x%04d %d", i, pvar->i);
                break;

            case PXSS_VAR_FLT:
                log_info(0, "\t\t\tSTK: 0x%04d %f", i, pvar->f);
                break;

            case PXSS_VAR_PTR:
                log_info(0, "\t\t\tSTK: 0x%04d %p", i, pvar->p);
                break;

            case PXSS_VAR_UNKNOWN:
                log_info(0, "\t\t\tSTK: 0x%04d UNKNOWN", i);
                log_info(0, "\t\t\t            %d", i, pvar->i);
                log_info(0, "\t\t\t            %f", i, pvar->f);
                log_info(0, "\t\t\t            %p", i, pvar->p);
                break;

            default:
                log_info(0, "\t\t\tSTK: 0x%04d INVALID", i);
                break;
        }
    }
}

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
_INLINE void push_int_stack(PBaseScriptContext pbs, SINT A)
{
    PBaseGlobalScriptContext   loc_pbgs;

    assert(NULL != pbs);

    loc_pbgs = pbs->pbgs;
    assert(NULL != loc_pbgs);

    assert(loc_pbgs->stack_head < MAX_STACK);

    log_info(pxss_debug_level, "\t\t\tSTK: 0x%04d push int %d", loc_pbgs->stack_head, A);

    pxss_var_set_int(loc_pbgs->stack + loc_pbgs->stack_head, A);
    loc_pbgs->stack_head++;
}

//-----------------------------------------------------------------------------------------------
_INLINE SINT pop_int_stack(PBaseScriptContext pbs)
{
    SINT A;

    PBaseGlobalScriptContext loc_pbgs;

    assert(NULL != pbs);

    loc_pbgs = pbs->pbgs;
    assert(NULL != loc_pbgs);

    assert(loc_pbgs->stack_head >= 1 && loc_pbgs->stack_head >= pbs->link);

    loc_pbgs->stack_head--;
    A = pxss_var_get_int(loc_pbgs->stack + loc_pbgs->stack_head);

    log_info(pxss_debug_level, "\t\t\tSTK: 0x%04d pop int %d", loc_pbgs->stack_head, A);
    return A;
}

//-----------------------------------------------------------------------------------------------
_INLINE SINT peek_int_stack(PBaseScriptContext pbs)
{
    PBaseGlobalScriptContext loc_pbgs;

    assert(NULL != pbs);

    loc_pbgs = pbs->pbgs;
    assert(NULL != loc_pbgs);

    return pxss_var_get_int(loc_pbgs->stack + loc_pbgs->stack_head - 1 );
}

//-----------------------------------------------------------------------------------------------
#define pop_int_stack_cast(PS, B) CAST(B, pop_int_stack(PS))
#define operate_int_stack(A, B) { \
        SINT old_val = pxss_var_get_int(pbgs->stack + pbgs->stack_head - 1); \
        SINT new_val = old_val A B;                                          \
        pxss_var_set_int(pbgs->stack + pbgs->stack_head - 1, new_val);         \
        log_info(pxss_debug_level, "\t\t\tSTK: 0x%04d operate int %d \"%s\" %d = %d", pbgs->stack_head-1, old_val, #A, B, new_val); \
    }
#define preoperate_int_stack(A) { \
        SINT old_val = pxss_var_get_int(pbgs->stack + pbgs->stack_head - 1); \
        SINT new_val = A (old_val);                                            \
        pxss_var_set_int(pbgs->stack + pbgs->stack_head - 1, new_val);         \
        log_info(pxss_debug_level, "\t\t\tSTK: 0x%04d pre-operate int \"%s\" %d = %d", pbgs->stack_head-1, #A, old_val, new_val); \
    }

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
_INLINE void push_flt_stack(PBaseScriptContext pbs, FLOT A)
{
    PBaseGlobalScriptContext   loc_pbgs;

    assert(NULL != pbs);

    loc_pbgs = pbs->pbgs;
    assert(NULL != loc_pbgs);

    assert(loc_pbgs->stack_head < MAX_STACK);

    log_info(pxss_debug_level, "\t\t\tSTK: 0x%04d push flt %d", loc_pbgs->stack_head, A);

    pxss_var_set_flt(loc_pbgs->stack + loc_pbgs->stack_head, A);
    loc_pbgs->stack_head++;
}

//-----------------------------------------------------------------------------------------------
_INLINE FLOT pop_flt_stack(PBaseScriptContext pbs)
{
    FLOT A;

    PBaseGlobalScriptContext loc_pbgs;

    assert(NULL != pbs);

    loc_pbgs = pbs->pbgs;
    assert(NULL != loc_pbgs);

    assert(loc_pbgs->stack_head >= 1 && loc_pbgs->stack_head >= pbs->link);

    loc_pbgs->stack_head--;
    A = pxss_var_get_flt(loc_pbgs->stack + loc_pbgs->stack_head);

    log_info(pxss_debug_level, "\t\t\tSTK: 0x%04d pop flt %d", loc_pbgs->stack_head, A);
    return A;
}

//-----------------------------------------------------------------------------------------------
_INLINE FLOT peek_flt_stack(PBaseScriptContext pbs)
{
    PBaseGlobalScriptContext loc_pbgs;

    assert(NULL != pbs);

    loc_pbgs = pbs->pbgs;
    assert(NULL != loc_pbgs);

    return pxss_var_get_flt(loc_pbgs->stack + loc_pbgs->stack_head - 1 );
}

//-----------------------------------------------------------------------------------------------
#define pop_flt_stack_cast(PS, B) CAST(B, pop_flt_stack(PS))
#define operate_flt_stack(A, B) { \
        FLOT old_val = pxss_var_get_flt(pbgs->stack + pbgs->stack_head - 1); \
        FLOT new_val = old_val A B;                                          \
        pxss_var_set_flt(pbgs->stack + pbgs->stack_head - 1, new_val);         \
        log_info(pxss_debug_level, "\t\t\tSTK: 0x%04d operate int %0.2f \"%s\" %0.2f = %0.2f", pbgs->stack_head-1, old_val, #A, B, new_val); \
    }
#define preoperate_flt_stack(A) { \
        FLOT old_val = pxss_var_get_flt(pbgs->stack + pbgs->stack_head - 1); \
        FLOT new_val = A (old_val);                                            \
        pxss_var_set_flt(pbgs->stack + pbgs->stack_head - 1, new_val);         \
        log_info(pxss_debug_level, "\t\t\tSTK: 0x%04d pre-operate int \"%s\" %0.2f = %0.2f", pbgs->stack_head-1, #A, old_val, new_val); \
    }

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
_INLINE PBaseScriptContext _pxss_read_callfunction_params(PBaseScriptContext pnew)
{
    PBaseGlobalScriptContext pbgs = NULL;

    SDF_PDATA _call_address, _return_address;
    SDF_PDATA _arg_address, _tmp_args, _file_address;
    char *    _function_name;
    int i, num_vars;

    pbgs = (NULL == pnew) ? NULL : pnew->pbgs;

    if (NULL == pnew || NULL == pbgs) return NULL;

    // Read in the addresses...
    _call_address   = (Uint8*) _receive_uint32(&(pnew->tokens));
    _return_address = (Uint8*) _receive_uint32(&(pnew->tokens));
    _arg_address    = (Uint8*) _receive_uint32(&(pnew->tokens));
    _file_address   = (Uint8*) _receive_uint32(&(pnew->tokens));
    _function_name  = pnew->tokens.read;

    // store some flow control values
    pnew->function_start = _call_address;
    pnew->return_address = _return_address;

    // Count args in argument list...
    num_vars = strlen(_arg_address);

    // "pop" the values off the stacks
    pbgs->stack_head -= num_vars;

    // Aaron's justification for not storing local variables on the stack:
    // "Copy arguments to a seperate data area...  Needed in case the stack header
    // is lying on the edge, about to wrap around, and some of the arguments fall
    // on one side, some on the other...  Otherwise we could just use the stacks
    // as they are..."

    _tmp_args = _arg_address;
    pnew->num_int_vars = 0;
    pnew->num_flt_vars = 0;
    repeat(i, num_vars)
    {
        if ( CHAR_TO_VAR(*_tmp_args) == VAR_INT)
        {
            pnew->int_variable[pnew->num_int_vars] = pxss_var_get_int(pbgs->stack + pbgs->stack_head + i);
            pnew->num_int_vars++;
        }
        else
        {
            pnew->flt_variable[pnew->num_flt_vars] = pxss_var_get_flt(pbgs->stack + pbgs->stack_head + i);
            pnew->num_flt_vars++;
        }

        _tmp_args++;
    }

    // set some debug info
    pnew->file_start     = _file_address;
    pnew->file_name      = pxss_find_file_name(_file_address);
    pnew->function_name  = _function_name;

    return pnew;
};

//-----------------------------------------------------------------------------------------------
_INLINE PBaseScriptContext pxss_prepare_fast_run_base(PBaseScriptContext pbs, Uint16 fast_function)
{
    if (NULL == pbs) return NULL;

    pxss_fast_run_found  = kfalse;
    pxss_fast_run_offset = endian_read_mem_int16(pbs->file_start + fast_function);

    if (UINT16_MAX == pxss_fast_run_offset || 0 == pxss_fast_run_offset)
    {
        return NULL;
    };

    pxss_fast_run_found = ktrue;

    pbs->function_name  = pxss_fast_function_name[fast_function>>1];

    pbs->function_start = pbs->file_start + pxss_fast_run_offset;

    return pbs;
}

//-----------------------------------------------------------------------------------------------
_INLINE PScriptContext pxss_prepare_fast_run(PScriptContext ps, Uint16 fast_function)
{
    if (NULL == pxss_prepare_fast_run_base( &(ps->base), fast_function))
    {
        return NULL;
    };

    return ps;
}

//-----------------------------------------------------------------------------------------------
_INLINE void pxss_script_clear_base( PBaseScriptContext pbs )
{
    pbs->file_start     = NULL;
    pbs->function_start = NULL;
    pbs->function_name  = "";
    pbs->running        = kfalse;

    sdf_stream_init( (&pbs->tokens) );
};

//-----------------------------------------------------------------------------------------------
_INLINE void pxss_script_clear( PScriptContext ps )
{
    pxss_script_clear_base( &(ps->base) );
}

//-----------------------------------------------------------------------------------------------
_INLINE void _update_global(PGlobalScriptContext pgs, PScriptContext ps)
{
    _update_global_basic( &(pgs->base), &(ps->base) );
};

//-----------------------------------------------------------------------------------------------
_INLINE void _update_global_basic(PBaseGlobalScriptContext pbgs, PBaseScriptContext pbs)
{
    //---- SHARED INFO ----
    memcpy( &(pbgs->return_var), &(pbs->return_var), sizeof(PXSS_VAR));
}
