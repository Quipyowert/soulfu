// The "Programmer X Scripting System" by Programmer X, aka Aaron Bishop
//
// This code was abstracted into a library from the scripting system in SoulFu and
// is licensed to be distributed solely with that application, under Aaron's "Be Nice" license.
//
// file : pxss_compile.h

#pragma once

#include "sdf_archive.h"

#define PXSS_MAX_TOKEN            0x40   // The maximum number of pieces per line of a SRC file
#define PXSS_TAG_SIZE             0x80   // The maximum size of each piece
#define PXSS_MAX_INDENT         0x0100   // Allow up to 0x0100 indentation levels...
#define PXSS_MAX_TOKEN_REGISTER 0x0100   // Allow up to 0x0100 tokens to be registered

#define CHAR_TO_VAR(XX) CAST(VAR_TYPE, (XX) - 'A')
#define VAR_TO_CHAR(XX) CAST(Uint8, (XX) + 'A')

enum var_type_e
{
    VAR_UNKNOWN  = ('?' - 'A'),   // Used a bunch
    VAR_VOID     = ('V' - 'A'),   // ?For void functions?
    VAR_INT      = ('I' - 'A'),   // Used a bunch
    VAR_FLT      = ('F' - 'A'),   // Used a bunch
    VAR_PTR      = ('P' - 'A'),   // Used for properties
    VAR_STRING   = ('S' - 'A'),   // For property extensions only
    VAR_TEXT     = ('T' - 'A'),   // For property extensions only
    VAR_BYTE     = ('B' - 'A'),   // For property extensions only
    VAR_WORD     = ('W' - 'A')    // For property extensions only
};
typedef enum var_type_e VAR_TYPE;

#define MAX_VARIABLE               0x20   // I00 - I31, F00 - F31...
#define MAX_ARGUMENT               16

#define PXSS_HEADERIZE   1   // The first stage of compilation
#define PXSS_COMPILERIZE 2   // The second stage of compilation
#define PXSS_FUNCTIONIZE 3   // The third stage of compilation

extern char * opcode_name[256];

extern bool_t pxss_file_found_error;                    // For compiler errors
extern bool_t pxss_compiler_error;

enum opcode_e
{
    OPCODE_ASSIGN                  =   0,
    OPCODE_ADD                , // =   1,
    OPCODE_SUBTRACT           , // =   2,
    OPCODE_MULTIPLY           , // =   3,
    OPCODE_DIVIDE             , // =   4,
    OPCODE_INCREMENT          , // =   5,
    OPCODE_DECREMENT          , // =   6,
    OPCODE_ISEQUAL            , // =   7,
    OPCODE_ISNOTEQUAL         , // =   8,
    OPCODE_ISGREATEREQUAL     , // =   9,
    OPCODE_ISLESSEREQUAL      , // =  10,
    OPCODE_ISGREATER          , // =  11,
    OPCODE_ISLESSER           , // =  12,
    OPCODE_LOGICALAND         , // =  13,
    OPCODE_LOGICALOR          , // =  14,
    OPCODE_LOGICALNOT         , // =  15,
    OPCODE_NEGATE             , // =  16,
    OPCODE_BITWISEXOR         , // =  17,
    OPCODE_BITWISEINVERT      , // =  18,
    OPCODE_BITWISEAND         , // =  19,
    OPCODE_BITWISEOR          , // =  20,
    OPCODE_BITWISELEFT        , // =  21,
    OPCODE_BITWISERIGHT       , // =  22,
    OPCODE_MODULUS            , // =  23,
    OPCODE_UNUSED_24          ,
    OPCODE_UNUSED_25          ,
    OPCODE_UNUSED_26          ,
    OPCODE_UNUSED_27          ,
    OPCODE_UNUSED_28          ,
    OPCODE_UNUSED_29          ,
    OPCODE_TOFLOAT            , // =  30,
    OPCODE_TOINT              , // =  31,
    OPCODE_F_ASSIGN           , // =  32,
    OPCODE_F_ADD              , // =  33,
    OPCODE_F_SUBTRACT         , // =  34,
    OPCODE_F_MULTIPLY         , // =  35,
    OPCODE_F_DIVIDE           , // =  36,
    OPCODE_F_INCREMENT        , // =  37,
    OPCODE_F_DECREMENT        , // =  38,
    OPCODE_F_ISEQUAL          , // =  39,
    OPCODE_F_ISNOTEQUAL       , // =  40,
    OPCODE_F_ISGREATEREQUAL   , // =  41,
    OPCODE_F_ISLESSEREQUAL    , // =  42,
    OPCODE_F_ISGREATER        , // =  43,
    OPCODE_F_ISLESSER         , // =  44,
    OPCODE_F_LOGICALAND       , // =  45,
    OPCODE_F_LOGICALOR        , // =  46,
    OPCODE_F_LOGICALNOT       , // =  47,
    OPCODE_F_NEGATE           , // =  48,
    OPCODE_UNUSED_49          , // =  49,
    OPCODE_UNUSED_50          ,
    OPCODE_UNUSED_51          ,
    OPCODE_UNUSED_52          ,
    OPCODE_UNUSED_53          ,
    OPCODE_UNUSED_54          ,
    OPCODE_UNUSED_55          ,
    OPCODE_UNUSED_56          ,
    OPCODE_UNUSED_57          ,
    OPCODE_UNUSED_58          ,
    OPCODE_UNUSED_59          ,
    OPCODE_UNUSED_60          ,
    OPCODE_UNUSED_61          ,
    OPCODE_UNUSED_62          ,
    OPCODE_UNUSED_63          ,
    OPCODE_CALLFUNCTION       , // =  64,
    OPCODE_RETURNINT          , // =  65,
    OPCODE_RETURNFLOAT        , // =  66,
    OPCODE_IFFALSEJUMP        , // =  67,
    OPCODE_JUMP               , // =  68,
    OPCODE_SQRT               , // =  69,
    OPCODE_UNUSED_70          ,
    OPCODE_UNUSED_71          ,
    OPCODE_UNUSED_72          ,
    OPCODE_UNUSED_73          ,
    OPCODE_UNUSED_74          ,
    OPCODE_UNUSED_75          ,
    OPCODE_UNUSED_76          ,
    OPCODE_UNUSED_77          ,
    OPCODE_UNUSED_78          ,
    OPCODE_UNUSED_79          ,
    OPCODE_UNUSED_80          ,
    OPCODE_UNUSED_81          ,
    OPCODE_UNUSED_82          ,
    OPCODE_SIN                , // =  83,
    OPCODE_UNUSED_84          ,
    OPCODE_UNUSED_85          ,
    OPCODE_UNUSED_86          ,
    OPCODE_UNUSED_87          ,
    OPCODE_UNUSED_88          ,
    OPCODE_NIL                , // =  89,
    OPCODE_TOBYTE             , // =  90,
    OPCODE_TOWORD             , // =  91,
    OPCODE_UNUSED_92          ,
    OPCODE_UNUSED_93          ,
    OPCODE_UNUSED_94          , // =   94,
    OPCODE_UNUSED_95          , // =   95,
    OPCODE_UNUSED_96          ,
    OPCODE_UNUSED_97          ,
    OPCODE_UNUSED_98          ,
    OPCODE_UNUSED_99          ,
    OPCODE_UNUSED_100         ,
    OPCODE_UNUSED_101         ,
    OPCODE_UNUSED_102         ,
    OPCODE_UNUSED_103         ,
    OPCODE_UNUSED_104         ,
    OPCODE_UNUSED_105         ,
    OPCODE_UNUSED_106         ,
    OPCODE_UNUSED_107         ,
    OPCODE_UNUSED_108         ,
    OPCODE_UNUSED_109         ,
    OPCODE_UNUSED_110         ,
    OPCODE_UNUSED_111         ,
    OPCODE_UNUSED_112         ,
    OPCODE_UNUSED_113         ,
    OPCODE_UNUSED_114         ,
    OPCODE_UNUSED_115         ,
    OPCODE_UNUSED_116         ,
    OPCODE_UNUSED_117         ,
    OPCODE_UNUSED_118         ,
    OPCODE_UNUSED_119         ,
    OPCODE_UNUSED_120         ,
    OPCODE_UNUSED_121         ,
    OPCODE_UNUSED_122         ,
    OPCODE_UNUSED_123         ,
    OPCODE_UNUSED_124         ,
    OPCODE_UNUSED_125         ,
    OPCODE_UNUSED_126         ,

    // Last basic function is 0x7F ...
    OPCODE_LAST_BASIC              =  0x7F,

    // Opcodes for integer registers
    OPCODE_IREG00                  = 0x80,
    OPCODE_IREG01,
    OPCODE_IREG02,
    OPCODE_IREG03,
    OPCODE_IREG04,
    OPCODE_IREG05,
    OPCODE_IREG06,
    OPCODE_IREG07,
    OPCODE_IREG08,
    OPCODE_IREG09,
    OPCODE_IREG0A,
    OPCODE_IREG0B,
    OPCODE_IREG0C,
    OPCODE_IREG0D,
    OPCODE_IREG0E,
    OPCODE_IREG0F,
    OPCODE_IREG10,
    OPCODE_IREG11,
    OPCODE_IREG12,
    OPCODE_IREG13,
    OPCODE_IREG14,
    OPCODE_IREG15,
    OPCODE_IREG16,
    OPCODE_IREG17,
    OPCODE_IREG18,
    OPCODE_IREG19,
    OPCODE_IREG1A,
    OPCODE_IREG1B,
    OPCODE_IREG1C,
    OPCODE_IREG1D,
    OPCODE_IREG1E,
    OPCODE_IREG1F,

    // Opcodes for properties derived from integer registers
    OPCODE_PROP00,
    OPCODE_PROP01,
    OPCODE_PROP02,
    OPCODE_PROP03,
    OPCODE_PROP04,
    OPCODE_PROP05,
    OPCODE_PROP06,
    OPCODE_PROP07,
    OPCODE_PROP08,
    OPCODE_PROP09,
    OPCODE_PROP0A,
    OPCODE_PROP0B,
    OPCODE_PROP0C,
    OPCODE_PROP0D,
    OPCODE_PROP0E,
    OPCODE_PROP0F,
    OPCODE_PROP10,
    OPCODE_PROP11,
    OPCODE_PROP12,
    OPCODE_PROP13,
    OPCODE_PROP14,
    OPCODE_PROP15,
    OPCODE_PROP16,
    OPCODE_PROP17,
    OPCODE_PROP18,
    OPCODE_PROP19,
    OPCODE_PROP1A,
    OPCODE_PROP1B,
    OPCODE_PROP1C,
    OPCODE_PROP1D,
    OPCODE_PROP1E,
    OPCODE_PROP1F,

    // Opcodes for float registers
    OPCODE_FREG00,
    OPCODE_FREG01,
    OPCODE_FREG02,
    OPCODE_FREG03,
    OPCODE_FREG04,
    OPCODE_FREG05,
    OPCODE_FREG06,
    OPCODE_FREG07,
    OPCODE_FREG08,
    OPCODE_FREG09,
    OPCODE_FREG0A,
    OPCODE_FREG0B,
    OPCODE_FREG0C,
    OPCODE_FREG0D,
    OPCODE_FREG0E,
    OPCODE_FREG0F,
    OPCODE_FREG10,
    OPCODE_FREG11,
    OPCODE_FREG12,
    OPCODE_FREG13,
    OPCODE_FREG14,
    OPCODE_FREG15,
    OPCODE_FREG16,
    OPCODE_FREG17,
    OPCODE_FREG18,
    OPCODE_FREG19,
    OPCODE_FREG1A,
    OPCODE_FREG1B,
    OPCODE_FREG1C,
    OPCODE_FREG1D,
    OPCODE_FREG1E,
    OPCODE_FREG1F,

    // opcodes for constant values
    OPCODE_ZERO                   , // =  224,
    OPCODE_INT_ONE                , // =  225,
    OPCODE_FLOAT_ONE              , // =  226,
    OPCODE_INT08                  , // =  227,
    OPCODE_INT16                  , // =  228,
    OPCODE_INT32                  , // =  229,
    OPCODE_FLOAT                  , // =  230,
    OPCODE_ZSTRING                , // =  231,

    OPCODE_DEBUG_LINE_NUMBER,
    OPCODE_DEBUG_LINE_POINTER,

    OPCODE_COUNT,

    OPCODE_INTERNAL_BEGIN = 256,

    // markers that help with parsing/tokenizing the line
    OPCODE_INT_COMMA,
    OPCODE_INT_LPAREN,
    OPCODE_INT_RPAREN,
    OPCODE_INT_LBRACKET,
    OPCODE_INT_RBRACKET,
    OPCODE_INT_WHILE,
    OPCODE_INT_IF,
    OPCODE_INT_ELSE,
    OPCODE_INT_RETURN,
    OPCODE_INT_MINUS,
    OPCODE_INT_PLUS
};
typedef enum opcode_e OPCODE;

enum opcode_lib_e
{
    OPCODE_LIB_CALLFUNCTION        = OPCODE_CALLFUNCTION,
    OPCODE_LIB_STRING              = OPCODE_UNUSED_49,
    OPCODE_LIB_STRINGGETNUMBER     = OPCODE_UNUSED_50,
    OPCODE_LIB_STRINGCLEAR         = OPCODE_UNUSED_51,
    OPCODE_LIB_STRINGCLEARALL      = OPCODE_UNUSED_52,
    OPCODE_LIB_STRINGAPPEND        = OPCODE_UNUSED_53,
    OPCODE_LIB_STRINGCOMPARE       = OPCODE_UNUSED_54,
    OPCODE_LIB_STRINGLENGTH        = OPCODE_UNUSED_55,
    OPCODE_LIB_STRINGCHOPLEFT      = OPCODE_UNUSED_56,
    OPCODE_LIB_STRINGCHOPRIGHT     = OPCODE_UNUSED_57,
    OPCODE_LIB_STRINGUPPERCASE     = OPCODE_UNUSED_62,
    OPCODE_LIB_STRINGAPPENDNUMBER  = OPCODE_UNUSED_63,
    OPCODE_LIB_FILEOPEN            = OPCODE_UNUSED_70,
    OPCODE_LIB_FILEREADBYTE        = OPCODE_UNUSED_71,
    OPCODE_LIB_FILEWRITEBYTE       = OPCODE_UNUSED_72,
    OPCODE_LIB_FILEINSERT          = OPCODE_UNUSED_73,
};
typedef enum opcode_lib_e OPCODE_LIB;

enum opcode_soulfu_e
{
    OPCODE_SF_CALLFUNCTION   = OPCODE_CALLFUNCTION,
    OPCODE_SF_SYSTEMSET      = OPCODE_UNUSED_27,
    OPCODE_SF_SYSTEMGET      = OPCODE_UNUSED_28
};
typedef enum opcode_soulfu_e OPCODE_SF;

extern char * opcode_name[256];

extern bool_t pxss_compiler_error;


//-----------------------------------------------------------------------------------------------
// shortcuts

extern char arg_list_none[];
extern char arg_list_i[];
extern char arg_list_ii[];
extern char arg_list_f[];
extern char arg_list_ff[];
extern char arg_list_iii[];
extern char arg_list_iif[];
extern char arg_list_iiii[];
extern char arg_list_iffi[];
extern char arg_list_ffii[];
extern char arg_list_iiiii[];
extern char arg_list_ifffi[];
extern char arg_list_ffiii[];
extern char arg_list_iiiiii[];
extern char arg_list_iffiii[];
extern char arg_list_ffiiii[];
extern char arg_list_ffiiiii[];
extern char arg_list_fffi[];
extern char arg_list_fffiiii[];
extern char arg_list_ffffi[];
extern char arg_list_ffffiii[];
extern char arg_list_ffffiiiiii[];
extern char arg_list_ffffffffffffiii[];

enum fix_bits_t
{
    FIX_INFIX   = (1 << 0),
    FIX_PREFIX  = (1 << 1),
    FIX_POSTFIX = (1 << 2),
    FIX_ASSIGN  = (1 << 3)
};
typedef enum fix_bits_t FIX_BITS;

//-----------------------------------------------------------------------------------------------

// Token stuff...
struct token_t
{
    char       tag[PXSS_TAG_SIZE];           // A place to put the pieces

    int /* OPCODE */     opcode;                      // For figurin' out the RPN data
    int /*enum token_type_e*/ type;                        // operand, operator, function, ...
    int /*enum var_type_e*/ variable_type;               // VAR_FLT or VAR_INT or VAR_UNKNOWN...
    Sint8      number_to_destroy;           // The number of arguments to a function
    char *     arg_list;                    // Points to somethin' like "FII" for a function...
    Sint32     extension;                   // For figurin' out the RPN data
    Uint32     fix_bits;

    bool_t          is_in_rpn;                   // For figurin' out the RPN order
    enum var_type_e change_type;                 // For variable type conversions
};
typedef struct token_t TOKEN;

//-----------------------------------------------------------------------------------------------
// Property stuff...
#define MAX_PROPERTY UINT08_SIZE                      // Must be UINT08_SIZE == 256...  Or else script props need to have 2 byte extensions...
struct property_t
{
    char     tag[16];         // Tags for the properties...  the x of window.x...
    VAR_TYPE type;           // F or I or others...
    Sint16   offset;         // Offset of data for this property
};
typedef struct property_t PROPERTY;

extern int      property_count;                        // The number of registered properties
extern PROPERTY properties[MAX_PROPERTY];

void property_reset_all(void);
void property_add(const char* tag, const char type, const char* offset);
int  property_find(const char* tag);


bool_t pxss_defines_setup(SDF_PSTREAM define_stream);
int    pxss_register_token(TOKEN * ptok);
bool_t pxss_read_token(SDF_PSTREAM pstream, TOKEN * ptok);
void   pxss_compile_setup();

extern bool_t pxss_extended_find_function(TOKEN * ptok);
extern bool_t pxss_headerize(SDF_PSTREAM src_stream, SDF_PSTREAM int_stream, char * filename);
extern bool_t pxss_compilerize(SDF_PSTREAM src_stream, SDF_PSTREAM compile_stream, const char* filename);
extern bool_t pxss_loadize(SDF_PSTREAM run_stream, const char* filename);
extern void   pxss_compile_setup();


extern Uint8* pxss_find_run_file_data(char * filename);
