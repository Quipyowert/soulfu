// The "Programmer X Scripting System" by Programmer X, aka Aaron Bishop
//
// This code was abstracted into a library from the scripting system in SoulFu and
// is licensed to be distributed solely with that application, under Aaron's "Be Nice" license.
//
// file : pxss_compile.inl

#pragma once

#include "pxss_compile.h"

enum token_type_e
{
    TOKEN_UNKNOWN = 0,
    TOKEN_OPERAND,
    TOKEN_OPERATOR,
    TOKEN_FUNCTION,
    TOKEN_DELIMITER
};
typedef enum token_type_e SF_TOKEN_TYPE;

//-----------------------------------------------------------------------------------------------
_INLINE void register_opcode_name(char * name_list[], OPCODE opcode, char * name)
{
    name_list[opcode] = name;
};

//-----------------------------------------------------------------------------------------------
// opcode stream stuff
_INLINE void _emit_uint08(SDF_PSTREAM ps, Uint8  val);
_INLINE void _emit_uint16(SDF_PSTREAM ps, Uint16 val);
_INLINE void _emit_uint32(SDF_PSTREAM ps, Uint32 val);
_INLINE void _emit_float (SDF_PSTREAM ps, float  val);
_INLINE void _emit_string(SDF_PSTREAM ps, const char * str);


//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
void _emit_uint08(SDF_PSTREAM ps, Uint8 val)
{
    *(ps->read) =  val;
    ps->read += sizeof(Uint8);
};

void _emit_uint16(SDF_PSTREAM ps, Uint16 val)
{
    endian_write_mem_int16(ps->read, val);
    ps->read += sizeof(Uint16);
};

void _emit_uint32(SDF_PSTREAM ps, Uint32 val)
{
    endian_write_mem_int32(ps->read, val);
    ps->read += sizeof(Uint32);
};

void _emit_float (SDF_PSTREAM ps, float val)
{
    endian_write_mem_float(ps->read, val);
    ps->read += sizeof(float);
};

void _emit_string(SDF_PSTREAM ps, const char * str)
{
    ps->read += sprintf(ps->read, "%s", str) + 1;
};

