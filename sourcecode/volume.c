// <ZZ> This file has all the stuff for drawing volumetric shadows...

//-----------------------------------------------------------------------------------------------
// The shadow vector tells us how much to offset the shadows for every foot dropped (x,y,z)
// (value at [ZZ] should always be -1.0f) and also tells us the z value ([3]) at which to
// cutoff the shadows...
//float global_volumetric_shadow_vector_xyzz[4] = {0.0f, 1.0f, -1.0f, -20.0f};
float global_volumetric_shadow_vector_xyzz[4] = {0.490f, -0.513f, -1.0f, -20.0f};
// !!!BAD!!!
// !!!BAD!!!  Should macroize this...
// !!!BAD!!!
void volume_shadow_edge(float* start_xyz, float* end_xyz)
{
    // <ZZ> This function draws a volumetric shadow quad for a triangle's edge...
    //      Should be called twice, once for front face and once for back face...
    float start_projected_xyz[3];
    float end_projected_xyz[3];
    float distance;

    // Calculate the projected xyz locations...
    distance = start_xyz[ZZ] - global_volumetric_shadow_vector_xyzz[3];
    start_projected_xyz[XX] = start_xyz[XX] + global_volumetric_shadow_vector_xyzz[XX] * distance;
    start_projected_xyz[YY] = start_xyz[YY] + global_volumetric_shadow_vector_xyzz[YY] * distance;
    start_projected_xyz[ZZ] = global_volumetric_shadow_vector_xyzz[3];

    distance = end_xyz[ZZ] - global_volumetric_shadow_vector_xyzz[3];
    end_projected_xyz[XX] = end_xyz[XX] + global_volumetric_shadow_vector_xyzz[XX] * distance;
    end_projected_xyz[YY] = end_xyz[YY] + global_volumetric_shadow_vector_xyzz[YY] * distance;
    end_projected_xyz[ZZ] = global_volumetric_shadow_vector_xyzz[3];

    // Now draw the quad...  Assume that display stuff has already been set...
    display_start_fan();
    {
        display_vertex(start_xyz);
        display_vertex(end_xyz);
        display_vertex(end_projected_xyz);
        display_vertex(start_projected_xyz);
    }
    display_end();
}

//-----------------------------------------------------------------------------------------------
void volume_rdy_find_temp_stuff(Uint8* data, Uint16 frame)
{
    // <ZZ> Sets up some stuff that's needed for attaching weapons...
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_bone;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* bone_data;
    Uint8 base_model, detail_level;
    Uint16 i;

    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) frame = 0;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);
    detail_level = global_rdy_detail_level;

    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 16;
    bone_data = DEREF( Uint8*, data );
    num_bone = DEREF( Uint16, base_model_data + 6 );

    repeat(i, 8)
    {
        temp_character_bone_number[i] = 255;
    }
    repeat(i, num_bone)
    {
        temp_character_bone_number[(*(bone_data+(i*9))) & 7] = (Uint8) i;
    }
    temp_character_frame_event = frame_data[1];
}

//-----------------------------------------------------------------------------------------------
void volume_rdy_character_shadow(Uint8* data, Uint16 frame, Uint8* bone_frame_data)
{
    // <ZZ> This function draws a volumetric shadow for an RDY file (character)...
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_vertex;
    Uint16 num_bone;
    Uint8* base_model_data;
    Uint8* vertex_data;
    Uint8* bone_data;
    Uint8* frame_data;
    Uint8* edge_line_data;
    Uint8* temp_data;
    Uint16 num_edge_line;
    Uint16 i;
    Uint8 base_model, detail_level;
    Uint32 value;
    float a_xyz[3];
    float b_xyz[3];
    float c_xyz[3];
    float d_xyz[3];
    float* temp_xyz;
    float edge_xyz[3];
    float vector_xyz[3];
    float plane_xyz[3];
    float front_dot;
    float back_dot;
    float cartoon_line_size;

    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) frame = 0;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);

    cartoon_line_size = 0.0875f;
    detail_level = global_rdy_detail_level;

    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 16;
    bone_data = DEREF( Uint8*, data );
    vertex_data = base_model_data;
    num_vertex = DEREF( Uint16, vertex_data ); vertex_data += 6;
    num_bone = DEREF( Uint16, vertex_data ); vertex_data += 2;

    // Generate vertex coordinates based on bone locations...
    if (bone_frame_data)
    {
        render_bone_frame(base_model_data, bone_data, bone_frame_data);
    }

    // Edge line data start...
    edge_line_data = bone_data + (num_bone * 9);
    num_edge_line = DEREF( Uint16, edge_line_data );  edge_line_data += 2;

    // Clear out visibility count for each vertex...
    // Get onscreen coordinates of each vertex...  Stored in fourthbuffer...
    if (num_edge_line > 0)
    {
        // Determine visibility of each edge line...
        temp_data = edge_line_data;
        repeat(i, num_edge_line)
        {
            // 4 vertices of cartoon line define two adjacent triangles with a shared edge...
            value = (DEREF( Uint16, temp_data ));  temp_data++;  temp_data++;  temp_xyz = (float*) (vertex_data + (value << 6));  a_xyz[XX] = temp_xyz[XX];  a_xyz[YY] = temp_xyz[YY];  a_xyz[ZZ] = temp_xyz[ZZ];
            temp_xyz = (float*) (vertex_data + (value << 6) + 15);  a_xyz[XX] += temp_xyz[XX] * cartoon_line_size;  a_xyz[YY] += temp_xyz[YY] * cartoon_line_size;  a_xyz[ZZ] += temp_xyz[ZZ] * cartoon_line_size;
            value = (DEREF( Uint16, temp_data ));  temp_data++;  temp_data++;  temp_xyz = (float*) (vertex_data + (value << 6));  b_xyz[XX] = temp_xyz[XX];  b_xyz[YY] = temp_xyz[YY];  b_xyz[ZZ] = temp_xyz[ZZ];
            temp_xyz = (float*) (vertex_data + (value << 6) + 15);  b_xyz[XX] += temp_xyz[XX] * cartoon_line_size;  b_xyz[YY] += temp_xyz[YY] * cartoon_line_size;  b_xyz[ZZ] += temp_xyz[ZZ] * cartoon_line_size;
            value = (DEREF( Uint16, temp_data ));  temp_data++;  temp_data++;  temp_xyz = (float*) (vertex_data + (value << 6));  c_xyz[XX] = temp_xyz[XX];  c_xyz[YY] = temp_xyz[YY];  c_xyz[ZZ] = temp_xyz[ZZ];
            temp_xyz = (float*) (vertex_data + (value << 6) + 15);  c_xyz[XX] += temp_xyz[XX] * cartoon_line_size;  c_xyz[YY] += temp_xyz[YY] * cartoon_line_size;  c_xyz[ZZ] += temp_xyz[ZZ] * cartoon_line_size;
            value = (DEREF( Uint16, temp_data ));  temp_data++;  temp_data++;  temp_xyz = (float*) (vertex_data + (value << 6));  d_xyz[XX] = temp_xyz[XX];  d_xyz[YY] = temp_xyz[YY];  d_xyz[ZZ] = temp_xyz[ZZ];
            temp_xyz = (float*) (vertex_data + (value << 6) + 15);  d_xyz[XX] += temp_xyz[XX] * cartoon_line_size;  d_xyz[YY] += temp_xyz[YY] * cartoon_line_size;  d_xyz[ZZ] += temp_xyz[ZZ] * cartoon_line_size;


            // Shared edge and light direction give a plane that splits space in half...  Are check points on same side of plane?
            edge_xyz[XX] = b_xyz[XX] - a_xyz[XX];  edge_xyz[YY] = b_xyz[YY] - a_xyz[YY];  edge_xyz[ZZ] = b_xyz[ZZ] - a_xyz[ZZ];
            cross_product(edge_xyz, global_volumetric_shadow_vector_xyzz, plane_xyz);
            vector_xyz[XX] = d_xyz[XX] - a_xyz[XX];  vector_xyz[YY] = d_xyz[YY] - a_xyz[YY];  vector_xyz[ZZ] = d_xyz[ZZ] - a_xyz[ZZ];
            back_dot = dot_product(vector_xyz, plane_xyz);
            vector_xyz[XX] = c_xyz[XX] - a_xyz[XX];  vector_xyz[YY] = c_xyz[YY] - a_xyz[YY];  vector_xyz[ZZ] = c_xyz[ZZ] - a_xyz[ZZ];
            front_dot = dot_product(vector_xyz, plane_xyz);


            // If both are on the same side, we've got an edge...
            if ((front_dot > 0.0f) == (back_dot > 0.0f))
            {
                // Now find surface normal of triangle ABC...  Stored in plane_xyz...
                cross_product(edge_xyz, vector_xyz, plane_xyz);

                // Now dot surface normal with light direction...
                front_dot = dot_product(plane_xyz, global_volumetric_shadow_vector_xyzz);

                if (front_dot > 0.0f)
                {
                    volume_shadow_edge(a_xyz, b_xyz);
                }
                else
                {
                    volume_shadow_edge(b_xyz, a_xyz);
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------
void volume_draw_room_shadows(Uint8* data)
{
    // <ZZ> This function draws all of the terrain shadows.  Should be called
    //      twice...
    Uint16 i;
    Uint16 num_edge_line;
    Uint16 value, c, d;
    Uint8* edge_line_data;
    Uint8* vertex_data;
    float a_xyz[3];
    float b_xyz[3];
    float c_xyz[3];
    float d_xyz[3];
    float edge_xyz[3];
    float vector_xyz[3];
    float plane_xyz[3];
    float* temp_xyz;
    float front_dot;
    float back_dot;

//    float vertex_xyz[4][3];

//    vertex_xyz[0][XX] = 0.0f;    vertex_xyz[0][YY] = 30.0f;    vertex_xyz[0][ZZ] = 20.0f;
//    vertex_xyz[1][XX] = 20.0f;   vertex_xyz[1][YY] = 30.0f;    vertex_xyz[1][ZZ] = 20.0f;
//    vertex_xyz[2][XX] = 20.0f;   vertex_xyz[2][YY] = 30.0f;    vertex_xyz[2][ZZ] = 0.0f;
//    vertex_xyz[3][XX] = 0.0f;    vertex_xyz[3][YY] = 30.0f;    vertex_xyz[3][ZZ] = 0.0f;

//    volume_shadow_edge(vertex_xyz[0], vertex_xyz[1]);
//    volume_shadow_edge(vertex_xyz[1], vertex_xyz[2]);
//    volume_shadow_edge(vertex_xyz[2], vertex_xyz[3]);
//    volume_shadow_edge(vertex_xyz[3], vertex_xyz[0]);


//    display_start_fan();
//   {
//        display_vertex(vertex_xyz[0]);
//        display_vertex(vertex_xyz[3]);
//        display_vertex(vertex_xyz[2]);
//        display_vertex(vertex_xyz[1]);
//    }
//    display_end();

    // Edge line data start...
    vertex_data = data + (DEREF( Uint32, data + SRF_VERTEX_OFFSET ));
    edge_line_data = data + (DEREF( Uint32, data + SRF_EDGE_LINE_OFFSET ));
    num_edge_line = DEREF( Uint16, edge_line_data );  edge_line_data += 2;
    vertex_data += 2;


    // Determine visibility of each edge line...
    repeat(i, num_edge_line)
    {
        // 4 vertices of cartoon line define two adjacent triangles with a shared edge...
        value = (DEREF( Uint16, edge_line_data ));  edge_line_data += 2;  temp_xyz = (float*) (vertex_data + (value * 26));  a_xyz[XX] = temp_xyz[XX];  a_xyz[YY] = temp_xyz[YY];  a_xyz[ZZ] = temp_xyz[ZZ];
        value = (DEREF( Uint16, edge_line_data ));  edge_line_data += 2;  temp_xyz = (float*) (vertex_data + (value * 26));  b_xyz[XX] = temp_xyz[XX];  b_xyz[YY] = temp_xyz[YY];  b_xyz[ZZ] = temp_xyz[ZZ];
        value = (DEREF( Uint16, edge_line_data ));  edge_line_data += 2;  temp_xyz = (float*) (vertex_data + (value * 26));  c_xyz[XX] = temp_xyz[XX];  c_xyz[YY] = temp_xyz[YY];  c_xyz[ZZ] = temp_xyz[ZZ];  c = value;
        value = (DEREF( Uint16, edge_line_data ));  edge_line_data += 2;  temp_xyz = (float*) (vertex_data + (value * 26));  d_xyz[XX] = temp_xyz[XX];  d_xyz[YY] = temp_xyz[YY];  d_xyz[ZZ] = temp_xyz[ZZ];  d = value;


        if (c != d)
        {
            // Shared edge and light direction give a plane that splits space in half...  Are check points on same side of plane?
            edge_xyz[XX] = b_xyz[XX] - a_xyz[XX];  edge_xyz[YY] = b_xyz[YY] - a_xyz[YY];  edge_xyz[ZZ] = b_xyz[ZZ] - a_xyz[ZZ];
            cross_product(edge_xyz, global_volumetric_shadow_vector_xyzz, plane_xyz);
            vector_xyz[XX] = d_xyz[XX] - a_xyz[XX];  vector_xyz[YY] = d_xyz[YY] - a_xyz[YY];  vector_xyz[ZZ] = d_xyz[ZZ] - a_xyz[ZZ];
            back_dot = dot_product(vector_xyz, plane_xyz);
            vector_xyz[XX] = c_xyz[XX] - a_xyz[XX];  vector_xyz[YY] = c_xyz[YY] - a_xyz[YY];  vector_xyz[ZZ] = c_xyz[ZZ] - a_xyz[ZZ];
            front_dot = dot_product(vector_xyz, plane_xyz);

            // If both are on the same side, we've got an edge...
            if ((front_dot > 0.0f) == (back_dot > 0.0f))
            {
                // Now find surface normal of triangle ABC...  Stored in plane_xyz...
                cross_product(edge_xyz, vector_xyz, plane_xyz);

                // Now dot surface normal with light direction...
                front_dot = dot_product(plane_xyz, global_volumetric_shadow_vector_xyzz);

                if (front_dot > 0.0f)
                {
                    volume_shadow_edge(a_xyz, b_xyz);
                }
                else
                {
                    volume_shadow_edge(b_xyz, a_xyz);
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------
// Stuff for finding screen corners
#define CORNER_FORE_SCALE (-ZNEAR)
#define CORNER_LEFT_SCALE (1.68f*ZNEAR)
#define CORNER_UP_SCALE (1.26f*ZNEAR)
void volume_shadow_draw_all()
{
    // <ZZ> This function draws all of the character's shadows (volumetric)...

    matrix_4x3 loc_matrix;
    Uint16 i, j, corner, frame;
    CHR_DATA *chr_data;
    MODEL_DATA *mdl_data;
    float screen_corner_xyz[4][3];
    float old_camera_xyz[3];
    PSoulfuScriptContext pss;

    if (fast_and_ugly_active)
    {
        // No shadows in fast and ugly mode...
        return;
    }

    // Setup some basic display stuff...
    display_paperdoll_off();
    display_shade_off();
    display_texture_off();
    display_zbuffer_on();
    display_zbuffer_write_off();
    display_draw_off();
    display_blend_off();
    display_stencilbuffer_on();

    // Back faces should be invisible and increment stencil buffer values...
    display_stencilbuffer_back();
    display_cull_frontface();

    // For each character...
    repeat(i, MAX_CHARACTER)
    {
        // Is character on?
        chr_data = chr_data_get_ptr( i );
        if (NULL == chr_data) continue;

        pss = &chr_data->script_ctxt;

        // Check character's alpha
        if ((chr_data->alpha > 128) && !(chr_data->mflags & MORE_FLAG_NOSHADOW))
        {
            // Draw this character's shadow
            frame = chr_data->frame;
            mdl_data = chr_data->model.parts + MODEL_PART_BASE;
            volume_rdy_find_temp_stuff(mdl_data->file, frame);

            // Main model
            volume_rdy_character_shadow(mdl_data->file, frame, temp_character_bone_frame[i]);

            // Non-animated overlapped parts...
            frame = 0;
            repeat(j, 7)
            {
                mdl_data = chr_data->model.parts + MODEL_PART_LEGS + j;
                if (mdl_data->file != NULL)
                {
                    volume_rdy_character_shadow(mdl_data->file, frame, temp_character_bone_frame[i]);
                }

            }

            // Draw the animated eyes...
            frame = chr_data->eframe;
            mdl_data = chr_data->model.parts + MODEL_PART_EYES;
            if (mdl_data->file != NULL)
            {
                volume_rdy_character_shadow(mdl_data->file, frame, temp_character_bone_frame[i]);
            }

            // Draw the animated mouth...
            frame = chr_data->mframe;
            mdl_data = chr_data->model.parts + MODEL_PART_MOUTH;
            if (mdl_data->file != NULL)
            {
                volume_rdy_character_shadow(mdl_data->file, frame, temp_character_bone_frame[i]);
            }

            // Draw the left and right and left2 and right2 weapons...  Only drawn if setup in character's model
            // data, and if character's model has the required bone...
            repeat(j, 4)
            {
                mdl_data = &chr_data->model.parts[MODEL_PART_LEFT+j];
                if (mdl_data->file != NULL && temp_character_bone_number[j+1] < 255)
                {
                    // Hand held weapons have 3 frames...  (should be 3 independent base models...)
                    //    Frame 0 is when holstered.
                    //    Frame 1 is when taken out (reversal for shields) or attacking (swipe for weapons)
                    //    Frame 2 is when attacking (swipe thing) (shields don't need this...)
                    switch (j&1)
                    {
                        case 0:
                            frame = (temp_character_frame_event & FRAME_EVENT_LEFT) ? 2 : (temp_character_frame_event >> 7);
                            break;
                        case 1:
                            frame = (temp_character_frame_event & FRAME_EVENT_RIGHT) ? 2 : (temp_character_frame_event >> 7);
                            break;
                    }

                    matrix_good_bone(pss->matrix, temp_character_bone_number[j+1], temp_character_bone_frame[i], chr_data);
                    render_generate_model_world_data(mdl_data->file, frame, loc_matrix, fourthbuffer);  // Generate new bone frame in fourthbuffer
                    volume_rdy_character_shadow(mdl_data->file, frame, fourthbuffer);
                }
            }
        }
    }

    // Now draw the backfaces of the room shadows...
    volume_draw_room_shadows(roombuffer);

    // Front faces should be invisible and decrement the stencil buffer values...
    display_stencilbuffer_front();
    display_cull_on();

    // For each character...
    repeat(i, MAX_CHARACTER)
    {
        // Is character on?
        chr_data = chr_data_get_ptr( i );
        if (NULL == chr_data) continue;

        pss = &chr_data->script_ctxt;

        // Check character's alpha
        if ((chr_data->alpha > 128) && !(chr_data->mflags & MORE_FLAG_NOSHADOW))
        {
            // Draw this character's shadow
            frame = chr_data->frame;
            mdl_data = chr_data->model.parts + MODEL_PART_BASE;
            volume_rdy_find_temp_stuff(mdl_data->file, frame);

            // Main model
            volume_rdy_character_shadow(mdl_data->file, frame, temp_character_bone_frame[i]);

            // Non-animated overlapped parts...  Arms, Head, etc...
            frame = 0;
            repeat(j, 7)
            {
                mdl_data = &chr_data->model.parts[MODEL_PART_LEGS+j];
                if (mdl_data->file != NULL)
                {
                    volume_rdy_character_shadow(mdl_data->file, frame, temp_character_bone_frame[i]);
                }
            }

            // Draw the animated eyes...
            frame = chr_data->eframe;
            mdl_data = chr_data->model.parts + MODEL_PART_EYES;
            if (mdl_data->file != NULL)
            {
                volume_rdy_character_shadow(mdl_data->file, frame, temp_character_bone_frame[i]);
            }

            // Draw the animated mouth...
            frame = chr_data->mframe;
            mdl_data = chr_data->model.parts + MODEL_PART_MOUTH;
            if (mdl_data->file != NULL)
            {
                volume_rdy_character_shadow(mdl_data->file, frame, temp_character_bone_frame[i]);
            }

            // Draw the left and right and left2 and right2 weapons...  Only drawn if setup in character's model
            // data, and if character's model has the required bone...
            repeat(j, 4)
            {
                mdl_data = &chr_data->model.parts[MODEL_PART_LEFT+j];
                if (mdl_data->file != NULL && temp_character_bone_number[j+1] < 255)
                {
                    // Hand held weapons have 3 frames...  (should be 3 independent base models...)
                    //    Frame 0 is when holstered.
                    //    Frame 1 is when taken out (reversal for shields) or attacking (swipe for weapons)
                    //    Frame 2 is when attacking (swipe thing) (shields don't need this...)
                    switch (j&1)
                    {
                        case 0:
                            frame = (temp_character_frame_event & FRAME_EVENT_LEFT) ? 2 : (temp_character_frame_event >> 7);
                            break;
                        case 1:
                            frame = (temp_character_frame_event & FRAME_EVENT_RIGHT) ? 2 : (temp_character_frame_event >> 7);
                            break;
                    }

                    matrix_good_bone(pss->matrix, temp_character_bone_number[j+1], temp_character_bone_frame[i], chr_data);
                    render_generate_model_world_data(mdl_data->file, frame, loc_matrix, fourthbuffer);  // Generate new bone frame in fourthbuffer
                    volume_rdy_character_shadow(mdl_data->file, frame, fourthbuffer);
                }
            }
        }
    }

    // Now draw the frontfaces of the room shadows...
    volume_draw_room_shadows(roombuffer);

    // Find screen corners...  Scooch the camera back a bit, so I can find the corners by goin' forward...
    old_camera_xyz[XX] = camera.xyz[XX];  camera.xyz[XX] -= rotate_camera_matrix[1] * CORNER_FORE_SCALE;
    old_camera_xyz[YY] = camera.xyz[YY];  camera.xyz[YY] -= rotate_camera_matrix[5] * CORNER_FORE_SCALE;
    old_camera_xyz[ZZ] = camera.xyz[ZZ];  camera.xyz[ZZ] -= rotate_camera_matrix[9] * CORNER_FORE_SCALE;

    // Top corners...
    corner = 0;
    repeat(i, 2)
    {
        screen_corner_xyz[corner][XX] = camera.xyz[XX]
                                        + rotate_camera_matrix[0] * CORNER_LEFT_SCALE * ((i << 1) - 1)
                                        + rotate_camera_matrix[1] * CORNER_FORE_SCALE
                                        + rotate_camera_matrix[2] * CORNER_UP_SCALE;
        screen_corner_xyz[corner][YY] = camera.xyz[YY]
                                        + rotate_camera_matrix[4] * CORNER_LEFT_SCALE * ((i << 1) - 1)
                                        + rotate_camera_matrix[5] * CORNER_FORE_SCALE
                                        + rotate_camera_matrix[6] * CORNER_UP_SCALE;
        screen_corner_xyz[corner][ZZ] = camera.xyz[ZZ]
                                        + rotate_camera_matrix[8] * CORNER_LEFT_SCALE * ((i << 1) - 1)
                                        + rotate_camera_matrix[9] * CORNER_FORE_SCALE
                                        + rotate_camera_matrix[10] * CORNER_UP_SCALE;
        corner++;
    }
    // Bottom corners...
    corner = 3;
    repeat(i, 2)
    {
        screen_corner_xyz[corner][XX] = camera.xyz[XX]
                                        + rotate_camera_matrix[0] * CORNER_LEFT_SCALE * ((i << 1) - 1)
                                        + rotate_camera_matrix[1] * CORNER_FORE_SCALE
                                        + rotate_camera_matrix[2] * -CORNER_UP_SCALE;
        screen_corner_xyz[corner][YY] = camera.xyz[YY]
                                        + rotate_camera_matrix[4] * CORNER_LEFT_SCALE * ((i << 1) - 1)
                                        + rotate_camera_matrix[5] * CORNER_FORE_SCALE
                                        + rotate_camera_matrix[6] * -CORNER_UP_SCALE;
        screen_corner_xyz[corner][ZZ] = camera.xyz[ZZ]
                                        + rotate_camera_matrix[8] * CORNER_LEFT_SCALE * ((i << 1) - 1)
                                        + rotate_camera_matrix[9] * CORNER_FORE_SCALE
                                        + rotate_camera_matrix[10] * -CORNER_UP_SCALE;
        corner--;
    }

    // Switch our settings again...
    display_draw_on();
    display_zbuffer_off();
    display_blend_trans();

    // Now draw the shadow as one big rectangle across screen...  Only affect areas where stencil buffer is 0 (normally, sometimes more if camera is inside a shadow)...
    display_stencilbuffer_shadow(8);
    color_temp[0] = 0;    color_temp[1] = 0;    color_temp[2] = 0;    color_temp[3] = 128; //82;
    display_color_alpha(color_temp);
    display_start_fan();
    {
        display_vertex(screen_corner_xyz[0]);
        display_vertex(screen_corner_xyz[1]);
        display_vertex(screen_corner_xyz[2]);
        display_vertex(screen_corner_xyz[3]);
    }
    display_end();

    // Restore the camera to where it was...
    camera.xyz[XX] = old_camera_xyz[XX];
    camera.xyz[YY] = old_camera_xyz[YY];
    camera.xyz[ZZ] = old_camera_xyz[ZZ];

    // Undo display settings...
    display_texture_on();
    display_zbuffer_on();
    display_stencilbuffer_off();

    display_color(red);
    display_start_line_loop();
    {
        display_vertex(screen_corner_xyz[0]);
        display_vertex(screen_corner_xyz[1]);
        display_vertex(screen_corner_xyz[2]);
        display_vertex(screen_corner_xyz[3]);
    }
    display_end();
}

//-----------------------------------------------------------------------------------------------

//Uint8 screen_image[640*480*4];
//Uint32 i;
//if(keyb.pressed[SDLK_F5])
//{
//    glReadPixels(0, 0, 640, 480, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, screen_image);
//    repeat(i, (640*480))
//    {
//        screen_image[i] = (screen_image[i]&3)*80;
//    }
//    display_export_tga("THIRD.TGA", screen_image, 640, 480, 1);
//}
//float value;
//if(keyb.pressed[SDLK_F5])
//{
//    glReadPixels(0, 0, 640, 480, GL_DEPTH_COMPONENT, GL_FLOAT, screen_image);
//    repeat(i, (640*480))
//    {
//        value = ((float*) screen_image)[i];
//        value = (value-0.5f)*2.0f;
//        value*=value;
//        value*=value;
//        value*=value;
//        value*=value;
//        value*=value;
//        value*=value;
//        screen_image[i] = (Uint8) (255.0f*value);
//    }
//    display_export_tga("FIRST.TGA", screen_image, 640, 480, 1);
//}
//if(keyb.pressed[SDLK_F5])
//{
//    glReadPixels(0, 0, 640, 480, GL_RGB, GL_UNSIGNED_BYTE, screen_image);
//    display_export_tga("SECOND.TGA", screen_image, 640, 480, 3);
//    sprintf(DEBUG_STRING, "Export %d", main_video_frame);
//}
//if(keyb.pressed[SDLK_F5])
//{
//    glReadPixels(0, 0, 640, 480, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, screen_image);
//    repeat(i, (640*480))
//    {
//        screen_image[i] = (screen_image[i]&3)*80;
//    }
//    display_export_tga("THIRD.TGA", screen_image, 640, 480, 1);
//}


//-----------------------------------------------------------------------------------------------
