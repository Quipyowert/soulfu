#pragma once

#include "soulfu_types.h"

extern int log_error_count;

Sint8 log_setup(void);
void  log_close(void);

void log_set_verb_level(int verb_level);
int log_get_verb_level();


void log_special(int verb_level, char * header, char *format, ...);
void log_error(int verb_level, char *format, ...);
void log_fail(int verb_level, char *format, ...);
void log_warning(int verb_level, char *format, ...);
void log_info(int verb_level, char *format, ...);
void log_message(int verb_level, char *format, ...);