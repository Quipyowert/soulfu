#pragma once

#include <SDL_types.h>


// use this macro to get around the fact that gcc does not respect pragma pack commands
#    if defined(__GNUC__)
#        define SET_PACKED() __attribute__ ((__packed__))
#    elif defined(_MSC_VER)
#        define SET_PACKED()
#    endif

#ifndef COMPATABILITY_64_BIT
// pointer-sized variables are 32 bit
typedef Sint32 SINT;
typedef Uint32 UINT;
typedef float  FLOT;
#else
// pointer-sized variables are 64 bit
typedef Sint64 SINT;
typedef Uint64 UINT;
typedef float  FLOT;
#endif

// Make sure we have reliable boolean definitions
#if defined(__cplusplus)
    typedef bool bool_t;
#   define ktrue  true
#   define kfalse false
#else
    typedef enum bool_e
    {
        kfalse = (1 == 0),
        ktrue  = (1 == 1)
    } bool_t;
#endif

#ifndef UINT64_MAX
#    define UINT64_MAX 0xFFFFFFFFFFFFFFFF
#endif
#define SINT64_MAX 0x7FFFFFFFFFFFFFFF
#define SINT64_MIN 0x8000000000000000

#define UINT32_SIZE (((Uint64)1) << 32)
#ifndef UINT32_MAX
#    define UINT32_MAX ((Uint32)(UINT32_SIZE-1))
#endif
#define SINT32_MAX 0x7FFFFFFF
#define SINT32_MIN 0x80000000

#define UINT16_SIZE     ((Uint32)(1 << 16))
#ifndef UINT16_MAX
#    define UINT16_MAX  ((Uint16)(UINT16_SIZE-1))
#endif
#define SINT16_MAX 0x7FFF
#define SINT16_MIN 0x8000
#define INV_UINT16_SIZE 0.0000152587890625f

#define UINT08_SIZE    ((Uint16)(1 <<  8))
#ifndef UINT08_MAX
#    define UINT08_MAX ((Uint8 )(UINT08_SIZE-1))
#endif
#define SINT08_MAX 0x7F
#define SINT08_MIN 0x80
#define INV_UINT08_SIZE 0.00390625f


struct soulfu_rect_t
{
    float x;
    float y;
    float w;
    float h;
};
typedef struct soulfu_rect_t SOULFU_RECT;

struct soulfu_window_t
{
    float x;
    float y;
    float w;
    float h;
    float scale;
};
typedef struct soulfu_window_t SOULFU_WINDOW;

typedef char  str16[16];
typedef char  str08[8];

//-----------------------------------------------------------------------------------------------
// a type-safe generic "pointer" type

struct base_data_t;
struct chr_data_t;
struct prt_data_t;
struct win_data_t;

typedef struct base_data_t BASE_DATA;
typedef struct chr_data_t  CHR_DATA;
typedef struct prt_data_t  PRT_DATA;
typedef struct win_data_t  WIN_DATA;

enum object_type_t
{
    OBJECT_UNKNOWN = -2,
    OBJECT_BASE    = -1,
    OBJECT_CHR     =  0,   // Object types...
    OBJECT_PRT,           //
    OBJECT_WIN            //
};
typedef enum object_type_t OBJECT_TYPE;



struct object_ptr_t
{
    OBJECT_TYPE type;
    union
    {
        void      * unk;
        BASE_DATA * bas;
        CHR_DATA  * chr;
        PRT_DATA  * prt;
        WIN_DATA  * win;
    };
};
typedef struct object_ptr_t OBJECT_PTR;

#define NO_ITEM     UINT16_MAX
#define DRAG_ITEM   (UINT16_MAX-1)

struct focus_info_t
{
    OBJECT_PTR object;
    Uint16     item;
};
typedef struct focus_info_t FOCUS_INFO;


#pragma pack(push,1)

struct config_pass_t
{
    Uint8 junk0[10];    // 000-009       Random values
    char  pass[5];      // 010,011,012,013,014 Password chunk 1
    Uint8 junk15;       // 015           Random values
} SET_PACKED();
typedef struct config_pass_t CONFIG_PASS;

struct config_player_data_t
{
    str16 name;         //112-0x7F   Player 1 name (locked if non null)
    Uint8 eqcol01;      //0x80   Player 1 EQCOL01
    Uint8 eqcol23;      //129   Player 1 EQCOL23
    Uint8 monster;      //130   Player 1 Monster Mode
    Uint8 unused131;    //131   Player 1 UNUSED
    Uint8 unused132;    //132   Player 1 UNUSED
    Uint8 unused133;    //133   Player 1 UNUSED
    Uint8 seed;         //134   Player 1 Random seed
    Uint8 voice_pitch;  //135   Player 1 Voice pitch
    Uint8 cclass;        //136   Player 1 Class (0 is random)
    Uint8 unused137[7];
} SET_PACKED();
typedef struct config_player_data_t CONFIG_PLAYER_DATA;

struct config_player_input_t
{
    Uint16 key_att_left;    //0x0100-257 Player 1 Keyboard Attack Left
    Uint16 key_att_right;   //258-259 Player 1 Keyboard Attack Right
    Uint16 key_act_left;    //260-261 Player 1 Keyboard Action Left
    Uint16 key_act_right;   //262-263 Player 1 Keyboard Action Right
    Uint16 key_items;       //264-265 Player 1 Keyboard Items
    Uint16 key_up;          //266-267 Player 1 Keyboard Move Up
    Uint16 key_down;        //268-269 Player 1 Keyboard Move Down
    Uint16 key_left;        //270-271 Player 1 Keyboard Move Left
    Uint16 key_right;       //272-273 Player 1 Keyboard Move Right

    Uint8 joy_att_left;     //274  Player 1 Joystick Attack Left
    Uint8 joy_att_right;    //275  Player 1 Joystick Attack Right
    Uint8 joy_act_left;     //276  Player 1 Joystick Action Left
    Uint8 joy_act_right;    //277  Player 1 Joystick Action Right
    Uint8 joy_items;        //278  Player 1 Joystick Items

    Uint8 device;           //279  Player 1 Device
    Uint8 joy_cam[4];       //280-283 Player 1 Joystick Camera Controls

    Uint8 mouse_shop;       //284  Player 1 Mouse-Shop

    Uint8 unused285[3];     //285-287 Unused
} SET_PACKED();
typedef struct config_player_input_t CONFIG_PLAYER_INPUT;

struct config_hiscore_t
{
    str16  name;    // Byte 0-15 Character Name (null terminated)
    Uint16 score;   // Byte 16-17 Score
    Uint8  cclass;   // Byte 18  Class
    Uint8  badges;  // Byte 19  Badge Flags (top bit used for top bit of eqcol01 - Jive Elf...)
} SET_PACKED();
typedef struct config_hiscore_t CONFIG_HISCORE;

struct config_data_t
{
    CONFIG_PASS pass_chunk[4];

    Uint8 scaling;          //064   Scaling
    Uint8 snd_vol;          //065   Sound volume
    Uint8 mus_vol;          //066   Music volume
    Uint8 lang;             //067   Language
    Uint8 scr_size;         //068   Screen size
    Uint8 rgb_size;         //069   RGB bits
    Uint8 zbuff_size;       //070   Z buffer bits
    Uint8 stencil_shadows;  //071   Stencil Buffer'd Shadows
    Uint8 fullscreen;       //072   Window/Fullscreen
    Uint8 timer;            //073   Pop up the test timer if ktrue

    Uint8 unused074[24];    //074-097   Unused

    Uint8 mipmap;           //098   Mip Map Active (0 is default)
    Uint8 water_layers;     //099   Water Layers Active (1 is default)
    Uint8 line_mode;        //100   Cartoon Line mode (3 is default)
    Uint8 fau_mode;         //101   Fast and Ugly mode (0 is default)
    Uint8 pan;              //102   Flip speaker pan (0 is default)

    Uint8 unused103[9];

    CONFIG_PLAYER_DATA player_data[4];

    Uint8 unused240[16];

    CONFIG_PLAYER_INPUT player_input[4];

    CONFIG_HISCORE hiscore[100];  //384-2383  HiScore Data (100 entries, 20 bytes each)*/
} SET_PACKED();
typedef struct config_data_t CONFIG_DATA;

struct language_file_header_t
{
    Uint32 entries;
    char*  entry[];
} SET_PACKED();
typedef struct language_file_header_t LANGUAGE_FILE_HEADER;

enum eunknown_t
{
    UNKNOWN_VOID = 0,
    UNKNOWN_CHR,
    UNKNOWN_PRT,
    UNKNOWN_WIN
} SET_PACKED();
typedef enum eunknown_t EUNKNOWN;

struct rgb_header_t
{
    Uint16 flags;
    Uint32 gl_tex_index;
    Uint16 x_size, y_size;
    Uint8  data[];
} SET_PACKED();
typedef struct rgb_header_t RGB_HEADER;

struct raw_header_t
{
    Uint32 samples_count; Uint16 samples[];
} SET_PACKED();
typedef struct raw_header_t RAW_HEADER;

struct mus_header_t
{
    char string[16];
} SET_PACKED();
typedef struct mus_header_t MUS_HEADER;

struct srf_header_t
{
    char   string1[0x20];
    Uint32 unknown;
    Uint32 vertex_offset;           // 40
    Uint32 tex_vertex_offset;       // 44
    Uint32 triangle_offset;         // 48
    Uint32 exterior_wall_offset;    // 52
    Uint32 texture_offset;          // 56
    Uint32 heightmap_offset;        // 60
    Uint32 waypoint_offset;         // 0x40
    Uint32 bridge_offset;           // 68
    Uint32 minimap_offset;          // 72
    Uint32 object_group_offset;     // 76
    Uint32 edge_line_offset;        // 80
} SET_PACKED();
typedef struct srf_header_t SRF_HEADER;

#pragma pack(pop)


