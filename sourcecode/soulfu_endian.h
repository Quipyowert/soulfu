#pragma once

#include "soulfu_config.h"

#include <SDL_types.h>
#include <SDL_endian.h>

_INLINE Uint8  SF_Swap08(Uint8 val);
_INLINE Uint16 SF_Swap16(Uint16 val);
_INLINE Uint32 SF_Swap32(Uint32 val);

#define ENDIAN_READ_STREAM_16(STREAM) endian_read_mem_int16(STREAM); STREAM += sizeof(Uint16)
#define ENDIAN_READ_STREAM_32(STREAM) endian_read_mem_int32(STREAM); STREAM += sizeof(Uint32)

#define ENDIAN_WRITE_STREAM_16(STREAM, VAL) endian_write_mem_int16(STREAM, VAL); STREAM += sizeof(Uint16)
#define ENDIAN_WRITE_STREAM_32(STREAM, VAL) endian_write_mem_int32(STREAM, VAL); STREAM += sizeof(Uint32)
