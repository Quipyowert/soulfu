// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!  Stuff up here
// !!!BAD!!!
// !!!BAD!!!

#include "input.h"


KEYBOARD keyb;

KEY_BUFFER key_buff;

#define KEY_BUFF_INC(POS) { POS++; if(POS >= MAX_KEY_BUFFER) POS -= MAX_KEY_BUFFER; }
#define KEY_BUFF_DEC(POS) { POS--; if(POS <  0)              POS += MAX_KEY_BUFFER; }

MOUSE mouse;

WINDOW_INPUT_INFO last_wi =
{
    {{OBJECT_UNKNOWN, NULL}, NO_ITEM}, // FOCUS_INFO input
    0,                                 // Sint32     cursor_pos
    0                                  // Uint8      mouse
};

PLAYER_DEVICE player_device[MAX_LOCAL_PLAYER];

/* begin - poobah */
typedef struct joystick_t
{
    SDL_Joystick *sdl_ptr;         // Used for closing the joysticks...

    Uint8  num_buttons;
    Uint8 *button_pressed;
    Uint8 *button_unpressed;
    Uint8 *button_down;
    Uint8  button_pressed_last;

    Uint8  num_axes;
    float *position;

} JOYSTICK;

Sint32     joy_list_count = 0;
JOYSTICK * joy_list       = NULL;
/* end - poobah */

void input_setup_key_buffer(KEY_BUFFER * kb);
void input_setup_key_shift(KEYBOARD * pk);
void input_setup_key_transform(KEYBOARD * pk);
void input_free_joysticks(void);
void input_write_key_buffer(Uint8 keytowrite);

//-----------------------------------------------------------------------------------------------
void keyboard_setup(KEYBOARD * pk)
{
    int i;

    pk->last_pressed = 0;

    // Setup keyboard information
    repeat(i, MAX_KEY)
    {
        pk->down[i]        = kfalse;
        pk->pressed[i]     = kfalse;
        pk->unpressed[i]   = kfalse;
        pk->win_pressed[i] = kfalse;
    }

    input_setup_key_buffer(&key_buff);
    input_setup_key_shift(pk);
};

//-----------------------------------------------------------------------------------------------
void mouse_setup(MOUSE * pm)
{
    // Setup mouse information

    int i;

    focus_clear( &(pm->last_click) );
    focus_clear( &(pm->last)       );
    focus_clear( &(pm->current)    );

    pm->draw          = ktrue;
    pm->camera_active = kfalse;

    pm->text[0]    = '\0';
    pm->text_timer = 0;

    pm->alpha = 0xFF;

    pm->x = DEFAULT_W * 0.5f;
    pm->y = DEFAULT_H * 0.5f;

    pm->last_x = pm->x;
    pm->last_y = pm->y;

    repeat(i, MAX_MOUSE_BUTTON)
    {
        pm->down[i]      = kfalse;
        pm->pressed[i]   = kfalse;
        pm->unpressed[i] = kfalse;
    }

    pm->idle_timer = 0;
}

//-----------------------------------------------------------------------------------------------
bool_t joystick_setup()
{
    // <poobah> modification to allow a variable number of joysticks
    //          to have SDL tell us the caps of the devices

    int i, j;

    if (0 != joy_list_count)
    {
        input_free_joysticks();
    };

    // Turn on the joysticks
    joy_list_count = SDL_NumJoysticks();

    if (joy_list_count <= 0)
    {
        // No joysticks detected
        log_info(0, "No joysticks detected");
        return kfalse;
    }

    // Allocate memory for each joystick
    joy_list = (JOYSTICK*)calloc(joy_list_count, sizeof(JOYSTICK));

    // Now check to see if it allocated properly
    if (joy_list_count > 0 && NULL == joy_list)
    {
        log_error(0, "joystick_setup() : could not allocate memory for joysticks.");
        return kfalse;
    }

    // Turn on the joysticks
    SDL_JoystickEventState(SDL_ENABLE);
    log_info(0, "Turning on %d joysticks...", joy_list_count);
    repeat(i, joy_list_count)
    {
        log_info(0, "%d...  %s", i, SDL_JoystickName(i));
        joy_list[i].sdl_ptr = SDL_JoystickOpen(i);
    }
    atexit(input_free_joysticks);

    // Allocate more memory for each button and axis, and set default values
    repeat(i, joy_list_count)
    {
        joy_list[i].button_pressed_last = 0xFF;

        joy_list[i].num_buttons      = SDL_JoystickNumButtons(joy_list[i].sdl_ptr);
        joy_list[i].button_pressed   = (Uint8 *)calloc(joy_list[i].num_buttons, sizeof(Uint8));
        joy_list[i].button_unpressed = (Uint8 *)calloc(joy_list[i].num_buttons, sizeof(Uint8));
        joy_list[i].button_down      = (Uint8 *)calloc(joy_list[i].num_buttons, sizeof(Uint8));

        // Check for failed allocations
        if (joy_list[i].num_buttons > 0)
        {
            if (NULL == joy_list[i].button_down || NULL == joy_list[i].button_unpressed || NULL == joy_list[i].button_unpressed)
            {
                return kfalse;
            };

            repeat(j, joy_list[i].num_buttons)
            {
                joy_list[i].button_pressed[j] = kfalse;
                joy_list[i].button_unpressed[j] = kfalse;
                joy_list[i].button_down[j] = kfalse;
            }
        }


        joy_list[i].num_axes         = SDL_JoystickNumAxes(joy_list[i].sdl_ptr);
        joy_list[i].position         = (float *)calloc(joy_list[i].num_axes, sizeof(float));

        if (joy_list[i].num_axes > 0)
        {
            if (NULL == joy_list[i].position)
            {
                return kfalse;
            }

            repeat(j, joy_list[i].num_axes)
            {
                joy_list[i].position[j] = 0.0f;
            }
        }

    }

    return ktrue;
}

//-----------------------------------------------------------------------------------------------
void input_setup_key_buffer(KEY_BUFFER * kb)
{
    // <ZZ> This function deletes all data in the key_buff.buffer
    int i;
    key_buff.read = 0;
    key_buff.write = 0;
    repeat(i, MAX_KEY_BUFFER)
    {
        key_buff.buffer[i] = 0;
    }
}

//-------------------------------------------------------------------------------------------
void input_setup_key_shift(KEYBOARD * pk)
{
    // <ZZ> This function sets up a lookup table.  The table converts an SDLK_
    //      value into the corresponding ASCII value for a shifted key.
    int i;


    // Fill the table
    repeat(i, MAX_ASCII)
    {
        // Default value
        pk->shift[i] = toupper(i);
    }

    // Shifted numbers
    pk->shift['0'] = ')';
    pk->shift['1'] = '!';
    pk->shift['2'] = '@';
    pk->shift['3'] = '#';
    pk->shift['4'] = '$';
    pk->shift['5'] = '%';
    pk->shift['6'] = '^';
    pk->shift['7'] = '&';
    pk->shift['8'] = '*';
    pk->shift['9'] = '(';
    // Other symbols
    pk->shift['`'] = '~';
    pk->shift['-'] = '_';
    pk->shift['='] = '+';
    pk->shift['['] = '{';
    pk->shift[']'] = '}';
    pk->shift[';'] = ':';
    pk->shift[','] = '<';
    pk->shift['.'] = '>';
    pk->shift['/'] = '?';
    pk->shift['\''] = '\"';
    pk->shift['\\'] = '|';
}

/* begin - poobah */
//-------------------------------------------------------------------------------------------
#ifdef WIN32
void input_setup_key_transform(KEYBOARD * pk)
{
    // <ZZ> This function sets up a lookup table.  The table converts a scancode
    //      value into the corresponding ASCII value on the user's current
    //      keyboard layout.
    int i;
    Uint8 state[0x0100];
    Uint16 result;
    HKL layout = GetKeyboardLayout(0);

    memset(state, 0, 0x0100);

    // Fill the table
    repeat(i, MAX_SCANCODE)
    {
        if (ToAsciiEx(MapVirtualKeyEx(i, 1, layout), i, state, &result, 0, layout) == 1)
            pk->transform[i] = (Uint8)result;
        else if (i == 83)
            pk->transform[83] = 0x7F; //Windows doesn't map the DEL key to an ASCII value, so it's done manually here
        else
            pk->transform[i] = ' '; //Or some other default value
    }
}
#endif

//-------------------------------------------------------------------------------------------
void input_free_joysticks(void)
{
    // <ZZ> This function should be called automatically when the program ends...  Just
    //      silly SDL stuff to free up memory.
    int i;

    if (NULL == joy_list) return;

    repeat(i, joy_list_count)
    {
        if (joy_list[i].sdl_ptr != NULL)
        {
            SDL_JoystickClose(joy_list[i].sdl_ptr);
        }

        if (NULL != joy_list[i].button_pressed)
        {
            free(joy_list[i].button_pressed);
            joy_list[i].button_pressed = NULL;
        }

        if (NULL != joy_list[i].button_unpressed)
        {
            free(joy_list[i].button_unpressed);
            joy_list[i].button_unpressed = NULL;
        };

        if (NULL != joy_list[i].button_down)
        {
            free(joy_list[i].button_down);
            joy_list[i].button_down = NULL;
        }

        if (joy_list[i].position)
        {
            free(joy_list[i].position);
            joy_list[i].position = NULL;
        }
    }

    if (NULL != joy_list)
    {
        free(joy_list);
        joy_list = NULL;
        joy_list_count = 0;
    };
}
/* end - poobah */

//-------------------------------------------------------------------------------------------
void input_update(void)
{
    // <ZZ> This function helps in doing the mouse/window interface & stuff...

    Uint16 i, j, button;
    Uint16 joystick;


    // For WindowInput()...  Clicking outside the input box unsets it...
    if (mouse.pressed[BUTTON0])
    {
        if ( !focus_equal(&(mouse.current), &(last_wi.input)) )
        {
            focus_clear(&(last_wi.input));
        }
    }

    if (input_active > 0)
    {
        input_active--;
    }

    if (mouse.idle_timer < UINT16_MAX)
    {
        mouse.idle_timer++;
    }


    // For all the window functions...
    if (mouse.pressed[BUTTON0])
    {
        focus_copy(&(mouse.last_click), &(mouse.current));
    }

    focus_copy(&(mouse.last), &(mouse.current));
    focus_clear(&(mouse.current));

    mouse.last_x = mouse.x;
    mouse.last_y = mouse.y;

    if (mouse.character_distance > 999999.0f)
    {
        i = mouse.text_timer;
        i -= main_frame_skip;

        if (i > 255) { mouse.text_timer = 0; }
        else { mouse.text_timer = (Uint8) i; }
    }

    mouse.character_distance = 1000000.0f;
    repeat(i, MAX_MOUSE_BUTTON)
    {
        mouse.pressed[i] = 0;
        mouse.unpressed[i] = 0;
    }


    // For player controls...
    repeat(i, MAX_LOCAL_PLAYER)
    {
        player_device[i].inventory_toggle = kfalse;
        player_device[i].inventory_down = kfalse;

        if (player_device[i].type > 1)
        {
            // Joystick controls...
            joystick = player_device[i].type - 2;
            player_device[i].xy[XX] = joy_list[joystick].position[XX];
            player_device[i].xy[YY] = joy_list[joystick].position[YY];


            // Fill in button presses...  Only set 'em (don't unset 'em here...  unset when used...)
            repeat(j, 4)
            {
                button = player_device[i].button[j];
                player_device[i].button_pressed[j] = kfalse;
                player_device[i].button_unpressed[j] = kfalse;

                if (button < joy_list[joystick].num_buttons)
                {
                    player_device[i].button_pressed[j] |= joy_list[joystick].button_pressed[button];

                    if (joy_list[joystick].button_pressed[button] && player_device[i].button_unpressed[j])
                    {
                        player_device[i].button_unpressed[j] = kfalse;
                    }

                    player_device[i].button_unpressed[j] |= joy_list[joystick].button_unpressed[button];
                }
            }
            button = player_device[i].button[PLAYER_DEVICE_BUTTON_ITEMS];

            if (button < joy_list[joystick].num_buttons)
            {
                player_device[i].inventory_toggle = joy_list[joystick].button_pressed[button];
                player_device[i].inventory_down = joy_list[joystick].button_down[button];
            }
        }
        else
        {
            player_device[i].xy[XX] = 0.0f;
            player_device[i].xy[YY] = 0.0f;

            if (player_device[i].type == PLAYER_DEVICE_KEYBOARD)
            {
                if (global_block_keyboard_timer > 0 || input_active > 0)
                {
                    // Keyboard controls don't work right now...  (Rogue book window is open or typing in a message window...)
                    repeat(j, 4)
                    {
                        player_device[i].button_unpressed[j] = kfalse;

                        if (player_device[i].button_pressed[j])
                        {
                            player_device[i].button_unpressed[j] = ktrue;
                        }

                        player_device[i].button_pressed[j] = kfalse;
                    }
                }
                else
                {
                    // Keyboard controls...
                    if (keyb.down[player_device[i].button[PLAYER_DEVICE_BUTTON_MOVE_UP] % MAX_KEY])
                    {
                        player_device[i].xy[YY] -= 1.0f;
                    }

                    if (keyb.down[player_device[i].button[PLAYER_DEVICE_BUTTON_MOVE_DOWN] % MAX_KEY])
                    {
                        player_device[i].xy[YY] += 1.0f;
                    }

                    if (keyb.down[player_device[i].button[PLAYER_DEVICE_BUTTON_MOVE_LEFT] % MAX_KEY])
                    {
                        player_device[i].xy[XX] -= 1.0f;
                    }

                    if (keyb.down[player_device[i].button[PLAYER_DEVICE_BUTTON_MOVE_RIGHT] % MAX_KEY])
                    {
                        player_device[i].xy[XX] += 1.0f;
                    }


                    // Button presses...
                    repeat(j, 4)
                    {
                        button = player_device[i].button[j] % MAX_KEY;
                        player_device[i].button_pressed[j] |= keyb.pressed[button]  ? 1 : 0;

                        if (keyb.pressed[button] && player_device[i].button_unpressed[j])
                        {
                            player_device[i].button_unpressed[j] = kfalse;
                        }

                        player_device[i].button_unpressed[j] |= keyb.unpressed[button] ? 1 : 0;
                    }
                    button = player_device[i].button[PLAYER_DEVICE_BUTTON_ITEMS] % MAX_KEY;
                    player_device[i].inventory_toggle = keyb.pressed[button];
                    player_device[i].inventory_down = keyb.down[button];
                }
            }
        }

        player_device[i].button_pressed_copy[0] = player_device[i].button_pressed[0];
        player_device[i].button_pressed_copy[1] = player_device[i].button_pressed[1];
        player_device[i].button_pressed_copy[2] = player_device[i].button_pressed[2];
        player_device[i].button_pressed_copy[3] = player_device[i].button_pressed[3];
    }


    // Block keyboard input for players...
    if (global_block_keyboard_timer > 0)
    {
        global_block_keyboard_timer--;
    }
}

//-------------------------------------------------------------------------------------------
Sint8 input_setup(void)
{
    // <ZZ> This function sets up all of our Human Interface Devices...

    int i;

    // setup keyboard
    keyboard_setup(&keyb);

    // setup the mouse
    mouse_setup(&mouse);

    // Turn on any joysticks
    joystick_setup();

    // Turn off all of the player devices...
    repeat(i, MAX_LOCAL_PLAYER)
    {
        player_device[i].type = PLAYER_DEVICE_NONE;
        player_device[i].inventory_toggle = kfalse;
        player_device[i].inventory_down = kfalse;
        player_device[i].controls_active = ktrue;
        local_player_character[i] = UINT16_MAX;
    }

    return ktrue;
}

//-------------------------------------------------------------------------------------------
Uint8 input_read_key_buffer(void)
{
    // <ZZ> This function returns the next character in the key_buff.buffer, or 0 if there is
    //      is no data
    Uint8 returnvalue;


    // Is there data?
    returnvalue = 0;

    if (key_buff.read != key_buff.write)
    {
        returnvalue = key_buff.buffer[key_buff.read];
        KEY_BUFF_INC(key_buff.read);
    }

    return returnvalue;
}

//-------------------------------------------------------------------------------------------
void input_write_key_buffer(Uint8 keytowrite)
{
    // <ZZ> This function adds a character to the key_buff.buffer
    KEY_BUFF_INC(key_buff.write);

    if (key_buff.read == key_buff.write)
    {
        // Wasn't room in the buffer, so back up
        KEY_BUFF_DEC(key_buff.write);
    }
    else
    {
        // Tested okay, so backup and add the key
        KEY_BUFF_DEC(key_buff.write);
        key_buff.buffer[key_buff.write] = keytowrite;
        KEY_BUFF_INC(key_buff.write);
    }
}

//-------------------------------------------------------------------------------------------
void input_reset_window_key_pressed(void)
{
    // <ZZ> This function clears out all of the keypresses for windows
    int i;

    // Unpress all of the keys
    repeat(i, MAX_KEY)
    {
        keyb.win_pressed[i] = kfalse;
    }
}


//-----------------------------------------------------------------------------------------------
void input_read(void)
{
    // <ZZ> This function asks SDL what's happening, and translates that into something more
    //      useful.  It fills up lots of input arrays...  This new version will hopefully
    //      not have the same trouble with stuck keys...
    SDL_Event event;
    Uint8 button_state, button_down;
    Uint16 i, j, num_keys;
    Uint16 key;
    Uint8* key_state;
    int temp;

    // Unpress all of the keys
    repeat(i, MAX_KEY)
    {
        keyb.pressed[i] = kfalse;
        keyb.unpressed[i] = kfalse;
    }


    // Unpress all of the joystick buttons...
    repeat(i, joy_list_count)
    {
        repeat(j, joy_list[i].num_buttons)
        {
            joy_list[i].button_pressed[j]   = kfalse;
            joy_list[i].button_unpressed[j] = kfalse;
        }
    }



    // Handle events that we've gotten since last update...
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {

            case SDL_KEYDOWN:
                key = (Uint16) event.key.keysym.sym;

                if (key < MAX_KEY)
                {
                    if (!keyb.down[key])
                    {
                        keyb.down[key]        = ktrue;
                        keyb.pressed[key]     = ktrue;
                        keyb.win_pressed[key] = ktrue;

                        // Remember the key...
                        keyb.last_pressed = key;
                    }


                    // If it's an ASCII character, add it to the key_buff.buffer...
                    if ((key < MAX_ASCII && key >= ' ') || key == SDLK_BACKSPACE || key == SDLK_RETURN)
                    {
                        if (event.key.keysym.mod & (KMOD_LSHIFT | KMOD_RSHIFT))
                        {
                            // Write a shifted character
                            input_write_key_buffer(keyb.shift[key]);
                        }
                        else
                        {
                            // Write a lowercase character
                            input_write_key_buffer((Uint8) key);
                        }
                    }
                }

                break;

            case SDL_KEYUP:
                key = (Uint16) event.key.keysym.sym;

                if (key < MAX_KEY)
                {
                    if (keyb.down[key])
                    {
                        keyb.down[key]      = kfalse;
                        keyb.unpressed[key] = ktrue;
                    }
                }

                break;

            case SDL_QUIT:
                quitgame = ktrue;
                break;

            case SDL_MOUSEMOTION:
                mouse.idle_timer = 0;

                if (display_full_screen)
                {
                    mouse.x += event.motion.xrel * 0.625f;
                    mouse.y += event.motion.yrel * 0.625f;
                    CLIP(0.0f, mouse.x, CAST(float, DEFAULT_W));
                    CLIP(0.0f, mouse.y, CAST(float, DEFAULT_H));
                }
                else
                {
                    mouse.x = event.motion.x * CAST(float, DEFAULT_W) / screen_x;
                    mouse.y = event.motion.y * CAST(float, DEFAULT_H) / screen_y;
                }

                break;

            case SDL_JOYAXISMOTION:

                if (event.jaxis.which < joy_list_count)
                {
                    if (event.jaxis.axis < joy_list[event.jaxis.which].num_axes)
                    {
                        joy_list[event.jaxis.which].position[event.jaxis.axis] = 0.0f;

                        if (event.jaxis.value < -JOY_TOLERANCE)
                        {
                            joy_list[event.jaxis.which].position[event.jaxis.axis] = (event.jaxis.value + JOY_TOLERANCE) / (((float)0x8000) - JOY_TOLERANCE);
                        }
                        else if (event.jaxis.value > JOY_TOLERANCE)
                        {
                            joy_list[event.jaxis.which].position[event.jaxis.axis] = (event.jaxis.value - JOY_TOLERANCE) / (((float)0x8000) - JOY_TOLERANCE);
                        }
                    }
                }

                break;

            case SDL_JOYBUTTONUP:

                if (event.jbutton.which < joy_list_count)
                {
                    if (event.jbutton.button < joy_list[event.jbutton.which].num_buttons)
                    {
                        joy_list[event.jbutton.which].button_unpressed[event.jbutton.button] = ktrue;
                        joy_list[event.jbutton.which].button_down[event.jbutton.button] = kfalse;
                    }
                }

                break;

            case SDL_JOYBUTTONDOWN:

                if (event.jbutton.which < joy_list_count)
                {
                    if (event.jbutton.button < joy_list[event.jbutton.which].num_buttons)
                    {
                        joy_list[event.jbutton.which].button_pressed[event.jbutton.button] = ktrue;
                        joy_list[event.jbutton.which].button_down[event.jbutton.button] = ktrue;
                        joy_list[event.jbutton.which].button_pressed_last = event.jbutton.button;
                    }
                }

                break;

            case SDL_MOUSEBUTTONUP:
                mouse.idle_timer = 0;
                i = (event.button.button - 1);

                if (i < MAX_MOUSE_BUTTON)
                {
                    if (mouse.down[i])
                    {
                        mouse.unpressed[i] = ktrue;
                        mouse.down[i]      = kfalse;
                    }
                }

                break;

            case SDL_MOUSEBUTTONDOWN:
                mouse.idle_timer = 0;
                i = (event.button.button - 1);

                if (i < MAX_MOUSE_BUTTON)
                {
                    if (mouse.draw)
                    {
                        if (!mouse.down[i])
                        {
                            mouse.pressed[i] = ktrue;
                            mouse.down[i]    = ktrue;
                        }
                    }
                }

                break;

            default:
                break;
        }
    }


    // Do a device state check on the keyboard every 16 drawn frames to make sure our
    // information is accurate (just in case we still get a stuck key)...
    if ((main_video_frame & 0x0F) == 0x00)
    {
        key_state = SDL_GetKeyState(&temp);
        num_keys = (Uint16) temp;

        if (num_keys > MAX_KEY)
        {
            // Shouldn't happen...
            num_keys = MAX_KEY;
        }

        repeat(key, num_keys)
        {
            if (key_state[key] != (keyb.down[key] ? 1 : 0))
            {
                if (key_state[key])
                {
                    // Key is newly pressed...
                    keyb.pressed[key] = ktrue;
                    keyb.win_pressed[key] = ktrue;
                    keyb.last_pressed = key;
                }
                else
                {
                    // Key was just let up...
                    keyb.unpressed[key] = ktrue;
                }

                keyb.down[key] = (1 == key_state[key]);
            }
        }
    }


    // Do a device state check on the mouse every 16 drawn frames to make sure our
    // information is accurate (just in case we still get a stuck button)...
    if ((main_video_frame & 0x0F) == 0x08)
    {
        button_state = SDL_GetMouseState(NULL, NULL);
        repeat(i, MAX_MOUSE_BUTTON)
        {
            button_down = (button_state >> i) & 1;

            if (button_down != (mouse.down[i] ? 1 : 0))
            {
                if (button_down)
                {
                    if (mouse.draw)
                    {
                        // Mouse button is newly pressed...
                        mouse.pressed[i] = ktrue;
                    }
                }
                else
                {
                    // Mouse button was just let up...
                    mouse.unpressed[i] = ktrue;
                }

                mouse.down[i] = (1 == button_down);
            }
        }
    }
}

//-------------------------------------------------------------------------------------------
bool_t input_camera_controls(void)
{
    // <ZZ> This function allows the players to rotate the camera and zoom in and out...
    Sint16 off_x, off_y;
    Uint8 count;
    Uint16 i;
    Uint8 joystick;
    Uint16 button;
    SDL_Event event;

    if (!play_game_active) return kfalse;

    // Make sure the mouse isn't over a window...
    if (mouse.last.object.unk == NULL)
    {
        // Also that camera control isn't turn'd off...
        if (mouse.camera_active)
        {
            if (mouse.down[BUTTON0] || mouse.down[BUTTON1] || mouse.down[BUTTON2])
            {
                // Motion...
                off_x = (Sint16) (mouse.x - mouse.last_x);
                off_y = (Sint16) (mouse.y - mouse.last_y);


                // Keep the mouse in one spot...
                SDL_WarpMouse((Uint16) (mouse.last_x * screen_x / CAST(float, DEFAULT_W)), (Uint16) (mouse.last_y * screen_y / CAST(float, DEFAULT_H)));
                mouse.x = mouse.last_x;
                mouse.y = mouse.last_y;

                // Flush out that last mouse event...
                while (SDL_PollEvent(&event));


                // Count number of buttons pressed...
                count = 0;

                if (mouse.down[BUTTON0]) count++;

                if (mouse.down[BUTTON1]) count++;

                if (mouse.down[BUTTON2]) count++;

                if (mouse.down[BUTTON3]) count++;


                // Zoom control if two buttons held...
                if (count > 1)
                {
                    // Zoom control...
                    camera.to_distance += (off_y * CAMERA_ZOOM_RATE);

                    if (camera.to_distance < MIN_CAMERA_DISTANCE)
                    {
                        camera.to_distance = MIN_CAMERA_DISTANCE;
                    }

                    if (camera.to_distance > MAX_CAMERA_DISTANCE)
                    {
                        camera.to_distance = MAX_CAMERA_DISTANCE;
                    }
                }
                else
                {
                    // Rotational control...
                    camera.rotation_add_xy[XX] += (Sint32) (off_x * CAMERA_ROTATION_RATE);
                    camera.rotation_add_xy[YY] -= (Sint32) (off_y * CAMERA_ROTATION_RATE);
                }
            }
            else
            {
                // No buttons down means we're done moving the camera...
                mouse.camera_active = kfalse;
            }
        }
        else
        {
            // Check for initiation of mouse camera control (clicking on empty area)...
            if (mouse.pressed[BUTTON0] || mouse.pressed[BUTTON1] || mouse.pressed[BUTTON2])
            {
                // Allow camera movement next time...
                mouse.camera_active = ktrue;
            }
        }
    }


    // Allow special +/- zoom controls...  Also 7 and 9 on keypad for rotation...
    if (input_active == 0 && global_block_keyboard_timer == 0)
    {
        // Zoom in
        if (keyb.down[SDLK_KP_PLUS] || keyb.down[SDLK_PLUS] || keyb.down[SDLK_EQUALS])
        {
            camera.to_distance -= (CAMERA_ZOOM_RATE * main_frame_skip);

            if (camera.to_distance < MIN_CAMERA_DISTANCE)
            {
                camera.to_distance = MIN_CAMERA_DISTANCE;
            }
        }


        // Zoom out
        if (keyb.down[SDLK_KP_MINUS] || keyb.down[SDLK_MINUS])
        {
            camera.to_distance += (CAMERA_ZOOM_RATE * main_frame_skip);

            if (camera.to_distance > MAX_CAMERA_DISTANCE)
            {
                camera.to_distance = MAX_CAMERA_DISTANCE;
            }
        }


        // Rotation
        off_x = 0;

        if (keyb.down[SDLK_KP7]) { off_x += 3; }

        if (keyb.down[SDLK_KP9]) { off_x -= 3; }

        camera.rotation_add_xy[XX] += (Sint32) (off_x * CAMERA_ROTATION_RATE * main_frame_skip);
    }


    // Allow joystick players to move camera too...
    repeat(i, MAX_LOCAL_PLAYER)
    {
        // Is the player a joystick player?
        if (player_device[i].type > PLAYER_DEVICE_KEYBOARD)
        {
            // Yup...  Check buttons
            joystick = player_device[i].type - PLAYER_DEVICE_JOYSTICK_1;

            // Zoom in button...
            button = player_device[i].button[PLAYER_DEVICE_BUTTON_MOVE_UP];

            if (button < joy_list[joystick].num_buttons)
            {
                if (joy_list[i].button_down[button])
                {
                    camera.to_distance -= (CAMERA_ZOOM_RATE * main_frame_skip);

                    if (camera.to_distance < MIN_CAMERA_DISTANCE)
                    {
                        camera.to_distance = MIN_CAMERA_DISTANCE;
                    }
                }
            }


            // Zoom out button...
            button = player_device[i].button[PLAYER_DEVICE_BUTTON_MOVE_DOWN];

            if (button < joy_list[joystick].num_buttons)
            {
                if (joy_list[i].button_down[button])
                {
                    camera.to_distance += (CAMERA_ZOOM_RATE * main_frame_skip);

                    if (camera.to_distance > MAX_CAMERA_DISTANCE)
                    {
                        camera.to_distance = MAX_CAMERA_DISTANCE;
                    }
                }
            }


            // Rotate buttons...
            off_x = 0;
            button = player_device[i].button[PLAYER_DEVICE_BUTTON_MOVE_LEFT];

            if (button < joy_list[joystick].num_buttons) { if (joy_list[i].button_down[button]) { off_x += 3; } }

            button = player_device[i].button[PLAYER_DEVICE_BUTTON_MOVE_RIGHT];

            if (button < joy_list[joystick].num_buttons) { if (joy_list[i].button_down[button]) { off_x -= 3; } }

            camera.rotation_add_xy[XX] += (Sint32)(off_x * CAMERA_ROTATION_RATE * main_frame_skip);
        }
    }

    return ktrue;
}

////-------------------------------------------------------------------------------------------
//// <ZZ> Old input_read() with stuck keys
//void input_read(void)
//{
//    // <ZZ> This function asks SDL what's happening, and translates that into something more
//    //      useful.  It fills up lots of input arrays...
//    SDL_Event event;
//    int i, j, num_event;
//
//
//
//    // Unpress all of the keys
//    repeat(i, MAX_KEY)
//    {
//        keyb.pressed[i] = kfalse;
//        keyb.unpressed[i] = kfalse;
//    }
//
//
//    // Unpress all of the joystick buttons...
//    repeat(i, joy_list_count)
//    {
//        repeat(j, MAX_JOYSTICK_BUTTON)
//        {
//            joy_list[i].button_pressed[j] = kfalse;
//            joy_list[i].button_unpressed[j] = kfalse;
//        }
//    }
//
//
//    // Handle events that we've gotten since last update...
//    while(SDL_PollEvent(&event))
//    {
//        switch(event.type)
//        {
//        case SDL_KEYDOWN:
//            keyb.down[event.key.keysym.sym] = ktrue;
//            keyb.pressed[event.key.keysym.sym] = ktrue;
//            keyb.win_pressed[event.key.keysym.sym] = ktrue;
//
//
//            // Remember the key...
//            keyb.last_pressed = (Uint16) event.key.keysym.sym;
//
//
//            // If it's an ASCII character, add it to the key_buff.buffer...
//            if((event.key.keysym.sym < MAX_ASCII && event.key.keysym.sym >= ' ') || event.key.keysym.sym == SDLK_BACKSPACE || event.key.keysym.sym == SDLK_RETURN)
//            {
//                if(event.key.keysym.mod & (KMOD_LSHIFT | KMOD_RSHIFT))
//                {
//                    // Write a shifted character
//                    input_write_key_buffer(keyb.shift[(Uint8) event.key.keysym.sym]);
//                }
//                else
//                {
//                    // Write a lowercase character
//                    input_write_key_buffer((Uint8) event.key.keysym.sym);
//                }
//            }
//            // !!!BAD!!!
//            // !!!BAD!!!
//            // !!!BAD!!!
//            //                #ifdef DEVTOOL
//            // !!!BAD!!!
//            // !!!BAD!!!
//            // !!!BAD!!!
//            if(event.key.keysym.sym == SDLK_ESCAPE)
//            {
//                quitgame = ktrue;
//                log_info(0, "Escape key pressed (DEVTOOL only)");
//            }
//            // !!!BAD!!!
//            // !!!BAD!!!
//            // !!!BAD!!!
//            //                #endif
//            // !!!BAD!!!
//            // !!!BAD!!!
//            // !!!BAD!!!
//            break;
//        case SDL_KEYUP:
//            keyb.down[event.key.keysym.sym] = kfalse;
//            keyb.unpressed[event.key.keysym.sym] = ktrue;
//            break;
//        case SDL_QUIT:
//            quitgame = ktrue;
//            break;
//        case SDL_MOUSEMOTION:
//            if(display_full_screen)
//            {
//                mouse.x += event.motion.xrel * 0.625f;
//                mouse.y += event.motion.yrel * 0.625f;
//                CLIP(0.0f, mouse.x, CAST(float, DEFAULT_W));
//                CLIP(0.0f, mouse.y, CAST(float, DEFAULT_H));
//            }
//            else
//            {
//                mouse.x = event.motion.x * CAST(float, DEFAULT_W) / screen_x;
//                mouse.y = event.motion.y * CAST(float, DEFAULT_H) / screen_y;
//            }
//            break;
//        case SDL_JOYAXISMOTION:
//            if(event.jaxis.which < MAX_JOYSTICK)
//            {
//                if(event.jaxis.axis == X || event.jaxis.axis == Y)
//                {
//                    joystick_position_xy[event.jaxis.which][event.jaxis.axis] = 0.0f;
//                    if(event.jaxis.value < -JOY_TOLERANCE)
//                    {
//                        joystick_position_xy[event.jaxis.which][event.jaxis.axis] = (event.jaxis.value+JOY_TOLERANCE)/(32768.0f-JOY_TOLERANCE);
//                    }
//                    else if(event.jaxis.value > JOY_TOLERANCE)
//                    {
//                        joystick_position_xy[event.jaxis.which][event.jaxis.axis] = (event.jaxis.value-JOY_TOLERANCE)/(32768.0f-JOY_TOLERANCE);
//                    }
//                }
//            }
//            break;
//        case SDL_JOYBUTTONUP:
//            if(event.jbutton.which < MAX_JOYSTICK)
//            {
//                if(event.jbutton.button < MAX_JOYSTICK_BUTTON)
//                {
//                    joy_list[event.jbutton.which].button_unpressed[event.jbutton.button] = ktrue;
//                    joy_list[event.jbutton.which].button_down[event.jbutton.button] = kfalse;
//                }
//            }
//            break;
//        case SDL_JOYBUTTONDOWN:
//            if(event.jbutton.which < MAX_JOYSTICK)
//            {
//                if(event.jbutton.button < MAX_JOYSTICK_BUTTON)
//                {
//                    joy_list[event.jbutton.which].button_pressed[event.jbutton.button] = ktrue;
//                    joy_list[event.jbutton.which].button_down[event.jbutton.button] = ktrue;
//                }
//            }
//            break;
//        case SDL_MOUSEBUTTONUP:
//        case SDL_MOUSEBUTTONDOWN:
//            i = (event.button.button-1);
//            if(i < MAX_MOUSE_BUTTON)
//            {
//                if(event.button.state == SDL_PRESSED)
//                {
//                    if(mouse.draw)
//                    {
//                        mouse.pressed[i] = ktrue;
//                        mouse.down[i] = ktrue;
//                    }
//                }
//                if(event.button.state == SDL_RELEASED)
//                {
//                    mouse.unpressed[i] = ktrue;
//                    mouse.down[i] = kfalse;
//                }
//            }
//            break;
//        default:
//            break;
//        }
//    }
//}