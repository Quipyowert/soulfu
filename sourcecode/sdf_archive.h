#pragma once

#include "soulfu_types.h"

#include "soulfu_endian.inl"

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

typedef Uint8 * SDF_PDATA;

struct sdf_header_t;
typedef struct sdf_header_t *SDF_PHEADER;

struct sf_stream_t;
typedef struct sf_stream_t  * SF_PSTREAM;

//-----------------------------------------------------------------------------------------------

struct sdf_stream_t
{
    SDF_PDATA       read;
    SDF_PDATA       read_beg;
    SDF_PDATA       read_end;
    int             line;
    bool_t          started;
};
typedef struct sdf_stream_t SDF_STREAM;

//extern SDF_STREAM sdf_stream;

//-----------------------------------------------------------------------------------------------

#define INVALID_SIZE  (~(size_t)0)
#define SDF_INVALID_INDEX (~(size_t)0)

#define SDF_INSTRUMENT      '-' // .OGG files prefixed with this are treated as instruments
#define SDF_NO_ECHO         '=' // .OGG files prefixed with this are not given an echo
#define SDF_ECHO_LOOP       '+' // .OGG files prefixed with this are echoed for loop playback
#define SDF_ECHO            'E' // Not a prefix


#define SDF_EXTRA_INDEX      0x800  // Allow this many files to be sdf_archive_add_file()'d to the index

enum sdf_flags_e
{
    SDF_FLAG_NO_SAVE     = 1 << (0 + 4), // Upper 4 bits...
    SDF_FLAG_WAS_CREATED = 1 << (1 + 4), // Upper 4 bits...
    SDF_FLAG_NO_UPDATE   = 1 << (2 + 4), // Upper 4 bits...
    SDF_FLAG_WAS_UPDATED = 1 << (3 + 4), // Upper 4 bits...
    SDF_FLAG_INVALID     = SDF_FLAG_NO_UPDATE | SDF_FLAG_WAS_UPDATED,
    SDF_FLAG_ALL         = 255 // We're covering SDF_FILE_TYPE as well...
};
typedef enum sdf_flags_e SDF_FLAGS;

enum sdf_file_type_e
{
    SDF_FILE_IS_UNUSED = 0,  // Lower 4 bits...  File was deleted (don't save index to disk)
    SDF_FILE_IS_TXT,         // Lower 4 bits...  File is a text or random naming file
    SDF_FILE_IS_JPG,         // Lower 4 bits...  File is a JPEG texture
    SDF_FILE_IS_OGG,         // Lower 4 bits...  File is an Ogg Vorbis sound
    SDF_FILE_IS_RGB,         // Lower 4 bits...  File is a decompressed texture (don't save)
    SDF_FILE_IS_RAW,         // Lower 4 bits...  File is a decompressed sound (don't save)
    SDF_FILE_IS_SRF,         // Lower 4 bits...  File is a Supercool Room Format File
    SDF_FILE_IS_MUS,         // Lower 4 bits...  File is a Music file (my format)
    SDF_FILE_IS_DAT,         // Lower 4 bits...  File is a Data file (raw data)
    SDF_FILE_IS_SRC,         // Lower 4 bits...  File is a script source file
    SDF_FILE_IS_RUN,         // Lower 4 bits...  File is a compiled script file
    SDF_FILE_IS_PCX,         // Lower 4 bits...  File is a PCX texture
    SDF_FILE_IS_LAN,         // Lower 4 bits...  File is a Language text file
    SDF_FILE_IS_DDD,         // Lower 4 bits...  File is a 3D model file
    SDF_FILE_IS_RDY,         // Lower 4 bits...  File is a 3D model file, ready for use
    SDF_FILE_IS_INT,         // Lower 4 bits...  File is some intemediate file (probably a headerized SRC file)
    SDF_FILETYPE_COUNT       // The number of defined extensions
};
typedef enum sdf_file_type_e SDF_FILE_TYPE;

#define TEXT_SIZE 0x00020000

//-----------------------------------------------------------------------------------------------

#define EXT_TYPE_MASK   ((Uint8)(0x0F))
#define EXT_FLAGS_MASK   ((Uint8)(0xF0))
#define EXT_SIZE_24_MASK ((Uint32)(0x00FFFFFF))

//-----------------------------------------------------------------------------------------------

struct sdf_extension_info_t
{
    char          extension[4]; // The extension strings that the program recognizes
    SDF_FILE_TYPE derived;      // The type of file that this file generates
    bool_t        need_update;  // What file types need to be updated (unless flag set)
    bool_t        should_save;  // What file types should save with sdf_archive_save()
    size_t        min_size;     // The minimum file size in DEVTOOL mode...
};
typedef struct sdf_extension_info_t SDF_EXTENSION_INFO, *SDF_PEXTENSION_INFO;

//-----------------------------------------------------------------------------------------------

//---- Archive management ----
bool_t  sdf_archive_create(Uint32 num_files);
bool_t  sdf_archive_load(const char *archive_filename);
void    sdf_archive_unload(void);
bool_t  sdf_archive_save(const char* filename);
void    sdf_archive_delete_all(Uint8 type_to_delete, const char* prefix_string);
void    sdf_archive_list_all(Uint8 type_to_list, const char* prefix_string, Uint8 export_too);
bool_t  sdf_archive_delete_file(const char* filename, SDF_FILE_TYPE filetype);
bool_t  sdf_archive_add_file(const char* sdf_filename, const char* input_filename);
bool_t  sdf_archive_add_new_file(const char* filename);
bool_t  sdf_archive_export_file(const char* filename, const char* filename_path);
bool_t  sdf_archive_delete_header(SDF_PHEADER pheader);

int    sdf_archive_get_num_files();
int    sdf_archive_free_file_count();

Uint32  sdf_archive_find_index_by_data(Uint8* file_start);
Uint32  sdf_archive_find_index_by_filename(const char* filename);
Uint32  sdf_archive_find_index_by_file_info(Uint8* filedata, Uint8 filetype);

bool_t  sdf_archive_get_filename(Sint32 num, char* filename, Uint8* filetype);
void    sdf_archive_set_flags(const char *filename, Uint8 flag);
void    sdf_archive_set_save(bool_t can_save);
void    sdf_archive_flag_clear(Uint8 flag);

SDF_PHEADER sdf_archive_create_header(SDF_PDATA mem, const char * filename, size_t size, SDF_FILE_TYPE type);
SDF_PHEADER sdf_archive_get_header(Uint32 index);
SDF_PHEADER sdf_archive_find_header_by_data(Uint8* file_start);
SDF_PHEADER sdf_archive_find_header(const char* filename);
SDF_PHEADER sdf_archive_find_header_by_file_info(Uint8* filedata, Uint8 filetype);
SDF_PHEADER sdf_archive_find_filetype(const char* file_name, char searchtype);

SDF_PEXTENSION_INFO sdf_archive_get_ext(Uint32 index);
bool_t sdf_archive_decompose_filename(const char* file_name, char* newfile_name, SDF_FILE_TYPE* newfile_type);
void sdf_archive_delete_all_files(SDF_FILE_TYPE type_to_delete, char* prefix_string);
const char * sdf_file_get_extension(SDF_PHEADER pheader);
void sdf_archive_optimize(void);

//---- SDF Header Management ----
void    sdf_file_set_unused(SDF_PHEADER pheader);

size_t  sdf_file_get_offset(SDF_PHEADER pheader);
bool_t sdf_file_get_file_info(SDF_PHEADER pheader, char* newfile_name, Uint8* file_type);

Uint8   sdf_file_get_info_byte(SDF_PHEADER pheader);
void    sdf_file_set_info(SDF_PHEADER pheader, Uint8 info);

SDF_FILE_TYPE sdf_file_get_type(SDF_PHEADER pheader);

Uint8   sdf_file_get_flags(SDF_PHEADER pheader);
void    sdf_file_set_flags(SDF_PHEADER pheader, Uint8 flags);
void    sdf_file_add_flags(SDF_PHEADER pheader, Uint8 flags);
void    sdf_file_remove_flags(SDF_PHEADER pheader, Uint8 flags);

size_t  sdf_file_get_size(SDF_PHEADER pheader);
void    sdf_file_set_size(SDF_PHEADER pheader, size_t size);

bool_t  sdf_file_get_filename(SDF_PHEADER pheader, char* file_name, bool_t add_extension);
void    sdf_file_set_name(SDF_PHEADER pheader, const char * name);
char   *sdf_file_get_name(SDF_PHEADER pheader);


void       sdf_file_set_data(SDF_PHEADER pheader, SDF_PDATA data);
SDF_PDATA  sdf_file_get_data(SDF_PHEADER pheader);

bool_t sdf_file_export_file(SDF_PHEADER pheader, const char* filename_path);

//---- stream IO, memory ----
SDF_PSTREAM sdf_stream_init(SDF_PSTREAM pstream);
SDF_PSTREAM sdf_stream_open_mem(SDF_PSTREAM pstream, SDF_PDATA filename, size_t size);
SDF_PSTREAM sdf_stream_open_filename(SDF_PSTREAM pstream, const char* filename);
SDF_PSTREAM sdf_stream_open_header(SDF_PSTREAM pstream, SDF_PHEADER pheader);
SDF_PSTREAM sdf_stream_clone(SDF_PSTREAM pdst, SDF_PSTREAM psrc);
SDF_PSTREAM sdf_stream_rewind(SDF_PSTREAM pstream);

bool_t sdf_stream_eos(SDF_PSTREAM pstream);
bool_t sdf_stream_read_line(SDF_PSTREAM pstream);


Sint8  sdf_insert_data(Uint8* file_pos, Uint8* data_to_add, Sint32 bytes_to_add);
