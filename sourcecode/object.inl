#pragma once

#include "soulfu_config.h"
#include "soulfu_common.h"
#include "object.h"

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
extern CHR_DATA _main_chr_data[MAX_CHARACTER];

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

_INLINE FOCUS_INFO * focus_clear(FOCUS_INFO * pi);
_INLINE FOCUS_INFO * focus_set(FOCUS_INFO * pi, OBJECT_PTR *pp, Uint16 item);
_INLINE FOCUS_INFO * focus_copy(FOCUS_INFO * pi1, FOCUS_INFO * pi2);
_INLINE bool_t       focus_equal(FOCUS_INFO * pi1, FOCUS_INFO * pi2);


_INLINE CHR_DATA * chr_data_get_ptr_raw( int i );
_INLINE PRT_DATA * prt_data_get_ptr_raw( int i );
_INLINE WIN_DATA * win_data_get_ptr_raw( int i );

_INLINE CHR_DATA * chr_data_get_ptr( int i );
_INLINE PRT_DATA * prt_data_get_ptr( int i );
_INLINE WIN_DATA * win_data_get_ptr( int i );

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

// "global" inline functions

_INLINE void   ptr_clear(OBJECT_PTR * pp);
_INLINE bool_t ptr_is_type(OBJECT_PTR * pp, OBJECT_TYPE ot);
_INLINE void * ptr_get_ptr(OBJECT_PTR * pp, OBJECT_TYPE ot);
_INLINE Uint16 ptr_get_idx(OBJECT_PTR * pp, OBJECT_TYPE ot);

_INLINE Uint16 ptr_equal(OBJECT_PTR * p1, OBJECT_PTR * p2);

_INLINE OBJECT_PTR * ptr_init    (OBJECT_PTR * pp);
_INLINE OBJECT_PTR * ptr_set     (OBJECT_PTR * p1, OBJECT_PTR * p2);
_INLINE OBJECT_PTR * ptr_set_type(OBJECT_PTR * pp, OBJECT_TYPE ot, void * data);
_INLINE OBJECT_PTR * ptr_set_unk (OBJECT_PTR * pp, void * data);

// "local" inline functions

_INLINE Uint16      _get_chr_index(CHR_DATA* data);
_INLINE Uint16      _get_prt_index(PRT_DATA* data);
_INLINE Uint16      _get_win_index(WIN_DATA* data);
_INLINE OBJECT_TYPE _get_obj_type(BASE_DATA* data);

_INLINE void *      _get_obj_base_ptr(OBJECT_PTR * ptr);


//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
_INLINE FOCUS_INFO * focus_clear(FOCUS_INFO * pi)
{
    if (NULL == pi) return NULL;

    ptr_init( &(pi->object) );
    pi->item = NO_ITEM;

    return pi;
}

_INLINE FOCUS_INFO * focus_set(FOCUS_INFO * pi, OBJECT_PTR *pp, Uint16 item)
{
    if (NULL == pi || NULL == pp) return NULL;

    ptr_set( &(pi->object), pp );
    pi->item = item;

    return pi;
};

_INLINE FOCUS_INFO * focus_copy(FOCUS_INFO * pi1, FOCUS_INFO * pi2)
{
    if (NULL == pi1 || NULL == pi2) return NULL;

    ptr_set( &(pi1->object), &(pi2->object) );
    pi1->item = pi2->item;

    return pi1;
};

_INLINE bool_t focus_equal(FOCUS_INFO * pi1, FOCUS_INFO * pi2)
{
    if (pi2 == pi1) return ktrue;

    if (NULL == pi1 || NULL == pi2) return kfalse;

    if (pi1->item != pi2->item) return kfalse;

    return ptr_equal( &(pi1->object), &(pi2->object) );
};

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
_INLINE void ptr_clear(OBJECT_PTR * pp)
{
    if (NULL == pp) return;

    pp->type = OBJECT_UNKNOWN;
    pp->unk  = NULL;
}

//-----------------------------------------------------------------------------------------------
_INLINE bool_t ptr_is_type(OBJECT_PTR * pp, OBJECT_TYPE ot)
{
    if (NULL == pp) return kfalse;

    if (OBJECT_UNKNOWN == pp->type || OBJECT_BASE == pp->type)
    {
        pp->type = _get_obj_type(pp->bas);
    };

    return ot == pp->type;
};

//-----------------------------------------------------------------------------------------------
_INLINE void * ptr_get_ptr(OBJECT_PTR * pp, OBJECT_TYPE ot)
{
    if (NULL == pp) return NULL;

    if (OBJECT_UNKNOWN == pp->type || OBJECT_BASE == pp->type)
    {
        pp->type = _get_obj_type(pp->bas);
    };

    if (ot == pp->type)
    {
        return _get_obj_base_ptr(pp);
    }

    return NULL;
};

//-----------------------------------------------------------------------------------------------
_INLINE Uint16 ptr_get_idx(OBJECT_PTR * pp, OBJECT_TYPE ot)
{
    Uint16 retval = UINT16_MAX;

    if (NULL == pp) return retval;

    if (OBJECT_UNKNOWN == pp->type || OBJECT_BASE == pp->type)
    {
        pp->type = _get_obj_type(pp->bas);
    };

    if (ot == pp->type)
    {
        switch (ot)
        {
            case OBJECT_CHR: return retval = _get_chr_index(pp->chr); break;
            case OBJECT_PRT: return retval = _get_prt_index(pp->prt); break;
            case OBJECT_WIN: return retval = _get_win_index(pp->win); break;
            default: retval = UINT16_MAX; break;
        };
    }

    return retval;
};

//-----------------------------------------------------------------------------------------------
_INLINE OBJECT_PTR * ptr_init(OBJECT_PTR * pp)
{
    if (NULL == pp) return NULL;

    pp->type = OBJECT_UNKNOWN;
    pp->unk  = NULL;

    return pp;
}

//-----------------------------------------------------------------------------------------------
_INLINE OBJECT_PTR * ptr_set (OBJECT_PTR * p1, OBJECT_PTR * p2)
{
    if (NULL == p1) return NULL;

    if (NULL == p2)
    {
        ptr_init(p1);
        return NULL;
    }

    memcpy(p1, p2, sizeof(OBJECT_PTR));

    return p1;
}

//-----------------------------------------------------------------------------------------------
_INLINE OBJECT_PTR * ptr_set_type(OBJECT_PTR * pp, OBJECT_TYPE ot, void * data)
{
    if (NULL == pp) return NULL;

    pp->type = ot;
    pp->unk  = data;

    return pp;
};

//-----------------------------------------------------------------------------------------------
_INLINE OBJECT_PTR * ptr_set_unk(OBJECT_PTR * pp, void * data)
{
    if (NULL == pp) return NULL;

    pp->type = _get_obj_type( CAST(BASE_DATA*,data) );
    pp->unk  = data;

    return pp;
};

//-----------------------------------------------------------------------------------------------

#include "object.h"

_INLINE OBJECT_TYPE _get_obj_type(BASE_DATA* data)
{
    OBJECT_TYPE type = OBJECT_UNKNOWN;

    if (NULL == data) return type;

    if (data >= CAST(BASE_DATA*, _main_chr_data) && data < CAST(BASE_DATA*, _main_chr_data + MAX_CHARACTER))
    {
        type = OBJECT_CHR;
    }
    else if (data >= CAST(BASE_DATA*, main_prt_data) && data < CAST(BASE_DATA*, main_prt_data + MAX_PARTICLE))
    {
        type = OBJECT_PRT;
    }
    else if (data >= CAST(BASE_DATA*, main_win_data) && data < CAST(BASE_DATA*, main_win_data + MAX_WINDOW))
    {
        type = OBJECT_WIN;
    }

    return type;
}

//-----------------------------------------------------------------------------------------------
_INLINE void * _get_obj_base_ptr(OBJECT_PTR * ptr)
{
    void * retval = NULL;
    size_t index;

    if (NULL == ptr || NULL == ptr->unk ) return NULL;

    if( OBJECT_CHR == ptr->type )
    {
        if (ptr->unk >= CAST(void*, _main_chr_data) && ptr->unk < CAST(void*, _main_chr_data + MAX_CHARACTER))
        {
            index = ( CAST(Uint8*, ptr->unk) - CAST(Uint8*, _main_chr_data) ) / sizeof(CHR_DATA);
            retval = CAST(void*, _main_chr_data + index);
        }
    }

    if( OBJECT_PRT == ptr->type )
    {
        if (ptr->unk >= CAST(void*, main_prt_data) && ptr->unk < CAST(void*, main_prt_data + MAX_PARTICLE))
        {
            index = ( CAST(Uint8*, ptr->unk) - CAST(Uint8*, main_prt_data) ) / sizeof(PRT_DATA);
            retval = CAST(void*, main_prt_data + index);
        }
    }

    if( OBJECT_WIN == ptr->type )
    {
        if (ptr->unk >= CAST(void*, main_win_data) && ptr->unk < CAST(void*, main_win_data + MAX_WINDOW))
        {
            index = ( CAST(Uint8*, ptr->unk) - CAST(Uint8*, main_win_data) ) / sizeof(WIN_DATA);
            retval = CAST(void*, main_win_data + index);
        }
    }

    return retval;
}

//-----------------------------------------------------------------------------------------------
_INLINE Uint16 _get_chr_index(CHR_DATA* data)
{
    if (NULL == data) return UINT16_MAX;

    if (data < _main_chr_data) return UINT16_MAX;

    if (data > _main_chr_data + MAX_CHARACTER) return UINT16_MAX;

    return CAST(int, CAST(Uint8*, data) - CAST(Uint8*, _main_chr_data)) / CAST(int, CAST(Uint8*, _main_chr_data + 1) - CAST(Uint8*, _main_chr_data + 0));
};

//-----------------------------------------------------------------------------------------------
_INLINE Uint16 _get_prt_index(PRT_DATA* data)
{
    if (NULL == data) return UINT16_MAX;

    if (data < main_prt_data) return UINT16_MAX;

    if (data > main_prt_data + MAX_PARTICLE) return UINT16_MAX;

    return CAST(int, CAST(Uint8*, data) - CAST(Uint8*, main_prt_data)) / CAST(int, CAST(Uint8*, main_prt_data + 1) - CAST(Uint8*, main_prt_data + 0));
};

//-----------------------------------------------------------------------------------------------
_INLINE Uint16 _get_win_index(WIN_DATA* data)
{
    if (NULL == data) return UINT16_MAX;

    if (data < main_win_data + 0) return UINT16_MAX;

    if (data > main_win_data + MAX_WINDOW) return UINT16_MAX;

    return CAST(int, CAST(Uint8*, data) - CAST(Uint8*, main_win_data)) / CAST(int, CAST(Uint8*, main_win_data + 1) - CAST(Uint8*, main_win_data + 0));
};

//-----------------------------------------------------------------------------------------------
_INLINE Uint16 ptr_equal(OBJECT_PTR * p1, OBJECT_PTR * p2)
{
    if (p1 == p2) return ktrue;

    if (NULL == p1 || NULL == p2) return kfalse;

    return (p1->unk == p2->unk);
}






//-----------------------------------------------------------------------------------------------
_INLINE bool_t win_request_promotion(OBJECT_PTR * po)
{
    if ( !ptr_is_type(po, OBJECT_WIN) ) return kfalse;

    promotion_buffer[promotion_count++] = ptr_get_idx(po, OBJECT_WIN);

    return ktrue;
}


//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
_INLINE CHR_DATA * chr_data_get_ptr_raw( int i )
{
    if( !VALID_CHR_RANGE(i) ) return NULL;

    return _main_chr_data + i;
}

//-----------------------------------------------------------------------------------------------
_INLINE PRT_DATA * prt_data_get_ptr_raw( int i )
{
    if( !VALID_PRT_RANGE(i) ) return NULL;

    return main_prt_data + i;
}

//-----------------------------------------------------------------------------------------------
_INLINE WIN_DATA * win_data_get_ptr_raw( int i )
{
    if( !VALID_WIN_RANGE(i) ) return NULL;

    return main_win_data + i;
}

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
_INLINE CHR_DATA * chr_data_get_ptr( int i )
{
    if( !VALID_CHR_RANGE(i) ) return NULL;
    if( !_main_chr_data[i].on ) return NULL;

    return _main_chr_data + i;
}

//-----------------------------------------------------------------------------------------------
_INLINE PRT_DATA * prt_data_get_ptr( int i )
{
    if( !VALID_PRT(i) ) return NULL;

    return main_prt_data + i;
}

//-----------------------------------------------------------------------------------------------
_INLINE WIN_DATA * win_data_get_ptr( int i )
{
    if( !VALID_WIN(i) ) return NULL;

    return main_win_data + i;
}
