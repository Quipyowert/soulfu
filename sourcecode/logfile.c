// <ZZ> This file contains functions for handling the _log_file.txt file
//      log_message             - Writes a message to the log file using printf style formatting
//      log_close           - Closes the _log_file, automatically called via atexit()
//      log_setup            - Opens the _log_file

#include "logfile.h"
#include <time.h>

int log_error_count;

static FILE* _log_file;
static int   _log_verb_level = 0;

//-----------------------------------------------------------------------------------------------
static char * _log_special(char * header, char *format, va_list ap)
{
    // <ZZ> This function spits out a message to the _log_file.txt file, following standard printf
    //      style formatting.  It also logs any errors to the message buffer (for ingame tools).

    time_t log_time = time(NULL);
    struct tm* log_time_tm = localtime(&log_time);
    static char log_buffer[1024];
    char log_buffer2[1024];

    vsprintf(log_buffer2, format, ap);

    // prepend the header
    sprintf(log_buffer, "%s %02d:%02d:%02d %s", header,
        log_time_tm->tm_hour, log_time_tm->tm_min, log_time_tm->tm_sec,
        log_buffer2);

    if (_log_file)
    {
        // write to the log file
        fprintf(_log_file, "%s\n", log_buffer);
        fflush(_log_file);
    }

#ifdef _DEBUG
    // echo to the console
    fprintf(stdout,  "%s\n", log_buffer);
    fflush(stdout);
#endif

    return log_buffer;

}

//-----------------------------------------------------------------------------------------------
void log_special(int verb_level, char * header, char *format, ...)
{
    va_list ap;

    if (verb_level > _log_verb_level) return;

    va_start(ap, format);
    _log_special(header, format, ap);
    va_end(ap);
}

//-----------------------------------------------------------------------------------------------
void log_error(int verb_level, char *format, ...)
{
    char * log_buffer;
    va_list ap;

    if (verb_level > _log_verb_level) return;

    va_start(ap, format);
    log_buffer = _log_special("ERROR: ", format, ap);
    va_end(ap);

    if (log_error_count < 1)
    {
        log_buffer[64] = 0;
        message_add(log_buffer, NULL, kfalse);
    }

    log_error_count++;
};

//-----------------------------------------------------------------------------------------------
void log_fail(int verb_level, char *format, ...)
{
    char * log_buffer;
    va_list ap;

    if (verb_level > _log_verb_level) return;

    va_start(ap, format);
    log_buffer = _log_special("FAIL:  ", format, ap);
    va_end(ap);

#ifdef DEVTOOL

    if (log_error_count < 1)
    {
        log_buffer[64] = 0;
        message_add(log_buffer, NULL, kfalse);
    }

#endif

    log_error_count++;
};

//-----------------------------------------------------------------------------------------------
void log_warning(int verb_level, char *format, ...)
{
    va_list ap;

    if (verb_level > _log_verb_level) return;

    va_start(ap, format);
    _log_special("WARN:  ", format, ap);
    va_end(ap);
};

//-----------------------------------------------------------------------------------------------
void log_info(int verb_level, char *format, ...)
{
    va_list ap;

    if (verb_level > _log_verb_level) return;

    va_start(ap, format);
    _log_special("INFO:  ", format, ap);
    va_end(ap);
};

//-----------------------------------------------------------------------------------------------
void log_message(int verb_level, char *format, ...)
{
    va_list ap;

    if (verb_level > _log_verb_level) return;

    va_start(ap, format);
    _log_special("       ", format, ap);
    va_end(ap);
};

//-----------------------------------------------------------------------------------------------
void log_close(void)
{
    // <ZZ> This function closes the _log_file, and should be run automatically on program
    //      termination...
    if (_log_file)
    {
        log_info(0, "Log file closed");
        fclose(_log_file);
    }
}

//-----------------------------------------------------------------------------------------------
Sint8 log_setup(void)
{
    // <ZZ> This function opens up the LOGFILE.TXT file and registers log_close() to run
    //      on program termination.  It returns ktrue if it worked okay, kfalse if there was a
    //      problem.

    time_t    local_time;
    struct tm * pparsed_time;

    _log_verb_level = 0;
    _log_file = fopen("LOGFILE.TXT", "w");
    log_error_count = 0;

    if (_log_file)
    {
        fprintf(_log_file, "Log file started");

        local_time   = time(NULL);
        pparsed_time = localtime(&local_time);

        if (NULL != pparsed_time)
        {
            fprintf(_log_file, " - %02d:%02d:%02d on %4d/%02d/%02d",
                    pparsed_time->tm_hour, pparsed_time->tm_min, pparsed_time->tm_sec,
                    pparsed_time->tm_year + 1900, pparsed_time->tm_mon, pparsed_time->tm_mday);
        }

        fprintf(_log_file, "\n");
        fflush(_log_file);

        atexit(log_close);
        return ktrue;
    }

    return kfalse;
}

//-----------------------------------------------------------------------------------------------
void log_set_verb_level(int verb_level)
{
    _log_verb_level = verb_level;
};

//-----------------------------------------------------------------------------------------------
int log_get_verb_level()
{
    return _log_verb_level;
};