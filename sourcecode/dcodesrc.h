#pragma once

#include "sdf_archive.h"

bool_t sf_defines_setup(void);
Sint8  sf_stage_compile(Uint8 stage, Uint8 mask, float loadin_min, float loadin_max);
bool_t sf_headerize(SDF_PHEADER src_header, char * filename);
bool_t sf_compilerize(SDF_PHEADER src_header, const char* filename);
bool_t sf_loadize(SDF_PHEADER run_header, const char* filename);
