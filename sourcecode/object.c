// <ZZ> This file contains functions related to characters, particles, and windows...
//      obj_setup           - Gets ready to spawn stuff
//      obj_spawn           - Creates a new object of the desired type
//      property_reset_all  - Gets ready to use properties
//      property_add    - Registers a property name...  for window.x type stuff in script
//      property_find    - Searches for a property name...  Returns index or -1 if not found
//      obj_get_script_name - Helper for the recompile functions
//      obj_recompile_start - Figures out which script each object is attached to
//      obj_recompile_end   - Gets the new script address for each object

#include "object.inl"

#include "soulfu_types.h"
#include "runsrc.h"
#include "network.h"
#include "logfile.h"
#include "soulfu.h"

#include "pxss_run.inl"


static int prt_get_free_index( int force_index );
static int chr_get_free_index( int force_index );
static int win_get_free_index();

// Room stuff...
float room_min_xyz[3];
float room_size_xyz[3];

Uint8* temp_character_bone_frame[MAX_CHARACTER];
Uint8  temp_character_bone_number[8];
Uint8  temp_character_frame_event;

WIN_DATA * current_win[MAX_WINDOW];
int        current_win_idx = 0;

//-----------------------------------------------------------------------------------------------
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!  Unused lists should be sorted...  Otherwise network numbers might get messed up...
// !!!BAD!!!  May be better to not keep unused lists for chars & parts, and just search backwards for highest character who's not on...
// !!!BAD!!!
// !!!BAD!!!

CHR_DATA _main_chr_data[MAX_CHARACTER];

static Uint16 _main_unused_character_count;
//CHR_DATA_EX main_chr_data[MAX_CHARACTER];

PRT_DATA main_prt_data[MAX_PARTICLE];

Uint16 main_unused_particle_count;
//PRT_DATA_EX main_prt_data[MAX_PARTICLE];

WIN_DATA      main_win_data[MAX_WINDOW];
//WIN_DATA_EX   main_win_data[MAX_WINDOW];

Uint16 main_window_poof_count;

static Uint16 main_unused_window_count;
static Uint16 main_unused_window[MAX_WINDOW];

Uint16 main_used_window_count;
Uint16 main_used_window[MAX_WINDOW];


//-----------------------------------------------------------------------------------------------
void obj_setup(void)
{
    // <ZZ> This function resets all of the lists for spawning objects
    Uint16 i;


    // Character list...
    repeat(i, MAX_CHARACTER)
    {
        _main_chr_data[i].on         = kfalse;
        _main_chr_data[i].reserve_on = kfalse;
    }
    _main_unused_character_count = UINT16_MAX;


    // Particle list...
    repeat(i, MAX_PARTICLE)
    {
        main_prt_data[i].on = kfalse;
    }
    main_unused_particle_count = MAX_PARTICLE;


    // Window list...
    repeat(i, MAX_WINDOW)
    {
        main_unused_window[i] = i;
    }
    main_unused_window_count = MAX_WINDOW;
    main_used_window_count = 0;
}

//-----------------------------------------------------------------------------------------------
void obj_get_script_name(Uint8* file_start, Uint8* file_name)
{
    // <ZZ> This function searches the index for a RUN file that starts at file_start, and fills
    //      in the file_name if it finds one.  Fills in 0 if not.
    int i;
    Uint8* data;
    Uint8 file_type;

    repeat(i, sdf_archive_get_num_files())
    {
        SDF_PHEADER pheader = sdf_archive_get_header(i);
        file_type = sdf_file_get_type(pheader);

        if (file_type == SDF_FILE_IS_RUN)
        {
            data = sdf_file_get_data(pheader);

            if (data == file_start)
            {
                if (0 == strcmp(sdf_file_get_name(pheader), "GENERIC")) return;

                memcpy(file_name, sdf_file_get_name(pheader), 8);
                return;
            }
        }
    }
    file_name[0] = 0;
}

//-----------------------------------------------------------------------------------------------
BASE_DATA * obj_spawn(PSoulfuScriptContext pss, SPAWN_INFO *psp, Uint8 type, float x, float y, float z, Uint8* script_file_start, Uint16 force_index)
{
    // <ZZ> This function spawns an object (OBJECT_CHR, OBJECT_PRT, or OBJECT_WIN) in the current_room.
    //      If we've run out of that type of object, the function returns NULL and doesn't spawn.
    //      Otherwise, it should return a pointer to the object's data (within the _main arrays).
    //      If force_index is less than MAX_(whatever), that object number should be used instead
    //      of automagically assigning one...

    BASE_DATA * retval = NULL;
    float floor_z;
	SPAWN_INFO loc_si;
    PSoulfuScriptContext pctxt;

    PSoulfuGlobalScriptContext psgs = (NULL == pss) ? &(g_sf_scr) : pss->psgs;

    loc_si.owner  = UINT16_MAX;
    loc_si.target = UINT16_MAX;
    loc_si.team   = 0;
	loc_si.subtype = 0;
	loc_si.cclass = 255;

    if (script_file_start == NULL) return retval;

    if (NULL == psp) psp = &loc_si;

    switch (type)
    {
        case OBJECT_CHR:
            {
                Uint16     chr_idx = UINT16_MAX;
                CHR_DATA * pchr    = NULL;

                chr_idx = chr_get_free_index( force_index );

                // No character slots left???
                if ( !VALID_CHR_RANGE(chr_idx) ) break;

                pchr = _main_chr_data + chr_idx;
                pctxt = &(pchr->script_ctxt);

                memset(CAST(void*, pchr), 0, sizeof(CHR_DATA));

                pchr->on        = ktrue;
                pchr->delete_me = kfalse;

                // At this point, chr_idx should be the correct index to use for this object...
                temp_character_bone_frame[chr_idx] = NULL;
                floor_z = room_heightmap_height_slow(roombuffer, x, y);

                if (z < floor_z)
                {
                    z = floor_z;
                }


                pchr->x = x;
                pchr->y = y;
                pchr->z = z;

                // Load default data...
                pchr->gotox = x;
                pchr->gotoy = y;
                pchr->hoverz = z;


                pchr->floorz = floor_z;

                pchr->flatten = 255;  // Flatness
                pchr->lastway = 255;  // Last waypoint
                pchr->nextway = 255;  // Next waypoint

                pchr->target = chr_idx;  // Target = self...
                pchr->bone = NO_BONE;
                pchr->action = ACTION_STAND;
                pchr->daction = ACTION_STAND;

                // set the matrix
                pchr->sidex = 1.0f;
                pchr->sidey = 0.0f;
                pchr->sidez = 0.0f;
                pchr->frontx = 0.0f;
                pchr->fronty = 1.0f;
                pchr->frontz = 0.0f;
                pchr->upx = 0.0f;
                pchr->upy = 0.0f;
                pchr->upz = 1.0f;


                pchr->boxsize = 1.0f;
                pchr->dmtimer =    0;

                pchr->flyfric = 0.95f;  // Default flying friction...

                pchr->cclass = psp->cclass;  // Character class
                pchr->bright = 255;  // Brightness
                pchr->alpha = 255;  // Alpha
                pchr->height = 5;  // Height


                // Fill in owner, target and team...
                pchr->owner = psp->owner;
                pchr->target = psp->target;						// Second value set here.
                pchr->team = psp->team;

                // Default to no rider/mount...
                pchr->rider = UINT16_MAX;  // self.rider
                pchr->mount = UINT16_MAX;  // self.mount

                // Fill in subtype (equipment colors 0 & 1...)
                pchr->eqcol01 = psp->subtype;

                // Fill in mana cost (so players without mana can buy it with stat points...)
                pchr->manacst = 5;

                // Clear the object spawn number...
                pchr->rm_ob_num = 255;

                // initialize the object script
                sf_script_spawn(psgs, pctxt);
                sf_script_init(pctxt, script_file_start, (Uint8*)pchr);

                obj_get_script_name(script_file_start, pchr->script_name);
                memcpy(&(pchr->script_ctxt.spawn_info), psp, sizeof(SPAWN_INFO));
                pchr->net_scr_ind = network_find_script_index(pchr->script_name);

                sf_fast_rerun_script(pctxt, FAST_FUNCTION_SPAWN);

                if (pchr->on || pchr->reserve_on)
                {
                    retval = CAST(BASE_DATA *, pchr);
                };
            }
            break;

        case OBJECT_PRT:
            {
                Uint16     prt_idx = UINT16_MAX;
                PRT_DATA * pprt    = NULL;

                prt_idx = prt_get_free_index(force_index);

                // No particle slots left???
                if ( !VALID_PRT_RANGE(prt_idx) ) break;

                pprt = main_prt_data + prt_idx;
                pctxt = &(pprt->script_ctxt);

                memset(CAST(void*, pprt), 0, sizeof(PRT_DATA));

                // At this point, prt_idx should be the correct index to use for this object...
                pprt->on        = ktrue;
                pprt->delete_me = kfalse;

                pprt->x = x;
                pprt->y = y;
                pprt->z = z;

                // Load default data...
                pprt->lastx  = x;
                pprt->lasty  = y + 0.0001f;
                pprt->lastz  = z;
                pprt->vx     = 0.0f;
                pprt->vy     = 0.0f;
                pprt->vz     = 0.0f;
                pprt->length = 1.0f;
                pprt->width  = 1.0f;

                pprt->is_spawning = ktrue;       // Just spawn'd...
                pprt->stuckto     = UINT16_MAX;  // Who it's stuck to...

                // Default tint color to white
                pprt->red   = 255;  // Red
                pprt->green = 255;  // Green
                pprt->blue  = 255;  // Blue
                pprt->alpha = 255;  // Alpha

                // Default damage type...
                pprt->dtype  = 15;  // No flags, type 15


                // Fill in owner, target and team...
                pprt->owner  = psp->owner;
                pprt->target = psp->target;
                pprt->team   = psp->team;
                
                // initialize the object script
                sf_script_spawn(psgs, pctxt);
                sf_script_init(pctxt, script_file_start, (Uint8*)pprt);

                obj_get_script_name(script_file_start, pprt->script_name);
                memcpy(&(pprt->script_ctxt.spawn_info), psp, sizeof(SPAWN_INFO));

                sf_fast_rerun_script(pctxt, FAST_FUNCTION_SPAWN);

                if (pprt->on)
                {
                    retval = CAST(BASE_DATA *, pprt);
                };
            }
            break;

        case OBJECT_WIN:
            {
                Uint16     win_idx = UINT16_MAX;
                WIN_DATA * pwin    = NULL;

                win_idx = win_get_free_index();

                if( !VALID_WIN_RANGE(win_idx) ) break;

                pwin  = main_win_data + win_idx;
                pctxt = &(pwin->script_ctxt);

                memset(CAST(void*, pwin), 0, sizeof(WIN_DATA));

                pwin->on        = ktrue;
                pwin->delete_me = kfalse;

                pwin->x = x;
                pwin->y = y;
                pwin->z = z;
                pwin->binding = UINT16_MAX;

                // initialize the object script
                sf_script_spawn(psgs, pctxt);
                sf_script_init(pctxt, script_file_start, (Uint8*)pwin);

                // set initialize variables for the new object
                obj_get_script_name(script_file_start, pwin->script_name);
                memcpy(&(pctxt->spawn_info), psp, sizeof(SPAWN_INFO));
                pctxt->window3d.order = MAX_WINDOW - ((Uint8) win_idx);

                // run the initialization script
                current_win[current_win_idx++] = pwin;
                sf_fast_rerun_script(pctxt, FAST_FUNCTION_SPAWN);
                assert(pwin == current_win[--current_win_idx] );
                current_win[current_win_idx] = NULL;

                // only return a pointer to the window if it is still valid
                if (pwin->on && !pwin->delete_me)
                {
                    retval = CAST(BASE_DATA *, pwin);
                }
            }
            break;
    }


    return retval;
}

//-----------------------------------------------------------------------------------------------
bool_t chr_delete(CHR_DATA * pchr)
{
    int ichr, iprt;

    if (NULL == pchr || !pchr->delete_me) return kfalse;

    ichr = _get_chr_index(pchr);

    // Request destruction of all the particles that are stuck to this character...
    repeat(iprt, MAX_PARTICLE)
    {
        PRT_DATA * pprt = main_prt_data + iprt;

        if (!pprt->on) continue;

        if (pchr->flags & PART_STUCK_ON)
        {
            if ( pprt->stuckto == ichr)
            {
                pprt->on        = kfalse;
                pprt->delete_me = ktrue;
            }
        }
    }


    //// Unassign local player if it was a local player...
    //repeat(iprt, MAX_LOCAL_PLAYER)
    //{
    //    // Is this player active?
    //    if(local_player_character[iprt] == ichr)
    //    {
    //        local_player_character[iprt] = UINT16_MAX;
    //    }
    //}

    _main_unused_character_count++;

    pchr->on        = kfalse;
    pchr->delete_me = kfalse;

    return ktrue;
}

//-----------------------------------------------------------------------------------------------
bool_t prt_delete(PRT_DATA * pprt)
{
    if (NULL == pprt || !pprt->delete_me) return kfalse;

    main_unused_particle_count++;

    pprt->on        = kfalse;
    pprt->delete_me = kfalse;

    return ktrue;
}

//-----------------------------------------------------------------------------------------------
bool_t win_delete(WIN_DATA * pwin)
{
    int iwin, j;
    bool_t found;

    if (NULL == pwin || !pwin->delete_me) return kfalse;

    iwin = _get_win_index(pwin);

    //---- remove a window from the active list ----
    {
        j = 0;
        found = kfalse;

        while (j < main_used_window_count)
        {
            if (main_used_window[j] == iwin)
            {
                found = ktrue;
                break;
            };

            j++;
        }

        while (j < main_used_window_count)
        {
            main_used_window[j] = main_used_window[j+1];
            j++;
        }

        if (found)
        {
            main_used_window_count--;
        };
    }

    //---- add a window from the free list ----
    main_unused_window[main_unused_window_count] = iwin;
    main_unused_window_count++;


    pwin->on        = kfalse;
    pwin->delete_me = kfalse;

    return ktrue;
}



//-----------------------------------------------------------------------------------------------
bool_t obj_destroy(OBJECT_PTR * pd)
{
    // <ZZ> This function destroys an object that had been previously spawned, and shuffles
    //      around the data accordingly...

    bool_t retval = kfalse;

    if (NULL == pd) return retval;

    if (ptr_is_type(pd, OBJECT_PRT))
    {
        PRT_DATA * pprt = (PRT_DATA *)ptr_get_ptr(pd, OBJECT_PRT);

        if (NULL != pprt && pprt->on && !pprt->delete_me)
        {
            pprt->on        = kfalse;
            pprt->delete_me = ktrue;

            retval = ktrue;
        };
    }
    else if (ptr_is_type(pd, OBJECT_CHR))
    {
        CHR_DATA * pchr = (CHR_DATA *)ptr_get_ptr(pd, OBJECT_CHR);

        if (NULL != pchr && pchr->on && !pchr->delete_me)
        {
            Uint16 ichr = ptr_get_idx(pd, OBJECT_CHR);

            log_info(1, "Requesting destruction of character %d", ichr);

            pchr->on = kfalse;

            if (!pchr->reserve_on)
            {
                pchr->delete_me = ktrue;
            }

            retval = ktrue;
        }
    }
    else if (ptr_is_type(pd, OBJECT_WIN))
    {
        WIN_DATA * pwin = (WIN_DATA *)ptr_get_ptr(pd, OBJECT_WIN);

        if (NULL != pwin && pwin->on && !pwin->delete_me)
        {
            pwin->on        = kfalse;
            pwin->delete_me = ktrue;

            retval = ktrue;
        }
    }
    else
    {
        // ???? unknown object type ????
        retval = kfalse;
    }

    return retval;
}

//-----------------------------------------------------------------------------------------------
void obj_poof_all(Uint8 type)
{
    // <ZZ> This function poofs all of the objects of a given type...
    Uint16 i;

    if (type == OBJECT_CHR)
    {
        repeat(i, MAX_CHARACTER)
        {
            _main_chr_data[i].on = kfalse;
            _main_chr_data[i].reserve_on = kfalse;
        }
        _main_unused_character_count = UINT16_MAX;


        // Turn off all local player controls...
        repeat(i, MAX_LOCAL_PLAYER)
        {
            local_player_character[i] = UINT16_MAX;
        }
    }

    if (type == OBJECT_PRT)
    {
        repeat(i, MAX_PARTICLE)
        {
            main_prt_data[i].on = kfalse;
        }
        main_unused_particle_count = MAX_PARTICLE;
    }

    // Don't bother with windows...
}

//-----------------------------------------------------------------------------------------------
Uint16 promotion_buffer[2*MAX_WINDOW];
Uint16 promotion_count = 0;

//-----------------------------------------------------------------------------------------------
static void _win_promote(int window)
{
    // <ZZ> This function puts a given window at the very top of the window_order.
    int i;
    bool_t found;

    found = kfalse;

    for (i = 0; i < main_used_window_count; i++)
    {
        if (main_used_window[i] == window)
        {
            found = ktrue;
            break;
        };
    }

    if (found)
    {
        // We found the given window in the current list, so remove it by moving everything
        // else back a slot...

        main_used_window_count--;

        while (i < main_used_window_count)
        {
            main_used_window[i] = main_used_window[i+1];
            i++;
        }

        main_used_window[main_used_window_count] = window;
        main_used_window_count++;
    };

}

//-----------------------------------------------------------------------------------------------
void win_promote_all()
{
    int i;

    repeat(i, promotion_count)
    {
        _win_promote(promotion_buffer[i]);
    }

    promotion_count = 0;
}

//-----------------------------------------------------------------------------------------------
void obj_recompile_start(void)
{
    // <ZZ> This function gets an 8 character filename for each object's script.
    int i;
    int object;



    // Do all windows
    repeat(i, main_used_window_count)
    {
        object = main_used_window[i];
        obj_get_script_name(main_win_data[object].script_ctxt.base.base.file_start, main_win_data[object].script_name);
    }


    // Do all particles
    repeat(i, MAX_PARTICLE)
    {
        if (!VALID_PRT(i)) continue;

        obj_get_script_name(main_prt_data[i].script_ctxt.base.base.file_start, main_prt_data[i].script_name);
    }


    // Do all characters
    repeat(i, MAX_CHARACTER)
    {
        if (_main_chr_data[i].on || _main_chr_data[i].reserve_on)
        {
            obj_get_script_name(_main_chr_data[i].script_ctxt.base.base.file_start, _main_chr_data[i].script_name);
        }
    }
}

//-----------------------------------------------------------------------------------------------
void sf_script_clear(PSoulfuScriptContext pss)
{
    pss->enc_cursor.active = kfalse;
    pss->self.item         = NO_ITEM;

    pxss_script_clear( &(pss->base) );
};

//-----------------------------------------------------------------------------------------------
void obj_recompile_end(void)
{
    // <ZZ> This function fills in the script_start for each object, based on the filename we
    //      found earlier.
    int i;
    SDF_PHEADER pheader;
    Uint8*      pdata;

    // Do all windows
    repeat(i, main_used_window_count)
    {
        int        win  = main_used_window[i];
        WIN_DATA * pwin = main_win_data + win;

        if (!pwin->on) continue;

        sf_script_clear( &(pwin->script_ctxt) );

        pheader = sdf_archive_find_filetype(pwin->script_name, SDF_FILE_IS_RUN);

        if (NULL == pheader)
        {
            // Try to use a generic script, so we don't have to respawn...
            // Generic is handled in the _start function, so the script doesn't get lost...
            pheader = sdf_archive_find_filetype("GENERIC", SDF_FILE_IS_RUN);
        }

        pdata = sdf_file_get_data(pheader);

        if (NULL == pdata)
        {
            OBJECT_PTR pp;
            obj_destroy( ptr_set_type(&pp, OBJECT_WIN, pwin) );
            i--;
        }
        else
        {
            sf_script_spawn( pwin->script_ctxt.psgs, &(pwin->script_ctxt) );
            sf_script_init(&(pwin->script_ctxt), sdf_file_get_data(pheader), (Uint8*)pwin);
        }

    }




    // Do all particles
    repeat(i, MAX_PARTICLE)
    {
        PRT_DATA * pprt = main_prt_data + i;

        if (!pprt->on) continue;

        sf_script_clear( &(pprt->script_ctxt) );

        pheader = sdf_archive_find_filetype(pprt->script_name, SDF_FILE_IS_RUN);

        if (NULL == pheader)
        {
            // Try to use a generic script, so we don't have to respawn...
            // Generic is handled in the _start function, so the script doesn't get lost...
            pheader = sdf_archive_find_filetype("GENERIC", SDF_FILE_IS_RUN);
        }

        pdata = sdf_file_get_data(pheader);

        if (NULL == pdata)
        {
            OBJECT_PTR pp;
            obj_destroy( ptr_set_type(&pp, OBJECT_PRT, pprt) );
            i--;
        }
        else
        {
            sf_script_spawn( pprt->script_ctxt.psgs, &(pprt->script_ctxt) );
            sf_script_init(&(pprt->script_ctxt), sdf_file_get_data(pheader), (Uint8*)pprt);
        }

    }




    // Do all characters
    repeat(i, MAX_CHARACTER)
    {
        CHR_DATA * pchr = chr_data_get_ptr( i );
        if (NULL == pchr) continue;

        sf_script_clear( &(pchr->script_ctxt) );

        pheader = sdf_archive_find_filetype(pchr->script_name, SDF_FILE_IS_RUN);

        if (NULL == pheader)
        {
            // Try to use a generic script, so we don't have to respawn...
            // Generic is handled in the _start function, so the script doesn't get lost...
            pheader = sdf_archive_find_filetype("GENERIC", SDF_FILE_IS_RUN);
        }

        pdata = sdf_file_get_data(pheader);

        if (NULL == pdata)
        {
            OBJECT_PTR pp;
            obj_destroy( ptr_set_type(&pp, OBJECT_CHR, pchr) );
            i--;
        }
        else
        {
            sf_script_spawn( pchr->script_ctxt.psgs, &(pchr->script_ctxt) );
            sf_script_init(&(pchr->script_ctxt), sdf_file_get_data(pheader), (Uint8*)pchr);
        };

    }

}

int prt_get_free_index( int force_index)
{
    int prt_idx = MAX_PARTICLE;

    if ( VALID_PRT_RANGE(force_index) )
    {
        // Use a specific particle slot...
        prt_idx = force_index;

        if (!VALID_PRT(prt_idx))
        {
            main_unused_particle_count--;
        }
    }
    else if (main_unused_particle_count > 0)
    {
        // Search for an empty particle slot...
        prt_idx = MAX_PARTICLE - 1;

        while (main_prt_data[prt_idx].on)
        {
            if (prt_idx == 0)
            {
                // This shouldn't happen...
                return MAX_PARTICLE;
            }

            prt_idx--;
        }

        main_unused_particle_count--;
    }


    return prt_idx;
}


int chr_get_free_index( int force_index)
{
    int chr_idx = MAX_CHARACTER;

    if ( VALID_CHR_RANGE(force_index) )
    {
        // Use a specific character slot...
        chr_idx = force_index;

        if (!_main_chr_data[chr_idx].on)
        {
            _main_unused_character_count--;
        }
    }
    else if (_main_unused_character_count > 0)
    {
        // Search for an empty character slot...
        chr_idx = MAX_CHARACTER - 1;

        while (_main_chr_data[chr_idx].on || _main_chr_data[chr_idx].reserve_on)
        {
            if (chr_idx == 0)
            {
                // This shouldn't happen...
                return MAX_CHARACTER;
            }

            chr_idx--;
        }

        _main_unused_character_count--;
    }

    return chr_idx;
}

int win_get_free_index()
{
    int win_idx = MAX_WINDOW;

    // Any window slots left???
    if ( 0!= main_unused_window_count )
    {
        main_unused_window_count--;
        win_idx = main_unused_window[main_unused_window_count];

        main_used_window[main_used_window_count] = win_idx;
        main_used_window_count++;
    }

    return win_idx;
}


void obj_delete()
{
    int i;

    // --- do all the actual deletions of objects here ----

    repeat(i, MAX_CHARACTER)
    {
        // do any scheduled deletions.
        // will not delete anything that is not marked
        chr_delete( _main_chr_data + i);
    }

    repeat(i, MAX_PARTICLE)
    {
        // do any scheduled deletions.
        // will not delete anything that is not marked
        prt_delete( main_prt_data + i);
    }

    repeat(i, MAX_WINDOW)
    {
        // do any scheduled deletions.
        // will not delete anything that is not marked
        win_delete( main_win_data + i);
    }
}
