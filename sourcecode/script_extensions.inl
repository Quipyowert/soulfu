#pragma once

#include "script_extensions.h"
#include "runsrc.h"
#include "soulfu.h"

#include "pxss_run.inl"
#include "object.inl"

_INLINE bool_t               _sf_fast_run_script_obj(PSoulfuGlobalScriptContext pgs, Uint8 *file_start, Uint16 fast_function, OBJECT_PTR * pobj);
_INLINE PSoulfuScriptContext _sf_script_init_obj(PSoulfuScriptContext pss, Uint8 *file_start, OBJECT_PTR * pobj);


//-----------------------------------------------------------------------------------------------
_INLINE PSoulfuScriptContext _sf_script_init_obj(PSoulfuScriptContext pss, Uint8 *file_start, OBJECT_PTR * pobj)
{
    //---- initialize the basic info ----
    focus_clear( &(pss->self) );

    if (NULL == pxss_script_init( &(pss->base), file_start )) return NULL;

    focus_set( &(pss->self), pobj, NO_ITEM);

    pss->screen.scale = window_scale;
    pss->screen.x = 0;
    pss->screen.y = 0;
    pss->screen.w = screen_x / pss->screen.scale;
    pss->screen.h = screen_y / pss->screen.scale;

    pss->enc_cursor.active = kfalse;

    return pss;
}

//-----------------------------------------------------------------------------------------------
_INLINE bool_t _sf_fast_run_script_obj(PSoulfuGlobalScriptContext psgs, Uint8 *file_start, Uint16 fast_function, OBJECT_PTR * pobj)
{
    SoulfuScriptContext loc_sc;

    // initialize the ScriptContext
    if (NULL == sf_script_spawn(psgs, &loc_sc) ) return kfalse;

    if (NULL == _sf_script_init_obj(&loc_sc, file_start, pobj)) return kfalse;

    if (NULL == pxss_prepare_fast_run( &(loc_sc.base), fast_function) )
    {
        return kfalse;
    };

    return sf_run_script(&loc_sc, kfalse);
};
