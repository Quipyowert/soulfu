// <ZZ> This file has all the stuff for rendering objects...
//      setup_shadow            - Reads shadow names from SHADOW.TXT
//      render_rdy              - Renders a model

#include "dcodesrc.h"

#ifdef DEVTOOL
Uint8 global_billboard_active = ktrue;
#endif


// For modeler...
#define MOVE_MODE_MOVE      0
#define MOVE_MODE_SCALE     1
#define MOVE_MODE_ROTATE    2

// For room editor...
#define MOVE_MODE_BRIDGE_PLOP 3



#define MAX_SHADOW_TEXTURE 256
Uint32 shadow_texture[MAX_SHADOW_TEXTURE];


#ifdef DEVTOOL
Uint8 do_anchor_swap = kfalse;
#endif


//-----------------------------------------------------------------------------------------------
//void render_set_light(Uint8* light_data)
//{
//    // <ZZ> This function sets up the light position and color for a character.  Must be called
//    //      before rendering the character...  light_data is a pointer to 3 bytes of color info,
//    //      1 byte of padding, and 3 floats of light position...
//    global_render_light_color_rgb[0] = light_data[0];
//    global_render_light_color_rgb[1] = light_data[1];
//    global_render_light_color_rgb[2] = light_data[2];
//
//
//    // Figure out the cartoon texture offset, depending on light position...
//    light_data += 4;
//    global_render_light_offset_xy[XX] = (camera.rotation_xy[XX]/UINT16_MAX) + (ATAN2(((float*) (light_data))[YY], ((float*) (light_data))[XX])/TWO_PI) + 0.25f;
//    global_render_light_offset_xy[YY] = 0.38f;
//}

//-----------------------------------------------------------------------------------------------
Uint8* render_generate_model_world_data(Uint8* data, Uint16 frame, float* matrix, Uint8* write)
{
    // <ZZ> This function generates new bone frame data for a given model (skipping first 11 values).
    //      This info is translated and rotated to a world coordinate system, which can be fed into
    //      render_bone_frame() to calculate vertex positions.  Write tells us where to put the new
    //      stuff.  Matrix is a 12 value array like this...
    //          [RightX][RightY][RightZ]  (entries 0 to 2)
    //          [FrontX][FrontY][FrontZ]  (entries 3 to 5)
    //          [ Up X ][ Up Y ][ Up Z ]  (entries 6 to 8)
    //          [OffstX][OffstY][OffstZ]  (entries 9 to 11)
    //      Function returns next place to write data...
    Uint16 num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* frame_data;
    float* joint_position_data;
    Uint8 base_model;
    Uint16 i, j;
    Uint8 num_base_model;
    Uint16 num_bone_frame;

    log_info(1, "render_generate_blah_blah_blah");


    data += 3;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data );  data += 2;
    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    if (frame >= num_bone_frame) frame = 0;

    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);  frame_data += 11;
    data = data + (base_model * 20);
    base_model_data = DEREF( Uint8*, data );


    base_model_data += 4;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;
    joint_position_data = (float*) frame_data;


    // Calculate new bone normals...  Just rotate, no translation...
    log_info(1, "Calculating bone normals");
    num_bone = num_bone << 1;
    repeat(i, num_bone)
    {
        DEREF( float, write ) = (joint_position_data[XX] * matrix[0]) + (joint_position_data[YY] * matrix[3]) + (joint_position_data[ZZ] * matrix[6]);  write += 4;
        DEREF( float, write ) = (joint_position_data[XX] * matrix[1]) + (joint_position_data[YY] * matrix[4]) + (joint_position_data[ZZ] * matrix[7]);  write += 4;
        DEREF( float, write ) = (joint_position_data[XX] * matrix[2]) + (joint_position_data[YY] * matrix[5]) + (joint_position_data[ZZ] * matrix[8]);  write += 4;
        //if((i & 1) == 0)
        //{
        //  log_info(0, "Front %d == (%f, %f, %f)", i>>1, DEREF( float, write-12 ), DEREF( float, write-8 ), DEREF( float, write-4 ));
        //}
        //else
        //{
        //  log_info(0, "Side %d == (%f, %f, %f)", i>>1, DEREF( float, write-12 ), DEREF( float, write-8 ), DEREF( float, write-4 ));
        //}
        joint_position_data += 3;
    }
    log_info(1, "Calculated bone normals");


    // Calculate new joint positions...  Translation and rotation...
    log_info(1, "Calculating joint positions");
    repeat(i, num_joint)
    {
        DEREF( float, write ) = (joint_position_data[XX] * matrix[0]) + (joint_position_data[YY] * matrix[3]) + (joint_position_data[ZZ] * matrix[6]) + matrix[9];  write += 4;
        DEREF( float, write ) = (joint_position_data[XX] * matrix[1]) + (joint_position_data[YY] * matrix[4]) + (joint_position_data[ZZ] * matrix[7]) + matrix[10];  write += 4;
        DEREF( float, write ) = (joint_position_data[XX] * matrix[2]) + (joint_position_data[YY] * matrix[5]) + (joint_position_data[ZZ] * matrix[8]) + matrix[11];  write += 4;
        //  log_info(0, "Joint %d == (%f, %f, %f)", i, DEREF( float, write-12 ), DEREF( float, write-8 ), DEREF( float, write-4 ));
        joint_position_data += 3;
    }
    log_info(1, "Calculated joint positions");


    // Calculate shadow information...
    log_info(1, "Calculating shadow corners");
    frame_data = (Uint8*) joint_position_data;
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        base_model = *frame_data;  // really shadow alpha, not base_model
        *write = base_model;  write++;  frame_data++;

        if (base_model)
        {
            // Rotate and translate each shadow coordinate...
            repeat(j, 4)
            {
                DEREF( float, write ) = ((((float*) frame_data)[XX]) * matrix[0]) + ((((float*) frame_data)[YY]) * matrix[3]) + matrix[9];  write += 4;
                DEREF( float, write ) = ((((float*) frame_data)[XX]) * matrix[1]) + ((((float*) frame_data)[YY]) * matrix[4]) + matrix[10];  write += 4;
                frame_data += 8;
            }
        }
    }
    log_info(1, "Calculated shadow corners");


    // We should now be able to feed render_bone_frame() our old write value...
    return write;
}

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint8 triangle_lines = kfalse;
void render_fix_model_to_bone_length(Uint8* data, Uint16 frame, Uint16 exempt_joint)
{
    // <ZZ> This function moves all of the joints, except exempt_joint, to make the bone lengths
    //      match the initial bone lengths.  My shoddy little inverse kinematics routine.
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* bone_data;
    float* joint_size_data;
    float start_xyz[3];
    float end_xyz[3];
    float length;
    Uint8 base_model, detail_level;
    Uint16 joint[2];
    float joint_weight[2];
    Uint16 i, j;
    float joint_movement_xyz[MAX_JOINT][3];
    float desired_length;
    float total_weight;



    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    detail_level = 0;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 12;
    joint_size_data = DEREF( float*, data );  data += 4;
    bone_data = DEREF( Uint8*, data );


    base_model_data += 4;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;

    if (num_joint >= MAX_JOINT) return;


    // Figure out bone movement...  Done interatively...
    repeat(i, 20)
    {
        // Clear joint movement vectors
        repeat(j, num_joint)
        {
            joint_movement_xyz[j][XX] = 0;
            joint_movement_xyz[j][YY] = 0;
            joint_movement_xyz[j][ZZ] = 0;
        }



        // Fill in joint movement vectors
        repeat(j, num_bone)
        {
            // Figure out bone/joint coordinates
            joint[0] = DEREF( Uint16, bone_data + 1 + (j * 9) );
            joint[1] = DEREF( Uint16, bone_data + 3 + (j * 9) );
            desired_length = DEREF( float, bone_data + 5 + (j * 9) );


            start_xyz[XX] = DEREF( float, frame_data + 11 + (num_bone * 24) + (joint[0] * 12) );
            start_xyz[YY] = DEREF( float, frame_data + 11 + 4 + (num_bone * 24) + (joint[0] * 12) );
            start_xyz[ZZ] = DEREF( float, frame_data + 11 + 8 + (num_bone * 24) + (joint[0] * 12) );


            end_xyz[XX] = DEREF( float, frame_data + 11 + (num_bone * 24) + (joint[1] * 12) );
            end_xyz[YY] = DEREF( float, frame_data + 11 + 4 + (num_bone * 24) + (joint[1] * 12) );
            end_xyz[ZZ] = DEREF( float, frame_data + 11 + 8 + (num_bone * 24) + (joint[1] * 12) );


            // Vector from bone start to bone end to find length...
            end_xyz[XX] -= start_xyz[XX];
            end_xyz[YY] -= start_xyz[YY];
            end_xyz[ZZ] -= start_xyz[ZZ];
            length = vector_length(end_xyz);

            if (length < 0.001f) length = 0.001f;

            // Figure out the movement...
            length = (length - desired_length) / length;
            end_xyz[XX] *= length;
            end_xyz[YY] *= length;
            end_xyz[ZZ] *= length;

            if (exempt_joint == joint[0])
            {
                // Only joint[1] should move
                joint_movement_xyz[joint[1]][XX] = -end_xyz[XX];
                joint_movement_xyz[joint[1]][YY] = -end_xyz[YY];
                joint_movement_xyz[joint[1]][ZZ] = -end_xyz[ZZ];
            }
            else if (exempt_joint == joint[1])
            {
                // Only joint[0] should move
                joint_movement_xyz[joint[0]][XX] = end_xyz[XX];
                joint_movement_xyz[joint[0]][YY] = end_xyz[YY];
                joint_movement_xyz[joint[0]][ZZ] = end_xyz[ZZ];
            }
            else
            {
                // Split movement according to weight
                joint_weight[0] = joint_size_data[joint[0]];
                joint_weight[1] = joint_size_data[joint[1]];

                if (joint_weight[0] < JOINT_COLLISION_SCALE) joint_weight[0] = JOINT_COLLISION_SCALE;

                if (joint_weight[1] < JOINT_COLLISION_SCALE) joint_weight[1] = JOINT_COLLISION_SCALE;

                total_weight = joint_weight[0] + joint_weight[1];

                joint_movement_xyz[joint[0]][XX] += end_xyz[XX] * joint_weight[1] / total_weight;
                joint_movement_xyz[joint[0]][YY] += end_xyz[YY] * joint_weight[1] / total_weight;
                joint_movement_xyz[joint[0]][ZZ] += end_xyz[ZZ] * joint_weight[1] / total_weight;

                joint_movement_xyz[joint[1]][XX] -= end_xyz[XX] * joint_weight[0] / total_weight;
                joint_movement_xyz[joint[1]][YY] -= end_xyz[YY] * joint_weight[0] / total_weight;
                joint_movement_xyz[joint[1]][ZZ] -= end_xyz[ZZ] * joint_weight[0] / total_weight;
            }
        }



        // Apply the movement for each joint...
        repeat(j, num_joint)
        {
            DEREF( float, frame_data + 11 + (num_bone*24) + (j*12) ) += joint_movement_xyz[j][XX] * 0.50f;
            DEREF( float, frame_data + 11 + (num_bone*24) + 4 + (j*12) ) += joint_movement_xyz[j][YY] * 0.50f;
            DEREF( float, frame_data + 11 + (num_bone*24) + 8 + (j*12) ) += joint_movement_xyz[j][ZZ] * 0.50f;
        }
    }
}
#endif

//-----------------------------------------------------------------------------------------------
void render_crunch_bone(Uint8* data, Uint16 frame, Uint16 bone, Uint8 detail_level)
{
    // <ZZ> This function generates the length of a given bone.  Bone length...
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_bone;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* bone_data;
    Uint8 bone_name;
    float start_xyz[3];
    float end_xyz[3];
    float length;
    Uint8 base_model;
    Uint16 joint[2];


    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;

    if (detail_level >= num_detail_level) return;

    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 16;
    bone_data = DEREF( Uint8*, data );


    base_model_data += 6;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;

    if (bone >= num_bone) return;



    // Figure out bone/joint coordinates
    bone_name = *(bone_data + (bone * 9));

    if (bone_name == 0)
    {
        joint[0] = DEREF( Uint16, bone_data + 1 + (bone * 9) );
        joint[1] = DEREF( Uint16, bone_data + 3 + (bone * 9) );


        start_xyz[XX] = DEREF( float, frame_data + 11 + (num_bone * 24) + (joint[0] * 12) );
        start_xyz[YY] = DEREF( float, frame_data + 11 + 4 + (num_bone * 24) + (joint[0] * 12) );
        start_xyz[ZZ] = DEREF( float, frame_data + 11 + 8 + (num_bone * 24) + (joint[0] * 12) );


        end_xyz[XX] = DEREF( float, frame_data + 11 + (num_bone * 24) + (joint[1] * 12) );
        end_xyz[YY] = DEREF( float, frame_data + 11 + 4 + (num_bone * 24) + (joint[1] * 12) );
        end_xyz[ZZ] = DEREF( float, frame_data + 11 + 8 + (num_bone * 24) + (joint[1] * 12) );


        // Vector from bone start to bone end to find length...
        end_xyz[XX] -= start_xyz[XX];
        end_xyz[YY] -= start_xyz[YY];
        end_xyz[ZZ] -= start_xyz[ZZ];
        length = vector_length(end_xyz);

        //        log_info(0, "Bone %d, from joint %d to %d, is %f long", bone, joint[0], joint[1], length);
    }
    else
    {
        length = 1.0f;
        //        log_info(0, "Bone %d, is a special grip bone...  Length is 1.0", bone);
    }


    // Write length to RDY data
    DEREF( float, bone_data + 5 + (bone*9) ) = length;
}

//-----------------------------------------------------------------------------------------------
void render_crunch_vertex(Uint8* data, Uint16 frame, Uint16 vertex, Uint8 recalc_weight, Uint8 detail_level)
{
    // <ZZ> This function generates the bone-offset coordinates for a given vertex.
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_vertex, num_bone;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* vertex_data;
    Uint8* bone_data;
    Uint16 i;
    float start_xyz[3];
    float end_xyz[3];
    float point_xyz[3];
    float height_xyz[3];
    float front_xyz[3];
    float side_xyz[3];
    float length;
    float height;
    float front;
    float side;
    Uint8 base_model;
    Uint8 bone_binding[2];
    Uint16 joint[2];
    float weight[2];


    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;

    if (detail_level >= num_detail_level) return;

    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 16;
    bone_data = DEREF( Uint8*, data );


    num_vertex = DEREF( Uint16, base_model_data ); base_model_data += 6;

    if (vertex >= num_vertex) return;

    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;


    // Skip ahead to the current vertex's data
    vertex_data = base_model_data + (vertex << 6);
    point_xyz[XX] = DEREF( float, vertex_data );
    point_xyz[YY] = DEREF( float, vertex_data + 4 );
    point_xyz[ZZ] = DEREF( float, vertex_data + 8 );
    bone_binding[0] = *(vertex_data + 12);
    bone_binding[1] = *(vertex_data + 13);


    log_info(1, "Point %d, Bone %d and %d", vertex, bone_binding[0], bone_binding[1]);
    log_info(1, "Actual = (%f, %f, %f)", point_xyz[XX], point_xyz[YY], point_xyz[ZZ]);

    // Crunch for each bone
    repeat(i, 2)
    {
        // Figure out bone/joint coordinates
        joint[0] = DEREF( Uint16, bone_data + 1 + (bone_binding[i] * 9) );
        joint[1] = DEREF( Uint16, bone_data + 3 + (bone_binding[i] * 9) );
        log_info(1, "Joint %d to %d", joint[0], joint[1]);
        start_xyz[XX] = DEREF( float, frame_data + 11 + (num_bone * 24) + (joint[0] * 12) );
        start_xyz[YY] = DEREF( float, frame_data + 11 + 4 + (num_bone * 24) + (joint[0] * 12) );
        start_xyz[ZZ] = DEREF( float, frame_data + 11 + 8 + (num_bone * 24) + (joint[0] * 12) );

        log_info(1, "StartPos = (%f, %f, %f)", start_xyz[XX], start_xyz[YY], start_xyz[ZZ]);


        end_xyz[XX] = DEREF( float, frame_data + 11 + (num_bone * 24) + (joint[1] * 12) );
        end_xyz[YY] = DEREF( float, frame_data + 11 + 4 + (num_bone * 24) + (joint[1] * 12) );
        end_xyz[ZZ] = DEREF( float, frame_data + 11 + 8 + (num_bone * 24) + (joint[1] * 12) );

        log_info(1, "EndPos = (%f, %f, %f)", end_xyz[XX], end_xyz[YY], end_xyz[ZZ]);


        front_xyz[XX] = DEREF( float, frame_data + 11 + (bone_binding[i] * 24) );
        front_xyz[YY] = DEREF( float, frame_data + 11 + 4 + (bone_binding[i] * 24) );
        front_xyz[ZZ] = DEREF( float, frame_data + 11 + 8 + (bone_binding[i] * 24) );

        side_xyz[XX] = DEREF( float, frame_data + 11 + 12 + (bone_binding[i] * 24) );
        side_xyz[YY] = DEREF( float, frame_data + 11 + 16 + (bone_binding[i] * 24) );
        side_xyz[ZZ] = DEREF( float, frame_data + 11 + 20 + (bone_binding[i] * 24) );


        // Vector from bone start to bone end
        end_xyz[XX] -= start_xyz[XX];
        end_xyz[YY] -= start_xyz[YY];
        end_xyz[ZZ] -= start_xyz[ZZ];

        log_info(1, "AlongBone = (%f, %f, %f)", end_xyz[XX], end_xyz[YY], end_xyz[ZZ]);

        // Vector from bone start to point
        height_xyz[XX] = point_xyz[XX] - start_xyz[XX];
        height_xyz[YY] = point_xyz[YY] - start_xyz[YY];
        height_xyz[ZZ] = point_xyz[ZZ] - start_xyz[ZZ];

        log_info(1, "BoneStartToPoint = (%f, %f, %f)", height_xyz[XX], height_xyz[YY], height_xyz[ZZ]);


        // Calculate height value
        length = dot_product(end_xyz, end_xyz);
        height = dot_product(end_xyz, height_xyz);
        log_info(1, "Length = %f, Height = %f", length, height);

        if (length > 0) height = height / length;
        else height = 0;

        log_info(1, "Height = %f", height);


        // Calculate the weight for this bone
        if (recalc_weight)
        {
            if (height < 1.5f && height > -0.5f)
            {
                weight[i] = 0.5f - height;
                ABS(weight[i]);
                weight[i] = 1.0f - weight[i];
            }
            else
            {
                weight[i] = 0;
            }
        }


        // Calculate the height position
        height_xyz[XX] = start_xyz[XX] + (end_xyz[XX] * height);
        height_xyz[YY] = start_xyz[YY] + (end_xyz[YY] * height);
        height_xyz[ZZ] = start_xyz[ZZ] + (end_xyz[ZZ] * height);
        log_info(1, "HeightPos = (%f, %f, %f)", height_xyz[XX], height_xyz[YY], height_xyz[ZZ]);



        // Calculate vector from height position (along bone) to point
        height_xyz[XX] = point_xyz[XX] - height_xyz[XX];
        height_xyz[YY] = point_xyz[YY] - height_xyz[YY];
        height_xyz[ZZ] = point_xyz[ZZ] - height_xyz[ZZ];
        log_info(1, "HeightPosToPoint = (%f, %f, %f)", height_xyz[XX], height_xyz[YY], height_xyz[ZZ]);



        // Calculate front and side values
        front = dot_product(front_xyz, height_xyz);
        side = dot_product(side_xyz, height_xyz);
        log_info(1, "Front = (%f, %f, %f)", front_xyz[XX], front_xyz[YY], front_xyz[ZZ]);
        log_info(1, "Front = %f", front);
        log_info(1, "Side = (%f, %f, %f)", side_xyz[XX], side_xyz[YY], side_xyz[ZZ]);
        log_info(1, "Side = %f", side);

        // Write the values for the current vertex/bone
        DEREF( float, vertex_data + 27 + (i << 2) ) = height;
        DEREF( float, vertex_data + 35 + (i << 2) ) = front;
        DEREF( float, vertex_data + 43 + (i << 2) ) = side;

        //end_xyz[XX] = start_xyz[XX] + (height*end_xyz[XX]) + (front*front_xyz[XX]) + (side*side_xyz[XX]);
        //end_xyz[YY] = start_xyz[YY] + (height*end_xyz[YY]) + (front*front_xyz[YY]) + (side*side_xyz[YY]);
        //end_xyz[ZZ] = start_xyz[ZZ] + (height*end_xyz[ZZ]) + (front*front_xyz[ZZ]) + (side*side_xyz[ZZ]);
        log_info(1, "Generd = (%f, %f, %f)", end_xyz[XX], end_xyz[YY], end_xyz[ZZ]);
    }

    if (recalc_weight)
    {
        if (weight[0] > 0.98f || weight[1] < 0.02f)
        {
            // Single bone
            //            *(vertex_data+14) = (Uint8) 255;
            *(vertex_data + 14) = ((*(vertex_data + 14)) & 128) | 127;  // Force full weight, but save anchor bit...  !!!ANCHOR!!!
        }
        else if (weight[1] > 0.98f || weight[0] < 0.02f)
        {
            // Single bone...  Copy second into first...
            //            *(vertex_data+14) = (Uint8) 255;
            *(vertex_data + 14) = ((*(vertex_data + 14)) & 128) | 127;  // Force full weight, but save anchor bit...  !!!ANCHOR!!!
            *(vertex_data + 12) = *(vertex_data + 13);
            DEREF( float, vertex_data + 27 ) = height;
            DEREF( float, vertex_data + 35 ) = front;
            DEREF( float, vertex_data + 43 ) = side;
        }
        else
        {
            //            weight[0] = 255.0f * 0.5f *(weight[0] + (1.0f - weight[1]));
            //            *(vertex_data+14) = (Uint8) weight[0];


            weight[0] = 127.0f * 0.5f * (weight[0] + (1.0f - weight[1]));
            *(vertex_data + 14) = ((*(vertex_data + 14)) & 128) | (((Uint8) weight[0]) & 127);  // Save anchor bit...  !!!ANCHOR!!!
        }
    }
}

//-----------------------------------------------------------------------------------------------
#define MAX_BASE_TO_CRUNCH 256

// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
void render_attach_vertex_to_bone(Uint8* data, Uint16 frame, Uint16 vertex);
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!



void render_crunch_rdy(Uint8* data)
{
    // <ZZ> This function crunches all of the vertices and bones in an RDY file, which needs to be done
    //      before the bone animation system can be put into effect.
    Uint16 frame;
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_vertex;
    Uint16 num_bone;
    Uint8* base_model_data;
    Uint8* start_data;
    Uint8* frame_data;
    Uint16 i;
    Uint8 base_model, detail_level;
    Uint8 base_crunched[MAX_BASE_TO_CRUNCH];


    // Keep track of which we've crunched
    repeat(i, MAX_BASE_TO_CRUNCH)
    {
        base_crunched[i] = kfalse;
    }


    start_data = data;
    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;
    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);

    repeat(frame, num_bone_frame)
    {
        // Go to the current base model
        frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
        base_model = *(frame_data + 2);

        if (base_crunched[base_model] == kfalse)
        {
            // The bone structure is held in the first frame in which a base model is used
            base_crunched[base_model] = ktrue;
            repeat(detail_level, num_detail_level)
            {
                base_model_data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
                base_model_data = DEREF( Uint8*, base_model_data );
                num_vertex = DEREF( Uint16, base_model_data );  base_model_data += 6;
                num_bone = DEREF( Uint16, base_model_data );


                // Do the crunching
                repeat(i, num_vertex)
                {
                    render_crunch_vertex(start_data, frame, i, kfalse, detail_level);
                }
                repeat(i, num_bone)
                {
                    render_crunch_bone(start_data, frame, i, detail_level);
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------
void render_crunch_all(Uint8 mask, float loadin_min, float loadin_max)
{
    // <ZZ> This function crunches all of the vertex data for all of the RDY files...
    //      Mask lets me say if I don't really want all of the files...  Seems to do
    //      bad things if crunched multiple times (?)...
    int i;
    char filename[9];
    Uint8 filetype;
    Uint8* file_start;

    bool_t draw_loadin;
    draw_loadin = loadin_max > loadin_min;


    // Go through each file in the index
    log_info(0, "Crunching RDY files...");
    repeat(i, sdf_archive_get_num_files())
    {
        SDF_PHEADER pheader  = sdf_archive_get_header(i);
        Uint8      file_type = sdf_file_get_type(pheader);

        UI_INTERRUPT(draw_loadin, i, loadin_min, loadin_max);

        // Check the type of file to decompress, then hand off to a subroutine...
        if (file_type & mask)
        {
            sdf_archive_get_filename(i, filename, &filetype);

            if ((filetype & EXT_TYPE_MASK) == SDF_FILE_IS_RDY)
            {
                file_start = sdf_file_get_data(pheader);
                render_crunch_rdy(file_start);
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint16 select_num = 0;
Uint16 select_list[MAX_SELECT];
Uint16 select_index = 0;
float select_xyz[MAX_SELECT][3];
float* select_data[MAX_SELECT];
Uint16 select_flag[MAX_SELECT];
void select_add(Uint16 item, float* item_xyz)
{
    // <ZZ> Adds an item to the selection list...
    if (select_num < MAX_SELECT)
    {
        select_list[select_num] = item;
        select_xyz[select_num][XX] = item_xyz[XX];
        select_xyz[select_num][YY] = item_xyz[YY];
        select_xyz[select_num][ZZ] = item_xyz[ZZ];
        select_data[select_num] = item_xyz;
        select_num++;
    }
}
Uint8 select_inlist(Uint16 item)
{
    // <ZZ> Returns ktrue if the given item has already been added to the selection list.
    //      Also sets select_index to the index of that item.
    Uint16 i;

    repeat(i, select_num)
    {
        if (select_list[i] == item)
        {
            select_index = i;
            return ktrue;
        }
    }
    select_index = 0;
    return kfalse;
}
void select_remove(Uint16 item)
{
    // <ZZ> Removes an item from the selection list
    if (item < select_num)
    {
        while (item < (select_num - 1))
        {
            select_list[item] = select_list[item+1];
            select_xyz[item][XX] = select_xyz[item+1][XX];
            select_xyz[item][YY] = select_xyz[item+1][YY];
            select_xyz[item][ZZ] = select_xyz[item+1][ZZ];
            select_data[item] = select_data[item+1];
            item++;
        }

        select_num--;
    }
}
void select_update_xyz(void)
{
    // <ZZ> Reloads xyz values from actual data
    Uint16 i;
    repeat(i, select_num)
    {
        select_xyz[i][XX] = select_data[i][XX];
        select_xyz[i][YY] = select_data[i][YY];
        select_xyz[i][ZZ] = select_data[i][ZZ];
    }
}
void select_update_xy(void)
{
    // <ZZ> Reloads xy values from actual data
    Uint16 i;
    repeat(i, select_num)
    {
        select_xyz[i][XX] = select_data[i][XX];
        select_xyz[i][YY] = select_data[i][YY];
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void hide_vertices(Uint8* data, Uint16 frame, Uint8 do_hide)
{
    // <ZZ> This function flags the selected vertices as being hidden, or unhides all vertices,
    //      depending on the value of do_hide...  Joints and other data may not be selected, only
    //      vertices...
    Uint16 num_vertex;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8 base_model;
    Uint16 i;
    Uint8 num_base_model;


    if (do_hide)
    {
        // Hide 'em...
        repeat(i, select_num)
        {
            *(((Uint8*) select_data[i]) + 63) = ktrue;
        }
        select_clear();
    }
    else
    {
        // Unhide 'em
        data += 3;
        num_base_model = *data;  data += 3;
        data += (ACTION_MAX << 1);
        data += (MAX_DDD_SHADOW_TEXTURE);

        // Go to the current base model
        frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
        base_model = *(frame_data + 2);  frame_data += 11;
        data = data + (base_model * 20);
        base_model_data = DEREF( Uint8*, data );
        num_vertex = DEREF( Uint16, base_model_data ); base_model_data += 8;

        repeat(i, num_vertex)
        {
            *(base_model_data + 63) = kfalse;
            base_model_data += 64;
        }
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
float old_front_xyz[MAX_BONE][3];
float old_side_xyz[MAX_BONE][3];
void render_rotate_bones(Uint8* data, Uint16 frame, Sint8 rotation, Uint8 undo_rotation)
{
    // <ZZ> This function rotates every bone that has both joints selected around its central
    //      axis.  Rotation of 0 means no rotation.  It makes a backup of the old rotation
    //      components, so the function can be undone, if undo_rotation is ktrue.
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* bone_data;
    Uint8 base_model, detail_level;
    Uint16 i;
    float frotation;
    float frotcos;
    float frotsin;


    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    detail_level = 0;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 16;
    bone_data = DEREF( Uint8*, data );


    base_model_data += 4;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;
    frame_data += 11;

    if (undo_rotation == kfalse)
    {
        // Go through each bone, looking for selected joints...
        frotation = rotation * 2.0f * PI / 256.0f;
        frotsin = SIN(frotation);
        frotcos = COS(frotation);
        repeat(i, num_bone)
        {
            // Save the old data for an undo...
            old_front_xyz[i][XX] = DEREF( float, frame_data );  frame_data += 4;
            old_front_xyz[i][YY] = DEREF( float, frame_data );  frame_data += 4;
            old_front_xyz[i][ZZ] = DEREF( float, frame_data );  frame_data += 4;
            old_side_xyz[i][XX] = DEREF( float, frame_data );  frame_data += 4;
            old_side_xyz[i][YY] = DEREF( float, frame_data );  frame_data += 4;
            old_side_xyz[i][ZZ] = DEREF( float, frame_data );  frame_data += 4;

            // Is this a selected bone?
            if (select_inlist(DEREF( Uint16, bone_data + 1 )) && select_inlist(DEREF( Uint16, bone_data + 3 )))
            {
                // Generate new bone normals...
                frame_data -= 24;
                DEREF( float, frame_data ) = (old_front_xyz[i][XX] * frotcos) + (old_side_xyz[i][XX] * frotsin);  frame_data += 4;
                DEREF( float, frame_data ) = (old_front_xyz[i][YY] * frotcos) + (old_side_xyz[i][YY] * frotsin);  frame_data += 4;
                DEREF( float, frame_data ) = (old_front_xyz[i][ZZ] * frotcos) + (old_side_xyz[i][ZZ] * frotsin);  frame_data += 4;
                DEREF( float, frame_data ) = (-old_front_xyz[i][XX] * frotsin) + (old_side_xyz[i][XX] * frotcos);  frame_data += 4;
                DEREF( float, frame_data ) = (-old_front_xyz[i][YY] * frotsin) + (old_side_xyz[i][YY] * frotcos);  frame_data += 4;
                DEREF( float, frame_data ) = (-old_front_xyz[i][ZZ] * frotsin) + (old_side_xyz[i][ZZ] * frotcos);  frame_data += 4;
            }

            bone_data += 9;
        }
    }
    else
    {
        // Go through each bone, restoring the old data...
        repeat(i, num_bone)
        {
            // Save the old data for an undo...
            DEREF( float, frame_data ) = old_front_xyz[i][XX];  frame_data += 4;
            DEREF( float, frame_data ) = old_front_xyz[i][YY];  frame_data += 4;
            DEREF( float, frame_data ) = old_front_xyz[i][ZZ];  frame_data += 4;
            DEREF( float, frame_data ) = old_side_xyz[i][XX];  frame_data += 4;
            DEREF( float, frame_data ) = old_side_xyz[i][YY];  frame_data += 4;
            DEREF( float, frame_data ) = old_side_xyz[i][ZZ];  frame_data += 4;
        }
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL

RENDER_SELECTION selection =
{
    kfalse, // Uint8 selection.box_on
    0,      // Uint8 selection.close_type
    kfalse, // Uint8 selection.move_on
    kfalse, // Uint8 selection.pick_on
    0,      // Uint8 selection.view
    0,      // Uint8 selection.move

    {0.0f, 0.0f, 0.0f}, // float selection.center_xyz[3]
    {0.0f, 0.0f, 0.0f}, // float selection.offset_xyz[3]

    {0.0f, 0.0f}, // float selection.box_tl[2]
    {0.0f, 0.0f}, // float selection.box_br[2]
    {0.0f, 0.0f}, // float selection.box_min[2]
    {0.0f, 0.0f}  // float selection.box_max[2]
};

Uint8 rotation_view = 0;

void render_box()
{
    // <ZZ> This function draws the selection box


    // Check size...
    CLIP(selection.box_min[XX], selection.box_br[XX], selection.box_max[XX]);
    CLIP(selection.box_min[YY], selection.box_br[YY], selection.box_max[YY]);


    // Draw it
    display_texture_off();
    display_color(yellow);

    display_start_line();
    {
        display_vertex_xyz(selection.box_tl[XX], selection.box_tl[YY], 0);
        display_vertex_xyz(selection.box_br[XX], selection.box_tl[YY], 0);
    }
    display_end();

    display_start_line();
    {
        display_vertex_xyz(selection.box_br[XX], selection.box_tl[YY], 0);
        display_vertex_xyz(selection.box_br[XX], selection.box_br[YY], 0);
    }
    display_end();

    display_start_line();
    {
        display_vertex_xyz(selection.box_br[XX], selection.box_br[YY], 0);
        display_vertex_xyz(selection.box_tl[XX], selection.box_br[YY], 0);
    }
    display_end();

    display_start_line();
    {
        display_vertex_xyz(selection.box_tl[XX], selection.box_br[YY], 0);
        display_vertex_xyz(selection.box_tl[XX], selection.box_tl[YY], 0);
    }
    display_end();

    display_texture_on();
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_get_point_xy(float x, float y, float z, float* x_spot, float* y_spot)
{
    // <ZZ> This function gets the onscreen coordinates of a given point, based on the current
    //      projection and model matrices...  If it's not onscreen, it sets the values to -9999
    //      something...
    float feedback_buffer[4];

    glFeedbackBuffer(4, GL_2D, feedback_buffer);
    glRenderMode(GL_FEEDBACK);

    glBegin(GL_POINTS);
    {
        display_vertex_xyz(x, y, z);
    }
    display_end();

    if (glRenderMode(GL_RENDER))
    {
        *x_spot = feedback_buffer[1] * CAST(float, DEFAULT_W) / ((float)screen_x);
        *y_spot = CAST(float, DEFAULT_H) - (feedback_buffer[2] * CAST(float, DEFAULT_H) / ((float)screen_y));
    }
    else
    {
        *x_spot = -9999.9f;
        *y_spot = -9999.9f;
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_get_point_xyd(float x, float y, float z, float* x_spot, float* y_spot, float* d_spot)
{
    // <ZZ> This function gets the onscreen coordinates of a given point, based on the current
    //      projection and model matrices.  D is depth...  If it's not onscreen, it sets the
    //      values to -9999 something...
    float feedback_buffer[4];

    glFeedbackBuffer(4, GL_3D, feedback_buffer);
    glRenderMode(GL_FEEDBACK);

    glBegin(GL_POINTS);
    {
        display_vertex_xyz(x, y, z);
    }
    display_end();

    if (glRenderMode(GL_RENDER))
    {
        *x_spot = feedback_buffer[1] * CAST(float, DEFAULT_W) / ((float)screen_x);
        *y_spot = CAST(float, DEFAULT_H) - (feedback_buffer[2] * CAST(float, DEFAULT_H) / ((float)screen_y));
        *d_spot = feedback_buffer[3];
    }
    else
    {
        *x_spot = -9999.9f;
        *y_spot = -9999.9f;
        *d_spot = -9999.9f;
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint8 render_get_point_xyz(SOULFU_WINDOW *pwin, float x, float y, float* x_spot, float* y_spot, float* z_spot)
{
    // <ZZ> This function gets the model coordinates of an onscreen point, based on the current
    //      model matrix.  Only works in ortho mode.
    float temp_matrix[16];


    if (x > pwin->x && y > pwin->y)
    {
        if (x < (pwin->x + pwin->w) && y < (pwin->y + pwin->h))
        {
            glGetFloatv(GL_MODELVIEW_MATRIX, temp_matrix);
            x = x - pwin->x;
            y = y - pwin->y;
            x = x / pwin->w;
            y = y / pwin->h;
            x = (x - 0.5f) * 4.0f;  // Assume -2 to 2 ortho
            y = (y - 0.5f) * -4.0f;  // Assume -2 to 2 ortho

            if (selection.view == VIEW_TOP_XY)
            {
                x = (x - temp_matrix[12]) / temp_matrix[0];
                y = (y - temp_matrix[13]) / temp_matrix[5];
                *x_spot = x;
                *y_spot = y;
                *z_spot = 0;
            }

            if (selection.view == VIEW_SIDE_YZ)
            {
                x = (temp_matrix[12] - x) / temp_matrix[9];
                y = (y - temp_matrix[13]) / temp_matrix[2];
                *x_spot = 0;
                *y_spot = x;
                *z_spot = y;
            }

            if (selection.view == VIEW_FRONT_XZ)
            {
                x = (x - temp_matrix[12]) / temp_matrix[0];
                y = (y - temp_matrix[13]) / temp_matrix[6];
                *x_spot = x;
                *y_spot = 0;
                *z_spot = y;
            }

            return ktrue;
        }
    }

    return kfalse;
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_model_move(void)
{
    // <ZZ> This function moves vertices and joints and stuff around in an RDY model
    Uint16 index;
    float x, y, z;
    float distance;
    float rotate_matrix[9];


    // Move, Rotate, or Scale selected vertices...
    if (selection.move == MOVE_MODE_ROTATE)
    {
        // Build the rotation matrix...
        x = selection.offset_xyz[XX] - selection.center_xyz[XX];
        y = selection.offset_xyz[YY] - selection.center_xyz[YY];
        z = selection.offset_xyz[ZZ] - selection.center_xyz[ZZ];

        if (rotation_view == VIEW_TOP_XY)         z = 0;
        else if (rotation_view == VIEW_FRONT_XZ)  y = 0;
        else if (rotation_view == VIEW_SIDE_YZ)   x = 0;

        distance = SQRT(x * x + y * y + z * z);

        if (distance > 0.20f)
        {
            // Start from identity
            rotate_matrix[0] = 1;  rotate_matrix[3] = 0; rotate_matrix[6] = 0;
            rotate_matrix[1] = 0;  rotate_matrix[4] = 1; rotate_matrix[7] = 0;
            rotate_matrix[2] = 0;  rotate_matrix[5] = 0; rotate_matrix[8] = 1;
            x = x / distance;  y = y / distance;  z = z / distance;

            if (rotation_view == VIEW_TOP_XY)
            {
                // Rotation in XY plane (top)
                rotate_matrix[0] = y;  rotate_matrix[3] = x;
                rotate_matrix[1] = -x;  rotate_matrix[4] = y;
                sprintf(DEBUG_STRING, "Rotation = %f", atan2(-x, y)*RAD_TO_DEG);
            }
            else if (rotation_view == VIEW_FRONT_XZ)
            {
                // Rotation in XZ plane (front)
                rotate_matrix[0] = -z;  rotate_matrix[6] = -x;
                rotate_matrix[2] = x;  rotate_matrix[8] = -z;
                sprintf(DEBUG_STRING, "Rotation = %f", -atan2(x, -z)*RAD_TO_DEG);
            }
            else if (rotation_view == VIEW_SIDE_YZ)
            {
                // Rotation in YZ plane (side)
                rotate_matrix[4] = -z;  rotate_matrix[7] = -y;
                rotate_matrix[5] = y;  rotate_matrix[8] = -z;
                sprintf(DEBUG_STRING, "Rotation = %f", atan2(y, -z)*RAD_TO_DEG);
            }
        }
        else
        {
            return;
        }
    }


    if (keyb.down[SDLK_LSHIFT] || keyb.down[SDLK_RSHIFT])
    {
        if (rotation_view == VIEW_TOP_XY)
        {
            selection.offset_xyz[YY] = selection.center_xyz[YY];
            sprintf(NAME_STRING, "Offset X == %f", selection.offset_xyz[XX] - selection.center_xyz[XX]);
        }
        else if (rotation_view == VIEW_FRONT_XZ)
        {
            selection.offset_xyz[XX] = selection.center_xyz[XX];
            sprintf(NAME_STRING, "Offset Z == %f", selection.offset_xyz[ZZ] - selection.center_xyz[ZZ]);
        }
        else if (rotation_view == VIEW_SIDE_YZ)
        {
            selection.offset_xyz[ZZ] = selection.center_xyz[ZZ];
            sprintf(NAME_STRING, "Offset Y == %f", selection.offset_xyz[YY] - selection.center_xyz[YY]);
        }
    }

    repeat(index, select_num)
    {
        x = select_xyz[index][XX];
        y = select_xyz[index][YY];
        z = select_xyz[index][ZZ];


        if (selection.move == MOVE_MODE_MOVE)
        {
            // Translation
            x += selection.offset_xyz[XX] - selection.center_xyz[XX];
            y += selection.offset_xyz[YY] - selection.center_xyz[YY];
            z += selection.offset_xyz[ZZ] - selection.center_xyz[ZZ];
        }
        else if (selection.move == MOVE_MODE_SCALE)
        {
            // Scaling
            x = ((x - selection.center_xyz[XX]) * (1.0f + selection.offset_xyz[XX] - selection.center_xyz[XX])) + selection.center_xyz[XX];
            y = ((y - selection.center_xyz[YY]) * (1.0f + selection.offset_xyz[YY] - selection.center_xyz[YY])) + selection.center_xyz[YY];
            z = ((z - selection.center_xyz[ZZ]) * (1.0f + selection.offset_xyz[ZZ] - selection.center_xyz[ZZ])) + selection.center_xyz[ZZ];
        }
        else if (selection.move == MOVE_MODE_ROTATE)
        {
            // Rotation
            x -= selection.center_xyz[XX];
            y -= selection.center_xyz[YY];
            z -= selection.center_xyz[ZZ];
            select_data[index][XX] = x * rotate_matrix[0] + y * rotate_matrix[3] + z * rotate_matrix[6];
            select_data[index][YY] = x * rotate_matrix[1] + y * rotate_matrix[4] + z * rotate_matrix[7];
            select_data[index][ZZ] = x * rotate_matrix[2] + y * rotate_matrix[5] + z * rotate_matrix[8];
            x = select_data[index][XX];
            y = select_data[index][YY];
            z = select_data[index][ZZ];
            x += selection.center_xyz[XX];
            y += selection.center_xyz[YY];
            z += selection.center_xyz[ZZ];
        }

        // Save
        select_data[index][XX] = x;
        select_data[index][YY] = y;
        select_data[index][ZZ] = z;
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_tex_move(Uint8 limit)
{
    // <ZZ> This function moves tex vertices around in an RDY model
    Uint16 index;
    float x, y;
    float distance;
    float rotate_matrix[9];


    // Move, Rotate, or Scale selected vertices...
    if (selection.move == MOVE_MODE_ROTATE)
    {
        // Build the rotation matrix...
        x = selection.offset_xyz[XX] - selection.center_xyz[XX];
        y = selection.offset_xyz[YY] - selection.center_xyz[YY];
        distance = SQRT(x * x + y * y);

        if (distance > 0.0001f)
        {
            // Rotation in XY plane (top)
            x = x / distance;  y = y / distance;
            rotate_matrix[0] = y;  rotate_matrix[3] = x;
            rotate_matrix[1] = -x;  rotate_matrix[4] = y;
        }
        else
        {
            return;
        }
    }

    repeat(index, select_num)
    {
        x = select_xyz[index][XX];
        y = select_xyz[index][YY];


        if (selection.move == MOVE_MODE_MOVE)
        {
            // Translation
            x += selection.offset_xyz[XX] - selection.center_xyz[XX];
            y += selection.offset_xyz[YY] - selection.center_xyz[YY];
        }
        else if (selection.move == MOVE_MODE_SCALE)
        {
            // Scaling
            x -= selection.center_xyz[XX];
            y -= selection.center_xyz[YY];

            if (limit)
            {
                x *= (1.0f + ((selection.offset_xyz[XX] - selection.center_xyz[XX]) * 4.0f));
                y *= (1.0f + ((selection.offset_xyz[YY] - selection.center_xyz[YY]) * 4.0f));
            }
            else
            {
                x *= (1.0f + (selection.offset_xyz[XX] - selection.center_xyz[XX]));
                y *= (1.0f + (selection.offset_xyz[YY] - selection.center_xyz[YY]));
            }

            x += selection.center_xyz[XX];
            y += selection.center_xyz[YY];
        }
        else if (selection.move == MOVE_MODE_ROTATE)
        {
            // Rotation
            x -= selection.center_xyz[XX];
            y -= selection.center_xyz[YY];
            select_data[index][XX] = x * rotate_matrix[0] + y * rotate_matrix[3];
            select_data[index][YY] = x * rotate_matrix[1] + y * rotate_matrix[4];
            x = select_data[index][XX];
            y = select_data[index][YY];
            x += selection.center_xyz[XX];
            y += selection.center_xyz[YY];
        }

        // Save
        if (limit)
        {
            if (x > 1.0f)  x = 1.0f;

            if (x < 0.0f)  x = 0.0f;

            if (y > 1.0f)  y = 1.0f;

            if (y < 0.0f)  y = 0.0f;
        }

        select_data[index][XX] = x;
        select_data[index][YY] = y;
    }
}
#endif

//-----------------------------------------------------------------------------------------------
void setup_shadow(void)
{
    // <ZZ> This function fills in the shadow_texture table
    Uint8* data;
    int i;
    Uint32 value;
    SDF_STREAM loc_stream;
    TOKEN      loc_token;


    // Clear out the table
    log_info(0, "Setting up the shadow texture...");
    repeat(i, MAX_SHADOW_TEXTURE)
    {
        shadow_texture[i] = 0;
    }


    // Fill in the table
    i = 0;

    if (NULL != sdf_stream_open_filename(&loc_stream, "SHADOW.TXT"))
    {
        while (sdf_stream_read_line(&loc_stream) && i < MAX_SHADOW_TEXTURE)
        {
            // Check for a file...
            if (pxss_read_token(&loc_stream, &loc_token))
            {
                SDF_PHEADER pheader;
                TOKEN       tmp_token;

                if (loc_token.tag[0] == '-' || loc_token.tag[0] == '=')
                {
                    pxss_read_token(&loc_stream, &tmp_token);
                    loc_token.tag[1] = EOS;
                    strcat(loc_token.tag, tmp_token.tag);
                }

                log_info(1, "Looking for %s", loc_token.tag);
                pheader = sdf_archive_find_header(loc_token.tag);
                value = 0;

                if (pheader)
                {
                    log_info(1, "Found file");
                    data = sdf_file_get_data(pheader);
                    value = DEREF( Uint32, data + 2 );
                }

                shadow_texture[i] = value;
                i++;
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------
void render_bone_frame(Uint8* base_model_data, Uint8* bone_data, Uint8* current_frame_data)
{
    // <ZZ> This function generates vertex positions and normals, based on bone positions.
    //      Current_frame_data starts with bone information (skips first 11 values of RDY bone frame)...
    Uint16 num_vertex;
    Uint16 num_bone;
    Uint16 i;
    Uint8 j;
    float* vertex_xyz;
    float* normal_xyz;
    float* start_xyz;
    float* end_xyz;
    float  height_xyz[3];
    float* side_xyz;
    float* front_xyz;
    float* joint_data;
    float* scalars;
    float height;
    float front;
    float side;
    float cross_xyz[3];
    Uint8 bone_binding[2];
    Uint8 bone_weight;
    Uint16 joint;


    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!


    // !!!BAD!!!
    // !!!BAD!!!  Optimize this!!!
    // !!!BAD!!!


    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!



    log_info(1, "Rendering bone frame...");
    num_vertex = DEREF( Uint16, base_model_data );  base_model_data += 6;
    num_bone = DEREF( Uint16, base_model_data );  base_model_data += 2;
    joint_data = (float*) (current_frame_data + (num_bone << 3) + (num_bone << 4));
    repeat(i, num_vertex)
    {
        vertex_xyz = (float*) base_model_data;  base_model_data += 12;
        bone_binding[0] = *(base_model_data);  base_model_data++;
        bone_binding[1] = *(base_model_data);  base_model_data++;
        bone_weight = *(base_model_data);  base_model_data++;
        normal_xyz = (float*) base_model_data;  base_model_data += 12;

        vertex_xyz[XX] = 0;
        vertex_xyz[YY] = 0;
        vertex_xyz[ZZ] = 0;
        scalars = (float*) base_model_data;


#ifdef DEVTOOL

        if ((bone_weight & 128) && global_billboard_active) // So modeler doesn't do billboard on boning models...
#else
        if (bone_weight & 128)
#endif
        {
            // Anchor flag is set...  Do billboard style vertex coordinates...  !!!ANCHOR!!!
            bone_weight <<= 1;  // Get rid of anchor flag...  !!!ANCHOR!!!

            normal_xyz[XX] = 1.0f;
            normal_xyz[YY] = 0.0f;
            normal_xyz[ZZ] = 0.0f;

            log_info(1, "Calc'in bill-vertex %d", i);

            repeat(j, 2)
            {
                joint = *((Uint16*) (bone_data + (bone_binding[j] << 3) + bone_binding[j] + 1));
                start_xyz = (joint_data + (joint << 1) + joint);
                joint = *((Uint16*) (bone_data + (bone_binding[j] << 3) + bone_binding[j] + 3));
                end_xyz = (joint_data + (joint << 1) + joint);
                height_xyz[XX] = end_xyz[XX] - start_xyz[XX];
                height_xyz[YY] = end_xyz[YY] - start_xyz[YY];
                height_xyz[ZZ] = end_xyz[ZZ] - start_xyz[ZZ];
                height = *scalars;
                front = *(scalars + 4);
                log_info(1, "Front %d == %f", j, front);
                scalars++;


                // Normalize the vector...
                cross_product(camera.fore_xyz, height_xyz, cross_xyz);
                side = *((float*) (bone_data + (bone_binding[j] << 3) + bone_binding[j] + 5));
                side = side - (0.5f * (dot_product(camera.fore_xyz, height_xyz)));
                cross_xyz[XX] /= side;
                cross_xyz[YY] /= side;
                cross_xyz[ZZ] /= side;



                vertex_xyz[XX] += (start_xyz[XX] + (height_xyz[XX] * height) + (cross_xyz[XX] * front) ) * bone_weight;
                vertex_xyz[YY] += (start_xyz[YY] + (height_xyz[YY] * height) + (cross_xyz[YY] * front) ) * bone_weight;
                vertex_xyz[ZZ] += (start_xyz[ZZ] + (height_xyz[ZZ] * height) + (cross_xyz[ZZ] * front) ) * bone_weight;
                bone_weight = 255 - bone_weight;
            }
            // Divide by 255 to correct for bone_weight
            vertex_xyz[XX] = vertex_xyz[XX] * INV_0xFF;
            vertex_xyz[YY] = vertex_xyz[YY] * INV_0xFF;
            vertex_xyz[ZZ] = vertex_xyz[ZZ] * INV_0xFF;
            log_info(1, "Position == %f, %f, %f", vertex_xyz[XX], vertex_xyz[YY], vertex_xyz[ZZ]);
            base_model_data += 37;
        }
        else
        {
            // Anchor flag is not set...  Do normal vertex coordinates...
            bone_weight <<= 1;  // Get rid of anchor flag...  !!!ANCHOR!!!
            repeat(j, 2)
            {
                front_xyz = (float*) (current_frame_data + (bone_binding[j] << 4) + (bone_binding[j] << 3));
                side_xyz = front_xyz + 3;
                log_info(1, "Vertex %d, Bone %d", i, j);


                joint = *((Uint16*) (bone_data + (bone_binding[j] << 3) + bone_binding[j] + 1));
                start_xyz = (joint_data + (joint << 1) + joint);
                log_info(1, "Joint 1 = %d", joint);
                log_info(1, "Start = (%f, %f, %f)", start_xyz[XX], start_xyz[YY], start_xyz[ZZ]);


                joint = *((Uint16*) (bone_data + (bone_binding[j] << 3) + bone_binding[j] + 3));
                end_xyz = (joint_data + (joint << 1) + joint);
                log_info(1, "Joint 2 = %d", joint);
                log_info(1, "End = (%f, %f, %f)", end_xyz[XX], end_xyz[YY], end_xyz[ZZ]);


                height_xyz[XX] = end_xyz[XX] - start_xyz[XX];
                height_xyz[YY] = end_xyz[YY] - start_xyz[YY];
                height_xyz[ZZ] = end_xyz[ZZ] - start_xyz[ZZ];
                log_info(1, "Height = (%f, %f, %f)", height_xyz[XX], height_xyz[YY], height_xyz[ZZ]);
                log_info(1, "Front = (%f, %f, %f)", front_xyz[XX], front_xyz[YY], front_xyz[ZZ]);
                log_info(1, "Side = (%f, %f, %f)", side_xyz[XX], side_xyz[YY], side_xyz[ZZ]);

                if (j == 0)
                {
                    height = *(scalars + 6);
                    front = *(scalars + 7);
                    side = *(scalars + 8);
                    normal_xyz[XX] = (height_xyz[XX] * height) + (front_xyz[XX] * front) + (side_xyz[XX] * side);
                    normal_xyz[YY] = (height_xyz[YY] * height) + (front_xyz[YY] * front) + (side_xyz[YY] * side);
                    normal_xyz[ZZ] = (height_xyz[ZZ] * height) + (front_xyz[ZZ] * front) + (side_xyz[ZZ] * side);
                    log_info(1, "Generd = (%f, %f, %f)", normal_xyz[XX], normal_xyz[YY], normal_xyz[ZZ]);
                }


                height = *scalars;
                front = *(scalars + 2);
                side = *(scalars + 4);
                scalars++;


                log_info(1, "Height = %f", height);
                log_info(1, "Front = %f", front);
                log_info(1, "Side = %f", side);

                vertex_xyz[XX] += (start_xyz[XX] + (height_xyz[XX] * height) + (front_xyz[XX] * front) + (side_xyz[XX] * side)) * bone_weight;
                vertex_xyz[YY] += (start_xyz[YY] + (height_xyz[YY] * height) + (front_xyz[YY] * front) + (side_xyz[YY] * side)) * bone_weight;
                vertex_xyz[ZZ] += (start_xyz[ZZ] + (height_xyz[ZZ] * height) + (front_xyz[ZZ] * front) + (side_xyz[ZZ] * side)) * bone_weight;
                bone_weight = 255 - bone_weight;
            }
            // Divide by 255 to correct for bone_weight
            vertex_xyz[XX] = vertex_xyz[XX] * INV_0xFF;
            vertex_xyz[YY] = vertex_xyz[YY] * INV_0xFF;
            vertex_xyz[ZZ] = vertex_xyz[ZZ] * INV_0xFF;
            log_info(1, "Vertex at (%f, %f, %f)", vertex_xyz[XX], vertex_xyz[YY], vertex_xyz[ZZ]);
            base_model_data += 37;
        }
    }
}

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
#define PLANE_NORMAL_TOLERANCE 0.02f
void render_generate_bone_normals(Uint8* data, Uint16 frame)
{
    // <ZZ> This function regenerates bone normals, for when the joints move around...
    float* front_xyz;
    float* side_xyz;
    float bone_xyz[3];
    float distance;
    Uint8* normal_data;
    Uint8* bone_data;
    Uint8* joint_position_data_start;
    Uint8* frame_data;
    float* joint_position_data;
    Uint16 joint[2];
    Uint16 num_bone;
    Uint8 num_base_model;
    Uint8 base_model;
    Uint16 num_bone_frame;
    Uint8 detail_level;
    Uint16 i;
    float new_front_xyz[3];
    float new_side_xyz[3];



    data += 3;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);

    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    log_info(1, "Base model %d", base_model);
    detail_level = 0;

    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    bone_data = DEREF( Uint8*, data );
    num_bone = DEREF( Uint16, bone_data + 6 );
    bone_data = DEREF( Uint8*, data + 16 );


    normal_data = frame_data + 11;
    joint_position_data_start = normal_data + (num_bone << 4) + (num_bone << 3);
    //joint_position_data = (float*) joint_position_data_start;
    //joint_position_data += 6;
    //*joint_position_data += 0.1f;


    // Make it not rotate in weird planes...
    //    keep[XX] = (rotation_view == VIEW_SIDE_YZ);
    //    keep[YY] = (rotation_view == VIEW_FRONT_XZ);
    //    keep[ZZ] = (rotation_view == VIEW_TOP_XY);
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!  Should only keep for main bones being moved...  Others may float around...  Maybe???
    // !!!BAD!!!
    // !!!BAD!!!


    repeat(i, num_bone)
    {
        log_info(1, "Generate bone %d", i);
        // Front and side normals
        front_xyz = (float*) normal_data;  normal_data += 12;
        side_xyz = (float*) normal_data;  normal_data += 12;


        // Get the current bone vector...  Normalize it...
        joint[0] = DEREF( Uint16, bone_data + 1 );
        joint[1] = DEREF( Uint16, bone_data + 3 );
        bone_data += 9;
        log_info(1, "Joint %d to %d", joint[0], joint[1]);
        joint_position_data = ((float*) (joint_position_data_start + (joint[1] << 3) + (joint[1] << 2)));
        bone_xyz[XX] = joint_position_data[XX];
        bone_xyz[YY] = joint_position_data[YY];
        bone_xyz[ZZ] = joint_position_data[ZZ];
        joint_position_data = ((float*) (joint_position_data_start + (joint[0] << 3) + (joint[0] << 2)));
        log_info(1, "Start = (%f, %f, %f)", joint_position_data[XX], joint_position_data[YY], joint_position_data[ZZ]);
        log_info(1, "End = (%f, %f, %f)", bone_xyz[XX], bone_xyz[YY], bone_xyz[ZZ]);
        bone_xyz[XX] -= joint_position_data[XX];
        bone_xyz[YY] -= joint_position_data[YY];
        bone_xyz[ZZ] -= joint_position_data[ZZ];


        // Calculate new front normal by crossing side and bone
        cross_product(bone_xyz, side_xyz, new_front_xyz);
        distance = vector_length(new_front_xyz);

        if (distance > 0.00001f)
        {
            new_front_xyz[XX] /= distance;  new_front_xyz[YY] /= distance;  new_front_xyz[ZZ] /= distance;
        }
        else
        {
            new_front_xyz[YY] = 1.0f;
        }

        // Calculate new side normal by crossing front and bone
        cross_product(bone_xyz, new_front_xyz, new_side_xyz);
        distance = vector_length(new_side_xyz);

        if (distance > 0.00001f)
        {
            new_side_xyz[XX] /= distance;  new_side_xyz[YY] /= distance;  new_side_xyz[ZZ] /= distance;
        }
        else
        {
            new_side_xyz[XX] = 1.0f;
        }

        // Only apply changes if past a certain threshold...
        distance =  FABS(side_xyz[XX] - new_side_xyz[XX]);
        distance += FABS(side_xyz[YY] - new_side_xyz[YY]);
        distance += FABS(side_xyz[ZZ] - new_side_xyz[ZZ]);
        distance += FABS(front_xyz[XX] - new_front_xyz[XX]);
        distance += FABS(front_xyz[YY] - new_front_xyz[YY]);
        distance += FABS(front_xyz[ZZ] - new_front_xyz[ZZ]);

        if (distance > 0.01f)
        {
            side_xyz[XX] = -new_side_xyz[XX];
            side_xyz[YY] = -new_side_xyz[YY];
            side_xyz[ZZ] = -new_side_xyz[ZZ];
            front_xyz[XX] = new_front_xyz[XX];
            front_xyz[YY] = new_front_xyz[YY];
            front_xyz[ZZ] = new_front_xyz[ZZ];
        }
    }




    /*
    distance = vector_length(bone_xyz);
    if(distance > 0.001f)
    {
    bone_xyz[XX]/=distance;
    bone_xyz[YY]/=distance;
    bone_xyz[ZZ]/=distance;


    // Determine new front normal by pushing and pulling it until it is the correct distance from the bone vector...
    repeat(j, 20)
    {
    spring_xyz[XX] = front_xyz[XX] - bone_xyz[XX];
    spring_xyz[YY] = front_xyz[YY] - bone_xyz[YY];
    spring_xyz[ZZ] = front_xyz[ZZ] - bone_xyz[ZZ];
    distance = vector_length(spring_xyz);
    if(distance > 0.001f)
    {
    distance = SQRT_2 / distance;
    front_xyz[XX] = bone_xyz[XX] + (spring_xyz[XX] * distance);
    front_xyz[YY] = bone_xyz[YY] + (spring_xyz[YY] * distance);
    front_xyz[ZZ] = bone_xyz[ZZ] + (spring_xyz[ZZ] * distance);
    distance = vector_length(front_xyz);
    if(distance > 0.001f)
    {
    front_xyz[XX]/=distance;
    front_xyz[YY]/=distance;
    front_xyz[ZZ]/=distance;
    }
    }


    // Try to keep the front normal from moving in the wrong plane...
    if(keep[XX])
    {
    front_xyz[XX] = old_front_xyz[XX];
    }
    if(keep[YY])
    {
    front_xyz[YY] = old_front_xyz[YY];
    }
    if(keep[ZZ])
    {
    front_xyz[ZZ] = old_front_xyz[ZZ];
    }
    }



    // Calculate side normal with a cross product
    cross_product(front_xyz, bone_xyz, side_xyz);
    distance = vector_length(side_xyz);
    if(distance > 0.001f)
    {
    side_xyz[XX]/=distance;
    side_xyz[YY]/=distance;
    side_xyz[ZZ]/=distance;
    }
    }
    }
    */

}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint8 render_insert_tex_vertex(Uint8* data, Uint16 frame, float* coordinates_xy, Uint16 tex_vertex_to_delete, Uint16 tex_vertex_replacement)
{
    // <ZZ> This function adds a new texture vertex to a model file.  If coordinates_xy is NULL,
    //      it deletes tex_vertex_to_delete, instead of adding one.  Returns ktrue if it worked,
    //      kfalse otherwise.  If tex_vertex_replacement is != MAX_TEX_VERTEX, and we're deleting,
    //      then a new tex_vertex is substituted for the old one...
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_tex_vertex;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* tex_vertex_data;
    Uint8* texture_data;
    Uint8* texture_data_start;
    Uint16 num_primitive, num_primitive_vertex;
    Uint8 texture_mode;
    Uint8 texture_flags;
    Uint16 i, j, k;
    Sint16 amount_to_add;
    Uint8 base_model, detail_level;



    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return kfalse;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 4;
    tex_vertex_data =  DEREF( Uint8*, data );  data += 4;
    texture_data = DEREF( Uint8*, data );

    base_model_data += 2;
    num_tex_vertex = DEREF( Uint16, base_model_data );
    amount_to_add = 0;

    if (coordinates_xy != NULL)
    {
        // Insert a texture vertex
        if (sdf_insert_data(tex_vertex_data + (num_tex_vertex << 3), NULL, 8))
        {
            // Added the tex vertex successfully...  Write coordinates
            DEREF( float, tex_vertex_data + (num_tex_vertex << 3) ) = coordinates_xy[XX];
            DEREF( float, tex_vertex_data + 4 + (num_tex_vertex << 3) ) = coordinates_xy[YY];


            // Select the new vertex
            select_add(num_tex_vertex, ((float*) (tex_vertex_data + (num_tex_vertex << 3))));


            // Update number of tex vertices for this base model
            num_tex_vertex++;
            DEREF( Uint16, base_model_data ) = num_tex_vertex;
            amount_to_add = 8;
        }
    }
    else
    {
        // Delete a tex vertex
        if (tex_vertex_to_delete < num_tex_vertex && num_tex_vertex > 1)
        {
            log_info(1, "Deleting tex vertex %d", tex_vertex_to_delete);
            // Make sure the tex vertex isn't in use
            texture_data_start = texture_data;
            repeat(i, MAX_DDD_TEXTURE)
            {
                texture_mode = *texture_data;  texture_data++;

                if (texture_mode != 0)
                {
                    texture_flags = *texture_data;
                    texture_data += 2;

                    // Strips...
                    num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                    repeat(j, num_primitive)
                    {
                        num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                        repeat(k, num_primitive_vertex)
                        {
                            texture_data += 2;

                            if (tex_vertex_to_delete == DEREF( Uint16, texture_data ))
                            {
                                if (tex_vertex_replacement == MAX_TEX_VERTEX)
                                {
                                    return kfalse;
                                }
                                else
                                {
                                    // Replace the tex vertex with a lower one...
                                    DEREF( Uint16, texture_data ) = tex_vertex_replacement;
                                }
                            }

                            texture_data += 2;
                        }
                    }


                    // Fans...  Shouldn't be any in DEVTOOL mode...
                    num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                    repeat(j, num_primitive)
                    {
                        num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                        repeat(k, num_primitive_vertex)
                        {
                            texture_data += 4;
                        }
                    }
                }
            }
            log_info(1, "Tex vertex not in use...  Safe to delete");


            // Delete the texture vertex
            if (sdf_insert_data(tex_vertex_data + (tex_vertex_to_delete << 3), NULL, -8))
            {
                log_info(1, "Deleted 8 bytes");

                // Renumber higher tex vertices
                texture_data = texture_data_start - 8;
                repeat(i, MAX_DDD_TEXTURE)
                {
                    texture_mode = *texture_data;  texture_data++;

                    if (texture_mode != 0)
                    {
                        log_info(1, "Texture %d is on", i);
                        texture_data += 2;


                        // Strips...
                        num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                        log_info(1, "%d strips", num_primitive);
                        repeat(j, num_primitive)
                        {
                            num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(k, num_primitive_vertex)
                            {
                                texture_data += 2;

                                if (tex_vertex_to_delete < DEREF( Uint16, texture_data )) DEREF( Uint16, texture_data ) -= 1;

                                texture_data += 2;
                            }
                        }


                        // Fans...  Shouldn't be any in DEVTOOL mode...
                        num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                        log_info(1, "%d fans", num_primitive);
                        repeat(j, num_primitive)
                        {
                            num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(k, num_primitive_vertex)
                            {
                                texture_data += 4;
                            }
                        }
                    }
                }



                // Update number of tex vertices for this base model
                num_tex_vertex--;
                DEREF( Uint16, base_model_data ) = num_tex_vertex;
                amount_to_add = -8;
                log_info(1, "Done deleting...  Just gotta finish up");
                log_info(1, "Left with %d tex vertices...", num_tex_vertex);
            }
        }
    }


    // Finish up...
    if (amount_to_add != 0)
    {
        // Update all base model pointers at start of file
        DEREF( Uint8*, data ) += amount_to_add;  data += 4;
        DEREF( Uint8*, data ) += amount_to_add;  data += 4;
        DEREF( Uint8*, data ) += amount_to_add;  data += 4;
        base_model++;

        while (base_model < num_base_model)
        {
            DEREF( Uint8*, data ) += amount_to_add;  data += 4;
            DEREF( Uint8*, data ) += amount_to_add;  data += 4;
            DEREF( Uint8*, data ) += amount_to_add;  data += 4;
            DEREF( Uint8*, data ) += amount_to_add;  data += 4;
            DEREF( Uint8*, data ) += amount_to_add;  data += 4;
            base_model++;
        }


        // Update bone frame pointers, only if bone frames are internal to this file (not linked to another RDY)
        if ((flags & DDD_EXTERNAL_BONE_FRAMES) == 0)
        {
            repeat(i, num_bone_frame)
            {
                DEREF( Uint8*, data ) += amount_to_add;  data += 4;
            }
        }

        return ktrue;
    }

    return kfalse;
}
#endif

//-----------------------------------------------------------------------------------------------
Uint8 render_pregenerate_normals(Uint8* data, Uint16 frame, Uint8 detail_level)
{
    // <ZZ> This function generates vertex normal info for an RDY file.  It also swaps
    //      bone attachments if the second attachment is higher weighted than the
    //      first, as normals only attach to the first bone...  Normals should
    //      attach to the bone of higher weight...  Returns ktrue if it worked,
    //      kfalse if not (because frame invalid).
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 base_model;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* texture_data;
    Uint8* vertex_data_start;
    Uint8* bone_data_start;
    float* bone_normal_data_start;
    float* joint_data_start;
    Uint16 num_primitive, num_primitive_vertex, num_vertex, num_joint, num_bone;
    Uint8 texture_mode;
    Uint16 i, j, k, m;
    Uint16 vertex[3];
    Uint16 joint[2];
    Uint16 bone;
    float length, height, front, side;
    float* normal_xyz;
    float start_xyz[3];
    float end_xyz[3];
    float front_xyz[3];
    float side_xyz[3];
    float height_xyz[3];
    float point_xyz[3];
    Uint8 tex_alpha;


    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return kfalse;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);



    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    frame_data += 11;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 8;
    texture_data = DEREF( Uint8*, data ); data += 8;
    bone_data_start = DEREF( Uint8*, data );


    num_vertex = DEREF( Uint16, base_model_data );
    num_joint = DEREF( Uint16, base_model_data + 4 );
    num_bone = DEREF( Uint16, base_model_data + 6 );
    vertex_data_start = base_model_data + 8;
    bone_normal_data_start = (float*) frame_data;
    joint_data_start = (float*) (frame_data + (num_bone * 24));


    // Clear out the normals for each vertex (swap bone bindings if need be)
    repeat(i, num_vertex)
    {
        normal_xyz = ((float*) (vertex_data_start + 15 + (i << 6)));
        normal_xyz[XX] = 0.0f;
        normal_xyz[YY] = 0.0f;
        normal_xyz[ZZ] = 0.0f;

        //        if(*(vertex_data_start+14+(i<<6)) < 128)
        if (((Uint8) ((*(vertex_data_start + 14 + (i << 6))) << 1)) < 124) // !!!ANCHOR!!!
        {
            // Swap the bindings
            //            *(vertex_data_start+14+(i<<6)) = 255 - *(vertex_data_start+14+(i<<6));
            *(vertex_data_start + 14 + (i << 6)) = (127 - ((*(vertex_data_start + 14 + (i << 6))) & 127)) | ((*(vertex_data_start + 14 + (i << 6))) & 128);  // !!!ANCHOR!!!


            k = *(vertex_data_start + 12 + (i << 6));
            *(vertex_data_start + 12 + (i << 6)) = *(vertex_data_start + 13 + (i << 6));
            *(vertex_data_start + 13 + (i << 6)) = (Uint8) k;
            // Swap the scalars
            front_xyz[XX] = DEREF( float, vertex_data_start + 27 + (i << 6) );
            front_xyz[YY] = DEREF( float, vertex_data_start + 35 + (i << 6) );
            front_xyz[ZZ] = DEREF( float, vertex_data_start + 43 + (i << 6) );
            DEREF( float, vertex_data_start + 27 + (i << 6) ) = DEREF( float, vertex_data_start + 31 + (i << 6) );
            DEREF( float, vertex_data_start + 35 + (i << 6) ) = DEREF( float, vertex_data_start + 39 + (i << 6) );
            DEREF( float, vertex_data_start + 43 + (i << 6) ) = DEREF( float, vertex_data_start + 47 + (i << 6) );
            DEREF( float, vertex_data_start + 31 + (i << 6) ) = front_xyz[XX];
            DEREF( float, vertex_data_start + 39 + (i << 6) ) = front_xyz[YY];
            DEREF( float, vertex_data_start + 47 + (i << 6) ) = front_xyz[ZZ];
        }
    }

    // Go through each texture triangle strip and fan, accumulating normal data as we go...
    repeat(i, MAX_DDD_TEXTURE)
    {
        texture_mode = *texture_data;  texture_data++;

        if (texture_mode != 0)
        {
            texture_data++;     // Skip flags
            tex_alpha = *texture_data;  texture_data++;


            // Strips...
            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
            repeat(j, num_primitive)
            {
                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                repeat(k, num_primitive_vertex)
                {
                    vertex[k%3] = DEREF( Uint16, texture_data );  texture_data += 4;


                    if (k > 1)
                    {
                        // We have a triangle...  Calculate its normal, then accumulate into each vertex...
                        start_xyz[XX] = DEREF( float, vertex_data_start + (vertex[0] << 6) );
                        start_xyz[YY] = DEREF( float, vertex_data_start + 4 + (vertex[0] << 6) );
                        start_xyz[ZZ] = DEREF( float, vertex_data_start + 8 + (vertex[0] << 6) );
                        end_xyz[XX] = DEREF( float, vertex_data_start + (vertex[1] << 6) );
                        end_xyz[YY] = DEREF( float, vertex_data_start + 4 + (vertex[1] << 6) );
                        end_xyz[ZZ] = DEREF( float, vertex_data_start + 8 + (vertex[1] << 6) );
                        end_xyz[XX] -= start_xyz[XX];
                        end_xyz[YY] -= start_xyz[YY];
                        end_xyz[ZZ] -= start_xyz[ZZ];
                        side_xyz[XX] = DEREF( float, vertex_data_start + (vertex[2] << 6) );
                        side_xyz[YY] = DEREF( float, vertex_data_start + 4 + (vertex[2] << 6) );
                        side_xyz[ZZ] = DEREF( float, vertex_data_start + 8 + (vertex[2] << 6) );
                        side_xyz[XX] -= start_xyz[XX];
                        side_xyz[YY] -= start_xyz[YY];
                        side_xyz[ZZ] -= start_xyz[ZZ];
                        cross_product(end_xyz, side_xyz, front_xyz);

                        if (k&1)
                        {
                            front_xyz[XX] = -front_xyz[XX];
                            front_xyz[YY] = -front_xyz[YY];
                            front_xyz[ZZ] = -front_xyz[ZZ];
                        }

                        if (tex_alpha < 255)
                        {
                            // Make multipass textures not mess up cartoon lines...
                            front_xyz[XX] *= 0.125f;
                            front_xyz[YY] *= 0.125f;
                            front_xyz[ZZ] *= 0.125f;
                        }

                        repeat(m, 3)
                        {
                            normal_xyz = ((float*) (vertex_data_start + 15 + (vertex[m] << 6)));
                            normal_xyz[XX] += front_xyz[XX];
                            normal_xyz[YY] += front_xyz[YY];
                            normal_xyz[ZZ] += front_xyz[ZZ];
                        }
                    }
                }
            }


            // Fans...
            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
            repeat(j, num_primitive)
            {
                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                repeat(k, num_primitive_vertex)
                {
                    if (k == 0)
                    {
                        vertex[k] = DEREF( Uint16, texture_data );
                    }
                    else
                    {
                        vertex[2-(k&1)] = DEREF( Uint16, texture_data );
                    }

                    texture_data += 4;

                    if (k > 1)
                    {
                        // We have a triangle...  Calculate its normal, then accumulate into each vertex...
                        start_xyz[XX] = DEREF( float, vertex_data_start + (vertex[0] << 6) );
                        start_xyz[YY] = DEREF( float, vertex_data_start + 4 + (vertex[0] << 6) );
                        start_xyz[ZZ] = DEREF( float, vertex_data_start + 8 + (vertex[0] << 6) );
                        end_xyz[XX] = DEREF( float, vertex_data_start + (vertex[1] << 6) );
                        end_xyz[YY] = DEREF( float, vertex_data_start + 4 + (vertex[1] << 6) );
                        end_xyz[ZZ] = DEREF( float, vertex_data_start + 8 + (vertex[1] << 6) );
                        end_xyz[XX] -= start_xyz[XX];
                        end_xyz[YY] -= start_xyz[YY];
                        end_xyz[ZZ] -= start_xyz[ZZ];
                        side_xyz[XX] = DEREF( float, vertex_data_start + (vertex[2] << 6) );
                        side_xyz[YY] = DEREF( float, vertex_data_start + 4 + (vertex[2] << 6) );
                        side_xyz[ZZ] = DEREF( float, vertex_data_start + 8 + (vertex[2] << 6) );
                        side_xyz[XX] -= start_xyz[XX];
                        side_xyz[YY] -= start_xyz[YY];
                        side_xyz[ZZ] -= start_xyz[ZZ];
                        cross_product(end_xyz, side_xyz, front_xyz);

                        if (tex_alpha < 255)
                        {
                            // Make multipass textures not mess up cartoon lines...
                            front_xyz[XX] *= 0.125f;
                            front_xyz[YY] *= 0.125f;
                            front_xyz[ZZ] *= 0.125f;
                        }

                        repeat(m, 3)
                        {
                            normal_xyz = ((float*) (vertex_data_start + 15 + (vertex[m] << 6)));
                            normal_xyz[XX] += front_xyz[XX];
                            normal_xyz[YY] += front_xyz[YY];
                            normal_xyz[ZZ] += front_xyz[ZZ];
                        }
                    }
                }
            }
        }
        else
        {
        }
    }


    // Normalize the normals for each vertex, and calculate the bone binding scalars
    repeat(i, num_vertex)
    {
        normal_xyz = ((float*) (vertex_data_start + 15 + (i << 6)));
        length = vector_length(normal_xyz);

        if (length > 0.01f)
        {
            normal_xyz[XX] /= length;
            normal_xyz[YY] /= length;
            normal_xyz[ZZ] /= length;
        }

        bone = *(vertex_data_start + 12 + (i << 6));
        joint[0] = DEREF( Uint16, bone_data_start + 1 + (bone * 9) );
        joint[1] = DEREF( Uint16, bone_data_start + 3 + (bone * 9) );


        // Yucky stuff...  Similar to crunch_vertex...
        start_xyz[XX] = *(joint_data_start + (joint[0] * 3));
        start_xyz[YY] = *(joint_data_start + 1 + (joint[0] * 3));
        start_xyz[ZZ] = *(joint_data_start + 2 + (joint[0] * 3));

        end_xyz[XX] = *(joint_data_start + (joint[1] * 3));
        end_xyz[YY] = *(joint_data_start + 1 + (joint[1] * 3));
        end_xyz[ZZ] = *(joint_data_start + 2 + (joint[1] * 3));

        front_xyz[XX] = DEREF( float, bone_normal_data_start + (bone * 6) );
        front_xyz[YY] = DEREF( float, bone_normal_data_start + 1 + (bone * 6) );
        front_xyz[ZZ] = DEREF( float, bone_normal_data_start + 2 + (bone * 6) );
        side_xyz[XX] = DEREF( float, bone_normal_data_start + 3 + (bone * 6) );
        side_xyz[YY] = DEREF( float, bone_normal_data_start + 4 + (bone * 6) );
        side_xyz[ZZ] = DEREF( float, bone_normal_data_start + 5 + (bone * 6) );

        // Point to find scalars for...
        point_xyz[XX] = normal_xyz[XX] + start_xyz[XX];
        point_xyz[YY] = normal_xyz[YY] + start_xyz[YY];
        point_xyz[ZZ] = normal_xyz[ZZ] + start_xyz[ZZ];

        // Vector from bone start to bone end
        end_xyz[XX] -= start_xyz[XX];
        end_xyz[YY] -= start_xyz[YY];
        end_xyz[ZZ] -= start_xyz[ZZ];

        // Vector from bone start to point
        height_xyz[XX] = normal_xyz[XX];
        height_xyz[YY] = normal_xyz[YY];
        height_xyz[ZZ] = normal_xyz[ZZ];

        // Calculate height value
        length = dot_product(end_xyz, end_xyz);
        height = dot_product(end_xyz, height_xyz);

        if (length > 0) height = height / length;
        else height = 0;

        // Calculate the height position
        height_xyz[XX] = start_xyz[XX] + (end_xyz[XX] * height);
        height_xyz[YY] = start_xyz[YY] + (end_xyz[YY] * height);
        height_xyz[ZZ] = start_xyz[ZZ] + (end_xyz[ZZ] * height);

        // Calculate vector from height position (along bone) to point
        height_xyz[XX] = point_xyz[XX] - height_xyz[XX];
        height_xyz[YY] = point_xyz[YY] - height_xyz[YY];
        height_xyz[ZZ] = point_xyz[ZZ] - height_xyz[ZZ];

        // Calculate front and side values
        front = dot_product(front_xyz, height_xyz);
        side = dot_product(side_xyz, height_xyz);

        // Write the normal scalars
        normal_xyz = ((float*) (vertex_data_start + 51 + (i << 6)));
        normal_xyz[XX] = height;
        normal_xyz[YY] = front;
        normal_xyz[ZZ] = side;
    }
    return ktrue;
}

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void clean_up_tex_vertex(Uint8* data, Uint16 frame)
{
    // <ZZ> This function deletes all unused tex vertices in a given model.
    Uint16 i;
    select_clear();
    repeat(i, MAX_VERTEX)
    {
        if (render_insert_tex_vertex(data, frame, NULL, i, MAX_TEX_VERTEX))
        {
            // Successfully deleted a tex vertex...  Later ones pushed back...
            i--;
        }
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void remove_all_tex_vertex(Uint8* data, Uint16 frame)
{
    // <ZZ> This function deletes all tex vertices in a given model.  Except 0.
    Uint16 i;
    select_clear();
    log_info(0, "Detexture...");
    i = 1;

    while (i < MAX_VERTEX)
    {
        if (render_insert_tex_vertex(data, frame, NULL, i, 0))
        {
            // Successfully deleted a tex vertex...  Later ones pushed back...
            i--;
        }

        i++;
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_joint_size(Uint8* data, Uint16 frame, Uint16 joint, Uint8 size)
{
    // <ZZ> This function sets the size of a given joint
    Uint8* frame_data;
    Uint8* joint_data;
    Uint8 base_model;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_joint;
    Uint8 detail_level;

    data += 3;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);

    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;

    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    joint_data = DEREF( Uint8*, data );
    num_joint = DEREF( Uint16, joint_data + 4 );
    joint_data = DEREF( Uint8*, data + 12 );

    if (joint < num_joint)
    {
        joint_data += (joint << 2);
        DEREF( float, joint_data ) = size * JOINT_COLLISION_SCALE;
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_attach_vertex_to_bone(Uint8* data, Uint16 frame, Uint16 vertex)
{
    // <ZZ> This function attaches a vertex to the nearest bone or nearest two bones...  Will
    //      not attach to special bones (weapon grips), unless there are no other options...
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_vertex, num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* bone_data;
    Uint16 i, j, k, joint;
    Uint8 base_model, detail_level;
    Uint8 found_match;
    float joint_distance[MAX_JOINT];
    Uint8 joint_order[MAX_JOINT];
    float* joint_xyz;
    float vector_xyz[3];
    float vertex_xyz[3];
    Uint16 num_normal_bone;



    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 4;
    bone_data =  DEREF( Uint8*, data + 12 );
    num_vertex = DEREF( Uint16, base_model_data ); base_model_data += 4;

    if (vertex >= num_vertex) return;

    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;


    // Figure out how many non-grip bones there are...
    num_normal_bone = 0;
    repeat(i, num_bone)
    {
        if (bone_data[i*9] == 0)
        {
            num_normal_bone++;
        }
    }

    // Figure out coordinates of current vertex (assume that it has been updated recently...)
    vertex_xyz[XX] = DEREF( float, base_model_data + (vertex << 6) );
    vertex_xyz[YY] = DEREF( float, base_model_data + 4 + (vertex << 6) );
    vertex_xyz[ZZ] = DEREF( float, base_model_data + 8 + (vertex << 6) );
    log_info(0, "Attaching vertex...");
    log_info(0, "Prior Actual = (%f, %f, %f)", vertex_xyz[XX], vertex_xyz[YY], vertex_xyz[ZZ]);


    // Make a list of joint distances, then order 'em nearest first...
    joint_xyz = (float*) (frame_data + 11 + (num_bone * 24));

    if (num_joint > MAX_JOINT)  num_joint = MAX_JOINT;

    repeat(i, num_joint)
    {
        vector_xyz[XX] = joint_xyz[XX] - vertex_xyz[XX];
        vector_xyz[YY] = joint_xyz[YY] - vertex_xyz[YY];
        vector_xyz[ZZ] = joint_xyz[ZZ] - vertex_xyz[ZZ];
        joint_distance[i] = vector_length(vector_xyz);
        joint_xyz += 3;
    }
    repeat(i, num_joint)
    {
        k = 0;  // Count of how many are before this one...
        j = 0;

        while (j < i)
        {
            if (joint_distance[i] >= joint_distance[j]) k++;

            j++;
        }

        j++;  // Skip the current joint...  Don't compare with self...

        while (j < num_joint)
        {
            if (joint_distance[i] > joint_distance[j]) k++;

            j++;
        }

        joint_order[k] = (Uint8) i;
    }


    // Find the nearest joint that's connected to another...
    found_match = kfalse;
    i = 0;

    while (found_match == kfalse && i < num_joint)
    {
        j = joint_order[i];
        repeat(k, num_bone)
        {
            if (num_normal_bone > 0)
            {
                if (j == DEREF( Uint16, bone_data + 1 + (k*9) ) || j == DEREF( Uint16, bone_data + 3 + (k*9) ))
                {
                    // Only pick non-grip joints...
                    if (*(bone_data + (k*9)) == 0)
                    {
                        joint = j;
                        found_match = ktrue;
                        k = num_bone;
                    }
                }
            }
            else
            {
                if (j == DEREF( Uint16, bone_data + 1 + (k*9) ) || j == DEREF( Uint16, bone_data + 3 + (k*9) ))
                {
                    // Pick any joint
                    joint = j;
                    found_match = ktrue;
                    k = num_bone;
                }
            }
        }
        i++;
    }

    log_info(0, "Nearest joint with a bone is %d", joint);


    // Go through every joint after, looking for nearest 2 connected bones...
    if (found_match)
    {
        found_match = 0;

        while (found_match < 2 && i < num_joint)
        {
            j = joint_order[i];
            repeat(k, num_bone)
            {
                if (joint == DEREF( Uint16, bone_data + 1 + (k*9) ) || joint == DEREF( Uint16, bone_data + 3 + (k*9) ))
                {
                    if (j == DEREF( Uint16, bone_data + 1 + (k*9) ) || j == DEREF( Uint16, bone_data + 3 + (k*9) ))
                    {
                        // Only take non-grip bones...
                        if (bone_data[k*9] == 0 || num_normal_bone == 0)
                        {
                            // Record the bone...
                            *(base_model_data + 12 + found_match + (vertex << 6)) = (Uint8) k;
                            log_info(0, "Attached to bone %d", k);
                            found_match++;
                            k = num_bone;
                        }
                    }
                }
            }
            i++;
        }
    }



    // If we only found one bone, force the weight to be full 255...
    if (found_match == 1)
    {
        *(base_model_data + 13 + (vertex << 6)) = *(base_model_data + 12 + (vertex << 6));
        //        *(base_model_data+14+(vertex<<6)) = 255;
        *(base_model_data + 14 + (vertex << 6)) = 127 | ((*(base_model_data + 14 + (vertex << 6))) & 128);  // Max weight is 127...  Anchor bit is topmost...  !!!ANCHOR!!!
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_solid_box(Uint8* color, float tlx, float tly, float tlz, float brx, float bry, float brz)
{
    // <ZZ> This function draws a 3D box for debug purposes...
    display_texture_off();
    display_blend_off();
    display_shade_off();
    display_color(color);


    // Top...
    display_start_fan();
    {
        display_vertex_xyz(tlx, tly, tlz);
        display_vertex_xyz(brx, tly, tlz);
        display_vertex_xyz(brx, bry, tlz);
        display_vertex_xyz(tlx, bry, tlz);
    }
    display_end();


    // Bottom...
    display_start_fan();
    {
        display_vertex_xyz(tlx, tly, brz);
        display_vertex_xyz(tlx, bry, brz);
        display_vertex_xyz(brx, bry, brz);
        display_vertex_xyz(brx, tly, brz);
    }
    display_end();


    // Front...
    display_start_fan();
    {
        display_vertex_xyz(tlx, bry, tlz);
        display_vertex_xyz(brx, bry, tlz);
        display_vertex_xyz(brx, bry, brz);
        display_vertex_xyz(tlx, bry, brz);
    }
    display_end();


    // Back...
    display_start_fan();
    {
        display_vertex_xyz(tlx, tly, tlz);
        display_vertex_xyz(tlx, tly, brz);
        display_vertex_xyz(brx, tly, brz);
        display_vertex_xyz(brx, tly, tlz);
    }
    display_end();



    // Left...
    display_start_fan();
    {
        display_vertex_xyz(tlx, tly, tlz);
        display_vertex_xyz(tlx, bry, tlz);
        display_vertex_xyz(tlx, bry, brz);
        display_vertex_xyz(tlx, tly, brz);
    }
    display_end();


    // Right...
    display_start_fan();
    {
        display_vertex_xyz(brx, tly, tlz);
        display_vertex_xyz(brx, tly, brz);
        display_vertex_xyz(brx, bry, brz);
        display_vertex_xyz(brx, bry, tlz);
    }
    display_end();


    // Draw edge lines...
    display_color(white);
    display_start_line();
    {
        display_vertex_xyz(tlx, tly, tlz);
        display_vertex_xyz(brx, tly, tlz);

        display_vertex_xyz(tlx, bry, tlz);
        display_vertex_xyz(brx, bry, tlz);

        display_vertex_xyz(tlx, tly, brz);
        display_vertex_xyz(brx, tly, brz);

        display_vertex_xyz(tlx, bry, brz);
        display_vertex_xyz(brx, bry, brz);



        display_vertex_xyz(tlx, tly, tlz);
        display_vertex_xyz(tlx, bry, tlz);

        display_vertex_xyz(brx, tly, tlz);
        display_vertex_xyz(brx, bry, tlz);

        display_vertex_xyz(tlx, tly, brz);
        display_vertex_xyz(tlx, bry, brz);

        display_vertex_xyz(brx, tly, brz);
        display_vertex_xyz(brx, bry, brz);



        display_vertex_xyz(tlx, tly, tlz);
        display_vertex_xyz(tlx, tly, brz);

        display_vertex_xyz(brx, tly, tlz);
        display_vertex_xyz(brx, tly, brz);

        display_vertex_xyz(tlx, bry, tlz);
        display_vertex_xyz(tlx, bry, brz);

        display_vertex_xyz(brx, bry, tlz);
        display_vertex_xyz(brx, bry, brz);
    }
    display_end();
}
#endif

//-----------------------------------------------------------------------------------------------

#define MAX_CARTOON_LINE 8192
Uint8 draw_cartoon_line[MAX_CARTOON_LINE];
Uint8 cartoon_line_alpha[MAX_CARTOON_LINE];  // Used in room drawing...


//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
float global_gnomify_working_direction_xyz[3];
void render_gnomify_working_direction(Uint8* data, Uint16 frame, Uint16 joint_one, Uint16 joint_two)
{
    // <ZZ> This function sets the working direction (a vector from one joint to another) for moving
    //      around joints for the modeler's g'nomify tool...
    Uint8 num_detail_level, num_base_model, base_model, detail_level;
    Uint16 num_bone_frame, num_joint, num_bone;
    Uint8* frame_data;
    float* joint_one_xyz;
    float* joint_two_xyz;

    // Default to 0, 0, 0 if we fail...
    global_gnomify_working_direction_xyz[XX] = 0.0f;
    global_gnomify_working_direction_xyz[YY] = 0.0f;
    global_gnomify_working_direction_xyz[ZZ] = 0.0f;

    // Read the RDY file's header...
    data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) { return; }

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    data = DEREF( Uint8*, data );
    data += 4;
    num_joint = DEREF( Uint16, data );  data += 2;
    num_bone =  DEREF( Uint16, data );

    if (joint_one >= num_joint || joint_two >= num_joint) { return; }

    // Find the joints...
    frame_data += 11;  // Skip bone_frame header...
    frame_data += 24 * num_bone;
    joint_one_xyz = (float*) (frame_data + (12 * joint_one));
    joint_two_xyz = (float*) (frame_data + (12 * joint_two));

    // Find the vector...
    global_gnomify_working_direction_xyz[XX] = joint_two_xyz[XX] - joint_one_xyz[XX];
    global_gnomify_working_direction_xyz[YY] = joint_two_xyz[YY] - joint_one_xyz[YY];
    global_gnomify_working_direction_xyz[ZZ] = joint_two_xyz[ZZ] - joint_one_xyz[ZZ];
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_gnomify_affect_joint(Uint8* data, Uint16 frame, Uint16 joint, Uint8 percent)
{
    // <ZZ> This function offsets the specified joint's position by the working direction vector.
    //      Percent is used to scale the effect (0 is no effect, 100 is full effect).

    Uint8 num_detail_level, num_base_model, base_model, detail_level;
    Uint16 num_bone_frame, num_joint, num_bone;
    Uint8* frame_data;
    float* joint_xyz;
    float scale;

    // Read the RDY file's header...
    data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) { return; }

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    data = DEREF( Uint8*, data );
    data += 4;
    num_joint = DEREF( Uint16, data );  data += 2;
    num_bone =  DEREF( Uint16, data );

    if (joint >= num_joint) { return; }

    // Find the joints...
    frame_data += 11;  // Skip bone_frame header...
    frame_data += 24 * num_bone;
    joint_xyz = (float*) (frame_data + (12 * joint));

    // Apply the vector...
    scale = percent * 0.01f;
    joint_xyz[XX] += global_gnomify_working_direction_xyz[XX] * scale;
    joint_xyz[YY] += global_gnomify_working_direction_xyz[YY] * scale;
    joint_xyz[ZZ] += global_gnomify_working_direction_xyz[ZZ] * scale;
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_joint_from_vertex_location(Uint8* data, Uint16 frame)
{
    // <ZZ> This function moves a specified joint to be at the same exact position as the given vertex...
    //      Useful for adding weapon bones to models after they've been animated (because I was too lazy
    //      to do it the right way in the first place)...  Assumes last 4 vertices and last 4 joints...
    Uint8 num_detail_level, num_base_model, base_model, detail_level;
    Uint16 num_bone_frame, num_joint, num_bone, num_vertex;
    Uint8* frame_data;
    Uint8* vertex_data;
    Uint16 joint, vertex;
    Uint16 i;
    float* joint_xyz;
    float* vertex_xyz;

    // Default to 0, 0, 0 if we fail...
    global_gnomify_working_direction_xyz[XX] = 0.0f;
    global_gnomify_working_direction_xyz[YY] = 0.0f;
    global_gnomify_working_direction_xyz[ZZ] = 0.0f;

    // Read the RDY file's header...
    data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) { return; }

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    data = DEREF( Uint8*, data );
    num_vertex = DEREF( Uint16, data );  data += 4;
    num_joint = DEREF( Uint16, data );  data += 2;
    num_bone =  DEREF( Uint16, data );  data += 2;
    vertex_data = data;

    if (num_joint < 4 || num_vertex < 4) { return; }

    joint = num_joint - 4;
    vertex = num_vertex - 4;
    frame_data += 11;  // Skip bone_frame header...
    frame_data += 24 * num_bone;


    repeat(i, 4)
    {
        // Find the vertex location...
        vertex_xyz = (float*) (vertex_data + (64 * vertex));


        // Find the joint location...
        joint_xyz = (float*) (frame_data + (12 * joint));


        // Modify the joint location...
        joint_xyz[XX] = vertex_xyz[XX];
        joint_xyz[YY] = vertex_xyz[YY];
        joint_xyz[ZZ] = vertex_xyz[ZZ];
        joint++;
        vertex++;
    }
}
#endif

//-----------------------------------------------------------------------------------------------
Uint8 global_rdy_show_joint_numbers = ktrue;
Uint8 global_rdy_detail_level = 0;
Uint16 global_num_bone = 0;    // Yicky thing for helping models in scripts
Uint8* global_bone_data = NULL;   // Yicky thing for helping models in scripts
float eye_quad_xy[4][2] = { {0.0f, 0.0f}, {0.0f, 0.5f}, {0.5f, 0.0f}, {0.5f, 0.5f} };
Uint8 eye_frame_quad[32] = {0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 2, 2, 2, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
void render_rdy(SOULFU_WINDOW *pwin, Uint8* data, Uint16 frame, Uint8 mode, Uint8** texture_file_data_block, Uint8 main_alpha, Uint8* bone_frame_data, Uint8 petrify, Uint8 eye_frame)
{
    // <ZZ> This function draws an RDY object model.  If bone_frame_data is non-NULL, it is used
    //      to generate vertex locations using render_bone_frame...  Should skip first 11 bytes
    //      of bone frame...
    //      Eye frame is for the per-texture RENDER_EYE_FLAG for tex vertex offsets...  Should be
    //      from 0-31

    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_vertex;
    Uint16 num_tex_vertex;
    Uint16 num_joint;
    Uint16 num_bone;
    Uint8* base_model_data;
    Uint8* vertex_data;
    Uint8* tex_vertex_data;
    Uint8* texture_data;
    Uint8* joint_data;
    Uint8* bone_data;
    Uint8* frame_data;
    float* texture_matrix;
    Uint8 texture_mode;
    Uint8 texture_flags;
    Uint8 texture_alpha;
    Uint16 num_primitive;
    Uint16 num_primitive_vertex;
    Uint16 i, j, k;
    Uint16 vertex;
    Uint16 tex_vertex;
    Uint8 base_model, detail_level;
    Uint32 color_argument;
    float start_xy[2];
    float texpos_xy[2];
    float dot;
    float* onscreen_joint_xyss;
    float* inworld_joint_xyz;
    float distance_xyz[3];
    Uint8 we_have_to_draw_it;
    Uint8* texture_data_start;
    float distance_to_camera;
    float cartoon_line_size;


#ifndef BACKFACE_CARTOON_LINES
    float perp_xy[2];
    float* vertex_bxy;
#endif


#ifdef DEVTOOL
    float x, y, z, d;
    float best_depth;
    Uint8* start_data;
    Uint16 m;
    Uint8 found_connection;
    Uint8* best_data;
    Uint8* primitive_data;
    Uint16 best_vertex;
    Uint8 texture_to_skin;
    float triangle_xy[3][2];
    float tex_vertex_onscreen_xy[MAX_VERTEX][2];
    Uint16 vertex_to_tex_vertex[MAX_VERTEX];
    Uint16 tex_vertex_to_add;
    float onscreen_min_x, onscreen_max_x, onscreen_min_y, onscreen_max_y;
    Uint8* joint_position_data;
    PRT_DATA  temporary_particle_data;
    start_data = data;
#endif



    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) frame = 0;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);



    distance_to_camera = 0.0f;


    if (drawing_world)
    {
        distance_xyz[XX] = onscreen_joint_character_data->x - camera.xyz[XX];
        distance_xyz[YY] = onscreen_joint_character_data->y - camera.xyz[YY];
        distance_xyz[ZZ] = onscreen_joint_character_data->z + 3.00f - camera.xyz[ZZ];
        distance_to_camera = distance_xyz[XX] * camera.fore_xyz[XX] + distance_xyz[YY] * camera.fore_xyz[YY] + distance_xyz[ZZ] * camera.fore_xyz[ZZ];

        if (distance_to_camera < 0.001f)
        {
            distance_to_camera = 0.001f;
        }

        if (distance_to_camera > 160.0f)
        {
            distance_to_camera = 160.0f;
        }

        cartoon_line_size = (distance_to_camera + 60.0f) * CHAR_LINE_SIZE;
        detail_level = (Uint8) ((global_rdy_detail_level) * distance_to_camera / 160.0f);
    }
    else
    {
        cartoon_line_size = 0.125f;
        detail_level = global_rdy_detail_level;
    }



    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);

    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 4;
    tex_vertex_data = DEREF( Uint8*, data );  data += 4;
    texture_data = DEREF( Uint8*, data );  data += 4;
    joint_data = DEREF( Uint8*, data );  data += 4;
    bone_data = DEREF( Uint8*, data );


    vertex_data = base_model_data;
    num_vertex = DEREF( Uint16, vertex_data ); vertex_data += 2;
    num_tex_vertex = DEREF( Uint16, vertex_data ); vertex_data += 2;
    num_joint = DEREF( Uint16, vertex_data ); vertex_data += 2;
    num_bone = DEREF( Uint16, vertex_data ); vertex_data += 2;



    // Figure out if any of the joints are onscreen or not...
    if (onscreen_joint_active)
    {
#ifdef DEVTOOL

        // Draw joints...
        if (keyb.down[SDLK_F11])
        {
            display_texture_off();
            inworld_joint_xyz = (float*) (bone_frame_data + (num_bone << 4) + (num_bone << 3));
            repeat(i, num_joint)
            {
                display_solid_marker(dark_red, inworld_joint_xyz[XX], inworld_joint_xyz[YY], inworld_joint_xyz[ZZ], DEREF( float, joint_data + (i << 2) ));
                inworld_joint_xyz += 3;
            }
        }

#endif




        // Find the bone numbers for named bones...  Left, Right, Left2, Right2...  Also get
        // the frame event flags...
        repeat(i, 8)
        {
            temp_character_bone_number[i] = 255;
        }
        repeat(i, num_bone)
        {
            temp_character_bone_number[(*(bone_data+(i*9))) & 7] = (Uint8) i;
        }
        temp_character_frame_event = frame_data[1];




        // Go ahead and do the onscreen joint stuff...
        onscreen_joint_xyss = (float*) fourthbuffer;
        inworld_joint_xyz = (float*) (bone_frame_data + (num_bone << 4) + (num_bone << 3));
        num_onscreen_joint = 0;
        repeat(i, num_joint)
        {
            // Transform by the onscreen matrix...
            onscreen_joint_xyss[XX] = onscreen_matrix[0] * inworld_joint_xyz[XX] + onscreen_matrix[4] * inworld_joint_xyz[YY] + onscreen_matrix[8]  * inworld_joint_xyz[ZZ] + onscreen_matrix[12];
            onscreen_joint_xyss[YY] = onscreen_matrix[1] * inworld_joint_xyz[XX] + onscreen_matrix[5] * inworld_joint_xyz[YY] + onscreen_matrix[9]  * inworld_joint_xyz[ZZ] + onscreen_matrix[13];
            dot                    = onscreen_matrix[3] * inworld_joint_xyz[XX] + onscreen_matrix[7] * inworld_joint_xyz[YY] + onscreen_matrix[11] * inworld_joint_xyz[ZZ] + onscreen_matrix[15];

            if (dot != 0.0f)
            {
                onscreen_joint_xyss[XX] /= dot;      onscreen_joint_xyss[YY] /= dot;
                onscreen_joint_xyss[XX] *= 0.5f * DEFAULT_W;  onscreen_joint_xyss[XX] += 0.5f * DEFAULT_W;
                onscreen_joint_xyss[YY] *= -0.5f * DEFAULT_H;  onscreen_joint_xyss[YY] += 0.5f * DEFAULT_H;
                num_onscreen_joint++;
                dot = (ONSCREEN_CLICK_SCALE / dot);
                onscreen_joint_xyss[2] = (DEREF( float, joint_data + (i << 2) )) * dot;  // Joint size...
                onscreen_joint_xyss[3] = ((DEREF( float, joint_data + (i << 2) )) + 1.5f) * dot;  // Joint size...  Enlarged to make onscreen check pass easier...
                onscreen_joint_xyss += 4;
            }

            inworld_joint_xyz += 3;
        }


        // All onscreen locations figured...  Do we have to draw it?
        we_have_to_draw_it = kfalse;
        onscreen_joint_xyss = (float*) fourthbuffer;
        repeat(i, num_onscreen_joint)
        {
            if (onscreen_joint_xyss[XX] > -onscreen_joint_xyss[3] && onscreen_joint_xyss[YY] > -onscreen_joint_xyss[3] && onscreen_joint_xyss[3] > 0.0f)
            {
                if (onscreen_joint_xyss[XX] < (CAST(float, DEFAULT_W) + onscreen_joint_xyss[3]) && onscreen_joint_xyss[YY] < (CAST(float, DEFAULT_H) + onscreen_joint_xyss[3]))
                {
                    we_have_to_draw_it = ktrue;
                    i = num_onscreen_joint;
                }
            }

            onscreen_joint_xyss += 4;
        }

        if (we_have_to_draw_it == kfalse)
        {
            onscreen_joint_active = kfalse;
            return;
        }

        onscreen_draw_count++;

        // Figure out if the cursor is over this character...
        if (mouse.last.object.unk == NULL)
        {
            // Was this mouse over this character last time?
            if (mouse.last.item == onscreen_joint_character)
            {
                CHR_DATA * ptmp;

                // Yup, highlight it...
                if ( VALID_CHR_RANGE(onscreen_joint_character) )
                {
                    // But only if we move mouse...  'Cause it's annoying to have characters flashing red all the time...
                    if (mouse.text_timer > 0 || mouse.x != mouse.last_x || mouse.y != mouse.last_y)
                    {
                        line_color[0] = 255;
                        line_color[1] = 64;

                        // Handle mouse over names...
                        mouse.text_timer = MOUSE_TEXT_TIME;

                        ptmp = chr_data_get_ptr(onscreen_joint_character);
                        if ( NULL != ptmp )
                        {
                            // Call the character's script to get its name...
                            sf_fast_rerun_script( &(ptmp->script_ctxt), FAST_FUNCTION_GETNAME);
                            strcpy(mouse.text, NAME_STRING);
                        }
                    }
                }
            }

            // Is it over it now?
            onscreen_joint_xyss = (float*) fourthbuffer;
            repeat(i, num_onscreen_joint)
            {
                distance_xyz[XX] = onscreen_joint_xyss[XX] - mouse.x;
                dot = onscreen_joint_xyss[YY] - mouse.y;

                if ((dot*dot + distance_xyz[XX]*distance_xyz[XX]) < (onscreen_joint_xyss[2]*onscreen_joint_xyss[2]))
                {
                    // What distance are we at?
                    distance_xyz[XX] = onscreen_joint_character_data->x - camera.xyz[XX];
                    distance_xyz[YY] = onscreen_joint_character_data->y - camera.xyz[YY];
                    distance_xyz[ZZ] = onscreen_joint_character_data->z - camera.xyz[ZZ];
                    dot = distance_xyz[XX] * distance_xyz[XX] + distance_xyz[YY] * distance_xyz[YY] + distance_xyz[ZZ] * distance_xyz[ZZ];

                    if (dot < mouse.character_distance)
                    {
                        // Remember that we're over it, but don't color line yet in case there's a better one...
                        mouse.character_distance = dot;
                        mouse.current.item = onscreen_joint_character;
                        i = num_onscreen_joint;
                    }
                }

                onscreen_joint_xyss += 4;
            }
        }
    }
    else
    {
        // Window rendering mode has to remember this stuff for attached parts...
        global_bone_data = bone_data;
        global_num_bone = num_bone;
    }


    // Generate vertex coordinates based on bone locations...
    if (bone_frame_data)
    {
        render_bone_frame(base_model_data, bone_data, bone_frame_data);
    }


#ifdef DEVTOOL

    switch (mode & 240)
    {
        case WIN_3D_VERTEX:
            // Show selected joints...
            display_texture_off();
            joint_position_data = frame_data + 11 + (num_bone * 24);
            best_data = joint_position_data;

            if ((mode&15) == 11)
            {
                primitive_data = joint_data;
                display_zbuffer_off();
                repeat(i, num_joint)
                {
                    d = DEREF( float, primitive_data );

                    if (d < 0.05f) d = 0.05f;

                    display_solid_marker(dark_red, DEREF( float, best_data ), DEREF( float, best_data + 4 ), DEREF( float, best_data + 8 ), d);
                    best_data += 12;
                    primitive_data += 4;
                }
                display_zbuffer_on();
                primitive_data = joint_data;
                best_data = joint_position_data;
                repeat(i, num_joint)
                {
                    d = DEREF( float, primitive_data );

                    if (d < 0.05f) d = 0.05f;

                    if (select_inlist(i))
                    {
                        // Selected joint
                        display_marker(white, DEREF( float, best_data + 0 ), DEREF( float, best_data + 4 ), DEREF( float, best_data + 8 ), d);
                    }
                    else
                    {
                        // Unselected joint
                        display_marker(red, DEREF( float, best_data + 0 ), DEREF( float, best_data + 4 ), DEREF( float, best_data + 8 ), d);
                    }

                    best_data += 12;
                    primitive_data += 4;
                }
            }
            else
            {
                repeat(i, num_joint)
                {
                    if ((mode&15) >= 6 && select_inlist(i))
                    {
                        // Selected joint
                        display_marker(white, DEREF( float, best_data ), DEREF( float, best_data + 4 ), DEREF( float, best_data + 8 ), 0.05f);

                        if (global_rdy_show_joint_numbers)
                        {
                            display_paperdoll_on();
                            display_texture_on();
                            temporary_particle_data.x      = (DEREF( float, best_data ));
                            temporary_particle_data.y      = (DEREF( float, best_data + 4 ));
                            temporary_particle_data.z      = (DEREF( float, best_data + 8 ));
                            temporary_particle_data.length = 0.5f;
                            temporary_particle_data.width  = 0.5f;
                            temporary_particle_data.image  = texture_ascii_ptr;
                            temporary_particle_data.red    = 255;
                            temporary_particle_data.green  = 255;
                            temporary_particle_data.blue   = 255;
                            temporary_particle_data.number = (Uint8) i;
                            temporary_particle_data.spin   = 0;
                            temporary_particle_data.vspin  = PART_SPIN_ON | PART_NUMBER_ON;
                            temporary_particle_data.alpha  = 255;
                            particle_draw( &temporary_particle_data );
                            display_texture_off();
                            display_paperdoll_off();
                        }
                    }
                    else
                    {
                        // Unselected joint
                        display_marker(red, DEREF( float, best_data + 0 ), DEREF( float, best_data + 4 ), DEREF( float, best_data + 8 ), 0.05f);
                    }

                    best_data += 12;
                }
            }


            // Show movement offset...
            if ((mode&15) == 13)
            {
                display_color(green);
                display_start_line();
                {
                    display_vertex_xyz(0, 0, 0);
                    display_vertex_xyz(DEREF( float, frame_data + 3 ), DEREF( float, frame_data + 7 ), 0);
                }
                display_end();
            }


            // Select vertices on box close...
            if (selection.close_type == BORDER_SELECT)
            {
                data = vertex_data;
                repeat(i, num_vertex)
                {
                    if (!select_inlist(i) && *(data + 63) == kfalse)
                    {
                        render_get_point_xy(DEREF( float, data ), DEREF( float, data + 4 ), DEREF( float, data + 8 ), &x, &y);

                        if (x > selection.box_tl[XX] &&  x < selection.box_br[XX])
                        {
                            if (y > selection.box_tl[YY] &&  y < selection.box_br[YY])
                            {
                                select_add(i, ((float*) data));
                            }
                        }
                    }

                    data += 64;
                }
            }
            else if (selection.close_type == BORDER_CROSS_HAIRS && selection.view != VIEW_3D_XYZ)
            {
                if ((mode & 15) != 1 && mode != 38)
                {
                    render_get_point_xyz(pwin, mouse.x, mouse.y, &selection.center_xyz[XX], &selection.center_xyz[YY], &selection.center_xyz[ZZ]);
                }
                else
                {
                    if (selection.view == VIEW_TOP_XY)
                    {
                        render_get_point_xyz(pwin, mouse.x, mouse.y, &selection.center_xyz[XX], &selection.center_xyz[YY], &dot);
                    }
                    else if (selection.view == VIEW_FRONT_XZ)
                    {
                        render_get_point_xyz(pwin, mouse.x, mouse.y, &selection.center_xyz[XX], &dot, &selection.center_xyz[ZZ]);
                    }
                    else
                    {
                        render_get_point_xyz(pwin, mouse.x, mouse.y, &dot, &selection.center_xyz[YY], &selection.center_xyz[ZZ]);
                    }

                    if (keyb.down[SDLK_LSHIFT] || keyb.down[SDLK_RSHIFT])
                    {
                        // Snapped to nearest quarter foot...
                        selection.center_xyz[XX] = ((int) (selection.center_xyz[XX] * 4.0f)) * 0.25f;
                        selection.center_xyz[YY] = ((int) (selection.center_xyz[YY] * 4.0f)) * 0.25f;
                        selection.center_xyz[ZZ] = ((int) (selection.center_xyz[ZZ] * 4.0f)) * 0.25f;
                    }
                }

                if ((mode & 15) == 13)
                {
                    // Movement offset mode...
                    DEREF( float, frame_data + 3 ) = selection.center_xyz[XX];
                    DEREF( float, frame_data + 7 ) = selection.center_xyz[YY];

                    if (keyb.down[SDLK_LSHIFT] || keyb.down[SDLK_RSHIFT])
                    {
                        // Snap to 0 on x axis...
                        DEREF( float, frame_data + 3 ) = 0.0f;
                    }
                }

                select_update_xyz();
            }
            else if (selection.close_type == BORDER_POINT_PICK)
            {
                best_vertex = UINT16_MAX;
                best_depth = 99999.0f;

                if ((mode&15) >= 6)
                {
                    // Selecting joints
                    data = joint_position_data;
                    j = 12;
                    k = num_joint;
                }
                else
                {
                    // Selecting vertices
                    data = vertex_data;
                    j = 64;
                    k = num_vertex;
                }

                // Look through the coordinates
                repeat(i, k)
                {
                    if (!select_inlist(i))
                    {
                        // Check hidden flag...
                        found_connection = ktrue;

                        if (j == 64)
                        {
                            if (*(data + 63))
                            {
                                found_connection = kfalse;
                            }
                        }

                        if (found_connection)
                        {
                            render_get_point_xyd(DEREF( float, data ), DEREF( float, data + 4 ), DEREF( float, data + 8 ), &x, &y, &d);
                            x = x - mouse.x;
                            y = y - mouse.y;
                            x = SQRT(x * x + y * y);

                            if (x < 5.0f && d < best_depth)
                            {
                                best_depth = d;
                                best_vertex = i;
                                best_data = data;
                            }
                        }
                    }

                    data += j;
                }

                if (best_vertex != UINT16_MAX)
                {
                    select_add(best_vertex, ((float*) best_data));
                }
            }
            else if (selection.close_type == BORDER_MOVE && selection.view != VIEW_3D_XYZ)
            {
                if (selection.move_on)
                {
                    render_get_point_xyz(pwin, mouse.x, mouse.y, &selection.offset_xyz[XX], &selection.offset_xyz[YY], &selection.offset_xyz[ZZ]);
                    render_model_move();

                    if ((mode&15) == 8 && select_num > 0)
                    {
                        render_fix_model_to_bone_length(start_data, frame, 9999);
                    }
                }
                else
                {
                    render_get_point_xyz(pwin, mouse.x, mouse.y, &selection.center_xyz[XX], &selection.center_xyz[YY], &selection.center_xyz[ZZ]);
                    select_update_xyz();

                    if ((mode&15) < 6)
                    {
                        repeat(i, select_num)
                        {
                            render_attach_vertex_to_bone(start_data, frame, select_list[i]);
                            render_crunch_vertex(start_data, frame, select_list[i], ktrue, 0);
                        }
                    }
                    else
                    {
                        if (keyb.down[SDLK_LSHIFT] == kfalse && keyb.down[SDLK_RSHIFT] == kfalse)
                        {
                            select_clear();
                        }
                    }
                }
            }
            else if (selection.close_type == BORDER_SPECIAL_SELECT_ALL)
            {
                data = vertex_data;
                repeat(i, num_vertex)
                {
                    if (!select_inlist(i) && *(data + 63) == kfalse)
                    {
                        select_add(i, ((float*) data));
                    }

                    data += 64;
                }
                selection.close_type = 0;
            }
            else if (selection.close_type == BORDER_SPECIAL_SELECT_INVERT)
            {
                data = vertex_data;
                repeat(i, num_vertex)
                {
                    if (!select_inlist(i))
                    {
                        if (*(data + 63) == kfalse)
                        {
                            select_add(i, ((float*) data));
                        }
                    }
                    else
                    {
                        // Items with a NULL data get removed later
                        select_data[select_index] = NULL;
                    }

                    data += 64;
                }
                repeat(i, select_num)
                {
                    if (select_data[i] == NULL)
                    {
                        select_remove(i);
                        i--;
                    }
                }
                selection.close_type = 0;
            }
            else if (selection.close_type == BORDER_SPECIAL_SELECT_CONNECTED)
            {
                // Flag all of our original vertices
                repeat(i, select_num)
                {
                    select_flag[i] = ktrue;
                }
                // Look for any new vertices that are connected to an original vertex by means of a strip
                data = vertex_data;
                repeat(i, num_vertex)
                {
                    if (!select_inlist(i) && *(data + 63) == kfalse)
                    {
                        // Check if there's a connection
                        found_connection = kfalse;
                        best_data = texture_data;
                        repeat(m, MAX_DDD_TEXTURE)
                        {
                            texture_mode = *best_data;  best_data++;

                            if (texture_mode != 0)
                            {
                                best_data += 2;

                                // Strips...
                                num_primitive = DEREF( Uint16, best_data );  best_data += 2;
                                repeat(j, num_primitive)
                                {
                                    num_primitive_vertex = DEREF( Uint16, best_data );  best_data += 2;
                                    best_vertex = kfalse;
                                    primitive_data = best_data;
                                    repeat(k, num_primitive_vertex)
                                    {
                                        vertex = DEREF( Uint16, best_data );  best_data += 4;

                                        if (vertex == i)  best_vertex = ktrue;
                                    }

                                    if (best_vertex)
                                    {
                                        best_data = primitive_data;
                                        repeat(k, num_primitive_vertex)
                                        {
                                            vertex = DEREF( Uint16, best_data );  best_data += 4;

                                            if (select_inlist(vertex))
                                            {
                                                if (select_flag[select_index])
                                                {
                                                    found_connection = ktrue;
                                                }
                                            }
                                        }
                                    }
                                }

                                // Skip fans...
                                num_primitive = DEREF( Uint16, best_data );  best_data += 2;
                                repeat(j, num_primitive)
                                {
                                    num_primitive_vertex = DEREF( Uint16, best_data );  best_data += 2;
                                    repeat(k, num_primitive_vertex)
                                    {
                                        best_data += 4;
                                    }
                                }
                            }
                        }

                        if (found_connection)
                        {
                            select_add(i, ((float*) data));
                            select_flag[select_num-1] = kfalse;
                        }
                    }

                    data += 64;
                }
                selection.close_type = 0;
            }



            // Show all of the vertices and their normals
            data = vertex_data;
            repeat(i, num_vertex)
            {
                // Check hidden flag...
                if (*(data + 63) == kfalse)
                {
                    if ((mode&15) < 6 && select_inlist(i))
                    {
                        // Selected vertex
                        if ((*(data + 14)) & 128)
                        {
                            // Anchor flag'd vertex...  !!!ANCHOR!!!
                            display_marker(yellow, DEREF( float, data ), DEREF( float, data + 4 ), DEREF( float, data + 8 ), 0.05f);
                        }
                        else
                        {
                            // Normal selected vertex...
                            display_marker(white, DEREF( float, data ), DEREF( float, data + 4 ), DEREF( float, data + 8 ), 0.05f);
                        }

                        // Do anchor switch key...  !!!ANCHOR!!!
                        if (do_anchor_swap)
                        {
                            (*(data + 14)) += 128;
                        }
                    }
                    else
                    {
                        // Unselected vertex
                        if ((*(data + 14)) & 128)
                        {
                            // Anchor flag'd vertex...  !!!ANCHOR!!!
                            display_marker(gold, DEREF( float, data ), DEREF( float, data + 4 ), DEREF( float, data + 8 ), 0.05f);
                        }
                        else
                        {
                            // Normal vertex...
                            display_marker(green, DEREF( float, data ), DEREF( float, data + 4 ), DEREF( float, data + 8 ), 0.05f);
                        }
                    }

                    // Normal
                    display_start_line();
                    {
                        display_vertex((float*) data);
                        display_vertex_xyz(DEREF( float, data ) + (DEREF( float, data + 15 )*0.15f), DEREF( float, data + 4 ) + (DEREF( float, data + 19 )*0.15f), DEREF( float, data + 8 ) + (DEREF( float, data + 23 )*0.15f));
                    }
                    display_end();
                }

                data += 64;
            }

            if (do_anchor_swap)
            {
                do_anchor_swap = kfalse;
            }

            // Display the crosshairs...
            if (((mode & 15) >= 1 && (mode&15) <= 4) || (mode&15) == 6)
            {
                display_marker(white, selection.center_xyz[XX], selection.center_xyz[YY], selection.center_xyz[ZZ], 500.00f);
            }

            display_texture_on();


            break;
        case WIN_3D_BONE:
            // Show all of the joints
            display_texture_off();
            data = frame_data + 11;
            best_data = data;
            data += (num_bone * 24);
            joint_position_data = data;
            // Show all of the bones
            data = bone_data;
            repeat(i, num_bone)
            {
                if ((*data) == 0)
                {
                    display_color(white);
                }
                else
                {
                    display_color(instrument_color[(*data)&7]);
                }

                data++;
                j = DEREF( Uint16, data );  data += 2;
                k = DEREF( Uint16, data );  data += 2;
                j = (j << 3) + (j << 2);
                k = (k << 3) + (k << 2);
                display_start_line();
                {
                    display_vertex((float*) (joint_position_data + j));
                    display_vertex((float*) (joint_position_data + k));
                }
                display_end();
                data += 4;
            }
            // Show all of the forward normals
            display_color(gold);
            data = bone_data;
            repeat(i, num_bone)
            {
                data++;
                j = DEREF( Uint16, data );  data += 2;
                k = DEREF( Uint16, data );  data += 2;
                j = j * 12;
                k = k * 12;
                m = i * 24;
                display_start_line();
                {
                    x = (0.60f * (DEREF( float, joint_position_data + j ))) + (0.40f * (DEREF( float, joint_position_data + k )));
                    y = (0.60f * (DEREF( float, joint_position_data + j + 4 ))) + (0.40f * (DEREF( float, joint_position_data + k + 4 )));
                    z = (0.60f * (DEREF( float, joint_position_data + j + 8 ))) + (0.40f * (DEREF( float, joint_position_data + k + 8 )));
                    display_vertex_xyz(x, y, z);
                    x = 0.50f * ((DEREF( float, joint_position_data + j )) + (DEREF( float, joint_position_data + k )));
                    y = 0.50f * ((DEREF( float, joint_position_data + j + 4 )) + (DEREF( float, joint_position_data + k + 4 )));
                    z = 0.50f * ((DEREF( float, joint_position_data + j + 8 )) + (DEREF( float, joint_position_data + k + 8 )));
                    x += 0.25f * (DEREF( float, best_data + m ));
                    y += 0.25f * (DEREF( float, best_data + m + 4 ));
                    z += 0.25f * (DEREF( float, best_data + m + 8 ));
                    display_vertex_xyz(x, y, z);
                }
                display_end();


                display_start_line();
                {
                    display_vertex_xyz(x, y, z);
                    x = (0.40f * (DEREF( float, joint_position_data + j ))) + (0.60f * (DEREF( float, joint_position_data + k )));
                    y = (0.40f * (DEREF( float, joint_position_data + j + 4 ))) + (0.60f * (DEREF( float, joint_position_data + k + 4 )));
                    z = (0.40f * (DEREF( float, joint_position_data + j + 8 ))) + (0.60f * (DEREF( float, joint_position_data + k + 8 )));
                    display_vertex_xyz(x, y, z);
                }
                display_end();
                data += 4;
            }



            // Show all of the side normals
            display_color(gold);
            data = bone_data;
            repeat(i, num_bone)
            {
                data++;
                j = DEREF( Uint16, data );  data += 2;
                k = DEREF( Uint16, data );  data += 2;
                j = j * 12;
                k = k * 12;
                m = (i * 24) + 12;
                display_start_line();
                {
                    x = (0.60f * (DEREF( float, joint_position_data + j ))) + (0.40f * (DEREF( float, joint_position_data + k )));
                    y = (0.60f * (DEREF( float, joint_position_data + j + 4 ))) + (0.40f * (DEREF( float, joint_position_data + k + 4 )));
                    z = (0.60f * (DEREF( float, joint_position_data + j + 8 ))) + (0.40f * (DEREF( float, joint_position_data + k + 8 )));
                    display_vertex_xyz(x, y, z);
                    x = 0.50f * ((DEREF( float, joint_position_data + j )) + (DEREF( float, joint_position_data + k )));
                    y = 0.50f * ((DEREF( float, joint_position_data + j + 4 )) + (DEREF( float, joint_position_data + k + 4 )));
                    z = 0.50f * ((DEREF( float, joint_position_data + j + 8 )) + (DEREF( float, joint_position_data + k + 8 )));
                    x += 0.25f * (DEREF( float, best_data + m ));
                    y += 0.25f * (DEREF( float, best_data + m + 4 ));
                    z += 0.25f * (DEREF( float, best_data + m + 8 ));
                    display_vertex_xyz(x, y, z);
                }
                display_end();


                display_start_line();
                {
                    display_vertex_xyz(x, y, z);
                    x = (0.40f * (DEREF( float, joint_position_data + j ))) + (0.60f * (DEREF( float, joint_position_data + k )));
                    y = (0.40f * (DEREF( float, joint_position_data + j + 4 ))) + (0.60f * (DEREF( float, joint_position_data + k + 4 )));
                    z = (0.40f * (DEREF( float, joint_position_data + j + 8 ))) + (0.60f * (DEREF( float, joint_position_data + k + 8 )));
                    display_vertex_xyz(x, y, z);
                }
                display_end();
                data += 4;
            }



            display_texture_on();
            break;
        case WIN_3D_BONE_UPDATE:

            if (selection.move_on == kfalse || (mode & 15))
            {
                render_bone_frame(base_model_data, bone_data, (frame_data + 11));
                render_pregenerate_normals(start_data, frame, 0);
            }

            break;
        case WIN_3D_TEXVERTEX:
            // Show all of the texture vertices
            display_texture_off();
            data = tex_vertex_data;
            repeat(i, num_tex_vertex)
            {
                x = DEREF( float, data );  data += 4;
                y = DEREF( float, data );  data += 4;
                x = (x * pwin->scale * 25.0f) + pwin->x;
                y = (y * pwin->scale * 25.0f) + pwin->y;

                if (select_inlist(i))
                {
                    // Selected texture vertex
                    display_2d_marker(white, x, y, 2.0f);
                }
                else
                {
                    // Unselected texture vertex
                    display_2d_marker(green, x, y, 2.0f);
                }
            }


            // Select tex vertices on box close...
            if (selection.close_type == BORDER_SELECT)
            {
                data = tex_vertex_data;
                repeat(i, num_tex_vertex)
                {
                    x = DEREF( float, data );;
                    y = DEREF( float, data + 4 );
                    x = (x * pwin->scale * 25.0f) + pwin->x;
                    y = (y * pwin->scale * 25.0f) + pwin->y;

                    if (!select_inlist(i))
                    {
                        if (x > (selection.box_tl[XX] - 1.0f) &&  x < (selection.box_br[XX] + 1.0f))
                        {
                            if (y > (selection.box_tl[YY] - 1.0f) &&  y < (selection.box_br[YY] + 1.0f))
                            {
                                select_add(i, ((float*) data));
                            }
                        }
                    }

                    data += 8;
                }
            }
            else if (selection.close_type == BORDER_SPECIAL_SELECT_ALL)
            {
                data = tex_vertex_data;
                repeat(i, num_tex_vertex)
                {
                    if (!select_inlist(i))
                    {
                        select_add(i, ((float*) data));
                    }

                    data += 8;
                }
                selection.close_type = 0;
            }
            else if (selection.close_type == BORDER_SPECIAL_SELECT_INVERT)
            {
                data = tex_vertex_data;
                repeat(i, num_tex_vertex)
                {
                    if (!select_inlist(i))
                    {
                        select_add(i, ((float*) data));
                    }
                    else
                    {
                        // Items with a NULL data get removed later
                        select_data[select_index] = NULL;
                    }

                    data += 8;
                }
                repeat(i, select_num)
                {
                    if (select_data[i] == NULL)
                    {
                        select_remove(i);
                        i--;
                    }
                }
                selection.close_type = 0;
            }
            else if (selection.close_type == BORDER_SPECIAL_SELECT_CONNECTED)
            {
                // Flag all of our original vertices
                repeat(i, select_num)
                {
                    select_flag[i] = ktrue;
                }
                // Look for any new vertices that are connected to an original vertex by means of a strip
                data = tex_vertex_data;
                repeat(i, num_tex_vertex)
                {
                    if (!select_inlist(i))
                    {
                        // Check if there's a connection
                        found_connection = kfalse;
                        best_data = texture_data;
                        repeat(m, MAX_DDD_TEXTURE)
                        {
                            texture_mode = *best_data;  best_data++;

                            if (texture_mode != 0)
                            {
                                best_data += 2;

                                // Strips...
                                num_primitive = DEREF( Uint16, best_data );  best_data += 2;
                                repeat(j, num_primitive)
                                {
                                    num_primitive_vertex = DEREF( Uint16, best_data );  best_data += 2;
                                    best_vertex = kfalse;
                                    primitive_data = best_data;
                                    repeat(k, num_primitive_vertex)
                                    {
                                        best_data += 2;
                                        vertex = DEREF( Uint16, best_data );  best_data += 2;

                                        if (vertex == i)  best_vertex = ktrue;
                                    }

                                    if (best_vertex)
                                    {
                                        best_data = primitive_data;
                                        repeat(k, num_primitive_vertex)
                                        {
                                            best_data += 2;
                                            vertex = DEREF( Uint16, best_data );  best_data += 2;

                                            if (select_inlist(vertex))
                                            {
                                                if (select_flag[select_index])
                                                {
                                                    found_connection = ktrue;
                                                }
                                            }
                                        }
                                    }
                                }

                                // Skip fans...
                                num_primitive = DEREF( Uint16, best_data );  best_data += 2;
                                repeat(j, num_primitive)
                                {
                                    num_primitive_vertex = DEREF( Uint16, best_data );  best_data += 2;
                                    repeat(k, num_primitive_vertex)
                                    {
                                        best_data += 4;
                                    }
                                }
                            }
                        }

                        if (found_connection)
                        {
                            select_add(i, ((float*) data));
                            select_flag[select_num-1] = kfalse;
                        }
                    }

                    data += 8;
                }
                selection.close_type = 0;
            }
            else if (selection.close_type == BORDER_MOVE)
            {
                if (selection.move_on)
                {
                    selection.offset_xyz[XX] = (mouse.x - pwin->x) / (25.0f * pwin->scale);
                    selection.offset_xyz[YY] = (mouse.y - pwin->y) / (25.0f * pwin->scale);
                    render_tex_move(ktrue);
                }
                else
                {
                    selection.center_xyz[XX] = (mouse.x - pwin->x) / (25.0f * pwin->scale);
                    selection.center_xyz[YY] = (mouse.y - pwin->y) / (25.0f * pwin->scale);
                    select_update_xy();
                }

                selection.close_type = 0;
            }
            else if (selection.close_type == BORDER_CROSS_HAIRS)
            {
                selection.center_xyz[XX] = (mouse.x - pwin->x) / (25.0f * pwin->scale);
                selection.center_xyz[YY] = (mouse.y - pwin->y) / (25.0f * pwin->scale);
                selection.close_type = 0;
            }


            // Show all of the texture lines
            display_color(green);
            repeat(i, MAX_DDD_TEXTURE)
            {
                texture_mode = *texture_data;  texture_data++;

                if (texture_mode != 0)
                {
                    texture_data += 2;

                    // Strips...
                    if (i == main_alpha) // Main alpha used for TEXVERTEX mode to tell which texture is being drawn, so we know which lines to slap down...
                    {
                        num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                        repeat(j, num_primitive)
                        {
                            num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                            display_start_line_loop();
                            repeat(k, num_primitive_vertex)
                            {
                                texture_data += 2;
                                tex_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                x = DEREF( float, tex_vertex_data + (tex_vertex << 3) );
                                y = DEREF( float, tex_vertex_data + 4 + (tex_vertex << 3) );
                                x = (x * pwin->scale * 25.0f) + pwin->x;
                                y = (y * pwin->scale * 25.0f) + pwin->y;
                                display_vertex_xyz(x, y, 0.0f);
                            }
                            display_end();
                        }
                    }
                    else
                    {
                        num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                        repeat(j, num_primitive)
                        {
                            num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(k, num_primitive_vertex)
                            {
                                texture_data += 4;
                            }
                        }
                    }

                    // Fans...
                    num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                    repeat(j, num_primitive)
                    {
                        num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                        repeat(k, num_primitive_vertex)
                        {
                            texture_data += 4;
                        }
                    }
                }
            }
            display_texture_on();
            break;
        case WIN_3D_SKIN_FROM_CAMERA:
            // Clear vertex to texture vertex table...  UINT16_MAX...  Also clear out onscreen coordinates...
#if DETAIL_LEVEL_MAX != 1
            break;
#endif
            log_info(1, "Skin from camera!!!");
            repeat(i, MAX_VERTEX)
            {
                vertex_to_tex_vertex[i] = UINT16_MAX;
                tex_vertex_onscreen_xy[i][XX] = 0.0f;
                tex_vertex_onscreen_xy[i][YY] = 0.0f;
            }
            onscreen_min_x = 9999;
            onscreen_max_x = 0;
            onscreen_min_y = 9999;
            onscreen_max_y = 0;
            tex_vertex_to_add = 0;
            log_info(1, "Cleared out old stuff");


            // Generate new tex vertices based on visible selected triangles
            texture_to_skin = mode & 3;
            repeat(i, MAX_DDD_TEXTURE)
            {
                texture_mode = *texture_data;  texture_data++;

                if (texture_mode != 0)
                {
                    log_info(1, "Texture %d is on", i);
                    texture_data += 2;

                    if (i == texture_to_skin)
                    {
                        log_info(1, "Texture %d is our texture_to_skin", i);
                        // Go through each strip looking for selected vertices, triangle visible to camera
                        num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                        log_info(1, "%d Triangles to search", num_primitive);
                        repeat(j, num_primitive)
                        {
                            num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                            primitive_data = texture_data;


                            // Are all 3 points of the triangle selected?
                            found_connection = ktrue;
                            repeat(k, num_primitive_vertex)
                            {
                                vertex = DEREF( Uint16, texture_data );  texture_data += 4;

                                if (select_inlist(vertex) == kfalse) found_connection = kfalse;
                            }

                            if (found_connection && num_primitive_vertex == 3)
                            {
                                log_info(1, "All points of triangle %d are selected", j);
                                // Make sure the triangle is visible
                                texture_data = primitive_data;
                                repeat(k, num_primitive_vertex)
                                {
                                    vertex = DEREF( Uint16, texture_data );  texture_data += 4;
                                    data = vertex_data + (vertex << 6);
                                    render_get_point_xy(DEREF( float, data ), DEREF( float, data + 4 ), DEREF( float, data + 8 ), &triangle_xy[k][XX], &triangle_xy[k][YY]);
                                    log_info(1, "Point %d at (%f, %f)", k, triangle_xy[k][XX], triangle_xy[k][YY]);
                                }
                                // First side of triangle...
                                triangle_xy[1][XX] -= triangle_xy[0][XX];
                                triangle_xy[1][YY] -= triangle_xy[0][YY];
                                // Third side of triangle...
                                triangle_xy[2][XX] -= triangle_xy[0][XX];
                                triangle_xy[2][YY] -= triangle_xy[0][YY];
                                // Perpenicular...
                                x = triangle_xy[1][YY];
                                y = -triangle_xy[1][XX];
                                // Is third point left or right of the first side?  Dot perpendicular to figure...
                                x = x * triangle_xy[2][XX] + y * triangle_xy[2][YY];

                                if (x <= 0)
                                {
                                    log_info(1, "Triangle %d is visible too", j);
                                    // Reconstitute onscreen coordinates
                                    triangle_xy[1][XX] += triangle_xy[0][XX];
                                    triangle_xy[1][YY] += triangle_xy[0][YY];
                                    triangle_xy[2][XX] += triangle_xy[0][XX];
                                    triangle_xy[2][YY] += triangle_xy[0][YY];


                                    // Triangle is visible...  Pick tex vertices to use now...  Add to file at end...
                                    // If vertex has been used already, use a matching tex vertex
                                    texture_data = primitive_data;
                                    repeat(k, num_primitive_vertex)
                                    {
                                        vertex = DEREF( Uint16, texture_data );  texture_data += 2;

                                        if (vertex_to_tex_vertex[vertex] == UINT16_MAX)
                                        {
                                            vertex_to_tex_vertex[vertex] = tex_vertex_to_add + num_tex_vertex;
                                            x = triangle_xy[k][XX];
                                            y = triangle_xy[k][YY];
                                            tex_vertex_onscreen_xy[tex_vertex_to_add+num_tex_vertex][XX] = x;
                                            tex_vertex_onscreen_xy[tex_vertex_to_add+num_tex_vertex][YY] = y;

                                            if (x < onscreen_min_x)  onscreen_min_x = x;

                                            if (x > onscreen_max_x)  onscreen_max_x = x;

                                            if (y < onscreen_min_y)  onscreen_min_y = y;

                                            if (y > onscreen_max_y)  onscreen_max_y = y;

                                            tex_vertex_to_add++;
                                        }

                                        DEREF( Uint16, texture_data ) = vertex_to_tex_vertex[vertex];  texture_data += 2;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // Skip strips for this texture
                        num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                        repeat(j, num_primitive)
                        {
                            num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(k, num_primitive_vertex)
                            {
                                texture_data += 4;
                            }
                        }
                    }


                    // Skip fans entirely
                    num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                    repeat(j, num_primitive)
                    {
                        num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                        repeat(k, num_primitive_vertex)
                        {
                            texture_data += 4;
                        }
                    }
                }
            }

            // Finish up putting in tex vertices
            if (tex_vertex_to_add > 0)
            {
                // Scale texture vertex coordinates to range from 0 to 1
                onscreen_max_x -= onscreen_min_x;
                log_info(1, "%d new tex vertices...", tex_vertex_to_add);
                onscreen_max_y -= onscreen_min_y;
                repeat(i, tex_vertex_to_add)
                {
                    tex_vertex_onscreen_xy[i+num_tex_vertex][XX] -= onscreen_min_x;
                    tex_vertex_onscreen_xy[i+num_tex_vertex][YY] -= onscreen_min_y;
                    tex_vertex_onscreen_xy[i+num_tex_vertex][XX] /= onscreen_max_x;
                    tex_vertex_onscreen_xy[i+num_tex_vertex][YY] /= onscreen_max_y;
                    log_info(1, "%d at (%f, %f)", i + num_tex_vertex, tex_vertex_onscreen_xy[i+num_tex_vertex][XX], tex_vertex_onscreen_xy[i+num_tex_vertex][YY]);

                    // Add the vertex to the file...
                    render_insert_tex_vertex(start_data, frame, tex_vertex_onscreen_xy[i+num_tex_vertex], 0, MAX_TEX_VERTEX);
                }
            }

            // Get rid of unused tex vertices
            clean_up_tex_vertex(start_data, frame);
            break;
        case WIN_3D_MODEL:
#endif
            texture_data_start = texture_data;




            // Draw all visible cartoon lines (for solid characters)
            /*
            #ifdef DEVTOOL
            if(((keyb.down[SDLK_F12] && selection.view == VIEW_3D_XYZ) || (line_mode&1)) && main_alpha == 255)
            #else
            if((line_mode&1) && main_alpha == 255)
            #endif
            {
            // Cartoon line data start...
            bone_data += (num_bone*9);
            num_primitive = DEREF( Uint16, bone_data );  bone_data += 2;


            // Clear out visibility count for each vertex...
            // Get onscreen coordinates of each vertex...  Stored in fourthbuffer...
            vertex_bxy = (float*) fourthbuffer;
            texture_data = vertex_data;
            repeat(i, num_vertex)
            {
            // Proper onscreen coordinate figurin'
            vertex_bxy[XX] = onscreen_matrix[0] * ((float*) texture_data)[XX] + onscreen_matrix[4] * ((float*) texture_data)[YY] + onscreen_matrix[8]  * ((float*) texture_data)[ZZ] + onscreen_matrix[12];
            vertex_bxy[YY] = onscreen_matrix[1] * ((float*) texture_data)[XX] + onscreen_matrix[5] * ((float*) texture_data)[YY] + onscreen_matrix[9]  * ((float*) texture_data)[ZZ] + onscreen_matrix[13];
            dot           = onscreen_matrix[3] * ((float*) texture_data)[XX] + onscreen_matrix[7] * ((float*) texture_data)[YY] + onscreen_matrix[11] * ((float*) texture_data)[ZZ] + onscreen_matrix[15];

            if(dot != 0.0f)
            {
            vertex_bxy[XX]/=dot; vertex_bxy[YY]/=dot;
            }
            texture_data += 64;
            vertex_bxy += 2;
            }
            vertex_bxy -= num_vertex<<1;


            // Determine visibility of each cartoon line...
            texture_data = bone_data;
            repeat(i, num_primitive)
            {
            // Find a perpendicular to the draw line...
            vertex = (DEREF( Uint16, texture_data ))<<1;
            perp_xy[YY] = -vertex_bxy[vertex];
            vertex++;
            perp_xy[XX] = vertex_bxy[vertex];
            vertex = (DEREF( Uint16, texture_data+2 ))<<1;
            start_xy[XX] = vertex_bxy[vertex];
            perp_xy[YY] += vertex_bxy[vertex];
            vertex++;
            start_xy[YY] = vertex_bxy[vertex];
            perp_xy[XX] -= vertex_bxy[vertex];


            // Check each check point's location against the perpendicular...
            vertex = (DEREF( Uint16, texture_data+4 ))<<1;
            texpos_xy[XX] = vertex_bxy[vertex] - start_xy[XX];
            vertex++;
            texpos_xy[YY] = vertex_bxy[vertex] - start_xy[YY];
            dot = texpos_xy[XX] * perp_xy[XX] + texpos_xy[YY] * perp_xy[YY];
            vertex = (DEREF( Uint16, texture_data+6 ))<<1;
            texpos_xy[XX] = vertex_bxy[vertex] - start_xy[XX];
            vertex++;
            texpos_xy[YY] = vertex_bxy[vertex] - start_xy[YY];

            dot *= (texpos_xy[XX] * perp_xy[XX] + texpos_xy[YY] * perp_xy[YY]);
            draw_cartoon_line[i] = (dot >= 0.0f);

            texture_data += 8;
            }



            // Happy fat lines...
            display_texture_off();
            display_cull_off();
            display_blend_off();
            display_color(line_color);
            repeat(i, num_primitive)
            {
            if(draw_cartoon_line[i])
            {
            // Find both points of line...  Not really texture stuff...
            vertex = DEREF( Uint16, bone_data );
            data = vertex_data + (vertex<<6);
            tex_vertex = DEREF( Uint16, bone_data+2 );
            texture_data = vertex_data + (tex_vertex<<6);


            // Start drawing the faded poly
            display_start_fan();


            // First point...
            vertex_bxy[XX] = ((float*) data)[XX];
            vertex_bxy[YY] = ((float*) data)[YY];
            vertex_bxy[ZZ] = ((float*) data)[ZZ];
            display_vertex(vertex_bxy);


            // Second point...
            data += 15;
            vertex_bxy[XX] += ((float*) data)[XX]*FAT_LINE_SIZE;
            vertex_bxy[YY] += ((float*) data)[YY]*FAT_LINE_SIZE;
            vertex_bxy[ZZ] += ((float*) data)[ZZ]*FAT_LINE_SIZE;
            display_vertex(vertex_bxy);


            // Third point...
            vertex_bxy[XX] = ((float*) texture_data)[XX];
            vertex_bxy[YY] = ((float*) texture_data)[YY];
            vertex_bxy[ZZ] = ((float*) texture_data)[ZZ];
            texture_data += 15;
            vertex_bxy[X+3] = vertex_bxy[XX] + ((((float*) texture_data)[XX])*FAT_LINE_SIZE);
            vertex_bxy[Y+3] = vertex_bxy[YY] + ((((float*) texture_data)[YY])*FAT_LINE_SIZE);
            vertex_bxy[Z+3] = vertex_bxy[ZZ] + ((((float*) texture_data)[ZZ])*FAT_LINE_SIZE);
            display_vertex(vertex_bxy+3);

            // Fourth point
            display_vertex(vertex_bxy);
            display_end();
            }
            bone_data += 8;
            }
            display_cull_on();
            display_texture_on();
            }
            */





            // !!!BAD!!!
            // !!!BAD!!!  Optimize this!!!  Especially with vertex = ((blah))...  data = vertex_data + (vertex<<6)...  No need to do first assignment...
            // !!!BAD!!!




            // Draw the character model
            texture_data = texture_data_start;
            color_argument = DEREF( Uint32, texture_file_data_block + 4 );
            repeat(i, MAX_DDD_TEXTURE)
            {
                texture_mode = *texture_data;  texture_data++;

                if (texture_mode != 0)
                {
                    // Funky thing to make layer'd triangles not blip - since we don't do multitexturing...
                    display_depth_layer(i);


                    texture_flags = *texture_data;  texture_data++;
                    texture_alpha = *texture_data;  texture_data++;




                    // Tell the graphics card which texture we're gonna use...
                    if (petrify)
                    {
                        // Use the petrify texture...
                        if ((texture_flags & RENDER_ENVIRO_FLAG))
                        {
                            // DWhite.RGB...
                            display_pick_texture(texture_petrify);
                        }
                        else if ((*texture_file_data_block) == NULL)
                        {
                            // Shouldn't happen...
                            display_pick_texture(0);
                        }
                        else
                        {
                            if (DEREF( Uint32, (*texture_file_data_block) + 2 ) == texture_armor)
                            {
                                // Use the stone armor texture...
                                display_pick_texture(texture_armorst);
                            }
                            else
                            {
                                // Use the actual texture...  Okay for things like eyes that don't have
                                // any color to 'em...
                                display_pick_texture(DEREF( Uint32, (*texture_file_data_block) + 2 ));
                            }
                        }
                    }
                    else
                    {
                        // Use the model's texture...
                        if ((*texture_file_data_block) == NULL)
                        {
                            // Shouldn't happen...
                            display_pick_texture(0);
                        }
                        else
                        {
                            display_pick_texture(DEREF( Uint32, (*texture_file_data_block) + 2 ));
                        }
                    }





                    // Check color flags...
                    if ((texture_flags & RENDER_COLOR_FLAG))
                    {
                        if (petrify)
                        {
                            color_temp[0] = 163;
                            color_temp[1] = 147;
                            color_temp[2] = 126;
                        }
                        else
                        {
                            color_temp[0] = (Uint8) ((color_argument >> 16) & 255);
                            color_temp[1] = (Uint8) ((color_argument >> 8) & 255);
                            color_temp[2] = (Uint8) (color_argument & 255);
                        }
                    }
                    else
                    {
                        color_temp[0] = 255;
                        color_temp[1] = 255;
                        color_temp[2] = 255;
                    }



                    // Check transparency flags...
                    if (texture_flags & RENDER_LIGHT_FLAG)
                    {
                        display_blend_light();
                        color_temp[3] = (main_alpha * texture_alpha) >> 8;
                        display_color_alpha(color_temp);
                    }
                    else
                    {
                        // Tint the object's color by the lighting values we setup earlier...
                        color_temp[0] = (color_temp[0] * global_render_light_color_rgb[0]) >> 8;
                        color_temp[1] = (color_temp[1] * global_render_light_color_rgb[1]) >> 8;
                        color_temp[2] = (color_temp[2] * global_render_light_color_rgb[2]) >> 8;

                        if (main_alpha < 255 || texture_alpha < 255)
                        {
                            if (texture_alpha < 255)
                            {
                                display_zbuffer_write_off();
                            }

                            display_blend_trans();
                            color_temp[3] = (main_alpha * texture_alpha) >> 8;

                            if (texture_flags & RENDER_EYE_FLAG)
                            {
                                // Make eyes always bright, regardless of invisibility amount...
                                color_temp[3] = (255 + color_temp[3]) >> 1;
                            }

                            display_color_alpha(color_temp);
                        }
                        else
                        {
                            display_blend_off();
                            display_color(color_temp);
                        }
                    }


                    // Are we doing paperdoll style transparency?
                    if (texture_flags & RENDER_PAPER_FLAG)
                    {
                        display_paperdoll_on();
                    }


                    // Figure out if we're not culling...
                    if (texture_flags & RENDER_NOCULL_FLAG)
                    {
                        display_cull_off();
                    }


                    // Which type of texturing are we doing?
                    if (texture_flags & RENDER_ENVIRO_FLAG)
                    {
                        // Environment mapped...
                        texture_matrix = rotate_enviro_matrix;


                        // Are we doing cartoon (positional) lighting?
                        if (texture_flags & RENDER_CARTOON_FLAG)
                        {
                            // Pan around texture to simulate light direction...
                            start_xy[XX] = global_render_light_offset_xy[XX];
                            start_xy[YY] = global_render_light_offset_xy[YY];
                        }
                        else
                        {
                            // Simple texture map...
                            start_xy[XX] = 0.5f;
                            start_xy[YY] = 0.5f;
                        }


                        // Strips...
                        num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                        repeat(j, num_primitive)
                        {
                            num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                            display_start_strip();
                            repeat(k, num_primitive_vertex)
                            {
                                vertex = DEREF( Uint16, texture_data );  texture_data += 4;
                                data = vertex_data + (vertex << 6);
                                texpos_xy[XX] = (DEREF( float, data + 15 ) * texture_matrix[0]) + (DEREF( float, data + 19 ) * texture_matrix[1]) + (DEREF( float, data + 23 ) * texture_matrix[2]) + start_xy[XX];
                                texpos_xy[YY] = (DEREF( float, data + 15 ) * texture_matrix[3]) + (DEREF( float, data + 19 ) * texture_matrix[4]) + (DEREF( float, data + 23 ) * texture_matrix[5]) + start_xy[YY];
                                display_texpos(texpos_xy);
                                display_vertex((float*) data);
                            }
                            display_end();
                        }


                        // Fans...
                        num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                        repeat(j, num_primitive)
                        {
                            num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                            display_start_fan();
                            repeat(k, num_primitive_vertex)
                            {
                                vertex = DEREF( Uint16, texture_data );  texture_data += 4;
                                data = vertex_data + (vertex << 6);
                                texpos_xy[XX] = (DEREF( float, data + 15 ) * texture_matrix[0]) + (DEREF( float, data + 19 ) * texture_matrix[1]) + (DEREF( float, data + 23 ) * texture_matrix[2]) + start_xy[XX];
                                texpos_xy[YY] = (DEREF( float, data + 15 ) * texture_matrix[3]) + (DEREF( float, data + 19 ) * texture_matrix[4]) + (DEREF( float, data + 23 ) * texture_matrix[5]) + start_xy[YY];
                                display_texpos(texpos_xy);
                                display_vertex((float*) data);
                            }
                            display_end();
                        }
                    }
                    else
                    {
                        // Non-enviroment map'd textured...
                        if (texture_flags & RENDER_EYE_FLAG)
                        {
                            // Texture is an eye texture...  That means its tex vertex locations are offset by the
                            // eye_quad_xy thing...
                            if (texture_flags & RENDER_CARTOON_FLAG)
                            {
                                if (texture_flags & RENDER_NO_LINE_FLAG)
                                {
                                    // Quick quads animation (for gears)...
                                    start_xy[XX] = eye_quad_xy[(eye_frame>>1)&3][XX];
                                    start_xy[YY] = eye_quad_xy[(eye_frame>>1)&3][YY];
                                }
                                else
                                {
                                    // Smoothly scrolling texture...
                                    start_xy[XX] = 0.0f;
                                    start_xy[YY] = eye_frame * 0.03125f;
                                }
                            }
                            else
                            {
                                // Normal bounced animation...  0, 1, 2, 3, 2, 1...
                                eye_frame &= 31;
                                start_xy[XX] = eye_quad_xy[eye_frame_quad[eye_frame]][XX];
                                start_xy[YY] = eye_quad_xy[eye_frame_quad[eye_frame]][YY];
                            }

                            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(j, num_primitive)
                            {
                                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                display_start_strip();
                                repeat(k, num_primitive_vertex)
                                {
                                    vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                    tex_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                    data = tex_vertex_data + (tex_vertex << 3);
                                    texpos_xy[XX] = (DEREF( float, data )) + start_xy[XX];  data += 4;
                                    texpos_xy[YY] = (DEREF( float, data )) + start_xy[YY];  data += 4;
                                    display_texpos(texpos_xy);
                                    data = vertex_data + (vertex << 6);  display_vertex((float*) data);
                                }
                                display_end();
                            }


                            // Fans...
                            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(j, num_primitive)
                            {
                                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                display_start_fan();
                                repeat(k, num_primitive_vertex)
                                {
                                    vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                    tex_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                    data = tex_vertex_data + (tex_vertex << 3);
                                    texpos_xy[XX] = (DEREF( float, data )) + start_xy[XX];  data += 4;
                                    texpos_xy[YY] = (DEREF( float, data )) + start_xy[YY];  data += 4;
                                    display_texpos(texpos_xy);
                                    data = vertex_data + (vertex << 6);  display_vertex((float*) data);
                                }
                                display_end();
                            }

                        }
                        else
                        {
                            // Just a normal simple, everyday texture...
                            // Strips...
                            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(j, num_primitive)
                            {
                                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                display_start_strip();
                                repeat(k, num_primitive_vertex)
                                {
                                    vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                    tex_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                    data = tex_vertex_data + (tex_vertex << 3);  display_texpos((float*) data);
                                    data = vertex_data + (vertex << 6);  display_vertex((float*) data);
                                }
                                display_end();
                            }


                            // Fans...
                            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(j, num_primitive)
                            {
                                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                display_start_fan();
                                repeat(k, num_primitive_vertex)
                                {
                                    vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                    tex_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                    data = tex_vertex_data + (tex_vertex << 3);  display_texpos((float*) data);
                                    data = vertex_data + (vertex << 6);  display_vertex((float*) data);
                                }
                                display_end();
                            }
                        }
                    }


                    // Turn culling back on...
                    if (texture_flags & RENDER_NOCULL_FLAG)
                    {
                        display_cull_on();
                    }

                    // Turn paperdoll off...
                    if (texture_flags & RENDER_PAPER_FLAG)
                    {
                        display_paperdoll_off();
                    }

                    // Turn z writes back on...
                    if (!(texture_flags & RENDER_LIGHT_FLAG) && (texture_alpha < 255))
                    {
                        display_zbuffer_write_on();
                    }
                }

                texture_file_data_block++;
            }











            //            // Draw all visible cartoon lines (for partially transparent characters)
            //#ifdef DEVTOOL
            //            if(((keyb.down[SDLK_F12] && selection.view == VIEW_3D_XYZ) || (line_mode&1)) && main_alpha != 255)
            //#else
            //            if((line_mode&1) && main_alpha != 255)
            //#endif
















            // Backface cartoon line stuff...  Looks much nicer...  No blipping...
#ifdef BACKFACE_CARTOON_LINES

            if (line_mode&1)
            {
                texture_data = texture_data_start;
                display_texture_off();
                display_cull_frontface();
                display_blend_off();
                display_color(line_color);
                repeat(i, MAX_DDD_TEXTURE)
                {
                    texture_mode = *texture_data;  texture_data++;

                    if (texture_mode != 0)
                    {
                        // Funky thing to make layer'd triangles not blip - since we don't do multitexturing...
                        display_depth_layer(i);

                        texture_flags = *texture_data;  texture_data++;
                        texture_alpha = *texture_data;  texture_data++;


                        // Which type of texturing are we doing?
                        if (texture_flags & RENDER_NO_LINE_FLAG)
                        {
                            // We're just skipping this texture...
                            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(j, num_primitive)
                            {
                                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                texture_data += 4 * num_primitive_vertex;
                            }
                            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(j, num_primitive)
                            {
                                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                texture_data += 4 * num_primitive_vertex;
                            }
                        }
                        else
                        {
                            // Draw all of the silly backface things...
                            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(j, num_primitive)
                            {
                                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                display_start_strip();
                                repeat(k, num_primitive_vertex)
                                {
                                    vertex = DEREF( Uint16, texture_data );  texture_data += 4;
                                    data = vertex_data + (vertex << 6);
                                    distance_xyz[XX] = ((float*) data)[XX] + ((float*) (data + 15))[XX] * cartoon_line_size;
                                    distance_xyz[YY] = ((float*) data)[YY] + ((float*) (data + 15))[YY] * cartoon_line_size;
                                    distance_xyz[ZZ] = ((float*) data)[ZZ] + ((float*) (data + 15))[ZZ] * cartoon_line_size;
                                    display_vertex(distance_xyz);
                                }
                                display_end();
                            }


                            // Fans...
                            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(j, num_primitive)
                            {
                                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                display_start_fan();
                                repeat(k, num_primitive_vertex)
                                {
                                    vertex = DEREF( Uint16, texture_data );  texture_data += 4;
                                    data = vertex_data + (vertex << 6);
                                    distance_xyz[XX] = ((float*) data)[XX] + ((float*) (data + 15))[XX] * cartoon_line_size;
                                    distance_xyz[YY] = ((float*) data)[YY] + ((float*) (data + 15))[YY] * cartoon_line_size;
                                    distance_xyz[ZZ] = ((float*) data)[ZZ] + ((float*) (data + 15))[ZZ] * cartoon_line_size;
                                    display_vertex(distance_xyz);
                                }
                                display_end();
                            }
                        }
                    }

                    texture_file_data_block++;
                }
                display_texture_on();
                display_cull_on();
            }








#else
            // Draw cartoon lines
#ifdef DEVTOOL

            if ((keyb.down[SDLK_F12] && selection.view == VIEW_3D_XYZ) || (line_mode&1))
#else
            if (line_mode&1)
#endif
            {
                // Cartoon line data start...
                bone_data += (num_bone * 9);
                num_primitive = DEREF( Uint16, bone_data );  bone_data += 2;


                // Clear out visibility count for each vertex...
                // Get onscreen coordinates of each vertex...  Stored in fourthbuffer...
                if (num_primitive > 0)
                {
                    vertex_bxy = (float*) fourthbuffer;
                    texture_data = vertex_data;
                    repeat(i, num_vertex)
                    {
                        // Proper onscreen coordinate figurin'
                        vertex_bxy[XX] = onscreen_matrix[0] * ((float*) texture_data)[XX] + onscreen_matrix[4] * ((float*) texture_data)[YY] + onscreen_matrix[8]  * ((float*) texture_data)[ZZ] + onscreen_matrix[12];
                        vertex_bxy[YY] = onscreen_matrix[1] * ((float*) texture_data)[XX] + onscreen_matrix[5] * ((float*) texture_data)[YY] + onscreen_matrix[9]  * ((float*) texture_data)[ZZ] + onscreen_matrix[13];
                        dot           = onscreen_matrix[3] * ((float*) texture_data)[XX] + onscreen_matrix[7] * ((float*) texture_data)[YY] + onscreen_matrix[11] * ((float*) texture_data)[ZZ] + onscreen_matrix[15];


                        if (dot != 0.0f)
                        {
                            vertex_bxy[XX] /= dot; vertex_bxy[YY] /= dot;
                        }

                        texture_data += 64;
                        vertex_bxy += 2;
                    }
                    vertex_bxy -= num_vertex << 1;


                    // Determine visibility of each cartoon line...
                    texture_data = bone_data;
                    repeat(i, num_primitive)
                    {
                        // Find a perpendicular to the draw line...
                        vertex = (DEREF( Uint16, texture_data )) << 1;
                        perp_xy[YY] = -vertex_bxy[vertex];
                        vertex++;
                        perp_xy[XX] = vertex_bxy[vertex];
                        vertex = (DEREF( Uint16, texture_data + 2 )) << 1;
                        start_xy[XX] = vertex_bxy[vertex];
                        perp_xy[YY] += vertex_bxy[vertex];
                        vertex++;
                        start_xy[YY] = vertex_bxy[vertex];
                        perp_xy[XX] -= vertex_bxy[vertex];


                        // Check each check point's location against the perpendicular...
                        vertex = (DEREF( Uint16, texture_data + 4 )) << 1;
                        texpos_xy[XX] = vertex_bxy[vertex] - start_xy[XX];
                        vertex++;
                        texpos_xy[YY] = vertex_bxy[vertex] - start_xy[YY];
                        dot = texpos_xy[XX] * perp_xy[XX] + texpos_xy[YY] * perp_xy[YY];
                        vertex = (DEREF( Uint16, texture_data + 6 )) << 1;
                        texpos_xy[XX] = vertex_bxy[vertex] - start_xy[XX];
                        vertex++;
                        texpos_xy[YY] = vertex_bxy[vertex] - start_xy[YY];

                        dot *= (texpos_xy[XX] * perp_xy[XX] + texpos_xy[YY] * perp_xy[YY]);
                        draw_cartoon_line[i] = (dot >= 0.0f);

                        texture_data += 8;
                    }



                    // Happy fat lines...
                    display_texture_off();
                    display_cull_off();
                    display_blend_off();
                    display_color(line_color);
                    repeat(i, num_primitive)
                    {
                        if (draw_cartoon_line[i])
                        {
                            // Find both points of line...  Not really texture stuff...
                            vertex = DEREF( Uint16, bone_data );
                            data = vertex_data + (vertex << 6);
                            tex_vertex = DEREF( Uint16, bone_data + 2 );
                            texture_data = vertex_data + (tex_vertex << 6);


                            // Start drawing the polygon...
                            display_start_fan();
                            {
                                // First point...
                                vertex_bxy[XX] = ((float*) data)[XX];
                                vertex_bxy[YY] = ((float*) data)[YY];
                                vertex_bxy[ZZ] = ((float*) data)[ZZ];
                                display_vertex(vertex_bxy);


                                // Second point...
                                data += 15;
                                vertex_bxy[XX] += ((float*) data)[XX] * cartoon_line_size;
                                vertex_bxy[YY] += ((float*) data)[YY] * cartoon_line_size;
                                vertex_bxy[ZZ] += ((float*) data)[ZZ] * cartoon_line_size;
                                display_vertex(vertex_bxy);


                                // Third point...
                                vertex_bxy[XX] = ((float*) texture_data)[XX];
                                vertex_bxy[YY] = ((float*) texture_data)[YY];
                                vertex_bxy[ZZ] = ((float*) texture_data)[ZZ];
                                texture_data += 15;
                                vertex_bxy[X+3] = vertex_bxy[XX] + ((((float*) texture_data)[XX]) * cartoon_line_size);
                                vertex_bxy[Y+3] = vertex_bxy[YY] + ((((float*) texture_data)[YY]) * cartoon_line_size);
                                vertex_bxy[Z+3] = vertex_bxy[ZZ] + ((((float*) texture_data)[ZZ]) * cartoon_line_size);
                                display_vertex(vertex_bxy + 3);

                                // Fourth point
                                display_vertex(vertex_bxy);
                            }
                            display_end();
                        }

                        bone_data += 8;
                    }
                    display_cull_on();
                    display_texture_on();
                }
            }
#endif




#ifdef DEVTOOL

            // Draw all triangle lines (T key in modeler)...
            if (triangle_lines)
            {
                display_color(red);
                display_texture_off();
                texture_data = texture_data_start;
                repeat(i, MAX_DDD_TEXTURE)
                {
                    texture_mode = *texture_data;  texture_data++;

                    if (texture_mode != 0)
                    {
                        texture_data += 2;

                        // Strips...
                        num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                        repeat(j, num_primitive)
                        {
                            num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                            display_start_line_loop();
                            repeat(k, num_primitive_vertex)
                            {
                                vertex = DEREF( Uint16, texture_data );  texture_data += 4;
                                data = vertex_data + (vertex << 6);
                                display_vertex((float*) data);
                            }
                            display_end();
                        }

                        // Skip fans
                        texture_data += 2;
                    }
                }
                display_end();
                display_texture_on();
            }

            break;
    }

#endif
}

//-----------------------------------------------------------------------------------------------
#define SHADOW_Z_CORRECTION 0.100f
#define SHADOW_MESH_SIZE 5
#define SHADOW_MESH_END  4   // _SIZE-1...
float shadow_mesh_xyz[SHADOW_MESH_SIZE][SHADOW_MESH_SIZE][3];
float shadow_mesh_tex_xy[SHADOW_MESH_SIZE][SHADOW_MESH_SIZE][2];
void render_shadow_setup(void)
{
    // <ZZ> This function sets up the texture coordinates for the shadow meshes...
    Uint16 x, y;
    repeat(y, SHADOW_MESH_SIZE)
    {
        repeat(x, SHADOW_MESH_SIZE)
        {
            shadow_mesh_tex_xy[y][x][XX] = ((float)x) / ((float) (SHADOW_MESH_SIZE - 1));
            shadow_mesh_tex_xy[y][x][YY] = ((float)y) / ((float) (SHADOW_MESH_SIZE - 1));
        }
    }
}

//-----------------------------------------------------------------------------------------------
void render_rdy_character_shadow(Uint8* data, CHR_DATA* chr_data, Uint8 main_alpha, float scale, float z)
{
    // <ZZ> This function draws an RDY object model's shadow.  This is one used ingame...
    //      Other one is for windows...
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_joint;
    Uint16 num_bone;
    Uint8* base_model_data;
    Uint8* texture_data;
    Uint8* frame_data;
    Uint16 i;
    Uint8 base_model, detail_level;
    Uint8 alpha;
    Uint16 frame;
    float vertex_xyz[3];
    float x, y;

    x = chr_data->x;
    y = chr_data->y;
    vertex_xyz[ZZ] = z + SHADOW_Z_CORRECTION;
    main_alpha =  (main_alpha * chr_data->alpha) >> 8;  // Modulate by character alpha...
    frame = chr_data->frame;

    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) { frame = 0; }

    data += (ACTION_MAX << 1);
    texture_data = data;
    data += (MAX_DDD_SHADOW_TEXTURE);

    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );

    base_model_data += 4;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;
    data = frame_data + 11 + (num_bone * 24) + (num_joint * 12);


    // Draw the shadow
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        // Shadow has several layers...
        alpha = *data;  data++;

        if (alpha)
        {
            alpha = (alpha * main_alpha) >> 8;
            shadow_color[3] = alpha;
            display_color_alpha(shadow_color);
            display_pick_texture(shadow_texture[texture_data[i]]);


            // Draw a simple flat shadow at the character's foot level...
            display_start_fan();
            {
                display_texpos_xy(0.0f, 0.0f);
                vertex_xyz[XX] = ((chr_data->sidex * (DEREF( float, data )) + chr_data->frontx * (DEREF( float, data + 4 ))) * scale) + x;
                vertex_xyz[YY] = ((chr_data->sidey * (DEREF( float, data )) + chr_data->fronty * (DEREF( float, data + 4 ))) * scale) + y;
                display_vertex(vertex_xyz);
                data += 8;

                display_texpos_xy(1.0f, 0.0f);
                vertex_xyz[XX] = ((chr_data->sidex * (DEREF( float, data )) + chr_data->frontx * (DEREF( float, data + 4 ))) * scale) + x;
                vertex_xyz[YY] = ((chr_data->sidey * (DEREF( float, data )) + chr_data->fronty * (DEREF( float, data + 4 ))) * scale) + y;
                display_vertex(vertex_xyz);
                data += 8;

                display_texpos_xy(1.0f, 1.0f);
                vertex_xyz[XX] = ((chr_data->sidex * (DEREF( float, data )) + chr_data->frontx * (DEREF( float, data + 4 ))) * scale) + x;
                vertex_xyz[YY] = ((chr_data->sidey * (DEREF( float, data )) + chr_data->fronty * (DEREF( float, data + 4 ))) * scale) + y;
                display_vertex(vertex_xyz);
                data += 8;

                display_texpos_xy(0.0f, 1.0f);
                vertex_xyz[XX] = ((chr_data->sidex * (DEREF( float, data )) + chr_data->frontx * (DEREF( float, data + 4 ))) * scale) + x;
                vertex_xyz[YY] = ((chr_data->sidey * (DEREF( float, data )) + chr_data->fronty * (DEREF( float, data + 4 ))) * scale) + y;
                display_vertex(vertex_xyz);
                data += 8;
            }
            display_end();
        }
    }
}

//-----------------------------------------------------------------------------------------------
void render_rdy_shadow(PSoulfuScriptContext pss, Uint8* data, Uint16 frame, float x, float y, float z, Uint8 mode)
{
    // <ZZ> This function draws an RDY object model's shadow.
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_joint;
    Uint16 num_bone;
    Uint8* base_model_data;
    Uint8* texture_data;
    Uint8* frame_data;
    Uint16 i;
    Uint8 base_model, detail_level;
    Uint8 alpha;
    float vertex_xyz[3];

#ifdef DEVTOOL
    float onscreen_x, onscreen_y;
    Uint16 item, j;
#endif


    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    data += (ACTION_MAX << 1);
    texture_data = data;
    data += (MAX_DDD_SHADOW_TEXTURE);

    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );

    base_model_data += 4;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;


    // Special tools for working with shadows...
#ifdef DEVTOOL

    if ((mode & 15) == 1)
    {
        // Select and modify points...
        if (selection.close_type == BORDER_SELECT)
        {
            data = frame_data + 11 + (num_bone * 24) + (num_joint * 12);
            repeat(i, MAX_DDD_SHADOW_TEXTURE)
            {
                alpha = *data;  data++;

                if (alpha)
                {
                    repeat(j, 4)
                    {
                        item = (i << 4) + j;

                        if (!select_inlist(item))
                        {
                            render_get_point_xy(DEREF( float, data ) + x, DEREF( float, data + 4 ) + y, 0.0f, &onscreen_x, &onscreen_y);

                            if (onscreen_x > selection.box_tl[XX] &&  onscreen_x < selection.box_br[XX])
                            {
                                if (onscreen_y > selection.box_tl[YY] &&  onscreen_y < selection.box_br[YY])
                                {
                                    select_add(item, ((float*) data));
                                }
                            }
                        }

                        data += 8;
                    }
                }
            }
        }
        else if (selection.close_type == BORDER_SPECIAL_SELECT_ALL)
        {
            data = frame_data + 11 + (num_bone * 24) + (num_joint * 12);
            repeat(i, MAX_DDD_SHADOW_TEXTURE)
            {
                alpha = *data;  data++;

                if (alpha)
                {
                    repeat(j, 4)
                    {
                        item = (i << 4) + j;

                        if (!select_inlist(item))
                        {
                            select_add(item, ((float*) data));
                        }

                        data += 8;
                    }
                }
            }
            selection.close_type = 0;
        }
        else if (selection.close_type == BORDER_SPECIAL_SELECT_INVERT)
        {
            data = frame_data + 11 + (num_bone * 24) + (num_joint * 12);
            repeat(i, MAX_DDD_SHADOW_TEXTURE)
            {
                alpha = *data;  data++;

                if (alpha)
                {
                    repeat(j, 4)
                    {
                        item = (i << 4) + j;

                        if (!select_inlist(item))
                        {
                            select_add(item, ((float*) data));
                        }
                        else
                        {
                            // Items with a NULL data get removed later
                            select_data[select_index] = NULL;
                        }

                        data += 8;
                    }
                }
            }
            repeat(i, select_num)
            {
                if (select_data[i] == NULL)
                {
                    select_remove(i);
                    i--;
                }
            }
            selection.close_type = 0;
        }
        else if (selection.close_type == BORDER_SPECIAL_SELECT_CONNECTED)
        {
            data = frame_data + 11 + (num_bone * 24) + (num_joint * 12);
            repeat(i, MAX_DDD_SHADOW_TEXTURE)
            {
                alpha = *data;  data++;

                if (alpha)
                {
                    item = i << 4;

                    if (select_inlist(item) || select_inlist((Uint16) (item + 1)) || select_inlist((Uint16) (item + 2)) || select_inlist((Uint16) (item + 3)))
                    {
                        repeat(j, 4)
                        {
                            item = (i << 4) + j;

                            if (!select_inlist(item))
                            {
                                select_add(item, ((float*) data));
                            }

                            data += 8;
                        }
                    }
                    else
                    {
                        data += 32;
                    }
                }
                else
                {
                    data += 32;
                }
            }
            selection.close_type = 0;
        }
        else if (selection.close_type == BORDER_MOVE)
        {
            if (selection.move_on)
            {
                render_get_point_xyz(&(pss->screen), mouse.x, mouse.y, &selection.offset_xyz[XX], &selection.offset_xyz[YY], &onscreen_y);
                render_tex_move(kfalse);
            }
            else
            {
                render_get_point_xyz(&(pss->screen), mouse.x, mouse.y, &selection.center_xyz[XX], &selection.center_xyz[YY], &onscreen_y);
                select_update_xy();
            }

            selection.close_type = 0;
        }
        else if (selection.close_type == BORDER_CROSS_HAIRS)
        {
            render_get_point_xyz(&(pss->screen), mouse.x, mouse.y, &selection.center_xyz[XX], &selection.center_xyz[YY], &onscreen_y);
            selection.close_type = 0;
        }


        // Draw the shadow vertices
        display_texture_off();
        data = frame_data + 11 + (num_bone * 24) + (num_joint * 12);
        repeat(i, MAX_DDD_SHADOW_TEXTURE)
        {
            alpha = *data;  data++;

            if (alpha)
            {
                repeat(j, 4)
                {
                    item = (i << 4) + j;

                    if (select_inlist(item))
                    {
                        display_marker(white, DEREF( float, data ) + x, DEREF( float, data + 4 ) + y, z, 0.05f);
                    }
                    else
                    {
                        display_marker(blue, DEREF( float, data ) + x, DEREF( float, data + 4 ) + y, z, 0.05f);
                    }

                    data += 8;
                }
            }
        }
    }

    display_texture_on();
#endif


    display_zbuffer_write_off();


    // Draw the shadow
    data = frame_data + 11 + (num_bone * 24) + (num_joint * 12);
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        alpha = *data;  data++;

        if (alpha)
        {
            shadow_color[3] = alpha;
            display_color_alpha(shadow_color);
            display_pick_texture(shadow_texture[texture_data[i]]);
            display_start_fan();
            {
                display_texpos_xy(0.0f, 0.0f);
                vertex_xyz[XX] = (pss->matrix[0]) * (DEREF( float, data )) + (pss->matrix[3]) * (DEREF( float, data + 4 )) + x;
                vertex_xyz[YY] = (pss->matrix[1]) * (DEREF( float, data )) + (pss->matrix[4]) * (DEREF( float, data + 4 )) + y;
                vertex_xyz[ZZ] = z;
                display_vertex(vertex_xyz);
                data += 8;

                display_texpos_xy(1.0f, 0.0f);
                vertex_xyz[XX] = (pss->matrix[0]) * (DEREF( float, data )) + (pss->matrix[3]) * (DEREF( float, data + 4 )) + x;
                vertex_xyz[YY] = (pss->matrix[1]) * (DEREF( float, data )) + (pss->matrix[4]) * (DEREF( float, data + 4 )) + y;
                display_vertex(vertex_xyz);
                data += 8;

                display_texpos_xy(1.0f, 1.0f);
                vertex_xyz[XX] = (pss->matrix[0]) * (DEREF( float, data )) + (pss->matrix[3]) * (DEREF( float, data + 4 )) + x;
                vertex_xyz[YY] = (pss->matrix[1]) * (DEREF( float, data )) + (pss->matrix[4]) * (DEREF( float, data + 4 )) + y;
                display_vertex(vertex_xyz);
                data += 8;

                display_texpos_xy(0.0f, 1.0f);
                vertex_xyz[XX] = (pss->matrix[0]) * (DEREF( float, data )) + (pss->matrix[3]) * (DEREF( float, data + 4 )) + x;
                vertex_xyz[YY] = (pss->matrix[1]) * (DEREF( float, data )) + (pss->matrix[4]) * (DEREF( float, data + 4 )) + y;
                display_vertex(vertex_xyz);
                data += 8;


                //                display_texpos_xy(0.0f, 0.0f);
                //                display_vertex_xyz(DEREF( float, data )+x, DEREF( float, data+4 )+y, z);
                //                data += 8;

                //                display_texpos_xy(1.0f, 0.0f);
                //                display_vertex_xyz(DEREF( float, data )+x, DEREF( float, data+4 )+y, z);
                //                data += 8;

                //                display_texpos_xy(1.0f, 1.0f);
                //                display_vertex_xyz(DEREF( float, data )+x, DEREF( float, data+4 )+y, z);
                //                data += 8;

                //                display_texpos_xy(0.0f, 1.0f);
                //                display_vertex_xyz(DEREF( float, data )+x, DEREF( float, data+4 )+y, z);
                //                data += 8;
            }
            display_end();
        }
    }


    display_zbuffer_write_on();
}

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint16* last_triangle_added_data;
Uint8 render_insert_triangle(Uint8* data, Uint16 frame, Uint16 insert_mode, Uint8 texture)
{
    // <ZZ> This function adds a triangle to a given model, using the first three selected vertices
    //      for placement.  If insert_mode is kfalse, it removes the given triangle.  A return
    //      value of ktrue means that it worked, kfalse means it didn't.
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint8* frame_data;
    Uint8* texture_data;
    Uint8* temp_data;
    Uint16 i, j, k;
    Uint8 texture_mode;
    Uint16 num_primitive, num_primitive_vertex;
    int amount_added;
    Uint8 base_model, detail_level;


    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return kfalse;

    if (texture >= MAX_DDD_TEXTURE) return kfalse;

    if (select_num < 3) return kfalse;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    data += 8;
    texture_data = DEREF( Uint8*, data );  data += 4;



    // Walk through texture data until we come to the appropriate texture
    repeat(i, texture)
    {
        texture_mode = *texture_data;  texture_data++;

        if (texture_mode != 0)
        {
            texture_data += 2;

            // Strips...
            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
            repeat(j, num_primitive)
            {
                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                repeat(k, num_primitive_vertex)
                {
                    texture_data += 4;
                }
            }


            // Fans...  Shouldn't be any in DEVTOOL mode...
            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
            repeat(j, num_primitive)
            {
                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                repeat(k, num_primitive_vertex)
                {
                    texture_data += 4;
                }
            }
        }
    }



    // Pick the type of operation
    amount_added = 0;

    if (insert_mode)
    {
        texture_mode = *texture_data;  texture_data++;

        if (texture_mode != 0)
        {
            // Texture is turned on...
            texture_data += 2;


            // Strips...
            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;

            if (sdf_insert_data(texture_data, NULL, 14))
            {
                num_primitive++;
                DEREF( Uint16, texture_data - 2 ) = num_primitive;
                last_triangle_added_data = ((Uint16*) texture_data);
                DEREF( Uint16, texture_data ) = 3;
                DEREF( Uint16, texture_data + 2 )  = select_list[0];
                DEREF( Uint16, texture_data + 4 )  = 0;
                DEREF( Uint16, texture_data + 6 )  = select_list[1];
                DEREF( Uint16, texture_data + 8 )  = 0;
                DEREF( Uint16, texture_data + 10 ) = select_list[2];
                DEREF( Uint16, texture_data + 12 ) = 0;
                amount_added = 14;
            }
        }
        else
        {
            // Need to turn texture on
            if (sdf_insert_data(texture_data, NULL, 20))
            {
                *(texture_data - 1) = 1;                                        // Texture mode
                *(texture_data) = 0;                                            // Flags
                *(texture_data + 1) = 255;                                      // Alpha
                DEREF( Uint16, texture_data + 2 )  = 1;                   // Number of strips
                last_triangle_added_data = ((Uint16*) (texture_data + 4));
                DEREF( Uint16, texture_data + 4 )  = 3;                   // Number of vertices
                DEREF( Uint16, texture_data + 6 )  = select_list[0];
                DEREF( Uint16, texture_data + 8 )  = 0;
                DEREF( Uint16, texture_data + 10 )  = select_list[1];
                DEREF( Uint16, texture_data + 12 )  = 0;
                DEREF( Uint16, texture_data + 14 ) = select_list[2];
                DEREF( Uint16, texture_data + 16 ) = 0;
                DEREF( Uint16, texture_data + 18 ) = 0;                   // Number of fans
                amount_added = 20;
            }
        }
    }
    else
    {
        // Deleting...
        temp_data = texture_data;
        texture_mode = *texture_data;  texture_data++;

        if (texture_mode != 0)
        {
            // Texture is turned on...
            texture_data += 2;


            // Strips...
            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
            repeat(i, num_primitive)
            {
                amount_added = 0;
                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                repeat(j, num_primitive_vertex)
                {
                    if (DEREF( Uint16, texture_data ) == select_list[0] || DEREF( Uint16, texture_data ) == select_list[1] || DEREF( Uint16, texture_data ) == select_list[2])
                    {
                        amount_added++;
                    }

                    texture_data += 4;
                }

                if (amount_added == num_primitive_vertex)
                {
                    // Found a match...  Delete it...
                    if (num_primitive == 1)
                    {
                        // Last one for this texture...  Delete the whole thing...  Assume there aren't any fans...
                        if (sdf_insert_data(temp_data, NULL, -8 - (num_primitive_vertex << 2)))
                        {
                            *temp_data = 0;  // Turn off the texture
                            amount_added = -8 - (num_primitive_vertex << 2);
                        }
                    }
                    else
                    {
                        // Delete the strip and down the counter
                        texture_data -= (num_primitive_vertex << 2) + 2;

                        if (sdf_insert_data(texture_data, NULL, -2 - (num_primitive_vertex << 2)))
                        {
                            DEREF( Uint16, temp_data + 3 ) = num_primitive - 1;  // Down the count for number of strips
                            amount_added = -2 - (num_primitive_vertex << 2);
                        }
                    }

                    i = num_primitive;
                }
                else
                {
                    // No match yet
                    amount_added = 0;
                }
            }
        }
    }



    if (amount_added != 0)
    {
        // Update all base model pointers at start of file
        DEREF( Uint8*, data ) += amount_added;  data += 4;
        DEREF( Uint8*, data ) += amount_added;  data += 4;
        base_model++;

        while (base_model < num_base_model)
        {
            DEREF( Uint8*, data ) += amount_added;  data += 4;
            DEREF( Uint8*, data ) += amount_added;  data += 4;
            DEREF( Uint8*, data ) += amount_added;  data += 4;
            DEREF( Uint8*, data ) += amount_added;  data += 4;
            DEREF( Uint8*, data ) += amount_added;  data += 4;
            base_model++;
        }


        // Update bone frame pointers, only if bone frames are internal to this file (not linked to another RDY)
        if ((flags & DDD_EXTERNAL_BONE_FRAMES) == 0)
        {
            repeat(i, num_bone_frame)
            {
                DEREF( Uint8*, data ) += amount_added;  data += 4;
            }
        }

        return ktrue;
    }

    return kfalse;
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint8 render_insert_vertex(Uint8* data, Uint16 frame, float* vertex_xyz, Uint16 vertex_to_remove, Uint16 vertex_replacement)
{
    // <ZZ> This function adds a vertex to a given model.  Returns ktrue if it worked.  If vertex_xyz
    //      is NULL, the function removes a vertex instead (and associated triangles).  Vertex_replacement
    //      is used to replace a vertex (with a lower valued one) instead of deleting associated triangles.
    //      A value of MAX_VERTEX means to delete the triangles...
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_vertex, num_tex_vertex, num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* texture_data;
    Uint8* temp_data;
    Uint8* start_data;
    Uint8* bone_data;
    Uint16 i, j, k;
    Uint8 texture_mode;
    Uint16 amount_removed, num_primitive, num_primitive_vertex, vertex;
    Uint8 base_model, detail_level;
    Uint8 found_match;



    start_data = data;
    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return kfalse;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 4;
    bone_data =  DEREF( Uint8*, data + 12 );

    num_vertex = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_tex_vertex = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;

    if (vertex_xyz != NULL)
    {
        // Insert a vertex
        if (sdf_insert_data(base_model_data + (num_vertex << 6), NULL, 64))
        {
            // Added the point successfully...  Write coordinates
            select_add(num_vertex, ((float*) (base_model_data + (num_vertex << 6))));
            DEREF( float, base_model_data + (num_vertex << 6) ) = vertex_xyz[XX];
            DEREF( float, base_model_data + 4 + (num_vertex << 6) ) = vertex_xyz[YY];
            DEREF( float, base_model_data + 8 + (num_vertex << 6) ) = vertex_xyz[ZZ];


            // Default bone info
            *(base_model_data + 12 + (num_vertex << 6)) = 0;
            *(base_model_data + 13 + (num_vertex << 6)) = 0;
            //            *(base_model_data+14+(num_vertex<<6)) = 128;
            *(base_model_data + 14 + (num_vertex << 6)) = 64;  // !!!ANCHOR!!!


            // Update number of vertices for this base model
            num_vertex++;
            DEREF( Uint16, base_model_data - 8 ) = num_vertex;


            // Update all base model pointers at start of file
            DEREF( Uint8*, data ) += 64;  data += 4;
            DEREF( Uint8*, data ) += 64;  data += 4;
            DEREF( Uint8*, data ) += 64;  data += 4;
            DEREF( Uint8*, data ) += 64;  data += 4;
            base_model++;

            while (base_model < num_base_model)
            {
                DEREF( Uint8*, data ) += 64;  data += 4;
                DEREF( Uint8*, data ) += 64;  data += 4;
                DEREF( Uint8*, data ) += 64;  data += 4;
                DEREF( Uint8*, data ) += 64;  data += 4;
                DEREF( Uint8*, data ) += 64;  data += 4;
                base_model++;
            }


            // Update bone frame pointers, only if bone frames are internal to this file (not linked to another RDY)
            if ((flags & DDD_EXTERNAL_BONE_FRAMES) == 0)
            {
                repeat(i, num_bone_frame)
                {
                    DEREF( Uint8*, data ) += 64;  data += 4;
                }
            }


            // Calculate scalars and weight and attachments...
            render_attach_vertex_to_bone(start_data, frame, (Uint16) (num_vertex - 1));
            render_crunch_vertex(start_data, frame, (Uint16) (num_vertex - 1), ktrue, 0);
            return ktrue;
        }
    }
    else
    {
        // Delete a given vertex
        if (vertex_to_remove < num_vertex)
        {
            if (sdf_insert_data(base_model_data + (vertex_to_remove << 6), NULL, -64))
            {
                // Deleted the vertex successfully...  Update number of vertices for this base model
                num_vertex--;
                DEREF( Uint16, base_model_data - 8 ) = num_vertex;
                amount_removed = 64;


                // Delete all connected textures...
                texture_data = base_model_data + (num_vertex << 6);
                texture_data += (num_tex_vertex << 3);


                repeat(i, MAX_DDD_TEXTURE)
                {
                    texture_mode = *texture_data;  texture_data++;

                    if (texture_mode != 0)
                    {
                        texture_data += 2;

                        // Strips...
                        temp_data = texture_data;
                        num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                        repeat(j, num_primitive)
                        {
                            found_match = kfalse;
                            num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(k, num_primitive_vertex)
                            {
                                vertex = DEREF( Uint16, texture_data );

                                if (vertex == vertex_to_remove) found_match = ktrue;

                                if (vertex > vertex_to_remove)
                                {
                                    DEREF( Uint16, texture_data ) = vertex - 1;
                                }

                                if (found_match && vertex_replacement != MAX_VERTEX)
                                {
                                    // Don't really delete the triangle, just replace the deleted vertex with this one...
                                    DEREF( Uint16, texture_data ) = vertex_replacement;
                                    found_match = kfalse;
                                }

                                texture_data += 4;
                            }

                            if (found_match)
                            {
                                // This triangle uses the vertex...  Delete it...
                                if (sdf_insert_data(texture_data - 2 - (num_primitive_vertex << 2), NULL, -2 - (num_primitive_vertex << 2)))
                                {
                                    amount_removed += 2 + (num_primitive_vertex << 2);
                                    texture_data -= 2 + (num_primitive_vertex << 2);
                                    num_primitive--;  j--;
                                }
                            }
                        }
                        DEREF( Uint16, temp_data ) = num_primitive;

                        if (num_primitive == 0)
                        {
                            // Whole texture has been deleted
                            if (sdf_insert_data(temp_data - 2, NULL, -6))
                            {
                                *(temp_data - 3) = 0;
                                texture_data = temp_data - 2;
                                amount_removed += 6;
                            }
                        }
                        else
                        {
                            // Fans...  Shouldn't be any in DEVTOOL mode...
                            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                            repeat(j, num_primitive)
                            {
                                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                                repeat(k, num_primitive_vertex)
                                {
                                    texture_data += 4;
                                }
                            }
                        }
                    }
                }




                // Update all base model pointers at start of file
                DEREF( Uint8*, data ) -= 64;  data += 4;
                DEREF( Uint8*, data ) -= 64;  data += 4;
                DEREF( Uint8*, data ) -= amount_removed;  data += 4;
                DEREF( Uint8*, data ) -= amount_removed;  data += 4;
                base_model++;

                while (base_model < num_base_model)
                {
                    DEREF( Uint8*, data ) -= amount_removed;  data += 4;
                    DEREF( Uint8*, data ) -= amount_removed;  data += 4;
                    DEREF( Uint8*, data ) -= amount_removed;  data += 4;
                    DEREF( Uint8*, data ) -= amount_removed;  data += 4;
                    DEREF( Uint8*, data ) -= amount_removed;  data += 4;
                    base_model++;
                }


                // Update bone frame pointers, only if bone frames are internal to this file (not linked to another RDY)
                if ((flags & DDD_EXTERNAL_BONE_FRAMES) == 0)
                {
                    repeat(i, num_bone_frame)
                    {
                        DEREF( Uint8*, data ) -= amount_removed;  data += 4;
                    }
                }

                return ktrue;
            }

        }
    }

    return kfalse;
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
int num_copy_vertex = 0;
int num_copy_tex_vertex = 0;
int num_copy_triangle[MAX_DDD_TEXTURE];
float copy_vertex_xyz[MAX_VERTEX][3];
float copy_tex_vertex_xy[MAX_TEX_VERTEX][2];
Uint16 copy_tex_vertex_number[MAX_TEX_VERTEX];
Uint16 copy_triangle_vertices[MAX_DDD_TEXTURE][MAX_TRIANGLE][3];
Uint16 copy_triangle_tex_vertices[MAX_DDD_TEXTURE][MAX_TRIANGLE][3];
void render_copy_selected(Uint8* data, Uint16 frame)
{
    // <ZZ> This function copies selected points, tex vertices, and triangles to a
    //      temporary buffer area, so it can be pasted in later.
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_vertex;
    Uint8* base_model_data;
    float* tex_vertex_data;
    Uint8* texture_data;
    Uint8* frame_data;
    Uint16 i, j, k, m;
    Uint8 base_model;
    Uint16 vertex[4];
    Uint16 tex_vertex[4];
    Uint8 tex_vertex_new[4];
    Uint8 texture_mode;
    Uint16 num_primitive;
    Uint16 num_primitive_vertex;
    Uint8 found_match;


    // Clear out the last copy
    num_copy_vertex = 0;
    num_copy_tex_vertex = 0;
    repeat(i, MAX_DDD_TEXTURE)
    {
        num_copy_triangle[i] = 0;
    }


    // Read the rdy header
    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;
    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    data = data + (base_model * 20);
    base_model_data = DEREF( Uint8*, data );
    num_vertex = DEREF( Uint16, base_model_data );
    tex_vertex_data = DEREF( float*, data + 4 );
    texture_data = DEREF( Uint8*, data + 8 );


    // Save the selected vertex coordinates
    repeat(i, select_num)
    {
        copy_vertex_xyz[i][XX] = select_xyz[i][XX];
        copy_vertex_xyz[i][YY] = select_xyz[i][YY];
        copy_vertex_xyz[i][ZZ] = select_xyz[i][ZZ];
    }
    num_copy_vertex = select_num;


    // Save the selected triangles, and related tex vertices as we go
    repeat(i, MAX_DDD_TEXTURE)
    {
        texture_mode = *texture_data;  texture_data++;

        if (texture_mode != 0)
        {
            texture_data += 2;

            // Strips...
            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
            repeat(j, num_primitive)
            {
                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                // See if it's selected...
                found_match = ktrue;
                repeat(k, num_primitive_vertex)
                {
                    vertex[k&3] = DEREF( Uint16, texture_data );  texture_data += 2;
                    tex_vertex[k&3] = DEREF( Uint16, texture_data );  texture_data += 2;

                    if (select_inlist(vertex[k&3]))
                    {
                        vertex[k&3] = select_index;
                        tex_vertex_new[k&3] = ktrue;
                        repeat(m, num_copy_tex_vertex)
                        {
                            if (tex_vertex[k&3] == copy_tex_vertex_number[m])
                            {
                                // Tex vertex has already been copied...
                                tex_vertex[k&3] = m;
                                m = num_copy_tex_vertex;
                                tex_vertex_new[k&3] = kfalse;
                            }
                        }
                    }
                    else found_match = kfalse;
                }

                if (found_match)
                {
                    // Add any new tex vertices...
                    repeat(k, 3)
                    {
                        if (tex_vertex_new[k])
                        {
                            copy_tex_vertex_xy[num_copy_tex_vertex][XX] = *(tex_vertex_data + (tex_vertex[k] << 1));
                            copy_tex_vertex_xy[num_copy_tex_vertex][YY] = *(tex_vertex_data + (tex_vertex[k] << 1) + 1);
                            copy_tex_vertex_number[num_copy_tex_vertex] = tex_vertex[k];
                            tex_vertex[k] = num_copy_tex_vertex;
                            num_copy_tex_vertex++;
                        }
                    }


                    // Copy this triangle...
                    repeat(k, 3)
                    {
                        copy_triangle_vertices[i][num_copy_triangle[i]][k] = vertex[k];
                        copy_triangle_tex_vertices[i][num_copy_triangle[i]][k] = tex_vertex[k];
                    }
                    num_copy_triangle[i]++;
                }
            }


            // Fans...  Shouldn't be any in DEVTOOL mode...
            texture_data += 2;
        }
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_paste_selected(Uint8* data, Uint16 frame, Uint8 start_texture)
{
    // <ZZ> This function pastes data that had been copied back into a model...  If start_texture
    //      is > 0, the textures are rotated up...  For making multitexturing easier...
    Uint8 num_base_model;
    Uint8* base_model_data;
    Uint8* start_data;
    Uint8* frame_data;
    float* tex_vertex_data;
    Uint16 i, j;
    Uint8 base_model;
    Uint16 first_vertex;
    Uint16 first_tex_vertex;


    // Read the rdy header to find out our starting vertices...
    start_data = data;
    data += 3;
    num_base_model = *data;  data += 3;
    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    data = data + (base_model * 20);
    base_model_data = DEREF( Uint8*, data );
    tex_vertex_data = DEREF( float*, data + 4 );
    first_vertex = DEREF( Uint16, base_model_data );
    first_tex_vertex = DEREF( Uint16, base_model_data + 2 );
    base_model_data += 8;


    // Add all new vertices
    repeat(i, num_copy_vertex)
    {
        if (render_insert_vertex(start_data, frame, copy_vertex_xyz[i], 0, MAX_VERTEX) == kfalse) return;
    }


    // Add all new tex vertices
    repeat(i, num_copy_tex_vertex)
    {
        if (render_insert_tex_vertex(start_data, frame, copy_tex_vertex_xy[i], 0, MAX_TEX_VERTEX) == kfalse) return;
    }


    // Add all new triangles
    repeat(i, MAX_DDD_TEXTURE)
    {
        repeat(j, num_copy_triangle[i])
        {
            select_clear();
            select_add((Uint16) (copy_triangle_vertices[i][j][0] + first_vertex), (float*)(base_model_data + ((copy_triangle_vertices[i][j][0] + first_vertex) << 6)));
            select_add((Uint16) (copy_triangle_vertices[i][j][1] + first_vertex), (float*)(base_model_data + ((copy_triangle_vertices[i][j][1] + first_vertex) << 6)));
            select_add((Uint16) (copy_triangle_vertices[i][j][2] + first_vertex), (float*)(base_model_data + ((copy_triangle_vertices[i][j][2] + first_vertex) << 6)));

            if (render_insert_triangle(start_data, frame, ktrue, (Uint8) ((i + start_texture)&3)) == kfalse) return;
            else
            {
                *(last_triangle_added_data + 2) = copy_triangle_tex_vertices[i][j][0] + first_tex_vertex;
                *(last_triangle_added_data + 4) = copy_triangle_tex_vertices[i][j][1] + first_tex_vertex;
                *(last_triangle_added_data + 6) = copy_triangle_tex_vertices[i][j][2] + first_tex_vertex;
            }
        }
    }


    // Now select the newly pasted vertices...
    select_clear();
    repeat(i, num_copy_vertex)
    {
        select_add((Uint16) (i + first_vertex), (float*)(base_model_data + ((i + first_vertex) << 6)));
    }
}
#endif


//-----------------------------------------------------------------------------------------------
void render_fill_temp_character_bone_number(Uint8* data)
{
    // <ZZ> This function helps us find a bone by a given name, by building us a cute little table...
    Uint8 num_base_model;
    Uint16 num_bone;
    Uint8* base_model_data;
    Uint8* bone_data;
    Uint8* frame_data;
    Uint16 i;
    Uint8 base_model;
    Uint16 frame;



    data += 3;
    num_base_model = *data;  data += 3;
    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame = 0;
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    data = data + (base_model * 20);
    base_model_data = DEREF( Uint8*, data );  data += 16;
    bone_data = DEREF( Uint8*, data );
    base_model_data += 6;
    num_bone = DEREF( Uint16, base_model_data );


    // Find the bone numbers for named bones...  Left, Right, Left2, Right2, Saddle...
    repeat(i, 8)
    {
        temp_character_bone_number[i] = 255;
    }
    repeat(i, num_bone)
    {
        temp_character_bone_number[(*(bone_data+(i*9))) & 7] = (Uint8) i;
    }
}


//-----------------------------------------------------------------------------------------------
Sint8 render_bone_id(Uint8* data, Uint16 frame, Uint16 joint_one, Uint16 joint_two, Uint8 bone_id)
{
    // <ZZ> This function sets the id of a given bone.  If the id is non-zero, the length is set
    //      to 1.0f, as it's being used as a weapon grip or a saddle.
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_bone;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* frame_data_start;
    Uint8* bone_data;
    Uint16 i;
    Uint8 base_model;




    flags = DEREF( Uint16, data ); data += 2;

    if ((flags & DDD_EXTERNAL_BONE_FRAMES)) return kfalse;

    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return kfalse;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data_start = data + (num_base_model * 20 * DETAIL_LEVEL_MAX);
    frame_data = DEREF( Uint8*, frame_data_start + (frame << 2) );
    base_model = *(frame_data + 2);
    data = data + (base_model * 20);
    base_model_data = DEREF( Uint8*, data );  data += 16;
    bone_data =  DEREF( Uint8*, data );

    base_model_data += 6;
    num_bone = DEREF( Uint16, base_model_data );



    // Look for a matching joint pair
    repeat(i, num_bone)
    {
        if (joint_one == DEREF( Uint16, bone_data + 1 ) || joint_one == DEREF( Uint16, bone_data + 3 ))
        {
            if (joint_two == DEREF( Uint16, bone_data + 1 ) || joint_two == DEREF( Uint16, bone_data + 3 ))
            {
                // Write the ID and length
                *bone_data = bone_id;

                if (bone_id != 0)
                {
                    DEREF( float, bone_data + 5 ) = 1.0f;
                }

                i = num_bone;
            }
        }

        bone_data += 9;
    }

    return ktrue;
}

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint8 render_insert_bone(Uint8* data, Uint16 frame, Uint16 joint_one, Uint16 joint_two, Uint8 insert_bone, Uint8 bone_id)
{
    // <ZZ> This function adds a bone to a given model.  Returns ktrue if it worked.  If insert_bone
    //      is kfalse, the function removes a bone instead.  Recalculates all vertex attachments
    //      afterwards.
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_vertex, num_tex_vertex, num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* frame_data_start;
    Uint8* start_data;
    Uint8* bone_data;
    Uint8* joint_data;
    Uint16 i, j;
    Uint8 base_model, detail_level;
    int amount_to_add;
    Uint8 single_joint;
    Uint8 remove_bone[MAX_BONE];
    Uint8 removed_all_bones;



    single_joint = kfalse;
    start_data = data;
    flags = DEREF( Uint16, data ); data += 2;

    if ((flags & DDD_EXTERNAL_BONE_FRAMES)) return kfalse;

    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return kfalse;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data_start = data + (num_base_model * 20 * DETAIL_LEVEL_MAX);
    frame_data = DEREF( Uint8*, frame_data_start + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 4;
    joint_data =  DEREF( Uint8*, data + 8 );
    bone_data =  DEREF( Uint8*, data + 12 );

    num_vertex = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_tex_vertex = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;

    if (insert_bone)
    {
        // Insert the bone
        if (joint_one >= num_joint)  return kfalse;

        if (joint_two >= num_joint)  return kfalse;

        if (num_bone == 255) return kfalse;



        // Go through all frame data looking for matching base models to add bone normals...
        repeat(i, num_bone_frame)
        {
            frame_data = DEREF( Uint8*, frame_data_start + (i << 2) );
            j = *(frame_data + 2);  // Base model for this frame...

            if (j == base_model)
            {
                // Need to update this frame...  Insert mocked up normals...
                frame_data += 11;
                frame_data += (num_bone * 24);

                if (sdf_insert_data(frame_data, NULL, 24))
                {
                    // Put in the normal info...
                    DEREF( float, frame_data ) = 0;
                    DEREF( float, frame_data + 4 ) = 1;
                    DEREF( float, frame_data + 8 ) = 0;
                    DEREF( float, frame_data + 12 ) = 1;
                    DEREF( float, frame_data + 16 ) = 0;
                    DEREF( float, frame_data + 20 ) = 0;


                    // Update frame data pointers...
                    j = i + 1;

                    while (j < num_bone_frame)
                    {
                        DEREF( Uint8*, frame_data_start + (j << 2) ) += 24;
                        j++;
                    }
                }
            }
        }



        bone_data += (num_bone * 9);

        if (sdf_insert_data(bone_data, NULL, 9))
        {
            // Write the ID and bone attachments
            *bone_data = bone_id;
            DEREF( Uint16, bone_data + 1 ) = joint_one;
            DEREF( Uint16, bone_data + 3 ) = joint_two;


            // Update number of bones for this base model
            num_bone++;
            DEREF( Uint16, base_model_data - 2 ) = num_bone;
            amount_to_add = 9;
        }
    }
    else
    {
        // Delete a bone which is described by a pair of joints, or delete all bones touching a single joint
        if (joint_one >= num_joint && joint_two >= num_joint) return kfalse;

        single_joint = (joint_one >= num_joint || joint_two >= num_joint);

        if (single_joint && joint_one >= num_joint)  joint_one = joint_two;

        log_info(0, "Prior bone list, Removing joint %d", joint_one);

        // Make a list of the bones we need to remove
        removed_all_bones = ktrue;
        amount_to_add = 0;
        repeat(i, num_bone)
        {
            remove_bone[i] = kfalse;
            log_info(0, "Bone %d, Joint %d to %d", i, DEREF( Uint16, bone_data + (i*9) + 1 ), DEREF( Uint16, bone_data + (i*9) + 3 ));

            if (single_joint)
            {
                if (DEREF( Uint16, bone_data + (i*9) + 1 ) == joint_one || DEREF( Uint16, bone_data + (i*9) + 3 ) == joint_one)
                {
                    if (amount_to_add < (num_bone - 1))
                    {
                        remove_bone[i] = ktrue;
                        amount_to_add++;
                    }
                    else
                    {
                        log_info(0, "Cannot remove bone %d", i);
                        removed_all_bones = kfalse;
                    }
                }
            }
            else
            {
                if ((DEREF( Uint16, bone_data + (i*9) + 1 ) == joint_one && DEREF( Uint16, bone_data + (i*9) + 3 ) == joint_two) || (DEREF( Uint16, bone_data + (i*9) + 3 ) == joint_one && DEREF( Uint16, bone_data + (i*9) + 1 ) == joint_two))
                {
                    if (amount_to_add < (num_bone - 1))
                    {
                        remove_bone[i] = ktrue;
                        amount_to_add++;
                    }
                    else
                    {
                        log_info(0, "Cannot remove bone %d", i);
                        removed_all_bones = kfalse;
                    }
                }
            }
        }




        // Go through all frame data looking for matching base models to remove bone normals...  Do backwards...
        repeat(i, num_bone_frame)
        {
            frame_data = DEREF( Uint8*, frame_data_start + (i << 2) );
            j = *(frame_data + 2);  // Base model for this frame...

            if (j == base_model)
            {
                // Need to update this frame...
                amount_to_add = 0;
                frame_data += 11;
                frame_data += (num_bone * 24);
                j = num_bone;

                while (j > 0)
                {
                    j--;
                    frame_data -= 24;

                    if (remove_bone[j])
                    {
                        if (sdf_insert_data(frame_data, NULL, -24))
                        {
                            amount_to_add -= 24;
                        }
                    }
                }



                // Update frame data pointers...
                j = i + 1;

                while (j < num_bone_frame)
                {
                    DEREF( Uint8*, frame_data_start + (j << 2) ) += amount_to_add;
                    j++;
                }
            }
        }



        // Remove the bones we found earlier (do backwards to keep numbering correct)
        // Renumber joints if single_joint mode, because that only happens when a joint is removed...
        j = num_bone;
        bone_data += (num_bone * 9);
        amount_to_add = 0;

        while (j > 0)
        {
            j--;
            bone_data -= 9;

            if (remove_bone[j])
            {
                if (sdf_insert_data(bone_data, NULL, -9))
                {
                    amount_to_add -= 9;
                    num_bone--;
                }
            }
            else if (single_joint && removed_all_bones)
            {
                // Mark down higher numbered joints
                if (DEREF( Uint16, bone_data + 1 ) > joint_one)
                {
                    DEREF( Uint16, bone_data + 1 ) = DEREF( Uint16, bone_data + 1 ) - 1;
                }

                if (DEREF( Uint16, bone_data + 3 ) > joint_one)
                {
                    DEREF( Uint16, bone_data + 3 ) = DEREF( Uint16, bone_data + 3 ) - 1;
                }
            }
        }


        log_info(0, "Removed %d bones", amount_to_add / -9);



        // !!!BAD!!!
        // !!!BAD!!!
        // !!!BAD!!!
        log_info(0, "Final bone list, %d bones, Removed joint %d", num_bone, joint_one);
        repeat(j, num_bone)
        {
            log_info(0, "Bone %d, Joint %d to %d", j, DEREF( Uint16, bone_data + 1 ), DEREF( Uint16, bone_data + 3 ));
            bone_data += 9;
        }
        // !!!BAD!!!
        // !!!BAD!!!
        // !!!BAD!!!


        // Write the new number of bones...
        DEREF( Uint16, base_model_data - 2 ) = num_bone;

        if (amount_to_add == 0 && removed_all_bones)
        {
            return ktrue;
        }
    }



    if (amount_to_add != 0)
    {
        // Update all base model pointers at start of file
        data += 16;
        base_model++;

        while (base_model < num_base_model)
        {
            DEREF( Uint8*, data ) += amount_to_add;  data += 4;
            DEREF( Uint8*, data ) += amount_to_add;  data += 4;
            DEREF( Uint8*, data ) += amount_to_add;  data += 4;
            DEREF( Uint8*, data ) += amount_to_add;  data += 4;
            DEREF( Uint8*, data ) += amount_to_add;  data += 4;
            base_model++;
        }


        // Update bone frame pointers...
        repeat(i, num_bone_frame)
        {
            DEREF( Uint8*, data ) += amount_to_add;  data += 4;
        }


        // Must recalculate vertex attachments and scalars...
        if (insert_bone)
        {
            num_bone--;
            render_crunch_bone(start_data, frame, num_bone, 0);
        }

        render_generate_bone_normals(start_data, frame);
        repeat(i, num_vertex)
        {
            render_attach_vertex_to_bone(start_data, frame, i);
            render_crunch_vertex(start_data, frame, i, ktrue, 0);
        }
        return removed_all_bones;
    }

    return kfalse;
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint8 render_insert_joint(Uint8* data, Uint16 frame, float* joint_xyz, Uint16 joint_to_remove, Uint8 joint_size)
{
    // <ZZ> This function adds a joint to a given model.  Returns ktrue if it worked.  If joint_xyz
    //      is NULL, the function removes a joint instead (and associated bones).
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_vertex, num_tex_vertex, num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* frame_data_start;
    Uint8* start_data;
    Uint8* bone_data;
    Uint8* joint_data;
    Uint16 i, j;
    Uint8 base_model, detail_level;
    Uint8 bone_delete_okay;


    // Remove bones attached to joint, if we're deleting a joint...
    log_info(0, "");
    log_info(0, "");
    log_info(0, "");
    start_data = data;

    if (joint_xyz == NULL)
    {
        // Do up here so our pointers don't get messed up later...
        bone_delete_okay = render_insert_bone(start_data, frame, joint_to_remove, 1024, kfalse, 0);

        if (bone_delete_okay)
        {
            log_info(0, "Insert bone said it was okay to delete a joint");
        }
    }



    flags = DEREF( Uint16, data ); data += 2;

    if ((flags & DDD_EXTERNAL_BONE_FRAMES)) return kfalse;

    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return kfalse;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);

    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!  Check filesize to make sure we have room...  Offset of last frame data to find...
    // !!!BAD!!!



    // Go to the current base model
    frame_data_start = data + (num_base_model * 20 * DETAIL_LEVEL_MAX);
    frame_data = DEREF( Uint8*, frame_data_start + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 4;
    joint_data =  DEREF( Uint8*, data + 8 );
    bone_data =  DEREF( Uint8*, data + 12 );

    num_vertex = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_tex_vertex = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;

    if (joint_xyz != NULL)
    {
        // Insert a joint...  Go through all frame data looking for matching base models to add joint position...
        repeat(i, num_bone_frame)
        {
            frame_data = DEREF( Uint8*, frame_data_start + (i << 2) );
            j = *(frame_data + 2);  // Base model for this frame...

            if (j == base_model)
            {
                // Need to update this frame...  Insert coordinates...
                frame_data += 11 + (num_bone * 24);
                frame_data += (num_joint * 12);

                if (sdf_insert_data(frame_data, NULL, 12))
                {
                    // Put in the coordinate info...
                    DEREF( float, frame_data ) = joint_xyz[XX];
                    DEREF( float, frame_data + 4 ) = joint_xyz[YY];
                    DEREF( float, frame_data + 8 ) = joint_xyz[ZZ];


                    // Update frame data pointers...
                    j = i + 1;

                    while (j < num_bone_frame)
                    {
                        DEREF( Uint8*, frame_data_start + (j << 2) ) += 12;
                        j++;
                    }
                }
            }
        }


        // Insert joint info to base model..
        if (sdf_insert_data(joint_data + (num_joint << 2), NULL, 4))
        {
            // Added the joint to base model data...  Write size...
            DEREF( float, joint_data + (num_joint << 2) ) = joint_size * JOINT_COLLISION_SCALE;


            // Update number of joints for this base model
            num_joint++;
            DEREF( Uint16, base_model_data - 4 ) = num_joint;


            // Update all base model pointers at start of file
            data += 12;
            DEREF( Uint8*, data ) += 4;  data += 4;
            base_model++;

            while (base_model < num_base_model)
            {
                DEREF( Uint8*, data ) += 4;  data += 4;
                DEREF( Uint8*, data ) += 4;  data += 4;
                DEREF( Uint8*, data ) += 4;  data += 4;
                DEREF( Uint8*, data ) += 4;  data += 4;
                DEREF( Uint8*, data ) += 4;  data += 4;
                base_model++;
            }


            // Update bone frame pointers...
            repeat(i, num_bone_frame)
            {
                DEREF( Uint8*, data ) += 4;  data += 4;
            }
            log_info(0, "Done with insert_joint");
            return ktrue;
        }

        // If it got here, we probably screwed everything up...
        log_error(0, "Model corrupted by insert_joint()...  Shoulda fixed that...");
    }
    else
    {
        // Delete a given joint
        if (joint_to_remove < num_joint)
        {
            // Remove any attached bones to start with...  Should also renumber higher joints...
            if (bone_delete_okay)
            {
                log_info(0, "Deleting joint %d", joint_to_remove);
                // Delete joint coordinates in every frame of animation
                repeat(i, num_bone_frame)
                {
                    frame_data = DEREF( Uint8*, frame_data_start + (i << 2) );
                    j = *(frame_data + 2);  // Base model for this frame...

                    if (j == base_model)
                    {
                        // Need to update this frame...  Insert coordinates...
                        frame_data += 11 + (num_bone * 24);
                        frame_data += (joint_to_remove * 12);

                        if (sdf_insert_data(frame_data, NULL, -12))
                        {
                            // Update frame data pointers...
                            j = i + 1;

                            while (j < num_bone_frame)
                            {
                                DEREF( Uint8*, frame_data_start + (j << 2) ) -= 12;
                                j++;
                            }
                        }
                    }
                }


                // Delete joint info from base model...
                if (sdf_insert_data(joint_data + (joint_to_remove << 2), NULL, -4))
                {
                    // Update number of joints for this base model
                    num_joint--;
                    DEREF( Uint16, base_model_data - 4 ) = num_joint;


                    // Update all base model pointers at start of file
                    data += 12;
                    DEREF( Uint8*, data ) -= 4;  data += 4;
                    base_model++;

                    while (base_model < num_base_model)
                    {
                        DEREF( Uint8*, data ) -= 4;  data += 4;
                        DEREF( Uint8*, data ) -= 4;  data += 4;
                        DEREF( Uint8*, data ) -= 4;  data += 4;
                        DEREF( Uint8*, data ) -= 4;  data += 4;
                        DEREF( Uint8*, data ) -= 4;  data += 4;
                        base_model++;
                    }


                    // Update bone frame pointers...
                    repeat(i, num_bone_frame)
                    {
                        DEREF( Uint8*, data ) -= 4;  data += 4;
                    }




                    // Must recalculate vertex attachments and scalars...
                    render_generate_bone_normals(start_data, frame);
                    repeat(i, num_vertex)
                    {
                        render_attach_vertex_to_bone(start_data, frame, i);
                        render_crunch_vertex(start_data, frame, i, ktrue, 0);
                    }
                    log_info(0, "Done with insert_joint");
                    return ktrue;
                }
            }
        }
    }

    log_info(0, "Done with insert_joint");
    return kfalse;
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void scale_all_joints_and_vertices(Uint8* data, float scale)
{
    // <ZZ> This function scales the joints and vertices...  Last minute sorta thing...
    float* position_xyz;
    Uint16 frame;
    Uint16 num_bone, num_joint, num_vertex;
    Uint8* data_start;
    Uint8* frame_data;
    Uint8* base_data;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint8 base_model, detail_level;
    float temp;
    Uint16 i;

    data_start = data;

    // Find our bone frames...
    data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;
    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);



    // Scale the bone lengths and vertex positions...
    repeat(base_model, num_base_model)
    {
        detail_level = 0;
        base_data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
        base_data = DEREF( Uint8*, base_data );
        num_vertex = DEREF( Uint16, base_data );  base_data += 6;
        num_bone = DEREF( Uint16, base_data );  base_data += 2;


        repeat(i, num_vertex)
        {
            position_xyz = (float*) base_data;
            position_xyz[XX] *= scale;
            position_xyz[YY] *= scale;
            position_xyz[ZZ] *= scale;
            base_data += 64;
        }



        base_data = data + (base_model * 20) + 16 + (num_base_model * 20 * detail_level);
        base_data = DEREF( Uint8*, base_data );  base_data += 5;
        repeat(i, num_bone)
        {
            temp = DEREF( float, base_data );
            DEREF( float, base_data ) = temp * scale;
            base_data += 9;
        }
    }



    // Scale the joint locations...
    frame = 0;
    repeat(frame, num_bone_frame)
    {
        frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );


        // Find number of bones and joints for bone frame...
        base_model = *(frame_data + 2);
        detail_level = 0;
        base_data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
        base_data = DEREF( Uint8*, base_data );  base_data += 4;
        num_joint = DEREF( Uint16, base_data );  base_data += 2;
        num_bone = DEREF( Uint16, base_data );


        // Skip to joint positions...
        // Scale all of the bones...
        frame_data += 11 + (num_bone * 24);


        // Now handle each joint's scaling...
        position_xyz = (float*) frame_data;
        repeat(i, num_joint)
        {
            position_xyz[XX] *= scale;
            position_xyz[YY] *= scale;
            position_xyz[ZZ] *= scale;
            position_xyz += 3;
        }
    }



    repeat(i, num_vertex)
    {
        render_attach_vertex_to_bone(data_start, 0, i);
        render_crunch_vertex(data_start, 0, i, ktrue, 0);
    }

}
#endif


//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void scale_selected_vertices(Uint8* data, Uint16 frame, Uint8 axis, float scale)
{
    // <ZZ> This function scales the selected points about a certain axis.
    Uint16 i;


    // Scale each of 'em...
    if (axis <= ZZ)
    {
        repeat(i, select_num)
        {
            select_xyz[i][axis] = select_xyz[i][axis] * scale;
            select_data[i][axis] = select_data[i][axis] * scale;
            // Recrunch the bone scalars too
            render_attach_vertex_to_bone(data, frame, select_list[i]);
            render_crunch_vertex(data, frame, select_list[i], ktrue, 0);
        }
    }
}
#endif


//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void scale_selected_vertices_centrid(Uint8* data, Uint16 frame, float scale)
{
    // <ZZ> This function scales the selected points towards another point...
    Uint16 i;
    float centrid_xyz[3], inverse;

    if (select_num < 1)
    {
        return;
    }

    // Find the centrid...
    centrid_xyz[XX] = 0.0f;
    centrid_xyz[YY] = 0.0f;
    centrid_xyz[ZZ] = 0.0f;
    repeat(i, select_num)
    {
        centrid_xyz[XX] += select_xyz[i][XX];
        centrid_xyz[YY] += select_xyz[i][YY];
        centrid_xyz[ZZ] += select_xyz[i][ZZ];
    }
    centrid_xyz[XX] /= select_num;
    centrid_xyz[YY] /= select_num;
    centrid_xyz[ZZ] /= select_num;
    //centrid_xyz[ZZ] = 0.125f;
    inverse = 1.0f - scale;
    centrid_xyz[XX] *= inverse;
    centrid_xyz[YY] *= inverse;
    centrid_xyz[ZZ] *= inverse;


    // Scale each of 'em...
    repeat(i, select_num)
    {
        select_xyz[i][XX] = (select_xyz[i][XX] * scale) + centrid_xyz[XX];
        select_data[i][XX] = (select_data[i][XX] * scale) + centrid_xyz[XX];
        select_xyz[i][YY] = (select_xyz[i][YY] * scale) + centrid_xyz[YY];
        select_data[i][YY] = (select_data[i][YY] * scale) + centrid_xyz[YY];
        select_xyz[i][ZZ] = (select_xyz[i][ZZ] * scale) + centrid_xyz[ZZ];
        select_data[i][ZZ] = (select_data[i][ZZ] * scale) + centrid_xyz[ZZ];
        // Recrunch the bone scalars too
        render_attach_vertex_to_bone(data, frame, select_list[i]);
        render_crunch_vertex(data, frame, select_list[i], ktrue, 0);
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void move_selected_vertices(Uint8* data, Uint16 frame, Uint8 axis, float offset)
{
    // <ZZ> This function moves the selected points on a certain axis.
    Uint16 i;


    // Move each of 'em...
    if (axis <= ZZ)
    {
        repeat(i, select_num)
        {
            select_xyz[i][axis] = select_xyz[i][axis] + offset;
            select_data[i][axis] = select_data[i][axis] + offset;
            // Recrunch the bone scalars too
            render_attach_vertex_to_bone(data, frame, select_list[i]);
            render_crunch_vertex(data, frame, select_list[i], ktrue, 0);
        }
    }
}
#endif


//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void set_selected_vertices(Uint8* data, Uint16 frame, Uint8 axis, float position)
{
    // <ZZ> This function sets the position of selected points on a certain axis.
    Uint16 i;


    // Move each of 'em...
    if (axis <= ZZ)
    {
        repeat(i, select_num)
        {
            select_xyz[i][axis] = position;
            select_data[i][axis] = position;
            // Recrunch the bone scalars too
            render_attach_vertex_to_bone(data, frame, select_list[i]);
            render_crunch_vertex(data, frame, select_list[i], ktrue, 0);
        }
    }
}
#endif


//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void break_anim_joints(Uint8* data, Uint16 frame)
{
    // <ZZ> A better version of the function below...  Gets velocity of each joint from the
    //      its position in the previous frame...
    float* position_xyz;
    float* last_position_xyz;
    float* next_position_xyz;
    float velocity_xyz[3];
    Uint16 last_frame, next_frame;
    Uint16 num_bone, num_joint;
    Uint8* data_start;
    Uint8* frame_data;
    Uint8* next_frame_data;
    Uint8* last_frame_data;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint8 base_model, detail_level;
    Uint16 i;
    float z;


    data_start = data;


    // Error check...
    if (frame < 2) { return; }

    last_frame = frame - 2;
    next_frame = frame - 1;


    // Find our bone frames...
    data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    last_frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (last_frame << 2) );
    next_frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (next_frame << 2) );


    // Find number of bones and joints for bone frame...
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    data = DEREF( Uint8*, data );  data += 4;
    num_joint = DEREF( Uint16, data );  data += 2;
    num_bone = DEREF( Uint16, data );


    // Error check...
    if (base_model != (*(last_frame_data + 2))) { return; }

    if (base_model != (*(next_frame_data + 2))) { return; }


    // Skip to joint positions...
    frame_data += 11 + (num_bone * 24);
    last_frame_data += 11 + (num_bone * 24);
    next_frame_data += 11 + (num_bone * 24);


    // Now handle each joint's movement offset...
    position_xyz = (float*) frame_data;
    last_position_xyz = (float*) last_frame_data;
    next_position_xyz = (float*) next_frame_data;

    // Floor level...
    z = 0.0f;
    //    if(frame > 5)
    //    {
    //        z = (float) (frame - 5);
    //        if(z > 10.0)
    //        {
    //            z = 0.75f;
    //        }
    //        else
    //        {
    //            z = z * 0.075f;
    //        }
    //    }
    repeat(i, num_joint)
    {
        velocity_xyz[XX] = next_position_xyz[XX] - last_position_xyz[XX];
        velocity_xyz[YY] = next_position_xyz[YY] - last_position_xyz[YY];
        velocity_xyz[ZZ] = next_position_xyz[ZZ] - last_position_xyz[ZZ];
        velocity_xyz[ZZ] -= 0.03f;
        position_xyz[XX] += velocity_xyz[XX];
        position_xyz[YY] += velocity_xyz[YY];
        position_xyz[ZZ] += velocity_xyz[ZZ];

        if (position_xyz[ZZ] < z)
        {
            position_xyz[XX] -= velocity_xyz[XX] * 0.25f;
            position_xyz[YY] -= velocity_xyz[YY] * 0.25f;

            position_xyz[ZZ] = position_xyz[ZZ] - z;
            position_xyz[ZZ] = position_xyz[ZZ] * -0.5f;
            position_xyz[ZZ] = position_xyz[ZZ] + z;
        }

        position_xyz += 3;
        last_position_xyz += 3;
        next_position_xyz += 3;
    }



    render_fix_model_to_bone_length(data_start, frame, 9999);
    render_generate_bone_normals(data_start, frame);
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
float break_anim_velocity_xyz[MAX_SELECT][3];
void break_anim_selected_vertices(Uint8* data, Uint16 frame)
{
    // <ZZ> This function helps me make the break animations for tiles...  Assumes 32 frame
    //      animation...
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint8* frame_data;
    Uint8* texture_data;
    Uint8 texture_mode;
    Uint16 num_primitive, num_primitive_vertex;
    Uint8 base_model, detail_level;
    float velocity_xyz[3];
    float offset_xyz[3];
    Uint16 v1, v2, v3;
    Uint8* data_start;
    float percent, inverse;

    Uint16 i, j, k, m;
    float distance;


    // Calculate velocity for each point...
    repeat(i, select_num)
    {
        distance = vector_length(select_xyz[i]);

        if (distance < 0.001f)  { distance = 0.001f; }

        distance *= 5.0f;
        break_anim_velocity_xyz[i][XX] = select_xyz[i][XX] / distance;
        break_anim_velocity_xyz[i][YY] = select_xyz[i][YY] / distance;
        break_anim_velocity_xyz[i][ZZ] = 2.0f * select_xyz[i][ZZ] / distance;
    }


    // Bleed velocities based on connectivity...
    data_start = data;
    repeat(m, 20)
    {
        data = data_start;
        data += 2;
        num_detail_level = *data;  data++;
        num_base_model = *data;  data++;
        num_bone_frame = DEREF( Uint16, data ); data += 2;

        if (frame >= num_bone_frame) return;

        data += (ACTION_MAX << 1);
        data += (MAX_DDD_SHADOW_TEXTURE);
        frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
        base_model = *(frame_data + 2);
        detail_level = 0;
        data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
        data += 8;
        texture_data = DEREF( Uint8*, data );  data += 4;
        repeat(i, MAX_DDD_TEXTURE)
        {
            texture_mode = *texture_data;  texture_data++;

            if (texture_mode != 0)
            {
                texture_data += 2;

                // Strips...
                num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                repeat(j, num_primitive)
                {
                    num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;

                    if (num_primitive_vertex == 3)
                    {
                        v1 = DEREF( Uint16, texture_data );
                        texture_data += 4;
                        v2 = DEREF( Uint16, texture_data );
                        texture_data += 4;
                        v3 = DEREF( Uint16, texture_data );
                        texture_data += 4;

                        if (select_inlist(v1))
                        {
                            v1 = select_index;

                            if (select_inlist(v2))
                            {
                                v2 = select_index;

                                if (select_inlist(v3))
                                {
                                    v3 = select_index;
                                    velocity_xyz[XX] = 0.33333f * (break_anim_velocity_xyz[v1][XX] + break_anim_velocity_xyz[v2][XX] + break_anim_velocity_xyz[v3][XX]);
                                    velocity_xyz[YY] = 0.33333f * (break_anim_velocity_xyz[v1][YY] + break_anim_velocity_xyz[v2][YY] + break_anim_velocity_xyz[v3][YY]);
                                    velocity_xyz[ZZ] = 0.33333f * (break_anim_velocity_xyz[v1][ZZ] + break_anim_velocity_xyz[v2][ZZ] + break_anim_velocity_xyz[v3][ZZ]);
                                    break_anim_velocity_xyz[v1][XX] = velocity_xyz[XX];  break_anim_velocity_xyz[v1][YY] = velocity_xyz[YY];  break_anim_velocity_xyz[v1][ZZ] = velocity_xyz[ZZ];
                                    break_anim_velocity_xyz[v2][XX] = velocity_xyz[XX];  break_anim_velocity_xyz[v2][YY] = velocity_xyz[YY];  break_anim_velocity_xyz[v2][ZZ] = velocity_xyz[ZZ];
                                    break_anim_velocity_xyz[v3][XX] = velocity_xyz[XX];  break_anim_velocity_xyz[v3][YY] = velocity_xyz[YY];  break_anim_velocity_xyz[v3][ZZ] = velocity_xyz[ZZ];
                                }
                            }
                        }
                    }
                    else
                    {
                        // Shouldn't happen
                        repeat(k, num_primitive_vertex)
                        {
                            texture_data += 4;
                        }
                    }
                }
            }
        }
    }



    // Shrink positions towards end...
    if (frame > 23)
    {
        repeat(m, ((frame - 23)*8))
        {
            percent = 0.01f;
            inverse = 1.0f - percent;
            data = data_start;
            data += 2;
            num_detail_level = *data;  data++;
            num_base_model = *data;  data++;
            num_bone_frame = DEREF( Uint16, data ); data += 2;

            if (frame >= num_bone_frame) return;

            data += (ACTION_MAX << 1);
            data += (MAX_DDD_SHADOW_TEXTURE);
            frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
            base_model = *(frame_data + 2);
            detail_level = 0;
            data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
            data += 8;
            texture_data = DEREF( Uint8*, data );  data += 4;
            repeat(i, MAX_DDD_TEXTURE)
            {
                texture_mode = *texture_data;  texture_data++;

                if (texture_mode != 0)
                {
                    texture_data += 2;

                    // Strips...
                    num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
                    repeat(j, num_primitive)
                    {
                        num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;

                        if (num_primitive_vertex == 3)
                        {
                            v1 = DEREF( Uint16, texture_data );
                            texture_data += 4;
                            v2 = DEREF( Uint16, texture_data );
                            texture_data += 4;
                            v3 = DEREF( Uint16, texture_data );
                            texture_data += 4;

                            if (select_inlist(v1))
                            {
                                v1 = select_index;

                                if (select_inlist(v2))
                                {
                                    v2 = select_index;

                                    if (select_inlist(v3))
                                    {
                                        v3 = select_index;
                                        velocity_xyz[XX] = 0.33333f * (select_xyz[v1][XX] + select_xyz[v2][XX] + select_xyz[v3][XX]);
                                        velocity_xyz[YY] = 0.33333f * (select_xyz[v1][YY] + select_xyz[v2][YY] + select_xyz[v3][YY]);
                                        velocity_xyz[ZZ] = 0.33333f * (select_xyz[v1][ZZ] + select_xyz[v2][ZZ] + select_xyz[v3][ZZ]);

                                        offset_xyz[XX] = ((inverse * select_xyz[v1][XX]) + (percent * velocity_xyz[XX])) - select_xyz[v1][XX];  offset_xyz[YY] = ((inverse * select_xyz[v1][YY]) + (percent * velocity_xyz[YY])) - select_xyz[v1][YY];  offset_xyz[ZZ] = ((inverse * select_xyz[v1][ZZ]) + (percent * velocity_xyz[ZZ])) - select_xyz[v1][ZZ];
                                        select_xyz[v1][XX] += offset_xyz[XX];  select_xyz[v1][YY] += offset_xyz[YY];  select_xyz[v1][ZZ] += offset_xyz[ZZ];
                                        select_data[v1][XX] += offset_xyz[XX];  select_data[v1][YY] += offset_xyz[YY];  select_data[v1][ZZ] += offset_xyz[ZZ];

                                        offset_xyz[XX] = ((inverse * select_xyz[v2][XX]) + (percent * velocity_xyz[XX])) - select_xyz[v2][XX];  offset_xyz[YY] = ((inverse * select_xyz[v2][YY]) + (percent * velocity_xyz[YY])) - select_xyz[v2][YY];  offset_xyz[ZZ] = ((inverse * select_xyz[v2][ZZ]) + (percent * velocity_xyz[ZZ])) - select_xyz[v2][ZZ];
                                        select_xyz[v2][XX] += offset_xyz[XX];  select_xyz[v2][YY] += offset_xyz[YY];  select_xyz[v2][ZZ] += offset_xyz[ZZ];
                                        select_data[v2][XX] += offset_xyz[XX];  select_data[v2][YY] += offset_xyz[YY];  select_data[v2][ZZ] += offset_xyz[ZZ];

                                        offset_xyz[XX] = ((inverse * select_xyz[v3][XX]) + (percent * velocity_xyz[XX])) - select_xyz[v3][XX];  offset_xyz[YY] = ((inverse * select_xyz[v3][YY]) + (percent * velocity_xyz[YY])) - select_xyz[v3][YY];  offset_xyz[ZZ] = ((inverse * select_xyz[v3][ZZ]) + (percent * velocity_xyz[ZZ])) - select_xyz[v3][ZZ];
                                        select_xyz[v3][XX] += offset_xyz[XX];  select_xyz[v3][YY] += offset_xyz[YY];  select_xyz[v3][ZZ] += offset_xyz[ZZ];
                                        select_data[v3][XX] += offset_xyz[XX];  select_data[v3][YY] += offset_xyz[YY];  select_data[v3][ZZ] += offset_xyz[ZZ];
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Shouldn't happen
                            repeat(k, num_primitive_vertex)
                            {
                                texture_data += 4;
                            }
                        }
                    }
                }
            }
        }
    }


    // Move each of 'em...
    repeat(i, select_num)
    {
        repeat(j, frame)
        {
            select_xyz[i][XX] += break_anim_velocity_xyz[i][XX];
            select_data[i][XX] += break_anim_velocity_xyz[i][XX];
            select_xyz[i][YY] += break_anim_velocity_xyz[i][YY];
            select_data[i][YY] += break_anim_velocity_xyz[i][YY];
            select_xyz[i][ZZ] += break_anim_velocity_xyz[i][ZZ];
            select_data[i][ZZ] += break_anim_velocity_xyz[i][ZZ];

            if (select_xyz[i][ZZ] < 0.0f)
            {
                select_xyz[i][ZZ] = 0.0f;
                select_data[i][ZZ] = 0.0f;
                break_anim_velocity_xyz[i][ZZ] = -0.5f * break_anim_velocity_xyz[i][ZZ];
                break_anim_velocity_xyz[i][XX] *= 0.75f;
                break_anim_velocity_xyz[i][YY] *= 0.75f;
            }


            // Change velocity...
            break_anim_velocity_xyz[i][XX] *= 0.95f;
            break_anim_velocity_xyz[i][YY] *= 0.95f;
            //            break_anim_velocity_xyz[i][ZZ] -= 0.05f;
            break_anim_velocity_xyz[i][ZZ] -= 0.03f;


        }



        // Recrunch the bone scalars too
        render_attach_vertex_to_bone(data_start, frame, select_list[i]);
        render_crunch_vertex(data_start, frame, select_list[i], ktrue, 0);
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void tree_rotate_selected_vertices(Uint8* data, Uint16 frame, float angle)
{
    // <ZZ> This function rotates the selected vertices by any angle, around the y axis
    //      (forward).  For doing the tree fall animation...
    Uint16 i;
    float x, z;
    float sine, cosine;

    sine = SIN(angle * DEG_TO_RAD);
    cosine = COS(angle * DEG_TO_RAD);
    repeat(i, select_num)
    {
        x = select_xyz[i][XX];
        z = select_xyz[i][ZZ];
        select_xyz[i][XX] = (cosine * x) + (sine * z);
        select_xyz[i][ZZ] = (cosine * z) - (sine * x);
        select_data[i][XX] = select_xyz[i][XX];
        select_data[i][ZZ] = select_xyz[i][ZZ];
        // Recrunch the bone scalars too
        render_attach_vertex_to_bone(data, frame, select_list[i]);
        render_crunch_vertex(data, frame, select_list[i], ktrue, 0);
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void rotate_selected_vertices(Uint8* data, Uint16 frame)
{
    // <ZZ> This function rotates the selected vertices 90' clockwise, around the
    //      z axis (up).
    Uint16 i;
    float x, y;


    repeat(i, select_num)
    {
        x = select_xyz[i][XX];
        y = select_xyz[i][YY];
        select_xyz[i][XX] = -y;
        select_xyz[i][YY] = x;
        select_data[i][XX] = -y;
        select_data[i][YY] = x;
        // Recrunch the bone scalars too
        render_attach_vertex_to_bone(data, frame, select_list[i]);
        render_crunch_vertex(data, frame, select_list[i], ktrue, 0);
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void flip_selected_vertices(Uint8* data, Uint16 frame, Uint8 axis)
{
    // <ZZ> This function flips the selected points about a certain axis.  Also flips the
    //      normals of selected triangles...
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint8* frame_data;
    Uint8* texture_data;
    Uint16 i, j, k, m;
    Uint8 texture_mode;
    Uint16 num_primitive, num_primitive_vertex;
    int amount_added;
    Uint8 base_model, detail_level;


    // Flip each of 'em...
    if (axis <= ZZ)
    {
        repeat(i, select_num)
        {
            select_xyz[i][axis] = -select_xyz[i][axis];
            select_data[i][axis] = -select_data[i][axis];
            // Recrunch the bone scalars too
            render_attach_vertex_to_bone(data, frame, select_list[i]);
            render_crunch_vertex(data, frame, select_list[i], ktrue, 0);
        }
    }


    // Run through each triangle, flipping normals if all three points are selected...
    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    if (select_num < 3) return;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    texture_data = DEREF( Uint8*, data + 8 );


    // Go through each triangle...
    repeat(m, MAX_DDD_TEXTURE)
    {
        texture_mode = *texture_data;  texture_data++;

        if (texture_mode != 0)
        {
            // Texture is turned on...
            texture_data += 2;


            // Strips...
            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
            repeat(i, num_primitive)
            {
                amount_added = 0;
                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                repeat(j, num_primitive_vertex)
                {
                    if (select_inlist(DEREF( Uint16, texture_data )))
                    {
                        amount_added++;
                    }

                    texture_data += 4;
                }

                if (amount_added == num_primitive_vertex)
                {
                    // Found a match...  Flip it...
                    j = DEREF( Uint16, texture_data - 4 );
                    k = DEREF( Uint16, texture_data - 2 );
                    DEREF( Uint16, texture_data - 4 ) = DEREF( Uint16, texture_data - 8 );
                    DEREF( Uint16, texture_data - 2 ) = DEREF( Uint16, texture_data - 6 );
                    DEREF( Uint16, texture_data - 8 ) = j;
                    DEREF( Uint16, texture_data - 6 ) = k;
                }
            }

            // Skip fans...
            texture_data += 2;
        }
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint8* get_start_of_triangles(Uint8* data, Uint16 frame, Uint8 texture)
{
    // <ZZ> This function returns a pointer to the start of a set of triangles within an RDY
    //      file.
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint8* frame_data;
    Uint8* texture_data;
    Uint16 i, j, m;
    Uint8 texture_mode;
    Uint16 num_primitive, num_primitive_vertex;
    Uint8 base_model, detail_level;
    Uint16 num_bone, num_joint;


    // Run through each triangle until we get to the desired texture...
    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return NULL;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);

    texture_data = DEREF( Uint8*, data );
    num_joint = DEREF( Uint16, texture_data + 4 );
    num_bone = DEREF( Uint16, texture_data + 6 );

    texture_data = DEREF( Uint8*, data + 8 );


    // Go through each triangle...
    repeat(m, MAX_DDD_TEXTURE)
    {
        if (m == texture)  return texture_data;

        texture_mode = *texture_data;  texture_data++;

        if (texture_mode != 0)
        {
            // Texture is turned on...
            texture_data += 2;


            // Strips...
            num_primitive = DEREF( Uint16, texture_data );  texture_data += 2;
            repeat(i, num_primitive)
            {
                num_primitive_vertex = DEREF( Uint16, texture_data );  texture_data += 2;
                repeat(j, num_primitive_vertex)
                {
                    texture_data += 4;
                }
            }

            // Skip fans...
            texture_data += 2;
        }
    }

    // Requested texture was more than we're supposed to have...  Means we're requesting cartoon line data...


    // Skip joints...
    texture_data += 4 * num_joint;

    // Skip bones...
    texture_data += 9 * num_bone;

    // Should be at cartoon lines...
    return texture_data;
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void weld_selected_vertices(Uint8* data, Uint16 frame, Uint8 glue_instead)
{
    // <ZZ> This function finds the best pairs of vertices within the selected group, and turns
    //      each pair into a single vertex.  Renumbers triangle associations to match.
    //      If glue_instead is set, it doesn't delete the extra vertices...
    Uint16 i, j;
    float distance_xyz[3];
    float distance;
    float best_distance;
    Uint16 best_partner;


    // Need an even number of 'em
    if (select_num & 1)
    {
        if ((keyb.down[SDLK_LSHIFT] || keyb.down[SDLK_RSHIFT]) && select_num > 2)
        {
            select_num--;
        }
        else
        {
            message_add("ERROR:  Need an even number to weld (shift to force)", NULL, kfalse);
            return;
        }
    }


    // Flag each of our selected vertices as having no partner...
    repeat(i, select_num)
    {
        select_flag[i] = MAX_VERTEX;
    }


    // Find a partner for each vertex
    repeat(i, select_num)
    {
        // Does it have a partner yet?
        if (select_flag[i] == MAX_VERTEX)
        {
            // Nope, so find one...
            best_partner = MAX_VERTEX;
            best_distance = 9999.9f;
            j = i + 1;

            while (j < select_num)
            {
                // Can we pair it with this one?
                if (select_flag[j] == MAX_VERTEX)
                {
                    // Yup, so check how close they are...
                    distance_xyz[XX] = select_xyz[j][XX] - select_xyz[i][XX];
                    distance_xyz[YY] = select_xyz[j][YY] - select_xyz[i][YY];
                    distance_xyz[ZZ] = select_xyz[j][ZZ] - select_xyz[i][ZZ];
                    distance = vector_length(distance_xyz);

                    if (distance < best_distance)
                    {
                        best_partner = j;
                        best_distance = distance;
                    }
                }

                j++;
            }

            // Shoulda found a partner, so exit if it didn't
            if (best_partner == MAX_VERTEX)
            {
                log_error(0, "Partnering error...  Shouldn't happen...");
                return;
            }

            // Remember our partners...
            select_flag[i] = best_partner;
            select_flag[best_partner] = i;
            // Find the center of the points, and move both to that location
            select_data[i][XX] = (select_xyz[i][XX] + select_xyz[best_partner][XX]) * 0.5f;
            select_data[i][YY] = (select_xyz[i][YY] + select_xyz[best_partner][YY]) * 0.5f;
            select_data[i][ZZ] = (select_xyz[i][ZZ] + select_xyz[best_partner][ZZ]) * 0.5f;
            select_data[best_partner][XX] = select_data[i][XX];
            select_data[best_partner][YY] = select_data[i][YY];
            select_data[best_partner][ZZ] = select_data[i][ZZ];
        }
    }
    select_update_xyz();


    if (glue_instead)
    {
        // Recrunch all selected vertices, so they're in the right positions....
        repeat(i, 5000)
        {
            if (select_inlist(i))
            {
                render_attach_vertex_to_bone(data, frame, i);
                render_crunch_vertex(data, frame, i, ktrue, 0);
            }
        }
    }
    else
    {
        // Delete the higher vertex of each pair...  Do in weird order so it doesn't mess up...
        // Do with hacked in replacement thing...
        i = 5000;

        while (i > 0)
        {
            i--;

            if (select_inlist(i))
            {
                if (select_flag[select_index] != MAX_VERTEX)
                {
                    // Delete the vertex, and replace any reference with one to its partner...
                    // Flag its partner to not be deleted...
                    best_partner = select_flag[select_index];
                    render_insert_vertex(data, frame, NULL, i, select_list[best_partner]);
                    select_flag[best_partner] = MAX_VERTEX;

                    // Recrunch the bone scalars for the undeleted partner...
                    render_attach_vertex_to_bone(data, frame, select_list[best_partner]);
                    render_crunch_vertex(data, frame, select_list[best_partner], ktrue, 0);
                }
            }
        }
    }

    select_clear();
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void weld_selected_tex_vertices(Uint8* data, Uint16 frame)
{
    // <ZZ> This function finds the best pairs of tex vertices within the selected group, and turns
    //      each pair into a single tex vertex.  Renumbers triangle associations to match.
    Uint16 i, j;
    float distance_xyz[3];
    float distance;
    float best_distance;
    Uint16 best_partner;


    // Need an even number of 'em
    if (select_num & 1)
    {
        if ((keyb.down[SDLK_LSHIFT] || keyb.down[SDLK_RSHIFT]) && select_num > 2)
        {
            select_num--;
        }
        else
        {
            message_add("ERROR:  Need an even number to weld (shift to force)", NULL, kfalse);
            return;
        }
    }


    // Flag each of our selected vertices as having no partner...
    repeat(i, select_num)
    {
        select_flag[i] = MAX_TEX_VERTEX;
    }


    // Find a partner for each vertex
    repeat(i, select_num)
    {
        // Does it have a partner yet?
        if (select_flag[i] == MAX_TEX_VERTEX)
        {
            // Nope, so find one...
            best_partner = MAX_TEX_VERTEX;
            best_distance = 9999.9f;
            j = i + 1;

            while (j < select_num)
            {
                // Can we pair it with this one?
                if (select_flag[j] == MAX_TEX_VERTEX)
                {
                    // Yup, so check how close they are...
                    distance_xyz[XX] = select_xyz[j][XX] - select_xyz[i][XX];
                    distance_xyz[YY] = select_xyz[j][YY] - select_xyz[i][YY];
                    distance_xyz[ZZ] = 0;
                    distance = vector_length(distance_xyz);

                    if (distance < best_distance)
                    {
                        best_partner = j;
                        best_distance = distance;
                    }
                }

                j++;
            }

            // Shoulda found a partner, so exit if it didn't
            if (best_partner == MAX_TEX_VERTEX)
            {
                log_error(0, "Partnering error...  Shouldn't happen...");
                return;
            }

            // Remember our partners...
            select_flag[i] = best_partner;
            select_flag[best_partner] = i;
            // Find the center of the points, and move both to that location
            select_data[i][XX] = (select_xyz[i][XX] + select_xyz[best_partner][XX]) * 0.5f;
            select_data[i][YY] = (select_xyz[i][YY] + select_xyz[best_partner][YY]) * 0.5f;
            select_data[best_partner][XX] = select_data[i][XX];
            select_data[best_partner][YY] = select_data[i][YY];
        }
    }
    select_update_xy();


    // Delete the higher vertex of each pair...  Do in weird order so it doesn't mess up...
    // Do with hacked in replacement thing...
    i = 5000;

    while (i > 0)
    {
        i--;

        if (select_inlist(i))
        {
            if (select_flag[select_index] != MAX_TEX_VERTEX)
            {
                // Delete the vertex, and replace any reference with one to its partner...
                // Flag its partner to not be deleted...
                best_partner = select_flag[select_index];
                render_insert_tex_vertex(data, frame, NULL, i, select_list[best_partner]);
                select_flag[best_partner] = MAX_TEX_VERTEX;
            }
        }
    }

    select_clear();
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void scale_selected_tex_vertices(Uint8* data, Uint16 frame, Uint8 axis, float scale)
{
    // <ZZ> This function shrinks the selected texture vertices...
    float add;
    Uint16 i;
    int temp;

    if (axis < 2)
    {
        // Scaling the x or y axis...
        add = (1.0f - scale) * 0.5f;
        repeat(i, select_num)
        {
            select_data[i][axis] = (select_data[i][axis] * scale) + add;
        }
    }
    else
    {
        // Really doing eye placement thing...  Lock vertices to nearest position...
        repeat(i, select_num)
        {
            temp = (int) ((select_data[i][XX] * 2.0f) + 0.5f);  select_data[i][XX] = (temp * 0.5f);
            temp = (int) ((select_data[i][YY] * 2.0f) + 0.5f);  select_data[i][YY] = (temp * 0.5f);
        }
    }

    select_update_xy();
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_bounding_box()
{
    // <ZZ> This function draws a bounding box test thing for the modeler
    display_color(white);


    // Small box...
    display_start_line_loop();
    {
        display_vertex_xyz(-1, -1, 0);
        display_vertex_xyz(1, -1, 0);
        display_vertex_xyz(1, 1, 0);
        display_vertex_xyz(-1, 1, 0);
    }
    display_end();


    // Medium box...
    display_start_line_loop();
    {
        display_vertex_xyz(-2, -2, 0);
        display_vertex_xyz(2, -2, 0);
        display_vertex_xyz(2, 2, 0);
        display_vertex_xyz(-2, 2, 0);
    }
    display_end();


    // Large box...
    display_start_line_loop();
    {
        display_vertex_xyz(-3, -3, 0);
        display_vertex_xyz(3, -3, 0);
        display_vertex_xyz(3, 3, 0);
        display_vertex_xyz(-3, 3, 0);
    }
    display_end();
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_axis()
{
    // <ZZ> This function draws 3D axis lines
    display_start_line();
    {
        display_color(red);
        display_vertex_xyz(0, 0, 0);
        display_vertex_xyz(1, 0, 0);
    }
    display_end();

    display_start_line();
    {
        display_color(green);
        display_vertex_xyz(0, 0, 0);
        display_vertex_xyz(0, 1, 0);
    }
    display_end();

    display_start_line();
    {
        display_color(blue);
        display_vertex_xyz(0, 0, 0);
        display_vertex_xyz(0, 0, 1);
    }
    display_end();
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint16 render_change_frame_base_model(Uint8* data, Uint16 frame, Uint16 new_base_model)
{
    // <ZZ> This function changes the base model for a given frame, and replaces bone/joint locations
    //      with values from the Boning Model frame.  If new_base_model is UINT16_MAX, the function returns
    //      the current base_model for the given frame.  Otherwise, it returns ktrue if the frame
    //      changed okay, or kfalse if there was a problem.
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint8** frame_data_start;
    Uint8** base_model_data_start;
    Uint8* base_model_data;
    Uint8* start_data;
    Uint8* frame_data;
    Uint8* new_frame_data;
    Uint16 i, j;
    Uint16 num_bone;
    Uint16 num_joint;
    Uint8 base_model;
    Sint32 size;
    Uint16 copy_from_frame;
    Sint32 copy_from_size;
    Uint8 alpha;


    // Go to the current base model, and determine its size
    start_data = data;
    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;
    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);

    base_model_data_start = (Uint8**) data;
    frame_data_start =  (Uint8**) (data + (num_base_model * 20 * DETAIL_LEVEL_MAX));

    if (new_base_model == UINT16_MAX)
    {
        // Return the base model for the specified frame...
        frame_data = frame_data_start[frame];
        return frame_data[2];
    }

    if (new_base_model >= num_base_model) return kfalse;


    // Need to change the base model for the given frame...
    // Start by finding the first frame that uses the desired base model...  Quit if there isn't one...
    j = num_bone_frame;
    repeat(i, num_bone_frame)
    {
        frame_data = frame_data_start[i];

        if (frame_data[2] == new_base_model)
        {
            j = i;
            i = num_bone_frame;
        }
    }

    if (j == num_bone_frame)  return kfalse; // Not found...

    copy_from_frame = j;


    // Determine the size of the current frame...
    frame_data = frame_data_start[frame];
    base_model = frame_data[2];
    base_model_data = base_model_data_start[base_model*5];
    num_joint = DEREF( Uint16, base_model_data + 4 );
    num_bone = DEREF( Uint16, base_model_data + 6 );
    size = 11 + (24 * num_bone) + (12 * num_joint);
    frame_data += size;
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        alpha = *frame_data;  frame_data++;  size++;

        if (alpha)
        {
            size += 32;  frame_data += 32;
        }
    }
    frame_data -= size;
    new_frame_data = frame_data;


    // Determine the size of the desired base model frame...
    frame_data = frame_data_start[copy_from_frame];
    base_model = frame_data[2];
    base_model_data = base_model_data_start[base_model*5];
    num_joint = DEREF( Uint16, base_model_data + 4 );
    num_bone = DEREF( Uint16, base_model_data + 6 );
    copy_from_size = 11 + (24 * num_bone) + (12 * num_joint);
    frame_data += copy_from_size;
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        alpha = *frame_data;  frame_data++;  copy_from_size++;

        if (alpha)
        {
            copy_from_size += 32;  frame_data += 32;
        }
    }
    frame_data -= size;


    // Delete the current frame, and move the desired one into its spot...
    memcpy(mainbuffer, frame_data, copy_from_size);

    if (sdf_insert_data(new_frame_data, NULL, -size))
    {
        sdf_insert_data(new_frame_data, mainbuffer, copy_from_size);
        // Change the pointers to all of the later frames to be correct...
        size = copy_from_size - size;  // Amount to push 'em back...
        i = frame + 1;

        while (i < num_bone_frame)
        {
            frame_data_start[i] += size;
            i++;
        }

        return ktrue;
    }

    return kfalse;
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint8 render_insert_base_model(Uint8* data, Uint16 frame, Uint8 insert_mode)
{
    // <ZZ> This function adds a new base model to a given file.  New one is a copy of the given
    //      one.  If insert mode is kfalse, the given base_model is deleted instead (frame is really
    //      base model in this case...)
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint8** frame_data_start;
    Uint8** base_model_data_start;
    Uint8* base_model_data;
    Uint8* bone_data;
    Uint8* start_data;
    Uint8* frame_data;
    Uint16 i;
    Uint8 detail_level;
    Uint16 num_bone;
    Uint16 num_line;
    Sint32 size;
    Uint16 base_model;

    // Go to the current base model, and determine its size
    start_data = data;
    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (insert_mode)
    {
        if (frame >= num_bone_frame) return kfalse;
    }

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);

    base_model_data_start = (Uint8**) data;
    frame_data_start =  (Uint8**) (data + (num_base_model * 20 * DETAIL_LEVEL_MAX));

    if (insert_mode)
    {
        frame_data = frame_data_start[frame];
        base_model = frame_data[2];
    }
    else
    {
        base_model = frame;
    }

    if (base_model >= num_base_model) return kfalse;

    detail_level = 0;
    base_model_data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    data = base_model_data;
    bone_data = DEREF( Uint8*, base_model_data + 16 );
    base_model_data = DEREF( Uint8*, base_model_data );
    num_bone = DEREF( Uint16, base_model_data + 6 );
    size = ((Uint32) bone_data) - ((Uint32) base_model_data);
    size += num_bone * 9;
    num_line = DEREF( Uint16, base_model_data + size );
    size += (num_line * 8) + 2;

    // Which mode are we in?
    if (insert_mode)
    {
        // Try to copy the data...
        if (num_base_model == 255) return kfalse;

        if (sdf_insert_data(base_model_data + size, base_model_data, size))
        {
            // Increase the number of base models...
            num_base_model++;
            *(start_data + 3) = num_base_model;

            // Now insert a new pointer block in the base model stuff...
            sdf_insert_data(data + 20, data, 20);

            // Okay, now update all of the base model pointers
            repeat(i, num_base_model)
            {
                if (i > base_model)
                {
                    base_model_data_start[0] += size + 20;
                    base_model_data_start[1] += size + 20;
                    base_model_data_start[2] += size + 20;
                    base_model_data_start[3] += size + 20;
                    base_model_data_start[4] += size + 20;
                }
                else
                {
                    base_model_data_start[0] += 20;
                    base_model_data_start[1] += 20;
                    base_model_data_start[2] += 20;
                    base_model_data_start[3] += 20;
                    base_model_data_start[4] += 20;
                }

                base_model_data_start += 5;
            }
            frame_data_start += 5;


            // Now update all of the frame pointers, update base model references while we're at it...
            repeat(i, num_bone_frame)
            {
                frame_data_start[i] += size + 20;
                frame_data = frame_data_start[i];

                if (frame_data[2] > base_model)
                {
                    frame_data[2]++;
                }
            }


            // Now change the base model of the current frame to be the new base model...
            frame_data = frame_data_start[frame];
            frame_data[2]++;
            return ktrue;
        }
    }
    else
    {
        // Make sure the base model isn't being used
        repeat(i, num_bone_frame)
        {
            frame_data = frame_data_start[i];

            if (frame_data[2] == base_model)
            {
                return kfalse;
            }
        }

        // Try to delete the data...
        if (num_base_model == 1) return kfalse;

        if (sdf_insert_data(base_model_data, NULL, -size))
        {
            // Decrease the number of base models...
            num_base_model--;
            *(start_data + 3) = num_base_model;

            // Now get rid of the pointer block in the base model stuff...
            sdf_insert_data(data, NULL, -20);

            // Okay, now update all of the base model pointers
            repeat(i, num_base_model)
            {
                if (i >= base_model)
                {
                    base_model_data_start[0] -= size + 20;
                    base_model_data_start[1] -= size + 20;
                    base_model_data_start[2] -= size + 20;
                    base_model_data_start[3] -= size + 20;
                    base_model_data_start[4] -= size + 20;
                }
                else
                {
                    base_model_data_start[0] -= 20;
                    base_model_data_start[1] -= 20;
                    base_model_data_start[2] -= 20;
                    base_model_data_start[3] -= 20;
                    base_model_data_start[4] -= 20;
                }

                base_model_data_start += 5;
            }
            frame_data_start -= 5;


            // Now update all of the frame pointers, update base model references while we're at it...
            repeat(i, num_bone_frame)
            {
                frame_data_start[i] -= size + 20;
                frame_data = frame_data_start[i];

                if (frame_data[2] > base_model)
                {
                    frame_data[2]--;
                }
            }
            return ktrue;
        }
    }

    return kfalse;
}
#endif

//-----------------------------------------------------------------------------------------------
Uint8* get_start_of_frame(Uint8* data, Uint16 frame)
{
    // <ZZ> This function returns a pointer to the start of a given frame's data.
    //      Returns NULL if external frames or if there's some other problem...
    Uint16 flags;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint8* frame_data;


    flags = DEREF( Uint16, data ); data += 3;

    if ((flags & DDD_EXTERNAL_BONE_FRAMES)) return NULL;

    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return NULL;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data = data + (num_base_model * 20 * DETAIL_LEVEL_MAX);
    frame_data = DEREF( Uint8*, frame_data + (frame << 2) );
    return frame_data;
}

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint8* get_frame_pointer(Uint8* data, Uint16 frame)
{
    // <ZZ> This function returns a pointer to the frame's pointer...  Blarg...
    Uint16 flags;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint8* frame_data;


    flags = DEREF( Uint16, data ); data += 3;

    if ((flags & DDD_EXTERNAL_BONE_FRAMES)) return NULL;

    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return NULL;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data = data + (num_base_model * 20 * DETAIL_LEVEL_MAX);
    frame_data = frame_data + (frame << 2);
    return frame_data;
}
#endif

//-----------------------------------------------------------------------------------------------
Uint16 get_number_of_bones(Uint8* data)
{
    // <ZZ> This function returns the number of bones in base model 0 of the given RDY file...
    data += 6;
    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to base model 0
    data = (DEREF( Uint8*, data )) + 6;
    return (DEREF( Uint16, data ));
}

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint8* get_start_of_frame_shadows(Uint8* data, Uint16 frame)
{
    // <ZZ> This function returns a pointer to the start of a frame's shadow data...
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint8* frame_data;
    Uint16 num_bone, num_joint;
    Uint8 base_model;


    data += 3;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return NULL;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data = data + (num_base_model * 20 * DETAIL_LEVEL_MAX);
    frame_data = DEREF( Uint8*, frame_data + (frame << 2) );
    base_model = frame_data[2];
    data = data + (base_model * 20);
    data = DEREF( Uint8*, data );  data += 4;
    num_joint = DEREF( Uint16, data ); data += 2;
    num_bone = DEREF( Uint16, data );
    frame_data += 11 + (24 * num_bone) + (12 * num_joint);
    return frame_data;
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint8 render_insert_frame(Uint8* data, Uint16 frame, Uint8 insert_mode)
{
    // <ZZ> This function inserts a copy of the given frame directly after the original.  Deletes
    //      the given frame if insert_mode is kfalse.  Returns ktrue if it worked, kfalse if not.
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* frame_data;
    Uint8* frame_data_start;
    Uint8* new_frame_data;
    Uint8* start_data;
    Uint8* base_model_data_start;
    Uint16 i, j;
    Uint16 size;
    Uint8 alpha;
    Uint8 base_model, detail_level;

    start_data = data;
    flags = DEREF( Uint16, data ); data += 2;

    if ((flags & DDD_EXTERNAL_BONE_FRAMES)) return kfalse;

    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return kfalse;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);
    base_model_data_start = data;


    // Go to the current base model
    frame_data_start = data + (num_base_model * 20 * DETAIL_LEVEL_MAX);
    frame_data = DEREF( Uint8*, frame_data_start + (frame << 2) );
    base_model = *(frame_data + 2);
    detail_level = 0;
    data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );  data += 4;
    base_model_data += 4;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;


    // Determine data size...   Need to count shadows...
    size = 11 + (24 * num_bone) + (12 * num_joint);
    frame_data += size;
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        alpha = *frame_data;  frame_data++;  size++;

        if (alpha)
        {
            size += 32;  frame_data += 32;
        }
    }


    if (insert_mode)
    {
        log_info(0, "Frame insert, %d to %d", frame, frame + 1);
        log_info(0, "Frame %d is %d bytes", frame, size);


        // Insert a frame...  Copy current one...
        if (sdf_insert_data(frame_data, frame_data - size, size))
        {
            log_info(0, "Inserted data");
            // Update number of frames for this base model
            new_frame_data = frame_data;
            num_bone_frame++;
            DEREF( Uint16, start_data + 4 ) = num_bone_frame;
            log_info(0, "Num_bone_frame updated");


            // Update bone frame pointers...
            frame++;
            frame_data = frame_data_start + (frame << 2);

            if (sdf_insert_data(frame_data, NULL, 4))
            {
                log_info(0, "Added bone frame pointer for frame %d", frame);
                DEREF( Uint8*, frame_data ) = new_frame_data;
                frame_data += 4;
                frame++;

                while (frame < num_bone_frame)
                {
                    log_info(0, "Pushed back pointer for frame %d by size", frame);
                    DEREF( Uint8*, frame_data ) += size;  frame_data += 4;
                    frame++;
                }



                // Update every every base model pointer to account for
                // scoochin' 'em back with last insert...
                base_model_data = base_model_data_start;
                repeat(i, num_detail_level)
                {
                    repeat(j, num_base_model)
                    {
                        log_info(0, "Pushed base %d, Detail %d, back by 4", j, i);
                        DEREF( Uint8*, base_model_data ) += 4;  base_model_data += 4;
                        DEREF( Uint8*, base_model_data ) += 4;  base_model_data += 4;
                        DEREF( Uint8*, base_model_data ) += 4;  base_model_data += 4;
                        DEREF( Uint8*, base_model_data ) += 4;  base_model_data += 4;
                        DEREF( Uint8*, base_model_data ) += 4;  base_model_data += 4;
                    }
                }


                // Update every bone frame pointer to account for
                // scoochin' 'em back with last insert...
                frame_data = frame_data_start;
                repeat(frame, num_bone_frame)
                {
                    log_info(0, "Pushed back frame %d by 4", frame);
                    DEREF( Uint8*, frame_data ) += 4;  frame_data += 4;
                }



                return ktrue;
            }
        }
    }
    else
    {
        // Delete the frame
        if (num_bone_frame < 2) return kfalse;

        frame_data -= size;

        if (sdf_insert_data(frame_data, NULL, -size))
        {
            // Update number of joints for this base model
            num_bone_frame--;
            DEREF( Uint16, start_data + 4 ) = num_bone_frame;


            // Update bone frame pointers...
            frame_data = frame_data_start + (frame << 2);

            if (sdf_insert_data(frame_data, NULL, -4))
            {
                while (frame < num_bone_frame)
                {
                    DEREF( Uint8*, frame_data ) -= size;  frame_data += 4;
                    frame++;
                }



                // Update every every base model pointer to account for
                // scoochin' 'em forward with last insert...
                base_model_data = base_model_data_start;
                repeat(i, num_detail_level)
                {
                    repeat(j, num_base_model)
                    {
                        DEREF( Uint8*, base_model_data ) -= 4;  base_model_data += 4;
                        DEREF( Uint8*, base_model_data ) -= 4;  base_model_data += 4;
                        DEREF( Uint8*, base_model_data ) -= 4;  base_model_data += 4;
                        DEREF( Uint8*, base_model_data ) -= 4;  base_model_data += 4;
                        DEREF( Uint8*, base_model_data ) -= 4;  base_model_data += 4;
                    }
                }


                // Update every bone frame pointer to account for
                // scoochin' 'em forward with last insert...
                frame_data = frame_data_start;
                repeat(frame, num_bone_frame)
                {
                    DEREF( Uint8*, frame_data ) -= 4;  frame_data += 4;
                }


                return ktrue;
            }
        }
    }

    return kfalse;
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
char no_link[] = "NO LINK";
char linkage_filename[16];
Uint8* render_get_set_external_linkage(Uint8* data, Uint8* link_data)
{
    // <ZZ> If link_data is given, this function links one RDY (data) file to the animations
    //      of another RDY file (link_data).  If link_data is NULL, the function returns the
    //      name of the RDY file used for animation frames, or the string "NO LINK".
    //      In order to link, the two files must have the same number of base models.  The link
    //      file must have an equal or greater number of bones and joints for each base model.
    //      The joints and bones should also be in the same order, so the animation doesn't get
    //      all weird, but that isn't checked...  If setting linkage doesn't work, it returns
    //      NULL.  Non-NULL means that it worked...  If link_data is the same as data, the file
    //      should be unlinked, though an export is necessary to save the frames...
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame, num_bone, num_joint;
    Uint16 link_num_bone_frame, link_num_bone, link_num_joint;
    Uint8* base_model_data;
    Uint8** base_model_data_start;
    Uint8** frame_data_start;
    Uint8** link_base_model_data_start;
    Uint8** link_frame_data_start;
    Uint8* start_data;
    Uint16 i, j, k;
    //Uint8     file_type;
    SDF_PDATA file_start;
    Uint32    file_size;
    SDF_PDATA first_frame_data;
    Sint32    push_back;


    // Read the header...
    start_data = data;
    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;
    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);
    base_model_data_start = (Uint8**) data;
    frame_data_start = (Uint8**) (data + (num_base_model * 20 * DETAIL_LEVEL_MAX));


    if (link_data)
    {
        if (link_data == start_data)
        {
            // Just unflag it...
            flags &= (0xFFFF - DDD_EXTERNAL_BONE_FRAMES);
            DEREF( Uint16, start_data ) = flags;
        }
        else
        {
            // Read the link file's header...
            link_data += 3;

            if (num_base_model != *link_data) return NULL;

            link_data++;
            link_num_bone_frame = DEREF( Uint16, link_data ); link_data += 2;
            link_data += (ACTION_MAX << 1);
            link_data += (MAX_DDD_SHADOW_TEXTURE);
            link_base_model_data_start = (Uint8**) link_data;
            link_frame_data_start = (Uint8**) (link_data + (num_base_model * 20 * DETAIL_LEVEL_MAX));


            // Trying to set linkage...  Check to make sure it won't explode everything...
            repeat(i, num_base_model)
            {
                // Read in number of bones and joints
                base_model_data = base_model_data_start[i*5];
                num_joint = DEREF( Uint16, base_model_data + 4 );
                num_bone = DEREF( Uint16, base_model_data + 6 );
                base_model_data = link_base_model_data_start[i*5];
                link_num_joint = DEREF( Uint16, base_model_data + 4 );
                link_num_bone = DEREF( Uint16, base_model_data + 6 );

                if (num_bone > link_num_bone) return NULL;

                if (num_joint > link_num_joint) return NULL;
            }


            // Okay, it shouldn't blow up...  Set the flag...
            flags |= DDD_EXTERNAL_BONE_FRAMES;
            DEREF( Uint16, start_data ) = flags;


            // Copy the frame pointer block from the linked file...  Destroy old frame pointers...
            sdf_insert_data((Uint8*) frame_data_start, NULL, -(num_bone_frame << 2));
            sdf_insert_data((Uint8*) frame_data_start, (Uint8*) link_frame_data_start, (link_num_bone_frame << 2));


            // Write the number of frames
            DEREF( Uint16, start_data + 4 ) = link_num_bone_frame;


            // Push back/forward the base model pointers to make up for adding/deleting frames
            push_back = link_num_bone_frame;
            push_back -= num_bone_frame;
            push_back = push_back << 2;
            repeat(i, num_detail_level)
            {
                repeat(j, num_base_model)
                {
                    repeat(k, 5)
                    {
                        *base_model_data_start += push_back;
                        base_model_data_start++;
                    }
                }
            }
        }

        // Return non-NULL to show that it worked...
        return ((Uint8*) 1);
    }
    else
    {
        // Trying to see if it's linked or not...  Check the flag...
        if ((flags & DDD_EXTERNAL_BONE_FRAMES) == 0) return no_link;

        if (num_bone_frame == 0) return no_link;


        // Looks like it is linked, search through all files to find the filename...
        first_frame_data = (SDF_PDATA) (frame_data_start[0]);
        repeat(i, sdf_archive_get_num_files())
        {
            SDF_PHEADER pheader  = sdf_archive_get_header(i);
            Uint8      file_type = sdf_file_get_type(pheader);

            // Only check RDY files...
            if (file_type == SDF_FILE_IS_RDY)
            {
                // See if the first frame is in the current file's data block...
                file_start = sdf_file_get_data(pheader);
                file_size  = sdf_file_get_size(pheader);

                if (first_frame_data >= file_start && first_frame_data < (file_start + file_size))
                {
                    // Found a match...
                    sdf_archive_get_filename(i, linkage_filename, &file_type);
                    return linkage_filename;
                }
            }
        }


        // Didn't find a match...
        return no_link;
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_center_frame(Uint8* data, Uint16 frame, Uint8 axis)
{
    // <ZZ> This function centers the joints of a model on the given axis...
    Uint8* frame_data;
    float* joint_data;
    float* joint_data_start;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* start_data;
    Uint8 base_model;
    Uint16 i;
    float min, max, offset;


    start_data = data;
    data += 3;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = frame_data[2];
    data = data + (base_model * 20);
    base_model_data = DEREF( Uint8*, data );
    base_model_data += 4;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;
    frame_data += 11;


    // Find the min and max on the given axis...
    frame_data += (num_bone * 24);  // Skip bones...
    joint_data = (float*) frame_data;
    joint_data_start = joint_data;
    min = 10000.0f;
    max = -10000.0f;
    repeat(i, num_joint)
    {
        if (joint_data[axis] < min) { min = joint_data[axis]; }

        if (joint_data[axis] > max) { max = joint_data[axis]; }

        joint_data += 3;
    }



    // Scooch the joints around...
    joint_data = joint_data_start;
    offset = ((max - min) * 0.5f) - max;

    if (axis == ZZ)
    {
        offset = -0.25f;
    }

    if (axis == 3)
    {
        offset = 0.25f;
        axis = ZZ;
    }


    repeat(i, num_joint)
    {
        joint_data[axis] += offset;
        joint_data += 3;
    }


    // Fix the model for bone length and stuff...
    if (!keyb.down[SDLK_f])
    {
        render_fix_model_to_bone_length(start_data, frame, 9999);
    }

    render_generate_bone_normals(start_data, frame);
}
#endif


//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint16 render_marked_frame = 0;
void render_copy_frame(Uint8* data, Uint16 frame)
{
    // <ZZ> This function copies the marked frame to the given one...
    Uint16 last_frame;
    Uint8* frame_data;
    Uint8* last_frame_data;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* start_data;
    Uint8 base_model;
    Uint16 i, j;


    start_data = data;
    data += 3;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;
    last_frame = render_marked_frame;

    if (frame >= num_bone_frame) return;

    if (last_frame >= num_bone_frame) return;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    last_frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (last_frame << 2) );
    base_model = frame_data[2];

    if (last_frame_data[2] != base_model) last_frame_data = frame_data;

    if (last_frame_data == frame_data) return; // Copying to same frame...

    data = data + (base_model * 20);
    base_model_data = DEREF( Uint8*, data );
    base_model_data += 4;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;
    frame_data += 11;
    last_frame_data += 11;

    // Do the joint/bone copy...
    num_joint = (num_joint * 3) + (num_bone * 6);
    repeat(i, num_joint)
    {
        DEREF( float, frame_data ) = DEREF( float, last_frame_data );
        frame_data += 4;
        last_frame_data += 4;
    }


    // Do the shadow copy...
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        if (*last_frame_data && *frame_data)
        {
            // All are turned on...  Do a copy...
            *frame_data = *last_frame_data;
            frame_data++;  last_frame_data++;
            repeat(j, 8)
            {
                DEREF( float, frame_data ) = DEREF( float, last_frame_data );
                frame_data += 4;  last_frame_data += 4;
            }
        }
        else
        {
            // At least one is off...  Skip 'em...
            if (*frame_data == 0)
            {
                frame_data++;
            }
            else
            {
                frame_data += 33;
            }

            if (*last_frame_data == 0)
            {
                last_frame_data++;
            }
            else
            {
                last_frame_data += 33;
            }
        }
    }



    // Fix the model for bone length and stuff...
    render_fix_model_to_bone_length(start_data, frame, 9999);
    render_generate_bone_normals(start_data, frame);
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Uint16 original_vertex_angle[MAX_SELECT];
Uint8 global_autoshadow_vertex = 255;
void render_auto_shadow(Uint8* data, Uint16 frame, Uint8 shadow)
{
    // <ZZ> This function takes the given shadow layer for the given frame and applies it
    //      across all frames of the RDY file...  Rotates and scales the shadow layer to
    //      represent the selected vertices...
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_vertex;
    Uint16 num_bone;
    Uint16 num_joint;
    Uint8* base_model_data;
    Uint8* vertex_data;
    Uint8* bone_data;
    Uint8* frame_data;
    Uint8* block_start;
    Uint8 base_model, detail_level, original_base_model, alpha;
    Uint16 i, j, vertex;
    float original_centrid_xy[2];
    float current_centrid_xy[2];
    float original_centrid_distance;
    float current_centrid_distance;
    float original_corner_xy[4][2];
    float vector_xyz[3];
    Uint16 current_vertex_angle;
    int average_vertex_angle_change;
    Sint16 current_vertex_angle_change;
    float angle, sine, cosine, scale;


    // Error check...
    if (shadow > MAX_DDD_SHADOW_TEXTURE || select_num < 1)
    {
        return;
    }


    // Read the model's header block...
    flags = DEREF( Uint16, data ); data += 2;
    num_detail_level = *data;  data++;
    detail_level = 0;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) frame = 0;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);
    block_start = data;



    // Find the centrid (xy) for the selected vertices...
    original_centrid_xy[XX] = 0.0f;
    original_centrid_xy[YY] = 0.0f;
    repeat(vertex, select_num)
    {
        original_centrid_xy[XX] += select_data[vertex][XX];
        original_centrid_xy[YY] += select_data[vertex][YY];
    }
    original_centrid_xy[XX] /= select_num;
    original_centrid_xy[YY] /= select_num;
    log_info(0, "Original centrid at %f, %f", original_centrid_xy[XX], original_centrid_xy[YY]);



    // Find the average distance (xy) from the centrid to each selected vertex...
    // Find the angle (topview) from the centrid to each selected vertex too...
    original_centrid_distance = 0.0f;
    vector_xyz[ZZ] = 0.0f;
    repeat(vertex, select_num)
    {
        vector_xyz[XX] = select_data[vertex][XX] - original_centrid_xy[XX];
        vector_xyz[YY] = select_data[vertex][YY] - original_centrid_xy[YY];
        original_centrid_distance += vector_length(vector_xyz);
        original_vertex_angle[vertex] = (Uint16) (atan2(vector_xyz[YY], vector_xyz[XX]) * 65536.0f / (2.0 * PI));
    }
    original_centrid_distance /= select_num;
    log_info(0, "Original (frame %d) centrid distance is %f", frame, original_centrid_distance);



    // Find the original base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    original_base_model = *(frame_data + 2);

    // Find and remember the shadow data for the original frame...
    data = data + (original_base_model * 20) + (num_base_model * 20 * detail_level);
    base_model_data = DEREF( Uint8*, data );
    num_joint = DEREF( Uint16, base_model_data + 4 );
    num_bone = DEREF( Uint16, base_model_data + 6 );
    frame_data = frame_data + (24 * num_bone) + (12 * num_joint) + 11;
    // Skip through to the selected shadow layer...
    repeat(j, shadow)
    {
        alpha = frame_data[0];  frame_data++;

        if (alpha > 0) { frame_data += 32; }
    }
    alpha = frame_data[0];  frame_data++;

    if (alpha)
    {
        // Remember the shadow coordinates for the original frame...
        repeat(j, 4)
        {
            original_corner_xy[j][XX] = DEREF( float, frame_data );  frame_data += 4;
            original_corner_xy[j][YY] = DEREF( float, frame_data );  frame_data += 4;
            original_corner_xy[j][XX] -= original_centrid_xy[XX];
            original_corner_xy[j][YY] -= original_centrid_xy[YY];
        }
    }
    else
    {
        // Shadow layer isn't active...  That's not gonna work...
        return;
    }




    // Read through each frame of the model...
    repeat(i, num_bone_frame)
    {
        // Go to the current base model
        data = block_start;
        frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (i << 2) );
        base_model = *(frame_data + 2);

        if (frame_data[0] > 0 && i != frame && base_model == original_base_model)
        {
            // Generate the vertex coordinates for this frame...
            data = data + (base_model * 20) + (num_base_model * 20 * detail_level);
            base_model_data = DEREF( Uint8*, data );  data += 16;
            bone_data = DEREF( Uint8*, data );
            num_vertex = DEREF( Uint16, base_model_data );
            num_joint = DEREF( Uint16, base_model_data + 4 );
            num_bone = DEREF( Uint16, base_model_data + 6 );
            vertex_data = base_model_data + 8;
            frame_data = frame_data + 11;
            render_bone_frame(base_model_data, bone_data, frame_data);


            // Find the centrid of the selected vertices (should now be in different locations from before)
            current_centrid_xy[XX] = 0.0f;
            current_centrid_xy[YY] = 0.0f;
            repeat(vertex, select_num)
            {
                current_centrid_xy[XX] += select_data[vertex][XX];
                current_centrid_xy[YY] += select_data[vertex][YY];
            }
            current_centrid_xy[XX] /= select_num;
            current_centrid_xy[YY] /= select_num;
            log_info(0, "Current (frame %d) centrid at %f, %f", i, current_centrid_xy[XX], current_centrid_xy[YY]);


            // Find the new average distance from the centrid for each of the selected vertices...
            // Find the average difference in angle for each of the selected vertices too (as compared to the original)...
            current_centrid_distance = 0.0f;
            vector_xyz[ZZ] = 0.0f;
            average_vertex_angle_change = 0;
            repeat(vertex, select_num)
            {
                vector_xyz[XX] = select_data[vertex][XX] - current_centrid_xy[XX];
                vector_xyz[YY] = select_data[vertex][YY] - current_centrid_xy[YY];
                current_centrid_distance += vector_length(vector_xyz);
                current_vertex_angle = (Uint16) (atan2(vector_xyz[YY], vector_xyz[XX]) * 65536.0f / (2.0 * PI));

                current_vertex_angle_change = current_vertex_angle - original_vertex_angle[vertex];
                average_vertex_angle_change -= current_vertex_angle_change;
            }
            current_centrid_distance /= select_num;
            average_vertex_angle_change /= select_num;
            log_info(0, "Current centrid distance is %f", current_centrid_distance);
            log_info(0, "Average angle change is %f degrees", average_vertex_angle_change * 360.0f / 65536.0f);





            // Jump ahead to the shadow data for this frame...
            frame_data = frame_data + (24 * num_bone) + (12 * num_joint);


            // Skip through to the selected shadow layer...
            repeat(j, shadow)
            {
                alpha = frame_data[0];  frame_data++;

                if (alpha > 0) { frame_data += 32; }
            }
            alpha = frame_data[0];  frame_data++;

            if (alpha)
            {
                // Apply the original shadow coordinates to the new frame, based on centrid
                // location, average angle difference, and average distance difference....
                // Note: Shadow must be turned on in frame for autoshadow to work...
                angle = average_vertex_angle_change * UINT16_TO_RAD;
                sine = SIN(angle);
                cosine = COS(angle);

                if (original_centrid_distance < 0.000001f)
                {
                    scale = 1.0f;
                }
                else
                {
                    scale = current_centrid_distance / original_centrid_distance;
                }

                repeat(j, 4)
                {
                    if (j == global_autoshadow_vertex || global_autoshadow_vertex > 3)
                    {
                        vector_xyz[XX] = (sine * original_corner_xy[j][YY]) + (cosine * original_corner_xy[j][XX]);
                        vector_xyz[YY] = (-sine * original_corner_xy[j][XX]) + (cosine * original_corner_xy[j][YY]);
                        vector_xyz[XX] *= scale;
                        vector_xyz[YY] *= scale;
                        vector_xyz[XX] += current_centrid_xy[XX];
                        vector_xyz[YY] += current_centrid_xy[YY];

                        DEREF( float, frame_data ) = vector_xyz[XX];  frame_data += 4;
                        DEREF( float, frame_data ) = vector_xyz[YY];  frame_data += 4;
                    }
                    else
                    {
                        frame_data += 8;
                    }
                }
            }
        }
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_interpolate(Uint8* data, Uint16 frame)
{
    // <ZZ> This function averages the joint positions of the current frame with those of the
    //      next and last frames, provided that all of those frames use the same base model.
    //      Also interpolates shadow vertex coordinates...
    Uint16 next_frame;
    Uint16 last_frame;
    Uint8* frame_data;
    Uint8* next_frame_data;
    Uint8* last_frame_data;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* start_data;
    Uint8 base_model;
    Uint16 i, j;
    Uint8 looking;

    //float x, y, z, angle;

    start_data = data;
    data += 3;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    next_frame = frame + 1;

    if (next_frame >= num_bone_frame) next_frame = frame;

    last_frame = frame;

    if (last_frame > 0) last_frame--;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    next_frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (next_frame << 2) );
    last_frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (last_frame << 2) );
    base_model = frame_data[2];

    if (next_frame_data[2] != base_model) next_frame_data = frame_data;

    if (last_frame_data[2] != base_model) last_frame_data = frame_data;

    if (next_frame_data[0] != frame_data[0]) next_frame_data = frame_data;

    if (last_frame_data[0] != frame_data[0]) last_frame_data = frame_data;

    if (next_frame_data == frame_data)
    {
        // Find first frame of this action...
        next_frame = frame;
        looking = ktrue;

        while (looking)
        {
            if (next_frame > 0)
            {
                next_frame--;
                next_frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (next_frame << 2) );

                if (next_frame_data[2] != base_model) looking = kfalse;

                if (next_frame_data[0] != frame_data[0]) looking = kfalse;

                if (looking == kfalse)
                {
                    next_frame++;
                    next_frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (next_frame << 2) );
                }
            }
            else
            {
                looking = kfalse;
                next_frame = frame;
                next_frame_data =  frame_data;
            }
        }
    }

    if (last_frame_data == frame_data)
    {
        // Find the final frame of this action...
        last_frame = frame;
        looking = ktrue;

        while (looking)
        {
            if (last_frame < (num_bone_frame - 1))
            {
                last_frame++;
                last_frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (last_frame << 2) );

                if (last_frame_data[2] != base_model) looking = kfalse;

                if (last_frame_data[0] != frame_data[0]) looking = kfalse;

                if (looking == kfalse)
                {
                    last_frame--;
                    last_frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (last_frame << 2) );
                }
            }
            else
            {
                looking = kfalse;
                last_frame = frame;
                last_frame_data =  frame_data;
            }
        }
    }



    if (next_frame_data == last_frame_data) return;

    data = data + (base_model * 20);
    base_model_data = DEREF( Uint8*, data );
    base_model_data += 4;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;
    frame_data += 11;
    next_frame_data += 11;
    last_frame_data += 11;











    // Interpolate bone normals
    num_bone = num_bone * 6;
    repeat(i, num_bone)
    {
        DEREF( float, frame_data ) = (DEREF( float, frame_data ) + DEREF( float, frame_data ) + DEREF( float, next_frame_data ) + DEREF( float, last_frame_data )) * 0.25f;
        frame_data += 4;
        next_frame_data += 4;
        last_frame_data += 4;
    }


    // Interpolate joint positions
    num_joint = num_joint * 3;
    repeat(i, num_joint)
    {
        DEREF( float, frame_data ) = (DEREF( float, frame_data ) + DEREF( float, frame_data ) + DEREF( float, next_frame_data ) + DEREF( float, last_frame_data )) * 0.25f;
        frame_data += 4;
        next_frame_data += 4;
        last_frame_data += 4;
    }



    // Do the shadow interpolations...
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        if (*next_frame_data && *last_frame_data && *frame_data)
        {
            // All are turned on...  Do an interpolation...
            j = *frame_data;
            j += *frame_data;
            j += *next_frame_data;
            j += *last_frame_data;
            *frame_data = j >> 2;
            frame_data++;  next_frame_data++;  last_frame_data++;
            repeat(j, 8)
            {
                DEREF( float, frame_data ) = (DEREF( float, frame_data ) + DEREF( float, frame_data ) + DEREF( float, next_frame_data ) + DEREF( float, last_frame_data )) * 0.25f;
                frame_data += 4;  next_frame_data += 4;  last_frame_data += 4;
            }
        }
        else
        {
            // At least one is off...  Skip 'em...
            if (*frame_data == 0)
            {
                frame_data++;
            }
            else
            {
                frame_data += 33;
            }

            if (*next_frame_data == 0)
            {
                next_frame_data++;
            }
            else
            {
                next_frame_data += 33;
            }

            if (*last_frame_data == 0)
            {
                last_frame_data++;
            }
            else
            {
                last_frame_data += 33;
            }
        }
    }



    // Fix the model for bone length and stuff...
    if (!keyb.down[SDLK_f])
    {
        render_fix_model_to_bone_length(start_data, frame, 9999);
    }

    render_generate_bone_normals(start_data, frame);
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void render_shadow_reset(Uint8* data, Uint16 frame)
{
    // <ZZ> This function resets the shadow vertex coordinates of the given frame to those
    //      of the last frame...
    Uint16 last_frame;
    Uint8* frame_data;
    Uint8* last_frame_data;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_bone, num_joint;
    Uint8* base_model_data;
    Uint8* start_data;
    Uint8 base_model;
    Uint16 i, j;


    start_data = data;
    data += 3;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return;

    last_frame = frame;

    if (last_frame > 0) last_frame--;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    last_frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (last_frame << 2) );
    base_model = frame_data[2];

    if (last_frame_data[2] != base_model) return;

    data = data + (base_model * 20);
    base_model_data = DEREF( Uint8*, data );
    base_model_data += 4;
    num_joint = DEREF( Uint16, base_model_data ); base_model_data += 2;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;
    frame_data += 11;
    last_frame_data += 11;


    // Skip the joint positions...
    num_joint = (num_joint * 3) + (num_bone * 6);
    frame_data += (num_joint * 4);
    last_frame_data += (num_joint * 4);


    // Do the shadow copy...
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        if (*last_frame_data && *frame_data)
        {
            // All are turned on...  Do an interpolation...
            *frame_data = *last_frame_data;
            frame_data++;  last_frame_data++;
            repeat(j, 8)
            {
                DEREF( float, frame_data ) = DEREF( float, last_frame_data );
                frame_data += 4;  last_frame_data += 4;
            }
        }
        else
        {
            // At least one is off...  Skip 'em...
            if (*frame_data == 0)
            {
                frame_data++;
            }
            else
            {
                frame_data += 33;
            }

            if (*last_frame_data == 0)
            {
                last_frame_data++;
            }
            else
            {
                last_frame_data += 33;
            }
        }
    }
}
#endif

//-----------------------------------------------------------------------------------------------
