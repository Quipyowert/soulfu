// <ZZ> This file contains functions pertaining to random numbers
//      random_setup            - Sets up the random number table
//      random_number()           - Returns a random number from 0 to 255
//      random_dice             - Simulates a dice roll

#include "random.h"
#include "sdf_archive.h"

RANDOM_DATA g_rand =
{
    0,   // next
    0,   // size
    NULL // table
};

//-----------------------------------------------------------------------------------------------
Sint8 random_setup(int seed)
{
    // <ZZ> This function figures out where the random number table is, and picks the first
    //      random number to use.

    SDF_PHEADER pheader = sdf_archive_find_header("RANDOM.DAT");

    g_rand.size = 0;
    g_rand.next = 0;
    g_rand.table = NULL;

    if (pheader)
    {
        g_rand.size  = sdf_file_get_size(pheader);
        g_rand.table = sdf_file_get_data(pheader);
        g_rand.next  = seed;
        log_info(0, "Found the random number table");
        return ktrue;
    }

    log_error(0, "Could not find the random number table");
    return kfalse;
}

//-----------------------------------------------------------------------------------------------
// <ZZ> This macro returns a random number...  No fancy stuff...  Usage...  x = random_number();
//      Result should range from 0 - 255...

Uint8 random_number()
{
    Uint8 retval;

    retval = g_rand.table[g_rand.next & (g_rand.size-1)];
    g_rand.next = (g_rand.next + 1) % (g_rand.size - 1);

    return retval;
}

//-----------------------------------------------------------------------------------------------

Uint8 get_random(Uint32 val)
{
    // <BB> grab a number from the random number table. Used to calculate the "CRC" to validate
    //      files / packets

    return g_rand.table[val % (g_rand.size-1)];
};

//-----------------------------------------------------------------------------------------------
Uint16 random_dice(Uint8 number, Uint16 sides)
{
    // <ZZ> This function rolls some dice and returns the result.  random_dice(2, 6) == 2d6...
    Uint8 i;
    Uint16 value;
    Uint8 temp;


    value = 0;

    if (sides > 0)
    {
        repeat(i, number)
        {
            // Roll one die...
            temp = random_number();
            temp = temp % sides;
            temp++;

            // And add it to the running total
            value += temp;
        }
    }

    return value;
}

//-----------------------------------------------------------------------------------------------
Uint8* random_name(Uint8* filedata)
{
    // <ZZ> This function returns a pointer to a random name string (NAME_STRING) from one of
    //      the random naming datafiles.  Data is in the format of a null-terminated text file,
    //      with commas seperating random choices and new lines (null terms) seperating
    //      segment blocks.  The '-' character is used to repeat the last value in the
    //      preceding block...  "Dun-agorn" becomes "Dunnagorn"...

#define MAX_NAME_SEGMENT 8
    Uint16 segment, num_segment;
    Uint16 num_choice[MAX_NAME_SEGMENT];
    Uint8* segment_start[MAX_NAME_SEGMENT];
    Uint16 choice;
    Uint8* read;
    Uint8 last_value;
    int filesize;
    int i;
    Uint16 name_size;
    SDF_PHEADER pheader;



    log_info(1, "Generating random name...");


    NAME_STRING[0] = 0;
    name_size = 0;
    pheader = sdf_archive_find_header_by_data(filedata);

    if (NULL != pheader)
    {
        // Found the file in the SDF datafile...
        filesize = sdf_file_get_size(pheader);

        // Clear out our totals...
        num_segment = 0;
        repeat(i, MAX_NAME_SEGMENT)
        {
            num_choice[i] = 0;
            segment_start[i] = filedata;
        }


        // Read through the text file, looking for commas and null terms...
        read = filedata;
        last_value = 0;
        repeat(i, filesize)
        {
            if (*read == ',')
            {
                // We found a new choice...
                num_choice[num_segment]++;
                log_info(1, "Found choice");
            }
            else if (*read == 0)
            {
                // Was the last value a null term too?
                if (last_value == 0)
                {
                    // Yup, that means we're all done with our preliminary scan...
                    // Two returns in a row should only happen at end of file...
                    i = filesize;
                }
                else
                {
                    // We found a new choice and segment...
                    log_info(1, "Found segment");
                    num_choice[num_segment]++;
                    num_segment++;

                    if (num_segment >= MAX_NAME_SEGMENT)
                    {
                        // We can't handle any more segments...  Time to quit...
                        i = filesize;
                    }
                    else
                    {
                        // Remember where the next segment starts...
                        segment_start[num_segment] = read + 1;
                    }
                }
            }


            last_value = *read;
            read++;
        }



        log_info(1, "Picking segments...");

        // Should now have a rough idea of how many name segments and stuff we're lookin' at...
        // Lets randomly pick some...
        repeat(segment, num_segment)
        {
            log_info(1, "Segment %d", segment);

            // Pick a random choice for each segment...
            if (num_choice[segment] > 0)
            {
                choice = random_number();
                choice = choice % num_choice[segment];
                log_info(1, "Choice %d (of %d)", choice, num_choice[segment]);


                // Now search for the selected choice by reading through where commas are...
                read = segment_start[segment];
                i = 0;

                while (i < choice)
                {
                    if (*read == ',')
                    {
                        i++;
                    }

                    read++;
                }


                // Read should now be at the start of our segment choice...  Copy it into the name string...
                while (*read != ',' && *read != 0 && name_size < 100)
                {
                    NAME_STRING[name_size] = *read;

                    if (NAME_STRING[name_size] == '-' && name_size > 0 && segment > 0)
                    {
                        NAME_STRING[name_size] = NAME_STRING[name_size-1];
                    }

                    read++;
                    name_size++;
                }

                NAME_STRING[name_size] = 0;
            }
        }
    }




    NAME_STRING[15] = 0;  // Never allow names longer than 15 characters...
    log_info(1, "New name is %s", NAME_STRING);
    return NAME_STRING;
}



//log_info(0, "Getting random name...");
//
//
//pheader = sdf_archive_find_header_by_data(filedata);
//if(NULL != pheader)
//{
//    Found the file in the SDF datafile...
//        filesize = sdf_file_get_size(pheader);
//
//
//    count = filesize >> 4;
//    log_info(0, "%d names in file...", count);
//    if(count > 0)
//    {
//        count = random_number();
//        count = count % (*filedata);
//        filedata += (count << 4) + 16;
//    }
//}
//return filedata;


//-----------------------------------------------------------------------------------------------
