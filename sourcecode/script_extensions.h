#pragma once

// Extended Script helper functions
#include "soulfu_types.h"
#include "pxss_lib.h"

// For AcquireTarget()
#define ACQUIRE_SEE_NEUTRAL   1
#define ACQUIRE_SEE_FRIENDLY  2
#define ACQUIRE_SEE_ENEMY     4
#define ACQUIRE_SEE_INVISIBLE 8
#define ACQUIRE_SEE_BEHIND    16
#define ACQUIRE_OPEN_MOUNT_ONLY 32
#define ACQUIRE_STANDING_ONLY 64
#define ACQUIRE_IGNORE_COLLISION 128

// For WindowEmacs
#define EMACS_SIZE 2048
#define EMACS_HORIZ_SIZE 74
#define EMACS_BAD_SIZE 13



// For SYS_MAPROOM...
#define MAP_ROOM_SRF                  0
#define MAP_ROOM_X                    1
#define MAP_ROOM_Y                    2
#define MAP_ROOM_DOOR_X               3
#define MAP_ROOM_DOOR_Y               4
#define MAP_ROOM_DOOR_Z               5
#define MAP_ROOM_ROTATION             6
#define MAP_ROOM_SEED                 7
#define MAP_ROOM_TWSET                8
#define MAP_ROOM_LEVEL                9
#define MAP_ROOM_FLAGS               10
#define MAP_ROOM_DIFFICULTY          11
#define MAP_ROOM_AREA                12
#define MAP_ROOM_DEFEATED_CHARACTER  13
#define MAP_ROOM_NEXT_ROOM           14
#define MAP_ROOM_FROM_ROOM           15
#define MAP_ROOM_MULTI_CONNECT       16
#define MAP_ROOM_NUMBER              17
#define MAP_ROOM_CURRENT             18
#define MAP_ROOM_DOOR_PUSHBACK       19
#define MAP_ROOM_DOOR_SPIN           20
#define MAP_ROOM_LAST_TOWN           21
#define MAP_ROOM_REMOVE             0x80
#define MAP_ROOM_UPDATE_FLAGS       254
#define MAP_ROOM_ADD                0xFF

#define FAST_FUNCTION_SPAWN        0   // Offset for the Spawn() function
#define FAST_FUNCTION_REFRESH      2   // Offset for the Refresh() function
#define FAST_FUNCTION_EVENT        4   // Offset for the Event() function
#define FAST_FUNCTION_AISCRIPT     6   // Offset for the AIScript() function
#define FAST_FUNCTION_BUTTONEVENT  8   // Offset for the ButtonEvent() function
#define FAST_FUNCTION_GETNAME      10  // Offset for the GetName() function
#define FAST_FUNCTION_UNPRESSED    12  // Offset for the Unpressed() function
#define FAST_FUNCTION_FRAMEEVENT   14  // Offset for the FrameEvent() function
#define FAST_FUNCTION_MODELSETUP   16  // Offset for the ModelSetup() function...
#define FAST_FUNCTION_DEFENSERATING 18 // Offset for the DefenseRating() function...
#define FAST_FUNCTION_SETUP        20  // Offset for the Setup() function...
#define FAST_FUNCTION_DIRECTUSAGE  22  // Offset for the DirectUsage() function...
#define FAST_FUNCTION_ENCHANTUSAGE 24  // Offset for the EnchantUsage() function...

struct script_context_t;
//typedef struct script_context_t * PScriptContext;

enum opcode_extended_e
{
    OPCODE_EX_CALLFUNCTION        = OPCODE_CALLFUNCTION,
    OPCODE_EX_LOCALMESSAGE        = OPCODE_UNUSED_24,
    OPCODE_EX_LOGMESSAGE          = OPCODE_UNUSED_25,
    OPCODE_EX_FINDSELF            = OPCODE_UNUSED_26,
    OPCODE_EX_DEBUGMESSAGE        = OPCODE_UNUSED_29,
    OPCODE_EX_STRINGRANDOMNAME    = OPCODE_UNUSED_58,
    OPCODE_EX_STRINGSANITIZE      = OPCODE_UNUSED_59,
    OPCODE_EX_NETWORKMESSAGE      = OPCODE_UNUSED_60,
    OPCODE_EX_STRINGLANGUAGE      = OPCODE_UNUSED_61,
    OPCODE_EX_SPAWN               = OPCODE_UNUSED_74,
    OPCODE_EX_GOPOOF              = OPCODE_UNUSED_75,
    OPCODE_EX_DISMOUNT            = OPCODE_UNUSED_76,
    OPCODE_EX_ROLLDICE            = OPCODE_UNUSED_77,
    OPCODE_EX_PLAYSOUND           = OPCODE_UNUSED_78,
    OPCODE_EX_PLAYMEGASOUND       = OPCODE_UNUSED_79,
    OPCODE_EX_DISTANCESOUND       = OPCODE_UNUSED_80,
    OPCODE_EX_PLAYMUSIC           = OPCODE_UNUSED_81,
    OPCODE_EX_UPDATEFILES         = OPCODE_UNUSED_82,
    OPCODE_EX_ACQUIRETARGET       = OPCODE_UNUSED_84,
    OPCODE_EX_FINDPATH            = OPCODE_UNUSED_85,
    OPCODE_EX_BUTTONPRESS         = OPCODE_UNUSED_86,
    OPCODE_EX_AUTOAIM             = OPCODE_UNUSED_87,
    OPCODE_EX_ROOMHEIGHTXY        = OPCODE_UNUSED_88,
    OPCODE_EX_TOTEXT              = OPCODE_UNUSED_92,
    OPCODE_EX_TOSTRING            = OPCODE_UNUSED_93,
    OPCODE_EX_WINDOWBORDER        = OPCODE_UNUSED_96,
    OPCODE_EX_WINDOWSTRING        = OPCODE_UNUSED_97,
    OPCODE_EX_WINDOWMINILIST      = OPCODE_UNUSED_98,
    OPCODE_EX_WINDOWSLIDER        = OPCODE_UNUSED_99,
    OPCODE_EX_WINDOWIMAGE         = OPCODE_UNUSED_100,
    OPCODE_EX_WINDOWTRACKER       = OPCODE_UNUSED_101,
    OPCODE_EX_WINDOWBOOK          = OPCODE_UNUSED_102,
    OPCODE_EX_WINDOWINPUT         = OPCODE_UNUSED_103,
    OPCODE_EX_WINDOWEMACS         = OPCODE_UNUSED_104,
    OPCODE_EX_WINDOWMEGAIMAGE     = OPCODE_UNUSED_105,
    OPCODE_EX_WINDOW3DSTART       = OPCODE_UNUSED_106,
    OPCODE_EX_WINDOW3DEND         = OPCODE_UNUSED_107,
    OPCODE_EX_WINDOW3DPOSITION    = OPCODE_UNUSED_108,
    OPCODE_EX_WINDOW3DMODEL       = OPCODE_UNUSED_109,
    OPCODE_EX_MODELASSIGN         = OPCODE_UNUSED_110,
    OPCODE_EX_PARTICLEBOUNCE      = OPCODE_UNUSED_111,
    OPCODE_EX_WINDOWEDITKANJI     = OPCODE_UNUSED_112,
    OPCODE_EX_WINDOW3DROOM        = OPCODE_UNUSED_113,
    OPCODE_EX_INDEXISLOCALPLAYER  = OPCODE_UNUSED_114,
    OPCODE_EX_FINDBINDING         = OPCODE_UNUSED_115,
    OPCODE_EX_FINDTARGET          = OPCODE_UNUSED_116,
    OPCODE_EX_FINDOWNER           = OPCODE_UNUSED_117,
    OPCODE_EX_FINDINDEX           = OPCODE_UNUSED_118,
    OPCODE_EX_FINDBYINDEX         = OPCODE_UNUSED_119,
    OPCODE_EX_FINDWINDOW          = OPCODE_UNUSED_120,
    OPCODE_EX_FINDPARTICLE        = OPCODE_UNUSED_121,
    OPCODE_EX_ATTACHTOTARGET      = OPCODE_UNUSED_123,
    OPCODE_EX_GETDIRECTION        = OPCODE_UNUSED_124,
    OPCODE_EX_DAMAGETARGET        = OPCODE_UNUSED_125,
    OPCODE_EX_EXPERIENCEFUNCTION  = OPCODE_UNUSED_126
};
typedef enum opcode_extended_e OPCODE_EXTENDED;

extern char system_file_name[16];
extern char unused_file_name[16];


//-----------------------------------------------------------------------------------------------
// Stuff for SYS_ENCHANT_CURSOR...
#define ENCHANT_CURSOR_ACTIVE       0
#define ENCHANT_CURSOR_TARGET       1
#define ENCHANT_CURSOR_TARGET_ITEM  2

// Stuff for the enchantment targeting cursor thing...
struct enchant_cursor_t
{
    bool_t active;
    Uint16 character;
    Uint16 item;
    Uint16 target;
    Uint8  target_item;
};
typedef struct enchant_cursor_t ENCHANT_CURSOR;


//-----------------------------------------------------------------------------------------------
#define GLOBAL_SPAWN_OWNER      0
#define GLOBAL_SPAWN_TARGET     1
#define GLOBAL_SPAWN_TEAM       2
#define GLOBAL_SPAWN_SUBTYPE    3
#define GLOBAL_SPAWN_CLASS      4

struct spawn_info_t
{
    Uint16 owner;
    Uint16 target;
    Uint8  team;
    Uint8  subtype;
    Uint8  cclass;
};
typedef struct spawn_info_t SPAWN_INFO;

//-----------------------------------------------------------------------------------------------
// For Window3D
struct win_3d_info_t
{
    float x;
    float y;
    float w;
    float h;
    float camera_xyz[3];
    float target_xyz[3];
    Uint8 order;
};
typedef struct win_3d_info_t WIN_3D_INFO;

//-----------------------------------------------------------------------------------------------
struct soulfu_global_script_context_t
{
    GlobalScriptContext base;

    float  scale;
    Uint16 item_keep;
    Uint16 item_index;
    Uint8  item_bone_name;

    //---- SHARED INFO ----
    SOULFU_WINDOW  _screen;
    FOCUS_INFO     _focus;
    ENCHANT_CURSOR enc_cursor;
    SPAWN_INFO     _spawn_info;
};
typedef struct soulfu_global_script_context_t SoulfuGlobalScriptContext, *PSoulfuGlobalScriptContext;

extern SoulfuGlobalScriptContext g_sf_scr;

struct soulfu_script_context_t
{
    ScriptContext              base;
    PSoulfuGlobalScriptContext psgs;

    // loads of global messiness
    matrix_4x3 matrix;
    vec2       point[4];
    vec2       texpoint[4];

    //---- SHARED INFO ----
    SOULFU_WINDOW  screen;
    WIN_3D_INFO    window3d;
    FOCUS_INFO     self;
    ENCHANT_CURSOR enc_cursor;
    SPAWN_INFO     spawn_info;

};
typedef struct soulfu_script_context_t SoulfuScriptContext, *PSoulfuScriptContext;

//-----------------------------------------------------------------------------------------------

extern float  autoaim_velocity_xyz[3];
extern Uint16 autoaim_target;

//-----------------------------------------------------------------------------------------------

void call_enchantment_function();

bool_t sf_extensions_setup(PSoulfuGlobalScriptContext psgs);
bool_t run_opcode_external(PSoulfuScriptContext ps, OPCODE_EXTENDED opcode);

