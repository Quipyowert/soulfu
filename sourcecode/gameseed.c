Uint32 game_seed;

//-----------------------------------------------------------------------------------------------
void generate_game_seed(void)
{
    // <ZZ> This function generates a new game seed...
    srand((unsigned int)time(0));
    game_seed  = (rand() & 255);  game_seed <<= 8;
    game_seed |= (rand() & 255);  game_seed <<= 8;
    game_seed |= (rand() & 255);  game_seed <<= 8;
    game_seed |= (rand() & 255);
    log_info(0, "Set game seed to 0x%08x", game_seed);
}

//-----------------------------------------------------------------------------------------------
