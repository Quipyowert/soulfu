#pragma once

#ifdef WIN32
#include <Windows.h>
#define MAX_SCANCODE 0x0100      // Must be 0x0100
#endif

#define JOY_TOLERANCE 5000                      // Joystick must move so much before it activates...

#define MAX_KEY        SDLK_LAST                // The number of keys
#define MAX_KEY_BUFFER 256                      // Must be 256
#define MAX_ASCII      128                      // Must be 128

struct keyboard_t
{
    Uint16 last_pressed;                 // The sdlk value of the last key pressed...
    Uint8  shift[MAX_ASCII];             // Convert an SDLK_ to ASCII caps

#ifdef WIN32
    Uint8 transform[MAX_SCANCODE];       // Maps scan code values to ASCII ones of the current keyboard layout
#endif

    bool_t down[MAX_KEY];                // ktrue if key is held down
    bool_t pressed[MAX_KEY];             // ktrue if key was just pressed (updated once per timing cycle)
    bool_t unpressed[MAX_KEY];           // ktrue if key was just released (updated once per timing cycle)
    bool_t win_pressed[MAX_KEY];         // ktrue if key was just pressed (updated once per display)
};
typedef struct keyboard_t KEYBOARD;

extern KEYBOARD keyb;

struct key_buffer_t
{
    Uint8  buffer[MAX_KEY_BUFFER];    // For reading keyboard strings
    Uint32 read;                      // FIFO read position
    Uint32 write;                     // FIFO write position
};
typedef struct key_buffer_t KEY_BUFFER;

extern KEY_BUFFER key_buff;

#define MOUSE_TEXT_TIME 10                      // Number of ticks mouse text should show after taking off of character...
#define MAX_MOUSE_BUTTON 4

#define BUTTON0    0
#define BUTTON1    1
#define BUTTON2    2
#define BUTTON3    3

struct mouse_t
{
    FOCUS_INFO last_click;                  // Object the mouse cursor last clicked on
    FOCUS_INFO last;                        // Object the mouse cursor was last over
    FOCUS_INFO current;                     // Object the mouse cursor is currently over
    bool_t     draw;
    bool_t     camera_active;
    char       text[MAX_STRING_SIZE];       // The alternate text...  Tool tips...
    char       text_timer;                  // For names of mouse-over'd characters/items
    Uint8      alpha;                       // Mouse transparency...

    float      x;                            // Left/Right cursor position, 0-399.99
    float      y;                            // Up/Down cursor position, 0-299.99
    float      last_x;                       // Last Left/Right cursor position, 0-399.99
    float      last_y;                       // Last Up/Down cursor position, 0-299.99
    float      character_distance;           // Distance from highlighted character (for finding nearest)
    Uint16     idle_timer;                   // Time mouse has been unmoved...

    bool_t     down[MAX_MOUSE_BUTTON];      // Current button states for the mouse
    bool_t     pressed[MAX_MOUSE_BUTTON];   // Button clicks...  like button
    bool_t     unpressed[MAX_MOUSE_BUTTON]; // Button releases...  ditto

};
typedef struct mouse_t MOUSE;

extern MOUSE mouse;

struct window_input_info_t
{
    FOCUS_INFO input;               // For WindowInput()
    Sint32     cursor_pos;          // For WindowInput()
    Uint8      mouse;               // For WindowInput()
};
typedef struct window_input_info_t WINDOW_INPUT_INFO;

extern WINDOW_INPUT_INFO last_wi;


/* begin - poobah */
#define JOY_TOLERANCE 5000                      // Joystick must move so much before it activates...
extern Sint32 joy_list_count;
/* end - poobah */

//-------------------------------------------------------------------------------------------
#define MAX_PLAYER_DEVICE_BUTTON 9              // 9 needed for keyboard...

#define PLAYER_DEVICE_NONE 0
#define PLAYER_DEVICE_KEYBOARD 1
#define PLAYER_DEVICE_JOYSTICK_1 2
#define PLAYER_DEVICE_JOYSTICK_2 3
#define PLAYER_DEVICE_JOYSTICK_3 4
#define PLAYER_DEVICE_JOYSTICK_4 5

#define PLAYER_DEVICE_TYPE -1
#define PLAYER_DEVICE_BUTTON_LEFT 0
#define PLAYER_DEVICE_BUTTON_RIGHT 1
#define PLAYER_DEVICE_BUTTON_SPECIAL1 2
#define PLAYER_DEVICE_BUTTON_SPECIAL2 3
#define PLAYER_DEVICE_BUTTON_ITEMS 4
#define PLAYER_DEVICE_BUTTON_MOVE_UP 5
#define PLAYER_DEVICE_BUTTON_MOVE_DOWN 6
#define PLAYER_DEVICE_BUTTON_MOVE_LEFT 7
#define PLAYER_DEVICE_BUTTON_MOVE_RIGHT 8
#define PLAYER_DEVICE_BUTTON_ITEMS_DOWN 9

struct player_device_t
{
    Uint8  type;                              // None, Keyboard, Joystick 1, Joystick 2, Joystick 3, Joystick 4...

    Uint16 button[MAX_PLAYER_DEVICE_BUTTON];  // Either keyboard scan codes or joystick button numbers...
    Uint8  button_pressed[4];                 // One if button was just pressed, zero if not...
    Uint8  button_unpressed[4];               // One if button was just let up, zero if not...
    Uint8  button_pressed_copy[4];

    Uint8  inventory_toggle;                  // ktrue if player pressed the inventory button (and script should toggle the gem)...
    Uint8  inventory_down;                    // ktrue if player is holding the inventory button...
    Uint8  controls_active;                   // kfalse if player controls don't actually move character...  But script can still read 'em...  Used to access inventory...
    float  xy[2];

};
typedef struct player_device_t PLAYER_DEVICE;

extern PLAYER_DEVICE player_device[MAX_LOCAL_PLAYER];

//-------------------------------------------------------------------------------------------
#define input_quick_reset_key_buffer() { key_buff.read = 0;  key_buff.write = 0; }


void input_update(void);
Sint8 input_setup(void);
Uint8 input_read_key_buffer(void);
void input_reset_window_key_pressed(void);
void input_read(void);
bool_t input_camera_controls(void);
