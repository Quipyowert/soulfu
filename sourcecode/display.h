#pragma once

#include "sdf_archive.h"
#include "soulfu_types.h"

#include <SDL_opengl.h>


#define INVALID_TEXTURE (~(GLuint)0)

//#define ZNEAR 1.25f                     // The near frustum plane
#define ZNEAR 0.625f                     // The near frustum plane
#define ALPHA_TOLERANCE 64              // Tolerance for color key transparency
#define ALPHA_BLUR 75                   // Amount to fade neighbors in '=' prefixed files
#define ALPHA_MIN  0                    // Minimum alpha allowed for neighbors

#define FADE_IN             -4          //
#define FADE_OUT            4           //
#define FADE_TYPE_NONE      0           // No fade
#define FADE_TYPE_CIRCLE    1           // Circle fade
#define FADE_TYPE_WARNING   2           // Warning flash
#define FADE_TYPE_FULL      3           // Fullscreen fade
#define CIRCLE_QUARTER 8                // Points in a quarter circle
#define CIRCLE_POINTS (CIRCLE_QUARTER*4)// Number of points in the circle fade effect
#define CIRCLE_TOTAL_POINTS (CIRCLE_POINTS + 5)  // 4 for corners, 1 redundant
#define WARNING_SIZE 20.0f              //

struct camera_t
{
    Uint16  rotation_xy[2];      // The current camera rotation...
    int     rotation_add_xy[2];  // Movement offsets for camera...
    float   distance;            // The last distance of the camera from the players
    float   to_distance;         // The desired distance of the camera from the players
    Uint16  to_rotation_xy[2];   // The desired camera rotation...
    float   xyz[3];              // The camera location
    float   fore_xyz[3];         // The forward vector of the camera
    float   side_xyz[3];         // The side vector of the camera
    float   target_xyz[3];       // The location of the camera's target point (look at)

};
typedef struct camera_t CAMERA;

extern CAMERA camera;



extern float map_side_xy[2];                   // For doin' the view triangle on the automap...


// Important textures...
extern GLuint texture_ascii;
extern void*  texture_ascii_ptr;  // Used for drawing joint number particles in modeler...
extern GLuint texture_bookfnt;
extern GLuint texture_paper[2];
extern GLuint texture_winalt;
extern GLuint texture_winside;
extern GLuint texture_wincorn;
extern GLuint texture_wintrim;
extern GLuint texture_winmini;
extern GLuint texture_winminy;
extern GLuint texture_winslid;
extern GLuint texture_wininput;
extern GLuint texture_petrify;
extern GLuint texture_armor;
extern GLuint texture_armorst;
extern GLuint texture_automap_stair;
extern GLuint texture_automap_town;
extern GLuint texture_automap_boss;
extern GLuint texture_automap_virtue;

// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
extern GLuint texture_bad_mpsamp00;
extern GLuint texture_bad_mpsamp01;
extern GLuint texture_bad_mpfence;
extern GLuint texture_bad_mppave;
extern GLuint texture_bad_mpdecal;
extern GLuint texture_bad_mphsbt00;
extern GLuint texture_bad_mphsmd00;
extern GLuint texture_bad_mphstp00;
extern GLuint texture_bad_mpflor00;
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!

void display_blur_image(Uint8* data, Uint16 x_size, Uint16 y_size);
void display_export_tga(char* filename, Uint8* data_start, Uint16 x_size, Uint16 y_size, Sint8 bytes_per_pixel);
void display_get_onscreen_xyd(float* in_xyz, float* out_xyd);
void display_slider(float x, float y, Uint8 w, Uint8 h, float scale, float position);
void display_window(SOULFU_WINDOW * pwin, Uint16 side_mask);
void display_mouse_text_box(float win_scale, float tlx, float tly, float brx, float bry, Uint32 image_number);
void display_marker(Uint8* color, float x, float y, float z, float scale);
void display_crosshairs(Uint8* color, float x, float y, float scale);
void display_riser_marker(Uint8* color, float x, float y, float z, float scale);
void display_2d_marker(Uint8* color, float x, float y, float scale);
void display_solid_marker(Uint8* color, float x, float y, float z, float scale);
void display_font(Uint8 ascii, float x, float y, float scale);
void display_kanji(Uint16 kanji, float x, float y, float scale_x, float scale_y);
void display_book_font(Uint8 ascii, float x, float y, float xtr, float ytr, float scale);
void display_book_page(Uint8* page_start, float x_start, float y_start, float* offsets_xy, float scale, int num_columns, int num_rows, Sint8 draw_back_page, Uint8 page_number);
void display_string(char* ascii_string, float x, float y, float scale);
void display_mini_list(char* options, float x, float y, Uint8 w, Uint8 h, float scale, Uint16 value);
void display_input(char* current_text, float x, float y, Sint8 w, float scale, Sint8 current_position);
void display_emacs(float x, float y, int w, int h, Uint8 curx, Uint8 cury, Uint8 scrollx, Uint16 scrolly, char* text, float scale);
void display_unload_all_textures(void);
Sint8 display_load_texture(SDF_PHEADER index);
void display_load_all_textures(void);
Uint8 display_extension_supported(char* extension_name);
Sint8 display_setup(Uint16 size_x, Uint16 size_y, Uint8 color_depth, Uint8 z_depth, Uint8 full_screen);
void display_character_dot(Uint8* color, float x, float y, float z, float scale);
void display_loadin(float amount_done, bool_t do_kanji);
void display_start_fade(Uint8 type, Sint8 direction, float x, float y, Uint8* color);
void display_fade(void);
void display_camera_position(Uint16 times_to_slog, float slog_weight, float slog_z_weight);
void display_look_at(float* lilcam_xyz, float* liltarg_xyz);
Uint32 display_get_texture(char* filename);
void display_kanji_setup(void);

//-----------------------------------------------------------------------------------------------
// <ZZ> Some macros to do simple stuff...
extern float    global_depth_min;
extern GLfloat  fog_color[4];
extern GLdouble clip_equation[4];

#define display_clear_zbuffer()         { glClear(GL_DEPTH_BUFFER_BIT); }
#define display_clear_buffers()         { glClearColor(color_temp[0]*INV_0xFF, color_temp[1]*INV_0xFF, color_temp[2]*INV_0xFF, 1.0);  if(volumetric_shadows_on) { glClearStencil(8);  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); } else { glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); } }
#define display_swap()                  { SDL_GL_SwapBuffers(); }
#define display_viewport(x, y, w, h)    { glViewport(x, y, w, h); }
#define display_zbuffer_on()            { glEnable(GL_DEPTH_TEST);  glDepthMask(ktrue);  glDepthFunc(GL_LEQUAL); }
#define display_zbuffer_shadow()        { glEnable(GL_DEPTH_TEST);  glDepthMask(kfalse); glDepthFunc(GL_GEQUAL); }
#define display_zbuffer_off()           { glDisable(GL_DEPTH_TEST); glDepthMask(kfalse); }
#define display_zbuffer_write_on()      { glDepthMask(ktrue); }
#define display_zbuffer_write_off()     { glDepthMask(kfalse); }
#define display_cull_on()               { glEnable(GL_CULL_FACE); glFrontFace(GL_CW); }
#define display_cull_off()              { glDisable(GL_CULL_FACE); }
#define display_cull_frontface()        { glEnable(GL_CULL_FACE); glFrontFace(GL_CCW); }
#define display_shade_on()              { glShadeModel(GL_SMOOTH); }
#define display_shade_off()             { glShadeModel(GL_FLAT); }
#define display_paperdoll_on()          { glEnable(GL_ALPHA_TEST); }
#define display_paperdoll_func(ALPHA)   { glAlphaFunc(GL_GREATER, ALPHA*INV_UINT08_SIZE); }
#define display_paperdoll_off()         { glDisable(GL_ALPHA_TEST); }
//#define display_blend_mult()            { glEnable(GL_BLEND); glBlendFunc(GL_DST_COLOR, GL_SRC_COLOR); }
//#define display_blend_mult()            { glEnable(GL_BLEND); glBlendFunc(GL_DST_COLOR, GL_ZERO); }
//#define display_blend_supermult()       { glEnable(GL_BLEND); glBlendFunc(GL_DST_COLOR, GL_SRC_ALPHA); }
#define display_blend_trans()           { glEnable(GL_BLEND); glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); }
#define display_blend_double()          { glEnable(GL_BLEND); glBlendFunc(GL_DST_COLOR, GL_ONE); }
#define display_blend_light()           { glEnable(GL_BLEND); glBlendFunc(GL_SRC_ALPHA, GL_ONE); }
#define display_blend_off()             { glDisable(GL_BLEND); }
#define display_depth_scene()           { glDepthRange(0.5005f, 1.0f); }
//#define display_depth_shadow()          { glDepthRange(0.5f, 0.9995f); }
//#define display_depth_backface()        { glDepthRange(0.5010f, 1.0f); }

#ifdef DEVTOOL
#define display_depth_layer(LAYER)  { };
#else
#define display_depth_layer(LAYER)  { glDepthRange(0.5006f - (LAYER*0.0002f), 1.0f); }
#endif

#define display_depth_overlay(D_MIN,D_MAX) { glDepthRange((D_MIN)* (0.5f/CAST(float,MAX_WINDOW)), (D_MAX) * (0.5f/CAST(float,MAX_WINDOW))); }
#define display_texture_on()            { glEnable(GL_TEXTURE_2D); }
#define display_texture_off()           { glDisable(GL_TEXTURE_2D); }
#define display_pick_texture(X)         { glBindTexture(GL_TEXTURE_2D, X); }
#define display_start_strip()           { glBegin(GL_TRIANGLE_STRIP); }
#define display_start_fan()             { glBegin(GL_TRIANGLE_FAN); }
#define display_start_points()          { glBegin(GL_POINTS); }
#define display_color(PTR)              { glColor3ubv(PTR); }
#define display_texpos(PTR)             { glTexCoord2fv(PTR); }
#define display_vertex(PTR)             { glVertex3fv(PTR); }
#define display_point(PTR)              { glVertex2fv(PTR); }
#define display_perspective_off()       { glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST); }
#define display_perspective_on()        { glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); }
#define display_end()                   { glEnd(); }
#define display_line_size(S)            { glLineWidth((float) S); }
#define display_point_size(S)           { glPointSize((float) S); }
#define display_fog_on()                { glEnable(GL_FOG); glFogf(GL_FOG_MODE, GL_LINEAR); glFogfv(GL_FOG_COLOR, fog_color); glFogf(GL_FOG_START, 1500.0f); glFogf(GL_FOG_END, MAX_TERRAIN_FOG_DISTANCE); }
#define display_fog_off()               { glDisable(GL_FOG); }
#define display_clip_on()               { glClipPlane(GL_CLIP_PLANE0, clip_equation); glEnable(GL_CLIP_PLANE0); }
#define display_clip_off()              { glDisable(GL_CLIP_PLANE0); }
#define display_flip_z()                { glScalef(1.0, 1.0, -1.0); }
#define display_draw_on()               { glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE); }
#define display_draw_off()              { glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); }



#define display_stencilbuffer_on()      { glEnable(GL_STENCIL_TEST); glStencilMask(255); } // glEnable(GL_STENCIL);}
#define display_stencilbuffer_off()     { glDisable(GL_STENCIL_TEST); } // glDisable(GL_STENCIL); }
#define display_stencilbuffer_back()    { glStencilOp(GL_KEEP, GL_KEEP, GL_INCR); glStencilFunc(GL_ALWAYS, 0, 255); }  // For drawing backfaces of volumetric shadows...  Increment stencil buffer...
#define display_stencilbuffer_front()   { glStencilOp(GL_KEEP, GL_KEEP, GL_DECR); glStencilFunc(GL_ALWAYS, 0, 255);  } // For drawing frontfaces of volumetric shadows...  Decrement stencil buffer...
#define display_stencilbuffer_shadow(BIAS)  { glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP); glStencilFunc(GL_NOTEQUAL,  BIAS, 255);  } // For drawing final shadow over entire screen...  Only passes if stencil buffer equals BIAS...



#define display_start_line()            { glBegin(GL_LINES); }
#define display_start_line_loop()       { glBegin(GL_LINE_LOOP); }
#define display_vertex_xy(X, Y)         { glVertex2f(X, Y); }
#define display_vertex_xyz(X, Y, Z)     { glVertex3f(X, Y, Z); }
#define display_texpos_xy(X, Y)         { glTexCoord2f(X, Y); }
#define display_color_alpha(PTR)        { glColor4ubv(PTR); }


extern Uint8 line_mode;            // Global control for cartoon lines
extern float initial_camera_matrix[16];        // A matrix to speed up/simplify 3D drawing routines
extern float rotate_camera_matrix[16];         // A matrix to speed up/simplify 3D drawing routines
extern float window_camera_matrix[16];         // A matrix to setup window drawing routines
extern float onscreen_matrix[16];              // A matrix for figurin' out onscreen point locations...
extern float modelview_matrix[16];             // A matrix for figurin' out onscreen point locations...
extern float rotate_enviro_matrix[6];          // Enviromap mini matrix...

extern Sint16 screen_shake_timer;
extern float screen_shake_amount;
extern float screen_frustum_x;
extern float screen_frustum_y;

//-----------------------------------------------------------------------------------------------
// <ZZ> This macro draws a partially transparent rectangle from the given coordinates.
#define display_highlight(x, y, xr, yb)                                         \
    {                                                                           \
        display_texture_off();                                                  \
        display_color_alpha(white);                                             \
        display_start_fan();                                                    \
        {                                                                       \
            display_vertex_xy(x, y);                                            \
            display_vertex_xy(xr, y);                                           \
            display_vertex_xy(xr, yb);                                          \
            display_vertex_xy(x, yb);                                           \
        }                                                                       \
        display_end();                                                          \
        display_texture_on();                                                   \
    }

//-----------------------------------------------------------------------------------------------
// <ZZ> This macro draws a 2D image...
#define display_image(tlx, tly, brx, bry, image_number)             \
    {                                                                   \
        display_pick_texture(image_number);                             \
        display_start_fan();                                            \
        {                                                               \
            display_texpos_xy(0, 0);  display_vertex_xy(tlx, tly);      \
            display_texpos_xy(1, 0);  display_vertex_xy(brx, tly);      \
            display_texpos_xy(1, 1);  display_vertex_xy(brx, bry);      \
            display_texpos_xy(0, 1);  display_vertex_xy(tlx, bry);      \
        }                                                               \
        display_end();                                                  \
    }
