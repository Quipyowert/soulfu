// <ZZ> This file contains functions to control the main data file (datafile.sdf).
//  **  sdf_archive_get_filename        - Gets the file_name and type of a given file (helper)
//  **  sdf_archive_unload              - Frees memory, automatically called via atexit()
//  **  sdf_archive_load                - Loads datafile.sdf into memory
//  **  sdf_archive_decompose_filename        - Turns "TROLL.JPG" into "TROLL" and SDF_FILE_IS_JPG (helper)
//  **  sdf_archive_find_header          - Returns a pointer to a given file's index information (helper)
//  **  sdf_archive_find_filetype       - Like find_index, but args are like "TROLL" and SDF_FILE_IS_JPG
//  **  sdf_archive_delete_file         - Deletes a file from the memory version of datafile.sdf
//  **  sdf_archive_add_file            - Adds a file into the memory version of datafile.sdf
//  **  sdf_archive_add_new_file            - Adds new a file into the memort version of datafile.sdf
//  **  sdf_archive_save                - Saves the memory version of datafile.sdf to disk
//  **  sdf_archive_export_file         - Exports a given file to disk
//  **  sdf_archive_stream_open                - Opens a .TXT file in the datafile for reading
//  **  stream_text_read_line           - Reads the next line of an open .TXT file
//  **  sdf_archive_set_flags            - Sets a given flag for a single file
//  **  sdf_archive_flag_clear          - Clears a given flag for all of the files
//  **  sdf_archive_delete_all_files    - Deletes all files of a given type
//  **  sdf_archive_find_index_by_data  - Looks for a file index, given a pointer to the file's data start...
//  **  sdf_data_checksum            - Generates a checksum for a piece of data
//  **  _stream_insert_data_mem         - Adds some data to a file, or removes some data (if to_add < 0)

#include "logfile.h"
#include "display.h"

#include "sdf_archive.h"

#include "soulfu_common.h"
#include "soulfu_config.h"

#include "soulfu.h"
#include "random.h"

#include "soulfu_math.inl"
#include "soulfu_endian.inl"

#include <memory.h>
#include <assert.h>
#include <stdio.h>


#define STB_DEFINE
#include "stb.h"

unsigned int sdf_file_name_hash(char *str)
{
    unsigned int hash = 0;
    unsigned int i = 0;
    while (*str && i < 8)
    {
        hash = (hash << 7) + (hash >> 25) + *str++;
        i++;
    }
    return hash + (hash >> 16);
}

#define SDF_KEY_CMP(str1, str2) \
    !strncmp(str1, str2, 8)

stb_declare_hash(STB_noprefix, pheadermap, pheadermap_, char *, SDF_PHEADER);
stb_define_hash_base(STB_noprefix, pheadermap, void*arena;, pheadermap_, pheadermapinternal_, 0.85f,
        char *, NULL, STB_SDEL, stb_sdict__copy, stb_sdict__dispose,
        STB_safecompare, SDF_KEY_CMP, STB_equal, return sdf_file_name_hash(k);,
        SDF_PHEADER, STB_nullvalue, NULL);

pheadermap *pheadermap_array[SDF_FILETYPE_COUNT];

SDF_PHEADER _sdf_archive_get_free_header();

#define SDF_FILENAME_COUNT 8
#define SDF_FILENAME_SIZE  (SDF_FILENAME_COUNT+1)

//===============================================================================================
// "compressed" data types for external storage
#pragma pack(push,1)

struct sdf_ext_archive_header_t
{
    str16 lines[4];
} SET_PACKED();
typedef struct sdf_ext_archive_header_t EXT_SDF_ARCHIVE_HEADER, *EXT_SDF_ARCHIVE_PHEADER;

struct sdf_ext_header_t
{
    Uint32   _file_offset;
    union
    {
        struct
        {
            Uint8    _info_byte;
            Uint8    junk[3];
        };
        Uint32 _size32;
    };
    str08     _fname;
} SET_PACKED();
typedef struct sdf_ext_header_t EXT_SDF_HEADER, *EXT_SDF_PHEADER;

#pragma pack(pop)

//-----------------------------------------------------------------------------------------------
// "uncompressed" data types for internal storage

struct sdf_header_t
{
    SDF_PDATA     data;
    Uint32        offset;
    SDF_FILE_TYPE type;
    Uint8         flags;
    size_t        size;
    size_t        allocated;
    char          fname[SDF_FILENAME_SIZE];
};
typedef struct sdf_header_t SDF_HEADER;
//typedef SDF_HEADER * SDF_PHEADER;         // this is already defined

//-----------------------------------------------------------------------------------------------

SDF_EXTENSION_INFO sdf_extensions[] =
{
    {"BAD", SDF_FILE_IS_UNUSED, kfalse, kfalse, 0x00000000 },
    {"TXT", SDF_FILE_IS_UNUSED, ktrue,  ktrue,  TEXT_SIZE  },
    {"JPG", SDF_FILE_IS_RGB,    ktrue,  ktrue,  0x00000000 },
    {"OGG", SDF_FILE_IS_RAW,    ktrue,  ktrue,  0x00000000 },
    {"RGB", SDF_FILE_IS_UNUSED, kfalse, kfalse, 0x00000000 },
    {"RAW", SDF_FILE_IS_UNUSED, kfalse, kfalse, 0x00000000 },
    {"SRF", SDF_FILE_IS_UNUSED, ktrue,  ktrue,  0x00010000 },
    {"MUS", SDF_FILE_IS_UNUSED, ktrue,  ktrue,  0x00010000 },
    {"DAT", SDF_FILE_IS_UNUSED, ktrue,  ktrue,  0x00002000 },
    {"SRC", SDF_FILE_IS_INT,    kfalse, ktrue,  TEXT_SIZE  },
    {"RUN", SDF_FILE_IS_UNUSED, ktrue,  ktrue,  0x00000000 },
    {"PCX", SDF_FILE_IS_RGB,    ktrue,  ktrue,  0x00000000 },
    {"LAN", SDF_FILE_IS_UNUSED, ktrue,  ktrue,  0x00010000 },
    {"DDD", SDF_FILE_IS_RDY,    ktrue,  ktrue,  0x00000000 },
    {"RDY", SDF_FILE_IS_UNUSED, ktrue,  kfalse, 0x001F4000 },
    {"INT", SDF_FILE_IS_RUN,    kfalse, ktrue,  0x00000000 },
    {"\0\0\0", SDF_FILE_IS_UNUSED, kfalse, kfalse, 0x00000000 }
};

//SDF_STREAM sdf_stream =  {NULL, NULL, NULL, 0, kfalse };

//-----------------------------------------------------------------------------------------------
typedef struct sdf_archive_info_t
{
    SDF_PHEADER     header_list;      // The datafile index after it has been loaded into memory
    EXT_SDF_PHEADER _ext_list;        // A (private) in-memory copy of the datafile index, as it existed on the disk


    bool_t       is_loaded;        // Did we load the datafile yet?
    Sint32       num_files;        // The number of files in the datafile after it has been loaded
    bool_t       can_save;         // Has the datafile been decompressed?
    bool_t       update_performed; // Do any files need decompressing?

    SDF_EXTENSION_INFO * ext;

    Uint16       free_index;
    Uint16*      free_list;

} SDF_INFO, *SDF_PINFO;

SDF_INFO sdf_info =
{
    NULL,          // index
    NULL,          // external index

    kfalse,        // is_loaded
    0,             // num_files
    // 2048,          // extra_files
    kfalse,        // can_save
    kfalse,        // update_performed

    NULL,          // ext

    0              // free_index
};

//===============================================================================================
//===============================================================================================
// "private" functions

static void pheadermap_array_init();
static void pheadermap_array_clear();
static void pheadermap_array_populate();
static pheadermap *pheadermap_array_create_entry();
static void pheadermap_array_delete_entry(pheadermap *d);
static void pheadermap_array_name_change(SDF_PHEADER pheader, const char *new_file_name);
static void pheadermap_array_type_change(SDF_PHEADER pheader, SDF_FILE_TYPE new_file_type);
static void pheadermap_array_set(SDF_FILE_TYPE file_type, const char *file_name, SDF_PHEADER pheader);
static SDF_PHEADER pheadermap_array_get(SDF_FILE_TYPE file_type, const char *file_name);

static bool_t _sdf_archive_allocate();
static void   _sdf_archive_deallocate();
static void   _sdf_archive_destroy();

//-----------------------------------------------------------------------------------------------
void pheadermap_array_populate()
{
    int i, ret;
    SDF_PHEADER pheader;
    SDF_FILE_TYPE file_type;
    char *file_name;

    // Now look through the pheader for a matching file_type and file_name
    repeat(i, sdf_info.num_files)
    {
        pheader = sdf_info.header_list + i;
        file_name = sdf_file_get_name(pheader);
        if (NULL != file_name)
        {
            file_type = sdf_file_get_type(pheader);

            if (pheadermap_array[file_type] == NULL)
                pheadermap_array[file_type] = pheadermap_array_create_entry();
            ret = pheadermap_set(pheadermap_array[file_type], file_name, pheader);
        }
    }
}

void pheadermap_array_init()
{
    int i;
    repeat(i, SDF_FILETYPE_COUNT)
    {
        pheadermap_array[i] = NULL;
    }
}

void pheadermap_array_clear()
{
    int i;
    repeat(i, SDF_FILETYPE_COUNT)
    {
        if (pheadermap_array[i] != NULL)
        {
            pheadermap_array_delete_entry(pheadermap_array[i]);
            pheadermap_array[i] = NULL;
        }
    }
}

pheadermap *pheadermap_array_create_entry()
{
    pheadermap *d = pheadermap_create();
    d->arena = stb_malloc_global(1);
    return d;
}

void pheadermap_array_delete_entry(pheadermap *d)
{
   if (d->arena)
      stb_free(d->arena);
   pheadermap_destroy(d);
}

void pheadermap_array_name_change(SDF_PHEADER pheader, const char *new_file_name)
{
    SDF_FILE_TYPE file_type = sdf_file_get_type(pheader);
    char *file_name = sdf_file_get_name(pheader);

    if (NULL != file_name && 0 != strlen(file_name))
        pheadermap_remove(pheadermap_array[file_type], file_name, NULL);
    pheadermap_array_set(file_type, new_file_name, pheader);
}

void pheadermap_array_type_change(SDF_PHEADER pheader, SDF_FILE_TYPE new_file_type)
{
    SDF_FILE_TYPE file_type = sdf_file_get_type(pheader);
    char *file_name = sdf_file_get_name(pheader);

    if (NULL != file_name)
        pheadermap_remove(pheadermap_array[file_type], file_name, NULL);
    pheadermap_array_set(new_file_type, file_name, pheader);
}

void pheadermap_array_set(SDF_FILE_TYPE file_type, const char *file_name, SDF_PHEADER pheader)
{
    if (file_type != SDF_FILE_IS_UNUSED && file_name != NULL)
    {
        if (NULL == pheadermap_array[file_type])
            pheadermap_array[file_type] = pheadermap_array_create_entry();
        pheadermap_set(pheadermap_array[file_type], (char*)file_name, pheader);
    }
}

SDF_PHEADER pheadermap_array_get(SDF_FILE_TYPE file_type, const char *file_name)
{
    if (NULL != pheadermap_array[file_type])
        return pheadermap_get(pheadermap_array[file_type], (char*)file_name);
    return NULL;
}

//===============================================================================================
bool_t _sdf_archive_allocate()
{
    //if there was a blank SDF before, create the index
    if (NULL == sdf_info.header_list)
    {
        // Get memory for the file index, 16 bytes per entry + some extra...
        log_info(0, "sdf_archive - first allocation of the file header list");

        sdf_info.header_list = (SDF_PHEADER    )calloc(sdf_info.num_files + SDF_EXTRA_INDEX, sizeof(SDF_HEADER)    );
        sdf_info._ext_list   = (EXT_SDF_PHEADER)calloc(sdf_info.num_files + SDF_EXTRA_INDEX, sizeof(EXT_SDF_HEADER));
        sdf_info.free_list   = (Uint16*)        calloc(sdf_info.num_files + SDF_EXTRA_INDEX, sizeof(Uint16)        );
    };

    return NULL != sdf_info.header_list;
};

//-----------------------------------------------------------------------------------------------
void _sdf_archive_deallocate()
{
    // Now get rid of the pheader
    log_info(0, "sdf_archive - deallocation of the file header list");

    if (NULL != sdf_info.header_list)
    {
        free(sdf_info.header_list);
        sdf_info.header_list = NULL;
    };

    if (NULL != sdf_info._ext_list)
    {
        free(sdf_info._ext_list);
        sdf_info._ext_list = NULL;
    };

    if (NULL != sdf_info.free_list)
    {
        free(sdf_info.free_list);
        sdf_info.free_list = NULL;
    };

};

//-----------------------------------------------------------------------------------------------
void _sdf_archive_destroy()
{
    int    i;
    SDF_PDATA memory;
    SDF_PHEADER pheader;

    // Go through each file in the pheader, and get rid of its memory
    repeat(i, sdf_info.num_files)
    {
        pheader = sdf_info.header_list + i;

        if (SDF_FILE_IS_UNUSED != sdf_file_get_type(pheader))
        {
            memory = sdf_file_get_data(pheader);

            if (NULL != memory)
            {
                free(memory);
                sdf_file_set_data(pheader, NULL);
            }
        }
    }

    _sdf_archive_deallocate();
}

//===============================================================================================

#define OBFUSCATEA ((Uint8) 97)
#define OBFUSCATEB ((Uint8) 11)
#define OBFUSCATEC ((Uint8) 53)
#define OBFUSCATED ((Uint8) 37)

_INLINE void _obfuscate_ext_header(EXT_SDF_PHEADER pheader)
{
    int i;
    SDF_PDATA ptmp = (SDF_PDATA)pheader;

    for (i = 0; i < 4; i++)
    {
        *ptmp++ += OBFUSCATEA;
    };

    for (i = 0; i < 4; i++)
    {
        *ptmp++ += OBFUSCATEB;
    };

    for (i = 0; i < 8; i++)
    {
        *ptmp++ += OBFUSCATEC;
    };
}

//-----------------------------------------------------------------------------------------------
_INLINE void _unobfuscate_ext_header(EXT_SDF_PHEADER pheader)
{
    int i;
    SDF_PDATA ptmp = (SDF_PDATA)pheader;

    for (i = 0; i < 4; i++)
    {
        *ptmp++ -= OBFUSCATEA;
    };

    for (i = 0; i < 4; i++)
    {
        *ptmp++ -= OBFUSCATEB;
    };

    for (i = 0; i < 8; i++)
    {
        *ptmp++ -= OBFUSCATEC;
    };

}

//-----------------------------------------------------------------------------------------------
_INLINE void _obfuscate_mem(SDF_PDATA mem, size_t size)
{
    Uint32 i;

    for (i = 0; i < size; i++)
    {
        *mem++ += OBFUSCATED;
    };
}


//-----------------------------------------------------------------------------------------------
_INLINE void _unobfuscate_mem(SDF_PDATA mem, size_t size)
{
    Uint32 i;

    for (i = 0; i < size; i++)
    {
        *mem++ -= OBFUSCATED;
    };
}

//===============================================================================================
_INLINE Uint32 sdf_data_checksum(SDF_PDATA data, Sint32 size)
{
    // <ZZ> This function generates a checksum based on the file's data...
    UINT checksum;
    int    i, j;
    checksum = 0;
    i = 0;
    j = 0;

    while (i < size)
    {
        checksum ^= ((UINT) g_rand.table[*data]) << j;
        data++;
        i++;
        j++;

        if (j > 24)  j = 0;
    }

    return checksum;
}

//===============================================================================================
static int _sdf_archive_spew_archive_header(FILE * dst, SDF_PHEADER file_list, size_t file_count)
{
    SDF_PHEADER src;
    char sdf_filename[0x20];
    Uint32 i;
    int  files_to_write;

    // go to the beginning of the file
    rewind(dst);

    // Figure out how many files there are to write
    files_to_write = 0;
    repeat(i, file_count)
    {
        src = file_list + i;

        // Should this file be saved?
        if (SDF_FILE_IS_UNUSED == src->type) continue;

        if (!sdf_info.ext[src->type].should_save) continue;

        if (0 != (src->flags & SDF_FLAG_NO_SAVE)) continue;

        files_to_write++;
    }

    // Write the header
    fprintf(dst, "----------------");
    fprintf(dst, "SOULFU DATA FILE");
    sprintf(sdf_filename, "%d FILES         ", files_to_write);
    repeat(i, 16) { fprintf(dst, "%c", sdf_filename[i]); }
    fprintf(dst, "----------------");

    return files_to_write;
}

//===============================================================================================
static bool_t _sdf_archive_rip_headers(FILE * src, EXT_SDF_PHEADER ext_list, size_t file_count)
{
    size_t i;

    // Read the external header list
    fseek(src, sizeof(EXT_SDF_ARCHIVE_HEADER), SEEK_SET);

    if (file_count != fread(ext_list, sizeof(EXT_SDF_HEADER), file_count, src))
    {
        //soulfu_assert(kfalse);
        log_error(0, "ARCHIVE - could not read all headers from archive");
    }

    // Unobfuscate the pheader...
    repeat(i, file_count)
    {
        _unobfuscate_ext_header(ext_list + i);
    };

    return ktrue;

};

//-----------------------------------------------------------------------------------------------
static bool_t _sdf_archive_uncompress_headers(SDF_PHEADER un_dest, EXT_SDF_PHEADER src, size_t file_count)
{
    // <BB> : "uncompress" file list data on the disk to the structure used internally

    Uint32 i;

    // check
    //soulfu_assert(NULL != un_dest && NULL != src);
    if (NULL == un_dest || NULL == src || 0 == file_count) return kfalse;

    for (i = 0; i < file_count; i++)
    {
        // the un_dest->data field must be filled in after the file is loaded into memory
        un_dest->data      = NULL;
        un_dest->offset    = endian_read_mem_int32((SDF_PDATA) & (src->_file_offset));
        un_dest->size      = endian_read_mem_int32((SDF_PDATA) & (src->_size32)) & EXT_SIZE_24_MASK;
        un_dest->allocated = un_dest->size;
        un_dest->type      = (SDF_FILE_TYPE)(src->_info_byte & EXT_TYPE_MASK);
        un_dest->flags     = src->_info_byte & EXT_FLAGS_MASK;

        memcpy(un_dest->fname, src->_fname, SDF_FILENAME_SIZE);

        // advance the pointer
        un_dest++;
        src++;
    }

    return ktrue;
};

//-----------------------------------------------------------------------------------------------
static bool_t _sdf_archive_compress_headers(EXT_SDF_PHEADER dst, SDF_PHEADER un_src)
{
    // <BB> : "compress" the file list structures in memory and stream them to the disk

    int i, total_headers;

    //soulfu_assert(NULL != dst && NULL != un_src);
    if (NULL == dst || NULL == un_src) return kfalse;

    total_headers = sdf_info.num_files + sdf_info.free_index;

    for (i = 0; i < total_headers; i++, un_src++, dst++)
    {
        // do an error check on the file size
        //soulfu_assert(un_src->size < EXT_SIZE_24_MASK);

        // convert/compress the data
        endian_write_mem_int32((SDF_PDATA)&(dst->_file_offset), 0);
        endian_write_mem_int32((SDF_PDATA)&(dst->_size32), sdf_file_get_size(un_src));
        dst->_info_byte = sdf_file_get_info_byte(un_src);
        memcpy(dst->_fname, sdf_file_get_name(un_src), SDF_FILENAME_COUNT);
    }

    return ktrue;
};

//-----------------------------------------------------------------------------------------------
static int _sdf_archive_spew_headers(FILE * dst, EXT_SDF_PHEADER ext_list)
{
    size_t file_offset;
    EXT_SDF_PHEADER src;
    int i, total_headers, num;

    // Write the index
    fseek(dst, sizeof(EXT_SDF_ARCHIVE_HEADER), SEEK_SET);
    file_offset   = 0;
    num = 0;
    total_headers = sdf_info.num_files + sdf_info.free_index;
    repeat(i, total_headers)
    {
        src = ext_list + i;

        // Should this file be saved?
        if (SDF_FILE_IS_UNUSED == (src->_info_byte & EXT_TYPE_MASK)) continue;

        if (!sdf_info.ext[(src->_info_byte & EXT_TYPE_MASK)].should_save) continue;

        if (0 != (src->_info_byte & SDF_FLAG_NO_SAVE)) continue;

        // set the file offset the offset
        endian_write_mem_int32( (Uint8*)&(src->_file_offset), file_offset);
        file_offset += endian_read_mem_int32( (Uint8*) & (src->_size32) ) & EXT_SIZE_24_MASK;

        _obfuscate_ext_header(src);
        fwrite(src, sizeof(EXT_SDF_HEADER), 1, dst);
        //_unobfuscate_ext_header(src);

        num++;
    }


    return num;
};

//===============================================================================================
static bool_t _sdf_archive_rip_files(FILE * src, SDF_PHEADER header_list, size_t file_count)
{
    size_t i;
    SDF_PHEADER pheader;

    repeat(i, file_count)
    {
        pheader = header_list + i;

#ifdef DEVTOOL

        // in DEV mode, allocate some extra memory
        if (pheader->size < sdf_info.ext[pheader->type].min_size)
        {
            pheader->allocated = sdf_info.ext[pheader->type].min_size;
        };

#endif

        pheader->data = (SDF_PDATA)calloc(pheader->allocated, sizeof(Uint8));

        if (NULL == pheader->data)
        {
            log_error(0, "ARCHIVE - Out of memory");
            goto _sdf_archive_rip_files_fail;
        }

        // Log some information...
        log_info(1, "sdf_archive - Allocated %d (%d) bytes for %s.%s", pheader->allocated, pheader->size, pheader->fname, sdf_info.ext[pheader->type].extension);

        // Read the main file data
        fseek(src, sizeof(EXT_SDF_ARCHIVE_HEADER) + file_count*sizeof(EXT_SDF_HEADER) + sizeof(str16) + pheader->offset, SEEK_SET);

        if (1 != fread(pheader->data, pheader->size, 1, src))
        {
            log_error(0, "ARCHIVE - Could not read data for file number %d (%s.%s) ", i, pheader->fname, sdf_info.ext[pheader->type].extension);
            goto _sdf_archive_rip_files_fail;
        }

        // Unobfuscate the file data...
        _unobfuscate_mem(pheader->data, pheader->size);
    }


    return ktrue;

_sdf_archive_rip_files_fail:

    // Don't bother to load the rest, just mark 'em as unused
    while (i < file_count)
    {
        sdf_file_set_unused(pheader);
        i++;
    }

    // Now unload all of the data
    sdf_archive_unload();

    return kfalse;
}

//-----------------------------------------------------------------------------------------------
static int _sdf_archive_spew_files(FILE * dst, SDF_PHEADER file_list)
{
    SDF_PHEADER src;
    int i, total_headers, num;

    num = 0;
    total_headers = sdf_info.num_files + sdf_info.free_index;
    repeat(i, total_headers)
    {
        src = file_list + i;

        // Should this file be saved?
        if (SDF_FILE_IS_UNUSED == src->type) continue;

        if (!sdf_info.ext[src->type].should_save) continue;

        if (0 != (src->flags & SDF_FLAG_NO_SAVE)) continue;

        // Write the data
        _obfuscate_mem(src->data, src->size);
        fwrite(src->data, 1, src->size, dst);
        _unobfuscate_mem(src->data, src->size);

        num++;
    }

    return num;
};












//===============================================================================================
//===============================================================================================
bool_t sdf_archive_get_filename(Sint32 num, char* file_name, Uint8* file_infobyte)
{
    // <ZZ> This function gets the file_name and type of a file that has been loaded into datafile
    //      memory.  The function returns ktrue and fills in file_name and file_type if it
    //      works, or it returns kfalse if it broke.  Index is the actual file number, starting
    //      from 0.  The file_name buffer should be SDF_FILENAME_SIZE characters long, while the file_type buffer
    //      should be a single character.  NULL can be passed to either file_name or file_type
    //      if it isn't needed.

    SDF_PHEADER pheader;

    //make sure we have a non-empty archive
    if (NULL == sdf_info.header_list)
    {
        log_error(0, "sdf_archive_get_filename() called with invalid archive");
        return kfalse;
    };

    if (num >= sdf_info.num_files)
    {
        log_error(0, "sdf_archive_get_filename() called with invalid index %d", sdf_info.num_files);
        return kfalse;
    };

    pheader = sdf_archive_get_header(num);

    return sdf_file_get_file_info(pheader, file_name, file_infobyte);
}

//-----------------------------------------------------------------------------------------------
void sdf_archive_unload(void)
{
    // <ZZ> This function tries to free() any memory taken up by the datafile.  It should be
    //      run automatically at program termination.

    pheadermap_array_clear();

    if (NULL != sdf_info.header_list)
    {
        _sdf_archive_destroy();
    };

    if (sdf_info.is_loaded) log_info(1, "ARCHIVE -  archive unloaded correctly");

    sdf_info.is_loaded = kfalse;
    sdf_info.can_save  = kfalse;
    sdf_info.ext       = NULL;
}

//-----------------------------------------------------------------------------------------------
void sdf_archive_flag_clear(Uint8 flag)
{
    // <ZZ> This function clears a given flag for all of the files...  Flags may be or'd for
    //      multiple flags...
    int    i;
    SDF_PHEADER pheader;

    //make sure we have a non-empty archive
    if (NULL == sdf_info.header_list)
    {
        log_error(0, "sdf_archive_flag_clear() called with invalid archive");
        return;
    };

    // Go through each file in the pheader
    repeat(i, sdf_info.num_files)
    {
        pheader = sdf_info.header_list + i;

        sdf_file_remove_flags(pheader, flag);
    }
}

//-----------------------------------------------------------------------------------------------
void sdf_archive_set_save(bool_t can_save)
{
    if (sdf_info.is_loaded)
        sdf_info.can_save = can_save;
};

//-----------------------------------------------------------------------------------------------
bool_t sdf_archive_create(Uint32 num_files)
{
    // <BB> This function "creates" an empty sdf archive in memory

    bool_t retval = kfalse;

    //unload any previous sdf archive
    sdf_archive_unload();

    if (!sdf_info.is_loaded)
    {
        //stream_init( sdf_archive_get_pstream() );
        //sdf_stream_init(&sdf_stream);

        sdf_info.num_files   = num_files;        // The number of files in the datafile after it has been loaded
        //sdf_info.extra_files = SDF_EXTRA_INDEX;  // The number of files we can still add
        sdf_info.can_save    = kfalse;           // Has the datafile been decompressed?

        retval = _sdf_archive_allocate();

        // set the free list. insert them in reverse order, so that the LIFO structure sdf_info.free_list
        // will allocate new headers starting from the lowest indices
        repeat(sdf_info.free_index, SDF_EXTRA_INDEX)
        {
            sdf_info.free_list[sdf_info.free_index] = sdf_info.num_files + (SDF_EXTRA_INDEX - 1) - sdf_info.free_index;
        };

        sdf_info.ext = sdf_extensions;
    };

    pheadermap_array_init();

    return retval;
};


//-----------------------------------------------------------------------------------------------
bool_t sdf_archive_load(const char *archive_filename)
{
    // <ZZ> This function tries to load archive_filename.sdf by checking its size, checking the header,
    //      allocating memory, and reading all of the data.  It also registers sdf_archive_unload to
    //      run atexit() to free up the memory.  The function returns kfalse on an error, ktrue
    //      if everything loaded okay.  If it worked, several global variables should also be
    //      set, indicating the number of files and stuff like that.

    EXT_SDF_ARCHIVE_HEADER header;

    Sint32 num_files;
    Sint32 file_size;
    FILE *openfile;
    bool_t retval = kfalse;

    // Open the data file in binary mode
    log_info(1, "ARCHIVE -  Trying to open \"%s\"", archive_filename);

    openfile = fopen(archive_filename, "rb");

    if (NULL == openfile)
    {
        log_error(0, "ARCHIVE - Unable to open \"%s\"", archive_filename);
        return kfalse;
    }

    // Check how big our data file is...
    fseek(openfile, 0, SEEK_END);
    file_size = ftell(openfile);
    fseek(openfile, 0, SEEK_SET);

    if (file_size <= sizeof(EXT_SDF_ARCHIVE_HEADER))
    {
        log_error(0, "ARCHIVE - File \"%s\" is too small", archive_filename);
        return kfalse;
    }

    log_info(1, "ARCHIVE -  File is %d (0x%X) bytes", file_size, file_size);

    // Read the header of the archive_filename
    if (sizeof(EXT_SDF_ARCHIVE_HEADER) != fread(&header, 1, sizeof(EXT_SDF_ARCHIVE_HEADER), openfile))
    {
        log_error(0, "ARCHIVE - Header could not be read");
        retval = kfalse;
        goto sdf_archive_load_exit;
    }

    // Check the first two lines (0x1F bytes) of the header to make sure it's okay
    if (0 != strncmp(header.lines[0], "----------------", sizeof(str16)) ||
            0 != strncmp(header.lines[1], "SOULFU DATA FILE", sizeof(str16)))
    {
        log_error(0, "ARCHIVE - Header invalid");
        retval = kfalse;
        goto sdf_archive_load_exit;
    };


    // Get the number of files from the third line
    sscanf(header.lines[2], "%d", &num_files);

    log_info(1, "ARCHIVE -  %d files in archive", num_files);


    if (num_files <= 0)
    {
        log_error(0, "ARCHIVE - No files in \"%s\"", archive_filename);
        retval = kfalse;
        goto sdf_archive_load_exit;
    }

    if ( !sdf_archive_create(num_files) )
    {
        log_error(0, "ARCHIVE - Out of memory when creating file header list");
        retval = kfalse;
        goto sdf_archive_load_exit;
    }


    // actually load the archive into memory
    {
        // Read in the data for the file header list
        _sdf_archive_rip_headers(openfile, sdf_info._ext_list, num_files);

        // convert the headers over to the internal format
        _sdf_archive_uncompress_headers(sdf_info.header_list, sdf_info._ext_list, num_files);

        // Read the data for each file in the header list...
        _sdf_archive_rip_files(openfile, sdf_info.header_list, num_files);

        sdf_info.is_loaded = ktrue;
    }

    //  repeat(i, num_files)
    //  {
    //    pheader = sdf_info.header_list + i;
    //
    //    // Get memory for this file data
    //    datasize = sdf_file_get_size(pheader);
    //    offset   = sdf_file_get_offset(pheader);
    //    sdf_archive_get_filename(i, file_name, &file_infobyte);
    //    file_type = file_infobyte & EXT_TYPE_MASK;
    //    allocsize = datasize;
    //#ifdef DEVTOOL
    //    // Allocate extra memory for certain file types, so we can edit 'em...
    //    if (datasize < sdf_info.ext[file_type].min_size)  allocsize = sdf_info.ext[file_type].min_size;
    //#endif
    //    filedata = (SDF_PDATA)calloc(allocsize, sizeof(Uint8));
    //    if (filedata)
    //    {
    //      // Log some information...
    //      //log_info(0, "sdf_archive - Allocated %d (%d) bytes for %s.%s", allocsize, datasize, file_name, sdf_info.ext[file_type].extension);
    //
    //
    //      // Read the main file data
    //      fseek(openfile, sizeof(EXT_SDF_ARCHIVE_HEADER) + num_files*sizeof(EXT_SDF_HEADER) + sizeof(str16) + offset, SEEK_SET);
    //      fread(filedata, datasize, 1, openfile);
    //
    //
    //      // Unobfuscate the file data...
    //      repeat(j, datasize)
    //      {
    //        filedata[j] -= OBFUSCATED;
    //      }
    //
    //      // Replace the 4 byte location offset with an actual address
    //      sdf_file_set_data(pheader, filedata);
    //    }
    //    else
    //    {
    //      log_error(0, "ARCHIVE - Out of memory when loading DATAFILE.SDF");
    //      // Don't bother to load the rest, just mark 'em as unused
    //      while (i < num_files)
    //      {
    //        sdf_file_set_unused(pheader);
    //        i++;
    //      }
    //      // Now unload all of the data
    //      sdf_archive_unload();
    //    }
    //  }

    if (sdf_info.is_loaded)
    {
        retval = ktrue;

        // Register the unload function to be called on exit
        atexit(sdf_archive_unload);

        // Everything worked okay
        sdf_info.can_save = ktrue;

        // Just in case flags are messed up...
        sdf_archive_flag_clear(SDF_FLAG_WAS_UPDATED);

        pheadermap_array_populate();
    }


sdf_archive_load_exit:

    fclose(openfile);

    return retval;
}

//-----------------------------------------------------------------------------------------------
bool_t sdf_archive_decompose_filename(const char* file_name, char* newfile_name, SDF_FILE_TYPE* newfile_type)
{
    // <ZZ> This function takes a standard 8.3 file_name (ie. "TROLL.JPG") and splits it into
    //      a new 8 character name ("TROLL") (SDF_FILENAME_SIZE with null on the end) and a file_type
    //      (SDF_FILE_IS_JPG).  The function then tries to write the results into newfile_name
    //      and newfile_type.  If it can't figure out what a file_type is (ie. extension of ".BAD")
    //      or it otherwise fails, it returns kfalse.  It returns ktrue if it manages to work
    //      correctly.  Note that if the file name is longer than 8.3, the function tries to
    //      slice the name down.  newfile_name should be SDF_FILENAME_SIZE characters long, newfile_type should
    //      be 1 character.


    char extension[4] = EMPTY_STRING;
    const char *psrc;
    char *pdst, *pdst_end;
    int    i, j;

    // do the default
    newfile_name[0] = EOS;
    *newfile_type   = SDF_FILE_IS_UNUSED;

    // copy the root file_name
    pdst     = newfile_name;
    pdst_end = pdst + SDF_FILENAME_SIZE;
    psrc     = file_name;
    for ( /*nothing*/; pdst < pdst_end && ('.' != *psrc) && (EOS != *psrc); psrc++, pdst++)
    {
        *pdst = toupper(*psrc);
    };
    newfile_name[SDF_FILENAME_COUNT] = EOS;

    // is the original filename valid?
    if (pdst == pdst_end && ('.' != *psrc))
    {
        return kfalse;
    };

    // copy the extension
    psrc++;
    pdst = extension;
    for (j = 0; j < 3; j++)
    {
        *pdst++ = toupper(*psrc++);
    };
    *pdst++ = EOS;


    // Check the extension against known types
    repeat(i, SDF_FILETYPE_COUNT)
    {
        if (0 == strncmp(extension, sdf_info.ext[i].extension, 3))
        {
            *newfile_type = i;
            break;
        }
    }

    // Did we find a valid extension?
    return (SDF_FILE_IS_UNUSED != *newfile_type);
}

//-----------------------------------------------------------------------------------------------
SDF_PHEADER sdf_archive_find_index_ext(const char* file_name, SDF_FILE_TYPE file_type)
{
    // <ZZ> This function looks in the sdf_info.header_list for a file based on the given file_name.  If it
    //      finds a matching entry, it returns a pointer to the start of that entry's 16 byte
    //      block (4 byte location, 1 byte flag, 3 byte size, 8 byte name).  If not, it returns
    //      NULL.  Note that the extension to the file_name must be one of the valid types...

    //int    i;
    // SDF_PHEADER pheader;

    //make sure we have a non-empty archive
    if (NULL == sdf_info.header_list)
    {
        log_error(0, "sdf_archive_find_header() called with invalid archive");
        return NULL;
    };

    return pheadermap_array_get(file_type, file_name);

    /*
    // Now look through the pheader for a matching file_type and file_name
    repeat(i, sdf_info.num_files)
    {
        pheader = sdf_info.header_list + i;

        // First look at filetypes
        if (file_type == sdf_file_get_type(pheader))
        {
            // Now check the name...
            if (0 == strncmp(sdf_file_get_name(pheader), file_name, SDF_FILENAME_COUNT))
            {
                // Looks like everything matches up, so we're done...
                return pheader;
            }
        }
    }

    return NULL;
    */
}


//-----------------------------------------------------------------------------------------------
SDF_PHEADER sdf_archive_find_header(const char* file_name)
{
    // <ZZ> This function looks in the sdf_info.header_list for a file based on the given file_name.  If it
    //      finds a matching entry, it returns a pointer to the start of that entry's 16 byte
    //      block (4 byte location, 1 byte flag, 3 byte size, 8 byte name).  If not, it returns
    //      NULL.  Note that the extension to the file_name must be one of the valid types...

    char          newfile_name[SDF_FILENAME_SIZE] = EMPTY_STRING;
    SDF_FILE_TYPE newfile_type;

    //make sure we have a non-empty archive
    if (NULL == sdf_info.header_list)
    {
        log_error(0, "sdf_archive_find_header() called with invalid archive");
        return NULL;
    };

    // Convert the file_name into an 8 character name and a 1 character extension
    if (sdf_archive_decompose_filename(file_name, newfile_name, &newfile_type))
    {
        return sdf_archive_find_index_ext(newfile_name, newfile_type);
    }

    return NULL;
}

//-----------------------------------------------------------------------------------------------
SDF_PHEADER sdf_archive_find_filetype(const char* file_name, SDF_FILE_TYPE searchtype)
{
    // <ZZ> This function looks in the sdf_info.header_list for a file based on the given file_name, without
    //      an extension.  The extension is given using the file_type.  If it finds a matching
    //      entry, it returns a pointer to the start of that entry's 16 byte block.  If not, it
    //      returns NULL.
    char        tmp_string[15];

    if (SDF_FILE_IS_UNUSED == searchtype) return NULL;


    // gcc is complaining about a stack corruption in this function.
    // maybe copying the filename of unknown length to a local variable will
    // prevent a buffer overflow
    if (NULL == file_name || '\0' == *file_name) return NULL;
    strncpy( tmp_string, file_name, SDL_arraysize(tmp_string));


    //make sure we have a non-empty archive
    if (NULL == sdf_info.header_list)
    {
        log_error(0, "sdf_archive_find_filetype() called with invalid archive");
        return NULL;
    };

    return pheadermap_array_get(searchtype, file_name);

    /*
    // Now look through the sdf_info.header_list for a matching file_type and file_name
    repeat(i, sdf_info.num_files)
    {
        pheader = sdf_info.header_list + i;

        // First look at filetypes
        if (searchtype != sdf_file_get_type(pheader)) continue;

        // Now check the name...
        if (0 == strncmp(sdf_file_get_name(pheader), tmp_string, SDF_FILENAME_COUNT)) return pheader;
    }

    return NULL;
    */
}

//-----------------------------------------------------------------------------------------------
SDF_PHEADER sdf_archive_create_header(SDF_PDATA mem, const char * filename, size_t size, SDF_FILE_TYPE type)
{
    SDF_PHEADER pheader;
    SDF_PDATA   pdata;
    size_t      alloc_size = size;

#if defined(DEVTOOL) && !defined(DEBUG_SRCDECODE)

    // Allocate extra memory for the modeler...
    if (alloc_size < (size_t) sdf_archive_get_ext(type)->min_size)
    {
        alloc_size = sdf_archive_get_ext(type)->min_size;
    }

#endif

    pdata = NULL;

    if (alloc_size > 0)
    {
        pdata = (SDF_PDATA)malloc(alloc_size);

        if (NULL == pdata)
        {
            //soulfu_assert(kfalse);
            return NULL;
        }

        // Log some information...
        log_info(1, "sdf_archive - Allocated %d (%d) bytes for \"%s\"", alloc_size, size, filename);
    };


    // Do we create a new pheader?
    pheader = _sdf_archive_get_free_header();

    if (NULL == pheader)
    {
        //soulfu_assert(kfalse);
        if (NULL != pdata) { free(pdata); pdata = NULL; }

        log_error(0, "ARCHIVE - Failed to create %s.%s", filename, sdf_info.ext[type].extension );

        return NULL;
    }

    log_info(1, "ARCHIVE -  creating %s.%s", filename, sdf_info.ext[type].extension );


    // Initialize the pheader...
    pheader->data      =  pdata;
    pheader->size      =  size;
    pheader->allocated =  alloc_size;
    pheader->type      =  type;
    pheader->flags     =  SDF_FLAG_WAS_CREATED;
    strncpy(pheader->fname, filename, SDF_FILENAME_SIZE);

    if (NULL == mem)
    {
        memset(pdata, 0, size);
    }
    else
    {
        memcpy(pdata, mem, size);
    };

    pheadermap_array_set(type, filename, pheader);

    return pheader;
};

//-----------------------------------------------------------------------------------------------
bool_t sdf_archive_delete_header(SDF_PHEADER pheader)
{
    // <ZZ> This function attempts to get rid of a file from the memory copy of datafile.sdf, by
    //      marking the file as having been deleted and by free()ing its memory.  It should
    //      return kfalse if there's been an error, or ktrue if the file was deleted correctly.

    SDF_PDATA  memory;
    UINT       index, error;

    if (pheader == NULL) return kfalse;

    log_info(1, "ARCHIVE - deleting %s.%s", sdf_file_get_name(pheader), sdf_info.ext[sdf_file_get_type(pheader)].extension );

    // Free up the file's memory block
    memory = sdf_file_get_data(pheader);

    if (NULL != memory) { free(memory); sdf_file_set_data(pheader, NULL); }

    // Now mark the file as being unused so it doesn't sdf_archive_save() with the other files
    sdf_file_set_unused(pheader);

    // calculate the index
    index = CAST(UINT, (Uint8*)pheader - (Uint8*)sdf_info.header_list) / CAST(UINT, (Uint8*)(sdf_info.header_list + 1) - (Uint8*)(sdf_info.header_list));
    error = CAST(UINT, (Uint8*)pheader - (Uint8*)sdf_info.header_list) % CAST(UINT, (Uint8*)(sdf_info.header_list + 1) - (Uint8*)(sdf_info.header_list));
    //soulfu_assert(0 == error);

    sdf_info.free_list[sdf_info.free_index] = index;
    sdf_info.free_index++;

    sdf_info.num_files--;

#ifdef DEVTOOL
    // reorganize the index
    //sdf_archive_optimize();
#endif

    return ktrue;
}

//-----------------------------------------------------------------------------------------------
bool_t sdf_archive_delete_file(const char* file_name, SDF_FILE_TYPE file_type)
{
    // <ZZ> This function attempts to get rid of a file from the memory copy of datafile.sdf, by
    //      marking the file as having been deleted and by free()ing its memory.  It should
    //      return kfalse if there's been an error, or ktrue if the file was deleted correctly.
    //      Note that the file_type to the file_name must be one of the valid types...
    SDF_PHEADER pheader;
    SDF_FILE_TYPE  derived;
    bool_t retval = kfalse;

    //do a recursive delete to remove all dependencies on this file
    derived = sdf_info.ext[file_type].derived;

    if (SDF_FILE_IS_UNUSED != derived)
    {
        sdf_archive_delete_file(file_name, derived);
    };

    // Find the file we're looking for...
    pheader = sdf_archive_find_index_ext(file_name, file_type);

    if (pheader != NULL)
    {
        retval = sdf_archive_delete_header(pheader);
    }

    return retval;
}

//-----------------------------------------------------------------------------------------------
SDF_PHEADER _sdf_archive_get_free_header()
{
    int i;
    SDF_PHEADER retval = NULL;

    //make sure that there is a valid index
    if ( _sdf_archive_allocate() )
    {
        // grab the header off the stack
        sdf_info.free_index--;
        i = sdf_info.free_list[sdf_info.free_index];

        retval = sdf_info.header_list + i;

        //bump the counters
        sdf_info.num_files++;
    };

    return retval;
};

////-----------------------------------------------------------------------------------------------
//bool_t sdf_archive_stream_open(const char* file_name)
//{
//  // <ZZ> This function opens a file in the database for reading line by line like a text
//  //      document.  It returns ktrue if the file was found, or kfalse if not.
//
//  SDF_PHEADER pheader = sdf_archive_find_header(file_name);
//
//  return (NULL != stream_open_header(sdf_archive_get_pstream(), pheader));
//}

//-----------------------------------------------------------------------------------------------
bool_t is_newline(int ascii, FILE * fp)
{
    bool_t retval = kfalse;

    if (0x0D == ascii || 0x0A == ascii)
    {
        retval = ktrue;

        // handle the possible extra character
        if (!feof(fp))
        {
            int ctmp = fgetc(fp);

            if (0x0D != ctmp && 0x0A != ctmp)
            {
                ungetc(ctmp, fp);
            }
        }
    }

    return retval;
};

//-----------------------------------------------------------------------------------------------
bool_t sdf_archive_add_file(const char* sdf_filename, const char* input_filename)
{
    // <ZZ> This function attempts to add a file from disk to the memory resident copy of
    //      datafile.sdf, by creating an pheader for the new file and requesting memory for
    //      its contents.  This function doesn't reuse space from deleted files or realloc
    //      more pheader space, because that would be a hassle.  So...  Only SDF_EXTRA_INDEX/16
    //      files may be added to the pheader before closing the program.  The function returns
    //      kfalse if something blew up, or ktrue if the file was added correctly.  Note that
    //      the extension to the sdf_filename must be one of the valid types...  Also note that
    //      trying to replace an existing sdf_filename may cause it to be sdf_archive_delete_file()'d.

    FILE* openfile;
    size_t file_size;

    SDF_PHEADER   pheader = NULL;
    SDF_FILE_TYPE file_type;
    Sint32        headersize;

    char  newfile_name[SDF_FILENAME_SIZE] = EMPTY_STRING;

    SDF_PDATA  memory;

    char readchar;
    size_t j;

    Sint32 line;

    bool_t retval = kfalse;

    if (NULL == input_filename) input_filename = sdf_filename;

    // is it a good file name?
    if (!sdf_archive_decompose_filename(sdf_filename, newfile_name, &file_type))
    {
        log_warning(1, "ARCHIVE -  File %s could not be added to the archive", input_filename);
        log_message(1, "Invalid filename or extension were specified", input_filename);
        return retval;
    }

    // Try to open the file on disk
    openfile = fopen(input_filename, "rb");

    if (NULL == openfile)
    {
        log_warning(1, "ARCHIVE -  File %s could not be added to the archive", input_filename);
        log_message(1, "It was not found on the disk", input_filename);
        return kfalse;
    }

    // Delete any existing files with the given name
    sdf_archive_delete_file(newfile_name, file_type);

    // Make sure we have room in the pheader for a new file
    if (sdf_info.free_index <= 0)
    {
        log_error(0, "ARCHIVE - No room left to add file, program must be restarted");
        return retval;
    }

    headersize = 0;
    file_size  = 0;

    if (SDF_FILE_IS_LAN == file_type)
    {
        // 0D 0A pairs are turned into just 0, and a 0 is appended to the end...

        // 4 bytes linecount, 4 byte address 0
        headersize += 2;

        while (!feof(openfile))
        {
            readchar = fgetc(openfile);

            if (is_newline(readchar, openfile))
            {
                headersize++;
            }

            file_size++;
        }

        file_size++;

        file_size += headersize << 2;
    }
    else if (SDF_FILE_IS_TXT == file_type || SDF_FILE_IS_SRC == file_type)
    {
        // 0D 0A pairs are turned into just 0, and a 0 is appended to the end...
        file_size = 0;

        while (!feof(openfile))
        {
            readchar = fgetc(openfile);

            is_newline(readchar, openfile);

            file_size++;
        }

        file_size++; // Extra byte for end 0
    }
    else
    {
        // Load the data exactly
        fseek(openfile, 0, SEEK_END);
        file_size = ftell(openfile);
    }

    // Go back to the start of the file...
    rewind(openfile);

    if (file_size <= 0)
    {
        log_error(0, "ARCHIVE - Filesize for \"%s\" seems incorrect", input_filename);
        goto sdf_archive_add_file_fail;
    }

    // Create a new pheader
    pheader = sdf_archive_create_header(NULL, newfile_name, file_size, file_type);

    if (NULL == pheader)
    {
        log_error(0, "ARCHIVE - Not enough memory to add file \"%s\"", input_filename);
        goto sdf_archive_add_file_fail;
    }

    // grab the memory
    memory = sdf_file_get_data(pheader);

    // Setup the header data for a language file...
    if (SDF_FILE_IS_LAN == file_type)
    {
        size_t txt_begin = headersize << 2;
        size_t hdr_begin = 4;
        line = 0;

        endian_write_mem_int32(memory, headersize - 1);
        endian_write_mem_int32(memory + hdr_begin + (line << 2), txt_begin); line++;

        // Parse out 0D 0A pairs and put a 0 on the end
        j = 0;

        while ( !feof(openfile) )
        {
            readchar = fgetc(openfile);

            if (feof(openfile)) break;

            if (is_newline(readchar, openfile))
            {
                // Save the address of the new line...
                endian_write_mem_int32(memory + hdr_begin + (line << 2), txt_begin + j + 1); line++;

                readchar = '\0';
            }
            else if (readchar == '\t')
            {
                readchar = ' ';
            }

            if (txt_begin + j < file_size)
            {
                *(memory + txt_begin + j) = readchar;
                j++;
            }
        };

        // make absolutely sure that there is a terminating EOS
        while (txt_begin + j < file_size)
        {
            *(memory + txt_begin + j) = EOS;
            j++;
        }
    }
    else if (SDF_FILE_IS_TXT == file_type || SDF_FILE_IS_SRC == file_type)
    {
        // Parse out 0D 0A pairs and put a 0 on the end

        j = 0;

        while ( !feof(openfile) )
        {
            readchar = fgetc(openfile);

            if (feof(openfile)) break;

            if (is_newline(readchar, openfile))
            {
                readchar = '\0';
            }
            else
            {
                if (readchar == '\t') readchar = ' ';
            }

            if (j < file_size) *(memory + j) = readchar;

            j++;
        }

        // make absolutely sure that there is a terminating EOS
        while (j < file_size)
        {
            *(memory + j) = EOS;
            j++;
        }
    }
    else
    {
        // Do a straight read...
        fread(memory, 4, file_size >> 2, openfile);
        fread(memory + (file_size&0xFFFFFFFC), 1, (file_size&3), openfile);
    }

    log_info(1, "ARCHIVE - Added \"%s\" to the datafile", sdf_filename);
    sdf_info.update_performed = ktrue;

    if (!sdf_info.is_loaded) sdf_info.is_loaded = ktrue;


    fclose(openfile);

    return ktrue;


sdf_archive_add_file_fail:

    fclose(openfile);

    return kfalse;

}

//-----------------------------------------------------------------------------------------------
bool_t sdf_archive_add_new_file(const char* file_name)
{
    // <ZZ> Makes a file on disk, then tries adding it in...
    FILE* openfile;

    openfile = fopen(file_name, "w");

    if (openfile)
    {
        fprintf(openfile, "New File\n");
        fclose(openfile);
        return sdf_archive_add_file(file_name, NULL);
    }

    return kfalse;
}

//-----------------------------------------------------------------------------------------------
bool_t sdf_archive_save(const char* archive_filename)
{
    // <ZZ> This function writes the memory copy of datafile.sdf to disk.  Files that have been
    //      marked for deletion in the index are skipped.  The function should return kfalse if
    //      there's a problem writing, or ktrue if it all worked.

    FILE* openfile;

    Uint32 total_num_files;
    Uint32 files_to_write;

    if (!sdf_info.is_loaded || !sdf_info.can_save)
    {
        log_error(0, "ARCHIVE - \"%s\" can't save before loading/after decompression", archive_filename);
        return kfalse;
    };

    // Open the file for writing
    openfile = fopen(archive_filename, "wb");

    if (NULL == openfile)
    {
        log_error(0, "ARCHIVE - \"%s\" file could not be opened for saving", archive_filename);
        return kfalse;
    }

    sdf_archive_optimize();

    // actually save the archive to disk
    {
        total_num_files = sdf_info.num_files + sdf_info.free_index;
        files_to_write  = _sdf_archive_spew_archive_header(openfile, sdf_info.header_list, total_num_files);

        // convert the headers over to the internal format
        _sdf_archive_compress_headers(sdf_info._ext_list, sdf_info.header_list);

        // Read in the data for the file header list
        assert( files_to_write == (Uint32)_sdf_archive_spew_headers(openfile, sdf_info._ext_list));

        fprintf(openfile, "----------------");

        // Read the data for each file in the header list...
        assert( files_to_write == (Uint32)_sdf_archive_spew_files(openfile, sdf_info.header_list));
    }

    // Close the file
    fclose(openfile);
    log_info(1, "ARCHIVE - \"%s\" file saved", archive_filename);

    return kfalse;
}


//-----------------------------------------------------------------------------------------------
void sdf_archive_set_flags(const char *file_name, Uint8 flag)
{
    // <ZZ> This function sets a flag for a single file.  Flags may be or'd...
    SDF_PHEADER file_index;

    file_index = sdf_archive_find_header(file_name);

    if (file_index)
    {
        sdf_file_add_flags(file_index, flag);
    }
}

//-----------------------------------------------------------------------------------------------
void sdf_archive_delete_all_files(SDF_FILE_TYPE type_to_delete, char* prefix_string)
{
    // <ZZ> Deletes all files of a given type
    int    i;

    char          file_name[SDF_FILENAME_SIZE] = EMPTY_STRING;
    SDF_FILE_TYPE file_type;
    Uint8         file_infobyte;

    //char newfile_name[20] = EMPTY_STRING;
    Uint16 length, j;
    bool_t found_mismatch;

    // Go through each file in the index
    length = 0;

    if (prefix_string)
    {
        log_info(1, "ARCHIVE - Deleting all %s files with prefix %s...", sdf_info.ext[type_to_delete].extension, prefix_string);
        length = strlen(prefix_string);
    }
    else
    {
        log_info(1, "ARCHIVE - Deleting all %s files...", sdf_info.ext[type_to_delete].extension);
    }

    repeat(i, sdf_info.num_files)
    {
        do_delay();

        sdf_archive_get_filename(i, file_name, &file_infobyte);
        file_type = file_infobyte & EXT_TYPE_MASK;

        if (type_to_delete == file_type)
        {
            found_mismatch = kfalse;
            repeat(j, length)
            {
                if (file_name[j] != prefix_string[j])
                {
                    found_mismatch = ktrue;
                    j = length;
                }
            }

            if (found_mismatch == kfalse)
            {
                sdf_archive_delete_file(file_name, type_to_delete);
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------
void sdf_archive_list_all_files(SDF_FILE_TYPE type_to_list, char* prefix_string, bool_t export_too)
{
    // <ZZ> Lists all files of a given type

    char          file_name[SDF_FILENAME_SIZE] = EMPTY_STRING;
    SDF_FILE_TYPE file_type;
    Uint8         file_infobyte;
    char          temp_string[255];

    int    i, j;
    Uint16 length;
    bool_t found_mismatch;

    // Go through each file in the index
    length = 0;

    if (prefix_string)
    {
        log_info(1, "ARCHIVE -  Listing all %s files with prefix %s...", sdf_info.ext[type_to_list].extension, prefix_string);
        length = strlen(prefix_string);
    }
    else
    {
        log_info(1, "ARCHIVE -  Listing all %s files...", sdf_info.ext[type_to_list].extension);
    }

    if (export_too)
    {
        log_info(1, "ARCHIVE -  Exporting 'em all too...");
    }

    repeat(i, sdf_info.num_files)
    {
        sdf_archive_get_filename(i, file_name, &file_infobyte);
        file_type = file_infobyte & EXT_TYPE_MASK;

        if (type_to_list == file_type)
        {
            found_mismatch = kfalse;
            repeat(j, length)
            {
                if (file_name[j] != prefix_string[j])
                {
                    found_mismatch = ktrue;
                    j = length;
                }
            }

            if (found_mismatch == kfalse)
            {
                log_info(0, "sdf_archive - Found %s.%s", file_name, sdf_info.ext[type_to_list].extension);

                if (export_too)
                {
                    sprintf(temp_string, "%s.%s", file_name, sdf_info.ext[type_to_list].extension);
                    sdf_archive_export_file(temp_string, NULL);
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------
void sdf_archive_optimize(void)
{
    // <ZZ> This function gets rid of all the deleted file entries in the index.  Simple cleanup
    //      routine...
    //
    // <BB> Modified to use a lot less copying



    SDF_PHEADER index_top, index_bot;
    SDF_HEADER  loc_index;

    int         i, j, num_files;

    // Go through each file in the index
    log_info(1, "ARCHIVE -  optimizing file list...");

    i = 0;
    j = sdf_info.num_files - 1;

    while (i < j)
    {
        //find first empty value, starting at the bottom of the stack
        while (i < j)
        {
            if (SDF_FILE_IS_UNUSED == (sdf_info.header_list + i)->type) break;

            i++;
        };

        //if the index stack is now packed, we are done
        if (j == i) return;

        //if (SDF_FILE_IS_UNUSED == file_type)
        //  log_message(0, "\tFound empty index on bottom: %d:\"%s\" UNUSED\n", i, file_name);
        //else
        //  log_message(0, "\tFound empty index on bottom: %d:\"%s.%s\"\n", i, file_name, sdf_info.ext[(file_type & EXT_TYPE_MASK)].extension);


        index_bot = sdf_info.header_list + i;

        //find first non-empty value, starting at the top of the stack
        while (j > i)
        {
            if (SDF_FILE_IS_UNUSED != (sdf_info.header_list + j)->type) break;

            j--;
        };

        //if the index stack is now packed, we are done
        if (j == i) return;

        //if (SDF_FILE_IS_UNUSED == file_type)
        //  log_message(0, "\tFound full index on top: %d:\"%s\" UNUSED\n", j, file_name);
        //else
        //  log_message(0, "\tFound full index on top: %d:\"%s.%s\"\n", j, file_name, sdf_info.ext[(file_type & EXT_TYPE_MASK)].extension);

        index_top = sdf_info.header_list + j;

        //swap the index data
        memcpy(&loc_index,  index_bot, sizeof(SDF_HEADER));
        memcpy( index_bot,  index_top, sizeof(SDF_HEADER));
        memcpy( index_top, &loc_index, sizeof(SDF_HEADER));

        //sdf_archive_get_filename(i, file_name, &file_infobyte);
        //file_type = file_infobyte & EXT_TYPE_MASK;
        //if (SDF_FILE_IS_UNUSED == file_type)
        //  log_message(0, "\tindex on bottom: %d:\"%s\" UNUSED\n", i, file_name);
        //else
        //  log_message(0, "\tindex on bottom: %d:\"%s.%s\"\n", i, file_name, sdf_info.ext[(file_type & EXT_TYPE_MASK)].extension);


        //sdf_archive_get_filename(j, file_name, &file_infobyte);
        //file_type = file_infobyte & EXT_TYPE_MASK;
        //if (SDF_FILE_IS_UNUSED == file_type)
        //  log_message(0, "\tindex on top: %d:\"%s\" UNUSED\n", j, file_name);
        //else
        //  log_message(0, "\tindex on top: %d:\"%s.%s\"\n", j, file_name, sdf_info.ext[(file_type & EXT_TYPE_MASK)].extension);
    }

    // fix the free list. insert them in reverse order, so that the LIFO structure sdf_info.free_list
    // will allocate new headers starting from the lowest indices
    num_files = sdf_info.free_index;
    repeat(sdf_info.free_index, num_files)
    {
        sdf_info.free_list[sdf_info.free_index] = sdf_info.num_files + (num_files - 1) - sdf_info.free_index;
    };

}

//-----------------------------------------------------------------------------------------------
SDF_PHEADER sdf_archive_find_header_by_data(SDF_PDATA file_start)
{
    // <BB> This function returns the pheader of a file, given a pointer to a file's data
    //      Returns NULL if no match found...

    Sint32      i;
    SDF_PHEADER pheader;
    SDF_PDATA   pdata_begin, pdata_end;

    repeat(i, sdf_info.num_files)
    {
        pheader = sdf_info.header_list + i;

        if (SDF_FILE_IS_UNUSED == sdf_file_get_type(pheader)) continue;

        pdata_begin = sdf_file_get_data(pheader);
        pdata_end   = pdata_begin + sdf_file_get_size(pheader);

        if (file_start >= pdata_begin && file_start < pdata_end)
        {
            return pheader;
        }
    }

    return NULL;
}


//-----------------------------------------------------------------------------------------------
size_t sdf_archive_find_index_by_data(SDF_PDATA file_start)
{
    // <ZZ> This function returns the pheader number of a file, given a pointer to the file's starting
    //      data...  Returns UINT16_MAX if no match found...
    int    i;
    SDF_PHEADER pheader;
    SDF_PDATA   pdata;

    repeat(i, sdf_info.num_files)
    {
        pheader = sdf_archive_find_header_by_data(file_start);

        if (SDF_FILE_IS_UNUSED == sdf_file_get_type(pheader)) continue;

        pdata = sdf_file_get_data(pheader);

        if (pdata == file_start)
        {
            return i;
        }
    }

    return SDF_INVALID_INDEX;
}


//-----------------------------------------------------------------------------------------------
bool_t sdf_archive_export_file(const char* file_name, const char* filename_path)
{
    // <ZZ> This function writes a single file from memory to disk.  The function should return
    //      kfalse if there's a problem writing, or ktrue if it all worked.

    SDF_PHEADER pheader;

    if (NULL == filename_path) filename_path = file_name;


    // See if we can find the file
    pheader = sdf_archive_find_header(file_name);

    if (NULL == pheader)
    {
        log_error(0, "ARCHIVE - \"%s\" could not be found in datafile", file_name);
        return kfalse;
    }

    return sdf_file_export_file(pheader, filename_path);
}


//-----------------------------------------------------------------------------------------------
SDF_PHEADER sdf_archive_get_header(Uint32 index)
{
    if(NULL == sdf_info.header_list) return NULL;
    if(index > (Uint32)sdf_info.num_files + sdf_info.free_index) return NULL;

    return sdf_info.header_list + index;
}

//-----------------------------------------------------------------------------------------------
int sdf_archive_get_num_files()     { return sdf_info.num_files; }

//-----------------------------------------------------------------------------------------------
int sdf_archive_free_file_count()   { return sdf_info.free_index; }

//-----------------------------------------------------------------------------------------------
bool_t sdf_archive_updated_performed() { return sdf_info.update_performed; }
//-----------------------------------------------------------------------------------------------
SDF_PEXTENSION_INFO sdf_archive_get_ext(Uint32 index)
{
    return sdf_info.ext + index;
}

//-----------------------------------------------------------------------------------------------
//SF_PSTREAM sdf_archive_get_pstream() { return &(sdf_info.stream); };

//===============================================================================================
//===============================================================================================
size_t sdf_file_get_size(SDF_PHEADER pheader)
{
    //soulfu_assert(NULL!=pheader);
    if (NULL == pheader) return INVALID_SIZE;

    return pheader->size;
};

//-----------------------------------------------------------------------------------------------
size_t sdf_file_get_allocated(SDF_PHEADER pheader)
{
    //soulfu_assert(NULL!=pheader);
    if (NULL == pheader) return INVALID_SIZE;

    return pheader->allocated;
};

//-----------------------------------------------------------------------------------------------
Uint8 sdf_file_get_info_byte(SDF_PHEADER pheader)
{
    //soulfu_assert(NULL!=pheader);
    if (NULL == pheader) return 0x00;

    return (pheader->flags & EXT_FLAGS_MASK) | (pheader->type & EXT_TYPE_MASK);
};


//-----------------------------------------------------------------------------------------------
Uint8 sdf_file_get_flags(SDF_PHEADER pheader)
{
    //soulfu_assert(NULL!=pheader);
    if (NULL == pheader) return 0x00;

    return pheader->flags;
};

//-----------------------------------------------------------------------------------------------
SDF_FILE_TYPE sdf_file_get_type(SDF_PHEADER pheader)
{
    //soulfu_assert(NULL!=pheader);
    if (NULL == pheader) return (SDF_FILE_TYPE)0xFF;

    return (SDF_FILE_TYPE)(pheader->type & EXT_TYPE_MASK);
};

//-----------------------------------------------------------------------------------------------
SDF_PDATA sdf_file_get_data(SDF_PHEADER pheader)
{
    //soulfu_assert(NULL!=pheader);
    if (NULL == pheader) return NULL;

    return pheader->data;
};

//-----------------------------------------------------------------------------------------------
void sdf_file_set_unused(SDF_PHEADER pheader)
{
    if (NULL == pheader) return;

    pheadermap_array_name_change(pheader, NULL);

    pheader->type = SDF_FILE_IS_UNUSED;
};

//-----------------------------------------------------------------------------------------------
void sdf_file_set_info(SDF_PHEADER pheader, Uint8 info)
{
    SDF_FILE_TYPE file_type;

    if (NULL == pheader) return;

    file_type = (SDF_FILE_TYPE)(info & EXT_TYPE_MASK);
    pheadermap_array_type_change(pheader, file_type);

    pheader->flags = info & EXT_FLAGS_MASK;
    pheader->type  = file_type;
};

//-----------------------------------------------------------------------------------------------
void sdf_file_set_type(SDF_PHEADER pheader, SDF_FILE_TYPE type)
{
    if (NULL == pheader) return;

    pheadermap_array_type_change(pheader, type);
    pheader->type  = type;
}

//-----------------------------------------------------------------------------------------------
void sdf_file_set_flags(SDF_PHEADER pheader, Uint8 flags)
{
    if (NULL == pheader) return;

    pheader->flags = flags;
};

//-----------------------------------------------------------------------------------------------
void sdf_file_add_flags(SDF_PHEADER pheader, Uint8 flags)
{
    if (NULL == pheader) return;

    FLAGS_ADD(pheader->flags, flags);
};

//-----------------------------------------------------------------------------------------------
void sdf_file_remove_flags(SDF_PHEADER pheader, Uint8 flags)
{
    if (NULL == pheader) return;

    FLAGS_REMOVE(pheader->flags, flags);
};

//-----------------------------------------------------------------------------------------------
void sdf_file_set_name(SDF_PHEADER pheader, const char * file_name)
{
    if (NULL == pheader) return;

    pheadermap_array_name_change(pheader, file_name);

    if (NULL == file_name)  { *(pheader->fname) = EOS; return; };

    strncpy(pheader->fname, file_name, SDF_FILENAME_SIZE);
};

//-----------------------------------------------------------------------------------------------
void sdf_file_set_data(SDF_PHEADER pheader, SDF_PDATA  data)
{
    if (NULL == pheader) return;

    pheader->data = data;
}

//-----------------------------------------------------------------------------------------------
void sdf_file_set_size(SDF_PHEADER pheader, size_t size)
{
    if (NULL == pheader) return;

    //!!!!make sure there is no overflow with the size.  Max size is 24 bits at the moment!!!!
    //soulfu_assert(size < EXT_SIZE_24_MASK);

    pheader->size = size;
};

//-----------------------------------------------------------------------------------------------
char * sdf_file_get_name(SDF_PHEADER pheader)
{
    if (NULL == pheader) return NULL;

    return pheader->fname;
}

//-----------------------------------------------------------------------------------------------
const char * sdf_file_get_extension(SDF_PHEADER pheader)
{
    if (NULL == pheader) return NULL;

    return sdf_info.ext[pheader->type].extension;
}

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
char * sdf_file_scan_line(SDF_PHEADER psrc_header, size_t line)
{
    size_t line_cnt;
    SDF_PDATA pdata, pdata_begin, pdata_end;

    if (NULL == psrc_header) return NULL;

    pdata_begin = sdf_file_get_data(psrc_header);
    pdata_end   = pdata_begin + sdf_file_get_size(psrc_header);

    for (pdata = pdata_begin, line_cnt = 1; pdata < pdata_end && line_cnt < line; pdata++)
    {
        if (EOS == *pdata) line_cnt++;
    }

    return (pdata < pdata_end) ? pdata : NULL;
}

//-----------------------------------------------------------------------------------------------
bool_t sdf_file_get_file_info(SDF_PHEADER pheader, char* newfile_name, Uint8* file_type)
{
    if (NULL == pheader)
    {
        if (NULL != newfile_name) newfile_name[0] = EOS;

        if (NULL !=    file_type) *file_type      = SDF_FILE_IS_UNUSED;

        return kfalse;
    }

    if (NULL != newfile_name)
    {
        strncpy(newfile_name, sdf_file_get_name(pheader), SDF_FILENAME_SIZE);
    };

    if (NULL != file_type)
    {
        *file_type = sdf_file_get_info_byte(pheader);
    };

    return ktrue;
}

//-----------------------------------------------------------------------------------------------
bool_t sdf_file_get_filename(SDF_PHEADER pheader, char* file_name, bool_t add_extension)
{
    int i;
    char *pdst, *psrc;
    Uint8 file_type;

    if (NULL == pheader) return kfalse;

    file_type = sdf_file_get_type(pheader);

    if (SDF_FILE_IS_UNUSED == file_type) return kfalse;

    pdst = file_name;
    psrc = pheader->fname;

    for (i = 0; i < SDF_FILENAME_SIZE && *psrc != EOS; i++)
    {
        *pdst++ = *psrc++;
    }

    *pdst++ = '.';
    *pdst++ = EOS;

    if (add_extension)
    {
        strcat(file_name, sdf_info.ext[file_type].extension);
    };

    return ktrue;
}


//-----------------------------------------------------------------------------------------------
bool_t sdf_file_export_file(SDF_PHEADER pheader, const char* filename_path)
{
    // <ZZ> This function writes a single file from memory to disk.  The function should return
    //      kfalse if there's a problem writing, or ktrue if it all worked.
    FILE*  openfile = NULL;
    Uint32 size;
    Uint8  file_type;
    Uint32 lan_lines;
    SDF_PDATA pdata;
    char file_name[16] = EMPTY_STRING;

    if (NULL == pheader)
    {
        log_error(0, "ARCHIVE - header is invalid");
        return kfalse;
    }

    sdf_file_get_filename(pheader, file_name, ktrue);

    if (NULL == filename_path) filename_path = file_name;

    // Get the size and data location
    file_type = sdf_file_get_type(pheader);
    pdata     = sdf_file_get_data(pheader);
    size      = sdf_file_get_size(pheader);

    if (SDF_FILE_IS_RDY == file_type)
    {
#if defined(EXPORTRDYASDDD) && !defined(_SOULFU_LIB)
        char ddd_fname[256];
        int  length;

        strcpy(ddd_fname, filename_path);

        // Export RDY files as DDD...
        length = strlen(ddd_fname);

        if (length > 4)
        {
            if (ddd_fname[length-3] == 'R' || ddd_fname[length-3] == 'r')
            {
                if (ddd_fname[length-2] == 'D' || ddd_fname[length-2] == 'd')
                {
                    if (ddd_fname[length-1] == 'Y' || ddd_fname[length-1] == 'y')
                    {
                        ddd_fname[length-3] = 'D';
                        ddd_fname[length-2] = 'D';
                        ddd_fname[length-1] = 'D';
                    }
                }
            }
        }

        openfile = fopen(ddd_fname, "wb");

        if (NULL == openfile)
        {
            log_error(0, "ARCHIVE - \"%s\" could not be opened for writing", file_name);
            return kfalse;
        }

        rdy_to_ddd_export(openfile, pheader);
#else
        openfile = fopen(filename_path, "wb");

        if (NULL == openfile)
        {
            log_error(0, "ARCHIVE - \"%s\" could not be opened for writing", file_name);
            return kfalse;
        }

        // Write the raw data
        fwrite(pdata, 1, size, openfile);
#endif /* defined(EXPORTRDYASDDD) && !defined(_SOULFU_LIB) */
    }
    else if (SDF_FILE_IS_SRF == file_type)
    {
        openfile = fopen(filename_path, "wb");

        if (NULL == openfile)
        {
            log_error(0, "ARCHIVE - \"%s\" could not be opened for writing", file_name);
            return kfalse;
        }

#ifdef DEVTOOL
        // Compute the waypoint table data before saving...
        room_srf_build_waypoint_table(pdata);
#endif

        // Write the raw data
        fwrite(pdata, 1, size, openfile);
    }
    else
    {
        // Open the file for writing
        openfile = fopen(filename_path, "wb");

        if (NULL == openfile)
        {
            log_error(0, "ARCHIVE - \"%s\" could not be opened for writing", file_name);
            return kfalse;
        }

        if (SDF_FILE_IS_TXT == file_type || SDF_FILE_IS_SRC == file_type || SDF_FILE_IS_LAN == file_type)
        {
            // Chop out language file header...
            if (SDF_FILE_IS_LAN == file_type)
            {
                lan_lines = endian_read_mem_int32(pdata);
                pdata += 4 + (lan_lines << 2);
                size -= 4 + (lan_lines << 2);
            }

            // Write data in original format...  0D 0A style returns instead of null term...
            while (size > 0)
            {
                if (*pdata == EOS)
                {
                    fprintf(openfile, "\r\n");
                }
                else
                {
                    fprintf(openfile, "%c", *pdata);
                }

                size--;
                pdata++;
            }
        }
        else
        {
            // Write the raw data
            fwrite(pdata, 1, size, openfile);
        }
    }

    // Close the file
    fclose(openfile);
    log_info(1, "ARCHIVE -  Exported file \"%s\" from datafile...", file_name);

    return ktrue;
}


//===============================================================================================
//===============================================================================================
// OLD NETWORK STUFF
//
//Uint16 sdf_archive_find_filetype_index(SDF_PDATA filedata, Uint8 file_type)
//{
//  // <ZZ> This function tells us that a given file is the fourth .RGB file, or the
//  //      28th .RUN file, etc...  Used for network transfers, since actual
//  //      memory addresses can't be used...  Returns UINT16_MAX if no match was found...
//  Uint16 i;
//  Uint16 search_index;
//  SDF_PDATA  search_filedata;
//  SDF_PHEADER pheader;
//
//
//
//  // Search through every file in the datafile...
//  search_index = 0;
//  repeat(i, sdf_info.num_files)
//  {
//    pheader = sdf_info.header_list + i;
//
//    // Match types...
//    if(file_type == sdf_file_get_type(pheader)  )
//    {
//      // Types match, but does the file data?
//      search_filedata = sdf_file_get_data(pheader);
//      if(search_filedata == filedata)
//      {
//        // Looks like we found our file...
//        return search_index;
//      }
//      search_index++;
//    }
//  }
//  // We didn't find our file...
//  return UINT16_MAX;
//}
//
////-----------------------------------------------------------------------------------------------
//SDF_PDATA  sdf_resolve_filetype_index(Uint16 filetype_index, Uint8 file_type)
//{
//  // <ZZ> This function returns the start of a file's data, when it's told something like
//  //      fourth .RGB file, or 28th .RUN file...  Used for network transfers, since actual
//  //      memory addresses can't be used...  Returns NULL if the pheader couldn't be resolved...
//  Uint16 i;
//  Uint16 search_index;
//  SDF_PDATA  search_filedata;
//  SDF_PHEADER pheader;
//
//
//  if(filetype_index < UINT16_MAX)
//  {
//    search_index = 0;
//    repeat(i, sdf_info.num_files)
//    {
//      pheader = sdf_info.header_list + i;
//
//      // Match the filetypes...
//      if(file_type == sdf_file_get_type(pheader) )
//      {
//        // Okay...  Looks like this file is the correct type, but is it the
//        // one we're lookin' for?
//        if(filetype_index == search_index)
//        {
//          // Yup it is...
//          search_filedata = sdf_file_get_data(pheader);
//          return search_filedata;
//        }
//        search_index++;
//      }
//    }
//  }
//  // The file_type pheader wasn't valid...
//  return NULL;
//}

//-----------------------------------------------------------------------------------------------
SDF_PSTREAM sdf_stream_clone(SDF_PSTREAM pdst, SDF_PSTREAM psrc)
{
    if (NULL == pdst) return NULL;

    sdf_stream_init(pdst);

    if (NULL == psrc) return pdst;

    memcpy(pdst, psrc, sizeof(SDF_STREAM));

    return pdst;
};

//-----------------------------------------------------------------------------------------------
bool_t sdf_stream_eos(SDF_PSTREAM pstream)
{
    if (NULL == pstream) return ktrue;

    return (pstream->read < pstream->read_beg) || (pstream->read >= pstream->read_end);
};


//-----------------------------------------------------------------------------------------------
SDF_PSTREAM sdf_stream_init(SDF_PSTREAM pstream)
{
    if (NULL == pstream) return NULL;

    pstream->read_beg = pstream->read_end = pstream->read = NULL;
    pstream->line     = 0;
    pstream->started  = kfalse;

    return pstream;
};

//-----------------------------------------------------------------------------------------------
SDF_PSTREAM sdf_stream_rewind(SDF_PSTREAM pstream)
{
    if (NULL == pstream) return NULL;

    pstream->read    = pstream->read_beg;
    pstream->line    = 0;
    pstream->started = kfalse;

    return pstream;
}

//-----------------------------------------------------------------------------------------------
SDF_PSTREAM sdf_stream_open_mem(SDF_PSTREAM pstream, SDF_PDATA data, size_t size)
{
    if (NULL == pstream) return NULL;

    sdf_stream_init(pstream);

    pstream->read_beg = data;
    pstream->read_end = data  + size;
    pstream->read     = pstream->read_beg;
    pstream->line     = 0;
    pstream->started  = kfalse;

    return pstream;
};


//-----------------------------------------------------------------------------------------------
SDF_PSTREAM sdf_stream_open_header(SDF_PSTREAM pstream, SDF_PHEADER pheader)
{
    if (NULL == pstream) return NULL;

    sdf_stream_init(pstream);

    if (NULL == pheader) return pstream;

    return sdf_stream_open_mem(pstream, sdf_file_get_data(pheader), sdf_file_get_size(pheader));
}

//-----------------------------------------------------------------------------------------------
SDF_PSTREAM sdf_stream_open_filename(SDF_PSTREAM pstream, const char* filename)
{
    SDF_PHEADER pheader = sdf_archive_find_header(filename);

    return sdf_stream_open_header(pstream, pheader);
}

//-----------------------------------------------------------------------------------------------
bool_t sdf_stream_read_line(SDF_PSTREAM pstream)
{
    // <ZZ> This function places pstream->ppos at the start of the next good line in a file
    //      opened by sdf_archive_stream_open().  If something goes wrong, it returns kfalse.  If something
    //      goes right, it returns ktrue.

    bool_t bfound = kfalse;

    if ( sdf_stream_eos(pstream) )
        return bfound;

    // Don't advance the read head on the first call...
    if (!pstream->started)
    {
        pstream->started = ktrue;
    }
    else
    {
        // Skip over the line until we hit a 0
        while (EOS != *(pstream->read) && !sdf_stream_eos(pstream))
        {
            pstream->read++;
        }
    }

    // Skip any 0's, so text_stream starts on some good characters...
    while (EOS == *(pstream->read) && !sdf_stream_eos(pstream))
    {
        pstream->read++;
        pstream->line++;
    }

    // Make sure we didn't run out of data...
    return !sdf_stream_eos(pstream);
}


//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
Sint8 sdf_insert_data(Uint8* file_pos, Uint8* data_to_add, Sint32 bytes_to_add)
{
    // <ZZ> This function adds some bytes to a file, at any location within the file.
    SDF_PHEADER pheader;
    Uint8* file_start;
    Sint32 file_size;
    Uint8 file_type;
    Sint32 bytes_to_subtract;
    int    i, j;
    Uint32 offset, file_offset;


    // First find the pheader information for the file, by checking every entry we've got...
    pheader = sdf_info.header_list;
    repeat(i, sdf_archive_get_num_files())
    {
        file_start = sdf_file_get_data(pheader);
        file_size  = sdf_file_get_size(pheader);
        file_type  = sdf_file_get_type(pheader);

        do_delay();

        if (file_type == SDF_FILE_IS_SRC || file_type == SDF_FILE_IS_TXT)
        {
            // Don't allow last 0 to be removed...
            file_size--;
        }

        if (file_pos >= file_start && file_pos <= (file_start + file_size) && file_type != SDF_FILE_IS_UNUSED)
        {
            // We found a match...  Make sure we have enough memory reserved to perform the operation...
            if (bytes_to_add < 0)
            {
                // Removing data...  Figure out how many bytes we're really getting rid of...
                bytes_to_subtract = -bytes_to_add;

                if (file_start + file_size > file_pos + bytes_to_subtract)
                {
                    // Trying to remove interior bytes...  Need to shuffle data...
                    memmove(file_pos, file_pos + bytes_to_subtract, file_size + (size_t)(file_start - file_pos));
                }

                file_size -= bytes_to_subtract;


                // Adjust header offsets...
                file_offset = (size_t) (file_pos - file_start);

                if (file_type == SDF_FILE_IS_SRF)
                {
                    repeat(j, 14)
                    {
                        offset = endian_read_mem_int32((Uint8*)(file_start + 40 + (j << 2)));

                        if (offset >= file_offset)
                        {
                            if (offset > ((size_t) bytes_to_subtract))
                            {
                                offset -= bytes_to_subtract;
                                endian_write_mem_int32((Uint8*)(file_start + 40 + (j << 2)), offset);
                            }
                        }
                    }
                }


                // Pad end of file with 0's
                memset(((Uint8*) file_start) + file_size, 0, bytes_to_subtract);

                if (file_type == SDF_FILE_IS_SRC || file_type == SDF_FILE_IS_TXT)
                {
                    // Don't allow last 0 to be removed...
                    file_size++;
                }


                // Then write down how big the file is, so we don't forget...
                if (file_size < 0) file_size = 0;

                sdf_file_set_size(pheader, file_size);
                sdf_file_add_flags(pheader, SDF_FLAG_WAS_UPDATED);
                return ktrue;
            }
            else
            {
                // Adding data...
                if ((size_t)(file_size + bytes_to_add) < sdf_archive_get_ext(file_type)->min_size)
                {
                    if (file_type == SDF_FILE_IS_SRC || file_type == SDF_FILE_IS_TXT)
                    {
                        // Don't allow last 0 to be removed...
                        file_size++;
                    }


                    // We have enough room to add the data...  Start by shuffling the old data so we
                    // don't overwrite anything...
                    memmove(file_pos + bytes_to_add, file_pos, file_size + (size_t) (file_start - file_pos));


                    // Then copy in the new data
                    if (data_to_add)
                    {
                        // Copy from the specified location
                        memcpy(file_pos, data_to_add, bytes_to_add);
                    }
                    else
                    {
                        // Zero out the new data
                        memset(file_pos, 0, bytes_to_add);
                    }



                    // Adjust header offsets...
                    file_offset = (size_t) (file_pos - file_start);

                    if (file_type == SDF_FILE_IS_SRF)
                    {
                        repeat(j, 14)
                        {
                            offset = endian_read_mem_int32((Uint8*)(file_start + 40 + (j << 2)));

                            if (offset >= file_offset)
                            {
                                offset += bytes_to_add;
                                endian_write_mem_int32((Uint8*)(file_start + 40 + (j << 2)), offset);
                            }
                        }
                    }


                    // Then write down how big the file is, so we don't forget...
                    file_size += bytes_to_add;
                    sdf_file_set_size(pheader, file_size);
                    sdf_file_add_flags(pheader, SDF_FLAG_WAS_UPDATED);
                    return ktrue;
                }

                return kfalse;
            }
        }
        else
        {
            // Keep looking...
            pheader++;
        }
    }
    return kfalse;
}
#endif


//-----------------------------------------------------------------------------------------------
void decode_sdf_archive(Uint8 mask, float loadin_min, float loadin_max)
{
    // <ZZ> This function chews on the raw data for every compressed file until it becomes
    //      something useful (ie. convert JPG->RGB).  Depending on the compile time options,
    //      the function may try to recycle space by deleting the compressed data, or it may
    //      not.  Mask lets me pick SDF_FLAG_ALL or SDF_FLAG_WAS_UPDATED...  Draw_loadin tells us
    //      whether or not we should draw the start up loading screen...

    int    i;
    bool_t succeed;

    char  * file_name = NULL;
    Uint8   file_type;

    SDF_PHEADER pheader;
    SDF_PDATA   pdata;

    bool_t draw_loadin;
    draw_loadin = loadin_max > loadin_min;

    // Go through each file in the pdata
    log_info(0, "Decompressing subfiles (%d of 'em)...", sdf_archive_get_num_files());
    repeat(i, sdf_archive_get_num_files())
    {

        UI_INTERRUPT(draw_loadin, i, loadin_min, loadin_max);

        pheader = sdf_archive_get_header(i);

        if ((SDF_FLAG_ALL != mask) && 0 == (sdf_file_get_flags(pheader)&mask)) continue;

        pdata = sdf_file_get_data(pheader);

        if (NULL == pdata) continue;

        // Check the type of file to decompress, then hand off to a subroutine...
        succeed = ktrue;
        file_name = sdf_file_get_name(pheader);
        file_type = sdf_file_get_type(pheader);

        switch (file_type)
        {
            case SDF_FILE_IS_JPG:

                if (mask != SDF_FLAG_ALL)
                {
                    sdf_archive_delete_file(file_name, SDF_FILE_IS_RGB);
                }

#if defined(DEBUG_SRCDECODE)
                // create a bogus file
                log_info(1, "bogus sdf_archive_create_header()");
                succeed = (NULL != sdf_archive_create_header(NULL, file_name, 0, SDF_FILE_IS_RGB));
#else
                log_info(1, "decode_jpg()");
                succeed = (NULL != decode_jpg(pheader, file_name));
#endif
                break;


            case SDF_FILE_IS_OGG:

                if (mask != SDF_FLAG_ALL)
                {
                    sdf_archive_delete_file(file_name, SDF_FILE_IS_RAW);
                }

#if defined(DEBUG_SRCDECODE)
                // create a bogus file
                log_info(1, "bogus sdf_archive_create_header()");
                succeed = (NULL != sdf_archive_create_header(NULL, file_name, 0, SDF_FILE_IS_RAW));
#else
                log_info(1, "decode_ogg()");
                succeed = (NULL != decode_ogg(pheader, file_name));
#endif
                break;


            case SDF_FILE_IS_PCX:

                if (mask != SDF_FLAG_ALL)
                {
                    sdf_archive_delete_file(file_name, SDF_FILE_IS_RGB);
                }

#if defined(DEBUG_SRCDECODE)
                // create a bogus file
                log_info(1, "bogus sdf_archive_create_header()");
                succeed = (NULL != sdf_archive_create_header(NULL, file_name, 0, SDF_FILE_IS_RGB));
#else
                log_info(1, "decode_pcx()");
                succeed = (NULL != decode_pcx(pheader, file_name));
#endif
                break;


            case SDF_FILE_IS_DDD:

                if (mask != SDF_FLAG_ALL)
                {
                    sdf_archive_delete_file(file_name, SDF_FILE_IS_RDY);
                }

#if defined(DEBUG_SRCDECODE)
                // create a bogus file
                log_info(1, "bogus sdf_archive_create_header()");
                succeed = (NULL != sdf_archive_create_header(NULL, file_name, 0, SDF_FILE_IS_RDY));
#else
                log_info(1, "decode_ddd()");
                succeed = (NULL != decode_ddd(pheader, file_name));
#endif
                break;


            default:
                break;
        }

        if (!succeed)
        {
            log_error(0, "ARCHIVE - Problem decoding \"%s.%s\"", file_name, sdf_file_get_extension(pheader));
        }

    }


    // Finish up
    log_info(0, "Decompression finished");
}
