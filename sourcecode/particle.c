// <ZZ> This file contains functions for particles...

#include "object.h"

float texpos_corner_xy[4][2] = {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}};
float texpos_number_xy[4][2] = {{0.0f, 0.1875f}, {1.0f, 0.1875f}, {1.0f, 0.25f}, {0.0f, 0.25f}};

//-----------------------------------------------------------------------------------------------
#define ATTACHED_SCALAR_MAX  4.0f
#define ATTACHED_SCALAR_MULTIPLY 16.0f
#define ATTACHED_SCALAR_DIVIDE   0.0625f
#define ATTACHED_SCALAR_ADD 64
Uint8 particle_attach_to_character(Uint16 particle, Uint16 character, Uint8 bone_type)
{
    // <ZZ> This function attaches a particle to a given character, returning ktrue if it worked,
    //      or kfalse if it didn't...  World bone frame information must've been generated for character
    //      prior to calling...
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_bone;
    Uint16 num_vertex;
    Uint8* vertex_data;
    Uint8* base_model_data;
    Uint8* bone_data;
    Uint8* frame_data;
    Uint16 i, j;
    Uint16 nearest_bone;
    Uint8* nearest_vertex_data;
    float nearest_distance;
    float distance;
    float distance_xyz[3];
    float direction_xyz[3];
    Uint8 base_model;
    Uint16 frame;
    CHR_DATA* chr_data;
    Uint8* data;
    float* joint_data;
    Uint16 joint[2];
    PRT_DATA* prt_data;
    Uint8 binding;
    float* particle_xyz;
    float midpoint_xyz[3];
    float length;
    float scalars_xyz[3];


    // Get information from the character variables...
    chr_data = chr_data_get_ptr( character );
    if ( NULL == chr_data )  return kfalse;

    data = chr_data->model.parts[MODEL_PART_BASE].file;

    if (data == NULL) return kfalse;

    frame = chr_data->frame;

    // Start reading the RDY file...
    data += 3;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data ); data += 2;

    if (frame >= num_bone_frame) return kfalse;

    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);


    // Go to the current base model
    frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
    base_model = *(frame_data + 2);
    data = data + (base_model * 20);
    base_model_data = DEREF( Uint8*, data );
    num_bone = DEREF( Uint16, base_model_data + 6 );


    // Skip to the bone connection info of the base model...
    bone_data = DEREF( Uint8*, data + 16 );


    // Go to the current frame...
    frame_data = temp_character_bone_frame[character];
    joint_data = (float*) (frame_data + (num_bone * 24));



    // Make sure frame data is valid...
    if (frame_data)
    {
        // Get the particle information...
        prt_data = main_prt_data + particle;
        particle_xyz = &(prt_data->x);

        if (prt_data->flags & PART_SPIN_ON)
        {
            // Single point particle
            midpoint_xyz[XX] = particle_xyz[XX];
            midpoint_xyz[YY] = particle_xyz[YY];
            midpoint_xyz[ZZ] = particle_xyz[ZZ];
        }
        else
        {
            // Double point particle
            midpoint_xyz[XX] = (particle_xyz[XX] + particle_xyz[3]) * 0.5f;
            midpoint_xyz[YY] = (particle_xyz[YY] + particle_xyz[4]) * 0.5f;
            midpoint_xyz[ZZ] = (particle_xyz[ZZ] + particle_xyz[5]) * 0.5f;
        }





        // Bone type 255 is special cased as a vertex-overlaping type...
        if (bone_type == 255)
        {
            // Search for closest vertex in the model...
            nearest_distance = 9999.9f;
            nearest_vertex_data = NULL;


            // Go through each model segment...
            repeat(j, 10)
            {
                // Find out some stuff about our sub-model...
                data = chr_data->model.parts[j].file;

                if (data)
                {
                    // Start reading the RDY file...
                    data += 3;
                    num_base_model = *data;  data += 3;
                    data += (ACTION_MAX << 1);
                    data += (MAX_DDD_SHADOW_TEXTURE);


                    // Assume base model 0...
                    base_model_data = DEREF( Uint8*, data );
                    num_vertex = DEREF( Uint16, base_model_data );
                    num_bone = DEREF( Uint16, base_model_data + 6 );


                    // Skip to the bone connection info of the base model...
                    vertex_data = base_model_data + 8;
                    bone_data = DEREF( Uint8*, data + 16 );


                    // Generate vertex coordinates for this sub-model, based on bone locations...
                    render_bone_frame(base_model_data, bone_data, frame_data);
                    repeat(i, num_vertex)
                    {
                        distance_xyz[XX] = (DEREF( float, vertex_data ))     - midpoint_xyz[XX];
                        distance_xyz[YY] = (DEREF( float, vertex_data + 4 )) - midpoint_xyz[YY];
                        distance_xyz[ZZ] = (DEREF( float, vertex_data + 8 )) - midpoint_xyz[ZZ];
                        distance = vector_length(distance_xyz);

                        // See if it's the best vertex or not...
                        if (distance < nearest_distance)
                        {
                            // Remember it...
                            nearest_vertex_data = vertex_data;
                            nearest_distance = distance;
                        }

                        vertex_data += 64;
                    }
                }
            }

//0  - X
//4  - Y
//8  - Z
//12 - bind A
//13 - bind B
//14 - weight
//15 - normal X
//19 - normal Y
//23 - normal Z
//27 - height percent A
//31 - height percent B
//35 - forward offset A
//39 - forward offset B
//43 - side offset A
//47 - side offset B

            // Did we find a valid vertex?
            if (nearest_vertex_data)
            {
                // Fill in the particle information...
                binding = (nearest_vertex_data[14] >> 6) & 1;
                prt_data->bone = nearest_vertex_data[12+binding];

                // Front end (xyz) of particle...
                binding = binding << 2;
                scalars_xyz[XX] = DEREF( float, nearest_vertex_data + 27 + binding );  // Height
                scalars_xyz[YY] = DEREF( float, nearest_vertex_data + 35 + binding );  // Forward
                scalars_xyz[ZZ] = DEREF( float, nearest_vertex_data + 43 + binding );  // Side

                if (scalars_xyz[XX] > ATTACHED_SCALAR_MAX)  scalars_xyz[XX] = ATTACHED_SCALAR_MAX;

                if (scalars_xyz[XX] < -ATTACHED_SCALAR_MAX)  scalars_xyz[XX] = -ATTACHED_SCALAR_MAX;

                if (scalars_xyz[YY] > ATTACHED_SCALAR_MAX)  scalars_xyz[YY] = ATTACHED_SCALAR_MAX;

                if (scalars_xyz[YY] < -ATTACHED_SCALAR_MAX)  scalars_xyz[YY] = -ATTACHED_SCALAR_MAX;

                if (scalars_xyz[ZZ] > ATTACHED_SCALAR_MAX)  scalars_xyz[ZZ] = ATTACHED_SCALAR_MAX;

                if (scalars_xyz[ZZ] < -ATTACHED_SCALAR_MAX)  scalars_xyz[ZZ] = -ATTACHED_SCALAR_MAX;

                prt_data->misc.i08[0] = (Uint8) ((scalars_xyz[XX] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);
                prt_data->misc.i08[1] = (Uint8) ((scalars_xyz[YY] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);
                prt_data->misc.i08[2] = (Uint8) ((scalars_xyz[ZZ] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);


                // Back end (last_xyz) of particle...
                scalars_xyz[XX] *= (1.0f + prt_data->length);
                scalars_xyz[YY] *= (1.0f + prt_data->length);
                scalars_xyz[ZZ] *= (1.0f + prt_data->length);

                if (scalars_xyz[XX] > ATTACHED_SCALAR_MAX)  scalars_xyz[XX] = ATTACHED_SCALAR_MAX;

                if (scalars_xyz[XX] < -ATTACHED_SCALAR_MAX)  scalars_xyz[XX] = -ATTACHED_SCALAR_MAX;

                if (scalars_xyz[YY] > ATTACHED_SCALAR_MAX)  scalars_xyz[YY] = ATTACHED_SCALAR_MAX;

                if (scalars_xyz[YY] < -ATTACHED_SCALAR_MAX)  scalars_xyz[YY] = -ATTACHED_SCALAR_MAX;

                if (scalars_xyz[ZZ] > ATTACHED_SCALAR_MAX)  scalars_xyz[ZZ] = ATTACHED_SCALAR_MAX;

                if (scalars_xyz[ZZ] < -ATTACHED_SCALAR_MAX)  scalars_xyz[ZZ] = -ATTACHED_SCALAR_MAX;

                prt_data->misc.i08[3] = (Uint8) ((scalars_xyz[XX] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);
                prt_data->misc.i08[4] = (Uint8) ((scalars_xyz[YY] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);
                prt_data->misc.i08[5] = (Uint8) ((scalars_xyz[ZZ] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);


                // Set the particle to be attached to the bone we found...
                (prt_data->flags) |= PART_STUCK_ON;
                prt_data->stuckto = character;
                return ktrue;
            }
        }
        else
        {
            // Search for the nearest bone...
            nearest_bone = 0;
            nearest_distance = 9999.9f;
            repeat(i, num_bone)
            {
                // Only attach to normal bones, not weapon grips and other types...  Or attach only to weapons...
                if ((*bone_data == bone_type))
                {
                    joint[0] = DEREF( Uint16, bone_data + 1 );
                    joint[1] = DEREF( Uint16, bone_data + 3 );
                    joint[0] *= 3;
                    joint[1] *= 3;

                    // Find the bone's midpoint...
                    distance_xyz[XX] = (joint_data[joint[0]] + joint_data[joint[1]]) * 0.5f;
                    distance_xyz[YY] = (joint_data[joint[0] + 1] + joint_data[joint[1] + 1]) * 0.5f;
                    distance_xyz[ZZ] = (joint_data[joint[0] + 2] + joint_data[joint[1] + 2]) * 0.5f;

                    // Find the distance from midpoint to midpoint...
                    distance_xyz[XX] -= midpoint_xyz[XX];
                    distance_xyz[YY] -= midpoint_xyz[YY];
                    distance_xyz[ZZ] -= midpoint_xyz[ZZ];
                    distance = vector_length(distance_xyz);

                    // See if it's the best bone or not...
                    if (distance < nearest_distance)
                    {
                        nearest_bone = i;
                        nearest_distance = distance;
                    }
                }

                bone_data += 9;
            }


            // Set the particle to be attached to the bone we found...
            (prt_data->flags) |= PART_STUCK_ON;
            prt_data->bone = (Uint8) nearest_bone;
            prt_data->stuckto = character;


            // Figger' the normal scalars for generating the particle position...
            bone_data = DEREF( Uint8*, data + 16 );
            bone_data += (nearest_bone * 9);
            joint[0] = DEREF( Uint16, bone_data + 1 );
            joint[1] = DEREF( Uint16, bone_data + 3 );
            joint[0] *= 3;
            joint[1] *= 3;
            frame_data += (nearest_bone * 24);
            direction_xyz[XX] = (joint_data[joint[1]] - joint_data[joint[0]]);
            direction_xyz[YY] = (joint_data[joint[1] + 1] - joint_data[joint[0] + 1]);
            direction_xyz[ZZ] = (joint_data[joint[1] + 2] - joint_data[joint[0] + 2]);
            distance_xyz[XX] = particle_xyz[XX] - joint_data[joint[0]];
            distance_xyz[YY] = particle_xyz[YY] - joint_data[joint[0] + 1];
            distance_xyz[ZZ] = particle_xyz[ZZ] - joint_data[joint[0] + 2];
            length = (DEREF( float, bone_data + 5 ));

            if (length < 0.00001f)  length = 0.00001f;

            length = length * length;
            scalars_xyz[XX] = dot_product(direction_xyz, distance_xyz) / (length); // Height
            scalars_xyz[YY] = dot_product(((float*) frame_data), distance_xyz); // Forward
            scalars_xyz[ZZ] = dot_product(((float*) (frame_data + 12)), distance_xyz); // Side

            if (scalars_xyz[XX] > ATTACHED_SCALAR_MAX)  scalars_xyz[XX] = ATTACHED_SCALAR_MAX;

            if (scalars_xyz[XX] < -ATTACHED_SCALAR_MAX)  scalars_xyz[XX] = -ATTACHED_SCALAR_MAX;

            if (scalars_xyz[YY] > ATTACHED_SCALAR_MAX)  scalars_xyz[YY] = ATTACHED_SCALAR_MAX;

            if (scalars_xyz[YY] < -ATTACHED_SCALAR_MAX)  scalars_xyz[YY] = -ATTACHED_SCALAR_MAX;

            if (scalars_xyz[ZZ] > ATTACHED_SCALAR_MAX)  scalars_xyz[ZZ] = ATTACHED_SCALAR_MAX;

            if (scalars_xyz[ZZ] < -ATTACHED_SCALAR_MAX)  scalars_xyz[ZZ] = -ATTACHED_SCALAR_MAX;

            prt_data->misc.i08[0] = (Uint8) ((scalars_xyz[XX] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);
            prt_data->misc.i08[1] = (Uint8) ((scalars_xyz[YY] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);
            prt_data->misc.i08[2] = (Uint8) ((scalars_xyz[ZZ] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);





            // Figger' the normal scalars for generating the particle's last position...
            particle_xyz = &(prt_data->lastx);
            direction_xyz[XX] = (joint_data[joint[1]] - joint_data[joint[0]]);
            direction_xyz[YY] = (joint_data[joint[1] + 1] - joint_data[joint[0] + 1]);
            direction_xyz[ZZ] = (joint_data[joint[1] + 2] - joint_data[joint[0] + 2]);
            distance_xyz[XX] = particle_xyz[XX] - joint_data[joint[0]];
            distance_xyz[YY] = particle_xyz[YY] - joint_data[joint[0] + 1];
            distance_xyz[ZZ] = particle_xyz[ZZ] - joint_data[joint[0] + 2];
            scalars_xyz[XX] = dot_product(direction_xyz, distance_xyz) / (length); // Height
            scalars_xyz[YY] = dot_product(((float*) frame_data), distance_xyz); // Forward
            scalars_xyz[ZZ] = dot_product(((float*) (frame_data + 12)), distance_xyz); // Side

            if (scalars_xyz[XX] > ATTACHED_SCALAR_MAX)  scalars_xyz[XX] = ATTACHED_SCALAR_MAX;
            if (scalars_xyz[XX] < -ATTACHED_SCALAR_MAX)  scalars_xyz[XX] = -ATTACHED_SCALAR_MAX;

            if (scalars_xyz[YY] > ATTACHED_SCALAR_MAX)  scalars_xyz[YY] = ATTACHED_SCALAR_MAX;
            if (scalars_xyz[YY] < -ATTACHED_SCALAR_MAX)  scalars_xyz[YY] = -ATTACHED_SCALAR_MAX;

            if (scalars_xyz[ZZ] > ATTACHED_SCALAR_MAX)  scalars_xyz[ZZ] = ATTACHED_SCALAR_MAX;
            if (scalars_xyz[ZZ] < -ATTACHED_SCALAR_MAX)  scalars_xyz[ZZ] = -ATTACHED_SCALAR_MAX;

            prt_data->misc.i08[3] = (Uint8) ((scalars_xyz[XX] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);
            prt_data->misc.i08[4] = (Uint8) ((scalars_xyz[YY] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);
            prt_data->misc.i08[5] = (Uint8) ((scalars_xyz[ZZ] * ATTACHED_SCALAR_MULTIPLY) + ATTACHED_SCALAR_ADD);
            return ktrue;
        }
    }

    return kfalse;
}

//-----------------------------------------------------------------------------------------------
void particle_draw(PRT_DATA* prt_data)
{
    // <ZZ> This function draws a particle.  Culling should be off.  Blend mode should be
    //      set prior to call.
    float* current_xyz;
    float last_xyz[3];
    float line_xyz[3];
    float offset_xyz[3];
    float up_xyz[3] = {0.0f, 0.0f, 1.0f};
    float height;
    Uint8* texture;
    Uint16 flags;
    float bad_rotation;
    Uint8 number, digit;
    float cosine;
    float sine;
    Uint8 color[4];


    // Figure out the texture...
    texture = prt_data->image;
    if (texture ==  NULL) return;

    texture += 2;
    display_pick_texture(DEREF( Uint32, texture ));


    // Get the particle color...
    color[0] = prt_data->red;
    color[1] = prt_data->green;
    color[2] = prt_data->blue;

    // Modify lighting for falling in pits...
    if (prt_data->z < ROOM_PIT_HIGH_LEVEL)
    {
        if (prt_data->z < ROOM_PIT_LOW_LEVEL)
        {
            color[0] = 0;
            color[1] = 0;
            color[2] = 0;
        }
        else
        {
            number = ((Uint8) 255) - ((Uint8) ((16.0f * (ROOM_PIT_HIGH_LEVEL - prt_data->z))));
            color[0] = color[0] * number >> 8;
            color[1] = color[1] * number >> 8;
            color[2] = color[2] * number >> 8;
        }
    }

    color[3] = prt_data->alpha;
    display_color_alpha(color);



    // Two different styles of rendering...  Line like between current and last points...
    // Or single point with a spinny amount...
    current_xyz = &(prt_data->x);
    flags = prt_data->flags;

    if (flags & PART_SPIN_ON)
    {
        // One point rendering...
        // Can be either normal spinny particle (aligned against camera) or a flat spinny
        // particle (flat on xy plane)
        if (flags & PART_FLAT_ON)
        {
            // Calculate the positions of the corners of the flat particle...
            bad_rotation = prt_data->spin * UINT16_TO_RAD;
            cosine = COS(bad_rotation) * 0.5f;
            sine   = SIN(bad_rotation) * 0.5f;


            // Rotate according to spinny amount...
            line_xyz[XX] = sine;
            line_xyz[YY] = cosine;
            offset_xyz[XX] = cosine;
            offset_xyz[YY] = -sine;


            // Scale by length & width...
            line_xyz[XX] *= (prt_data->length);
            line_xyz[YY] *= (prt_data->length);
            offset_xyz[XX] *= (prt_data->width);
            offset_xyz[YY] *= (prt_data->width);


            // Render the particle...
            last_xyz[ZZ] = current_xyz[ZZ];
            display_start_fan();
            {
                last_xyz[XX] = current_xyz[XX] + line_xyz[XX] - offset_xyz[XX];
                last_xyz[YY] = current_xyz[YY] + line_xyz[YY] - offset_xyz[YY];
                display_texpos(texpos_corner_xy[0]);  display_vertex(last_xyz);

                last_xyz[XX] = current_xyz[XX] + line_xyz[XX] + offset_xyz[XX];
                last_xyz[YY] = current_xyz[YY] + line_xyz[YY] + offset_xyz[YY];
                display_texpos(texpos_corner_xy[1]);  display_vertex(last_xyz);

                last_xyz[XX] = current_xyz[XX] - line_xyz[XX] + offset_xyz[XX];
                last_xyz[YY] = current_xyz[YY] - line_xyz[YY] + offset_xyz[YY];
                display_texpos(texpos_corner_xy[2]);  display_vertex(last_xyz);

                last_xyz[XX] = current_xyz[XX] - line_xyz[XX] - offset_xyz[XX];
                last_xyz[YY] = current_xyz[YY] - line_xyz[YY] - offset_xyz[YY];
                display_texpos(texpos_corner_xy[3]);  display_vertex(last_xyz);
            }
            display_end();
        }
        else
        {
            last_xyz[XX] = rotate_camera_matrix[2] * 0.5f;
            last_xyz[YY] = rotate_camera_matrix[6] * 0.5f;
            last_xyz[ZZ] = rotate_camera_matrix[10] * 0.5f;


            offset_xyz[XX] = rotate_camera_matrix[0] * 0.5f;
            offset_xyz[YY] = rotate_camera_matrix[4] * 0.5f;
            offset_xyz[ZZ] = rotate_camera_matrix[8] * 0.5f;


            // !!!BAD!!!
            // !!!BAD!!!  Need to redo for precomputed sin/cos
            // !!!BAD!!!
            bad_rotation = prt_data->spin * UINT16_TO_RAD;
            cosine = COS(bad_rotation);
            sine = SIN(bad_rotation);


            // Rotate according to spinny amount...
            line_xyz[XX] = sine * offset_xyz[XX] + cosine * last_xyz[XX];
            line_xyz[YY] = sine * offset_xyz[YY] + cosine * last_xyz[YY];
            line_xyz[ZZ] = sine * offset_xyz[ZZ] + cosine * last_xyz[ZZ];
            sine = -sine;
            offset_xyz[XX] = sine * last_xyz[XX] + cosine * offset_xyz[XX];
            offset_xyz[YY] = sine * last_xyz[YY] + cosine * offset_xyz[YY];
            offset_xyz[ZZ] = sine * last_xyz[ZZ] + cosine * offset_xyz[ZZ];


            // Scale by length and width...
            line_xyz[XX] *= (prt_data->length);
            line_xyz[YY] *= (prt_data->length);
            line_xyz[ZZ] *= (prt_data->length);
            offset_xyz[XX] *= (prt_data->width);
            offset_xyz[YY] *= (prt_data->width);
            offset_xyz[ZZ] *= (prt_data->width);


            if (prt_data->flags & PART_NUMBER_ON)
            {
                // Particle is a number particle...
                number = prt_data->number;

                if (number < 10)
                {
                    // Single digit number...  Generate texture coordinates...  Y already filled in...
                    texpos_number_xy[0][XX] = number * 0.0625f;
                    texpos_number_xy[1][XX] = (number + 1) * 0.0625f;
                    texpos_number_xy[2][XX] = texpos_number_xy[1][XX];
                    texpos_number_xy[3][XX] = texpos_number_xy[0][XX];



                    // Render the particle...
                    display_start_fan();
                    {
                        last_xyz[XX] = current_xyz[XX] + line_xyz[XX] - offset_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] + line_xyz[YY] - offset_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] + line_xyz[ZZ] - offset_xyz[ZZ];
                        display_texpos(texpos_number_xy[0]);  display_vertex(last_xyz);

                        last_xyz[XX] = current_xyz[XX] + line_xyz[XX] + offset_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] + line_xyz[YY] + offset_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] + line_xyz[ZZ] + offset_xyz[ZZ];
                        display_texpos(texpos_number_xy[1]);  display_vertex(last_xyz);

                        last_xyz[XX] = current_xyz[XX] - line_xyz[XX] + offset_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] - line_xyz[YY] + offset_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] - line_xyz[ZZ] + offset_xyz[ZZ];
                        display_texpos(texpos_number_xy[2]);  display_vertex(last_xyz);

                        last_xyz[XX] = current_xyz[XX] - line_xyz[XX] - offset_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] - line_xyz[YY] - offset_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] - line_xyz[ZZ] - offset_xyz[ZZ];
                        display_texpos(texpos_number_xy[3]);  display_vertex(last_xyz);
                    }
                    display_end();
                }
                else
                {
                    // Double digit number (limit to 99)
                    if (number > 99) number = 99;

                    offset_xyz[XX] += offset_xyz[XX];
                    offset_xyz[YY] += offset_xyz[YY];
                    offset_xyz[ZZ] += offset_xyz[ZZ];


                    // Draw the high digit first...
                    digit = number / 10;
                    texpos_number_xy[0][XX] = digit * 0.0625f;
                    texpos_number_xy[1][XX] = (digit + 1) * 0.0625f;
                    texpos_number_xy[2][XX] = texpos_number_xy[1][XX];
                    texpos_number_xy[3][XX] = texpos_number_xy[0][XX];

                    display_start_fan();
                    {
                        last_xyz[XX] = current_xyz[XX] + line_xyz[XX] - offset_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] + line_xyz[YY] - offset_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] + line_xyz[ZZ] - offset_xyz[ZZ];
                        display_texpos(texpos_number_xy[0]);  display_vertex(last_xyz);

                        last_xyz[XX] = current_xyz[XX] + line_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] + line_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] + line_xyz[ZZ];
                        display_texpos(texpos_number_xy[1]);  display_vertex(last_xyz);

                        last_xyz[XX] = current_xyz[XX] - line_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] - line_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] - line_xyz[ZZ];
                        display_texpos(texpos_number_xy[2]);  display_vertex(last_xyz);

                        last_xyz[XX] = current_xyz[XX] - line_xyz[XX] - offset_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] - line_xyz[YY] - offset_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] - line_xyz[ZZ] - offset_xyz[ZZ];
                        display_texpos(texpos_number_xy[3]);  display_vertex(last_xyz);
                    }
                    display_end();



                    // Draw the low digit second...
                    digit = number % 10;
                    texpos_number_xy[0][XX] = digit * 0.0625f;
                    texpos_number_xy[1][XX] = (digit + 1) * 0.0625f;
                    texpos_number_xy[2][XX] = texpos_number_xy[1][XX];
                    texpos_number_xy[3][XX] = texpos_number_xy[0][XX];

                    display_start_fan();
                    {
                        last_xyz[XX] = current_xyz[XX] + line_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] + line_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] + line_xyz[ZZ];
                        display_texpos(texpos_number_xy[0]);  display_vertex(last_xyz);

                        last_xyz[XX] = current_xyz[XX] + line_xyz[XX] + offset_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] + line_xyz[YY] + offset_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] + line_xyz[ZZ] + offset_xyz[ZZ];
                        display_texpos(texpos_number_xy[1]);  display_vertex(last_xyz);

                        last_xyz[XX] = current_xyz[XX] - line_xyz[XX] + offset_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] - line_xyz[YY] + offset_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] - line_xyz[ZZ] + offset_xyz[ZZ];
                        display_texpos(texpos_number_xy[2]);  display_vertex(last_xyz);

                        last_xyz[XX] = current_xyz[XX] - line_xyz[XX];
                        last_xyz[YY] = current_xyz[YY] - line_xyz[YY];
                        last_xyz[ZZ] = current_xyz[ZZ] - line_xyz[ZZ];
                        display_texpos(texpos_number_xy[3]);  display_vertex(last_xyz);
                    }
                    display_end();
                }
            }
            else
            {
                // Render the particle...
                display_start_fan();
                {
                    last_xyz[XX] = current_xyz[XX] + line_xyz[XX] - offset_xyz[XX];
                    last_xyz[YY] = current_xyz[YY] + line_xyz[YY] - offset_xyz[YY];
                    last_xyz[ZZ] = current_xyz[ZZ] + line_xyz[ZZ] - offset_xyz[ZZ];
                    display_texpos(texpos_corner_xy[0]);  display_vertex(last_xyz);

                    last_xyz[XX] = current_xyz[XX] + line_xyz[XX] + offset_xyz[XX];
                    last_xyz[YY] = current_xyz[YY] + line_xyz[YY] + offset_xyz[YY];
                    last_xyz[ZZ] = current_xyz[ZZ] + line_xyz[ZZ] + offset_xyz[ZZ];
                    display_texpos(texpos_corner_xy[1]);  display_vertex(last_xyz);

                    last_xyz[XX] = current_xyz[XX] - line_xyz[XX] + offset_xyz[XX];
                    last_xyz[YY] = current_xyz[YY] - line_xyz[YY] + offset_xyz[YY];
                    last_xyz[ZZ] = current_xyz[ZZ] - line_xyz[ZZ] + offset_xyz[ZZ];
                    display_texpos(texpos_corner_xy[2]);  display_vertex(last_xyz);

                    last_xyz[XX] = current_xyz[XX] - line_xyz[XX] - offset_xyz[XX];
                    last_xyz[YY] = current_xyz[YY] - line_xyz[YY] - offset_xyz[YY];
                    last_xyz[ZZ] = current_xyz[ZZ] - line_xyz[ZZ] - offset_xyz[ZZ];
                    display_texpos(texpos_corner_xy[3]);  display_vertex(last_xyz);
                }
                display_end();
            }
        }
    }
    else
    {
        // Two point rendering...
        last_xyz[XX] = prt_data->lastx;
        last_xyz[YY] = prt_data->lasty;
        last_xyz[ZZ] = prt_data->lastz;

        line_xyz[XX] = current_xyz[XX] - last_xyz[XX];
        line_xyz[YY] = current_xyz[YY] - last_xyz[YY];
        line_xyz[ZZ] = current_xyz[ZZ] - last_xyz[ZZ];

        // Change last xyz if fit to length flag is set...
        if (flags & PART_FIT_LENGTH_ON)
        {
            height = vector_length(line_xyz);

            // Scale by length...
            if (height > 0.00001f)
            {
                height /= prt_data->length;
                line_xyz[XX] /= height;
                line_xyz[YY] /= height;
                line_xyz[ZZ] /= height;
                last_xyz[XX] = current_xyz[XX] - line_xyz[XX];
                last_xyz[YY] = current_xyz[YY] - line_xyz[YY];
                last_xyz[ZZ] = current_xyz[ZZ] - line_xyz[ZZ];
            }
        }
        else
        {
            height = prt_data->length;

            if (height > 1.001f)
            {
                line_xyz[XX] *= height;
                line_xyz[YY] *= height;
                line_xyz[ZZ] *= height;
                last_xyz[XX] = current_xyz[XX] - line_xyz[XX];
                last_xyz[YY] = current_xyz[YY] - line_xyz[YY];
                last_xyz[ZZ] = current_xyz[ZZ] - line_xyz[ZZ];
            }
        }



        if (flags & PART_FLAT_ON)
        {
            cross_product(line_xyz, up_xyz, offset_xyz);
        }
        else
        {
            cross_product(line_xyz, camera.fore_xyz, offset_xyz);
        }

        height = vector_length(offset_xyz);

        if (height > 0.00001f)
        {
            height *= 2.0f;
            // Scale by width...
            height /= prt_data->width;
            offset_xyz[XX] /= height;
            offset_xyz[YY] /= height;
            offset_xyz[ZZ] /= height;


            display_start_fan();
            {
                line_xyz[XX] = current_xyz[XX] - offset_xyz[XX];
                line_xyz[YY] = current_xyz[YY] - offset_xyz[YY];
                line_xyz[ZZ] = current_xyz[ZZ] - offset_xyz[ZZ];
                display_texpos(texpos_corner_xy[0]);  display_vertex(line_xyz);

                line_xyz[XX] = current_xyz[XX] + offset_xyz[XX];
                line_xyz[YY] = current_xyz[YY] + offset_xyz[YY];
                line_xyz[ZZ] = current_xyz[ZZ] + offset_xyz[ZZ];
                display_texpos(texpos_corner_xy[1]);  display_vertex(line_xyz);

                line_xyz[XX] = last_xyz[XX] + offset_xyz[XX];
                line_xyz[YY] = last_xyz[YY] + offset_xyz[YY];
                line_xyz[ZZ] = last_xyz[ZZ] + offset_xyz[ZZ];
                display_texpos(texpos_corner_xy[2]);  display_vertex(line_xyz);

                line_xyz[XX] = last_xyz[XX] - offset_xyz[XX];
                line_xyz[YY] = last_xyz[YY] - offset_xyz[YY];
                line_xyz[ZZ] = last_xyz[ZZ] - offset_xyz[ZZ];
                display_texpos(texpos_corner_xy[3]);  display_vertex(line_xyz);
            }
            display_end();
        }
    }
}

//-----------------------------------------------------------------------------------------------
void particle_draw_below_water()
{
    // <ZZ> This function draws all of the particles in the room that are below the water level...
    Uint16 i;


    // Change some display settings...
    display_cull_off();
    display_zbuffer_write_off();



    // Draw all of the transparent alpha particles...
    display_blend_trans();
    repeat(i, MAX_PARTICLE)
    {
        if (!VALID_PRT(i)) continue;

        if (!(main_prt_data[i].flags & (PART_LIGHT_ON)) && (main_prt_data[i].z <= room_water_level) && !(main_prt_data[i].flags & (PART_AFTER_WATER)))
        {
            if (main_prt_data[i].alpha < 255)
            {
                particle_draw(main_prt_data + i);
            }
        }
    }


    // Draw all of the solid alpha particles...
    display_blend_trans();
    repeat(i, MAX_PARTICLE)
    {
        if (!VALID_PRT(i)) continue;

        if (!(main_prt_data[i].flags & (PART_LIGHT_ON)) && (main_prt_data[i].z <= room_water_level) && !(main_prt_data[i].flags & (PART_AFTER_WATER)))
        {
            if (main_prt_data[i].alpha > 254)
            {
                particle_draw(main_prt_data + i);
            }
        }
    }


    // Draw all of the light particles...
    display_blend_light();
    repeat(i, MAX_PARTICLE)
    {
        if (!VALID_PRT(i)) continue;

        if ((main_prt_data[i].flags & PART_LIGHT_ON) && (main_prt_data[i].z <= room_water_level) && !(main_prt_data[i].flags & (PART_AFTER_WATER)))
        {
            particle_draw(main_prt_data + i);
        }

    }


    // Reset the video stuff to what it was...
    display_zbuffer_write_on();
    display_cull_on();
}

//-----------------------------------------------------------------------------------------------
void particle_draw_above_water()
{
    // <ZZ> This function draws all of the particles in the room that are above the water level...
    Uint16 i;


    // Change some display settings...
    display_cull_off();
    display_zbuffer_write_off();


    // Draw all of the transparent alpha particles...
    display_blend_trans();
    repeat(i, MAX_PARTICLE)
    {
        if (!VALID_PRT(i)) continue;

        if (!(main_prt_data[i].flags & (PART_LIGHT_ON)) && ((main_prt_data[i].z > room_water_level) || (main_prt_data[i].flags & (PART_AFTER_WATER))))
        {
            if (main_prt_data[i].alpha < 255)
            {
                particle_draw(main_prt_data + i);
            }
        }

    }


    // Draw all of the solid alpha particles...
    display_blend_trans();
    repeat(i, MAX_PARTICLE)
    {
        if (!VALID_PRT(i)) continue;

        if (!(main_prt_data[i].flags & (PART_LIGHT_ON)) && ((main_prt_data[i].z > room_water_level) || (main_prt_data[i].flags & (PART_AFTER_WATER))))
        {
            if (main_prt_data[i].alpha > 254)
            {
                particle_draw(main_prt_data + i);
            }
        }

    }


    // Draw all of the light particles...
    display_blend_light();
    repeat(i, MAX_PARTICLE)
    {
        if (!VALID_PRT(i)) continue;

        if ((main_prt_data[i].flags & PART_LIGHT_ON) && ((main_prt_data[i].z > room_water_level) || (main_prt_data[i].flags & (PART_AFTER_WATER))))
        {
            particle_draw(main_prt_data + i);
        }
    }


    // Reset the video stuff to what it was...
    display_zbuffer_write_on();
    display_cull_on();
}

//-----------------------------------------------------------------------------------------------
void particle_update_all()
{
    // <ZZ> This function moves the particles in the room, calls their refreshes, updates
    //      timers, etc.
    Uint16 i, j, character, target, stuckto;
    PRT_DATA* prt_data;
    Uint16 flags;
    float distance, collide_distance;
    Uint8* data;
    Uint8* frame_data;
    Uint8* base_model_data;
    CHR_DATA* chr_data;
    float x, y, z;
    float* joint_data;
    Uint16 joint[2];
    Uint16 num_bone, bone, num_base_model, num_bone_frame, frame;
    Uint8 base_model;
    Uint8 frame_event_flags;
    float scalar_xyz[3];
    Uint16 fast_count;

    OBJECT_PTR obj_ptr;

#ifdef CAN_BLOW_SELF_UP
    Uint8 old_team;
#endif


    // Update all of the particles...
    fast_count = 0;
    repeat(i, MAX_PARTICLE)
    {
        if (!VALID_PRT(i)) continue;

        prt_data = main_prt_data + i;
        flags = prt_data->flags;


        // Spin
        prt_data->spin += prt_data->vspin;



        // Update position for stuck particles...
        if (flags & PART_STUCK_ON)
        {
            // Figger out who we're stuck to...
            bone = prt_data->bone;
            character = (prt_data->stuckto);

            if ( VALID_CHR_RANGE(character) )
            {
                chr_data = chr_data_get_ptr( character );

                if ( NULL != chr_data )
                {
                    data = chr_data->model.parts[MODEL_PART_BASE].file;

                    if( data != NULL )
                    {
                        // Get information from the character variables...
                        frame = chr_data->frame;

                        // Start reading the RDY file...
                        data += 3;
                        num_base_model = *data;  data++;
                        num_bone_frame = DEREF( Uint16, data ); data += 2;

                        if (frame >= num_bone_frame) frame = 0;

                        data += (ACTION_MAX << 1);
                        data += (MAX_DDD_SHADOW_TEXTURE);


                        // Go to the current base model
                        frame_data =  DEREF( Uint8*, data + (num_base_model * 20 * DETAIL_LEVEL_MAX) + (frame << 2) );
                        base_model = *(frame_data + 2);
                        data = data + (base_model * 20);
                        base_model_data = DEREF( Uint8*, data );
                        base_model_data += 6;
                        num_bone = DEREF( Uint16, base_model_data );

                        if (bone >= num_bone) bone = 0;


                        // Skip to the bone connection info of the base model...
                        base_model_data = DEREF( Uint8*, data + 16 );
                        base_model_data += bone * 9;
                        joint[0] = DEREF( Uint16, base_model_data + 1 );
                        joint[1] = DEREF( Uint16, base_model_data + 3 );
                        joint[0] *= 3;
                        joint[1] *= 3;


                        // Go to the current frame...
                        frame_data = temp_character_bone_frame[character];

                        if (frame_data)
                        {
                            joint_data = (float*) (frame_data + (num_bone * 24));
                            frame_data += (24 * bone);


                            // Find the particle position...  Not too complicated...
                            scalar_xyz[XX] = (((Sint16) prt_data->misc.i08[0]) - ATTACHED_SCALAR_ADD) * ATTACHED_SCALAR_DIVIDE;
                            scalar_xyz[YY] = (((Sint16) prt_data->misc.i08[1]) - ATTACHED_SCALAR_ADD) * ATTACHED_SCALAR_DIVIDE;
                            scalar_xyz[ZZ] = (((Sint16) prt_data->misc.i08[2]) - ATTACHED_SCALAR_ADD) * ATTACHED_SCALAR_DIVIDE;
                            prt_data->x = joint_data[joint[0]] + (((joint_data[joint[1]] - joint_data[joint[0]])) * scalar_xyz[XX]) + ((DEREF( float, frame_data )) * scalar_xyz[YY]) + ((DEREF( float, frame_data + 12 )) * scalar_xyz[ZZ]);
                            prt_data->y = joint_data[joint[0] + 1] + (((joint_data[joint[1] + 1] - joint_data[joint[0] + 1])) * scalar_xyz[XX]) + ((DEREF( float, frame_data + 4 )) * scalar_xyz[YY]) + ((DEREF( float, frame_data + 16 )) * scalar_xyz[ZZ]);
                            prt_data->z = joint_data[joint[0] + 2] + (((joint_data[joint[1] + 2] - joint_data[joint[0] + 2])) * scalar_xyz[XX]) + ((DEREF( float, frame_data + 8 )) * scalar_xyz[YY]) + ((DEREF( float, frame_data + 20 )) * scalar_xyz[ZZ]);


                            // Find the last particle position...  Not too complicated...
                            scalar_xyz[XX] = (((Sint16) prt_data->misc.i08[3]) - ATTACHED_SCALAR_ADD) * ATTACHED_SCALAR_DIVIDE;
                            scalar_xyz[YY] = (((Sint16) prt_data->misc.i08[4]) - ATTACHED_SCALAR_ADD) * ATTACHED_SCALAR_DIVIDE;
                            scalar_xyz[ZZ] = (((Sint16) prt_data->misc.i08[5]) - ATTACHED_SCALAR_ADD) * ATTACHED_SCALAR_DIVIDE;
                            prt_data->lastx = joint_data[joint[0]] + (((joint_data[joint[1]] - joint_data[joint[0]])) * scalar_xyz[XX]) + ((DEREF( float, frame_data )) * scalar_xyz[YY]) + ((DEREF( float, frame_data + 12 )) * scalar_xyz[ZZ]);
                            prt_data->lasty = joint_data[joint[0] + 1] + (((joint_data[joint[1] + 1] - joint_data[joint[0] + 1])) * scalar_xyz[XX]) + ((DEREF( float, frame_data + 4 )) * scalar_xyz[YY]) + ((DEREF( float, frame_data + 16 )) * scalar_xyz[ZZ]);
                            prt_data->lastz = joint_data[joint[0] + 2] + (((joint_data[joint[1] + 2] - joint_data[joint[0] + 2])) * scalar_xyz[XX]) + ((DEREF( float, frame_data + 8 )) * scalar_xyz[YY]) + ((DEREF( float, frame_data + 20 )) * scalar_xyz[ZZ]);
                        }
                    }
                }
                else
                {
                    // Poof particle...  Either character it was attached to poof'd, or that character's model has become invalid...
                    // Should automatically get poofed when obj_destroy() is called for character, so this is a double check...
                    obj_destroy( ptr_set_type(&obj_ptr, OBJECT_PRT, prt_data) );
                }
            }
        }
        // Update motion...
        else
        {
            // Do water collisions...
            if ((prt_data->z) < room_water_level && (flags & PART_WALL_COLLISION_ON))
            {
                // Particle is now in water...
                if (!(flags & PART_IN_WATER))
                {
                    // But wasn it in water last frame?
                    prt_data->event = prt_data->is_spawning ? EVENT_SPAWNED_IN_WATER : EVENT_HIT_WATER;
                    sf_fast_rerun_script(&(main_prt_data[i].script_ctxt), FAST_FUNCTION_EVENT);
                    prt_data->flags |= PART_IN_WATER;
                }
            }
            else
            {
                // Particle is not in water...
                prt_data->flags &= (UINT16_MAX - PART_IN_WATER);
            }

            // Clear the Just Spawn'd flag...
            prt_data->is_spawning = 0;


            // Remember last_xyz...
            if (prt_data->trail)
            {
                // Trail is set, so the last_xyz gets sluggified...
                distance = prt_data->trail * INV_0xFF;
                collide_distance = 1.0f - distance;
                prt_data->lastx = distance * (prt_data->lastx) + collide_distance * (prt_data->x);
                prt_data->lasty = distance * (prt_data->lasty) + collide_distance * (prt_data->y);
                prt_data->lastz = distance * (prt_data->lastz) + collide_distance * (prt_data->z);
            }
            else
            {
                // Last_xyz is the actual last position (normal way it works)
                prt_data->lastx = prt_data->x;
                prt_data->lasty = prt_data->y;
                prt_data->lastz = prt_data->z;
            }

            // Gravity
            if (flags & PART_GRAVITY_ON)
            {
                prt_data->vz += GRAVITY;

                if (flags & PART_IN_WATER)
                {
                    // Solid objects (affected by gravity and wall collisions) should be slowed by water friction...
                    (prt_data->vx) *= 0.90f;
                    (prt_data->vy) *= 0.90f;
                    (prt_data->vz) *= 0.75f;
                }
            }


            // Motion...
            prt_data->x += prt_data->vx;
            prt_data->y += prt_data->vy;
            prt_data->z += prt_data->vz;
        }



        // Do wall/floor collisions...
        if (flags & PART_WALL_COLLISION_ON)
        {
            x = prt_data->x;
            y = prt_data->y;
            z = room_heightmap_height(roombuffer, x, y);

            if (z > ((prt_data->z) - (prt_data->colsize)))
            {
                // Bottom of particle (gotten using colsize) is below the floor...
                x = prt_data->vz;  // vz...
                ABS(x);

                if (z > ((prt_data->z) + x))
                {
                    // Top of particle (gotten using vz) is also below the floor...
                    // That sounds like a wall to me...
                    prt_data->event = EVENT_HIT_WALL;
                    sf_fast_rerun_script(&(main_prt_data[i].script_ctxt), FAST_FUNCTION_EVENT);
                }
                else
                {
                    // Looks like a floor...
                    prt_data->event = EVENT_HIT_FLOOR;
                    sf_fast_rerun_script(&(main_prt_data[i].script_ctxt), FAST_FUNCTION_EVENT);
                }
            }
        }




        // Do particle-door collisions...
        if (((flags & PART_CHAR_COLLISION_ON) && ((!(flags & PART_SLOW_ON)) || (main_game_frame&3) == 0)) || (flags & PART_WALL_COLLISION_ON))
        {
            // Size of particle...
            collide_distance = prt_data->width;
            collide_distance *= 0.5f;
            repeat(character, MAX_CHARACTER)
            {
                chr_data = chr_data_get_ptr( character );
                if( NULL == chr_data ) continue;

                if ( (chr_data->mflags & MORE_FLAG_DOOR) && (chr_data->intimer == 0))
                {
                    // The character is an open/closed door...
                    // Open doors stop particless from going through 'em if the particle is too high up (like an
                    // arrow flying through the wall above the door)...
                    x = (prt_data->x) - (chr_data->x);
                    y = (prt_data->y) - (chr_data->y);
                    distance = x * x + y * y;
                    z = collide_distance + chr_data->boxsize;
                    z = z * z;  // Minimum distance between two before a collision (squared to make check easier)

                    if (distance < z)
                    {
                        // Then check the dot to divide the door in two (front and back)...
                        distance = ((chr_data->frontx) * x) + ((chr_data->fronty) * y);
                        distance = collide_distance + 0.5f - distance;

                        if (distance > 0.0f)
                        {
                            // Particle is on the other side of the door...  Might be okay if the
                            // door is open (Knock Out)...
                            z = (((chr_data->z) + chr_data->height) - ((prt_data->z) + (prt_data->colsize))); // Distance between particle and top of door passage (in feet)

                            if (chr_data->daction != ACTION_KNOCK_OUT || (z < 0.0f))
                            {
                                // We have a collision...  Run the bump event...
                                if ((flags & PART_CHAR_COLLISION_ON))
                                {
                                    // Particle is some sort of attack particle...
                                    prt_data->event = EVENT_HIT_CHARACTER;
                                    stuckto = (prt_data->stuckto);

                                    // Particles that are stuck to door shouldn't hit it...  Otherwise arrows keep triggering a hit...
                                    if (stuckto != character)
                                    {
                                        // Check to see if the door is "blocking" (frame event flags on metal grate door - so it triggers a blocked hit animation...)
                                        frame_event_flags = 0;
                                        frame = chr_data->frame;
                                        data = chr_data->model.parts[MODEL_PART_BASE].file;
                                        data += 3;
                                        num_base_model = *data;  data++;
                                        num_bone_frame = DEREF( Uint16, data ); data += 2;
                                        data += (ACTION_MAX << 1);
                                        data += (MAX_DDD_SHADOW_TEXTURE);
                                        data += (num_base_model * 20 * DETAIL_LEVEL_MAX);

                                        if (frame < num_bone_frame)
                                        {
                                            data =  DEREF( Uint8*, data + (frame << 2) );
                                            frame_event_flags = *(data + 1);

                                            if (frame_event_flags & FRAME_EVENT_BLOCK)
                                            {
                                                prt_data->event = EVENT_BLOCKED;
                                            }
                                        }

                                        target = (prt_data->target);
                                        prt_data->target = character;  // Set the .target...
                                        sf_fast_rerun_script(&(main_prt_data[i].script_ctxt), FAST_FUNCTION_EVENT);
                                        prt_data->target = target;
                                    }
                                }
                                else
                                {
                                    // Particle is just a bouncy particle hitting into the door...
                                    prt_data->event = EVENT_HIT_WALL;
                                    sf_fast_rerun_script(&(main_prt_data[i].script_ctxt), FAST_FUNCTION_EVENT);
                                }

                                // Don't bother with any further door collisions for this particle...
                                break;
                            }
                        }
                    }
                }
            }
        }


        // Do particle-character collisions...
        if ((flags & PART_CHAR_COLLISION_ON) && ((!(flags & PART_SLOW_ON)) || (main_game_frame&3) == 0))
        {
            // Size of particle...
            collide_distance = prt_data->width;
            collide_distance *= 0.5f;
            repeat(j, num_character_collision_list)
            {
                // Check teams...
                character = character_collision_list[j];
                chr_data = chr_data_get_ptr( character );
                if( NULL == chr_data ) continue;

                // X and Y collision...
                x = (chr_data->x) - (prt_data->x);
                y = (chr_data->y) - (prt_data->y);
                distance = x * x + y * y;
                z = (chr_data->boxsize);
                z += collide_distance;
                z = z * z;  // distance to check square'd...

                if (distance < z)
                {
                    distance = (chr_data->z) - (prt_data->z);

                    if (distance < 0.0f)
                    {
                        distance = -(distance + ((float) (chr_data->height)));
                    }

                    if (distance < collide_distance)
                    {
                        // Make sure character isn't invincible, or the particle's owner...
                        //                            if(((chr_data->intimer) == 0) && (((prt_data->owner != character) && (prt_data->owner != chr_data->rider)) || ((prt_data->flags & PART_HIT_OWNER_ON) && !(chr_data->vflags & VIRTUE_FLAG_IMMUNE))  ))
                        //(chr_data->intimer == 0)
                        //(prt_data->owner != character)
                        //(prt_data->owner != chr_data->rider)
                        //(prt_data->flags & PART_HIT_OWNER_ON)
                        //(chr_data->vflags & VIRTUE_FLAG_IMMUNE)
                        //((character.invincible == 0) || (part.flags & PART_IGNORE_INTIMER)) && (((particle.owner != character.index) && (particle.owner != character.rider)) || ((particle.flags & PART_HIT_OWNER_ON) && !(character.vflags & VIRTUE_FLAG_IMMUNE)) )




                        if (((chr_data->intimer == 0) || (prt_data->flags & PART_IGNORE_INTIMER)) && (((prt_data->owner != character) && (prt_data->owner != chr_data->rider)) || ((prt_data->flags & PART_HIT_OWNER_ON) && !(chr_data->vflags & VIRTUE_FLAG_IMMUNE)) ))
                        {
                            // Make sure the particle is still on...  May have poofed on a wall...  Also make sure it's still doing collisions...
                            if (VALID_PRT(i) && ((prt_data->flags) & PART_CHAR_COLLISION_ON))
                            {
                                // We have a particle-character collision...  Need some sort of callback...
                                prt_data->event = EVENT_HIT_CHARACTER;

                                log_info(1, "Character %d hit by particle %d", character, i);

                                // But the particle might've been blocked...  Check for that...
                                // Need to get the frame event flags...
                                frame_event_flags = 0;
                                frame = chr_data->frame;
                                log_info(1, "Character frame == %d", frame);
                                log_info(1, "Character action == %d", chr_data->action);
                                log_info(1, "Character daction == %d", chr_data->daction);
                                data = chr_data->model.parts[MODEL_PART_BASE].file;
                                data += 3;
                                num_base_model = *data;  data++;
                                num_bone_frame = DEREF( Uint16, data ); data += 2;
                                data += (ACTION_MAX << 1);
                                data += (MAX_DDD_SHADOW_TEXTURE);
                                data += (num_base_model * 20 * DETAIL_LEVEL_MAX);

                                if (frame < num_bone_frame)
                                {
                                    data =  DEREF( Uint8*, data + (frame << 2) );
                                    frame_event_flags = *(data + 1);
                                }

                                if ((frame_event_flags & FRAME_EVENT_BLOCK) && (chr_data->pttimer) == 0)
                                {
                                    // Character is trying to block particle collision...  Need to check facing against particle velocity...
                                    joint_data = CAST(float*, &(chr_data->frontx));  // Character forward normal vector

                                    if (flags & PART_STUCK_ON)
                                    {
                                        // Use stuck character's fore normal for particle direction...
                                        // stuckto and itarget_data here refers to the attacker (who's weapon the particle is stuck to)...
                                        stuckto = prt_data->stuckto;

                                        if( VALID_CHR_RANGE(stuckto) )
                                        {
                                            scalar_xyz[XX] = chr_data_get_ptr_raw( stuckto )->frontx;
                                            scalar_xyz[YY] = chr_data_get_ptr_raw( stuckto )->fronty;
                                            scalar_xyz[ZZ] = chr_data_get_ptr_raw( stuckto )->frontz;
                                        }
                                    }
                                    else
                                    {
                                        // Use particle velocity for it's direction...
                                        scalar_xyz[XX] = prt_data->vx;
                                        scalar_xyz[YY] = prt_data->vy;
                                        scalar_xyz[ZZ] = prt_data->vz;
                                    }

                                    log_info(1, "Block...  Part vel xyz == %f, %f, %f", scalar_xyz[XX], scalar_xyz[YY], scalar_xyz[ZZ]);
                                    log_info(1, "Block...  Character facing == %f, %f, %f", chr_data->frontx, chr_data->fronty, chr_data->frontz);
                                    distance = dot_product(scalar_xyz, joint_data);
                                    log_info(1, "Block...  Dot == %f", distance);

                                    if (distance < 0.0f)
                                    {
                                        log_info(1, "Block worked...");
                                        // Character is facing particle...  Block was successful...
                                        prt_data->event = EVENT_BLOCKED;

                                        if (chr_data->eflags & ENCHANT_FLAG_MIRROR_DEFLECT)
                                        {
                                            if (!(flags & PART_STUCK_ON))
                                            {
                                                prt_data->event = EVENT_DEFLECTED;
                                            }
                                        }

                                        // Tell character that block worked...
                                        chr_data->event = EVENT_BLOCKED;
                                        sf_fast_rerun_script(&(chr_data->script_ctxt), FAST_FUNCTION_EVENT);
                                    }
                                }

                                if (chr_data->eflags & ENCHANT_FLAG_DEFLECT)
                                {
                                    if (!(flags & PART_STUCK_ON))
                                    {
                                        prt_data->event = EVENT_DEFLECTED;
                                    }
                                }


                                // Call the event function for the particle, 'cause something happened...
#ifdef CAN_BLOW_SELF_UP

                                if (prt_data->owner == character)
                                {
                                    // Temporarily modify the particle's team if it hit it's owner, so it does full damage...
                                    // This should rarely happen...
                                    old_team = prt_data->team;
                                    prt_data->team = 255;
                                }

#endif
                                target = (prt_data->target);
                                prt_data->target = character;  // Temporarily set the .target...
                                sf_fast_rerun_script(&(main_prt_data[i].script_ctxt), FAST_FUNCTION_EVENT);
                                prt_data->target = target;
#ifdef CAN_BLOW_SELF_UP

                                if (prt_data->owner == character)
                                {
                                    // Reset the team...
                                    prt_data->team = old_team;
                                }

#endif

                                if (!VALID_PRT(i))
                                {
                                    // Handle poofing nicely...
                                    j = num_character_collision_list;
                                }
                            }
                        }
                    }
                }
            }
        }





        // Refresh timer...  Need to check if on in case it poofed...
        if (prt_data->timer != 0 && VALID_PRT(i))
        {
            (prt_data->timer)--;

            if (prt_data->timer == 0)
            {
                // Call the refresh function
                sf_fast_rerun_script(&(main_prt_data[i].script_ctxt), FAST_FUNCTION_REFRESH);
            }
        }


        // Event timer...  Need to check if on in case it poofed...
        if (prt_data->evtimer != 0 && VALID_PRT(i))
        {
            (prt_data->evtimer)--;

            if (prt_data->evtimer == 0)
            {
                // Call the event handler...
                prt_data->event = EVENT_TIMER;
                sf_fast_rerun_script(&(main_prt_data[i].script_ctxt), FAST_FUNCTION_EVENT);
            }
        }



        // Keep working on the same particle if it's flag'd to move extra fast...
        if (flags & PART_FAST_ON)
        {
            fast_count++;

            if (fast_count < 1000)
            {
                i--;
            }
            else
            {
                // Uh, oh...  This particle was updated 1000 times, but it still hasn't
                // Poof'd...  That's like a Shotgonne shootin' straight up or something...
                // We'd better poof it automagically to make sure it's okay...
                obj_destroy( ptr_set_type(&obj_ptr, OBJECT_PRT, prt_data) );
                fast_count = 0;
            }
        }
        else
        {
            fast_count = 0;
        }
    }

}

//-----------------------------------------------------------------------------------------------
