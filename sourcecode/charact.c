// <ZZ> This file has stuff for moving characters and stuff.  Stuff.

#include "object.h"

#define CLIMB_HEIGHT 1.75f
#define WALL_COLLISION_DELAY 20
#define BOUNCE_SCALE -0.35f

Uint16 character_collision_list[MAX_CHARACTER];
Uint16 num_character_collision_list = 0;
Uint16 character_door_collision_list[MAX_CHARACTER];
Uint16 num_character_door_collision_list = 0;

//-----------------------------------------------------------------------------------------------
Uint8 action_can_turn[ACTION_MAX];
void character_action_setup()
{
    // <ZZ> Sets up an array...
    Uint16 i;

    repeat(i, ACTION_MAX)
    {
        action_can_turn[i] = ktrue;
    }
    action_can_turn[ACTION_STUN_BEGIN] = kfalse;
    action_can_turn[ACTION_STUN] = kfalse;
    action_can_turn[ACTION_STUN_END] = kfalse;
    action_can_turn[ACTION_KNOCK_OUT_BEGIN] = kfalse;
    action_can_turn[ACTION_KNOCK_OUT] = kfalse;
    action_can_turn[ACTION_KNOCK_OUT_STUN] = kfalse;
    action_can_turn[ACTION_KNOCK_OUT_END] = kfalse;
    action_can_turn[ACTION_BLOCK_BEGIN] = kfalse;
    action_can_turn[ACTION_BLOCK] = kfalse;
    action_can_turn[ACTION_BLOCK_END] = kfalse;
    action_can_turn[ACTION_SPECIAL_4] = kfalse;

    action_can_turn[ACTION_SPECIAL_10] = kfalse;
    action_can_turn[ACTION_SPECIAL_11] = kfalse;
    action_can_turn[ACTION_SPECIAL_12] = kfalse;
}

//-----------------------------------------------------------------------------------------------
void character_collide_all()
{
    // <ZZ> Makes a list of all the characters that need to be checked for collisions...
    //      Also does character-character collisions...
    Uint16 i, j;
    Uint16 check;
    Uint16 character;
    CHR_DATA *current_chr, *collide_chr;
    float distance;
    float collide_distance;
    float disx, disy, disz;
    float clearance;
    Uint8* mount_rdy_file;


    // Start by building our collision lists...
    num_character_collision_list = 0;
    num_character_door_collision_list = 0;
    repeat(i, MAX_CHARACTER)
    {
        CHR_DATA * pchr = chr_data_get_ptr( i );
        if (NULL == pchr) continue;

        // Check no collision flag...
        if (!(pchr->flags & CHAR_NO_COLLISION))
        {
            // Check mounted character...
            if (pchr->mount == UINT16_MAX)
            {
                // Character should collide with doors...
                character_door_collision_list[num_character_door_collision_list] = i;
                num_character_door_collision_list++;

                // Check no collision timer...
                if (pchr->nctimer == 0)
                {
                    character_collision_list[num_character_collision_list] = i;
                    num_character_collision_list++;
                }
            }
        }
    }

    // Check each door against each normal character...  (Doors shouldn't be in collision list...  Should be flag'd as CHAR_NO_COLLISION)
    global_room_changed = kfalse;
    repeat(check, MAX_CHARACTER)
    {
        current_chr = chr_data_get_ptr( check );
        if( NULL == current_chr ) continue;

        if ( 0 == (current_chr->mflags & MORE_FLAG_DOOR) ) continue;

        // The check character is an open/closed door, and we need to make sure no character passes it...
        // Open doors stop characters from going through 'em if the character is too high up (like a
        // dragon flying through the wall above the door)...
        repeat(j, num_character_door_collision_list)
        {
            character = character_door_collision_list[j];
            collide_chr = chr_data_get_ptr( character );
            if( NULL == collide_chr ) continue;

            collide_distance = collide_chr->boxsize + current_chr->boxsize;

            // Do a circular collision check...
            disx = collide_chr->x - current_chr->x;
            disy = collide_chr->y - current_chr->y;
            distance = (disx * disx) + (disy * disy);
            collide_distance *= collide_distance;

            if (distance < collide_distance)
            {
                // Then check the dot to divide the door in two (front and back)...
                distance = (disx * current_chr->frontx) + (disy * current_chr->fronty);
                distance = collide_chr->boxsize + 0.5f - distance;

                if (distance > 0.0f)
                {
                    // Character is on the other side of the door...  Might be okay if the
                    // door is open (Knock Out)...  But character can bump an open door if
                    // far to the back (leaving room) or if too high up...
                    clearance = ((current_chr->z + current_chr->height) - (collide_chr->z + collide_chr->height)); // Distance between head and top of door passage (in feet)

                    if (current_chr->daction != ACTION_KNOCK_OUT || (clearance < 0.0f) || distance > 6.0f)
                    {
                        // We have a collision...
                        // Run the bump event for the door...
                        global_bump_char = character;
                        current_chr->event = EVENT_HIT_CHARACTER;
                        sf_fast_rerun_script( &(current_chr->script_ctxt), FAST_FUNCTION_EVENT);

                        if (global_room_changed)
                        {
                            // We changed rooms...  Skip collisions for right now...
                            //j     = UINT16_MAX;
                            //check = UINT16_MAX;
                            break;
                        }

                        // Run the bump event for the character...  Pretend the door is a wall...
                        if (current_chr->daction != ACTION_KNOCK_OUT)
                        {
                            // Character only gets bump wall event if door is closed...
                            global_bump_char = check;
                            collide_chr->event = EVENT_HIT_WALL;
                            sf_fast_rerun_script(&(collide_chr->script_ctxt), FAST_FUNCTION_EVENT);
                        }

                        if (current_chr->daction != ACTION_KNOCK_OUT || (clearance < 0.0f))
                        {
                            // Bump head if well behind the door...
                            if (distance > 1.0f && clearance < 0.0f)
                            {
                                collide_chr->z += clearance;

                                // But don't push 'em through floor...
                                if (collide_chr->z < collide_chr->floorz)
                                    collide_chr->z = collide_chr->floorz;
                            }

                            // Stop character from moving towards door...  Don't restrict motion away from door...
                            disz = 0.000001f - ((collide_chr->vx * current_chr->frontx) + (collide_chr->vy * current_chr->fronty));

                            if (disz > 0.0f)
                            {
                                collide_chr->vx += current_chr->frontx * disz;
                                collide_chr->vy += current_chr->fronty * disz;
                            }


                            // Pushable characters are thrown back super hard so they don't get pushed through door...
                            if (collide_chr->mflags & MORE_FLAG_PUSHABLE)
                            {
                                collide_chr->x = collide_chr->x + current_chr->frontx * distance;
                                collide_chr->y = collide_chr->y + current_chr->fronty * distance;
                            }
                            else
                            {
                                distance = (collide_chr->flags & CHAR_HOVER_ON) ? distance * 0.002f : distance * 0.20f;
                                collide_chr->vx += current_chr->frontx * distance;
                                collide_chr->vy += current_chr->fronty * distance;
                            }
                        }
                    }
                }
            }
        }
    }

    // Now check each character in the list against every other one...
    if (global_room_changed == kfalse)
    {
        repeat(i, num_character_collision_list)
        {
            check = character_collision_list[i];
            current_chr = chr_data_get_ptr(check);
            if( NULL == current_chr ) continue;
            
            // Normal collision checks...
            for (j = i + 1; j < num_character_collision_list; j++)
            {
                character = character_collision_list[j];
                collide_chr = chr_data_get_ptr(character);
                if( NULL == collide_chr ) continue;

                collide_distance = collide_chr->boxsize + current_chr->boxsize;

                // Do a circular collision check...
                disx = collide_chr->x - current_chr->x;
                disy = collide_chr->y - current_chr->y;
                distance = (disx * disx) + (disy * disy);
                collide_distance *= collide_distance;

                if (distance < collide_distance)
                {
                    collide_distance = (collide_chr->z < current_chr->z) ? (collide_chr->height - current_chr->vz) : (current_chr->height - collide_chr->vz);
                    disz = collide_chr->z - current_chr->z;
                    ABS(disz);

                    if (disz < collide_distance)
                    {
                        // We have a collision...
                        // Run the bump event...
                        global_bump_abort = kfalse;
                        global_bump_char = check;
                        collide_chr->event = EVENT_HIT_CHARACTER;
                        sf_fast_rerun_script(&(collide_chr->script_ctxt), FAST_FUNCTION_EVENT);

                        // Run the bump event for the other character...
                        global_bump_char = character;
                        current_chr->event = EVENT_HIT_CHARACTER;
                        sf_fast_rerun_script( &(current_chr->script_ctxt), FAST_FUNCTION_EVENT);


                        // Does the script say to keep goin'?
                        if (!global_bump_abort)
                        {
                            // Disallow XY movement if overlapping, but not if one char is standin' on another...
                            collide_distance = (collide_distance > 6.0f) ? (collide_distance - 3.0f) : (collide_distance * 0.5f);

                            if (disz < collide_distance)
                            {
                                // One character is well below the other...  Do bump collisions...
                                disz = disx;
                                disx = disy;
                                disy = -disz;
                                distance = SQRT(distance);
                                distance += 0.01f;
                                disx /= distance;
                                disy /= distance;
                                distance = -disy * current_chr->vx + disx * current_chr->vy;

                                if (distance > 0.0f)
                                {
                                    distance = disx * current_chr->vx + disy * current_chr->vy;
                                    current_chr->vx = distance * disx;
                                    current_chr->vy = distance * disy;
                                }

                                distance = -disy * collide_chr->vx + disx * collide_chr->vy;

                                if (distance < 0.0f)
                                {
                                    distance = disx * collide_chr->vx + disy * collide_chr->vy;
                                    collide_chr->vx = distance * disx;
                                    collide_chr->vy = distance * disy;
                                }
                            }
                            else
                            {
                                // One char is standing on the other...  Check for platforms and mounting...
                                if (current_chr->z < collide_chr->z)
                                {
                                    // Check for mounting...
                                    if (collide_chr->vz < 0.0f)
                                    {
                                        if ((current_chr->flags & CHAR_CAN_BE_MOUNTED) && current_chr->rider == UINT16_MAX)
                                        {
                                            if ((collide_chr->flags & CHAR_CAN_RIDE_MOUNT) && !(collide_chr->flags & CHAR_HOVER_ON) && collide_chr->mount == UINT16_MAX)
                                            {
                                                // Looks like character is trying to mount check...
                                                if ((current_chr->team == collide_chr->team || current_chr->team == TEAM_NEUTRAL) && current_chr->pttimer == 0 && collide_chr->hits > 0 && current_chr->hits > 0)
                                                {
                                                    // Teams match...  Attach 'em and run their callbacks...
                                                    mount_rdy_file = (Uint8*) & current_chr->model;

                                                    if (mount_rdy_file && (current_chr->action < ACTION_KNOCK_OUT_BEGIN || current_chr->action > ACTION_KNOCK_OUT_END))
                                                    {
                                                        render_fill_temp_character_bone_number(mount_rdy_file);

                                                        if (temp_character_bone_number[SADDLE_BONE] < 255)
                                                        {
                                                            // Saddle bone is valid...
                                                            current_chr->rider = character;
                                                            collide_chr->mount = check;
                                                            collide_chr->bone = temp_character_bone_number[SADDLE_BONE];
                                                            collide_chr->action = ACTION_RIDE;
                                                            collide_chr->daction = ACTION_RIDE;

                                                            // Give each character a script event...
                                                            collide_chr->event = EVENT_MOUNTED;
                                                            sf_fast_rerun_script(&(collide_chr->script_ctxt), FAST_FUNCTION_EVENT);

                                                            current_chr->event = EVENT_MOUNTED;
                                                            sf_fast_rerun_script( &(current_chr->script_ctxt), FAST_FUNCTION_EVENT);

                                                            // Unpress buttons...
                                                            collide_chr->bflags = 0;
                                                            current_chr->bflags = 0;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ((current_chr->flags & CHAR_PLATFORM_ON) && !(collide_chr->mflags & MORE_FLAG_PLATFALL))
                                    {
                                        // Mount takes precedence over platform...
                                        if (!(current_chr->flags & CHAR_CAN_BE_MOUNTED) || current_chr->rider != UINT16_MAX)
                                        {
                                            // Smaller collision test for platforms stacked on platforms...
                                            collide_distance = collide_chr->boxsize + current_chr->boxsize - 0.5f;
                                            collide_distance *= collide_distance;

                                            if (distance < collide_distance || !(collide_chr->flags & CHAR_PLATFORM_ON))
                                            {
                                                // Make the character stand on the platform
                                                collide_chr->z = current_chr->z + current_chr->height - 0.1f;

                                                if (collide_chr->vz < 0.01f)
                                                {
                                                    collide_chr->flags |= CHAR_ON_PLATFORM;
                                                    collide_chr->vz = -GRAVITY;
                                                    collide_chr->vx += current_chr->vx;
                                                    collide_chr->vy += current_chr->vy;


                                                    // Press event...
                                                    global_bump_char = character;
                                                    current_chr->event = EVENT_JUMPED_ON;
                                                    sf_fast_rerun_script( &(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    // Check for mounting...
                                    if (current_chr->vz < 0.0f)
                                    {
                                        if ((collide_chr->flags & CHAR_CAN_BE_MOUNTED) && collide_chr->rider == UINT16_MAX)
                                        {
                                            if ((current_chr->flags & CHAR_CAN_RIDE_MOUNT) && !(current_chr->flags & CHAR_HOVER_ON) && current_chr->mount == UINT16_MAX)
                                            {
                                                // Looks like check is trying to mount character...
                                                if ((current_chr->team == collide_chr->team || collide_chr->team == TEAM_NEUTRAL) && collide_chr->pttimer == 0 && collide_chr->hits > 0 && current_chr->hits > 0)
                                                {
                                                    // Teams match...  Attach 'em and run their callbacks...
                                                    mount_rdy_file = (Uint8*) & collide_chr->model;

                                                    if (mount_rdy_file && (collide_chr->action < ACTION_KNOCK_OUT_BEGIN || collide_chr->action > ACTION_KNOCK_OUT_END))
                                                    {
                                                        render_fill_temp_character_bone_number(mount_rdy_file);

                                                        if (temp_character_bone_number[SADDLE_BONE] < 255)
                                                        {
                                                            // Saddle bone is valid...
                                                            collide_chr->rider = check;
                                                            current_chr->mount = character;
                                                            current_chr->bone = temp_character_bone_number[SADDLE_BONE];
                                                            current_chr->action = ACTION_RIDE;
                                                            current_chr->daction = ACTION_RIDE;

                                                            // Give each character a script event...
                                                            collide_chr->event = EVENT_MOUNTED;
                                                            sf_fast_rerun_script(&(collide_chr->script_ctxt), FAST_FUNCTION_EVENT);

                                                            current_chr->event = EVENT_MOUNTED;
                                                            sf_fast_rerun_script( &(current_chr->script_ctxt), FAST_FUNCTION_EVENT);

                                                            // Unpress buttons...
                                                            collide_chr->bflags = 0;
                                                            current_chr->bflags = 0;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ((collide_chr->flags & CHAR_PLATFORM_ON) && !(current_chr->mflags & MORE_FLAG_PLATFALL))
                                    {
                                        // Mount takes precedence over platform...
                                        if (!(collide_chr->flags & CHAR_CAN_BE_MOUNTED) || collide_chr->rider != UINT16_MAX)
                                        {
                                            // Smaller collision test for platforms stacked on platforms...
                                            collide_distance = (collide_chr->boxsize + current_chr->boxsize) * 0.5f;
                                            collide_distance *= collide_distance;

                                            if (distance < collide_distance || !(current_chr->flags & CHAR_PLATFORM_ON))
                                            {
                                                // Make the character stand on the platform
                                                current_chr->z = collide_chr->z + collide_chr->height - 0.1f;

                                                if (current_chr->vz < 0.01f)
                                                {
                                                    current_chr->flags |= CHAR_ON_PLATFORM;
                                                    current_chr->vz = -GRAVITY;
                                                    current_chr->vx += collide_chr->vx;
                                                    current_chr->vy += collide_chr->vy;


                                                    // Press event...
                                                    global_bump_char = check;
                                                    collide_chr->event = EVENT_JUMPED_ON;
                                                    sf_fast_rerun_script(&(collide_chr->script_ctxt), FAST_FUNCTION_EVENT);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
    }
}

//-----------------------------------------------------------------------------------------------
#define MAX_SHADOW_DISTANCE 20.0f
void character_shadow_draw_all()
{
    // <ZZ> This function draws all of the character's shadows
    Uint16 i;
    float z, distance, scale;
    Uint8 alpha;
    CHR_DATA *current_chr;

    if (fast_and_ugly_active)
    {
        // No shadows in fast and ugly mode...
        return;
    }


    // !!!BAD!!!
    // !!!BAD!!!  Only do what needs to be done...
    // !!!BAD!!!
    display_color(white);
    display_shade_off();
    display_texture_on();
    display_blend_trans();
    display_zbuffer_on();
    display_zbuffer_write_off();
    display_cull_off();
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!



    // Render shadows...
    repeat(i, MAX_CHARACTER)
    {
        current_chr = chr_data_get_ptr( i );
        if (NULL == current_chr) continue;

        z = current_chr->floorz;

        if (z < room_water_level)
        {
            // Character is over/under water...  Shadow should be on surface
            z = room_water_level;
            distance = current_chr->z - z;
            distance = (distance < 0.0f) ? (distance * -5.0f) : distance;
        }
        else
        {
            // Normal shadow...
            distance = current_chr->z - z;
        }

        if (distance < MAX_SHADOW_DISTANCE)
        {
            if (distance < 0.0f)
            {
                alpha = 255;
                scale = 1.0f;
            }
            else
            {
                scale = (MAX_SHADOW_DISTANCE - distance) / MAX_SHADOW_DISTANCE;
                alpha = (Uint8) (scale * 255.0f);
            }

            render_rdy_character_shadow(current_chr->model.parts[MODEL_PART_BASE].file, current_chr, alpha, scale, z);
        }

    }

    display_zbuffer_write_on();
    display_cull_on();
}

//-----------------------------------------------------------------------------------------------
//#define SHOW_INVINCIBILITY   // Uncomment to show invincibility timer...
Uint8 character_draw_on_pass[MAX_CHARACTER];
void character_draw_all_prime()
{
    // <ZZ> This function sets up the array that tells us the order in which to draw our characters...
    Uint16 i;
    CHR_DATA *current_chr;
    repeat(i, MAX_CHARACTER)
    {
        character_draw_on_pass[i] = 255;

        current_chr = chr_data_get_ptr( i );
        if (NULL == current_chr) continue;

        // Draw solid chars on first pass and fuzzy characters on second and transparent characters on third...
        // Water comes next...
        // Then draw chars that are flagged as "after water", then fuzzy chars that are flagged as "after water", then transparent characters flagged as "after water"...
        character_draw_on_pass[i] = (current_chr->flags & CHAR_FUZZY_ON) ? 1 : 0;
        character_draw_on_pass[i] = (current_chr->alpha < 255) ? 2 : character_draw_on_pass[i];
        character_draw_on_pass[i] = (current_chr->vflags & VIRTUE_FLAG_AFTER_WATER) ? (character_draw_on_pass[i] + 3) : character_draw_on_pass[i];
        character_draw_on_pass[i] = (current_chr->mflags & MORE_FLAG_DOOR) ? 7 : character_draw_on_pass[i];
    }
}
//-----------------------------------------------------------------------------------------------
void character_draw_all(Uint8 after_water, Uint8 draw_only_doors)
{
    // <ZZ> This function draws all of the characters...  After_water should be ktrue if the
    //      function is called after drawing water (called twice now)...

    PSoulfuScriptContext pss;
    Uint16 i, j, k, num_character;
    Uint16 frame;
    Uint8 alpha;
    MODEL_DATA *mdl_data;
    Uint8 pass, pass_end;
    Uint16 timer;
    Uint8 petrify;
    Uint8 eye_frame;
    Uint8 brightness;
    Uint16 num_door_list;
    Uint16 door_list[MAX_CHARACTER];
    Uint16 door_list_order[MAX_CHARACTER];
    Uint16 door_list_final[MAX_CHARACTER];
    float door_list_distance[MAX_CHARACTER];
    float x, y, z;
    CHR_DATA *current_chr;

#ifdef DEVTOOL
    float top_left_xyz[3];
    float bottom_right_xyz[3];
    float boxsize;
    Uint8 height;
#endif


    // Draw goto point
#ifdef DEVTOOL

    if (debug_active && after_water)
    {
        display_color(red);
        display_texture_off();
        display_blend_off();
        repeat(i, MAX_CHARACTER)
        {
            current_chr = chr_data_get_ptr( i );
            if (NULL == current_chr) continue;

            display_start_line();
            display_vertex_xyz(current_chr->gotox, current_chr->gotoy, 0.0f);
            display_vertex_xyz(current_chr->gotox, current_chr->gotoy, 1000.0f);
            display_end();
        }
        display_texture_on();
    }

#endif




    if (draw_only_doors)
    {
        num_door_list = 0;
        repeat(i, MAX_CHARACTER)
        {
            if (character_draw_on_pass[i] == 7)
            {
                // It's a door...
                current_chr = chr_data_get_ptr( i );
                if( NULL != current_chr )
                {
                    x = camera.xyz[XX] - current_chr->x;
                    y = camera.xyz[YY] - current_chr->y;
                    z = camera.xyz[ZZ] - current_chr->z;
                    door_list_distance[num_door_list] = (camera.fore_xyz[XX] * x) + (camera.fore_xyz[YY] * y) + (camera.fore_xyz[ZZ] * z);
                    door_list[num_door_list] = i;
                    door_list_order[num_door_list] = 0;
                    repeat(j, num_door_list)
                    {
                        if (door_list_distance[j] < door_list_distance[num_door_list])
                        {
                            // Current door character should be drawn after the one we're checking...
                            door_list_order[num_door_list]++;
                        }
                        else
                        {
                            // Other way around...
                            door_list_order[j]++;
                        }
                    }
                    num_door_list++;
                }
            }
        }

        // Now figger the final door list...
        repeat(i, num_door_list)
        {
            repeat(j, num_door_list)
            {
                if (door_list_order[j] == i)
                {
                    door_list_final[i] = door_list[j];
                }
            }
        }
    }

#ifdef DEVTOOL

    if (!keyb.down[SDLK_F12])
    {
#endif
        // Solid, Fuzzy, Transparent
        num_character = MAX_CHARACTER;
        pass = 0;
        pass_end = 3;

        if (after_water)
        {
            // Solid after water, Fuzzy after water, Transparent after water
            pass = 3;
            pass_end = 6;
        }

        if (draw_only_doors)
        {
            num_character = num_door_list;
            pass = 7;
            pass_end = 8;
        }

        while (pass < pass_end)
        {
            repeat(k, num_character)
            {
                i = k;

                if (draw_only_doors)
                {
                    i = door_list_final[k];
                }

                if (character_draw_on_pass[i] == pass)
                {
                    current_chr = chr_data_get_ptr( i );
                    if( NULL != current_chr )
                    {
                        pss = &current_chr->script_ctxt;
                        alpha = current_chr->alpha;
                        mdl_data = current_chr->model.parts + MODEL_PART_BASE;
                        eye_frame = (Uint8)current_chr->eframe;
                        frame = current_chr->frame;

                        // Draw the base model...  (May just be bones)
                        if (mdl_data->file != NULL)
                        {
                            onscreen_joint_active = ktrue;  // Figure out if all joints are onscreen before drawing...
                            onscreen_joint_character = i;
                            onscreen_joint_character_data = current_chr;

                            // Setup the lighting information...
                            global_render_light_color_rgb[0] = 255;
                            global_render_light_color_rgb[1] = 255;
                            global_render_light_color_rgb[2] = 255;
                            global_render_light_offset_xy[XX] = 0.75f;  // 0.25
                            global_render_light_offset_xy[YY] = 0.60f;  // 0.38


                            // Modify lighting for poison...
                            timer = current_chr->pstimer;

                            if (timer > 0)
                            {
                                if (timer > 63)
                                {
                                    global_render_light_color_rgb[0] = 0;
                                    global_render_light_color_rgb[2] = 0;
                                }
                                else
                                {
                                    timer = 255 - (timer << 2);
                                    global_render_light_color_rgb[0] = (Uint8) timer;
                                    global_render_light_color_rgb[2] = (Uint8) timer;
                                }
                            }



                            // Modify lighting for damage...
                            timer = current_chr->dmtimer;

                            if (timer > 0)
                            {
                                global_render_light_color_rgb[0] = 255;

                                if (timer > 15)
                                {
                                    global_render_light_color_rgb[1] = 0;
                                    global_render_light_color_rgb[2] = 0;
                                }
                                else
                                {
                                    timer = 255 - (timer << 4);
                                    global_render_light_color_rgb[1] = (global_render_light_color_rgb[1] * ((Uint8) timer)) >> 8;
                                    global_render_light_color_rgb[2] = (global_render_light_color_rgb[2] * ((Uint8) timer)) >> 8;
                                }
                            }

    // Modify lighting for invincibility
    #ifdef DEVTOOL
    #ifdef SHOW_INVINCIBILITY
                            timer = current_chr->intimer;

                            if (timer > 0)
                            {
                                global_render_light_color_rgb[0] = 0;
                                global_render_light_color_rgb[1] = 255;
                                global_render_light_color_rgb[2] = 255;
                            }

    #endif
    #endif

                            // Modify lighting for character brightness (script value and falling in pits)...
                            brightness = current_chr->bright;

                            if (current_chr->z < ROOM_PIT_HIGH_LEVEL)
                            {
                                if (current_chr->z < ROOM_PIT_LOW_LEVEL)
                                {
                                    brightness = 0;
                                }
                                else
                                {
                                    brightness -= (Uint8) ((0.0625f * (ROOM_PIT_HIGH_LEVEL - current_chr->z)) * brightness);
                                }
                            }

                            global_render_light_color_rgb[0] = (global_render_light_color_rgb[0] * brightness) >> 8;
                            global_render_light_color_rgb[1] = (global_render_light_color_rgb[1] * brightness) >> 8;
                            global_render_light_color_rgb[2] = (global_render_light_color_rgb[2] * brightness) >> 8;

                            // Petrify stuff...
                            petrify = ((current_chr->pttimer) > 0);
                            render_rdy(&(pss->screen), mdl_data->file, frame, WIN_3D_MODEL, mdl_data->tex_lst, alpha, temp_character_bone_frame[i], petrify, eye_frame);

                            if (onscreen_joint_active)
                            {
                                // Character base model (bones) was onscreen...  Go ahead and actually draw the
                                // character's parts now...
                                onscreen_joint_active = kfalse;

                                // Draw all of the non-animated overlapped parts...  Arms, Head, etc...
                                frame = 0;
                                repeat(j, 7)
                                {
                                    mdl_data = &current_chr->model.parts[MODEL_PART_LEGS+j];
                                    if (mdl_data->file != NULL)
                                    {
                                        render_rdy(&(pss->screen), mdl_data->file, frame, WIN_3D_MODEL, mdl_data->tex_lst, alpha, temp_character_bone_frame[i], petrify, eye_frame);
                                    }
                                }

                                // Draw the animated mouth...
                                frame = current_chr->mframe;
                                mdl_data = current_chr->model.parts + MODEL_PART_MOUTH;

                                if (mdl_data->file != NULL)
                                {
                                    render_rdy(&(pss->screen), mdl_data->file, frame, WIN_3D_MODEL, mdl_data->tex_lst, alpha, temp_character_bone_frame[i], petrify, eye_frame);
                                }

                                // Draw the animated eyes...
                                frame = current_chr->eframe;
                                mdl_data = current_chr->model.parts + MODEL_PART_EYES;

                                if (mdl_data->file != NULL)
                                {
                                    render_rdy(&(pss->screen), mdl_data->file, frame, WIN_3D_MODEL, mdl_data->tex_lst, alpha, temp_character_bone_frame[i], petrify, eye_frame);
                                }

                                // Draw the left and right and left2 and right2 weapons...  Only drawn if setup in character's model
                                // data, and if character's model has the required bone...
                                repeat(j, 4)
                                {
                                    mdl_data = current_chr->model.parts + MODEL_PART_LEFT + j;
                                    if (mdl_data->file != NULL && temp_character_bone_number[j+1] < 255)
                                    {
                                        // Hand held weapons have 3 frames...  (should be 3 independent base models...)
                                        //    Frame 0 is when holstered.
                                        //    Frame 1 is when taken out (reversal for shields) or attacking (swipe for weapons)
                                        //    Frame 2 is when attacking (swipe thing) (shields don't need this...)
                                        if (current_chr->pttimer > 0)
                                        {
                                            // Petrified characters don't have swipe thing...  But do have weapon on back thing...
                                            temp_character_frame_event &= 128;
                                        }

                                        switch (j&1)
                                        {
                                            case 0:
                                                frame = (temp_character_frame_event & FRAME_EVENT_LEFT) ? 2 : (temp_character_frame_event >> 7);
                                                break;
                                            case 1:
                                                frame = (temp_character_frame_event & FRAME_EVENT_RIGHT) ? 2 : (temp_character_frame_event >> 7);
                                                break;
                                        }

                                        matrix_good_bone(pss->matrix, temp_character_bone_number[j+1], temp_character_bone_frame[i], current_chr);
                                        render_generate_model_world_data(mdl_data->file, frame, pss->matrix, fourthbuffer);  // Generate new bone frame in fourthbuffer
                                        render_rdy(&(pss->screen), mdl_data->file, frame, WIN_3D_MODEL, mdl_data->tex_lst, alpha, fourthbuffer, petrify, 0);
                                    }
                                }

                                // Reset line color (may've changed if highlighted...)
                                line_color[0] = 0;
                                line_color[1] = 0;
                                line_color[2] = 0;
                            }
                        }
                    }
                }
            }
            pass++;
        }

        onscreen_joint_active = kfalse;
#ifdef DEVTOOL
    }
    else
    {
        if (after_water)
        {
            repeat(i, MAX_CHARACTER)
            {
                current_chr = chr_data_get_ptr( i );
                if (NULL == current_chr) continue;

                if (!(current_chr->flags & CHAR_NO_COLLISION))
                {
                    top_left_xyz[XX] = current_chr->x;
                    top_left_xyz[YY] = current_chr->y;
                    top_left_xyz[ZZ] = current_chr->z;
                    bottom_right_xyz[XX] = current_chr->x;
                    bottom_right_xyz[YY] = current_chr->y;
                    bottom_right_xyz[ZZ] = current_chr->z;
                    boxsize = current_chr->boxsize;
                    height = current_chr->height;
                    top_left_xyz[XX] -= boxsize;
                    top_left_xyz[YY] -= boxsize;
                    top_left_xyz[ZZ] += height;
                    bottom_right_xyz[XX] += boxsize;
                    bottom_right_xyz[YY] += boxsize;
                    render_solid_box(red, top_left_xyz[XX], top_left_xyz[YY], top_left_xyz[ZZ], bottom_right_xyz[XX], bottom_right_xyz[YY], bottom_right_xyz[ZZ]);
                }
            }
        }
    }

#endif


    // Reset the depth buffer stuff (funky thing for making layer'd textures not blip...)
    display_depth_scene();
}

//-----------------------------------------------------------------------------------------------
// <ZZ> This is helper stuff for doing character-wall collision detection, and modifying velocity
//      to slide off wall...
#define NUMBER_OF_COLLISION_ITERATIONS 8
float global_collision_vx;
float global_collision_vy;
float global_collision_z;
Uint8 global_collision_fail_count;
Uint8 global_collision_hit_wall;
void char_collision_point(float* position_xyz, float* velocity_xyz, float x_offset, float y_offset)
{
    float x, y, z;
    x = position_xyz[XX] + x_offset + velocity_xyz[XX];
    y = position_xyz[YY] + y_offset + velocity_xyz[YY];
    z = room_heightmap_height(roombuffer, x, y);

    if (z > global_collision_z)
    {
        // Remember our highest floor level...
        global_collision_z = z;
    }

    if (position_xyz[ZZ] < (z - CLIMB_HEIGHT))
    {
        // We hit a wall...
        global_collision_fail_count++;
        global_collision_hit_wall = ktrue;
        global_collision_vx -= x_offset;
        global_collision_vy -= y_offset;
    }
}
void char_floor_point(float* position_xyz)
{
    float x, y, z;
    x = position_xyz[XX];
    y = position_xyz[YY];
    z = room_heightmap_height(roombuffer, x, y);

    if (z > global_collision_z)
    {
        // Remember our highest floor level...
        global_collision_z = z;
    }
}

//-----------------------------------------------------------------------------------------------
void character_update_all(PSoulfuScriptContext pss)
{
    // <ZZ> This function moves the characters around and stuff.
    Uint16 i, j, k;
    Uint8 on_ground, turning;
    float cosine, sine;
    float velx, boxsize;
    float vely;
    float x, y;
    Uint16 rider;
    Uint8 can_swim;
    Uint8* next_frame_data;
    Uint16 num_base_model;
    Uint16 num_bone_frame;
    Uint16 frame;
    Uint8 action;
    Uint8 next_action;
    Uint16 current_facing, desired_facing, spin_rate;
    Sint16 spin_difference;
    float height;
    Uint8 skip;
    Uint8 frame_event_flags;
    bool_t in_water;
    CHR_DATA* mount_data;
    Uint8* child_data;
    Uint16 mount;
    bool_t eye_model_valid, mouth_model_valid, base_model_valid;
    Uint8 dexterity;
    CHR_DATA *current_chr;
    Uint16 * action_first_frame = NULL;
    Uint8  * rdy_data;


    // Update all of the characters...
    repeat(i, MAX_CHARACTER)
    {
        current_chr = chr_data_get_ptr( i );
        if (NULL == current_chr) continue;

        // Make it more readable...

        height = current_chr->height;
        dexterity = current_chr->dex_min;

        if (current_chr->eflags & ENCHANT_FLAG_HASTE)
        {
            // Character is hasten'd...  Counts as +25 dexterity...
            dexterity += 25;
        }

        // Model eye frame stuff...
        if (current_chr->eaction != 0)
        {
            Uint8 * eye_rdy_data;
            Uint16 * eaction_first_frame;

            // Do we have a valid eye model?  Do simple eye quad style animation (standard type)
            // if the eye model only has 1 frame, or if there isn't a model...
            frame = current_chr->eframe;
            eye_rdy_data = current_chr->model.parts[MODEL_PART_EYES].file;
            eye_model_valid = kfalse;

            if (eye_rdy_data)
            {
                // Have an eye model...  Start reading the header...
                eye_rdy_data += 3;
                num_base_model = *eye_rdy_data;  eye_rdy_data++;
                num_bone_frame = DEREF( Uint16, eye_rdy_data ); eye_rdy_data += 2;

                if (num_bone_frame > 1)
                {
                    // We have enough frames to do model style animation...
                    eye_model_valid = ktrue;
                }
            }

            if (eye_model_valid)
            {
                // Continue reading the eye model header...
                eaction_first_frame = (Uint16*) eye_rdy_data;
                eye_rdy_data += (ACTION_MAX << 1);
                eye_rdy_data += (MAX_DDD_SHADOW_TEXTURE);
                eye_rdy_data += (num_base_model * 20 * DETAIL_LEVEL_MAX);

                if (frame < (num_bone_frame - 1))
                {
                    next_frame_data = DEREF(Uint8*, eye_rdy_data + ((frame + 1) << 2));
                    eye_rdy_data =  DEREF( Uint8*, eye_rdy_data + (frame << 2) );
                    action = *eye_rdy_data;
                    next_action = *next_frame_data;

                    // Eye action animation
                    if (action != current_chr->eaction)
                    {
                        // Start a new action...
                        current_chr->eframe = eaction_first_frame[current_chr->eaction];

                        if (current_chr->eframe == UINT16_MAX)
                        {
                            // Default to boning action if action not found...
                            current_chr->eaction = 0;
                            current_chr->eframe = eaction_first_frame[0];
                        }
                    }
                    else
                    {
                        // Increment the frame...
                        current_chr->eframe++;

                        if (next_action != action)
                        {
                            // Return to the default action...
                            current_chr->eaction = 0;
                            current_chr->eframe = eaction_first_frame[0];
                        }
                    }
                }
                else
                {
                    // Return to the default action...
                    current_chr->eaction = 0;
                    current_chr->eframe = eaction_first_frame[0];
                }
            }
            else
            {
                // No eye model, or not enough frames to be valid...
                // Do special cased frame increment...  32 frames...
                // Model probably has an eye texture that has a magic little flag on it
                // to change tex vertex positions based on eye frame...
                if (current_chr->action < ACTION_KNOCK_OUT_BEGIN || current_chr->action > ACTION_KNOCK_OUT_STUN)
                {
                    // Character is awake...
                    if (frame < 31)
                    {
                        // Blinking...
                        current_chr->eframe++;
                    }
                    else
                    {
                        // Return to the default action...
                        current_chr->eaction = 0;
                        current_chr->eframe = 0;
                    }
                }
                else
                {
                    // Character is knock'd out...  Close eyes...
                    current_chr->eframe = 7;
                }
            }

            if( NULL == action_first_frame || NULL != eaction_first_frame )
            {
                action_first_frame = eaction_first_frame;
            }

            if( NULL == rdy_data || NULL != eye_rdy_data )
            {
                eye_rdy_data = eye_rdy_data;
            }
        }



        // Model mouth frame stuff...
        if (current_chr->maction != 0)
        {
            Uint8 * mouth_rdy_data = NULL;
            Uint16 * maction_first_frame = NULL;

            frame = current_chr->mframe;
            mouth_rdy_data = current_chr->model.parts[MODEL_PART_MOUTH].file;

            if (mouth_rdy_data)
            {
                // Have an mouth model...  Start reading the header...
                mouth_rdy_data += 3;
                num_base_model = *mouth_rdy_data;  mouth_rdy_data++;
                num_bone_frame = DEREF( Uint16, mouth_rdy_data ); mouth_rdy_data += 2;

                if (num_bone_frame > 1)
                {
                    // We have enough frames to do model style animation...
                    mouth_model_valid = ktrue;
                }
            }

            if (mouth_rdy_data)
            {
                maction_first_frame = (Uint16*) mouth_rdy_data;
                mouth_rdy_data += (ACTION_MAX << 1);
                mouth_rdy_data += (MAX_DDD_SHADOW_TEXTURE);
                mouth_rdy_data += (num_base_model * 20 * DETAIL_LEVEL_MAX);

                if (frame < (num_bone_frame - 1))
                {
                    next_frame_data = DEREF(Uint8*, mouth_rdy_data + ((frame + 1) << 2));
                    mouth_rdy_data =  DEREF( Uint8*, mouth_rdy_data + (frame << 2) );
                    action = *mouth_rdy_data;
                    next_action = *next_frame_data;

                    // Mouth action animation
                    if (action != current_chr->maction)
                    {
                        // Start a new action...
                        current_chr->mframe = maction_first_frame[current_chr->maction];

                        if (current_chr->mframe == UINT16_MAX)
                        {
                            // Default to boning action if action not found...
                            current_chr->maction = 0;
                            current_chr->mframe = maction_first_frame[0];
                        }
                    }
                    else
                    {
                        // Increment the frame...
                        current_chr->mframe++;

                        if (next_action != action)
                        {
                            // Return to the default action...
                            current_chr->maction = 0;
                            current_chr->mframe = maction_first_frame[0];
                        }
                    }
                }
                else
                {
                    // Return to the default action...
                    current_chr->maction = 0;
                    current_chr->mframe = maction_first_frame[0];
                }
            }

            if( NULL == action_first_frame || NULL != maction_first_frame )
            {
                action_first_frame = maction_first_frame;
            }
            if( NULL == rdy_data || NULL != mouth_rdy_data )
            {
                rdy_data = mouth_rdy_data;
            }
        }



        // Model frame stuff...
        frame_event_flags = 0;
        skip = 1;

        if (current_chr->action != ACTION_BONING && current_chr->pttimer == 0)
        {
            // Character is unpetrified and doing a normal action...
            if (dexterity > 19)
            {
                // Characters with high dexterity skip frames in animation to make it appear faster...
                if (dexterity > 39)
                {
                    if (dexterity > 60)
                    {
                        // 2.0x normal rate...
                        skip++;
                    }
                    else
                    {
                        // 1.5x normal rate...
                        skip += (main_game_frame & 1);
                    }
                }
                else
                {
                    // 1.25x normal rate...
                    skip += ((main_game_frame & 3) == 3);
                }
            }
        }

        while (skip > 0)
        {
            Uint8 * base_rdy_data;
            Uint16 * base_action_first_frame;

            frame    = current_chr->frame;
            base_rdy_data = current_chr->model.parts[MODEL_PART_BASE].file;

            if (base_rdy_data)
            {
                // Have a base model...  Start reading the header...
                base_rdy_data += 3;
                num_base_model = *base_rdy_data;  base_rdy_data++;
                num_bone_frame = DEREF( Uint16, base_rdy_data ); base_rdy_data += 2;

                if (num_bone_frame > 1)
                {
                    // We have enough frames to do model style animation...
                    base_model_valid = ktrue;
                }
            }

            if( base_rdy_data)
            {
                base_action_first_frame = (Uint16*) base_rdy_data;
                base_rdy_data += ACTION_MAX << 1;
                base_rdy_data += MAX_DDD_SHADOW_TEXTURE;
                base_rdy_data += num_base_model * 20 * DETAIL_LEVEL_MAX;

                if (frame < (num_bone_frame - 1))
                {
                    next_frame_data = DEREF(Uint8*, base_rdy_data + ((frame + 1) << 2) );
                    base_rdy_data =  DEREF( Uint8*, base_rdy_data + (frame << 2) );
                }
                else
                {
                    next_frame_data = DEREF( Uint8*, base_rdy_data );

                    if (frame < num_bone_frame)
                    {
                        base_rdy_data = DEREF( Uint8*, base_rdy_data + (frame << 2) );
                    }
                    else
                    {
                        base_rdy_data = next_frame_data;
                    }
                }

                action      = *base_rdy_data;
                next_action = *next_frame_data;

                // Get the frame events flag...
                frame_event_flags |= *(base_rdy_data + 1);

                skip--;
                if (skip > 0)
                {
                    if (frame < (num_bone_frame - 1))
                    {
                        if (next_action == action)
                        {
                            current_chr->frame++;
                        }
                    }
                }
            }

            if( NULL == action_first_frame || NULL != base_action_first_frame )
            {
                action_first_frame = base_action_first_frame;
            }

            if( NULL == rdy_data || NULL != base_rdy_data )
            {
                rdy_data = base_rdy_data;
            }
        }

        if (current_chr->pttimer > 0)
        {
            // Petrified characters don't get frame events...
            frame_event_flags = 0;
        }




        // Make sure self.rider is either valid or UINT16_MAX...
        rider = current_chr->rider;

        if ( VALID_CHR_RANGE(rider) )
        {
            if (!chr_data_get_ptr_raw( rider )->on || chr_data_get_ptr_raw( rider )->mount != i)
            {
                // Character we think is our rider doesn't recognize us as its mount...  Or rider has been poof'd...
                current_chr->rider = UINT16_MAX;
                current_chr->event = EVENT_DISMOUNTED;
                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
            }
        }


        // Check if the character is mounted...
        mount_data = NULL;
        mount = current_chr->mount;

        if (VALID_CHR_RANGE(mount))
        {
            mount_data = chr_data_get_ptr( mount );
            if (NULL != mount_data)
            {
                if (!(mount_data->flags & CHAR_CAN_BE_MOUNTED) || mount_data->pttimer > 0)
                {
                    // Uh oh...  Invalid mount (or mount is petrified)...  Knock 'em off...
                    current_chr->mount = UINT16_MAX;
                    current_chr->action = ACTION_STAND;
                    current_chr->daction = ACTION_STAND;
                    mount_data = NULL;

                    // Give the rider a dismount event...
                    current_chr->event = EVENT_DISMOUNTED;
                    sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                }
            }
            else
            {
                // Invalid mount...  Mount poof'd or somethin'...
                current_chr->mount = UINT16_MAX;
                current_chr->action = ACTION_STAND;
                current_chr->daction = ACTION_STAND;

                // Give the rider a dismount event...
                current_chr->event = EVENT_DISMOUNTED;
                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
            }
        }




        // Only do motion updates on characters who're not mounted...
        if (mount_data)
        {
            // Copy the character's controls over to its mount...
            if (mount != local_player_character[0] && mount != local_player_character[1] && mount != local_player_character[2] && mount != local_player_character[3])
            {
                // But only if the mount is not a player...
                if (current_chr->pttimer == 0)
                {
                    // Non-petrified characters only...
                    mount_data->btimer[0] = (current_chr->btimer[0] > mount_data->btimer[0]) ? current_chr->btimer[0] : mount_data->btimer[0];  current_chr->btimer[0] = (Uint8) 0;
                    mount_data->btimer[1] = (current_chr->btimer[1] > mount_data->btimer[1]) ? current_chr->btimer[1] : mount_data->btimer[1];  current_chr->btimer[1] = (Uint8) 0;
                    mount_data->btimer[2] = (current_chr->btimer[2] > mount_data->btimer[2]) ? current_chr->btimer[2] : mount_data->btimer[2];  current_chr->btimer[2] = (Uint8) 0;
                    mount_data->btimer[3] = (current_chr->btimer[3] > mount_data->btimer[3]) ? current_chr->btimer[3] : mount_data->btimer[3];  current_chr->btimer[3] = (Uint8) 0;
                    mount_data->bflags |= current_chr->bflags;  current_chr->bflags = (Uint8) 0;
                    mount_data->baxis   = current_chr->baxis;   current_chr->baxis = (Uint8) 0;


                    // Goto xy also...
                    (mount_data->gotox) = (current_chr->gotox - (current_chr->x) + (mount_data->x) + (mount_data->gotox)) * 0.5f;
                    (mount_data->gotoy) = (current_chr->gotoy - (current_chr->y) + (mount_data->y) + (mount_data->gotoy)) * 0.5f;
                }
                else
                {
                    // Petrified characters don't make very good drivers...
                    current_chr->btimer[0] = (Uint8) 0;
                    current_chr->btimer[1] = (Uint8) 0;
                    current_chr->btimer[2] = (Uint8) 0;
                    current_chr->btimer[3] = (Uint8) 0;
                    current_chr->bflags = (Uint8) 0;
                    current_chr->baxis = (Uint8) 0;
                    mount_data->btimer[0] = current_chr->btimer[0];
                    mount_data->btimer[1] = current_chr->btimer[1];
                    mount_data->btimer[2] = current_chr->btimer[2];
                    mount_data->btimer[3] = current_chr->btimer[3];
                    mount_data->bflags = current_chr->bflags;
                    mount_data->baxis = current_chr->baxis;
                    (mount_data->gotox) = (mount_data->x);
                    (mount_data->gotoy) = (mount_data->y);
                }
            }


            // Drown timer for rider...
            if (((current_chr->z + height) < room_water_level && room_water_type != WATER_TYPE_LAVA) || (current_chr->z < room_water_level && room_water_type == WATER_TYPE_LAVA && current_chr->def[0] < 4)) // DefFire of 4 prevents lava damage...
            {
                if ((current_chr->hits > 0) && (current_chr->hitsmax < 255) && (!(current_chr->vflags&VIRTUE_FLAG_NO_DROWN) || room_water_type == WATER_TYPE_LAVA))
                {
                    current_chr->timer_drown++;

                    if (current_chr->timer_drown >= drown_delay[current_chr->def[3]]) // Amount of time before damage based on DefFire for lava...
                    {
                        // Uh, oh...  We flipped the drown timer...  That means we gotta do some damage...
                        // Decrement Hits...  Increment Taps...
                        if (current_chr->hits > 0)
                        {
                            current_chr->hits--;
                            current_chr->hitstap++;
                        }


                        // Spawn a damage particle...
                        if (room_water_type != WATER_TYPE_SAND)
                        {
                            SPAWN_INFO si;
                            si.owner   = i;
                            si.subtype = 0;
                            si.target  = current_chr->target;
                            si.team    = current_chr->team;

                            child_data = (Uint8*)obj_spawn(pss, &si, OBJECT_PRT, current_chr->x, current_chr->y, current_chr->z + (current_chr->height - 1), pnumber_file, MAX_PARTICLE);

                            if (child_data)
                            {
                                // Tint the number...
                                if (room_water_type == WATER_TYPE_WATER)
                                {
                                    child_data[48] = 255;
                                    child_data[49] = 255;
                                    child_data[50] = 255;
                                }
                                else
                                {
                                    child_data[48] = 255;
                                    child_data[49] = 50;
                                    child_data[50] = 0;
                                    current_chr->dmtimer = current_chr->timer_drown + 15;
                                }

                                child_data[51] = 1;
                            }
                        }

                        // Reset drown timer...
                        current_chr->timer_drown = 0;


                        // Call the event function if the drowning killed the character...
                        if (current_chr->hits == 0)
                        {
                            current_chr->event = EVENT_DAMAGED;
                            attack_info.index = i;
                            attack_info.spin = current_chr->spin + UINT16_180DEG;
                            sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                        }
                        else
                        {
                            // Otherwise call the drown event, so we know to spawn some bubbles...
                            current_chr->event = EVENT_DROWN;
                            sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                        }
                    }
                }
            }
            else
            {
                // Reset drown timer...
                current_chr->timer_drown = 0;
            }







            // Save our floor height for later...
            global_collision_z = -9999.0f;
            char_floor_point(&current_chr->x);
            current_chr->floorz = global_collision_z;
        }
        else
        {
            // Do the button press/unpress/timers script calls...  Only if not petrified...
            if (current_chr->pttimer == 0)
            {
                repeat(j, 4)
                {
                    // Button hold timer...  Unpress'd when it hits 0...
                    if (current_chr->btimer[j] > 0)
                    {
                        current_chr->btimer[j]--;

                        if (current_chr->btimer[j] == 0)
                        {
                            current_chr->bflags |= (16 << j);
                        }
                    }


                    // Button press scripts...
                    global_button_handled = kfalse;

                    if (current_chr->bflags & (1 << j))
                    {
                        current_chr->event = (Uint8) j;
                        sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_BUTTONEVENT);

                        if (global_button_handled)
                        {
                            current_chr->bflags &= 255 - (1 << j);
                        }
                    }


                    // Button release scripts...
                    global_button_handled = kfalse;

                    if (current_chr->bflags & (16 << j))
                    {
                        // Turn off any presses to same button......
                        current_chr->bflags &= 255 - (1 << j);


                        current_chr->event = ((Uint8) j) | 8;
                        sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_BUTTONEVENT);

                        if (global_button_handled)
                        {
                            current_chr->bflags &= 255 - (16 << j);
                        }
                    }
                }
            }




            // Rotate to face the desired x, y coordinates
            turning = kfalse;

            if (action_can_turn[current_chr->action & (ACTION_MAX-1)] && (current_chr->pttimer == 0))
            {
                // Character is able to turn...
                x = current_chr->gotox;
                y = current_chr->gotoy;
                x -= current_chr->x;
                y -= current_chr->y;

                if ((x*x + y*y) > 1.00f)
                {
                    // Gotoxy is at least a little bit away from our character...  Start doin' a
                    // walk action, if we're standing still...
                    if (current_chr->action == ACTION_STAND)
                    {
                        if (!(current_chr->flags & CHAR_FINISH_ACTION))
                        {
                            // Some characters wait until current action is done before switchin'...
                            current_chr->action = ACTION_WALK;
                        }

                        current_chr->daction = ACTION_WALK;
                    }

                    if (current_chr->action == ACTION_SWIM)
                    {
                        current_chr->action = ACTION_SWIM_FORWARD;
                        current_chr->daction = ACTION_SWIM;
                    }


                    // Gradually spin around to face the desired direction
                    // !!!BAD!!!
                    // !!!BAD!!!  Get rid of atan()...  Maybe do every 16 frames or so (save desired facing in character data)...  Maybe some lookup thing...
                    // !!!BAD!!!
                    desired_facing = (Uint16) (ATAN2(y, x) * RAD_TO_UINT16);
                    current_facing = current_chr->spin;
                    spin_rate = current_chr->vspin;
                    spin_difference = desired_facing - current_facing;

                    if (spin_difference > 0)
                    {
                        if (spin_difference < spin_rate)
                        {
                            (current_chr->spin) = desired_facing;
                        }
                        else
                        {
                            (current_chr->spin) += spin_rate;
                            turning = ktrue;
                        }
                    }
                    else
                    {
                        if (-spin_difference < spin_rate)
                        {
                            (current_chr->spin) = desired_facing;
                        }
                        else
                        {
                            (current_chr->spin) -= spin_rate;
                            turning = ktrue;
                        }
                    }
                }
                else
                {
                    // Very close to goto point...  Should stop...
                    if (current_chr->action == ACTION_WALK)
                    {
                        if (!(current_chr->flags & CHAR_FINISH_ACTION))
                        {
                            // Some characters wait until current action is done before switchin'...
                            current_chr->action = ACTION_STAND;
                        }

                        current_chr->daction = ACTION_STAND;

                        // Give us a hit waypoint event for the script to chew on...
                        rider = current_chr->rider;
                        if (rider == UINT16_MAX)
                        {
                            // Normal character...
                            current_chr->event = EVENT_HIT_WAYPOINT;
                            sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                        }
                        else
                        {
                            // A mount has reached its waypoint...  Give its rider the hit waypoint event...
                            CHR_DATA * prider = chr_data_get_ptr( rider );
                            if ( NULL != prider )
                            {
                                prider->event = EVENT_HIT_WAYPOINT;
                                sf_fast_rerun_script( &(prider->script_ctxt), FAST_FUNCTION_EVENT);
                            }
                            else
                            {
                                // Rider seems to be invalid...
                                current_chr->rider = UINT16_MAX;
                                current_chr->event = EVENT_HIT_WAYPOINT;
                                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                            }
                        }
                    }
                }
            }


            // Update motion...  Gravity...
            if (current_chr->flags & CHAR_GRAVITY_ON)
            {
                current_chr->vz += GRAVITY;
            }


            // Get the size of the bounding box...
            boxsize = current_chr->boxsize;

            if (current_chr->flags & CHAR_LITTLE_BOX)
            {
                boxsize = 1.0f;
            }


            // Setup stuff...
            on_ground = (current_chr->flags & CHAR_ON_PLATFORM) >> 8;  // Usually kfalse, unless we stood on a platform last collision update...
            current_chr->flags &= ~(CHAR_ON_PLATFORM);


            // Do the character-terrain collisions...
            global_collision_hit_wall = kfalse;

            if (current_chr->oflags&OTHER_FLAG_NO_WALL_COLLIDE)
            {
                // Oops...  Looks like we can pass through terrain...  Never mind...
                global_collision_z = -999.0f;
            }
            else
            {
                // Check the heightmap a bunch of times (several samples in concentric circles)...
                repeat(j, NUMBER_OF_COLLISION_ITERATIONS)
                {
                    global_collision_vx = 0.0f;
                    global_collision_vy = 0.0f;
                    global_collision_z = -9999.0f;
                    global_collision_fail_count = 0;

                    k = 0;

                    while (k < NUM_SINE_ENTRIES)
                    {
                        sine = sine_table[k] * boxsize;
                        cosine = cosine_table[k] * boxsize;
                        char_collision_point(&current_chr->x, &current_chr->vx, sine, cosine);
                        k += 256;
                    }


                    // Did we hit a wall?
                    if (global_collision_fail_count)
                    {
                        // Yup we did...  Now we'll have to modify our velocity and do the
                        // checks again until it works...
                        x = (current_chr->vx * current_chr->vx + current_chr->vy * current_chr->vy);
                        y = (global_collision_vx * global_collision_vx + global_collision_vy * global_collision_vy);
                        // Scale the velocity correction amount (global_collision_v's) to match
                        // the old in magnitude (but not direction)...
                        x = SQRT(x / y);
                        global_collision_vx *= x;
                        global_collision_vy *= x;
                        // Then do a weighted average of 'em for our new velocity...
                        current_chr->vx = (current_chr->vx * 0.80f) + (global_collision_vx * 0.40f);
                        current_chr->vy = (current_chr->vy * 0.80f) + (global_collision_vy * 0.40f);
                    }
                    else
                    {
                        // Nope, we're okay to move this character...  Stop iterating through
                        // these collision checks...
                        j = NUMBER_OF_COLLISION_ITERATIONS;
                    }
                }

                if (global_collision_fail_count)
                {
                    // Even our last check failed...  Looks like we'll just have to stop movement...
                    current_chr->vx = 0.0f;
                    current_chr->vy = 0.0f;
                    global_collision_z = current_chr->floorz;  // Use floor height from last position...
                }

                // Save our floor height for later...
                char_floor_point(&current_chr->x);
            }

            current_chr->floorz = global_collision_z;


            // Actually move the character...  At this point our velocity has been approved...  Wherever we end up should be safe for our character to fit in (not bumping any walls)...  At least that's how it's supposed to work...
            current_chr->x += current_chr->vx;
            current_chr->y += current_chr->vy;
            current_chr->z += current_chr->vz;

            // Stop character from passing through ceiling...
            current_chr->z = (current_chr->z > ROOM_CEILING_BUMP_Z) ? ROOM_CEILING_BUMP_Z : current_chr->z;


            // Now check to see if the character hit the floor below 'em...
            //                if(current_chr->z < global_collision_z && velocity_xyz[ZZ] < 0.0f)
            //                if(current_chr->z < (global_collision_z-velocity_xyz[ZZ]) && velocity_xyz[ZZ] < 0.0f)
            if (current_chr->z < (global_collision_z - current_chr->vz + 0.25f) && current_chr->vz < 0.0f)
            {
                // Character is below the ground and sinking...  Raise 'em up...
                current_chr->z = ((current_chr->z - current_chr->vz + 0.25f) > global_collision_z) ? global_collision_z : (current_chr->z - current_chr->vz + 0.25f);
                on_ground = ktrue;
            }



            //                // Now check to see if the character hit the floor below 'em...
            //                if(current_chr->z < (global_collision_z+0.25f) && velocity_xyz[ZZ] < 0.0f)
            //                {
            //                    // Character is below the ground and sinking...  Raise 'em up...
            //                    current_chr->z = ((current_chr->z+0.25f) > global_collision_z) ? global_collision_z : (current_chr->z+0.25f);
            //                    on_ground = ktrue;
            //                }


            // Now check if the character is hovering...
            if (current_chr->flags & CHAR_HOVER_ON)
            {
                y = (room_water_level > global_collision_z) ? room_water_level : global_collision_z;
                y += 1.0f;
                current_chr->hoverz = (current_chr->hoverz < y) ? ((current_chr->hoverz * 0.80f) + (y * 0.20f)) : current_chr->hoverz;

                y = (current_chr->pttimer == 0) ? 0.5f * sine_table[((main_game_frame+(i<<3))&63)<<6] : 0.0f;  // Waver amount, or 0.0 if petrified...
                y += current_chr->hoverz;  // plus hoverz...
                current_chr->vz = ((y - current_chr->z) * 0.01f) + (current_chr->vz * 0.90f);
                current_chr->vz = (on_ground && current_chr->vz < 0.0f) ? (0.0f) : (current_chr->vz);  // Don't allow movement below ground...
                //                    current_chr->hoverz = (current_chr->z < (global_collision_z+1.25f)) ? (current_chr->hoverz+0.025f) : current_chr->hoverz;  // Slowly correct hoverz if we're bumping the ground...
                on_ground = kfalse;
            }



            // Give the character a hit wall event...
            if (global_collision_hit_wall)
            {
                // Check the wall collision delay timer (only give an event if this is 0, to prevent getting lots of events)
                if (current_chr->cdtimer == 0)
                {
                    current_chr->cdtimer = WALL_COLLISION_DELAY;
                    current_chr->event = EVENT_HIT_WALL;
                    sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                }
            }




            // Do water checks...
            in_water = (current_chr->z < room_water_level);

            if (in_water)
            {
                // Character is in water/lava/sand area...
                if (!(current_chr->flags & CHAR_IN_WATER))
                {
                    current_chr->flags |= CHAR_IN_WATER;

                    // Call the water event for making splashes...
                    current_chr->event = EVENT_HIT_WATER;
                    sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);

                    if (room_water_type == WATER_TYPE_LAVA)
                    {
                        if (current_chr->def[0] < 4)
                        {
                            current_chr->dmtimer = drown_delay[current_chr->def[3]] + 15;
                        }
                    }
                }




                // Drown timer...
                if (((current_chr->z + height - 1.0f) < room_water_level && room_water_type != WATER_TYPE_LAVA) || (current_chr->z < room_water_level && room_water_type == WATER_TYPE_LAVA && current_chr->def[0] < 4)) // DefFire of 4 prevents lava damage...
                {
                    if ((current_chr->hits > 0) && (current_chr->hitsmax < 255) && (!(current_chr->vflags&VIRTUE_FLAG_NO_DROWN) || room_water_type == WATER_TYPE_LAVA))
                    {
                        current_chr->timer_drown++;

                        if (current_chr->timer_drown >= drown_delay[current_chr->def[3]]) // Amount of time before damage based on DefFire for lava...
                        {
                            // Uh, oh...  We flipped the drown timer...  That means we gotta do some damage...
                            // Decrement Hits...  Increment Taps...
                            if (current_chr->hits > 0)
                            {
                                current_chr->hits--;
                                current_chr->hitstap++;
                            }


                            // Spawn a damage particle...
                            if (room_water_type != WATER_TYPE_SAND)
                            {
                                SPAWN_INFO si;
                                si.owner = i;
                                si.subtype = 0;
                                si.target = current_chr->target;
                                si.team   = current_chr->team;

                                child_data = (Uint8*)obj_spawn(pss, &si, OBJECT_PRT, current_chr->x, current_chr->y, current_chr->z + (current_chr->height - 1), pnumber_file, MAX_PARTICLE);

                                if (child_data)
                                {
                                    // Tint the number...
                                    if (room_water_type == WATER_TYPE_WATER)
                                    {
                                        child_data[48] = 255;
                                        child_data[49] = 255;
                                        child_data[50] = 255;
                                    }
                                    else
                                    {
                                        child_data[48] = 255;
                                        child_data[49] = 50;
                                        child_data[50] = 0;
                                        current_chr->dmtimer = current_chr->timer_drown + 15;
                                    }

                                    child_data[51] = 1;
                                }
                            }


                            // Reset drown timer...
                            current_chr->timer_drown = 0;


                            // Call the event function if the drowning killed the character...
                            if (current_chr->hits == 0)
                            {
                                current_chr->event = EVENT_DAMAGED;
                                attack_info.index = i;
                                attack_info.spin = current_chr->spin + UINT16_180DEG;
                                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                            }
                            else
                            {
                                // Otherwise call the drown event, so we know to spawn some bubbles...
                                current_chr->event = EVENT_DROWN;
                                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                            }
                        }
                    }
                }
                else
                {
                    // Reset drown timer...
                    current_chr->timer_drown = 0;


                    // Give us some ripples...
                    if (((main_game_frame + i)&7) < 1)
                    {
                        current_chr->event = EVENT_RIPPLE;
                        sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                    }
                }



                // Make the character sink or swim depending on flag...
                if (room_water_type == WATER_TYPE_LAVA)
                {
                    current_chr->vz *= 0.60f;
                    on_ground = ktrue;
                }
                else if (room_water_type == WATER_TYPE_SAND)
                {
                    if (current_chr->vz < (GRAVITY* -0.5f))
                    {
                        current_chr->vz = GRAVITY * -0.5f;
                    }

                    on_ground = ktrue;
                }
                else if (NULL != action_first_frame)
                {
                    can_swim = (action_first_frame[ACTION_SWIM] != UINT16_MAX) && (current_chr->hits > 0) && (current_chr->pttimer == 0);

                    if (can_swim)
                    {
                        // Swim action...
                        if (!on_ground && (current_chr->z + height*0.50f) < room_water_level)
                        {
                            current_chr->daction = ACTION_SWIM;
                        }

                        // Float on surface...  Bouy'd up by percentage of height under water...
                        if (height < 2.1f)
                        {
                            y = room_water_level - current_chr->z + 4.0f;
                        }
                        else
                        {
                            y = room_water_level - current_chr->z + 2.20f;
                        }

                        y += 0.25f * sine_table[((main_game_frame+(i<<3))&63)<<6];  // waver amount...
                        current_chr->vz -= (GRAVITY * y * 0.25f);

                        if (current_chr->vz > 2.0f)
                        {
                            // Limit rise rate so characters don't go shooting out of the water...
                            current_chr->vz = 2.0f;
                        }
                    }

                    current_chr->vz *= 0.90f;
                }
            }
            else
            {
                current_chr->flags &= ~(CHAR_IN_WATER);

                if (current_chr->daction == ACTION_SWIM)
                {
                    current_chr->action = ACTION_STAND;
                    current_chr->daction = ACTION_STAND;
                }


                // Reset drown timer...
                current_chr->timer_drown = 0;


                // Pit hurt...
                if ((current_chr->z < ROOM_PIT_HURT_LEVEL) && !(current_chr->flags & CHAR_HOVER_ON))
                {
                    // Call the pit event function...  Should poof character and play scream sound...
                    current_chr->event = EVENT_FELL_IN_PIT;
                    sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                }
            }


            if (on_ground)
            {
                if (current_chr->flags & CHAR_FALL_ON)
                {
                    current_chr->flags &= ~(CHAR_FALL_ON);
                    // Call the event function
                    current_chr->event = EVENT_HIT_FLOOR;
                    sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                }

                current_chr->flags |= CHAR_ON_GROUND;
            }
            else
            {
                current_chr->flags &= ~(CHAR_ON_GROUND);
            }





            // Generate the character's matrix...
            x = 1.0f;
            sine = sine_table[(current_chr->spin)>>4];
            cosine = cosine_table[(current_chr->spin)>>4];


            // Flatten'd characters...
            if (current_chr->flatten < 255)
            {
                x = ((255 - current_chr->flatten) * INV_0x0200) + 1.0f;
                sine *= x;
                cosine *= x;
                x = current_chr->flatten * INV_UINT08_SIZE;
                j = current_chr->flatten + (current_chr->flatten >> 4) + 1;

                if (j > 255) { j = 255; }

                current_chr->flatten = (Uint8) j;
            }


            // Side
            current_chr->sidex = sine;
            current_chr->sidey = -cosine;
            current_chr->sidez = 0.0f;


            // Front
            current_chr->frontx = cosine;
            current_chr->fronty = sine;
            current_chr->frontz = 0.0f;


            // Up
            current_chr->upx = 0.0f;
            current_chr->upy = 0.0f;
            current_chr->upz = x;







            // Give character velocity for next time, based on frame...  If character isn't
            // on the ground, velocity is kept from last frame...
            if (on_ground)
            {
                // Do we need to stop swimming?
                if (current_chr->daction == ACTION_SWIM)
                {
                    current_chr->daction = ACTION_STAND;
                }


                // Get the movement offset from the current frame
                velx = (DEREF( float, rdy_data + 3 ));    // rdy_data is actually pointing to the bone frame, so +3 = X movement offset
                vely = (DEREF( float, rdy_data + 7 ));    // rdy_data is actually pointing to the bone frame, so +7 = Y movement offset

                velx = (velx + (velx * dexterity * 0.02f));
                vely = (vely + (vely * dexterity * 0.02f));

                current_chr->vx = current_chr->sidex * velx + current_chr->frontx * vely;
                current_chr->vy = current_chr->sidey * velx + current_chr->fronty * vely;


                if ((turning && !(current_chr->mflags & MORE_FLAG_FAST_TURN)) || in_water)
                {
                    current_chr->vx *= 0.5f;
                    current_chr->vy *= 0.5f;

                    if (in_water && room_water_type != WATER_TYPE_WATER)
                    {
                        // Sand and lava slow us down more than water...
                        current_chr->vx *= 0.75f;
                        current_chr->vy *= 0.75f;
                    }

                }

                // Bounce off ground just a little bit...
                if (current_chr->vz < 0.0f && !in_water)
                {
                    current_chr->vz *= BOUNCE_SCALE;
                }
            }
            else
            {
                if ((action >= ACTION_JUMP_BEGIN && action <= ACTION_JUMP_END) || (action >= ACTION_DOUBLE_BEGIN && action <= ACTION_DOUBLE_END))
                {
                    // Jumping creature...  Allow direct control with goto point...
                    vely = (DEREF( float, rdy_data + 7 )) * 3.75f;     // rdy_data is actually pointing to the bone frame, so +7 = Y movement offset
                    vely = (vely + (vely * dexterity * 0.02f));

                    x = current_chr->gotox;
                    y = current_chr->gotoy;
                    x -= current_chr->x;
                    y -= current_chr->y;
                    velx = (x * x + y * y);
                    velx = SQRT(velx);
                    velx += 1.0f;  // Prevents divide by 0 and shakiness associated with small goto offset...
                    vely = vely / velx;
                    x *= vely;
                    y *= vely;

                    if (current_chr->flags & CHAR_HOVER_ON)
                    {
                        // Flying characters have custom friction and poor acceleration...
                        current_chr->vx += x * 0.1f;
                        current_chr->vy += y * 0.1f;
                        current_chr->vx *= current_chr->flyfric;  //0.95f;
                        current_chr->vy *= current_chr->flyfric;  //0.95f;
                    }
                    else
                    {
                        // Jumping characters have high friction for better control...
                        current_chr->vx += x;
                        current_chr->vy += y;
                        current_chr->vx *= 0.75f;
                        current_chr->vy *= 0.75f;
                    }
                }
                else
                {
                    // Swimming or Hovering creature...
                    velx = (DEREF( float, rdy_data + 3 )) * 0.025f;    // rdy_data is actually pointing to the bone frame, so +3 = X movement offset
                    vely = (DEREF( float, rdy_data + 7 )) * 0.025f;    // rdy_data is actually pointing to the bone frame, so +7 = Y movement offset
                    velx = (velx + (velx * dexterity * 0.02f));
                    vely = (vely + (vely * dexterity * 0.02f));
                    current_chr->vx += current_chr->sidex * velx + current_chr->frontx * vely;
                    current_chr->vy += current_chr->sidey * velx + current_chr->fronty * vely;
                    current_chr->vx *= 0.975f;
                    current_chr->vy *= 0.975f;
                }
            }
        }




        // Do frame events and callback...
        if (frame_event_flags&15)
        {
            current_chr->event = frame_event_flags;
            sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_FRAMEEVENT);
        }



        // Action animations...
        if (current_chr->action != ACTION_BONING && NULL != action_first_frame)
        {
            // Start a new action if .action isn't what it was before...  Only allow if not petrified...
            if (action != current_chr->action && (current_chr->pttimer == 0))
            {
                // Start a new action...
                current_chr->frame = action_first_frame[current_chr->action];

                if ((current_chr->frame) == UINT16_MAX)
                {
                    // Action wasn't found...
                    if (current_chr->action >= ACTION_BASH_LEFT && current_chr->action <= ACTION_SLASH_RIGHT)
                    {
                        // Tried to do an attack action...  Try a different attack instead...
                        j = current_chr->action & 1;

                        if (action_first_frame[ACTION_BASH_LEFT | j] != UINT16_MAX)
                        {
                            // Bash action is valid...
                            current_chr->action = ACTION_BASH_LEFT | j;
                            current_chr->frame = action_first_frame[current_chr->action];
                        }
                        else if (action_first_frame[ACTION_THRUST_LEFT | j] != UINT16_MAX)
                        {
                            // Thrust action is valid...
                            current_chr->action = ACTION_THRUST_LEFT | j;
                            current_chr->frame = action_first_frame[current_chr->action];
                        }
                        else if (action_first_frame[ACTION_SLASH_LEFT | j] != UINT16_MAX)
                        {
                            // Slash action is valid...
                            current_chr->action = ACTION_SLASH_LEFT | j;
                            current_chr->frame = action_first_frame[current_chr->action];
                        }
                        else
                        {
                            // Default to stand or jump action...
                            if (action_first_frame[ACTION_STAND] != UINT16_MAX)
                            {
                                current_chr->action = ACTION_STAND;
                            }
                            else
                            {
                                current_chr->action = ACTION_JUMP;
                            }

                            current_chr->frame = action_first_frame[current_chr->action];
                        }
                    }
                    else
                    {
                        // Default to stand or jump action...
                        if (action_first_frame[ACTION_STAND] != UINT16_MAX)
                        {
                            current_chr->action = ACTION_STAND;
                        }
                        else
                        {
                            current_chr->action = ACTION_JUMP;
                        }

                        current_chr->frame = action_first_frame[current_chr->action];
                    }
                }
            }
            else
            {
                // Increment the frame...  Only if not petrified...
                if (current_chr->pttimer == 0)
                {
                    current_chr->frame++;

                    if (next_action != current_chr->action)
                    {
                        // Done with this action...  Return to the default action...
                        current_chr->action = current_chr->daction;
                        current_chr->frame = action_first_frame[current_chr->action];

                        if (current_chr->frame == UINT16_MAX)
                        {
                            // Default to stand or jump action
                            if (action_first_frame[ACTION_STAND] != UINT16_MAX)
                            {
                                current_chr->action = ACTION_STAND;
                            }
                            else
                            {
                                current_chr->action = ACTION_JUMP;
                            }

                            current_chr->daction = current_chr->action;
                            current_chr->frame = action_first_frame[current_chr->action];
                        }
                    }
                }
            }
        }

        // !!!BAD!!!
        // !!!BAD!!!
        // !!!BAD!!!
        // !!!BAD!!!
        // !!!BAD!!!
        // !!!BAD!!!



        // Wall collision delay timer
        if (current_chr->cdtimer)
        {
            current_chr->cdtimer--;
        }


        // Refresh timer
        if (current_chr->timer > 0)
        {
            current_chr->timer--;

            if (current_chr->timer == 0)
            {
                // Call the refresh function
                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_REFRESH);
            }
        }



        // Event timer
        if (current_chr->evtimer > 0)
        {
            current_chr->evtimer--;

            if (current_chr->evtimer == 0)
            {
                // Call the event handler...
                current_chr->event = EVENT_TIMER;
                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
            }
        }


        // Secondary event timer
        if (current_chr->setimer > 0)
        {
            current_chr->setimer--;

            if (current_chr->setimer == 0)
            {
                // Call the event handler...
                current_chr->event = EVENT_SECONDARY_TIMER;
                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
            }
        }


        // Thirdiary event timer
        if (current_chr->tetimer > 0)
        {
            current_chr->tetimer--;

            if (current_chr->tetimer == 0)
            {
                // Call the event handler...
                current_chr->event = EVENT_THIRDIARY_TIMER;
                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
            }
        }


        // No collision timer
        if (current_chr->nctimer > 0)
        {
            current_chr->nctimer--;
        }


        // Food timer (food of 0 means don't bother with hunger for this character...)
        if (current_chr->food > 0)
        {
            current_chr->food--;

            // Do hunger damage...
            if (current_chr->food == 0)
            {
                current_chr->food = 3600;  // Give 'em a little while 'til we hurt 'em again...


                if (current_chr->hits > 0)
                {
                    SPAWN_INFO si;
                    si.owner = i;
                    si.subtype = 0;
                    si.target = current_chr->target;
                    si.team   = current_chr->team;

                    // Decrement Hits...  Increment Taps...
                    current_chr->hits--;
                    current_chr->hitstap++;


                    // Spawn a damage particle...
                    child_data = (Uint8*)obj_spawn(pss, &si, OBJECT_PRT, current_chr->x, current_chr->y, current_chr->z + (current_chr->height - 1), pnumber_file, MAX_PARTICLE);

                    if (child_data)
                    {
                        // Tint the number...
                        child_data[48] = 255;
                        child_data[49] = 0;
                        child_data[50] = 0;
                        child_data[51] = 1;
                    }


                    // Call the event function if the hunger killed the character...
                    if (current_chr->hits == 0)
                    {
                        current_chr->event = EVENT_DAMAGED;
                        attack_info.index = i;
                        attack_info.spin = current_chr->spin + UINT16_180DEG;
                        sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                    }
                    else
                    {
                        // Otherwise they get a hunger damaged event...
                        current_chr->event = EVENT_HUNGER_DAMAGED;
                        sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                    }
                }
            }
        }





        // Poison (color) timer
        if (current_chr->pstimer > 0)
        {
            current_chr->pstimer--;


            // Do poison damage...  Every 256 frames...  About once every 4 seconds...
            if ((current_chr->pstimer & 255) == 0)
            {
                if (current_chr->hits > 0 && current_chr->pstimer > 64)
                {
                    SPAWN_INFO si;
                    si.owner = i;
                    si.subtype = 0;
                    si.target = current_chr->target;
                    si.team   = current_chr->team;

                    // Decrement Hits...
                    current_chr->hits--;

                    // Spawn a damage particle...
                    child_data = (Uint8*)obj_spawn(pss, &si, OBJECT_PRT, current_chr->x, current_chr->y, current_chr->z + (current_chr->height - 1), pnumber_file, MAX_PARTICLE);

                    if (child_data)
                    {
                        // Tint the number according to the damage type...
                        child_data[48] = damage_color_rgb[DAMAGE_ACID][0];
                        child_data[49] = damage_color_rgb[DAMAGE_ACID][1];
                        child_data[50] = damage_color_rgb[DAMAGE_ACID][2];
                        child_data[51] = 1;
                    }


                    // Call the event function if the poison killed the character...
                    if (current_chr->hits == 0)
                    {
                        current_chr->event = EVENT_DAMAGED;
                        attack_info.index = current_chr->lasthit;  // self.lasthit
                        attack_info.spin = current_chr->spin + UINT16_180DEG;
                        sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
                    }
                }
            }
        }


        // Petrify (texture) timer
        if (current_chr->pttimer > 0)
        {
            // Character is petrified...
            (current_chr->pttimer)--;

            // Stop movement...
            current_chr->vx = 0.0f;
            current_chr->vy = 0.0f;
        }
        else
        {
            // Character is not petrified...
            // Increase the combo timer...
            current_chr->cbtimer++;

            // Decrease the combo counter (number of times character has been hit) if the combo timer is high enough...
            if (current_chr->cbtimer > 120)
            {
                if (current_chr->combo > 0)
                {
                    current_chr->combo--;
                }

                current_chr->cbtimer = 60;
            }
        }


        // Petrify events...
        if (current_chr->mflags & MORE_FLAG_PETRIFY_ON)
        {
            // Character was petrified last frame...
            if (current_chr->pttimer == 0)
            {
                // Character is no longer petrified...
                current_chr->event = EVENT_UNPETRIFIED;
                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
            }
        }
        else
        {
            // Character wasn't petrified last frame...
            if (current_chr->pttimer > 0)
            {
                // Character is now petrified...
                current_chr->event = EVENT_PETRIFIED;
                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_EVENT);
            }
        }


        // Remember whether we're petrified or not for next frame...
        current_chr->mflags = (current_chr->pttimer > 0) ? (current_chr->mflags | MORE_FLAG_PETRIFY_ON) : (current_chr->mflags & (255 - MORE_FLAG_PETRIFY_ON));


        // Damage (color) timer
        if (current_chr->dmtimer > 0)
        {
            current_chr->dmtimer--;
        }


        // Invincibility timer
        if (current_chr->intimer > 0)
        {
            current_chr->intimer--;
        }


        // Reload left timer (doubles as a reload timer for special 1)
        if (current_chr->rltimer > 0)
        {
            current_chr->rltimer--;
        }


        // Reload right timer (doubles as a reload timer for special 2)
        if (current_chr->rrtimer > 0)
        {
            current_chr->rrtimer--;
        }


        // AI timer
        if (current_chr->aitimer > 0)
        {
            (current_chr->aitimer)--;

            if (current_chr->aitimer == 0)
            {
                // Call the player function...
                sf_fast_rerun_script(&(current_chr->script_ctxt), FAST_FUNCTION_AISCRIPT);
            }
        }

    }
}

//-----------------------------------------------------------------------------------------------
void character_bone_frame_clear()
{
    // <ZZ> This function resets the bone frame list for each character to NULL...
    Uint16 i;
    repeat(i, MAX_CHARACTER)
    {
        temp_character_bone_frame[i] = NULL;
    }
}

//-----------------------------------------------------------------------------------------------
void character_bone_frame_all()
{
    // <ZZ> This function generates world bone frame data for all the characters in the room.
    //      The bone frame data is stored in mainbuffer, and is used to generate vertex
    //      coordinates for object models.  Bone frames should be generated every game cycle...

    PSoulfuScriptContext pss;
    Uint16 i;
    Uint16 frame;
    Uint8* place_to_stick_bone_frame;
    CHR_DATA* mount_data;
    Uint16 mount;
    Uint8 bone;
    CHR_DATA *current_chr;


    place_to_stick_bone_frame = mainbuffer;
    // Handle unmounted characters in first pass...
    repeat(i, MAX_CHARACTER)
    {
        temp_character_bone_frame[i] = NULL;

        current_chr = chr_data_get_ptr(i);
        if (NULL == current_chr) continue;

        pss = &(current_chr->script_ctxt);

        if (current_chr->model.parts[MODEL_PART_BASE].file != NULL)
        {
            mount = current_chr->mount;

            mount_data = chr_data_get_ptr(mount);
            if (mount_data == NULL)
            {
                // We're a normal, unmounted character...
                // Copy the position matrix from the character's data...
                pss->matrix[0]  = current_chr->sidex;
                pss->matrix[1]  = current_chr->sidey;
                pss->matrix[2]  = current_chr->sidez;
                pss->matrix[3]  = current_chr->frontx;
                pss->matrix[4]  = current_chr->fronty;
                pss->matrix[5]  = current_chr->frontz;
                pss->matrix[6]  = current_chr->upx;
                pss->matrix[7]  = current_chr->upy;
                pss->matrix[8]  = current_chr->upz;
                pss->matrix[9]  = current_chr->x;
                pss->matrix[10] = current_chr->y;
                pss->matrix[11] = current_chr->z + CHARACTER_Z_FLOAT;

                // Generate the bone frame data...
                frame = current_chr->frame;
                temp_character_bone_frame[i] = place_to_stick_bone_frame;
                place_to_stick_bone_frame = render_generate_model_world_data(current_chr->model.parts[MODEL_PART_BASE].file, frame, pss->matrix, place_to_stick_bone_frame);
            }
        }
    }

    // Handle mounted characters in second pass...
    repeat(i, MAX_CHARACTER)
    {
        current_chr = chr_data_get_ptr( i );
        if (NULL == current_chr) continue;

        pss = &(current_chr->script_ctxt);

        if (current_chr->model.parts[MODEL_PART_BASE].file != NULL)
        {
            mount = current_chr->mount;

            if ( VALID_CHR_RANGE(mount) )
            {
                bone = current_chr->bone;  // Should've been set when we first mounted other character...
            }

            mount_data = chr_data_get_ptr(mount);
            if (NULL != mount_data)
            {
                // We're a mounted character...
                // Generate the position matrix from the mount's saddle bone...
                matrix_good_bone(pss->matrix, bone, temp_character_bone_frame[mount], mount_data);


                // Set the character's position based on the saddle matrix...
                current_chr->x = pss->matrix[9];
                current_chr->y = pss->matrix[10];
                current_chr->z = pss->matrix[11];
                current_chr->spin = mount_data->spin;



                // Update the character's matrix...
                current_chr->sidex = pss->matrix[0];
                current_chr->sidey = pss->matrix[1];
                current_chr->sidez = pss->matrix[2];
                current_chr->frontx = pss->matrix[3];
                current_chr->fronty = pss->matrix[4];
                current_chr->frontz = pss->matrix[5];
                current_chr->upx = pss->matrix[6];
                current_chr->upy = pss->matrix[7];
                current_chr->upz = pss->matrix[8];


                // Generate the bone frame data...
                frame = current_chr->frame;
                temp_character_bone_frame[i] = place_to_stick_bone_frame;
                place_to_stick_bone_frame = render_generate_model_world_data(current_chr->model.parts[MODEL_PART_BASE].file, frame, pss->matrix, place_to_stick_bone_frame);
            }
        }
    }

}

//-----------------------------------------------------------------------------------------------
#define BUTTON_PRESS            0
#define BUTTON_UNPRESS          1
#define BUTTON_PRESS_AND_HOLD   2
#define BUTTON_HANDLED          3
void character_button_function(Uint16 character, Uint8 code, Uint8 button, Uint8 axis)
{
    // <ZZ> This function stores button press information (for players or AI's) in a character's
    //      data block...  Character's ButtonEvent() script should be called at a later point to
    //      actually use items and set actions and stuff...
    CHR_DATA *current_chr;

    current_chr = chr_data_get_ptr( character );
    if ( NULL != current_chr )
    {
        button &= 3;
        code &= 3;

        switch (code)
        {
        case BUTTON_PRESS:
            current_chr->bflags |= (1 << button);
            current_chr->baxis = axis | (16 << button) | (current_chr->baxis & 240);
            current_chr->btimer[button] = 1;  // Release right away...
            break;
        case BUTTON_UNPRESS:
            current_chr->bflags |= (16 << button);
            current_chr->baxis = axis | (current_chr->baxis & (255 - (16 << button)));
            break;
        case BUTTON_PRESS_AND_HOLD:
            // Axis parameter is really time to hold...  0 means until unpressed...
            current_chr->bflags |= (1 << button);
            current_chr->baxis = (16 << button) | (current_chr->baxis & 240);
            current_chr->btimer[button] = axis;  // Really duration...
            break;
        case BUTTON_HANDLED:
            global_button_handled = ktrue;
            break;
        }
    }
}

//-----------------------------------------------------------------------------------------------
void character_local_player_control(void)
{
    // <ZZ> This function updates the characters controlled by the local players, so that
    //      their gotoxy's & stuff are set correctly...
    Uint16 i;
    Uint8 button;
    Uint16 character;
    CHR_DATA *current_chr;
    Uint8 axis;
    float x, y;


    repeat(i, MAX_LOCAL_PLAYER)
    {
        character = local_player_character[i];

        current_chr = chr_data_get_ptr( character );
        if ( NULL != current_chr )
        {
            if (player_device[i].controls_active)
            {
                // Generate gotoxy, based on camera rotation...  Map_side_xy shouldn't have gotten corrupted
                // by window cameras...
                x = current_chr->x;
                y = current_chr->y;
                x += (player_device[i].xy[XX] * map_side_xy[XX] - player_device[i].xy[YY] * map_side_xy[YY]) * 10.0f;
                y += (player_device[i].xy[XX] * map_side_xy[YY] + player_device[i].xy[YY] * map_side_xy[XX]) * 10.0f;
                current_chr->gotox = x;
                current_chr->gotoy = y;


                // Turn off the AI timer...
                (current_chr->aitimer) = 0;


                // Transfer player button presses into character data...
                axis  = ((player_device[i].xy[YY] < -0.10f) << 3);  // 8 = Up
                axis |= ((player_device[i].xy[YY] > 0.10f)  << 2);  // 4 = Down
                axis |= ((player_device[i].xy[XX] < -0.10f) << 1);  // 2 = Left
                axis |= ((player_device[i].xy[XX] > 0.10f)      );  // 1 = Right
                repeat(button, 4)
                {
                    if (player_device[i].button_pressed[button])
                    {
                        character_button_function(character, BUTTON_PRESS, button, axis);
                        player_device[i].button_pressed[button] = kfalse;
                        current_chr->btimer[button] = 0;  // Make it not release right away...
                    }

                    if (player_device[i].button_unpressed[button])
                    {
                        character_button_function(character, BUTTON_UNPRESS, button, axis);
                        player_device[i].button_unpressed[button] = kfalse;
                        player_device[i].button_pressed[button] = kfalse;
                    }
                }
                current_chr->baxis = (current_chr->baxis & 240) | (axis & 15);  // Make sure baxis is always set for players...
            }
            else
            {
                // Stop the character from moving...
                x = current_chr->x;
                y = current_chr->y;
                current_chr->gotox = x;
                current_chr->gotoy = y;

                // Turn off the AI timer...
                current_chr->aitimer = 0;

                // Don't allow button presses just yet...
                repeat(button, 4)
                {
                    player_device[i].button_unpressed[button] = kfalse;
                    player_device[i].button_pressed[button] = kfalse;
                }
            }
        }
        else
        {
            // Character is defeated...
            repeat(button, 4)
            {
                player_device[i].button_unpressed[button] = kfalse;
                player_device[i].button_pressed[button]   = kfalse;
            }
        }

    }
}

//-----------------------------------------------------------------------------------------------
void character_refresh_items_all()
{
    // <ZZ> Calls the Refresh() script for each of a character's 4 items (weapons & actions)...
    Uint16 i;
    Uint8 slot;
    CHR_DATA *current_chr;
    Uint16 item_type;
    Uint8* item_script;


    repeat(i, MAX_CHARACTER)
    {
        // Call the refresh function for each of the character's four weapons/specials...
        current_chr = chr_data_get_ptr( i );
        if (NULL == current_chr) continue;


        // Only allow if not petrified and not mounted and not in deep water...
        if ((current_chr->pttimer == 0) && (current_chr->mount == UINT16_MAX) && ((current_chr->z > room_water_level)   || (room_water_type != WATER_TYPE_WATER)   ||   ((current_chr->z + (current_chr->height * 0.50f)) > room_water_level && (current_chr->flags & CHAR_ON_GROUND))))
        {
            repeat(slot, 4)
            {
                item_type = current_chr->eqslot[slot];
                item_script = item_list[item_type].script;

                if (item_script)
                {
                    // Call the refresh function...  Used to make flame effects & stuff on weapons...
                    g_sf_scr.item_index = item_type;
                    g_sf_scr.item_bone_name = slot + 1;
                    sf_fast_run_script(item_script, FAST_FUNCTION_REFRESH, (Uint8*)current_chr);
                }
            }
        }
    }

}


//-----------------------------------------------------------------------------------------------
