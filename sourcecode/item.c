// <ZZ> This file contains functions related to player inventory & item registry...

ITEM_INFO item_list[MAX_ITEM_TYPE];

Uint16 weapon_setup_grip = 0;  // For scripted ModelAssign() call to weapons...

// Item flags...
#define ITEM_FLAG_WEAPON     1
#define ITEM_FLAG_SHIELD     2
#define ITEM_FLAG_SPECIAL    4
#define ITEM_FLAG_HELM       8
#define ITEM_FLAG_BODY       16
#define ITEM_FLAG_LEGS       32
#define ITEM_FLAG_SPELL      64

float weapon_refresh_xyz[3];

//-----------------------------------------------------------------------------------------------
void item_type_setup()
{
    // <ZZ> This function clears out all of the item data...
    Uint16 i;
    repeat(i, MAX_ITEM_TYPE)
    {
        item_list[i].script = NULL;
        item_list[i].icon = NULL;
        item_list[i].overlay = NULL;
        item_list[i].price = 0;
        item_list[i].flags = 0;
        item_list[i].istr = 0;
        item_list[i].idex = 0;
        item_list[i].iint = 0;
        item_list[i].imana = 0;
        item_list[i].ammo = 0;
    }
}

//-----------------------------------------------------------------------------------------------
bool_t item_get_type_name(Uint16 item_type, BASE_DATA * item_oject)
{
    // <ZZ> This function calls the GetName() script for a given item type...
    //      Should fill in NAME_STRING for later use...  Returns ktrue if it worked...

    NAME_STRING[0] = 0;
    item_type = item_type % MAX_ITEM_TYPE;

    if (item_list[item_type].script)
    {
        sf_fast_run_script(item_list[item_type].script, FAST_FUNCTION_GETNAME, (Uint8*)item_oject);

        if (NAME_STRING[0] != 0)
        {
            return ktrue;
        }
    }

    return kfalse;
}

//-----------------------------------------------------------------------------------------------
Uint8 item_find_random_xyz(PSoulfuScriptContext pss, Uint16 character_index, Uint8 character_bone_name)
{
    // <ZZ> This function finds a random xyz position along one of the bones of a character's weapon.
    //      Fills in global weapon_refresh_xyz for return value.  Used to find where a weapon's
    //      refresh (enchantment) particles should be spawn'd...  Returns ktrue if it work'd...
    Uint16 i;
    Uint16 num_bone, num_joint;
    Uint16 joint[2];
    float percent, inverse;
    float* joint_xyz[2];
    Uint8* base_model_data;
    Uint8* bone_data;
    Uint8* joint_data;
    Uint8* character_rdy_file;
    Uint8* item_rdy_file;


    // Find the RDY files for the character and the item...
    if (character_bone_name != LEFT_BONE && character_bone_name != RIGHT_BONE)
    {
        // Only work for left and right weapons...
        return kfalse;
    }

    if ( !VALID_CHR_RANGE(character_index) ) return kfalse;

    character_rdy_file =  chr_data_get_ptr_raw( character_index )->model.parts[MODEL_PART_BASE].file;

    if (character_bone_name == LEFT_BONE)
    {
        item_rdy_file = chr_data_get_ptr_raw( character_index )->model.parts[MODEL_PART_LEFT].file;
    }
    else if (character_bone_name == RIGHT_BONE)
    {
        item_rdy_file = chr_data_get_ptr_raw( character_index )->model.parts[MODEL_PART_RIGHT].file;
    }

    if (item_rdy_file == NULL || character_rdy_file == NULL)
    {
        return kfalse;
    }



    // Read the character's RDY file, find data positions for base model 0...
    character_rdy_file += 6;
    character_rdy_file += (ACTION_MAX << 1);
    character_rdy_file += (MAX_DDD_SHADOW_TEXTURE);
    base_model_data = DEREF( Uint8*, character_rdy_file );
    bone_data = DEREF( Uint8*, character_rdy_file + 16 );
    num_bone = DEREF( Uint16, base_model_data + 6 );



    // Find the bone numbers for the character's named bones...  Left, Right, Left2, Right2...
    character_bone_name = character_bone_name & 7;
    temp_character_bone_number[character_bone_name] = 255;
    repeat(i, num_bone)
    {
        temp_character_bone_number[(*(bone_data+(i*9))) & 7] = (Uint8) i;
    }


    // Did we find a bone number?
    if (temp_character_bone_number[character_bone_name] != 255)
    {
        // Yes, we did, so character seems to have this bone alright...
        // Generate the matrix for the weapon bone...
        matrix_good_bone(pss->matrix, temp_character_bone_number[character_bone_name], temp_character_bone_frame[character_index], chr_data_get_ptr_raw(character_index));


        // Generate joint positions for the item...  Assume frame 1...
        render_generate_model_world_data(item_rdy_file, 1, pss->matrix, fourthbuffer);  // Generate new bone frame in fourthbuffer


        // Read the item's RDY file, find number of bones & joints for base model 0...
        // !!!BAD!!!
        // !!!BAD!!! Assume that # of bones and joints will be same for all base models of weapon...
        // !!!BAD!!!
        item_rdy_file += 6;
        item_rdy_file += (ACTION_MAX << 1);
        item_rdy_file += (MAX_DDD_SHADOW_TEXTURE);
        base_model_data = DEREF( Uint8*, item_rdy_file );
        bone_data = DEREF( Uint8*, item_rdy_file + 16 );
        num_joint = DEREF( Uint16, base_model_data + 4 );
        num_bone = DEREF( Uint16, base_model_data + 6 );

        if (num_bone > 0)
        {
            joint_data = fourthbuffer + (24 * num_bone);
            i = random_number();
            i = i % num_bone;
            bone_data = bone_data + (i * 9) + 1;
            joint[0] = DEREF( Uint16, bone_data );
            joint[1] = DEREF( Uint16, bone_data + 2 );
            joint_xyz[0] = (float*) (joint_data + (joint[0] * 12));
            joint_xyz[1] = (float*) (joint_data + (joint[1] * 12));

            i = random_number();
            percent = i * INV_UINT08_SIZE;
            inverse = 1.0f - percent;
            weapon_refresh_xyz[XX] = joint_xyz[0][XX] * percent + joint_xyz[1][XX] * inverse;
            weapon_refresh_xyz[YY] = joint_xyz[0][YY] * percent + joint_xyz[1][YY] * inverse;
            weapon_refresh_xyz[ZZ] = joint_xyz[0][ZZ] * percent + joint_xyz[1][ZZ] * inverse;
            return ktrue;
        }
    }

    return kfalse;
}

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
void matrix_good_bone(matrix_4x3 bone_matrix, Uint8 bone, Uint8* data_start, CHR_DATA* chr_data)
{
    // <ZZ> This function fills in bone_matrix from a bone's position...  Used to
    //      attach weapons/riders to bones.  Bone is the actual bone index...  Data_start is a pointer
    //      to the bone frame block of a character (generated...  without 11 byte header...)
    //      Data is a pointer to a character's data block...
    Uint16 joint[2];
    Uint16 num_bone;
    Uint8* base_model_data;
    Uint8* data;


    // Figure out how many bones we have, and what the joints are for our bone...
    data = chr_data->model.parts[0].file;  // Data is now start of character's model data...

    // Go to base model 0...  Count bones of that one...
    data += 6;
    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);
    base_model_data = (DEREF( Uint8*, data )) + 6;
    num_bone = DEREF( Uint16, base_model_data ); base_model_data += 2;

    if (bone > num_bone) bone = 0;

    data = (DEREF( Uint8*, data + 16 ));  // Bone data...
    data = data + (bone << 3) + bone + 1;
    joint[0] = DEREF( Uint16, data );  data += 2;
    joint[1] = DEREF( Uint16, data );


    // Skip to the bone normal data for our selected bone...
    data = data_start + (bone << 4) + (bone << 3);


    // Front normal...
    bone_matrix[3] = DEREF( float, data );  data += 4;
    bone_matrix[4] = DEREF( float, data );  data += 4;
    bone_matrix[5] = DEREF( float, data );  data += 4;


    // Side normal...
    bone_matrix[0] =  DEREF( float, data );  data += 4;
    bone_matrix[1] =  DEREF( float, data );  data += 4;
    bone_matrix[2] =  DEREF( float, data );  data += 4;


    // Up normal...  Calculated from joint positions...
    data_start += (num_bone << 4) + (num_bone << 3);
    data = data_start + (joint[1] << 3) + (joint[1] << 2);
    bone_matrix[6] =  DEREF( float, data );  data += 4;
    bone_matrix[7] =  DEREF( float, data );  data += 4;
    bone_matrix[8] =  DEREF( float, data );  data += 4;
    data = data_start + (joint[0] << 3) + (joint[0] << 2);
    bone_matrix[9] =  DEREF( float, data );  data += 4;
    bone_matrix[10] =  DEREF( float, data );  data += 4;
    bone_matrix[11] =  DEREF( float, data );  data += 4;
    bone_matrix[6] -= bone_matrix[9];
    bone_matrix[7] -= bone_matrix[10];
    bone_matrix[8] -= bone_matrix[11];
}

