// <ZZ> This file has stuff related to damage...

Uint8* pnumber_file = NULL;

Uint8 damage_color_rgb[MAX_DAMAGE_TYPE][3] = {{200, 0, 0},
    {200,   0,   0},
    {117, 214,   0},
    {255, 128,   0},
    {142, 255, 255},
    {255, 255,   0},
    {200,   0,   0}
};


ATTACK_DATA attack_info =
{
    0,             // spin
    UINT16_MAX     // index
};

//-----------------------------------------------------------------------------------------------
void damage_setup()
{
    // <ZZ> Sets up stuff for damaging characters...
    SDF_PHEADER pnumber_header;

    pnumber_file = NULL;
    pnumber_header = sdf_archive_find_filetype("PNUMBER", SDF_FILE_IS_RUN);

    if (pnumber_header)
    {
        pnumber_file = sdf_file_get_data(pnumber_header);
    }
}

//-----------------------------------------------------------------------------------------------
void damage_character(PSoulfuScriptContext pss, Uint16 character, Uint8 damage_type, Uint16 damage_amount, Uint16 wound_amount, Uint8 attacker_team)
{
    // <ZZ> This function does damage to a character.
    float spawn_xyz[3];
    CHR_DATA* chr_data;
    PRT_DATA* child_data;
    Sint8 resistance;
    Sint16 temp;
    Uint8 min_hits;
    Uint8 friendly_fire;
    Uint8 start_hits;


    // Find the character...
    chr_data = chr_data_get_ptr(character);
    if ( NULL == chr_data ) return;

    if( damage_type >= MAX_DAMAGE_TYPE) return;

    friendly_fire = (attacker_team == chr_data->team || chr_data->team == TEAM_NEUTRAL);
    // Neutral characters are able to damage others because explodie barrels need to
    // deal damage...
    start_hits = chr_data->hits;



    log_info(1, "Damage done to character %d", character);
    log_info(1, "Type == %d, Amount == %d, Wound == %d", damage_type, damage_amount, wound_amount);

    // Do the damage...  Reduce amounts by defense...  Tap wounds...
    resistance = CAST(Sint8, chr_data->def[damage_type] );
    log_info(1, "Resistance == %d", resistance);

    if (resistance < 0)
    {
        // Character is susceptible to damage...  Character takes an extra point
        // for a point, up to the limit marked by the resistance...
        resistance = -resistance;
        damage_amount = (damage_amount > resistance) ? (damage_amount + resistance) : (damage_amount << 1);
    }
    else
    {
        // Character resists damage...  Damage amount is reduced by the resistance
        // amount.
        damage_amount = (damage_amount > resistance) ? (damage_amount - resistance) : 0;
    }

    resistance = CAST(Sint8, chr_data->def[DAMAGE_WOUND] );
    log_info(1, "Wound resistance == %d", resistance);

    if (resistance < 0)
    {
        // Character is susceptible to wounding...  Character takes an extra point
        // for a point, up to the limit marked by the resistance...
        resistance = -resistance;
        wound_amount = (wound_amount > resistance) ? (wound_amount + resistance) : (wound_amount << 1);
    }
    else
    {
        // Character resists wounding...  Wound amount is reduced by the resistance
        // amount.
        wound_amount = (wound_amount > resistance) ? (wound_amount - resistance) : 0;
    }


    // Modify damage for petrification...
    if ((chr_data->pttimer) > 0)
    {
        damage_amount >>= 1;
    }

    log_info(1, "Type == %d, Amount == %d, Wound == %d", damage_type, damage_amount, wound_amount);


    // Apply damage...
    if (friendly_fire)
    {
        if (chr_data->team == TEAM_NEUTRAL && !(chr_data->flags & CHAR_CAN_BE_MOUNTED))
        {
            // Friendly fire against neutral target (that isn't a mount)...  Don't lower hits below half of max...
            damage_amount = damage_amount + wound_amount;  // No wound damage...
            min_hits = chr_data->hitsmax >> 1;

            if (min_hits == 0)
            {
                // Should never kill...
                min_hits++;
            }

            if (chr_data->hits < min_hits)
            {
                // Hmmm...  That's weird...  Let's not do anything...
                damage_amount = 0;
            }
            else
            {
                temp = ((Sint16) chr_data->hits) - damage_amount;
                chr_data->hits = (Uint8) temp;

                if (temp < min_hits)
                {
                    damage_amount = damage_amount - (min_hits - temp);
                    chr_data->hits = min_hits;
                }
            }
        }
    }
    else
    {
        // Apply normal damage...
        chr_data->hits = (chr_data->hits > damage_amount) ? (chr_data->hits - damage_amount) : 0;
    }




    // Apply wound damage...
    if (!friendly_fire)
    {
        chr_data->hits = (chr_data->hits > wound_amount) ? (chr_data->hits - wound_amount) : 0;

        if ((chr_data->hitstap + wound_amount) < chr_data->hitsmax)
        {
            chr_data->hitstap += wound_amount;
        }
        else
        {
            chr_data->hitstap = chr_data->hitsmax;
        }
    }


    log_info(1, "Type == %d, Amount == %d, Wound == %d", damage_type, damage_amount, wound_amount);


    // Spawn the bouncing number particle, but only for things that aren't invincible (255 max hits)
    if (chr_data->hitsmax < 255)
    {
        // Okay to spawn it...
        if (pnumber_file && damage_type != DAMAGE_WOUND)
        {
            SPAWN_INFO si;
            si.owner   = character;
            si.subtype = 0;
            si.target  = chr_data->target;
            si.team    = chr_data->team;

            damage_amount = start_hits - chr_data->hits;
            attack_info.amount = damage_amount;
            spawn_xyz[XX] = chr_data->x;
            spawn_xyz[YY] = chr_data->y;
            spawn_xyz[ZZ] = (chr_data->z) + (chr_data->height - 1);


            log_info(1, "Particle number amount == %d", damage_amount);

            child_data = (PRT_DATA*)obj_spawn(pss, &si, OBJECT_PRT, spawn_xyz[XX], spawn_xyz[YY], spawn_xyz[ZZ], pnumber_file, MAX_PARTICLE);

            if (child_data)
            {
                // Tint the number according to the damage type...
                child_data->red    = damage_color_rgb[damage_type][0];
                child_data->green  = damage_color_rgb[damage_type][1];
                child_data->blue   = damage_color_rgb[damage_type][2];
                child_data->number = (Uint8)damage_amount;
            }
        }
    }


    // Run the damage event on the target
    chr_data->event = EVENT_DAMAGED;
    sf_fast_rerun_script(&(chr_data->script_ctxt), FAST_FUNCTION_EVENT);
}

//-----------------------------------------------------------------------------------------------
