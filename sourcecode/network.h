#pragma once

#include "soulfu_types.h"
#include <SDL_net.h>

// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
#define ALLOW_LOCAL_PACKETS
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!

#if SDL_BYTEORDER == SDL_LIL_ENDIAN
#    define GENERATE_PACKED_IP(A,B,C,D)  ((((A)*0xFF)) | (((B)*0xFF)<<8) | (((C)*0xFF)<<16) | (((D)*0xFF)<<24))
#else
#    define GENERATE_PACKED_IP(A,B,C,D)  ((((D)*0xFF)) | (((C)*0xFF)<<8) | (((B)*0xFF)<<16) | (((A)*0xFF)<<24))
#endif

#define UDP_PORT_SERVER   17859                      // Orangeville, PA
#define UDP_PORT_CLIENT   (UDP_PORT_SERVER+1)        // Orangeville, PA

#define TCPIP_PORT_SERVER 30628                      // Colbert, GA
#define TCPIP_PORT_CLIENT (TCPIP_PORT_SERVER+1)      // Colbert, GA                  

#define LOCALHOST GENERATE_PACKED_IP(127,0,0,1)

#define MAX_REMOTE       1024   // Maximum number of network'd computers...

//#define MAIN_SERVER_NAME "www.aaronbishopgames.com"
#define MAIN_SERVER_NAME "localhost"

enum packet_type_e
{
    PACKET_TYPE_NULL              = -1,
    PACKET_TYPE_CHAT              =  0,
    PACKET_TYPE_I_AM_HERE,
    PACKET_TYPE_ROOM_UPDATE,
    PACKET_TYPE_I_WANNA_PLAY,
    PACKET_TYPE_I_WANNA_QUIT,
    PACKET_TYPE_OKAY_YOU_CAN_PLAY,
    PACKET_TYPE_CLOSE_CONNECTION,
    PACKET_TYPE_NEW_CONNECTION,
    PACKET_TYPE_PLAYER_UPDATE,
    PACKET_TYPE_SERVER_HANDSHAKE
};
typedef enum packet_type_e PACKET_TYPE;


#define MAX_PACKET_SIZE 8192

#define NETWORK_ALL_REMOTES_IN_GAME             0
#define NETWORK_ALL_REMOTES_IN_ROOM             1
#define NETWORK_ALL_REMOTES_IN_NEARBY_ROOMS     2
#define NETWORK_SERVER_VIA_TCPIP                3


//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

extern Uint8  network_on;
extern Uint8* netlist_data;

extern Uint16 net_seed;

extern IPaddress         remote_ip_address;
extern TCPsocket         remote_socket_tcp;
extern UDPsocket         remote_socket_udp;
extern bool_t            remote_connected;
extern SDLNet_SocketSet  remote_socket_set;

extern IPaddress         local_ip_address;
extern TCPsocket         local_socket_tcp;
extern int               local_port_udp;
extern UDPsocket         local_socket_udp;
extern bool_t            local_connected;
extern SDLNet_SocketSet  local_socket_set;

extern UDPsocket        net_serrver_socket_udp;
extern UDPsocket        net_remote_socket_udp[MAX_REMOTE];

extern UDPsocket  local_socket_udp;

struct s_remote_data
{
    IPaddress  address;
    Uint16     room_number;
    Uint8      is_neighbor;
    Uint8      on;
};
typedef struct s_remote_data remote_data_t;

extern int           remote_list_count;
extern remote_data_t remote_list[MAX_REMOTE];

#define PACKET_HEADER_SIZE                  3

#pragma pack(push,1)
    union packet_data_t
    {
        struct
        {
            Uint8  type;
            Uint8  seed;
            Uint8  checksum;
        };

        Uint8 buffer[MAX_PACKET_SIZE];

    } SET_PACKED();
    typedef union packet_data_t PACKET_DATA;
#pragma pack(pop)

struct packet_t
{
    PACKET_DATA data;

    Sint16      length;
    Uint16      counter;
    Uint16      readpos;
};
typedef struct packet_t PACKET;

extern PACKET packet;

extern Uint8  global_version_error;
extern Uint16 required_executable_version;
extern Uint16 required_data_version;

struct Net_Script_Info_t
{
    Uint8  newly_spawned;
    Uint8  extra_data;
    Uint8  remote_index;
    Uint8  netlist_index;
    Uint16 x;
    Uint16 y;
    Uint8  z;
    Uint8  facing;
    Uint8  action;
    Uint8  team;
    Uint8  poison;
    Uint8  petrify;
    Uint8  alpha;
    Uint8  deflect;
    Uint8  haste;
    Uint8  other_enchant;
    Uint8  eqleft;
    Uint8  eqright;
    Uint8  eqcol01;
    Uint8  eqcol23;              // high-data only
    Uint8  eqspec1;              // high-data only
    Uint8  eqspec2;              // high-data only
    Uint8  eqhelm;               // high-data only
    Uint8  eqbody;               // high-data only
    Uint8  eqlegs;               // high-data only
    Uint8  cclass;                // high-data only
    Uint16 mount_index;          // high-data only
};
typedef struct Net_Script_Info_t NET_SCRIPT_INFO;

extern NET_SCRIPT_INFO netscript;

//-----------------------------------------------------------------------------------------------
// Packet macros...
//-----------------------------------------------------------------------------------------------
void packet_begin(PACKET * pp, Uint32 id, Uint32 type);
void packet_add_data(PACKET * pp, void * data, size_t sz);
void packet_add_string(PACKET * pp, char * string);
void packet_add_uint32(PACKET * pp, Uint32 number);
void packet_add_uint16(PACKET * pp, Uint16 number);
void packet_add_uint8(PACKET * pp, Uint8 number);
Sint8 packet_read_string(PACKET * pp, char * string);
Sint8 packet_read_uint32(PACKET * pp, Uint32* to_set);
Sint8 packet_read_uint16(PACKET * pp, Uint16* to_set);
Sint8 packet_read_uint08(PACKET * pp, Uint8* to_set);
void packet_encrypt(PACKET * pp);
void packet_decrypt(PACKET * pp);
void calculate_packet_checksum(PACKET * pp);
void packet_end(PACKET * pp);

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
Uint8 packet_valid(PACKET * pp);

Sint8 network_receive_packet_tcp(PACKET * pp, TCPsocket socket);
Sint8 network_transmit_packet_tcp(PACKET * pp, Uint16 *seed, TCPsocket socket);
Sint8 network_receive_packet_udp(PACKET * pp, UDPsocket socket, IPaddress *ip);
Sint8 network_transmit_packet_udp(PACKET * pp, Uint16 *seed, UDPsocket socket, IPaddress *ip);

Uint8  network_setup(void);
void   network_close(void);
bool_t cl_listen();
bool_t network_listen_server();

void network_clear_remote_list();
Uint8 network_add_remote(char* remote_name);
void network_delete_remote(Uint16 remote);
Uint16 network_find_remote_character(Uint32 ip_address_of_remote, Uint8 local_index_on_remote);


void cl_transmit_chat(Uint8 speaker_class, char* speaker_name, char* message);
void cl_transmit_room_update();
void cl_transmit_join();
void cl_transmit_quit();


Uint8 network_find_script_index(char* filename);
