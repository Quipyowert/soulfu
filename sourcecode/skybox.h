//============================================================================
// SKYBOX.H -- CODE FOR DRAWING SKYBOX BACKGROUND TEXTURES IN OpenGL
// (C) 1999,2004   Bill Baxter
//============================================================================
// HINT:  Get your sky textures from http://www.planethalflife.com/crinity
//============================================================================
// There's a much easier way to do this if you have cube map support
// in your hardware.  If not then this will just have to do.
//============================================================================
// Permission to use, copy, modify, distribute and sell this software
// and its documentation for any purpose is hereby granted without
// fee, provided that the above copyright notice appear in all copies
// and that both that copyright notice and this permission notice
// appear in supporting documentation.  Binaries may be compiled with
// this software without any royalties or restrictions.
//
// The University of North Carolina at Chapel Hill makes no representations
// about the suitability of this software for any purpose. It is provided
// "as is" without express or implied warranty.
//============================================================================

#include <SDL_types.h>

enum { SKY_BACK, SKY_DOWN, SKY_FRONT, SKY_LEFT, SKY_RIGHT, SKY_UP };
long SKYBOX_ON = 0;
float SKYBOX_X = -200.0;
float SKYBOX_DX = 400.0;
float SKYBOX_Y = -200.0;
float SKYBOX_DY = 400.0;
float SKYBOX_Z = -100.0;
float SKYBOX_DZ = 200.0;

struct  simp_img_t
{
    Uint8 *data;
    int W, H;
};
typedef struct  simp_img_t SimpImg;

//void UploadSkyTextures(Uint32 texIDs[6], SimpImg images[6]);

Uint32 g_skyTexID[] = {0, 0, 0, 0, 0, 0};
SimpImg g_skyImageData[] = {{0}, {0}, {0}, {0}, {0}, {0}};

Uint32 g_skyTexIDs[10][6];

Uint32 texture_skybox_0  = 0;
Uint32 texture_skybox_1  = 0;
Uint32 texture_skybox_2  = 0;
Uint32 texture_skybox_3  = 0;
Uint32 texture_skybox_4  = 0;
Uint32 texture_skybox_5  = 0;

//void LoadSkyBoxTextures(Uint32 texIds[6]);
//void DrawSkyBox(Uint32 texIds[6]);

