// <ZZ> This file contains lots of global variables and defines and other fun stuff.

#pragma once

// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
// Don't need sqrt when just tryin' to find closest...  Can do with squared...
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!

// !!!BAD!!!
// !!!BAD!!! Get rid of all of those extra libraries!!!
// !!!BAD!!!

#define STREAM_READ(TYPE,STREAM)      DEREF(TYPE, STREAM);       STREAM += sizeof(TYPE)
#define STREAM_WRITE(TYPE,STREAM,VAL) DEREF(TYPE, STREAM) = VAL; STREAM += sizeof(TYPE)

//-Includes--------------------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#ifdef NULL
#undef NULL
#define NULL 0
#endif

#include <SDL.h>
#include <SDL_opengl.h>

// Include platform specific stuff here
#ifdef WIN32
#undef FAR          // WinDef.h declares FAR the same as jpeglib...
#include <windows.h>
#endif


// Stuff for SDL
#include <SDL.h>
#include <SDL_net.h>
#include <SDL_opengl.h>
#include <glext.h>

#include "sdf_archive.h"
#include "pxss_lib.h"
#include "script_extensions.h"





//-Defines---------------------------------------------------------------------------------------


extern bool_t update_performed;   // Do any files need decompressing?



// Stops player input from keyboard...
extern Uint16 global_block_keyboard_timer;



// For Window3D

#define WIN_3D_AXIS     0
#define WIN_3D_SHADOW   16
#define WIN_3D_VERTEX   32
#define WIN_3D_BONE     48
#define WIN_3D_MODEL    64
#define WIN_3D_BONE_UPDATE 80
#define WIN_3D_TEXVERTEX 96
#define WIN_3D_SKIN_FROM_CAMERA  112
#define WIN_3D_MODEL_EDIT 128


// For Window3DRoom
#define ROOM_MODE_MINIMAP       0
#define ROOM_MODE_GRID          16
#define ROOM_MODE_VERTEX        32
#define ROOM_MODE_TRIANGLE      48
#define ROOM_MODE_EXTERIOR_WALL 64
#define ROOM_MODE_WAYPOINT      80
#define ROOM_MODE_BRIDGE        96
#define ROOM_MODE_OBJECT_GROUP  112


// RDY flags...
#define RENDER_LIGHT_FLAG        1
#define RENDER_COLOR_FLAG        2
#define RENDER_NOCULL_FLAG       4
#define RENDER_ENVIRO_FLAG       8
#define RENDER_CARTOON_FLAG     16
#define RENDER_EYE_FLAG         32
#define RENDER_NO_LINE_FLAG     64
#define RENDER_PAPER_FLAG      128


extern Uint8 global_render_light_color_rgb[3];
extern float global_render_light_offset_xy[2];


// For makin' TIL generation easier...
//extern Uint8* global_ddd_file_start;
//extern Uint8* global_rdy_file_start;

#define MAX_LOCAL_PLAYER 4                      // Total players on local machine...


// Bones...
#define LEFT_BONE   1
#define RIGHT_BONE  2
#define LEFT2_BONE  3
#define RIGHT2_BONE 4
#define SADDLE_BONE 5

#define NAME_STRING (pxss_run_string[MAX_STRING-2])  // Second to last string used for GetName() and random_name...
#define DEBUG_STRING (pxss_run_string[MAX_STRING-1])  // Last string used for debug...
#define CHARACTER_Z_FLOAT   0.15f   // For ground collisions...

#define ROOM_LINE_SIZE  0.0025f         // For room outlines
#define CHAR_LINE_SIZE  0.0015f         // For character outlines


// For making input and emacs type-ins not interfere with other things...
extern Uint8 input_active;


// Language stuff...
extern Uint8 user_language;
extern int user_language_phrases;
extern char bad_string[];        // Used as return on language failures...

#define LANGUAGE_MAX 16
extern Uint8* language_file[LANGUAGE_MAX];
extern Uint8* kanji_data;



// Camera stuff..
#define CAMERA_ROTATION_RATE  90.0f
#define CAMERA_ZOOM_RATE 0.75f
#define MIN_CAMERA_DISTANCE 10.0f
#define MAX_CAMERA_DISTANCE 75.0f
#define MIN_CAMERA_Y  4096  // Make smaller to view more of horizon...
#define MAX_CAMERA_Y  15360
#define MAX_TERRAIN_DISTANCE 500.0f



// Level of detail stuff
#ifdef DEVTOOL
#define DETAIL_LEVEL_MAX 1      // Number of detail levels...  1 for DEVTOOL mode...
#else
//    #define DETAIL_LEVEL_MAX 16     // Number of detail levels...  16 for non-DEVTOOL...
#define DETAIL_LEVEL_MAX 1      // Detail levels didn't work very well...
#endif
extern Uint8 drawing_world ;


#define NO_BONE 255


#define MAX_DDD_TEXTURE 4           // Number of textures in an object file...
#define MAX_DDD_SHADOW_TEXTURE 4    // Number of Shadow textures...
#define DDD_SCALE_WEIGHT 20000.0f   // DDD coordinates should range from - to + this number...
#define DDD_EXTERNAL_BONE_FRAMES 16384  // Flag for external linkage...
#define JOINT_COLLISION_SCALE 0.015f// For converting joint sizes from byte to float...


// Weird & Silly stuff for figurin' out where a character's joints are onscreen...
// Helps figure out whether we need to actually draw the character or not, as well
// as telling us if the mouse cursor is over the character...
#define ONSCREEN_CLICK_SCALE  CAST(float, DEFAULT_H)  // Magic number...
extern Uint16 num_onscreen_joint;
extern Uint8 onscreen_joint_active;
extern Uint16 onscreen_joint_character;
extern CHR_DATA* onscreen_joint_character_data;


// Turn pretty shadows off by default...  Later read from config file...
Uint8 volumetric_shadows_on;


// Stuff for character button controls...
Uint8 global_button_handled;


#define GRAVITY -0.05f

// Event callback codes...
#define EVENT_HIT_WALL         1
#define EVENT_HIT_WATER        2
#define EVENT_HIT_CHARACTER    3
#define EVENT_HIT_FLOOR        4  // For jumping characters or any type of particle...
#define EVENT_TIMER            5
#define EVENT_FELL_IN_PIT      6
#define EVENT_DAMAGED          7
#define EVENT_LEVEL_UP         8
#define EVENT_SECONDARY_TIMER  9
#define EVENT_THIRDIARY_TIMER  10
#define EVENT_MOUNTED          11
#define EVENT_DISMOUNTED       12
#define EVENT_BLOCKED          13
#define EVENT_DEFLECTED        14 // Only particles get this one...
#define EVENT_PETRIFIED        15
#define EVENT_UNPETRIFIED      16
#define EVENT_JUMPED_ON        17 // For when a platform is jumped on...
#define EVENT_DROWN            18 // Spawn some bubbles...
#define EVENT_RIPPLE           19
#define EVENT_SPAWNED_IN_WATER 20
#define EVENT_HIT_WAYPOINT     21
#define EVENT_HUNGER_DAMAGED   22
#define EVENT_NETWORK_UPDATE   23



// Enchant flags...  (for characters...  Guess why...)
#define ENCHANT_FLAG_SUMMON_1         1 // Character is summon'd (character is instantly destroy'd if disenchanted)
#define ENCHANT_FLAG_SUMMON_2         2 //   Lowest 2 bits used to specify level of summoning (& mana to be refunded on DChant)
#define ENCHANT_FLAG_SUMMON_3         3 //
#define ENCHANT_FLAG_LEVITATE         4 // Jump action allows levitation if held down (should fall if disenchanted)
#define ENCHANT_FLAG_INVISIBLE        8 // Character is invisible (turns visible again if disenchanted)
#define ENCHANT_FLAG_MORPH           16 // Character is morph'd into an alternate form (revert to normal form if disenchanted)
#define ENCHANT_FLAG_DEFLECT         32 // Arrows and other particles deflect off character (glowie sphere goes away if disenchanted) (glow particle shatters if it absorbs too many particles too quickly - should flash with each one it deflects)
#define ENCHANT_FLAG_HASTE           64 // Character moves as if they had double Dex (goes away if disenchanted)
#define ENCHANT_FLAG_MIRROR_DEFLECT 128 // Character is using a mirror shield to deflect stuff...





// Simple directions...
#define DIRECTION_FRONT  0
#define DIRECTION_LEFT   1
#define DIRECTION_RIGHT  2
#define DIRECTION_BACK   3


//-Globals---------------------------------------------------------------------------------------
#ifdef DEVTOOL
extern Uint8 debug_active;
#endif


extern Uint8 color_temp[4];
extern Uint8 black[4];
extern Uint8 red[4];
extern Uint8 line_color[4];
extern Uint8 dark_red[4];
extern Uint8 green[4];
extern Uint8 dark_green[4];
extern Uint8 light_green[4];
extern Uint8 blue[4];
extern Uint8 cyan[4];
extern Uint8 magenta[4];
extern Uint8 grey[4];
extern Uint8 white[4];
extern Uint8 shadow_color[4];
extern Uint8 gold[4];
extern Uint8 brown[4];
extern Uint8 bronze[4];
extern Uint8 yellow[4];
extern Uint8 med_yellow[4];
extern Uint8 dark_yellow[4];
extern Uint8 faded_yellow[4];
extern Uint8 whiter[4];
extern Uint8 whitest[4];
extern Uint8 instrument_color[8][4];



extern Uint16 map_room_door_spin;  // For telling players their new spin on room change...



//extern Uint8* current_object_data;  // Should be filled before run_script...
//extern Uint16 current_object_item;  // Ditto

extern bool_t quitgame;
extern Uint32 main_game_frame;        // Should update at a rate of GAME_FRAMES_PER_SECOND (but only when game is running...)
extern Uint32 main_video_frame;       // Number of times the screen has been drawn...
extern Uint32 main_video_frame_skipped;  // Should update at a rate of GAME_FRAMES_PER_SECOND (even when game isn't active...)
extern Uint16 main_frame_skip;  // Number of game frames in last update...


// Network and game system stuff...
extern bool_t main_game_active;         // ktrue if the local machine has joined or started a game
extern bool_t play_game_active;         // ktrue if the local players are actually playing (ie characters are spawned and runnin' around)
extern bool_t paying_customer;  // Is the local machine a paying customer?
extern bool_t update_active;    // For updating files in the datafile...


// Heartbeat stuff...
extern Uint32 time_since_i_got_heartbeat;// Frames since I got a heartbeat from somebody...
extern Uint32 time_since_i_sent_heartbeat;// Frames since I sent a heartbeat from somebody...


extern Uint8 mip_map_active;
extern Uint8 fast_and_ugly_active;


#define MAX_SCREEN_SIZES 8
extern Uint16 screen_sizes_xy[MAX_SCREEN_SIZES][2];

extern int screen_x;                       // The x screen size
extern int screen_y;                       // The y screen size
extern float window_scale;




// For door collisions...
Uint8 global_room_changed;


// For water...
#define WATER_TYPE_WATER 0
#define WATER_TYPE_LAVA  1
#define WATER_TYPE_SAND  2

extern float room_water_level;
extern Uint8 room_water_type;
extern Uint8 room_metal_box_item;

// For modeler/border tools
#define BORDER_SIMPLE       0
#define BORDER_DRAG         1
#define BORDER_SELECT       2
#define BORDER_POINT_PICK   3
#define BORDER_CROSS_HAIRS  4
#define BORDER_MOVE         5
#define BORDER_SPECIAL_SELECT_ALL  20
#define BORDER_SPECIAL_SELECT_INVERT  21
#define BORDER_SPECIAL_SELECT_CONNECTED  22

//*****************************************************
// dcode.h
#define TEXTURE_NO_ALPHA       0    // Normal texture
#define TEXTURE_ALPHA          1    // Color keyed transparency, Prefix '-'
#define TEXTURE_SUPER_ALPHA    2    // Color keyed with edge blur, Prefix '='
#define TEXTURE_DONT_LOAD      3    // Not supposed to load this onto card, Prefix '+'
#define TEXTURE_ON_CARD        4    // Has been loaded onto the card

#define USED_TEMPORARY 2
#define USED_PERMANENT 1

//*****************************************************
// water.h

#define MAX_WATER_FRAME 32
extern GLuint texture_water[MAX_WATER_FRAME];
extern GLuint texture_shimmer[MAX_WATER_FRAME];
extern GLuint texture_sand;

extern Uint8 water_layers_active;

//*****************************************************
// sound.h
#define MAX_CHANNEL 64                  // Maximum number of simultaneous sounds
#define LEFT 0
#define RIGHT 1

extern Sint8   channel_active[MAX_CHANNEL];
extern Sint16* channel_data[MAX_CHANNEL];
extern Sint32  channel_start_delay[MAX_CHANNEL];
extern Uint32  channel_position[MAX_CHANNEL];
extern Uint16  channel_position_add[MAX_CHANNEL];
extern Uint32  channel_position_end[MAX_CHANNEL];
extern Uint32  channel_total_position[MAX_CHANNEL];
extern Uint32  channel_total_position_end[MAX_CHANNEL];
extern Uint8   channel_volume[MAX_CHANNEL][2];
extern Uint8   channel_new_volume[MAX_CHANNEL][2];
extern Uint32  channel_loop_start[MAX_CHANNEL];
extern Uint32  channel_fade_start[MAX_CHANNEL];     // Time after which it starts fading out
extern Uint32  channel_fade_step[MAX_CHANNEL][2];   // Fade by 1 every so many samples
extern Uint32  channel_fade_count[MAX_CHANNEL][2];  // For counting our steps
extern Uint8   channel_infinite_loop[MAX_CHANNEL];

extern Uint8 sound_flip_pan;   // Flip left & right speakers...
extern Uint8 main_volume;              // For fades...
extern Uint8 master_sfx_volume;        // User option
extern Uint8 master_music_volume;      // User option

#define MUSIC_STOP 0
#define MUSIC_ONCE 1
#define MUSIC_PLAY 2

extern Uint8* music_file_instrument;
extern Uint8* music_file_note;
extern int music_num_instrument;
extern int music_num_note;
extern int music_first_note;
extern int music_next_note;
extern int music_intro_time;
extern int music_tempo;
extern Sint32 music_time;
extern Uint8 music_mode;

void play_sound(Uint32 start_delay, Uint8* sound_file_start, Uint8 volume, Uint8 pan, Uint16 pitch_skip, Uint8* loop_data);
void copy_sound_buffer_to_sdl(void *udata, Uint8 *stream, int len);
void sound_reset_channels(void);
void sound_setup(void);
void fill_sound_buffer(void);
void fill_music(void);
void play_music(Uint8* filedata, Uint16 start_time, Uint8 mode);



//*****************************************************
// page.h
#define PAGE_COL (25+2)                     // The number of columns in the page animation
#define PAGE_FRAME 32                       // The number of frames in the page animation
extern float page_xy[PAGE_FRAME][PAGE_COL*2];      // The page animation

//*****************************************************
// message.h
#define MAX_SANITIZE 256                // Maximum number of bad words
#define MESSAGE_BITS      4
#define MESSAGE_MAX      (1<<MESSAGE_BITS)   // Maximum number of messages
#define MESSAGE_MASK     (MESSAGE_MAX-1)
#define MAX_MESSAGE_SIZE 128                 // Maximum length of the messages

extern Uint8 message_read;         // The most recent message
extern Uint8 message_write;        // The next message to fill in
extern char  message_buffer[MESSAGE_MAX][MAX_MESSAGE_SIZE];

extern Uint8* sanitize_file;
extern int    sanitize_count;
extern Uint8* sanitize_token[MAX_SANITIZE];

extern Uint16 message_size;

void message_reset(void);
void message_setup(void);
void message_sanitize(char* message_text);
void message_sanitize(char* message_text);
void message_add(char* message_text, char* speaker_name, Uint8 sanitize);
void language_message_add(Uint16 message_index);

//-----------------------------------------------------------------------------------------------
// <ZZ> Macro for getting a message, 0 is most recent...
#define message_get(VAL) message_buffer[(message_read-VAL) & MESSAGE_MASK]


// Frame events...
#define FRAME_EVENT_LEFT    16
#define FRAME_EVENT_RIGHT   32
#define FRAME_EVENT_BLOCK   64
#define FRAME_EVENT_COMBAT  128

//*****************************************************
// render.h

#define MAX_SELECT 8192

extern Uint16 select_num;
extern Uint16 select_list[MAX_SELECT];
extern Uint16 select_index;
extern float  select_xyz[MAX_SELECT][3];
extern float* select_data[MAX_SELECT];
extern Uint16 select_flag[MAX_SELECT];

#define select_clear() { select_num = 0; }

#define MAX_JOINT 256
#define MAX_BONE 256
#define MAX_VERTEX 16384
#define MAX_TEX_VERTEX 16384
#define MAX_TRIANGLE 16384

#define VIEW_TOP_XY    0
#define VIEW_SIDE_YZ   1
#define VIEW_FRONT_XZ  2
#define VIEW_3D_XYZ    3

struct render_selection_t
{
    bool_t box_on;
    Uint8  close_type;
    bool_t move_on;
    bool_t pick_on;
    Uint8  view;
    Uint8  move;

    float center_xyz[3];
    float offset_xyz[3];

    float box_tl[2];
    float box_br[2];
    float box_min[2];
    float box_max[2];
};
typedef struct render_selection_t RENDER_SELECTION;

extern RENDER_SELECTION selection;

extern Uint8 rotation_view;

extern Uint8 global_rdy_show_joint_numbers;
extern Uint8 global_rdy_detail_level;
extern Uint16 global_num_bone;    // Yicky thing for helping models in scripts
extern Uint8* global_bone_data;   // Yicky thing for helping models in scripts
extern Uint16 original_vertex_angle[MAX_SELECT];
extern Uint8 global_autoshadow_vertex;

#ifdef DEVTOOL
extern Uint8 do_anchor_swap;
extern Uint8 triangle_lines;
extern Uint8 global_billboard_active;
extern Uint16 render_marked_frame;
#endif

void render_fix_model_to_bone_length(Uint8* data, Uint16 frame, Uint16 exempt_joint);
void render_crunch_bone(Uint8* data, Uint16 frame, Uint16 bone, Uint8 detail_level);
void render_crunch_vertex(Uint8* data, Uint16 frame, Uint16 vertex, Uint8 recalc_weight, Uint8 detail_level);
void render_attach_vertex_to_bone(Uint8* data, Uint16 frame, Uint16 vertex);
void render_crunch_rdy(Uint8* data);
void render_crunch_all(Uint8 mask, float loadin_min, float loadin_max);
void select_add(Uint16 item, float* item_xyz);
Uint8 select_inlist(Uint16 item);
void select_remove(Uint16 item);
void select_update_xyz(void);
void select_update_xy(void);
void hide_vertices(Uint8* data, Uint16 frame, Uint8 do_hide);
void render_rotate_bones(Uint8* data, Uint16 frame, Sint8 rotation, Uint8 undo_rotation);
void render_box();
void render_get_point_xy(float x, float y, float z, float* x_spot, float* y_spot);
void render_get_point_xyd(float x, float y, float z, float* x_spot, float* y_spot, float* d_spot);
Uint8 render_get_point_xyz(SOULFU_WINDOW *pwin, float x, float y, float* x_spot, float* y_spot, float* z_spot);
void render_model_move(void);
void render_tex_move(Uint8 limit);
void setup_shadow(void);
void render_bone_frame(Uint8* base_model_data, Uint8* bone_data, Uint8* current_frame_data);
void render_generate_bone_normals(Uint8* data, Uint16 frame);
Uint8 render_insert_tex_vertex(Uint8* data, Uint16 frame, float* coordinates_xy, Uint16 tex_vertex_to_delete, Uint16 tex_vertex_replacement);
Uint8 render_pregenerate_normals(Uint8* data, Uint16 frame, Uint8 detail_level);
void clean_up_tex_vertex(Uint8* data, Uint16 frame);
void remove_all_tex_vertex(Uint8* data, Uint16 frame);
void render_joint_size(Uint8* data, Uint16 frame, Uint16 joint, Uint8 size);
void render_attach_vertex_to_bone(Uint8* data, Uint16 frame, Uint16 vertex);
void render_solid_box(Uint8* color, float tlx, float tly, float tlz, float brx, float bry, float brz);
void render_gnomify_working_direction(Uint8* data, Uint16 frame, Uint16 joint_one, Uint16 joint_two);
void render_gnomify_affect_joint(Uint8* data, Uint16 frame, Uint16 joint, Uint8 percent);
void render_joint_from_vertex_location(Uint8* data, Uint16 frame);
void render_rdy(SOULFU_WINDOW *pwin, Uint8* data, Uint16 frame, Uint8 mode, Uint8** texture_file_data_block, Uint8 main_alpha, Uint8* bone_frame_data, Uint8 petrify, Uint8 eye_frame);
void render_shadow_setup(void);
void render_rdy_character_shadow(Uint8* data, CHR_DATA* chr_data, Uint8 main_alpha, float scale, float z);
void render_rdy_shadow(PSoulfuScriptContext pss, Uint8* data, Uint16 frame, float x, float y, float z, Uint8 mode);
Uint8 render_insert_triangle(Uint8* data, Uint16 frame, Uint16 insert_mode, Uint8 texture);
Uint8 render_insert_vertex(Uint8* data, Uint16 frame, float* vertex_xyz, Uint16 vertex_to_remove, Uint16 vertex_replacement);
void render_copy_selected(Uint8* data, Uint16 frame);
void render_paste_selected(Uint8* data, Uint16 frame, Uint8 start_texture);
void render_fill_temp_character_bone_number(Uint8* data);
Sint8 render_bone_id(Uint8* data, Uint16 frame, Uint16 joint_one, Uint16 joint_two, Uint8 bone_id);
Uint8 render_insert_bone(Uint8* data, Uint16 frame, Uint16 joint_one, Uint16 joint_two, Uint8 insert_bone, Uint8 bone_id);
Uint8 render_insert_joint(Uint8* data, Uint16 frame, float* joint_xyz, Uint16 joint_to_remove, Uint8 joint_size);
void scale_all_joints_and_vertices(Uint8* data, float scale);
void scale_selected_vertices(Uint8* data, Uint16 frame, Uint8 axis, float scale);
void scale_selected_vertices_centrid(Uint8* data, Uint16 frame, float scale);
void move_selected_vertices(Uint8* data, Uint16 frame, Uint8 axis, float offset);
void set_selected_vertices(Uint8* data, Uint16 frame, Uint8 axis, float position);
void break_anim_joints(Uint8* data, Uint16 frame);
void break_anim_selected_vertices(Uint8* data, Uint16 frame);
void tree_rotate_selected_vertices(Uint8* data, Uint16 frame, float angle);
void rotate_selected_vertices(Uint8* data, Uint16 frame);
void flip_selected_vertices(Uint8* data, Uint16 frame, Uint8 axis);
void weld_selected_vertices(Uint8* data, Uint16 frame, Uint8 glue_instead);
void weld_selected_tex_vertices(Uint8* data, Uint16 frame);
void scale_selected_tex_vertices(Uint8* data, Uint16 frame, Uint8 axis, float scale);
void render_bounding_box();
void render_axis();
Uint16 render_change_frame_base_model(Uint8* data, Uint16 frame, Uint16 new_base_model);
Uint8 render_insert_base_model(Uint8* data, Uint16 frame, Uint8 insert_mode);
Uint16 get_number_of_bones(Uint8* data);
Uint8 render_insert_frame(Uint8* data, Uint16 frame, Uint8 insert_mode);
void render_center_frame(Uint8* data, Uint16 frame, Uint8 axis);
void render_copy_frame(Uint8* data, Uint16 frame);
void render_auto_shadow(Uint8* data, Uint16 frame, Uint8 shadow);
void render_interpolate(Uint8* data, Uint16 frame);
void render_shadow_reset(Uint8* data, Uint16 frame);

Uint8* render_generate_model_world_data(Uint8* data, Uint16 frame, float* matrix, Uint8* write);
Uint8* get_start_of_triangles(Uint8* data, Uint16 frame, Uint8 texture);
Uint8* get_start_of_frame(Uint8* data, Uint16 frame);
Uint8* get_frame_pointer(Uint8* data, Uint16 frame);
Uint8* get_start_of_frame_shadows(Uint8* data, Uint16 frame);
Uint8* render_get_set_external_linkage(Uint8* data, Uint8* link_data);


//*****************************************************
// item.h
#define MAX_ITEM_TYPE 256

struct item_info_t
{
    Uint8* script;
    Uint8* icon;
    Uint8* overlay;
    Sint16 price;
    Uint16 flags;
    Uint8  istr;
    Uint8  idex;
    Uint8  iint;
    Uint8  imana;
    Uint8  ammo;
};
typedef struct item_info_t ITEM_INFO;

extern ITEM_INFO item_list[MAX_ITEM_TYPE];


extern Uint16 weapon_setup_grip;  // For scripted ModelAssign() call to weapons...
extern float weapon_refresh_xyz[3];

void   item_type_setup();
bool_t item_get_type_name(Uint16 item_type, BASE_DATA * item_oject);
Uint8  item_find_random_xyz(PSoulfuScriptContext pss, Uint16 character_index, Uint8 character_bone_name);

//*****************************************************
// map.h
#define MAP_ROOM_FLAG_FOUND       128
#define MAP_ROOM_FLAG_DUAL_LEVEL  64
#define MAP_ROOM_FLAG_TOWN        32
#define MAP_ROOM_FLAG_BOSS        16
#define MAP_ROOM_FLAG_VIRTUE      8
#define MAP_ROOM_FLAG_OUTSIDE     4

struct map_create_info_t
{
    Uint8*  srf_file;
    float   x;
    float   y;
    Uint16  rotation;
    Uint8   seed;
    Uint8   twset;
    Uint8   level;
    Uint8   flags;
    Uint8   difficulty;
    Uint8   area;
    Uint16  from_room;
    Uint8   multi_connect;
};
typedef struct map_create_info_t MAP_CREATE_INFO;

#define MAX_MAP_ROOM 4000
extern Uint16 num_map_room;
extern Uint8 map_room_data[MAX_MAP_ROOM][40];

#define MAX_AUTOMAP_ROOM 200
extern Uint16 num_automap_room;
extern Uint16 automap_room_list[MAX_AUTOMAP_ROOM];

extern Uint16 map_current_room;
extern Uint16 map_last_town_room;

extern float map_room_door_pushback;  // For figgerin' door xyz location on room change...

void  map_clear();
Uint8 map_doors_overlap(Uint16 room_a, Uint8 room_a_wall, Uint16 room_b, Uint8 room_b_wall);
Uint8 map_connect_rooms(Uint16 room_a, Uint16 room_b);
Uint8 map_rooms_overlap_elaborate(Uint8* room_a_srf, float* room_a_xy, Uint16 room_a_rotation, Uint8* room_b_srf, float* room_b_xy, Uint16 room_b_rotation);
Uint8 map_rooms_overlap(Uint16 room_a, Uint16 room_b);
Uint8 map_create_room(MAP_CREATE_INFO * pinfo);
void  map_remove_room(void);
void  map_automap_prime(float centerx, float centery, Uint8 level, float tolerance);
void  map_automap_draw(PSoulfuScriptContext pss);

//*****************************************************
// damage.h
enum DAMAGE_E
{
    DAMAGE_EDGE = 0,
    DAMAGE_BASH,
    DAMAGE_ACID,
    DAMAGE_FIRE,
    DAMAGE_ICE,
    DAMAGE_VOLT,
    DAMAGE_WOUND,
    MAX_DAMAGE_TYPE
};

struct attack_data_t
{
    Uint16 spin;
    Uint16 index;
    Sint16 amount;
    Sint8  defense[MAX_DAMAGE_TYPE];
};
typedef struct attack_data_t ATTACK_DATA;

extern ATTACK_DATA attack_info;


extern Uint8 damage_color_rgb[MAX_DAMAGE_TYPE][3];

void damage_setup();
void damage_character(PSoulfuScriptContext pss, Uint16 character, Uint8 damage_type, Uint16 damage_amount, Uint16 wound_amount, Uint8 attacker_team);


//*****************************************************
// room.h

#define ROOM_HEIGHTMAP_SCALE 0.00244140625f // ROOM_HEIGHTMAP_Z / 16384...  for the heightmap return function...
#define ROOM_HEIGHTMAP_SIZE 1024            // Heightmap is 2048x2048, 16 values per square foot
#define ROOM_HEIGHTMAP_PRECISION 4.0f       // Each foot of room is sampled this many times (square for a square foot)
#define ROOM_HEIGHTMAP_BIAS      0.125f     // Should be 0.5/precision...  Used to help out with rounding...

#define WALL_TEXTURE_SCALE   0.050f   // Wall texture repeats every 20ft...
//#define WALL_TEXTURE_SCALE   0.0625f   // Wall texture repeats every 16ft...

#define EXTERIOR_VERTEX             0
#define EXTERIOR_TEX_VERTEX         1
#define EXTERIOR_TRIANGLE           2

extern Uint16 global_num_vertex;
extern Uint16 global_num_tex_vertex;
extern Uint16 global_num_triangle;
extern Uint16 global_room_active_group;

extern float autotrim_length;
extern float autotrim_offset;
extern float autotrim_scaling;

#define MAX_ROOM_SELECT 8192

extern Uint8 plopping_bridge;
extern Uint16 room_select_num;
extern Uint16 room_select_list[MAX_ROOM_SELECT];
extern Uint16 room_select_index;
extern float room_select_xyz[MAX_ROOM_SELECT][3];
extern Uint8* room_select_data[MAX_ROOM_SELECT];
extern Uint16 room_select_flag[MAX_ROOM_SELECT];
extern Uint8 room_select_axes[MAX_ROOM_SELECT];
extern Uint16 room_autotrim_select_count[3];

// ??? like FOCUS_INFO ???
extern Uint16 global_room_active_object;
extern Uint8* global_room_active_object_data;

void room_select_add(Uint16 item, Uint8* item_xyz, Uint8 axes);
Uint8 room_select_inlist(Uint16 item);
void room_select_remove(Uint16 item);
void room_select_update_xyz(void);
void room_weldable_vertex_clear(void);
void room_weldable_vertex_add(Uint16 vertex, Uint16 segment, float* intersection_info);
void room_weldable_vertex_remove(Uint16 vertex);
void intersect_xy_lines(float* info_a, float* info_b, float* write_xy);
Uint8* room_ddd_plop_function(Uint8 function, Uint8* write, Uint8* ddd_file_start, Uint8 base_model, Uint8* texture_list, float* position_xyz, float* fore_xy, Uint8 pillar_stretch);
Uint8* room_plop_all_function(Uint8 function, Uint8* write, Uint8* data, Uint8* srf_file, Uint8* ddd_wall_door_file, Uint16 object_group, Uint16 rotation);
Uint8* room_wall_segment_function(Uint8 function, Uint8* srf_file, Uint8* write, Uint8* vertex_data, Uint8* tex_vertex_data, Uint16 segment, Uint16 num_segment, Uint16 vertex, Uint16 last_vertex, Uint8* ddd_wall_door_file, Uint8 base_model, float wall_texture_offset, float wall_texture_scale, Uint16 rotation);
Uint8* room_exterior_wall_function(Uint8 function, Uint8* srf_file, Uint8* write, Uint8* vertex_data, Uint8* tex_vertex_data, Uint8* ddd_wall_door_file, Uint8* door_wall_list, Uint16 rotation);
float room_heightmap_height_slow(Uint8* data, float x, float y);
void room_heightmap_clear(Sint16* heightmap_data);
void room_heightmap_triangle(Sint16* heightmap_data, float* fa_xyz, float* fb_xyz, float* fc_xyz);
float room_srf_waypoint_distance(Uint8* waypoint_data, Uint8 from_waypoint, Uint8 to_waypoint);
void room_srf_find_goto_waypoint(Uint8* waypoint_data, Uint8* waypoint_line_data, Uint8 initial_waypoint);
void room_setup_lighting(Uint8* data, float* light_xyz, float ambient);
void room_set_texture_data(Uint8* texture_data, Uint8 texture, Uint32 hardware_texture, Uint8 flags);
void room_spawn_all(Uint8* srf_file, Uint16 rotation, Uint8* door_wall_list, Uint8* object_defeated_list, Uint8 difficulty, Uint16 seed, Uint16 object_group);
void room_edge_line_add(Uint8* edge_line_data, Uint16 vertex_a, Uint16 vertex_b, Uint16 check_vertex);
void room_restock_monsters(Uint8* srf_file, Uint8* object_defeated_list, Uint8 difficulty, Uint16 seed, Uint16 object_group);
Uint8 room_uncompress(Uint8* srf_file, Uint8* destination_buffer, Uint8* ddd_wall_door_file, Uint16 rotation, Uint8* door_wall_list, Uint8* object_defeated_list, Uint8 difficulty, Uint16 seed, Uint16 object_group);
void room_get_point_xyz(PSoulfuScriptContext pss, float percent_x, float percent_y, float* final_x, float* final_y, float* final_z, float scale, Uint8 z_finder_mode, Uint8 snap_mode);
void room_srf_move(void);
void room_srf_triangle_vertex_replace(Uint8* data, Uint16 vertex_to_replace, Uint16 vertex_to_use_instead, Uint8 vertex_was_deleted);
void room_srf_triangle_tex_vertex_replace(Uint8* data, Uint16 tex_vertex_to_replace);
void room_srf_minimap_clear(Uint8* data);
void room_srf_clear_exterior(Uint8* data, Uint16 vertex, Uint8 force_clear);
Uint8 room_srf_add_next_exterior_wall(Uint8* data, Uint16 vertex);
void room_srf_exterior_wall_vertex_replace(Uint8* data, Uint16 vertex_to_replace, Uint16 vertex_to_use_instead);
Uint8 room_srf_exterior_wall_flags(Uint8* data, Uint16 vertex_a, Uint16 vertex_b, Uint8 overwrite_flags, Uint8 new_flags);
Uint8 room_srf_delete_triangle(Uint8* data, Uint16 vertex_a, Uint16 vertex_b, Uint16 vertex_c);
Uint8 room_srf_add_waypoint(Uint8* data, float x, float y);
void room_srf_waypoint_replace(Uint8* data, Uint8 waypoint_to_replace);
Uint8 room_srf_link_waypoint(Uint8* data, Uint8 first, Uint8 second);
Uint8 room_srf_unlink_waypoint(Uint8* data, Uint8 first, Uint8 second);
Uint8 room_srf_delete_waypoint(Uint8* data, Uint8 waypoint);
void room_srf_build_waypoint_table(Uint8* srf_file);
Uint8 room_srf_add_bridge(Uint8* data, float ax, float ay, float bx, float by);
Uint8 room_srf_delete_bridge(Uint8* data, Uint8 bridge_to_delete);
Uint8 room_srf_intersection_helper(float* a_xy, float* b_xy, float* c_xy, float* d_xy);
Uint8 room_srf_minimap_create_triangle(Uint8* data, Uint16 va, Uint16 vb, Uint16 vc);
void room_srf_minimap_build(Uint8* data);
Uint8 room_srf_add_tex_vertex(Uint8* data, float x, float y);
Uint8 room_srf_delete_tex_vertex(Uint8* data, Uint16 tex_vertex);
Uint8 room_srf_add_texture_triangle(Uint8* data, Uint8 texture, Uint16 va, Uint16 vb, Uint16 vc, float* tvaxy, float* tvbxy, float* tvcxy);
void room_srf_autoweld_tex_vertices(Uint8* data);
void room_srf_clean_up_tex_vertices(Uint8* data);
Uint8 room_srf_textureflags(Uint8* data, Uint8 texture, Uint8 set_flags, Uint8 new_flags);
void room_srf_autotexture(Uint8* data);
void room_srf_autotrim(Uint8* data);
Uint8 room_srf_add_vertex(Uint8* data, float x, float y, float z, Uint8 select_add);
Uint8 room_srf_delete_vertex(Uint8* data, Uint16 vertex);
void room_srf_weld_selected_vertices(Uint8* data);
void room_group_set(Uint8* srf_file, Uint16 new_group_number);
void room_group_add(Uint8* srf_file, Uint8 delete_instead);
void room_object_set(Uint8* srf_file, Uint16 new_object_number);
void room_object_add(Uint8* srf_file, Uint8 delete_instead);
void room_srf_hardplopper(Uint8* srf_file, Uint8 width, Uint8 normalize, Uint8 type, Uint8 base_vertex_ledge, Uint8 randomize_ledge, Uint8 ledge_height);
void room_srf_copy(Uint8* data);
void room_srf_paste(Uint8* data);
Uint8 room_find_wall_center(Uint8* data, Uint16 rotation, Uint16 wall, float* center_xyz, float* offset_xyz, float normal_add);
Uint8 room_find_best_wall(Uint8* data, Uint8 allow_bottom_doors, Uint16 rotation, float* vector_xy, Uint8* walls_to_not_use);
void room_draw_srf(PSoulfuScriptContext pss, float x, float y, float z, Uint8* data, Uint8* color_rgb, Uint16 rotation, Uint8 mode);
void room_draw(Uint8* data);
Uint8 room_findpath(Uint8* data, float* from_xy, float* to_xy, float* write_xy, CHR_DATA* chr_data);
void room_shadow_test(float x, float y, float z, float radius);
void room_light_test(float x, float y, float z, float radius);

//-----------------------------------------------------------------------------------------------
// <ZZ> Macroized version of the function below...  This is straight forward enough...
#define room_heightmap_height(DATA, FX, FY)   ((*(((Sint16*) (DATA + (DEREF( Uint32, DATA+SRF_HEIGHTMAP_OFFSET ))))+((((Sint16) (((FY+ROOM_HEIGHTMAP_BIAS)*ROOM_HEIGHTMAP_PRECISION) + (ROOM_HEIGHTMAP_SIZE>>1))) & (ROOM_HEIGHTMAP_SIZE-1))*ROOM_HEIGHTMAP_SIZE)+(((Sint16) (((FX+ROOM_HEIGHTMAP_BIAS)*ROOM_HEIGHTMAP_PRECISION) + (ROOM_HEIGHTMAP_SIZE>>1))) & (ROOM_HEIGHTMAP_SIZE-1))))*ROOM_HEIGHTMAP_SCALE)



#define MAX_ROOM_TEXTURE 32

#ifdef DEVTOOL
extern Uint32 room_editor_texture[MAX_ROOM_TEXTURE];
#endif


#define room_select_clear() { room_select_num = 0; }

//*****************************************************
// tool.h

#define KANJI_ADD_VERTEX    0
#define KANJI_ADD_TRIANGLE  1
#define KANJI_MOV_VERTEX    2
#define KANJI_DEL_VERTEX    3


#define PITCH_DIVISIONS 25

extern Sint8 tracker_adding_note;
extern Uint16 tracker_adding_note_time;
extern Uint16 tracker_adding_note_duration;
extern Uint8 tracker_adding_note_pitch;
extern Sint8 tracker_stall;
extern Uint16 kanji_copy_from;


//*****************************************************
// dcodeddd.h
void ddd_generate_model_action_list(Uint8* data);
void ddd_remove_data(Uint8* data_block_start, Uint32 data_size, Uint8* data_to_remove, Uint16 bytes_to_remove);
void ddd_simplify_two_vertices(Uint16* num_vertex_spot, Uint16* num_tex_vertex_spot, Uint16 vertex_to_keep, Uint16 vertex_to_remove, Uint16 tex_vertex_to_keep, Uint16 tex_vertex_to_remove, Uint8* data, Uint32 data_size, Uint8 num_texture);
Uint8 ddd_simplify_once(Uint16* num_vertex_spot, Uint16* num_tex_vertex_spot, Uint8* data, Uint32 data_size, Uint8 num_texture);
void ddd_simplify_geometry(Uint16* num_vertex_spot, Uint16* num_tex_vertex_spot, Uint8* data, Uint8 percent, Uint32 data_size, Uint8 num_texture);
void ddd_continue_simplify(Uint16* num_vertex_spot, Uint16* num_tex_vertex_spot, Uint8* data, Uint8 percent, Uint32 data_size, Uint8 num_texture, Uint16 num_vertex);
void add_cartoon_line(Uint16 vertex_a, Uint16 vertex_b, Uint16 check, Uint8 force_line, Uint16* cartoon_data);
void remove_cartoon_lines(Uint16* cartoon_data);
Uint8* ddd_create_strip(Uint8* helper_data, Uint8* new_data, Uint16 main_triangle, Uint16 num_triangle);
void ddd_strip_fan_geometry(Uint16 num_vertex, Uint16 num_tex_vertex, Uint8* old_data, Uint8* helper_data, Uint8* new_data, Uint8 num_texture);
void ddd_decode_base_model(Uint8** olddata_spot, Uint8** newdata_spot, Uint8** newindex_spot, Uint8* newdata_start, Uint8 detail_level, float scale);
void ddd_decode_bone_frame(Uint8** olddata_spot, Uint8** newdata_spot, Uint8** newindex_spot, Uint8* newdata_start, float scale);
SDF_PHEADER decode_ddd(SDF_PHEADER ddd_header, const char* filename);
void ddd_magic_update_thing(Uint8 mask, float loadin_min, float loadin_max);
Sint8 rdy_to_ddd_export(FILE * openfile, SDF_PHEADER pheader);


//*****************************************************
// tool.h
int  tool_tracker(PSoulfuScriptContext pss, float tlx, float tly, float brx, float bry, Uint8* mus_data, int position, int snap, int instrument, int volume, Uint8 pan);
void tool_kanjiedit(PSoulfuScriptContext pss, Uint16 kanji, float x, float y, float scale, Uint16 edit_mode);


//*****************************************************
// experi.h
Uint16 experience_function_character(Uint16 character, Uint8 experience_type, Sint16 experience_amount, Uint8 affecting_single_character);

//*****************************************************



// Bump event callback stuff...
extern float global_bump_normal_xyz[3];
extern Uint16 global_bump_flags;
extern Uint16 global_bump_char;
extern Uint8 global_bump_abort;


// Strings for clicked and unclicked cursor...
extern Uint8  cursor_click[2];
extern Uint8  cursor_normal[2];
extern Uint8  cursor_target[2];


// Stuff for character collisions...
#define COLLISION_ALLOW_X_MOVEMENT 1
#define COLLISION_ALLOW_Y_MOVEMENT 2
#define COLLISION_ALLOW_Z_MOVEMENT 4
#define COLLISION_DISALLOW_X_MOVEMENT 6
#define COLLISION_DISALLOW_Y_MOVEMENT 5
#define COLLISION_DISALLOW_Z_MOVEMENT 3


// Stuff for room format...
#define SRF_VERTEX_OFFSET           40
#define SRF_TEX_VERTEX_OFFSET       44
#define SRF_TRIANGLE_OFFSET         48
#define SRF_EXTERIOR_WALL_OFFSET    52
#define SRF_TEXTURE_OFFSET          56
#define SRF_HEIGHTMAP_OFFSET        60
#define SRF_WAYPOINT_OFFSET         64
#define SRF_BRIDGE_OFFSET           68
#define SRF_MINIMAP_OFFSET          72
#define SRF_OBJECT_GROUP_OFFSET     76
#define SRF_EDGE_LINE_OFFSET        80


extern Uint16 global_luck_timer;


//-Things I Dislike Doing------------------------------------------------------------------------
SDF_PHEADER decode_jpg(SDF_PHEADER jpg_header, const char* filename);
SDF_PHEADER decode_pcx(SDF_PHEADER pcx_header, const char* filename);
SDF_PHEADER decode_ogg(SDF_PHEADER ogg_header, const char* filename);
SDF_PHEADER decode_ddd(SDF_PHEADER ddd_header, const char* filename);

void language_message_add(Uint16 message_index);
void message_add(char* message_text, char* speaker_name, Uint8 sanitize);
void setup_shadow(void);
Uint8* render_get_set_external_linkage(Uint8* data, Uint8* link_data);
Uint8 render_pregenerate_normals(Uint8* data, Uint16 frame, Uint8 detail_level);
Uint8 particle_attach_to_character(Uint16 particle, Uint16 character, Uint8 bone_type);
void render_rdy(SOULFU_WINDOW *pwin, Uint8* data, Uint16 frame, Uint8 mode, Uint8** texture_file_data_block, Uint8 main_alpha, Uint8* bone_frame_data, Uint8 petrify, Uint8 eye_frame);
Uint8* render_generate_model_world_data(Uint8* data, Uint16 frame, float* matrix, Uint8* write);
void render_rdy_shadow(PSoulfuScriptContext pss, Uint8* data, Uint16 frame, float x, float y, float z, Uint8 mode);
void character_button_function(Uint16 character, Uint8 code, Uint8 button, Uint8 axis);
float room_heightmap_height_slow(Uint8* data, float x, float y);
void matrix_good_bone(matrix_4x3 bone_matrix, Uint8 bone, Uint8* data_start, CHR_DATA* chr_data);
void particle_draw(PRT_DATA* i8_particle_data);
void room_srf_build_waypoint_table(Uint8* srf_file);
void water_drown_delay_setup();
void character_bone_frame_clear();
void character_update_all();
void input_read(void);

void decode_sdf_archive(Uint8 mask, float loadin_min, float loadin_max);

//-----------------------------------------------------------------------------------------------
#ifdef _SDL_H
extern Uint32 last_ticks;

#    define do_delay() { SDL_Delay(1); input_read(); }
#    define UI_INTERRUPT(draw_loadin, i, loadin_min, loadin_max) \
        { \
            Uint32 ticks = SDL_GetTicks(); \
            if (NULL == last_ticks) \
                last_ticks = ticks; \
            if (ticks - last_ticks > 100) \
            { \
                last_ticks = ticks; \
                \
                do_delay(); \
                if (draw_loadin) \
                { \
                    float loc_frac = CAST(float, i) / CAST(float, sdf_archive_get_num_files()); \
                    display_loadin((loadin_max - loadin_min)*loc_frac  + loadin_min, ktrue); \
                } \
            } \
        }
#else
#    define do_delay()
#    define UI_INTERRUPT()
#endif


extern char run_string[MAX_STRING][MAX_STRING_SIZE];