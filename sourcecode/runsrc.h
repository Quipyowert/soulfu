#pragma once

#include "soulfu_types.h"
#include "script_extensions.h"

bool_t               sf_run_setup(PSoulfuGlobalScriptContext psgs );

PSoulfuScriptContext sf_script_spawn(PSoulfuGlobalScriptContext psgs, PSoulfuScriptContext pss);
PSoulfuScriptContext sf_script_init(PSoulfuScriptContext pss, Uint8 *file_start, Uint8 * pdata);

bool_t               sf_fast_run_script(Uint8 *file_start, Uint16 fast_function, Uint8 * pdata);
bool_t               sf_fast_rerun_script(PSoulfuScriptContext pss, Uint16 fast_function);

bool_t               sf_run_script(PSoulfuScriptContext pss, bool_t share_itemid_pool);