#pragma once

#include <math.h>
#include <SDL_types.h>

#define XX 0
#define YY 1
#define ZZ 2


#define PI          3.1415926535897932384626433832795f
#define TWO_PI      6.283185307179586476925286766559f
#define FOUR_PI    12.566370614359172953850573533118f
#define PI_DIV_2   1.5707963267948966192313216916398f
#define PI_DIV_4   0.78539816339744830961566084581988f
#define PI_DIV_8   0.39269908169872415480783042290994f
#define PI_DIV_16  0.19634954084936207740391521145497f
#define PI_DIV_32  0.098174770424681038701957605727484f
#define PI_DIV_64  0.049087385212340519350978802863742f

#define SQRT_2      1.4142135623730950488016887242097f

#define INV_TWO_PI 0.15915494309189533576888376337251f
#define INV_SQRT_2 0.70710678118654752440084436210485f

#define ABS(A) { A = (A < 0) ? -A : A; }

#ifndef MIN
#    define MIN(A,B) ( (A)<(B) ? (A) : (B) )
#endif

#ifndef MIN3
#    define MIN3(A,B,C) MIN(MIN(A,B),C)
#endif

#ifndef MAX
#    define MAX(A,B) ( (A)>(B) ? (A) : (B) )
#endif

#ifndef MAX3
#    define MAX3(A,B,C) MAX(MAX(A,B),C)
#endif

#define SIN(ARG)   ((float)sin(ARG))
#define COS(ARG)   ((float)cos(ARG))
#define SQRT(ARG)  ((float)sqrt(ARG))
#define FABS(ARG)  ((float)fabs(ARG))
#define FLOOR(ARG) ((float)floor(ARG))
#define CEIL(ARG)  ((float)ceil(ARG))
#define ATAN(ARG)  ((float)atan(ARG))
#define ATAN2(ARG1, ARG2) ((float)atan2(ARG1, ARG2))


#define CLIP(LO, A, HI)    \
    {                          \
        A = (A < LO) ? LO : A;   \
        A = (A > HI) ? HI : A;   \
    }

#define INV_UINT16_SIZE 0.0000152587890625f
#define INV_UINT08_SIZE 0.00390625f

#define UINT16_TO_FLOAT INV_UINT16_SIZE
#define RAD_TO_UINT16   (UINT16_SIZE * INV_TWO_PI)
#define UINT16_TO_RAD   (INV_UINT16_SIZE * TWO_PI)

#define UINT08_TO_FLOAT INV_UINT08_SIZE
#define RAD_TO_UINT08   (UINT08_SIZE * INV_TWO_PI)
#define UINT08_TO_RAD   (INV_UINT08_SIZE * TWO_PI)

#define UINT16_360DEG   UINT16_SIZE
#define UINT16_180DEG   (UINT16_360DEG / 2)

#define DEG_TO_RAD      0.017453292519943295769236907684886f
#define RAD_TO_DEG     57.295779513082320876798154814105f

#define INV_0x10   0.0625f
#define INV_0x20   0.03125f
#define INV_0x40   0.015625f
#define INV_0x80   0.0078125f
#define INV_0xFF   0.003921568627450980392156862745098f
#define INV_0x0100 0.00390625f
#define INV_0x01FF 0.0019569471624266144814090019569472f
#define INV_0x0200 0.001953125f
#define INV_0x03FF 0.00097751710654936461388074291300098f
#define INV_0x0400 0.0009765625f
#define INV_0x0800 0.00048828125f

#define INV_100      0.01f
#define INV_1000     0.001f
#define INV_10000    0.0001f
#define INV_100000   0.00001f
#define INV_1000000  0.000001f
#define INV_10000000 0.0000001f

#define INV_16   INV_0x10
#define INV_32   INV_0x20
#define INV_64   INV_0x40
#define INV_128  INV_0x80
#define INV_255  INV_0xFF
#define INV_256  INV_0x0100
#define INV_511  INV_0x01FF
#define INV_512  INV_0x0200
#define INV_1024 INV_0x0400
#define INV_2048 INV_0x0800

// -- trig tables ---------------------------------------------------------------------
#define NUM_SINE_BITS             12
#define NUM_SINE_ENTRIES         (1 << NUM_SINE_BITS)
#define TRIG_TABLE_MASK        ( (1 << NUM_SINE_BITS) - 1   )
#define UINT16_TO_TRIG_ARG(XX) ( CAST(Uint16, XX) >> (16-NUM_SINE_BITS) )
#define TRIG_ARG_TO_UINT16(XX) ( (XX) << (16-NUM_SINE_BITS) )

extern float sine_table[NUM_SINE_ENTRIES];
extern float cosine_table[NUM_SINE_ENTRIES];

void sine_table_setup();

// -- vector typedefs -----------------------------------------------------------------
// ------------------------------------------------------------------------------------

typedef float matrix_4x3[12];

#pragma pack(push,1)

union vec2_u
{
    float v[2];
    struct { float x, y; };
} SET_PACKED();
typedef union vec2_u vec2, *pvec2;

union vec3_u
{
    float v[3];
    struct { float x, y, z; };
} SET_PACKED();
typedef union vec3_u vec3, *pvec3;

union vec4_u
{
    float v[4];
    struct { float x, y, z, w; };
} SET_PACKED();
typedef union vec4_u vec4, *pvec4;

union vec2_ui8_u
{
    Uint8 v[2];
    struct { Uint8 x, y; };
} SET_PACKED();
typedef union vec2_ui8_u vec2_ui8, *pvec2_ui8;

union vec3_ui8_u
{
    Uint8 v[3];
    struct { Uint8 x, y, z; };
    struct { Uint8 r, g, b; };
} SET_PACKED();
typedef union vec3_ui8_u vec3_ui8, *pvec3_ui8;

union vec4_ui8_u
{
    Uint8 v[4];
    struct { Uint8 x, y, z, w; };
    struct { Uint8 r, g, b, a; };
} SET_PACKED();
typedef union vec4_ui8_u vec4_ui8, *pvec4_ui8;


union vec2_ui16_u
{
    Uint16 v[2];
    struct { Uint16 x, y; };
} SET_PACKED();
typedef union vec2_ui16_u vec2_ui16, *pvec2_ui16;

union vec2_si16_u
{
    Sint16 v[2];
    struct { Sint16 x, y; };
} SET_PACKED();
typedef union vec2_si16_u vec2_si16, *pvec2_si16;

union vec3_ui16_u
{
    Uint16 v[3];
    struct { Uint16 x, y, z; };
} SET_PACKED();
typedef union vec3_ui16_u vec3_ui16, *pvec3_ui16;

union vec3_si16_u
{
    Sint16 v[3];
    struct {  Sint16 x, y, z; };
} SET_PACKED();
typedef union vec3_si16_u vec3_si16, *pvec3_si16;

union vec2_ui32_u
{
    Uint16 v[2];
    struct {  Uint16 x, y; };
} SET_PACKED();
typedef union vec2_ui32_u vec2_ui32, *pvec2_ui32;

union vec3_ui32_u
{
    Uint16 v[3];
    struct {  Uint16 x, y, z; };
} SET_PACKED();
typedef union vec3_ui32_u vec3_ui32, *pvec3_ui32;

union vec2_si32_u
{
    Sint16 v[2];
    struct {  Sint16 x, y; };
} SET_PACKED();
typedef union vec2_si32_u vec2_si32, *pvec2_si32;

union vec3_si32_u
{
    Sint16 v[3];
    struct { Sint16 x, y, z; };
} SET_PACKED();
typedef union vec3_si32_u vec3_si32, *pvec3_si32;
#pragma pack(pop)


// ------------------------------------------------------------------------------------
// -- matrix typedefs -----------------------------------------------------------------

#pragma pack(push,1)
struct mat_float_4x4_t
{
    float v[16];
} SET_PACKED();
typedef struct mat_float_4x4_t mat_flt_4x4, *pmat_flt_4x4;

struct mat_float_3x3_t
{
    float v[16];
} SET_PACKED();
typedef struct mat_float_3x3_t mat_flt_3x3, *pmat_flt_3x3;

struct mat_float_3x4_t
{
    float v[12];
} SET_PACKED();
typedef struct mat_float_3x4_t mat_flt_3x4, *pmat_flt_3x4;

struct mat_float_2x3_t
{
    float v[6];
} SET_PACKED();
typedef struct mat_float_2x3_t mat_flt_2x3, *pmat_flt_2x3;

#pragma pack(pop)
