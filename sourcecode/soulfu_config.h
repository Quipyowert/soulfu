#pragma once

#include <ctype.h>
#include <SDL_types.h>

#ifdef WIN32
#    define SLASH_CHR '\\'
#    define SLASH_STR "\\"
#else
#    define SLASH_CHR '/'
#    define SLASH_STR "/"
#endif

#define DEFAULT_H 300
#define DEFAULT_W 400

#define EOS            '\0'    // the end of string character
#define EMPTY_STRING { EOS }   // shortcut initializer for setting a char array to "" at define-time

// Uncomment this for debug info...
//#define DEVTOOL                     // Compile time option for running in developer mode
//#define SHOW_JOYSTICK_POSITIONS     // Compile time option to show joystick movement...
#define KEEP_COMPRESSED_FILES       // Compile time option to not delete compressed data...  DO NOT REMOVE!!!
//#define CAN_BLOW_SELF_UP            // Compile time option for being able to hurt self...
#define BACKFACE_CARTOON_LINES      // Seem to perform slightly better, and look a lot nicer...
//#define VERBOSE_COMPILE

#undef DEBUG_SRCDECODE

#undef COMPATABILITY_64_BIT

// define the byteorder of external files
#define EXTERN_BYTEORDER SDL_BIG_ENDIAN

//MSVC inline function keyword
#if defined(_MSC_VER)
#    define _INLINE _inline  /* In MS visual C, the "inline" keyword seems to be depreciated. Must to be promoted to "_inline" of "__inline" */
#else
#    define _INLINE static inline
#endif

#ifdef WIN32
#pragma warning(disable: 4996)  // disable warning deprecated posix names, and C++ ISO names.
// This is needed for linking purposes.
#define snprintf _snprintf
#endif

