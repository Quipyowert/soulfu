#pragma once

#include "soulfu_types.h"
#include "script_extensions.h"
#include "soulfu.h"

// Ex. Player 2 is character 1023...    MAX_CHARACTER means invalid...
extern Uint16 local_player_character[MAX_LOCAL_PLAYER];

// Teams
#define TEAM_NEUTRAL   0 // Default team
#define TEAM_MONSTER   1
#define TEAM_GOOD      2
#define TEAM_EVIL      3

#define CHR_FLT_OFF_x             0 //
#define CHR_FLT_OFF_y             4 //
#define CHR_FLT_OFF_z             8 //
#define CHR_FLT_OFF_gotox        12 //
#define CHR_FLT_OFF_gotoy        16 //
#define CHR_FLT_OFF_hoverz       20 // only used if CHAR_HOVER_ON
#define CHR_FLT_OFF_vx           24 //
#define CHR_FLT_OFF_vy           28 //
#define CHR_FLT_OFF_vz           32 //
#define CHR_I08_OFF_btimer       36//
#define CHR_I16_OFF_pstimer      40 // Poison timer
#define CHR_I16_OFF_pttimer      42 // Petrify timer
#define CHR_FLT_OFF_floorz       44 // floor level below character
#define CHR_I08_OFF_flatten      48 // Character flatness...  0xFF is normal...
#define CHR_I08_OFF_timer_drown  49 // used internally for character drown timer
#define CHR_I08_OFF_lastway      50 // Last waypoint the character was at...  For findpath
#define CHR_I08_OFF_nextway      51 // Next waypoint the character is going to...  For findpath
#define CHR_I16_OFF_timer        52 // Refresh timer
#define CHR_I16_OFF_evtimer      54 // Event timer
#define CHR_I16_OFF_spin         56 //
#define CHR_I16_OFF_vspin        58 //
#define CHR_I16_OFF_flags        60 //
#define CHR_I16_OFF_target       62 // Stores index...  No target.x stuff...
#define CHR_I08_OFF_bone         64 // Part uses for stuck parts...
#define CHR_I08_OFF_action       65 // current action
#define CHR_I08_OFF_daction      66 // default action when current action ends
#define CHR_I08_OFF_event        67 //
#define CHR_I16_OFF_morphoc      68 // index of morph's old character  so we can respawn it on disenchant
#define CHR_I16_OFF_exp          70 // experience
#define CHR_I08_OFF_cbtimer      72 // Combo timer...  Reset to 0 every time hit...  Decrements combo count after it counts up to a certain number...
#define CHR_I08_OFF_level        73 // character's experience level
#define CHR_I08_OFF_statpts      74 // Points to spend upgrading character
#define CHR_I08_OFF_combo        75 // How many times in a row character has been hit...  Decrements every now and then
#define CHR_I16_OFF_owner        76 //
#define CHR_I08_OFF_team         78 //
#define CHR_I08_OFF_alpha        79 //
#define CHR_I08_OFF_hitsmax      80 // The character's stats
#define CHR_I08_OFF_hitstap      81 //
#define CHR_I08_OFF_hits         82 //
#define CHR_I08_OFF_manamax      83 //
#define CHR_I08_OFF_manatap      84 //
#define CHR_I08_OFF_mana         85 //
#define CHR_I08_OFF_str_min      86 //
#define CHR_I08_OFF_dex_min      87 //
#define CHR_I08_OFF_int_min      88 //
#define CHR_I08_OFF_vir          89 // virtue array
#define CHR_I08_OFF_unused_94    94 // unused
#define CHR_I08_OFF_unused_95    95 // unused
#define CHR_I08_OFF_oflags       96 // Other flags
#define CHR_I08_OFF_def          97 //
#define CHR_I08_OFF_mflags      104 // More flags
#define CHR_I08_OFF_vflags      105 // Virtue flags
#define CHR_I16_OFF_rider       106 // Index of the character who's riding current character... == UINT16_MAX for none...
#define CHR_FLT_OFF_sidex       108 // The character's rotation matrix
#define CHR_FLT_OFF_sidey       112 //
#define CHR_FLT_OFF_sidez       116 //
#define CHR_FLT_OFF_frontx      120 //
#define CHR_FLT_OFF_fronty      124 //
#define CHR_FLT_OFF_frontz      128 //
#define CHR_FLT_OFF_upx         132 //
#define CHR_FLT_OFF_upy         136 //
#define CHR_FLT_OFF_upz         140 //
#define CHR_STR_OFF_name        144 //
#define CHR_FLT_OFF_boxsize     160 //
#define CHR_I16_OFF_mount       164 // Index of the character who current character is riding... UINT16_MAX for none...
#define CHR_I16_OFF_nctimer     166 // No Collision Timer...  Character doesn't collide with other chars or particles
#define CHR_I16_OFF_dmtimer     168 // Damage timer...  Makes color change
#define CHR_I16_OFF_intimer     170 // Invincibility timer...  Character doesn't collide with particles
#define CHR_I16_OFF_vrtimer     172 // Virtue timer
#define CHR_I08_OFF_theight     174 // Temporary height...  Saves actual height when knocked out
#define CHR_I08_OFF_height      175 //
#define CHR_I16_OFF_aitimer     176 // AI timer
#define CHR_I16_OFF_frame       178 //
#define CHR_I16_OFF_food        180 //
#define CHR_I16_OFF_eframe      182 // Eye frame
#define CHR_I16_OFF_mframe      184 // Mouth frame
#define CHR_I08_OFF_eaction     186 // Eye action
#define CHR_I08_OFF_maction     187 // Mouth action
#define CHR_I16_OFF_money       188 // Amount of money character is carrying
#define CHR_I08_OFF_keys        190 // Number of keys character is carrying
#define CHR_I08_OFF_cdtimer     191 // Collision delay timer character only gets hit wall events if this is 0...
#define CHR_I08_OFF_aistate     192 // generic AI variable
#define CHR_I08_OFF_aimode      193 // generic AI variable
#define CHR_I16_OFF_rltimer     194 // Reload left timer can't use left button when > 0 also for special 1
#define CHR_I16_OFF_rrtimer     196 // Reload right timer can't use right button when > 0 also for special 2
#define CHR_I08_OFF_bflags      198 // Button state flags     Low 4 bits for pressed     High 4 bits for unpressed
#define CHR_I08_OFF_baxis       199 // Button axis flags     Low 4 bits     AXIS_UP etc...     High 4 bits for button down flags...
#define CHR_I16_OFF_setimer     200 // Secondary event timer
#define CHR_I16_OFF_tetimer     202 // Thirdiary event timer
#define CHR_I08_OFF_cclass      204 // Character class sent if CHAR_FULL_NETWORK
#define CHR_I08_OFF_sflags      205 // Script flags
#define CHR_I08_OFF_bright      206 // Character brightness...  0xFF is normal...
#define CHR_I08_OFF_charge      207 // For chargeable weapons & stuff...
#define CHR_I16_OFF_lasthit     208 // Index of the character who hit our character with poison     or who killed 'em... Used to figger who gets the experience...
#define CHR_I16_OFF_expgive     210 // Experience points that this character gives when killed
#define CHR_FLT_OFF_flyfric     212 // Flying friction...  Defaults to 0.95 on spawn
#define CHR_I08_OFF_eflags      216 // Enchantment flags...
#define CHR_I08_OFF_manacst     217 // Number of stat points required for mana - usually 5
#define CHR_I08_OFF_unused_218  218
#define CHR_I08_OFF_unused_219  219
#define CHR_I08_OFF_unused_220  220
#define CHR_I08_OFF_unused_221  221
#define CHR_I08_OFF_unused_222  222
#define CHR_I08_OFF_unused_224  224
#define CHR_I08_OFF_item        224 // inventory
#define CHR_I08_OFF_eqcol01     240 // Color slider 0 is high 4 bits, Color slider 1 is low 4 bits (actual meaning interpreted by script) (always sent over network)
#define CHR_I08_OFF_eqcol23     241 // Color slider 2 is high 4 bits, Color slider 3 is low 4 bits (actual meaning interpreted by script) (sent if CHAR_FULL_NETWORK)
#define CHR_I08_OFF_eqslot      242
#define CHR_I08_OFF_rm_ob_num   249 // room object number - for remembering which have been defeated...  Used in CRANDOM by number...  Used in PTRAP by number...
#define CHR_I08_OFF_remote_ind  250 // the remote index number (the character number as it is on the remote's computer that is running this character)
#define CHR_I08_OFF_net_scr_ind 251 // network script index (see NETLIST.DAT)
#define CHR_UIP_OFF_rem_ip_add  252

#define MODEL_BASE_FILE    0x0100
#define MODEL_CHEST_FILE   280
#define MODEL_ARMS_FILE    304
#define MODEL_HANDS_FILE   328
#define MODEL_LEGS_FILE    352
#define MODEL_SHOES_FILE   376
#define MODEL_HEAD_FILE    400
#define MODEL_EYES_FILE    424
#define MODEL_HAIR_FILE    448
#define MODEL_MOUTH_FILE   472
#define MODEL_RIDER_FILE   496
#define MODEL_LEFT_FILE    520
#define MODEL_RIGHT_FILE   544
#define MODEL_LEFT2_FILE   568
#define MODEL_RIGHT2_FILE  592
// Data size must be 616 for full load of objects...

// Action names...
#define ACTION_BONING            0
#define ACTION_STAND             1
#define ACTION_WALK              2
#define ACTION_STUN_BEGIN        3
#define ACTION_STUN              4
#define ACTION_STUN_END          5
#define ACTION_KNOCK_OUT_BEGIN   6
#define ACTION_KNOCK_OUT         7
#define ACTION_KNOCK_OUT_STUN    8
#define ACTION_KNOCK_OUT_END     9
#define ACTION_BASH_LEFT         10
#define ACTION_BASH_RIGHT        11
#define ACTION_THRUST_LEFT       12
#define ACTION_THRUST_RIGHT      13
#define ACTION_SLASH_LEFT        14
#define ACTION_SLASH_RIGHT       15
#define ACTION_ATTACK_FAIL       16
#define ACTION_BLOCK_BEGIN       17
#define ACTION_BLOCK             18
#define ACTION_BLOCK_END         19
#define ACTION_JUMP_BEGIN        20
#define ACTION_JUMP              21
#define ACTION_JUMP_END          22
#define ACTION_RIDE              23
#define ACTION_SWIM              24
#define ACTION_SWIM_FORWARD      25
#define ACTION_MAGIC             26
#define ACTION_FIRE_BEGIN        27
#define ACTION_FIRE_READY        28
#define ACTION_FIRE              29
#define ACTION_FIRE_END          30
#define ACTION_EXTRA             31
#define ACTION_SPECIAL_0         32
#define ACTION_SPECIAL_1         33
#define ACTION_SPECIAL_2         34
#define ACTION_SPECIAL_3         35
#define ACTION_SPECIAL_4         36
#define ACTION_SPECIAL_5         37
#define ACTION_SPECIAL_6         38
#define ACTION_SPECIAL_7         39
#define ACTION_SPECIAL_8         40
#define ACTION_SPECIAL_9         41
#define ACTION_SPECIAL_10        42
#define ACTION_SPECIAL_11        43
#define ACTION_SPECIAL_12        44
#define ACTION_DOUBLE_BEGIN      45
#define ACTION_DOUBLE            46
#define ACTION_DOUBLE_END        47
#define ACTION_MAX              128              // Number of action types...

#define PART_FLAGS(PDATA)   (DEREF( Uint16, CAST(Uint8*,PDATA)+60 ))
#define CHAR_FLAGS(PDATA)   (DEREF( Uint16, CAST(Uint8*,PDATA)+60 ))
#define MOUNT_FLAGS(PDATA)  (DEREF( Uint16, CAST(Uint8*,PDATA)+60 ))
#define CHECK_FLAGS(PDATA)  (DEREF( Uint16, CAST(Uint8*,PDATA)+60 ))
#define TARGET_FLAGS(PDATA) (DEREF( Uint16, CAST(Uint8*,PDATA)+60 ))


// Character flags...
#define CHAR_FALL_ON         1      // Character is falling if ktrue...
#define CHAR_FUZZY_ON        2      // Character is drawn in second pass (along with transparent chars...)
#define CHAR_ON_GROUND       4      // Character is standing on the ground...
#define CHAR_GRAVITY_ON      8      // Character is affected by gravity...
#define CHAR_IN_WATER        16     // Character is in/under water
#define CHAR_NO_COLLISION    32     // Character doesn't collide with other characters
#define CHAR_FINISH_ACTION   64     // Character shouldn't switch between walk & stand in mid stride...
#define CHAR_NO_STUCK_PARTS  128    // Character can't have arrows attached to 'em...  Must check manually in script...
#define CHAR_ON_PLATFORM     256    // Character is standing on a platform (used internally)
#define CHAR_CAN_BE_MOUNTED  512    // Character can be mounted
#define CHAR_CAN_RIDE_MOUNT  1024   // Character can ride mounts
#define CHAR_LITTLE_BOX      2048   // Character has a boxsize of 1.0 for room geometry collisions (but not for character-particle or character-character collisions)
#define CHAR_FULL_NETWORK    4096   // All of character's equipment data is sent over network (all armor & actions, not just 2 weapons)
#define CHAR_PLATFORM_ON     8192   // Character can be stood on...
#define CHAR_HOVER_ON        16384  // Character moves with sluggish velz and floats towards a given z position...
#define CHAR_NO_BACKSTAB     32768  // Character is immune to backstabs...


// Particle flags...
#define PART_CHAR_COLLISION_ON 1    // Part collides with characters...
#define PART_WALL_COLLISION_ON 2    // Part collides with heightmap/terrain...
#define PART_IGNORE_INTIMER 4       // Part collides with characters even if they are invincible (part should check in script)...
#define PART_FIT_LENGTH_ON 8        // 2 point particle is scaled to a set length...  So arrows don't stretch...
#define PART_SPIN_ON       16       // Part is a 1 point spinny particle
#define PART_FLAT_ON       32       // Part is drawn flat on xy plane (like ripples on water)
#define PART_STUCK_ON      64       // Part is stuck to a character...
#define PART_GRAVITY_ON    128      // Part is pulled by gravity
#define PART_LIGHT_ON      256      // Part is drawn with additive transparency
#define PART_NUMBER_ON     512      // Part image is a number from 0-99
#define PART_IN_WATER      1024     // Part is under water
#define PART_AFTER_WATER   2048     // Part is drawn after water (so bubbles are more visible)
#define PART_SLOW_ON       4096     // Part is moving slow, so character collisions can be done less often...
#define PART_FAST_ON       8192     // Part is moving extra fast...  Update 10 times per frame (instead of just once)...
#define PART_HIT_OWNER_ON  16384    // Part can damage its owner...  Only if friendly fire is active...
#define PART_ATTACK_SPIN_ON 32768   // Attack spin is set from particle location instead of owner's direction...


// Virtue flags...  (for characters...  Because I ran out of character flags again...)
#define VIRTUE_FLAG_HITS         1  // Prayer whammy
#define VIRTUE_FLAG_EATS_TRIPE   2  // Character can be healed/charmed with tripe
#define VIRTUE_FLAG_AFTER_WATER  4  // Character is always drawn after water...
#define VIRTUE_FLAG_NO_DROWN     8  // Character doesn't take drown damage...
#define VIRTUE_FLAG_STILL_HIDE  16  // Character is hidden when action == ACTION_STAND (enemies have trouble acquiring character as a target)
#define VIRTUE_FLAG_RUN_ME_OVER 32  // Can be run over by tankes (without being knocked down first)
#define VIRTUE_FLAG_BUMP_IMMUNE 64  // Immune to damage from bumps (unless bumper is also immune)
#define VIRTUE_FLAG_IMMUNE     128  // Immune to damage from own shots...


// More flags...  (for characters....  Because I ran out of character flags...)
#define MORE_FLAG_FAST_TURN   1   // Makes character not slow down when turning...
#define MORE_FLAG_PETRIFY_ON  2   // Don't mess with this in script...  Used internally to give petrify events...
#define MORE_FLAG_HIT_HEAD    4   // Hitting in head does double damage...
#define MORE_FLAG_PUSHABLE    8   // Character is pushable...
#define MORE_FLAG_PLATFALL    16  // Character falls through platforms
#define MORE_FLAG_NOSHADOW    64  // Character's volumetric shadow is never drawn (if pretty shadows are turned on)
#define MORE_FLAG_DOOR        128 // Weird collision thing...


// Other flags...  (for characters...  Ran out...)
#define OTHER_FLAG_ENCHANT_CENTER     1 // Character is using an ENCHANT.SRC spell and targeting circle has already been center'd...
#define OTHER_FLAG_NO_MORPH           2 // Character can't be Morph'd (for bosses & chests & stuff)
#define OTHER_FLAG_NO_HEARTS          4 // Character can't grab Hearts (for tankes & stuff)
#define OTHER_FLAG_EASY_ENTANGLE      8 // Character can be Entangle'd easily
#define OTHER_FLAG_NO_WALL_COLLIDE   16 // Character doesn't bump into walls...
#define OTHER_FLAG_ENCHANT_MOVED     32 // Character has moved the enchantment target cursor thing off of his own character (so we don't accidentally DChant ourselves)
// 64 is unused
// 128 is unused

enum MODEL_PARTS_E
{
    MODEL_PART_BASE = 0,
    MODEL_PART_LEGS,
    MODEL_PART_CHEST,
    MODEL_PART_ARMS,
    MODEL_PART_PADS,
    MODEL_PART_FEET,
    MODEL_PART_HEAD,
    MODEL_PART_HAIR,
    MODEL_PART_EYES,
    MODEL_PART_MOUTH,
    MODEL_PART_RIDER,
    MODEL_PART_LEFT,
    MODEL_PART_RIGHT,
    MODEL_PART_LEFT2,
    MODEL_PART_RIGHT2,
    MODEL_PART_COUNT
};

enum VIRTUE_E
{
    VIRTUE_COMPASSION   = 0,
    VIRTUE_DILIGENCE,
    VIRTUE_HONOR,
    VIRTUE_FAITH,
    VIRTUE_COURAGE,
    MAX_VIRTUE_TYPE
};

enum SLOT_E
{
    SLOT_LEFT,
    SLOT_RIGHT,
    SLOT_SPEC1,
    SLOT_SPEC2,
    SLOT_HELM,
    SLOT_BODY,
    SLOT_LEGS,
    SLOT_COUNT
};

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// the basic packed data definitions that intersace with the scripting engine

#pragma pack(push,1)

struct base_data_t
{
    float x;    // 0 :
    float y;    // 4 :
    float z;    // 8 :
    float unused_12;
    float unused_16;
    float unused_20;
    float vx;    // 24 :
    float vy;    // 28 :
    float vz;    // 0x20 :
    Uint8 unused_36;
    Uint8 unused_37;
    Uint8 unused_38;
    Uint8 unused_39;
    Uint16 unused_40;
    Uint16 unused_42;
    float unused_44;
    Uint8 unused_48;
    Uint8 unused_49;
    Uint8 unused_50;
    Uint8 unused_51;
    Uint16 timer;    // 52 : Refresh timer
    Uint16 evtimer;    // 54 : Event timer
    Uint16 spin;    // 56 :
    Uint16 vspin;    // 58 :
    Uint16 flags;    // 60 :
    Uint16 target;    // 62 : Stores index...  No target.x stuff...
    Uint8 bone;    // 0x40 : Part uses for stuck parts...
    Uint8 unused_65;
    Uint8 unused_66;
    Uint8 event;         // 67 :
    Uint16 unused_68;
    Uint16 unused_70;
    Uint8 unused_72;
    Uint8 unused_73;
    Uint8 unused_74;
    Uint8 unused_75;
    Uint16 owner;    // 76 :
    Uint8 team;    // 78 :
    Uint8 alpha;    // 79 :
} SET_PACKED();
//typedef struct base_data_t BASE_DATA;

//---------------------------------------------------------------------------------------------
#define MODEL_DATA_TEX 4
struct model_data_t
{
    Uint8 * file;
    Uint8 * tex_lst[MODEL_DATA_TEX];
    Uint32 color;
} SET_PACKED();
typedef struct model_data_t MODEL_DATA;

//---------------------------------------------------------------------------------------------

struct model_t
{
    MODEL_DATA parts[MODEL_PART_COUNT];
} SET_PACKED();
typedef struct model_t MODEL;

//---------------------------------------------------------------------------------------------

union uip_t
{
    Uint8  i08[4]; // 252 : remote IP Address (0.0.0.0 means hosted locally)
    Uint32 i32;
} SET_PACKED();
typedef union uip_t UIP;

//---------------------------------------------------------------------------------------------

struct chr_data_t
{
    float x;    // 0 :
    float y;    // 4 :
    float z;    // 8 :
    float gotox;    // 12 :
    float gotoy;    // 16 :
    float hoverz;    // 20 : only used if CHAR_HOVER_ON
    float vx;    // 24 :
    float vy;    // 28 :
    float vz;    // 32 :
    Uint8 btimer[4]; // 36:
    Uint16 pstimer;    // 40 : Poison timer
    Uint16 pttimer;    // 42 : Petrify timer
    float floorz;    // 44 : floor level below character
    Uint8 flatten;    // 48 : Character flatness...  0xFF is normal...
    Uint8 timer_drown;            // 49 : used internally for character drown timer
    Uint8 lastway;    // 50 : Last waypoint the character was at...  For findpath
    Uint8 nextway;    // 51 : Next waypoint the character is going to...  For findpath
    Uint16 timer;    // 52 : Refresh timer
    Uint16 evtimer;    // 54 : Event timer
    Uint16 spin;    // 56 :
    Uint16 vspin;    // 58 :
    Uint16 flags;    // 60 :
    Uint16 target;    // 62 : Stores index...  No target.x stuff...
    Uint8 bone;    // 64 : Part uses for stuck parts...
    Uint8 action;    // 65 : current action
    Uint8 daction;    // 66 : default action when current action ends
    Uint8 event;    // 67 :
    Uint16 morphoc;    // 68 : index of morph's old character  so we can respawn it on disenchant
    Uint16 exp_;    // 70 : experience
    Uint8 cbtimer;    // 72 : Combo timer...  Reset to 0 every time hit...  Decrements combo count after it counts up to a certain number...
    Uint8 level;    // 73 : character's experience level
    Uint8 statpts;    // 74 : Points to spend upgrading character
    Uint8 combo;    // 75 : How many times in a row character has been hit...  Decrements every now and then
    Uint16 owner;    // 76 :
    Uint8 team;    // 78 :
    Uint8 alpha;    // 79 :
    Uint8 hitsmax;    // 80 : The character's stats
    Uint8 hitstap;    // 81 :
    Uint8 hits;    // 82 :
    Uint8 manamax;    // 83 :
    Uint8 manatap;    // 84 :
    Uint8 mana;    // 85 :
    Uint8 str_min;    // 86 :
    Uint8 dex_min;    // 87 :
    Uint8 int_min;    // 88 :
    Uint8 vir[MAX_VIRTUE_TYPE];
    Uint8 unused_94;            // 94 : unused
    Uint8 unused_95;            // 95 : unused
    Uint8 oflags;    // 96 : Other flags
    Uint8 def[MAX_DAMAGE_TYPE]; // 97 :
    Uint8 mflags;    // 104 : More flags
    Uint8 vflags;    // 105 : Virtue flags
    Uint16 rider;    // 106 : Index of the character who's riding current character... == UINT16_MAX for none...
    float sidex;    // 108 : The character's rotation matrix
    float sidey;    // 112 :
    float sidez;    // 116 :
    float frontx;    // 120 :
    float fronty;    // 124 :
    float frontz;    // 0x80 :
    float upx;    // 132 :
    float upy;    // 136 :
    float upz;    // 140 :
    str16 name;    // 144 :
    float boxsize;    // 160 :
    Uint16 mount;    // 164 : Index of the character who current character is riding... UINT16_MAX for none...
    Uint16 nctimer;    // 166 : No Collision Timer...  Character doesn't collide with other chars or particles
    Uint16 dmtimer;    // 168 : Damage timer...  Makes color change
    Uint16 intimer;    // 170 : Invincibility timer...  Character doesn't collide with particles
    Uint16 vrtimer;    // 172 : Virtue timer
    Uint8 theight;    // 174 : Temporary height...  Saves actual height when knocked out
    Uint8 height;    // 175 :
    Uint16 aitimer;    // 176 : AI timer
    Uint16 frame;    // 178 :
    Uint16 food;    // 180 :
    Uint16 eframe;    // 182 : Eye frame
    Uint16 mframe;    // 184 : Mouth frame
    Uint8 eaction;    // 186 : Eye action
    Uint8 maction;    // 187 : Mouth action
    Uint16 money;    // 188 : Amount of money character is carrying
    Uint8 keys;    // 190 : Number of keys character is carrying
    Uint8 cdtimer;    // 191 : Collision delay timer character only gets hit wall events if this is 0...
    Uint8 aistate;    // 192 : generic AI variable
    Uint8 aimode;    // 193 : generic AI variable
    Uint16 rltimer;    // 194 : Reload left timer can't use left button when > 0 also for special 1
    Uint16 rrtimer;    // 196 : Reload right timer can't use right button when > 0 also for special 2
    Uint8 bflags;    // 198 : Button state flags     Low 4 bits for pressed     High 4 bits for unpressed
    Uint8 baxis;    // 199 : Button axis flags     Low 4 bits     AXIS_UP etc...     High 4 bits for button down flags...
    Uint16 setimer;    // 200 : Secondary event timer
    Uint16 tetimer;    // 202 : Thirdiary event timer
    Uint8 cclass;    // 204 : Character class sent if CHAR_FULL_NETWORK
    Uint8 sflags;    // 205 : Script flags
    Uint8 bright;    // 206 : Character brightness...  0xFF is normal...
    Uint8 charge;    // 207 : For chargeable weapons & stuff...
    Uint16 lasthit;    // 208 : Index of the character who hit our character with poison     or who killed 'em... Used to figger who gets the experience...
    Uint16 expgive;    // 210 : Experience points that this character gives when killed
    float flyfric;    // 212 : Flying friction...  Defaults to 0.95 on spawn
    Uint8 eflags;    // 216 : Enchantment flags...
    Uint8 manacst;    // 217 : Number of stat points required for mana - usually 5
    Uint8 unused_218;
    Uint8 unused_219;
    Uint8 unused_220;
    Uint8 unused_221;
    Uint8 unused_222;
    Uint8 unused_224;
    Uint8 item[16];    // 224 : inventory

    Uint8 eqcol01;      // 240 : Color slider 0 is high 4 bits, Color slider 1 is low 4 bits (actual meaning interpreted by script) (always sent over network)
    Uint8 eqcol23;      // 241 : Color slider 2 is high 4 bits, Color slider 3 is low 4 bits (actual meaning interpreted by script) (sent if CHAR_FULL_NETWORK)
    Uint8 eqslot[SLOT_COUNT];
    Uint8 rm_ob_num;         // 249 : room object number - for remembering which have been defeated...  Used in CRANDOM by number...  Used in PTRAP by number...
    Uint8 remote_ind;    // 250 : the remote index number (the character number as it is on the remote's computer that is running this character)
    Uint8 net_scr_ind;   // 251 : network script index (see NETLIST.DAT)
    UIP   rem_ip_add;    // 252 : remote ip address

    struct model_t model; // 256 : sizeof(struct model_t) = 360 = sizeof(struct model_data_t) * 15 = 24 * 15

    //---- EXTENDED DATA ----
    bool_t              on;
    bool_t              delete_me;
    char                script_name[8];
    SoulfuScriptContext script_ctxt;

    bool_t              reserve_on;

} SET_PACKED();
//typedef struct chr_data_t CHR_DATA;

//---------------------------------------------------------------------------------------------

union ustat_t
{
    Uint8  i08[4];
    Uint16 i16[2];
    Uint32 i32;
} SET_PACKED();
typedef union ustat_t USTAT;

//---------------------------------------------------------------------------------------------

struct win_data_t
{
    float x; // 0 :
    float y; // 4 :
    float z; // 8 :

    USTAT stat[15];

    Uint8 unused_72; // 72 : unused
    Uint8 unused_73; // 73 : unused
    Uint8 unused_74; // 74 : unused
    Uint8 unused_75; // 75 : unused

    str16 bigtext;   // 76 :
    str16 string[8]; // 92 :

    Uint16 binding;  // 220 :
    Uint8  slotype;  // 222 :
    Uint8  unused_223;  // 223 :
    Uint16 xtra[4];  // 224 :

    Uint8 unused_232;
    Uint8 unused_233;
    Uint8 unused_234;
    Uint8 unused_235;
    Uint8 unused_236;
    Uint8 unused_237;
    Uint8 unused_238;
    Uint8 unused_239;

    Uint8 eqcol01;      // 240 : Color slider 0 is high 4 bits, Color slider 1 is low 4 bits (actual meaning interpreted by script) (always sent over network)
    Uint8 eqcol23;      // 241 : Color slider 2 is high 4 bits, Color slider 3 is low 4 bits (actual meaning interpreted by script) (sent if CHAR_FULL_NETWORK)
    Uint8 eqslot[SLOT_COUNT];
    Uint8 rm_ob_num;         // 249 : room object number - for remembering which have been defeated...  Used in CRANDOM by number...  Used in PTRAP by number...
    Uint8 remote_ind;    // 250 : the remote index number (the character number as it is on the remote's computer that is running this character)
    Uint8 net_scr_ind;   // 251 : network script index (see NETLIST.DAT)

    UIP   rem_ip_add;

    struct model_t model;

    //---- EXTENDED DATA ----
    bool_t              on;
    bool_t              delete_me;
    char                script_name[8];
    SoulfuScriptContext script_ctxt;

} SET_PACKED();
//typedef struct win_data_t WIN_DATA;

//---------------------------------------------------------------------------------------------

union particle_misc_t
{
    Uint32 i32[2];
    Uint16 i16[4];
    Uint8  i08[8];
} SET_PACKED();
typedef union particle_misc_t PART_MISC;

//---------------------------------------------------------------------------------------------

struct prt_data_t
{
    float x;    // 0 :
    float y;    // 4 :
    float z;    // 8 :
    float lastx;    // 12 :
    float lasty;    // 16 :
    float lastz;    // 20 :
    float vx;    // 24 :
    float vy;    // 28 :
    float vz;    // 32 :
    float length;    // 36 :
    float width;    // 40 :
    void* image;    // 44 :  doesn't draw if NULL
    Uint8 red;    // 48 :  for tinting particle to a desired color...  defaults to white...
    Uint8 green;    // 49 :
    Uint8 blue;    // 50 :
    Uint8 number;    // 51 :  for PART_NUMBER_ON
    Uint16 timer;    // 52 :   Refresh timer
    Uint16 evtimer;    // 54 :   Event timer
    Uint16 spin;    // 56 :
    Uint16 vspin;    // 58 :
    Uint16 flags;    // 60 :
    Uint16 target;    // 62 :   Stores index...  No target.x stuff...
    Uint8 bone;    // 0x40 :   Part uses for stuck parts...
    Uint8 trail;    // 65 :  0 means lastxyz is actual last position     1-0xFF means lastxyz is sluggified
    Uint8 dtype;    // 66 :  Damage type in low 4 bits     Top 4 bits have special flags...
    Uint8 event;    // 67 :
    PART_MISC misc; // 68 - 75
    Uint16 owner;    // 76 :
    Uint8 team;    // 78 :
    Uint8 alpha;    // 79 :
    float colsize;    // 80 :  used for bumping the floor...  Diameter of sphere for particle...  Doesn't work as well as it should...
    Uint8 is_spawning;   // 84 : is used internally as the particle's spawn'd in water detection thinger...
    Uint8 unused_85;   // 85 : is unused
    Uint16 stuckto;    // 86 :  used when particle is stuck to a character...

    //---- EXTENDED DATA ----

    bool_t              on;
    bool_t              delete_me;
    char                script_name[8];
    SoulfuScriptContext script_ctxt;

} SET_PACKED();
//typedef struct prt_data_t PRT_DATA;

#pragma pack(pop)


//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// The extended data (packed using the "natural" packing for the compiler)

//struct chr_data_ex_t
//{
//    bool_t on;
//    Uint8* script_start;
//    char   script_name[8];
//    bool_t reserve_on;
//};
//typedef struct chr_data_ex_t CHR_DATA_EX;

//struct prt_data_ex_t
//{
//    bool_t on;
//    Uint8* script_start;
//    char   script_name[8];
//};
//typedef struct prt_data_ex_t PRT_DATA_EX;

//struct win_data_ex_t
//{
//    bool_t              on;
//    Uint8*              script_start;
//    char                script_name[8];
//    SoulfuScriptContext script_ctxt;
//};
//typedef struct win_data_ex_t WIN_DATA_EX;

//-----------------------------------------------------------------------------------------------
// external variable definitions

#define MAX_CHARACTER_SHIFT     7
#define MAX_CHARACTER         (1<<MAX_CHARACTER_SHIFT) // The maximum number of characters in a single room
#define VALID_CHR_RANGE(XX)   ( (XX)>=0 && (XX)<MAX_CHARACTER )
#define VALID_CHR(XX)         ( VALID_CHR_RANGE(XX) && _main_chr_data[XX].on )

//extern CHR_DATA_EX main_chr_data[MAX_CHARACTER];

//--------------------------------------------

#define MAX_PARTICLE_SHIFT     12
#define MAX_PARTICLE          (1<<MAX_PARTICLE_SHIFT)  // The maximum number of particles in a single room
#define VALID_PRT_RANGE(XX)   ( (XX)>=0 && (XX)<MAX_PARTICLE )
#define VALID_PRT(XX)         ( VALID_PRT_RANGE(XX) && main_prt_data[XX].on )

extern PRT_DATA main_prt_data[MAX_PARTICLE];

extern Uint16 main_unused_particle_count;
//extern PRT_DATA_EX main_prt_data[MAX_PARTICLE];

//--------------------------------------------

#define MAX_WINDOW_SHIFT       4
#define MAX_WINDOW            (1<<MAX_WINDOW_SHIFT)                            // The maximum number of windows a machine can have open
#define VALID_WIN_RANGE(XX)   ( (XX)>=0 && (XX)<MAX_WINDOW )
#define VALID_WIN(XX)         ( VALID_WIN_RANGE(XX) && main_win_data[XX].on )

extern WIN_DATA    main_win_data[MAX_WINDOW];
//extern WIN_DATA_EX main_win_data[MAX_WINDOW];

extern Uint16 main_window_poof_count;

extern Uint16 main_used_window_count;
extern Uint16 main_used_window[MAX_WINDOW];


//-----------------------------------------------------------------------------------------------
// forward declarations of the external functions
struct spawn_info_t;
//typedef struct spawn_info_t SPAWN_INFO;

BASE_DATA * obj_spawn(PSoulfuScriptContext pss, SPAWN_INFO *psp, Uint8 type, float x, float y, float z, Uint8* script_file_start, Uint16 force_index);

void   obj_setup(void);
void   obj_get_script_name(Uint8* file_start, Uint8* file_name);
bool_t obj_destroy(OBJECT_PTR * object_data);
void   obj_poof_all(Uint8 type);
void   obj_recompile_start(void);
void   obj_recompile_end(void);

void win_promote_all();

bool_t chr_delete(CHR_DATA * pchr);
bool_t prt_delete(PRT_DATA * pchr);
bool_t win_delete(WIN_DATA * pchr);



// Bone frame pointers...  Should point into mainbuffer...
extern Uint8* temp_character_bone_frame[MAX_CHARACTER];

// Table that tells us which bone in a model has a given name/id...  Have to either call render_rdy
// or special render_fill_temp_XXXX function...
extern Uint8 temp_character_bone_number[8];

// Frame event flags for last render'd character...
extern Uint8 temp_character_frame_event;

extern Uint16 character_collision_list[MAX_CHARACTER];
extern Uint16 num_character_collision_list;

extern Uint16 character_door_collision_list[MAX_CHARACTER];
extern Uint16 num_character_door_collision_list;

extern Uint16 promotion_buffer[2*MAX_WINDOW];
extern Uint16 promotion_count;

extern WIN_DATA * current_win[MAX_WINDOW];
extern int        current_win_idx;

void          obj_delete();




#define PRT_FLT_OFF_x           0 //
#define PRT_FLT_OFF_y           4 //
#define PRT_FLT_OFF_z           8 //
#define PRT_FLT_OFF_lastx       12 //
#define PRT_FLT_OFF_lasty       16 //
#define PRT_FLT_OFF_lastz       20 //
#define PRT_FLT_OFF_vx          24 //
#define PRT_FLT_OFF_vy          28 //
#define PRT_FLT_OFF_vz          32 //
#define PRT_FLT_OFF_length      36 //
#define PRT_FLT_OFF_width       40 //
#define PRT_PTR_OFF_image       44 //  doesn't draw if NULL
#define PRT_I08_OFF_red         48 //  for tinting particle to a desired color...  defaults to white...
#define PRT_I08_OFF_green       49 //
#define PRT_I08_OFF_blue        50 //
#define PRT_I08_OFF_number      51 //  for PART_NUMBER_ON
#define PRT_I16_OFF_timer       52 //   Refresh timer
#define PRT_I16_OFF_evtimer     54 //   Event timer
#define PRT_I16_OFF_spin        56 //
#define PRT_I16_OFF_vspin       58 //
#define PRT_I16_OFF_flags       60 //
#define PRT_I16_OFF_target      62 //   Stores index...  No target.x stuff...
#define PRT_I08_OFF_bone        64 //   Part uses for stuck parts...
#define PRT_I08_OFF_trail       65 //  0 means lastxyz is actual last position     1-0xFF means lastxyz is sluggified
#define PRT_I08_OFF_dtype       66 //  Damage type in low 4 bits     Top 4 bits have special flags...
#define PRT_I08_OFF_event       67 //
#define PRT_ARY_OFF_misc        68 //
#define PRT_I16_OFF_owner       76 //
#define PRT_I08_OFF_team        78 //
#define PRT_I08_OFF_alpha       79 //
#define PRT_FLT_OFF_colsize     80 //  used for bumping the floor...  Diameter of sphere for particle...  Doesn't work as well as it should...
#define PRT_I08_OFF_is_spawning 84 // is used internally as the particle's spawn'd in water detection thinger...
#define PRT_I08_OFF_unused_85   85 // is unused
#define PRT_I16_OFF_stuckto     86 //  used when particle is stuck to a character...

