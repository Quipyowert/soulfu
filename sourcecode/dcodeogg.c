// <ZZ> This file contains functions to convert OGG files to RAW
//  **  decode_ogg            - The main function to do an OGG conversion

#include "soulfu_config.h"
#include "sdf_archive.h"
#include "logfile.h"

#include <vorbis/codec.h>

ogg_int16_t convbuffer[4096];
int convsize = 4096;

Uint8* bling_sound = NULL;
Uint8* buzzer_sound = NULL;
Uint8* cash_sound = NULL;

//-----------------------------------------------------------------------------------------------
SDF_PHEADER decode_ogg(SDF_PHEADER ogg_header, const char* filename)
{
    // <ZZ> This function decompresses an ogg file that has been stored in memory.  Index is a
    //      pointer to the start of the file's ogg_header in the sdf_archive, and can be gotten from
    //      sdf_archive_find_index_from_filename.  If the function works okay, it should create a new RAW file in the
    //      ogg_header and return ktrue.  It might also delete the original compressed file to save
    //      space, but that's a compile time option.  If it fails it should return kfalse, or
    //      it might decide to crash.

    ogg_int16_t* tempbuffer;   // The mainbuffer
    Uint32 bufferspace;  // Number of bytes written to the mainbuffer
    float rate;

    Uint8* ogg_data;       // Compressed
    Uint32 ogg_size;         // Compressed

    SDF_PHEADER raw_header = NULL;
    Uint8* raw_data; // Decompressed
    Uint32 raw_size;   // Decompressed



    // OGG Garbage
    ogg_sync_state   oy;
    ogg_stream_state os;
    ogg_page         og;
    ogg_packet       op;
    vorbis_info      vi;
    vorbis_comment   vc;
    vorbis_dsp_state vd;
    vorbis_block     vb;
    char *buffer;
    int  bytes;
    int eos;
    int i, j;
    int result;
    float **pcm;
    int samples;
    int clipflag;
    int bout;
    ogg_int16_t* ptr;
    float* mono;
    int val;



    // Log what we're doing

    log_info(1, "Decoding %s.OGG to %s.RAW", filename, filename);



    // Find the location of the file ogg_data, and its ogg_size...
    ogg_data = sdf_file_get_data(ogg_header);
    ogg_size = sdf_file_get_size(ogg_header);


    // Make sure we have room in the ogg_header for a new file
    if (sdf_archive_free_file_count() <= 0)
    {
        log_error(0, "No room left to add RAW file, program must be restarted");
        goto decode_ogg_fail;
    }

    // Decompress headers
    bufferspace = 0;
    tempbuffer = (ogg_int16_t*) (mainbuffer + 4);
    ogg_sync_init(&oy);

    while (1)
    {
        eos = 0;
        buffer = ogg_sync_buffer(&oy, 4096);
        // Replacement for stdin...
        // Replacement for stdin...
        // Replacement for stdin...
        // bytes=fread(buffer,1,4096,stdin);
        bytes = 4096;

        if (ogg_size < 4096) bytes = ogg_size;

        memcpy(buffer, ogg_data, bytes);
        ogg_size -= bytes;
        ogg_data += bytes;
        // Replacement for stdin...
        // Replacement for stdin...
        // Replacement for stdin...
        ogg_sync_wrote(&oy, bytes);

        if (ogg_sync_pageout(&oy, &og) != 1)
        {
            if (bytes < 4096)break;

            log_error(0, "File wasn't really an OGG file");
            goto decode_ogg_fail;
        }

        ogg_stream_init(&os, ogg_page_serialno(&og));
        vorbis_info_init(&vi);
        vorbis_comment_init(&vc);

        if (ogg_stream_pagein(&os, &og) < 0)
        {
            log_error(0, "OGG ogg_data corrupt");
            goto decode_ogg_fail;
        }

        if (ogg_stream_packetout(&os, &op) != 1)
        {
            log_error(0, "OGG ogg_data corrupt");
            goto decode_ogg_fail;
        }

        if (vorbis_synthesis_headerin(&vi, &vc, &op) < 0)
        {
            log_error(0, "No audio ogg_data in OGG file");
            goto decode_ogg_fail;
        }



        i = 0;

        while (i < 2)
        {
            while (i < 2)
            {
                result = ogg_sync_pageout(&oy, &og);

                if (result == 0)break;

                if (result == 1)
                {
                    ogg_stream_pagein(&os, &og);

                    while (i < 2)
                    {
                        result = ogg_stream_packetout(&os, &op);

                        if (result == 0)break;

                        if (result < 0)
                        {
                            log_error(0, "OGG ogg_data is corrupt");
                            goto decode_ogg_fail;
                        }

                        vorbis_synthesis_headerin(&vi, &vc, &op);
                        i++;
                    }
                }
            }

            buffer = ogg_sync_buffer(&oy, 4096);


            // Replacement for stdin...
            // Replacement for stdin...
            // Replacement for stdin...
            // bytes=fread(buffer,1,4096,stdin);
            bytes = 4096;

            if (ogg_size < 4096) bytes = ogg_size;

            memcpy(buffer, ogg_data, bytes);
            ogg_size -= bytes;
            ogg_data += bytes;
            // Replacement for stdin...
            // Replacement for stdin...
            // Replacement for stdin...


            if (bytes == 0 && i < 2)
            {
                log_error(0, "OGG file was missing ogg_data");
                exit(1);
            }

            ogg_sync_wrote(&oy, bytes);
        }


        // Start to decode actual ogg_data here...
        convsize = 4096 / vi.channels;
        vorbis_synthesis_init(&vd, &vi);
        vorbis_block_init(&vd, &vb);

        while (!eos)
        {
            while (!eos)
            {
                result = ogg_sync_pageout(&oy, &og);

                if (result == 0)break;

                if (result < 0)
                {
                    log_error(0, "OGG ogg_data was corrupt");
                }
                else
                {
                    ogg_stream_pagein(&os, &og);

                    while (1)
                    {
                        result = ogg_stream_packetout(&os, &op);

                        if (result == 0)break;

                        if (result < 0)
                        {
                        }
                        else
                        {
                            if (vorbis_synthesis(&vb, &op) == 0)  vorbis_synthesis_blockin(&vd, &vb);

                            while ((samples = vorbis_synthesis_pcmout(&vd, &pcm)) > 0)
                            {
                                clipflag = 0;
                                bout = (samples < convsize ? samples : convsize);

                                for (i = 0; i < vi.channels; i++)
                                {
                                    ptr = convbuffer + i;
                                    mono = pcm[i];

                                    for (j = 0; j < bout; j++)
                                    {
                                        val = (int) (mono[j] * 32767.f);

                                        if (val > 32767) val = 32767;

                                        if (val < -32768) val = -32768;

                                        *ptr = val;
                                        ptr += vi.channels;
                                    }
                                }

                                // Replacement for stdout...
                                // Replacement for stdout...
                                // Replacement for stdout...
                                // Copy the decoded ogg_data into the mainbuffer (44khz -> 22khz)
                                rate = (float) 1.0;

                                if (vi.rate > 0) rate = ((float) 22050) / vi.rate;

                                j = (int) (bout * vi.channels * rate);
                                repeat(i, j)
                                {
                                    // Take samples and stick 'em in the mainbuffer
                                    tempbuffer[bufferspace] = convbuffer[((int) (i/rate))];
                                    bufferspace++;
                                }
                                // Replacement for stdout...
                                // Replacement for stdout...
                                // Replacement for stdout...
                                vorbis_synthesis_read(&vd, bout);
                            }
                        }
                    }

                    if (ogg_page_eos(&og))eos = 1;
                }
            }

            if (!eos)
            {
                buffer = ogg_sync_buffer(&oy, 4096);
                // Replacement for stdin...
                // Replacement for stdin...
                // Replacement for stdin...
                // bytes=fread(buffer,1,4096,stdin);
                bytes = 4096;

                if (ogg_size < 4096) bytes = ogg_size;

                memcpy(buffer, ogg_data, bytes);
                ogg_size -= bytes;
                ogg_data += bytes;
                // Replacement for stdin...
                // Replacement for stdin...
                // Replacement for stdin...
                ogg_sync_wrote(&oy, bytes);

                if (bytes == 0)eos = 1;
            }
        }

        ogg_stream_clear(&os);
        vorbis_block_clear(&vb);
        vorbis_dsp_clear(&vd);
        vorbis_comment_clear(&vc);
        vorbis_info_clear(&vi);
    }

    ogg_sync_clear(&oy);



    // Allocate memory for the new file...
    raw_size = bufferspace << 1;

    raw_header = sdf_archive_create_header(mainbuffer, filename, raw_size + 4, SDF_FILE_IS_RAW);

    if (NULL == raw_header)
    {
        log_error(0, "Couldn't create RAW file");
        goto decode_ogg_fail;
    }

    raw_data = sdf_file_get_data(raw_header);

    // Copy the ogg_data from the mainbuffer to the newly created file
    DEREF( Uint32, raw_data ) = (raw_size >> 1);

    // Decide if we should get rid of the compressed file or not...
#ifndef KEEP_COMPRESSED_FILES
    sdf_archive_delete_header(ogg_header);
    sdf_archive_set_save(kfalse);
#endif

    return raw_header;

decode_ogg_fail:

    if (NULL != raw_header)
    {
        sdf_archive_delete_header(raw_header);
    }

    return NULL;

}

//-----------------------------------------------------------------------------------------------
