// <ZZ> This file has all the stuff for displaying the screen...  3D card does everything...
//      These are the more general purpose functions, wrappers, and 2D windowing stuff...
//      Render.c has the 3D model rendering stuff...
//  x   display_clear_zbuffer       - Clears the zbuffer
//  x   display_clear_buffers       - Clears the zbuffer and the display
//  x   display_swap                - Swaps the display onto the screen after we've rendered it
//  x   display_viewport            - Tells us where to render on the screen
//  x   display_zbuffer_on          - Turns on zbuffering
//  x   display_zbuffer_off         - Turns off zbuffering
//  x   display_cull_on             - Turns on backface culling
//  x   display_cull_off            - Turns off backface culling
//  x   display_shade_on            - Turns on smooth shading
//  x   display_shade_off           - Turns off smooth shading
//      display_blend_trans         - Turns on transparency style average blending
//      display_blend_light         - Turns on light style additive blending
//      display_blend_off           - Turns off blending
//      display_depth_scene         - Sets up depth buffering range for the main 3D stuff
//      display_depth_overlay       - Sets up depth buffering range for windows
//  x   display_texture_on          - Turns on texturing
//  x   display_texture_off         - Turns off texturing
//  x   display_pick_texture        - Picks the texture to use
//  x   display_start_strip         - Starts a triangle strip
//  x   display_start_fan           - Starts a triangle fan
//  x   display_color               - Sets the color for the next vertex
//      display_texpos              - Sets the texture position for the next vertex
//  x   display_vertex              - Adds a vertex
//  x   display_end                 - Ends a triangle strip or fan
//      display_slider              - Draws a horizontal or vertical slider bar...
//      display_highlight           - Draws a white rectangle for selection lists...
//      display_window
//      display_image               - Draws a 2D textured image
//      display_font
//      display_kanji
//      display_string
//      display_mini_list
//      display_input
//      display_emacs
//  x   blurry_alpha_helper         - Helper for display_load_texture
//  x   display_load_texture        - Loads an RGB file onto the graphics card as a texture
//  x   display_load_all_textures   - Loads all of the textures onto the card
//  x   display_setup               - Sets up all of the mandatory silly stuff for SDL/GL/whatever else
//      display_render              - The main function for rendering a scene
//      display_start_fade          - Starts to fade out the screen (circle effect)
//      display_fade                - Draws the fade effect
//      display_look_at             - Sets up the camera matrix...

#include "display.h"
#include "input.h"

#include "soulfu_endian.inl"
#include "soulfu_math.inl"

Uint32 last_ticks = NULL;

float circle_xyz[CIRCLE_TOTAL_POINTS][3];  // Points for the circle fade
float fade_x;                           // Center of the circle
float fade_y;                           // Center of the circle
Uint8 fade_type;                //
Sint16 fade_time = 0;             // 0 is normal, 255 is fully faded
Sint8 fade_direction;             // In or Out
Uint8 fade_color[4] = {255, 255, 255, 80};
Uint8 display_full_screen = ktrue;

Sint16 screen_shake_timer = 0;
float screen_shake_amount = 0.0f;
float screen_frustum_x;
float screen_frustum_y;

Uint8 line_mode = 0;            // Global control for cartoon lines
float initial_camera_matrix[16];        // A matrix to speed up/simplify 3D drawing routines
float rotate_camera_matrix[16];         // A matrix to speed up/simplify 3D drawing routines
float window_camera_matrix[16];         // A matrix to setup window drawing routines
float onscreen_matrix[16];              // A matrix for figurin' out onscreen point locations...
float modelview_matrix[16];             // A matrix for figurin' out onscreen point locations...
float rotate_enviro_matrix[6];          // Enviromap mini matrix...

CAMERA camera;

float map_side_xy[2];                   // For doin' the view triangle on the automap...
Uint8 temp_texture_data[512*512*4];  // Temporary memory for loading textures onto the card


// Important textures...
GLuint texture_ascii   = INVALID_TEXTURE;
void*  texture_ascii_ptr = NULL;     // Used for drawing joint number particles in modeler...
GLuint texture_bookfnt = INVALID_TEXTURE;
GLuint texture_paper[2] = { INVALID_TEXTURE, INVALID_TEXTURE };
GLuint texture_winalt  = INVALID_TEXTURE;
GLuint texture_winside = INVALID_TEXTURE;
GLuint texture_wincorn = INVALID_TEXTURE;
GLuint texture_wintrim = INVALID_TEXTURE;
GLuint texture_winmini = INVALID_TEXTURE;
GLuint texture_winminy = INVALID_TEXTURE;
GLuint texture_winslid = INVALID_TEXTURE;
GLuint texture_wininput = INVALID_TEXTURE;
GLuint texture_petrify = INVALID_TEXTURE;
GLuint texture_armor   = INVALID_TEXTURE;
GLuint texture_armorst = INVALID_TEXTURE;
GLuint texture_automap_stair = INVALID_TEXTURE;
GLuint texture_automap_town  = INVALID_TEXTURE;
GLuint texture_automap_boss  = INVALID_TEXTURE;
GLuint texture_automap_virtue  = INVALID_TEXTURE;

// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
GLuint texture_bad_mpsamp00 = INVALID_TEXTURE;
GLuint texture_bad_mpsamp01 = INVALID_TEXTURE;
GLuint texture_bad_mpfence = INVALID_TEXTURE;
GLuint texture_bad_mppave = INVALID_TEXTURE;
GLuint texture_bad_mpdecal = INVALID_TEXTURE;
GLuint texture_bad_mphsbt00 = INVALID_TEXTURE;
GLuint texture_bad_mphsmd00 = INVALID_TEXTURE;
GLuint texture_bad_mphstp00 = INVALID_TEXTURE;
GLuint texture_bad_mpflor00 = INVALID_TEXTURE;
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!



Uint32 max_texture_size = 256;


GLfloat  fog_color[4] = {1.0f, 1.0f, 1.0f, 1.0f};
GLdouble clip_equation[4] = { 0.0, 0.0, 1.0, 0.0};
float global_depth_min = 0.066f;

static void camera_setup(CAMERA * pc);

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void display_blur_image(Uint8* data, Uint16 x_size, Uint16 y_size)
{
    // <ZZ> This function blurs an RGB (3 byte) image...
    Uint16 x, y;
    Uint8 last_red, last_green, last_blue;
    Uint8 red, green, blue;

    // Blur horizontally...
    repeat(y, y_size)
    {
        last_red = *(data + (((y * x_size) + x_size - 1) * 3));
        last_green = *(data + (((y * x_size) + x_size - 1) * 3) + 1);
        last_blue = *(data + (((y * x_size) + x_size - 1) * 3) + 2);
        repeat(x, x_size)
        {
            red = *(data + (((y * x_size) + x) * 3));
            green = *(data + (((y * x_size) + x) * 3) + 1);
            blue = *(data + (((y * x_size) + x) * 3) + 2);
            last_red = (last_red >> 1) + (red >> 1);
            last_green = (last_green >> 1) + (green >> 1);
            last_blue = (last_blue >> 1) + (blue >> 1);
            *(data + (((y*x_size) + x)*3))   = last_red;
            *(data + (((y*x_size) + x)*3) + 1) = last_green;
            *(data + (((y*x_size) + x)*3) + 2) = last_blue;
            last_red = red;
            last_green = green;
            last_blue = blue;
        }
    }



    // Blur vertically...
    repeat(x, x_size)
    {
        last_red = *(data + ((((y_size - 1) * x_size) + x) * 3));
        last_green = *(data + ((((y_size - 1) * x_size) + x) * 3) + 1);
        last_blue = *(data + ((((y_size - 1) * x_size) + x) * 3) + 2);
        repeat(y, y_size)
        {
            red = *(data + (((y * x_size) + x) * 3));
            green = *(data + (((y * x_size) + x) * 3) + 1);
            blue = *(data + (((y * x_size) + x) * 3) + 2);
            last_red = (last_red >> 1) + (red >> 1);
            last_green = (last_green >> 1) + (green >> 1);
            last_blue = (last_blue >> 1) + (blue >> 1);
            *(data + (((y*x_size) + x)*3))   = last_red;
            *(data + (((y*x_size) + x)*3) + 1) = last_green;
            *(data + (((y*x_size) + x)*3) + 2) = last_blue;
            last_red = red;
            last_green = green;
            last_blue = blue;
        }
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void display_export_tga(char* filename, Uint8* data_start, Uint16 x_size, Uint16 y_size, Sint8 bytes_per_pixel)
{
    // <ZZ> This function saves a TGA file...
    FILE* openfile;
    Uint16 x, y;
    Uint8 value;
    Uint8* data;


    openfile = fopen(filename, "wb");

    if (openfile == NULL) return;


    // Write the TGA header...
    putc(0, openfile);
    putc(0, openfile);
    putc(2, openfile);
    putc(0, openfile); putc(0, openfile);
    putc(0, openfile); putc(0, openfile);
    putc(0, openfile);
    putc(0, openfile); putc(0, openfile);
    putc(0, openfile); putc(0, openfile);
    putc((x_size & 0x00FF), openfile);
    putc((x_size & 0xFF00) >> 8, openfile);
    putc((y_size & 0x00FF), openfile);
    putc((y_size & 0xFF00) >> 8, openfile);
    putc(24, openfile);
    putc(0, openfile);


    // Write the file
    if (bytes_per_pixel == 1)
    {
        // 8-bit monochrome data
        repeat(y, y_size)
        {
            data = data_start + ((y_size - y - 1) * x_size);
            repeat(x, x_size)
            {
                putc(*data, openfile);
                putc(*data, openfile);
                putc(*data, openfile);  data++;
            }
        }
    }

    if (bytes_per_pixel == 2)
    {
        // 16-bit monochrome data (system endian unsigned shorts)
        repeat(y, y_size)
        {
            data = data_start + ((y_size - y - 1) * x_size * 2);
            repeat(x, x_size)
            {
                value = (DEREF( Uint16, data )) >> 8;  data += 2;
                putc(value, openfile);
                putc(value, openfile);
                putc(value, openfile);
            }
        }
    }

    if (bytes_per_pixel == -2)
    {
        // 16-bit monochrome data (system endian signed shorts)
        repeat(y, y_size)
        {
            data = data_start + ((y_size - y - 1) * x_size * 2);
            repeat(x, x_size)
            {
                value = ((DEREF( Sint16, data )) + 32767) >> 8;  data += 2;
                putc(value, openfile);
                putc(value, openfile);
                putc(value, openfile);
            }
        }
    }

    if (bytes_per_pixel == 3)
    {
        // 24-bit RGB data
        repeat(y, y_size)
        {
            data = data_start + ((y_size - y - 1) * x_size * 3);
            repeat(x, x_size)
            {
                putc(data[2], openfile); // Blue
                putc(data[1], openfile); // Green
                putc(data[0], openfile); // Red
                data += 3;
            }
        }
    }

    fclose(openfile);
}
#endif

//-----------------------------------------------------------------------------------------------
void display_get_onscreen_xyd(float* in_xyz, float* out_xyd)
{
    // <ZZ> Test for another way of getting onscreen point locations, rather than using
    //      glFeedback...

    out_xyd[XX] = onscreen_matrix[0] * in_xyz[XX] + onscreen_matrix[4] * in_xyz[YY] + onscreen_matrix[8]  * in_xyz[ZZ] + onscreen_matrix[12];
    out_xyd[YY] = onscreen_matrix[1] * in_xyz[XX] + onscreen_matrix[5] * in_xyz[YY] + onscreen_matrix[9]  * in_xyz[ZZ] + onscreen_matrix[13];
    out_xyd[2] = onscreen_matrix[3] * in_xyz[XX] + onscreen_matrix[7] * in_xyz[YY] + onscreen_matrix[11] * in_xyz[ZZ] + onscreen_matrix[15];

    if (out_xyd[2] != 0.0f)
    {
        out_xyd[XX] /= out_xyd[2]; out_xyd[YY] /= out_xyd[2];
        out_xyd[XX] *= 0.5f * DEFAULT_W;   out_xyd[XX] += 0.5f * DEFAULT_W;
        out_xyd[YY] *= -0.5f * DEFAULT_H;  out_xyd[YY] += 0.5f * DEFAULT_H;
    }
}

//-----------------------------------------------------------------------------------------------
void display_slider(float x, float y, Uint8 w, Uint8 h, float scale, float position)
{
    // <ZZ> This function draws a slider bar at x,y.  W is how long it is (horizontal), or H is
    //      how tall it is (vertical).  Scale of 10.0f is normal.  Position is 0 to 1, and
    //      represents the position of the dinglethorpe
    float scale_w;
    float scale_h;
    float scale_x;
    float scale_y;
    float offset;



    // Error check...
    if (w == 0 || h == 0)  return;

    display_pick_texture(texture_winslid);

    if (h > w)
    {
        // Vertical bar
        offset = .28125f * scale;
        scale_h = scale * h;
        scale_w = .4375f * scale;
        scale_y = .35f * scale;
        display_color(white);

        display_start_fan();
        {
            display_texpos_xy(.78125, 0);   display_vertex_xy(x + offset, y + scale_y);
            display_texpos_xy(1, 0);        display_vertex_xy(x + offset + scale_w, y + scale_y);
            display_texpos_xy(1, h);        display_vertex_xy(x + offset + scale_w, y + scale_h - scale_y);
            display_texpos_xy(.78125, h);   display_vertex_xy(x + offset, y + scale_h - scale_y);
        }
        display_end();


        // Book ends...  Bottom
        offset = .09375f * scale;
        scale_x = .8125f * scale;
        scale_y = .5f * scale;

        display_start_fan();
        {
            display_texpos_xy(.15625f, 0.75f);   display_vertex_xy(x + offset, y + scale_h - scale_y);
            display_texpos_xy(.5625f, 0.75f);    display_vertex_xy(x + offset + scale_x, y + scale_h - scale_y);
            display_texpos_xy(.5625f, 1);        display_vertex_xy(x + offset + scale_x, y + scale_h);
            display_texpos_xy(.15625f, 1);       display_vertex_xy(x + offset, y + scale_h);
        }
        display_end();


        // Book ends...  Top
        display_start_fan();
        {
            display_texpos_xy(.15625f, 1);       display_vertex_xy(x + offset, y);
            display_texpos_xy(.5625f, 1);        display_vertex_xy(x + offset + scale_x, y);
            display_texpos_xy(.5625f, 0.75f);    display_vertex_xy(x + offset + scale_x, y + scale_y);
            display_texpos_xy(.15625f, 0.75f);   display_vertex_xy(x + offset, y + scale_y);
        }
        display_end();


        // Dinglethorpe center position...
        x = x + scale_y;
        y = y + (position * (scale_h - scale)) + scale_y;
    }
    else
    {
        // Horizontal bar
        offset = .28125f * scale;
        scale_h = .4375f * scale;
        scale_w = scale * w;
        scale_x = .35f * scale;
        display_color(white);

        display_start_fan();
        {
            display_texpos_xy(.78125, 0);   display_vertex_xy(x + scale_x, y + scale_h + offset);
            display_texpos_xy(1, 0);        display_vertex_xy(x + scale_x, y + offset);
            display_texpos_xy(1, w);        display_vertex_xy(x + scale_w - scale_x, y + offset);
            display_texpos_xy(.78125, w);   display_vertex_xy(x + scale_w - scale_x, y + scale_h + offset);
        }
        display_end();


        // Book ends...  Right
        offset = .09375f * scale;
        scale_x = .5f * scale;
        scale_y = .8125f * scale;

        display_start_fan();
        {
            display_texpos_xy(.15625f, 0.75f);       display_vertex_xy(x - scale_x + scale_w, y + offset + scale_y);
            display_texpos_xy(.5625f, 0.75f);        display_vertex_xy(x - scale_x + scale_w, y + offset);
            display_texpos_xy(.5625f, 1);            display_vertex_xy(x + scale_w, y + offset);
            display_texpos_xy(.15625f, 1);           display_vertex_xy(x + scale_w, y + offset + scale_y);
        }
        display_end();


        // Book ends...  Left
        display_start_fan();
        {
            display_texpos_xy(.15625f, 1);           display_vertex_xy(x, y + offset + scale_y);
            display_texpos_xy(.5625f, 1);            display_vertex_xy(x, y + offset);
            display_texpos_xy(.5625f, 0.75f);        display_vertex_xy(x + scale_x, y + offset);
            display_texpos_xy(.15625f, 0.75f);       display_vertex_xy(x + scale_x, y + offset + scale_y);
        }
        display_end();



        // Dinglethorpe center position...
        y = y + scale_x;
        x = x + (position * (scale_w - scale)) + scale_x;
    }




    // Draw the dinglethorpe...  Rotate as it moves
// !!!BAD!!!
// !!!BAD!!!  Do sin/cos look up
// !!!BAD!!!
    scale_x = COS(position * FOUR_PI) * scale;
    scale_y = SIN(position * FOUR_PI) * scale;

    display_start_fan();
    {
        display_texpos_xy(.03125f, .03125f);     display_vertex_xy(x - scale_x, y - scale_y);
        display_texpos_xy(.75f, .03125f);        display_vertex_xy(x + scale_y, y - scale_x);
        display_texpos_xy(.75f, .71875f);        display_vertex_xy(x + scale_x, y + scale_y);
        display_texpos_xy(.03125f, .71875f);     display_vertex_xy(x - scale_y, y + scale_x);
    }
    display_end();


}



//-----------------------------------------------------------------------------------------------
void display_window(SOULFU_WINDOW * pwin, Uint16 side_mask)
{
    // <ZZ> This function draws a standard-style window.  The top left of the font area is given
    //      in pwin->x,pwin->y.  W is the number of fonts that can fit across.  H is the number of fonts that
    //      can fit downwards.  Scale of 10.0 is normal size.  Side_mask tells us which of the 4
    //      sides and 4 corners of the border to draw.  Also tells us which of the 4 fancy
    //      corner trims to draw, and also whether or not to draw the background.  Alpha blend
    //      should be turned on.  Shading should be off (flat).  Perspective and zbuffering
    //      should be off.  W and H should both be positive whole numbers...  Texturing should be
    //      on too.
    float scale_w;
    float scale_h;
    float half;
    float temp_scale;


    // Do some calculations...
    scale_w = pwin->scale * pwin->w;
    scale_h = pwin->scale * pwin->h;
    half = pwin->scale * .5f;


    // First draw the background...
    if (side_mask & 240)
    {
        display_texture_off();
        display_color_alpha(brown);
        display_start_fan();
        {
            display_vertex_xy(pwin->x - half, pwin->y - half);
            display_vertex_xy(pwin->x + scale_w + half, pwin->y - half);
            display_vertex_xy(pwin->x + scale_w + half, pwin->y + scale_h + half);
            display_vertex_xy(pwin->x - half, pwin->y + scale_h + half);
        }
        display_end();
        display_texture_on();
    }

    display_color(white);


    // Now draw the sides and corners...
    if (side_mask & 61440)
    {
        display_pick_texture(texture_winside);
    }


    // Top side...
    if (side_mask & 32768)
    {
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x, pwin->y - pwin->scale);
            display_texpos_xy(pwin->w, 0);  display_vertex_xy(pwin->x + scale_w, pwin->y - pwin->scale);
            display_texpos_xy(pwin->w, 1);  display_vertex_xy(pwin->x + scale_w, pwin->y);
            display_texpos_xy(0, 1);  display_vertex_xy(pwin->x, pwin->y);
        }
        display_end();
    }


    // Right side
    if (side_mask & 16384)
    {
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x + scale_w + pwin->scale, pwin->y);
            display_texpos_xy(pwin->h, 0);  display_vertex_xy(pwin->x + scale_w + pwin->scale, pwin->y + scale_h);
            display_texpos_xy(pwin->h, 1);  display_vertex_xy(pwin->x + scale_w, pwin->y + scale_h);
            display_texpos_xy(0, 1);  display_vertex_xy(pwin->x + scale_w, pwin->y);
        }
        display_end();
    }


    // Bottom side
    if (side_mask & 8192)
    {
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x + scale_w, pwin->y + scale_h + pwin->scale);
            display_texpos_xy(pwin->w, 0);  display_vertex_xy(pwin->x, pwin->y + scale_h + pwin->scale);
            display_texpos_xy(pwin->w, 1);  display_vertex_xy(pwin->x, pwin->y + scale_h);
            display_texpos_xy(0, 1);  display_vertex_xy(pwin->x + scale_w, pwin->y + scale_h);
        }
        display_end();
    }


    // Left side
    if (side_mask & 4096)
    {
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x - pwin->scale, pwin->y + scale_h);
            display_texpos_xy(pwin->h, 0);  display_vertex_xy(pwin->x - pwin->scale, pwin->y);
            display_texpos_xy(pwin->h, 1);  display_vertex_xy(pwin->x, pwin->y);
            display_texpos_xy(0, 1);  display_vertex_xy(pwin->x, pwin->y + scale_h);
        }
        display_end();
    }


    // Now draw the corners...
    if (side_mask & 3840)
    {
        display_pick_texture(texture_wincorn);
    }


    // Top left corner
    if (side_mask & 2048)
    {
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x - pwin->scale, pwin->y - pwin->scale);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x, pwin->y - pwin->scale);
            display_texpos_xy(1, 1);  display_vertex_xy(pwin->x, pwin->y);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x - pwin->scale, pwin->y);
        }
        display_end();
    }



    // Top right corner
    if (side_mask & 1024)
    {
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x + scale_w + pwin->scale, pwin->y - pwin->scale);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x + scale_w + pwin->scale, pwin->y);
            display_texpos_xy(1, 1);  display_vertex_xy(pwin->x + scale_w, pwin->y);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x + scale_w, pwin->y - pwin->scale);
        }
        display_end();
    }


    // Bottom right corner
    if (side_mask & 512)
    {
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x + scale_w + pwin->scale, pwin->y + scale_h + pwin->scale);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x + scale_w, pwin->y + scale_h + pwin->scale);
            display_texpos_xy(1, 1);  display_vertex_xy(pwin->x + scale_w, pwin->y + scale_h);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x + scale_w + pwin->scale, pwin->y + scale_h);
        }
        display_end();
    }


    // Bottom left corner
    if (side_mask & 256)
    {
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x - pwin->scale, pwin->y + scale_h + pwin->scale);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x - pwin->scale, pwin->y + scale_h);
            display_texpos_xy(1, 1);  display_vertex_xy(pwin->x, pwin->y + scale_h);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x, pwin->y + scale_h + pwin->scale);
        }
        display_end();
    }



    // Now draw the trim...
    temp_scale = 1;

    if (side_mask & 15)
    {
        display_pick_texture(texture_wintrim);
        temp_scale = pwin->scale * .115f;
    }


    // Top left corner
    if (side_mask & 8)
    {
        pwin->x -= temp_scale;
        pwin->y -= temp_scale;
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x - half,  pwin->y - half);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x + half,  pwin->y - half);
            display_texpos_xy(1, 1);  display_vertex_xy(pwin->x + half,  pwin->y + half);
            display_texpos_xy(0, 1);  display_vertex_xy(pwin->x - half,  pwin->y + half);
        }
        display_end();
        pwin->x += temp_scale;
        pwin->y += temp_scale;
    }


    // Top right corner
    if (side_mask & 4)
    {
        pwin->x += temp_scale;
        pwin->y -= temp_scale;
        pwin->x += scale_w;
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x + half,  pwin->y - half);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x + half,  pwin->y + half);
            display_texpos_xy(1, 1);  display_vertex_xy(pwin->x - half,  pwin->y + half);
            display_texpos_xy(0, 1);  display_vertex_xy(pwin->x - half,  pwin->y - half);
        }
        display_end();
        pwin->x -= scale_w;
        pwin->x -= temp_scale;
        pwin->y += temp_scale;
    }


    // Bottom right corner
    if (side_mask & 2)
    {
        pwin->x += temp_scale;
        pwin->y += temp_scale;
        pwin->y += scale_h;
        pwin->x += scale_w;
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x + half,  pwin->y + half);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x - half,  pwin->y + half);
            display_texpos_xy(1, 1);  display_vertex_xy(pwin->x - half,  pwin->y - half);
            display_texpos_xy(0, 1);  display_vertex_xy(pwin->x + half,  pwin->y - half);
        }
        display_end();
        pwin->x -= scale_w;
        pwin->y -= scale_h;
        pwin->x -= temp_scale;
        pwin->y -= temp_scale;
    }


    // Bottom left corner
    if (side_mask & 1)
    {
        pwin->x -= temp_scale;
        pwin->y += temp_scale;
        pwin->y += scale_h;
        display_start_fan();
        {
            display_texpos_xy(0, 0);  display_vertex_xy(pwin->x - half,  pwin->y + half);
            display_texpos_xy(1, 0);  display_vertex_xy(pwin->x - half,  pwin->y - half);
            display_texpos_xy(1, 1);  display_vertex_xy(pwin->x + half,  pwin->y - half);
            display_texpos_xy(0, 1);  display_vertex_xy(pwin->x + half,  pwin->y + half);
        }
        display_end();
        pwin->y -= scale_h;
        pwin->x += temp_scale;
        pwin->y -= temp_scale;
    }
}

//-----------------------------------------------------------------------------------------------
void display_mouse_text_box(float win_scale, float tlx, float tly, float brx, float bry, Uint32 image_number)
{
    // <ZZ> Draws a little box for the mouse text with nice little edges...
    float xlx, xrx;

    xlx = tlx + (win_scale * 0.5f);
    xrx = brx - (win_scale * 0.5f);

    display_pick_texture(image_number);

    display_start_fan();
    {
        display_texpos_xy(0.000f, 0.00f);  display_vertex_xy(tlx, tly);
        display_texpos_xy(0.025f, 0.00f);  display_vertex_xy(xlx, tly);
        display_texpos_xy(0.025f, 1.00f);  display_vertex_xy(xlx, bry);
        display_texpos_xy(0.000f, 1.00f);  display_vertex_xy(tlx, bry);
    }
    display_end();

    display_start_fan();
    {
        display_texpos_xy(0.025f, 0.00f);  display_vertex_xy(xlx, tly);
        display_texpos_xy(0.975f, 0.00f);  display_vertex_xy(xrx, tly);
        display_texpos_xy(0.975f, 1.00f);  display_vertex_xy(xrx, bry);
        display_texpos_xy(0.025f, 1.00f);  display_vertex_xy(xlx, bry);
    }
    display_end();

    display_start_fan();
    {
        display_texpos_xy(0.975f, 0.00f);  display_vertex_xy(xrx, tly);
        display_texpos_xy(1.000f, 0.00f);  display_vertex_xy(brx, tly);
        display_texpos_xy(1.000f, 1.00f);  display_vertex_xy(brx, bry);
        display_texpos_xy(0.975f, 1.00f);  display_vertex_xy(xrx, bry);
    }
    display_end();
}


//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void display_marker(Uint8* color, float x, float y, float z, float scale)
{
    display_color(color);
    display_start_line();
    {
        display_vertex_xyz(x - scale, y, z);
        display_vertex_xyz(x + scale, y, z);

        display_vertex_xyz(x, y - scale, z);
        display_vertex_xyz(x, y + scale, z);

        display_vertex_xyz(x, y, z - scale);
        display_vertex_xyz(x, y, z + scale);
    }
    display_end();
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void display_crosshairs(Uint8* color, float x, float y, float scale)
{
    display_color(color);
    display_start_line();
    {
        display_vertex_xyz(-scale, y, 0.0f);
        display_vertex_xyz(scale, y, 0.0f);

        display_vertex_xyz(x, -scale, 0.0f);
        display_vertex_xyz(x, scale, 0.0f);
    }
    display_end();
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void display_riser_marker(Uint8* color, float x, float y, float z, float scale)
{
    display_color(color);
    display_start_line();
    {
        display_vertex_xyz(x - scale, y, z);
        display_vertex_xyz(x + scale, y, z);

        display_vertex_xyz(x, y - scale, z);
        display_vertex_xyz(x, y + scale, z);

        display_vertex_xyz(x, y, 0.0f);
        display_vertex_xyz(x, y, z);
    }
    display_end();
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void display_2d_marker(Uint8* color, float x, float y, float scale)
{
    display_color(color);
    display_start_line();
    {
        display_vertex_xyz(x - scale, y, 0);
        display_vertex_xyz(x + scale, y, 0);

        display_vertex_xyz(x, y - scale, 0);
        display_vertex_xyz(x, y + scale, 0);
    }
    display_end();
}
#endif

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void display_solid_marker(Uint8* color, float x, float y, float z, float scale)
{
    display_color(color);
    display_start_fan();
    {
        display_vertex_xyz(x, y, z - scale);
        display_vertex_xyz(x, y - scale, z);
        display_vertex_xyz(x + scale, y, z);
        display_vertex_xyz(x, y + scale, z);
        display_vertex_xyz(x - scale, y, z);
        display_vertex_xyz(x, y - scale, z);
    }
    display_end();

    display_start_fan();
    {
        display_vertex_xyz(x, y, z + scale);
        display_vertex_xyz(x, y - scale, z);
        display_vertex_xyz(x - scale, y, z);
        display_vertex_xyz(x, y + scale, z);
        display_vertex_xyz(x + scale, y, z);
        display_vertex_xyz(x, y - scale, z);
    }
    display_end();
}
#endif

//-----------------------------------------------------------------------------------------------
void display_font(Uint8 ascii, float x, float y, float scale)
{
    // <ZZ> This function draws the specified ascii character at x,y on the screen.  Screen size
    //      is assumed to be DEFAULT_W x DEFAULT_H.  Note that the proper texture must be picked before calling
    //      this function, and the vertex color should be set to white (unless tinting is
    //      desired).  Perspective should also be turned off prior to calling, and zbuffering
    //      should be turned off too.  Alpha blending should be turned on.  Shading should be
    //      turned off (we want 'em flat shaded).  Scale of 10 is normal size.  Texturing should
    //      be on too.
    float tx_min, tx_max, ty_min, ty_max;
    float x_min, x_max, y_min, y_max;

    tx_min = ((ascii >> 0) & 0x0F) * INV_0x10;
    tx_max = tx_min + INV_0x10;

    ty_min = ((ascii >> 4) & 0x0F) * INV_0x10;
    ty_max = ty_min + INV_0x10;

    x_min = x;
    x_max = x_min + scale;
    y_min = y;
    y_max = y_min + scale;

    display_start_fan();
    {
        display_texpos_xy(tx_min, ty_min);  display_vertex_xy(x_min, y_min);
        display_texpos_xy(tx_max, ty_min);  display_vertex_xy(x_max, y_min);
        display_texpos_xy(tx_max, ty_max);  display_vertex_xy(x_max, y_max);
        display_texpos_xy(tx_min, ty_max);  display_vertex_xy(x_min, y_max);
    }
    display_end();

// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!  Should probably inline this in display_string...
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!

}

//-----------------------------------------------------------------------------------------------
void display_kanji(Uint16 kanji, float x, float y, float scale_x, float scale_y)
{
    // <ZZ> This function draws the specified kanji character at x,y on the screen.  Screen size
    //      is assumed to be DEFAULT_W x DEFAULT_H.
    Uint16 num_kanji;
    Uint8* data;
    Uint32 offset;
    Uint16 num_vertex;
    Uint16 num_triangle;
    Uint8* vertex_data;
    Uint16 vertex;
    float vx, vy;
    Uint16 i;


    scale_x *= INV_0xFF;
    scale_y *= INV_0xFF;


    // Do we have the kanji file?
    if (kanji_data)
    {
        // Make sure the desired character is valid...
        num_kanji = *kanji_data;  num_kanji <<= 8;  num_kanji += *(kanji_data + 1);

        if (kanji < num_kanji)
        {
            display_texture_off();
            data = kanji_data + 2 + (kanji << 2);
            offset = *data;  offset <<= 8;  data++;  offset += *data;  offset <<= 8;  data++;  offset += *data;  offset <<= 8;  data++;  offset += *data;
            data = kanji_data + offset;
            num_vertex = *data;  data++;
            vertex_data = data;
            data += num_vertex << 1;
            num_triangle = *data;  data++;


            repeat(i, num_triangle)
            {
                display_start_fan();
                {
                    vertex = *data;  vertex <<= 1;  data++;
                    vx = x + (vertex_data[vertex] * scale_x);  vertex++;  vy = y + (vertex_data[vertex] * scale_y);
                    display_vertex_xy(vx, vy);

                    vertex = *data;  vertex <<= 1;  data++;
                    vx = x + (vertex_data[vertex] * scale_x);  vertex++;  vy = y + (vertex_data[vertex] * scale_y);
                    display_vertex_xy(vx, vy);

                    vertex = *data;  vertex <<= 1;  data++;
                    vx = x + (vertex_data[vertex] * scale_x);  vertex++;  vy = y + (vertex_data[vertex] * scale_y);
                    display_vertex_xy(vx, vy);
                }
                display_end();
            }


            display_texture_on();
        }
    }


// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!  Should probably inline this in display_string...
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!

}

//-----------------------------------------------------------------------------------------------
void display_book_font(Uint8 ascii, float x, float y, float xtr, float ytr, float scale)
{
    // <ZZ> This function works just like display_font(), except this one can skew the fonts...
    float tx, ty;

    tx = ((float) (ascii & 15)) * 0.0625f;
    ty = ((float) (ascii >> 4)) * 0.0625f;
    display_start_fan();
    {
        display_texpos_xy(tx, ty);  display_vertex_xy(x, y);
        display_texpos_xy(tx + .0625f, ty);  display_vertex_xy(xtr, ytr);
        display_texpos_xy(tx + .0625f, ty + .0625f);  display_vertex_xy(xtr, ytr + scale);
        display_texpos_xy(tx, ty + .0625f);  display_vertex_xy(x, y + scale);
    }
    display_end();

// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!  Must inline this!!!  All of the data is setup already, just stick it in whenever
// !!!BAD!!!  everything is working perfect...
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!

}

//-----------------------------------------------------------------------------------------------
void display_book_page(Uint8* page_start, float x_start, float y_start, float* offsets_xy, float scale, int num_columns, int num_rows, Sint8 draw_back_page, Uint8 page_number)
{
    // <ZZ> This function draws the text of a book...  And the text...
    //      Offsets_xy is an array that controls how the page curves...
    Uint8* page;
    int current_row, current_column;
    float vertical_size;
    float x, y, x_right, y_right;
    float y_down, y_right_down;
    float tx, ty;


    // Figure out which texture to use for the paper...  Alternate every page
    page_number = page_number >> 1;  // Paper pages, not printed sides...
    display_color(white);
    display_pick_texture(texture_paper[page_number&1]);


    // Draw the blank paper...
    x = x_start;
    y = y_start - scale;  // Space for page edge...
    vertical_size = (num_rows + 2) * scale;
    y_down = y + vertical_size;
    num_columns += 2;  // 2 additional columns for gutter and edge...
    tx = 0;
    ty = 0.9765625f / num_columns;
    repeat(current_column, num_columns)
    {
        x_right = x + (offsets_xy[(current_column<<1)] * scale);
        y_right = y + (offsets_xy[(current_column<<1)+1] * scale);
        y_right_down = y_right + vertical_size;

        // Draw the paper...
        if (x_right > x)
        {
            if (draw_back_page == kfalse)
            {
                display_start_fan();
                {
                    display_texpos_xy(tx, 0);  display_vertex_xy(x, y);
                    display_texpos_xy(tx + ty, 0);  display_vertex_xy(x_right, y_right);
                    display_texpos_xy(tx + ty, 1);  display_vertex_xy(x_right, y_right_down);
                    display_texpos_xy(tx, 1);  display_vertex_xy(x, y_down);
                }
                display_end();
            }
        }
        else
        {
            if (draw_back_page)
            {
                display_start_fan();
                {
                    display_texpos_xy(tx + ty, 0);  display_vertex_xy(x_right, y_right);
                    display_texpos_xy(tx, 0);  display_vertex_xy(x, y);
                    display_texpos_xy(tx, 1);  display_vertex_xy(x, y_down);
                    display_texpos_xy(tx + ty, 1);  display_vertex_xy(x_right, y_right_down);
                }
                display_end();
            }
        }

        x = x_right;
        y = y_right;
        y_down = y_right_down;
        tx += ty;
    }
    num_columns -= 2;




    // Draw the text on the page...
    if (page_start != NULL)
    {
        display_pick_texture(texture_bookfnt);

        if (draw_back_page)
        {
            page_start = page_start + num_columns - 1;
        }

        x_start += (offsets_xy[0] * scale);  // Skip over the gutter...
        y_start += (offsets_xy[1] * scale);
        x = x_start;
        repeat(current_column, num_columns)
        {
            y = y_start;
            x_right = x + (offsets_xy[(current_column<<1)+2] * scale);
            y_right = y + (offsets_xy[(current_column<<1)+3] * scale);
            y_start = y_right;

            if (x_right > x)
            {
                if (draw_back_page == kfalse)
                {
                    // Normal page text...
                    page = page_start + current_column;
                    // Walk down the page vertically writing characters...
                    repeat(current_row, num_rows)
                    {
                        y_down = y + scale;
                        y_right_down = y_right + scale;

                        if (*page != ' ')
                        {
                            display_book_font(*page, x, y, x_right, y_right, scale);
                        }

                        y = y_down;
                        y_right = y_right_down;
                        page += num_columns;
                    }
                }
            }
            else
            {
                if (draw_back_page)
                {
                    // Text for next page...
                    page = page_start - current_column;
                    // Walk down the page vertically writing characters...
                    repeat(current_row, num_rows)
                    {
                        y_down = y + scale;
                        y_right_down = y_right + scale;

                        if (*page != ' ')
                        {
                            display_book_font(*page, x_right, y_right, x, y, scale);
                        }

                        y = y_down;
                        y_right = y_right_down;
                        page += num_columns;
                    }
                }
            }

            x = x_right;
        }
    }
}

//-----------------------------------------------------------------------------------------------
void display_string(char* ascii_string, float x, float y, float scale)
{
    // <ZZ> This function displays a string onscreen at the given location.  All of the notes for
    //      display_font apply here too.
    Uint16 i;

    i = 0;

    while (ascii_string[i] != 0)
    {
        if (ascii_string[i] != ' ')  display_font(ascii_string[i], x, y, scale);

        x += scale;
        i++;
    }
}

//-----------------------------------------------------------------------------------------------
void display_mini_list(char* options, float x, float y, Uint8 w, Uint8 h, float scale, Uint16 value)
{
    // <ZZ> This function draws a mini list input box.  Options is a string of different choices
    //      for the user to pick from.  X and y are the top left coordinates.  10.0 scale is
    //      normal.  W is the number of characters that can fit in a box (width).  Expansion
    //      ranges from 0 to 255, with 255 being fully expanded, 0 being unhighlighted, 128 being
    //      highlighted.  H is the number of options.

    float offset;
    float scale_w;
    float scale_h;
    float scale_t;
    float half;
    Uint8 current_choice;
    Uint8 expansion;
    Uint8 alpha_complex_uv[4];
    char *next_option;
    int i;


    // Decode the value argument...
    current_choice = value >> 8;
    expansion = value & 255;


    // Draw the top level choice box...
    display_pick_texture(texture_winmini);
    half = scale * .5f;
    alpha_complex_uv[0] = 255;
    alpha_complex_uv[1] = 255;
    alpha_complex_uv[2] = 255;

    if (expansion > 127)
    {
        // Solid blue box
        offset = 0.5f;
        alpha_complex_uv[3] = 255;
    }
    else
    {
        // Transparent grey box
        offset = 0.0f;
        alpha_complex_uv[3] = expansion + 64;
    }

    scale_w = scale * w;
    display_color_alpha(alpha_complex_uv);
    display_start_fan();
    {
        display_texpos_xy(0, 0 + offset);       display_vertex_xy(x, y);
        display_texpos_xy(1, 0 + offset);       display_vertex_xy(x + scale_w, y);
        display_texpos_xy(1, 0.53125f + offset);  display_vertex_xy(x + scale_w, y + scale);
        display_texpos_xy(0, 0.53125f + offset);  display_vertex_xy(x, y + scale);
    }
    display_end();




    // Now draw the expanded list, if the mini list is clicked...
    if (expansion > 128)
    {
        alpha_complex_uv[0] = 128;
        alpha_complex_uv[1] = 128;
        alpha_complex_uv[2] = 128;
        alpha_complex_uv[3] = 230;
        display_color_alpha(alpha_complex_uv);
        scale_t = ((expansion - 0x80) * h) * INV_0x80;
        scale_h = scale_t * scale;

        // Top segment, with corners...
        display_start_fan();
        {
            display_texpos_xy(0, 0.5f);       display_vertex_xy(x, y + scale);
            display_texpos_xy(1, 0.5f);       display_vertex_xy(x + scale_w, y + scale);
            display_texpos_xy(1, 0.765625f);  display_vertex_xy(x + scale_w, y + half + scale);
            display_texpos_xy(0, 0.765625f);  display_vertex_xy(x, y + half + scale);
        }
        display_end();


        // Bottom segment, with corners...
        display_start_fan();
        {
            display_texpos_xy(0, 0.765625f);  display_vertex_xy(x, y + scale + scale_h + half);
            display_texpos_xy(1, 0.765625f);  display_vertex_xy(x + scale_w, y + scale + scale_h + half);
            display_texpos_xy(1, 1.03125f);   display_vertex_xy(x + scale_w, y + half + scale + scale_h + half);
            display_texpos_xy(0, 1.03125f);   display_vertex_xy(x, y + half + scale + scale_h + half);
        }
        display_end();


        // Mid segment...
        display_pick_texture(texture_winminy);
        display_start_fan();
        {
            display_texpos_xy(0, 0.0);        display_vertex_xy(x, y + half + scale);
            display_texpos_xy(1, 0.0);        display_vertex_xy(x + scale_w, y + half + scale);
            display_texpos_xy(1, scale_t);    display_vertex_xy(x + scale_w, y + scale + scale_h + half);
            display_texpos_xy(0, scale_t);    display_vertex_xy(x, y + scale + scale_h + half);
        }
        display_end();
    }



    // Now draw the text for the list
    display_pick_texture(texture_ascii);
    scale_h = scale;
    display_color(white);
    scale_w = (expansion - 0x80) * INV_0x80;
    scale_t = scale * scale_w;
    scale_w *= half;
    repeat(i, h)
    {
        next_option = strpbrk(options, ",");

        if (next_option) *next_option = 0;

        if (expansion > 128)
        {
            display_string(options, x + half, y + scale_h + scale_w, scale);
            scale_h += scale_t;
        }

        if (i == current_choice)
        {
            display_string(options, x + half, y, scale);
        }

        if (next_option) *next_option = ',';

        options = next_option + 1;
    }
}

//-----------------------------------------------------------------------------------------------
void display_input(char* current_text, float x, float y, Sint8 w, float scale, Sint8 current_position)
{
    // <ZZ> This function draws a text input box.  Current_text is the text that has been input
    //      so far.  X and y are the top left coordinates.  10.0 scale is normal.  W is the
    //      number of characters that can fit in a box (width).  Current_position is the spot where
    //      the next letter goes (-1 for not typing).
    float scale_w;


    // Draw the top level choice box...
    display_pick_texture(texture_wininput);
    display_color_alpha(whiter);
    scale_w = scale * w;
    display_start_fan();
    {
        display_texpos_xy(0, 0);       display_vertex_xy(x, y);
        display_texpos_xy(1, 0);       display_vertex_xy(x + scale_w, y);
        display_texpos_xy(1, 1.0625f);  display_vertex_xy(x + scale_w, y + scale);
        display_texpos_xy(0, 1.0625f);  display_vertex_xy(x, y + scale);
    }
    display_end();


    // Then draw the text string...
    x += scale * .5f;
    display_pick_texture(texture_ascii);
    display_color(white);
    display_string(current_text, x, y, scale);


    // Then draw the cursor...
    if (current_position >= 0 && current_position < w)
    {
        if ((main_video_frame_skipped >> 3)&1)
        {
            display_string("_", x + (scale*current_position), y, scale);
        }
    }
}

//-----------------------------------------------------------------------------------------------
void display_emacs(float x, float y, int w, int h, Uint8 curx, Uint8 cury, Uint8 scrollx, Uint16 scrolly, char* text, float scale)
{
    // <ZZ> This function draws an emacs text editor box, with text and cursor.
    //      Must be in DEVTOOL mode...
    float xr, yb;
    int read;
    int line;
    Uint8 letter;
    int length;

    // Draw the background
    xr = x + (scale * w);
    yb = y + (scale * h);
    display_texture_off();
    display_color_alpha(dark_green);
    display_start_fan();
    {
        display_vertex_xy(x, y);
        display_vertex_xy(xr, y);
        display_vertex_xy(xr, yb);
        display_vertex_xy(x, yb);
    }
    display_end();
    display_texture_on();


    // Skip down several rows depending on the scrolly setting...  Make sure we don't run out of data...
    line = 0;
    read = 0;

    while (line < scrolly && read < TEXT_SIZE)
    {
        if (text[read] == 0)
        {
            line++;
        }

        read++;
    }


    // Make sure file ends with a 0...
    text[TEXT_SIZE-1] = 0;


    // Print out each line until we're done...
    scale = scale * .66666666f;
    w += w >> 1;
    h += h >> 1;
    display_pick_texture(texture_ascii);
    display_color(light_green);
    text += read;
    line = 0;
    yb = y;

    while (line < h && read < TEXT_SIZE)
    {
        length = strlen(text);

        if (length > scrollx)
        {
            text += scrollx;
            length -= scrollx;

            if (length > w)
            {
                // Cropped string
                letter = text[w];
                text[w] = 0;
                display_string(text, x, yb, scale);
                text[w] = letter;
            }
            else
            {
                // Full string
                display_string(text, x, yb, scale);
            }
        }

        length++;
        read += length;
        text += length;
        yb += scale;
        line++;
    }


    // Then draw the cursor...
    if (curx < w && cury < h)
    {
        if ((main_video_frame_skipped >> 3)&1)
        {
            display_string("_", x + (scale*curx), y + (scale*cury), scale);
        }
    }
}

//-----------------------------------------------------------------------------------------------
// <ZZ> This macro helps out with display_load_texture...
#define blurry_alpha_helper(X, Y, AMOUNT)                                   \
    {                                                                           \
        if((x+X) >= 0 && (y+Y) >= 0 && (x+X) < size_x && (y+Y) < size_y)        \
        {                                                                       \
            write = data + (((X) + (size_x*(Y)))<<2);                           \
            if((((int) (*write))-(AMOUNT)) > ALPHA_MIN)  (*write) -= (AMOUNT);    \
        }                                                                       \
    }

//-----------------------------------------------------------------------------------------------
// <ZZ> This macro helps out with display_load_texture...
#define bleed_edge_helper(X, Y)                                             \
    {                                                                           \
        if((x+X) >= 0 && (y+Y) >= 0 && (x+X) < size_x && (y+Y) < size_y)        \
        {                                                                       \
            write = data + (((X) + (size_x*(Y)))<<2);                           \
            if(write[0] > data[0] && write[1] > data[1] && write[2] > data[2])  \
            {                                                                   \
                data[0] = write[0];                                             \
                data[1] = write[1];                                             \
                data[2] = write[2];                                             \
            }                                                                   \
        }                                                                       \
    }

//-----------------------------------------------------------------------------------------------
void display_unload_all_textures(void)
{
    // <ZZ> This function removes all texture data from the video card
    int i;
    Uint8* data;
    Uint8 filetype;


    log_info(0, "Removing all textures from the graphics card...");
    repeat(i, sdf_archive_get_num_files())
    {
        SDF_PHEADER pheader = sdf_archive_get_header(i);
        filetype = sdf_file_get_type(pheader);

        if (filetype == SDF_FILE_IS_RGB)
        {
            data = sdf_file_get_data(pheader);

            if ((*(data + 1)) & TEXTURE_ON_CARD)  glDeleteTextures(1, (Uint32*) (data + 2));
        }
    }
}

//-----------------------------------------------------------------------------------------------
Sint8 display_load_texture(SDF_PHEADER rgb_header)
{
    // <ZZ> This function loads an RGB file onto the graphics card as a texture.  Index is the
    //      SDF rgb_header for the file.  Returns ktrue (good) or kfalse (bad).
    char filename[9];
    Uint8* data;
    Uint8* write;

//    Uint8 hi_byte;
//    Uint8 lo_byte;
    Uint8 red;
    Uint8 green;
    Uint8 blue;
    Uint8 alpha;
    Uint16 level;
    Sint16 variance;
    Sint8 alpha_channel_used;
    Uint8 file_is_shadow;
    Uint8 file_is_light;
    Uint8 file_is_mip;
    int x, y;
    int size_x, size_y;
    int color;
    char * fname;


    // Make sure the file is the correct type...
    if ( sdf_file_get_type(rgb_header) != SDF_FILE_IS_RGB) return kfalse;

    fname = sdf_file_get_name(rgb_header);

    // Make sure the file isn't prefixed with a '+' sign...  Means don't load onto card...
    if (*fname == '+') return kfalse;


    // Make sure the file isn't prefixed with a 'HITE'...  Means it's a special heightmap thing that doesn't get loaded onto card...
    if ((*fname == 'H') && (*(fname + 1) == 'I') && (*(fname + 2) == 'T') && (*(fname + 3) == 'E')) return kfalse;


    // Log what we're doing...
    memcpy(filename, fname, 8);
    filename[8] = 0;

    log_info(1, "Loading %s.RGB as a texture", filename);


    // Start reading the file...
    data = sdf_file_get_data(rgb_header);
    size_x = endian_read_mem_int16(data + 6);
    size_y = endian_read_mem_int16(data + 8);
    data += 10;


    // Don't allow incorrect texture sizes...
    if (size_x != 8 && size_x != 16 && size_x != 32 && size_x != 64 && size_x != 128 && size_x != 256 && size_x != 512)
    {
        log_error(0, "Texture size X was bad (8, 16, 32, 64, 128, 256, 512)...  %dx%d", size_x, size_y);
        return kfalse;
    }

    if (size_y != 8 && size_y != 16 && size_y != 32 && size_y != 64 && size_y != 128 && size_y != 256 && size_y != 512)
    {
        log_error(0, "Texture size Y was bad (8, 16, 32, 64, 128, 256, 512)...  %dx%d", size_x, size_y);
        return kfalse;
    }


    // Start writing the data to a temporary location
    file_is_shadow = (filename[0] == 'S' && filename[1] == 'H' && filename[2] == 'A' && filename[3] == 'D');
    file_is_shadow |= (filename[0] == 'M' && filename[1] == 'P' && filename[2] == 'S' && filename[3] == 'H' && filename[4] == 'A' && filename[5] == 'D');
    file_is_light  = (filename[0] == 'L' && filename[1] == 'I' && filename[2] == 'T' && filename[3] == 'E');
    file_is_light |= (filename[0] == 'M' && filename[1] == 'P' && filename[2] == 'L' && filename[3] == 'I' && filename[4] == 'T' && filename[5] == 'E');
    file_is_mip = kfalse;

    if (mip_map_active)
    {
        // User allows mip mapping...
        file_is_mip  = (filename[0] == 'M' && filename[1] == 'P') || ((filename[0] == '=' || filename[0] == '-') && filename[1] == 'M' && filename[2] == 'P');
    }

    alpha_channel_used = (filename[0] == '-' || filename[0] == '=' || file_is_shadow || file_is_light);
    write = temp_texture_data;
    repeat(y, size_y)
    {
        repeat(x, size_x)
        {
            red = *data;  data++;
            green = *data;  data++;
            blue = *data;  data++;
            *write = red;  write++;
            *write = green;  write++;
            *write = blue;  write++;

            if (alpha_channel_used)
            {
                *(write) = 255;  write++;
            }
        }
    }


    // Go through and set alpha channel to 0 in places where it's color keyed...
    if (alpha_channel_used)
    {
        red = temp_texture_data[0];
        green = temp_texture_data[1];
        blue = temp_texture_data[2];
        data = temp_texture_data;

        // Turn off all texels that are close in value...
        repeat(y, size_y)
        {
            repeat(x, size_x)
            {
                // Copy in the RGB values...
                variance = 0;
                if ((*data) > red) variance += (*data) - red;  else variance += red - (*data);  data++;
                if ((*data) > green) variance += (*data) - green;  else variance += green - (*data);  data++;
                if ((*data) > blue) variance += (*data) - blue;  else variance += blue - (*data);  data++;

                // Write the new Alpha value...
                if (variance < ALPHA_TOLERANCE || file_is_shadow || file_is_light)
                {
                    if (file_is_light)
                    {
                        // Set the alpha equal to the highest channel value......
                        (*data) = (*(data - 1));

                        if ((*(data - 2)) > (*data))  { (*data) = (*(data - 2)); }

                        if ((*(data - 3)) > (*data))  { (*data) = (*(data - 3)); }

                        // Multiply the color, so the highest channel is 255...
                        if ((*data) > 0)
                        {
                            (*(data - 1)) = (*(data - 1)) * 255 / (*data);
                            (*(data - 2)) = (*(data - 2)) * 255 / (*data);
                            (*(data - 3)) = (*(data - 3)) * 255 / (*data);
                        }
                    }
                    else if (file_is_shadow)
                    {
                        // Set the alpha depending on darkness of rgb...
                        color = (*(data - 3)) + (*(data - 2)) + (*(data - 1));
                        color = color / 3;
                        color = 250 - color;

                        if (color < 0) color = 0;

                        (*data) = color;

                        // ...And set the color to black...
                        (*(data - 3)) = 0;
                        (*(data - 2)) = 0;
                        (*(data - 1)) = 0;
                    }
                    else
                    {
                        // Set the color to black...
                        (*(data - 3)) = 0;
                        (*(data - 2)) = 0;
                        (*(data - 1)) = 0;

                        // ...And the alpha to 0...
                        (*data) = 0;

                        // A super alpha file has blurry alpha along the edges...
                        if (filename[0] == '=')
                        {
                            blurry_alpha_helper(-1, -1, ALPHA_BLUR / 2);
                            blurry_alpha_helper( 0, -1, ALPHA_BLUR);
                            blurry_alpha_helper( 1, -1, ALPHA_BLUR / 2);

                            blurry_alpha_helper(-1,  0, ALPHA_BLUR);
                            blurry_alpha_helper( 1,  0, ALPHA_BLUR);

                            blurry_alpha_helper(-1,  1, ALPHA_BLUR / 2);
                            blurry_alpha_helper( 0,  1, ALPHA_BLUR);
                            blurry_alpha_helper( 1,  1, ALPHA_BLUR / 2);
                        }
                    }
                }

                data++;
            }
        }


        // Bleed edges on super alpha files...
        if (filename[0] == '=')
        {
            data = temp_texture_data;

            // Look for blocks that are alpha 0...
            repeat(y, size_y)
            {
                repeat(x, size_x)
                {
                    if (data[3] == 0)
                    {
                        // It's transparent...  Get the color from one of it's neighbors...
                        bleed_edge_helper(-1, 0);
                        bleed_edge_helper(1, 0);
                        bleed_edge_helper(0, -1);
                        bleed_edge_helper(0, 1);
                    }

                    data += 4;
                }
            }
        }
    }


    // Now load the texture onto the graphics card...  We do have a graphics card, right?
    data = sdf_file_get_data(rgb_header);

    if ((*(data + 1)) & TEXTURE_ON_CARD)  glDeleteTextures(1, (Uint32*) (data + 2));

    switch (filename[0])
    {
        case '-':
            // The texture should be alpha blended to cut out color keys...
            endian_write_mem_int16(data, (Uint16) TEXTURE_ALPHA);
            break;
        case '=':
            // The texture should be alpha blended to cut out color keys and to antialias...
            endian_write_mem_int16(data, (Uint16) TEXTURE_SUPER_ALPHA);
            break;
        default:
            // The texture is a normal RGB texture...
            endian_write_mem_int16(data, (Uint16) TEXTURE_NO_ALPHA);
            break;
    }

    glGenTextures(1, (Uint32*) (data + 2));
    glBindTexture(GL_TEXTURE_2D, DEREF( Uint32, data + 2 ));

    /* begin - skyboxing mode - MiR 2008 */
    if (strncmp(filename, "SKYBOX", 6) == 0)
    {
        char skbx[7];
        int numsky;
        int numtex;

        //glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        sscanf(filename, "%6s%1d%1d", skbx, &numsky, &numtex);

        if (numsky >= 0 && numsky<10 && numtex>0 && numtex < 7)
        {
            g_skyTexIDs[numsky][numtex-1] = DEREF( Uint32, data + 2 );
        }
    }
    else
        /* end   - skyboxing mode - MiR 2008 */

        if (fast_and_ugly_active)
        {
            // Fast and Ugly mode...

            file_is_mip = kfalse;
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        }
        else
        {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            if (file_is_mip)
            {
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);  // Best, but looks noisy (graphics card issue?)
                //glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST); // Works well, but has crease line...
            }
            else
            {
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            }
        }


// !!!BAD!!!
// !!!BAD!!!  Leave this code here, but don't use...  It's for the old tile based stuff...
// !!!BAD!!!
//    // Make mip mapped files not wrap...
//    if(file_is_mip)
//    {
//        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
//        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
//    }
//
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!



    if (alpha_channel_used)
    {
        if (file_is_mip)
        {
            level = 0;

            while (size_x >= 1 || size_y >= 1)
            {
                // Add the image to the texture...
                if (((Uint32) size_x) <= max_texture_size && ((Uint32) size_y) <= max_texture_size)
                {
                    glTexImage2D(GL_TEXTURE_2D, level, 4, size_x, size_y, 0, GL_RGBA, GL_UNSIGNED_BYTE, temp_texture_data);
                    log_info(1, "Adding (alpha) mip level %d (%dx%d) to %s.RGB", level, size_x, size_y, filename);
                    level++;
                }

                size_x >>= 1;
                size_y >>= 1;


                // Fix for non-square images...
                if (size_x == 0 || size_y == 0)
                {
                    if (size_x < size_y)
                    {
                        size_x++;
                    }
                    else if (size_y < size_x)
                    {
                        size_y++;
                    }
                }


                // Generate the mip map image...
                repeat(y, size_y)
                {
                    repeat(x, size_x)
                    {
                        // Rescale the image by half in each direction...  4 samples...
                        write = temp_texture_data + (((y * size_x << 2) + (x << 1)) * 4);
                        red = write[0] >> 2;
                        green = write[1] >> 2;
                        blue = write[2] >> 2;
                        alpha = write[3] >> 2;
                        write += 4;
                        red += write[0] >> 2;
                        green += write[1] >> 2;
                        blue += write[2] >> 2;
                        alpha += write[3] >> 2;
                        write += size_x * 8;
                        red += write[0] >> 2;
                        green += write[1] >> 2;
                        blue += write[2] >> 2;
                        alpha += write[3] >> 2;
                        write -= 4;
                        red += write[0] >> 2;
                        green += write[1] >> 2;
                        blue += write[2] >> 2;
                        alpha += write[3] >> 2;


                        // Write the final color...
                        write = temp_texture_data + (((y * size_x) + x) * 4);
                        write[0] = red;
                        write[1] = green;
                        write[2] = blue;
                        write[3] = alpha;
                    }
                }
            }
        }
        else
        {
            // Non-mipmapped texture...
            glTexImage2D(GL_TEXTURE_2D, 0, 4, size_x, size_y, 0, GL_RGBA, GL_UNSIGNED_BYTE, temp_texture_data);
        }
    }
    else
    {
        if (file_is_mip)
        {
            level = 0;

            while (size_x >= 1 || size_y >= 1)
            {
                // Add the image to the texture...
                if (((Uint32) size_x) <= max_texture_size && ((Uint32) size_y) <= max_texture_size)
                {
                    glTexImage2D(GL_TEXTURE_2D, level, 3, size_x, size_y, 0, GL_RGB, GL_UNSIGNED_BYTE, temp_texture_data);
                    log_info(1, "Adding mip level %d (%dx%d) to %s.RGB", level, size_x, size_y, filename);
// !!!BAD!!!
// !!!BAD!!! Mip map scaling test export...
// !!!BAD!!!
//if(0 == strcmp(filename, "MPSAMP01"))
//{
//    sprintf(DEBUG_STRING, "%s-%03dx%03d.TGA", filename, size_x, size_y);
//    display_export_tga(DEBUG_STRING, temp_texture_data, (Uint16) size_x, (Uint16) size_y, 3);
//    DEBUG_STRING[0] = 0;
//}
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
                    level++;
                }

                size_x >>= 1;
                size_y >>= 1;


                // Fix for non-square images...
                if (size_x == 0 || size_y == 0)
                {
                    if (size_x < size_y)
                    {
                        size_x++;
                    }
                    else if (size_y < size_x)
                    {
                        size_y++;
                    }
                }


                // Generate the mip map image...
                repeat(y, size_y)
                {
                    repeat(x, size_x)
                    {
                        // Rescale the image by half in each direction...  4 samples...
                        write = temp_texture_data + (((y * size_x << 2) + (x << 1)) * 3);
                        red = write[0] >> 2;
                        green = write[1] >> 2;
                        blue = write[2] >> 2;
                        write += 3;
                        red += write[0] >> 2;
                        green += write[1] >> 2;
                        blue += write[2] >> 2;
                        write += size_x * 6;
                        red += write[0] >> 2;
                        green += write[1] >> 2;
                        blue += write[2] >> 2;
                        write -= 3;
                        red += write[0] >> 2;
                        green += write[1] >> 2;
                        blue += write[2] >> 2;


                        // Write the final color...
                        write = temp_texture_data + (((y * size_x) + x) * 3);
                        write[0] = red;
                        write[1] = green;
                        write[2] = blue;
                    }
                }
            }
        }
        else
        {
            // Non-mipmapped texture...
            glTexImage2D(GL_TEXTURE_2D, 0, 3, size_x, size_y, 0, GL_RGB, GL_UNSIGNED_BYTE, temp_texture_data);
        }
    }


    *(data + 1) |= TEXTURE_ON_CARD;
    log_info(1, "Loaded as texture number %d", DEREF( Uint32, data + 2 ));



    // Remember important textures that I hardcoded for windows and stuff...
    if (0 == strcmp(filename, "=ASCII"))   texture_ascii    = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=ASCII"))   texture_ascii_ptr = ((void*) data);

    if (0 == strcmp(filename, "=BOOKFNT")) texture_bookfnt  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=PAPER0"))  texture_paper[0] = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=PAPER1"))  texture_paper[1] = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "WINALT"))   texture_winalt   = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=WINSIDE")) texture_winside  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=WINCORN")) texture_wincorn  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=WINTRIM")) texture_wintrim  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "WINMINI"))  texture_winmini  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "WINMINY"))  texture_winminy  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=WINSLID")) texture_winslid  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "WININPUT")) texture_wininput = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "DWHITE"))   texture_petrify  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=ARMOR"))   texture_armor    = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=ARMORST")) texture_armorst  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=AMSTAIR")) texture_automap_stair = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=AMTOWN"))  texture_automap_town  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=AMBOSS"))  texture_automap_boss  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=AMVIRTU")) texture_automap_virtue  = DEREF( Uint32, data + 2 );

// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
    if (0 == strcmp(filename, "MPSAMP00")) texture_bad_mpsamp00 = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "MPSAMP01")) texture_bad_mpsamp01 = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "MPFENCE"))  texture_bad_mpfence  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "MPPAVE"))  texture_bad_mppave  = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "=MPDECAL"))  texture_bad_mpdecal  = DEREF( Uint32, data + 2 );



    if (0 == strcmp(filename, "MPHSBT00")) texture_bad_mphsbt00 = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "MPHSMD00")) texture_bad_mphsmd00 = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "MPHSTP00")) texture_bad_mphstp00 = DEREF( Uint32, data + 2 );

    if (0 == strcmp(filename, "MPFLOR00")) texture_bad_mpflor00 = DEREF( Uint32, data + 2 );

// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!


    return ktrue;
}

//-----------------------------------------------------------------------------------------------
void display_load_all_textures(void)
{
    // <ZZ> This function loads all of the textures in the datafile onto the graphics card.
    int i;


    // Go through each file and try to load it...
    log_info(0, "Trying to load all textures into graphics memory...");
    repeat(i, sdf_archive_get_num_files())
    {
        do_delay();
        display_load_texture( sdf_archive_get_header(i) );
    }
    setup_shadow();
}

//-----------------------------------------------------------------------------------------------
Uint8 display_extension_supported(char* extension_name)
{
    // <ZZ> This function returns ktrue if the given extension is supported...
    Uint8* extensions;
    Uint8* data;
    Uint8 perfect_match;
    Uint16 length;

    extensions = (Uint8*) glGetString(GL_EXTENSIONS);
    data = extensions;
    length = strlen(extension_name);

    while (data)
    {
        data = strstr(data, extension_name);

        if (data)
        {
            // Found a matching substring, but make sure it doesn't have something in front
            // of it, or at its end...
            perfect_match = ktrue;

            if (data != extensions)
            {
                if ( (*(data - 1)) != ' ')
                {
                    // Had somethin' funny in front...  Means we didn't really match it...
                    perfect_match = kfalse;
                }
            }

            if (perfect_match)
            {
                if ( (*(data + length)) == ' ' || (*(data + length)) == EOS)
                {
                    return ktrue;
                }
            }

            data += length;
        }
    }

    return kfalse;
}

//-----------------------------------------------------------------------------------------------
Sint8 display_setup(Uint16 size_x, Uint16 size_y, Uint8 color_depth, Uint8 z_depth, Uint8 full_screen)
{
    // <ZZ> This function initializes the display via SDL and GL.
    int rgb_size[3];
    int value;
    int i;
    Uint32 flags;


    // Let's get started...
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0)
    {
        log_error(0, "Couldn't turn on SDL...  That's bad");
        return kfalse;
    }

    if (volumetric_shadows_on)
    {
        log_info(0, "Trying to turn on the display with volumetric shadows");
    }
    else
    {
        log_info(0, "Trying to turn on the display");
    }


    // Figure out all of the display settings...
    if (color_depth == 0)
    {
        if (SDL_GetVideoInfo()->vfmt->BitsPerPixel <= 8) color_depth = 8;
        else color_depth = 16;
    }

    switch (color_depth)
    {
        case 8:
            rgb_size[0] = 2;
            rgb_size[1] = 3;
            rgb_size[2] = 3;
            break;
        case 15:
        case 16:
            rgb_size[0] = 5;
            rgb_size[1] = 5;
            rgb_size[2] = 5;
            break;
        default:
            rgb_size[0] = 8;
            rgb_size[1] = 8;
            rgb_size[2] = 8;
            break;
    }

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, rgb_size[0]);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, rgb_size[1]);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, rgb_size[2]);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, z_depth);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, ktrue);

    if (volumetric_shadows_on)
    {
        SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    }


    flags = 0;
    display_full_screen = full_screen; // remember global value...

    if (full_screen) { flags = SDL_FULLSCREEN;  log_info(0, "Requested fullscreen mode..."); }

    if (SDL_SetVideoMode(size_x, size_y, color_depth, SDL_OPENGL | flags) == NULL)
    {
        log_error(0, "SDL couldn't turn on GL, trying 640x480 mode...  %s", SDL_GetError());
        color_depth = 16;
        z_depth = 16;
        rgb_size[0] = 5;
        rgb_size[1] = 5;
        rgb_size[2] = 5;
        size_x = 640;
        size_y = 480;
        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, rgb_size[0]);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, rgb_size[1]);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, rgb_size[2]);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, z_depth);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, ktrue);
        SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 0);
        volumetric_shadows_on = kfalse;

        if (SDL_SetVideoMode(size_x, size_y, color_depth, SDL_OPENGL | SDL_FULLSCREEN) == NULL)
        {
            log_error(0, "Even 640x480 didn't work...  %s", SDL_GetError());
            SDL_Quit();
            return kfalse;
        }
    }


    // Remember how big our display is...
    screen_x = size_x;
    screen_y = size_y;
    display_viewport(0, 0, screen_x, screen_y);


    // Setup the fades...
    repeat(i, CIRCLE_TOTAL_POINTS)
    {
        circle_xyz[i][2] = 0.0;
    }
    circle_xyz[0][0] = 0.0;                     circle_xyz[0][1] = 0.0;
    circle_xyz[1][0] = CAST(float, DEFAULT_W);  circle_xyz[1][1] = 0.0;
    circle_xyz[2][0] = CAST(float, DEFAULT_W);  circle_xyz[2][1] = CAST(float, DEFAULT_H);
    circle_xyz[3][0] = 0.0;                     circle_xyz[3][1] = CAST(float, DEFAULT_H);



    // Log some information
    log_info(0, "Vendor        == %s", glGetString(GL_VENDOR));
    log_info(0, "Renderer      == %s", glGetString(GL_RENDERER));
    log_info(0, "Version       == %s", glGetString(GL_VERSION));
    glGetIntegerv(GL_MAX_CLIP_PLANES, &value);
    log_info(0, "Clip Planes   == %d", value);
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &value);
    log_info(0, "Max Tex Size  == %d", value);
    max_texture_size = (Uint32) value;
    SDL_GL_GetAttribute( SDL_GL_DOUBLEBUFFER, &value );
    log_info(0, "Double Buffer == %d", value);
    SDL_GL_GetAttribute( SDL_GL_STENCIL_SIZE, &value );
    log_info(0, "Stencil Bits  == %d", value);
    log_info(0, "Color Depth...");
    SDL_GL_GetAttribute(SDL_GL_RED_SIZE, &value);
    log_info(0, "Red bits    == %d", value);
    SDL_GL_GetAttribute(SDL_GL_GREEN_SIZE, &value);
    log_info(0, "Green bits  == %d", value);
    SDL_GL_GetAttribute(SDL_GL_BLUE_SIZE, &value);
    log_info(0, "Blue bits   == %d", value);
    SDL_GL_GetAttribute(SDL_GL_DEPTH_SIZE, &value);
    log_info(0, "Z Depth       == %d", value);


    // Set the window manager title bar
    SDL_WM_SetCaption( "SoulFu", "SoulFu" );


    // Keep the mouse inside the window
    if (full_screen)
    {
        SDL_WM_GrabInput(SDL_GRAB_ON);
    }


    // Hide the mouse cursor
    SDL_ShowCursor(0);


    // Clear the buffers
    display_clear_buffers();
    display_swap();


    // Make sure the graphics library doesn't try to do its own lighting...
    glDisable(GL_LIGHTING);


    // Setup our camera's field of view...
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-0.4, 0.4, -0.3, 0.3, ZNEAR, MAX_TERRAIN_DISTANCE);
    screen_frustum_x = 0.4f;
    screen_frustum_y = 0.3f;


    // Setup the texture matrix...
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();


    // Setup our initial camera matrix
    // This fixes rotation so it makes sense...  x,y checkerboard...  z height off ground
    // It also scoots the camera back a tad, so the near clipping plane is where it should be...
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glGetFloatv(GL_MODELVIEW_MATRIX, rotate_camera_matrix);
    glTranslatef(0.0, 0.0, -(ZNEAR + 1));
    glRotatef(90.0, 1.0, 0.0, 0.0);
    glRotatef(180.0, 0.0, 1.0, 0.0);
    glScalef(-1.0, 1.0, 1.0);
    glGetFloatv(GL_MODELVIEW_MATRIX, initial_camera_matrix);



    // Setup the window matrix...  Don't worry about projection...  Just turn off z...
    // Top left corner is 0, 0...  Bottom right is DEFAULT_W, DEFAULT_H...
    glLoadIdentity();
    glScalef(1.0, -1.0, 1.0);
    glTranslatef(-0.8f, -0.6f, -2*ZNEAR);
    glScalef(0.004f, 0.004f, 1.0);
    glGetFloatv(GL_MODELVIEW_MATRIX, window_camera_matrix);
    log_info(0, "------------------------------------------");


    // Setup the paperdoll stuff...  Draw or don't draw texture elements based on alpha value...  Used for unsorted transparency (trees)...
    display_paperdoll_func(1);

    camera_setup(&camera);

    // Remember to quit...
    atexit(SDL_Quit);
    return ktrue;
}

//-----------------------------------------------------------------------------------------------
void display_character_dot(Uint8* color, float x, float y, float z, float scale)
{
    // <ZZ> Draws a circle for character movement...  Circle texture should already be picked...
    z += CHARACTER_Z_FLOAT;
    display_color_alpha(color);
    display_start_fan();
    {
        display_texpos_xy(0, 0);  display_vertex_xyz(x - scale, y - scale, z);
        display_texpos_xy(1, 0);  display_vertex_xyz(x + scale, y - scale, z);
        display_texpos_xy(1, 1);  display_vertex_xyz(x + scale, y + scale, z);
        display_texpos_xy(0, 1);  display_vertex_xyz(x - scale, y + scale, z);
    }
    display_end();
}

//-----------------------------------------------------------------------------------------------
void display_loadin(float amount_done, bool_t do_kanji)
{
    // <ZZ> Draws some stuff onscreen so the player doesn't get too impatient waiting for it
    //      to start...  Amount_done ranges from 0.0 (0%) to 1.0 (100%)
    float x, x_add;
    Uint16 i;

    display_blend_off();
    display_shade_off();
    display_zbuffer_off();
    display_cull_on();
    display_clear_buffers();
    glLoadMatrixf(window_camera_matrix);


    if (do_kanji)
    {
        x_add = 15.0f;
        x = (CAST(float, DEFAULT_W) - (x_add * 7.0f)) * 0.5f;
        repeat(i, 7)
        {
            display_color(grey);
            display_kanji(i, x - 2.5f, 135.0f, 30.0f, 30.0f);
            display_color(white);
            display_kanji(i, x, 137.5f, 25.0f, 25.0f);
            x += x_add;
        }
    }

    // Draw the amount done bar...
    display_texture_off();

    if (amount_done < 0.0f) { amount_done = 0.0f; }

    if (amount_done > 1.0f) { amount_done = 1.0f; }

    amount_done *= (DEFAULT_W - 0.5f * DEFAULT_W);

    display_color(grey);
    display_start_fan();
    {
        display_vertex_xy(100 - 2.5f,             (DEFAULT_H - 50) - 2.5f);
        display_vertex_xy((DEFAULT_W - 100) + 2.5f, (DEFAULT_H - 50) - 2.5f);
        display_vertex_xy((DEFAULT_W - 100) + 2.5f, (DEFAULT_H - 25) + 2.5f);
        display_vertex_xy(100 - 2.5f,             (DEFAULT_H - 25) + 2.5f);
    }
    display_end();

    display_color(white);
    display_start_fan();
    {
        display_vertex_xy(100.0f,             (DEFAULT_H - 50));
        display_vertex_xy(100.0f + amount_done, (DEFAULT_H - 50));
        display_vertex_xy(100.0f + amount_done, (DEFAULT_H - 25));
        display_vertex_xy(100.0f,             (DEFAULT_H - 25));
    }
    display_end();

    display_swap();
    display_texture_on();
}

//-----------------------------------------------------------------------------------------------
void display_start_fade(Uint8 type, Sint8 direction, float x, float y, Uint8* color)
{
    // <ZZ> This function begins a new fade
    if (type != FADE_TYPE_WARNING || fade_type == FADE_TYPE_NONE || fade_type == FADE_TYPE_WARNING)
    {
        fade_type = type;
        fade_x = x;
        fade_y = y;
        fade_direction = direction;
        fade_color[0] = color[0];
        fade_color[1] = color[1];
        fade_color[2] = color[2];
        fade_color[3] = 255;
        fade_time = 0;

        if (direction < 0)
        {
            fade_time = 255;
        }
    }
}

//-----------------------------------------------------------------------------------------------
#define circle_quarter(corner, first, count)                                            \
    {                                                                                       \
        display_start_fan();                                                                \
        {                                                                                   \
            display_vertex(circle_xyz[corner]);                                             \
            display_vertex(circle_xyz[(corner+1)&3]);                                       \
            repeat(i, (count+1))                                                            \
            {                                                                               \
                display_vertex(circle_xyz[first+(count-i)]);                                \
            }                                                                               \
        }                                                                                   \
        display_end();                                                                      \
    }

//-----------------------------------------------------------------------------------------------
#define warning_side(corner)                                                                    \
    {                                                                                           \
        display_start_fan();                                                                    \
        {                                                                                       \
            fade_color[3] = (Uint8) (fade_time<<2);                                             \
            display_color_alpha(fade_color);                                                    \
            display_vertex(circle_xyz[corner]);                                                 \
            display_vertex(circle_xyz[(corner+1)&3]);                                           \
            fade_color[3] = 0;                                                                  \
            display_color_alpha(fade_color);                                                    \
            display_vertex(circle_xyz[((corner+1)&3)+4]);                                       \
            display_vertex(circle_xyz[corner+4]);                                               \
        }                                                                                       \
        display_end();                                                                          \
    }

//-----------------------------------------------------------------------------------------------
void display_fade(void)
{
    // <ZZ> This function draws the fade out effect on screen...
    int i;
    float scale_outer;

    if (fade_type != FADE_TYPE_NONE)
    {
        // Change the time
        fade_time += fade_direction * main_frame_skip;
        CLIP(0, fade_time, 255);


        if (fade_type == FADE_TYPE_CIRCLE)
        {
            // Generate the points...
            scale_outer = 1.00f * (255 - fade_time);
            repeat(i, CIRCLE_POINTS + 1)
            {
                // !!!BAD!!!
                // !!!BAD!!! Sin Cos lookup...
                // !!!BAD!!!
                circle_xyz[i+4][0] = (scale_outer) * SIN(i * (TWO_PI / CIRCLE_POINTS)) + fade_x;
                circle_xyz[i+4][1] = -(scale_outer) * COS(i * (TWO_PI / CIRCLE_POINTS)) + fade_y;
            }


            // Draw the main circle
            display_color(fade_color);
            display_shade_off();
            circle_quarter(1, 4, CIRCLE_QUARTER);
            circle_quarter(2, (4 + CIRCLE_QUARTER), CIRCLE_QUARTER);
            circle_quarter(3, (4 + 2*CIRCLE_QUARTER), CIRCLE_QUARTER);
            circle_quarter(0, (4 + 3*CIRCLE_QUARTER), CIRCLE_QUARTER);
        }

        if (fade_type == FADE_TYPE_WARNING)
        {
            // Generate the points...
            circle_xyz[4][0] = WARNING_SIZE;             circle_xyz[4][1] = WARNING_SIZE;
            circle_xyz[5][0] = DEFAULT_W - WARNING_SIZE;   circle_xyz[5][1] = WARNING_SIZE;
            circle_xyz[6][0] = DEFAULT_W - WARNING_SIZE;   circle_xyz[6][1] = DEFAULT_H - WARNING_SIZE;
            circle_xyz[7][0] = WARNING_SIZE;             circle_xyz[7][1] = DEFAULT_H - WARNING_SIZE;


            // Draw the effect
            if (fade_time > 30) { fade_direction = FADE_IN;  fade_time = 30; }

            display_blend_trans();
            warning_side(0);
            warning_side(1);
            warning_side(2);
            warning_side(3);
        }

        if (fade_type == FADE_TYPE_FULL)
        {
            // Draw the effect
            display_blend_trans();
            display_start_fan();
            {
                fade_color[3] = (Uint8) fade_time;
                display_color_alpha(fade_color);
                display_vertex(circle_xyz[0]);
                display_vertex(circle_xyz[1]);
                display_vertex(circle_xyz[2]);
                display_vertex(circle_xyz[3]);
            }
            display_end();
        }


        if (fade_time == 255) fade_direction = -fade_direction;

        if (fade_time == 0) fade_type = FADE_TYPE_NONE;
    }
}

//-----------------------------------------------------------------------------------------------
void display_camera_position(Uint16 times_to_slog, float slog_weight, float slog_z_weight)
{
    // <ZZ> This function generates the camera's xyz position, based on its rotation...
    float angle_xy[2];
    Uint16 i;
    Uint16 num_local_player;
    Uint16 character;
    Uint8 found;
    Uint16 mount;
    float inverse_weight;
    float inverse_z_weight;
    float target_temp_xyz[3];
    float offset_xyz[3];
    float shake_modifier;
    float centrid_distance;


    // Track local player characters...  Centrid of local players is the camera target...
    target_temp_xyz[XX] = 0.0f;
    target_temp_xyz[YY] = 0.0f;
    target_temp_xyz[ZZ] = 0.0f;
    num_local_player = 0;
    repeat(i, MAX_LOCAL_PLAYER)
    {
        // Is this player active?
        if (player_device[i].type)
        {
            CHR_DATA * pchr;

            // Find this player's character
            character = local_player_character[i];
            pchr = chr_data_get_ptr( character );
            if ( NULL != pchr )
            {
                target_temp_xyz[XX] += pchr->x;
                target_temp_xyz[YY] += pchr->y;
                target_temp_xyz[ZZ] += pchr->z + 4.0f;  // Plus 4 so we center more on character faces...
                num_local_player++;
            }
        }
    }

    if (num_local_player > 0)
    {
        target_temp_xyz[XX] /= num_local_player;
        target_temp_xyz[YY] /= num_local_player;
        target_temp_xyz[ZZ] /= num_local_player;
    }
    else
    {
        target_temp_xyz[XX] = camera.target_xyz[XX];
        target_temp_xyz[YY] = camera.target_xyz[YY];
        target_temp_xyz[ZZ] = camera.target_xyz[ZZ];
    }



    centrid_distance = 0.0f;

    if (num_local_player > 1)
    {
        // Do auto zoom out if more than one player...
        repeat(i, MAX_LOCAL_PLAYER)
        {
            // Is this player active?
            if (player_device[i].type)
            {
                CHR_DATA * pchr;

                // Find this player's character
                character = local_player_character[i];
                pchr = chr_data_get_ptr( character );
                if ( NULL != pchr )
                {
                    offset_xyz[XX] = target_temp_xyz[XX] - pchr->x;
                    offset_xyz[YY] = target_temp_xyz[YY] - pchr->y;
                    offset_xyz[ZZ] = 0.0f;
                    centrid_distance += vector_length(offset_xyz);
                }
            }
        }
        centrid_distance /= num_local_player;
        centrid_distance += (num_local_player - 1) * 3.0f;
    }


    // Do auto zoom out if any player is mounted...
    found = kfalse;
    repeat(i, MAX_LOCAL_PLAYER)
    {
        // Is this player active?
        if (player_device[i].type)
        {
            CHR_DATA * pchr;

            // Find this player's character
            character = local_player_character[i];
            pchr = chr_data_get_ptr( character );
            if ( NULL != pchr )
            {
                mount = pchr->mount;

                if ( VALID_CHR_RANGE(mount) )
                {
                    found = ktrue;
                }
            }
        }
    }

    if (found)
    {
        centrid_distance += 20.0f;
    }


    // Slog the target so it's more squishy...
    inverse_weight = 1.0f - slog_weight;
    inverse_z_weight = 1.0f - slog_z_weight;
    times_to_slog++;
    repeat(i, times_to_slog)
    {
        camera.target_xyz[XX] = (camera.target_xyz[XX] * slog_weight) + (target_temp_xyz[XX] * inverse_weight);
        camera.target_xyz[YY] = (camera.target_xyz[YY] * slog_weight) + (target_temp_xyz[YY] * inverse_weight);
        camera.target_xyz[ZZ] = (camera.target_xyz[ZZ] * slog_z_weight) + (target_temp_xyz[ZZ] * inverse_z_weight);

//        camera.distance = (camera.distance*0.95f) + (camera.to_distance*0.05f);
        camera.distance = (camera.distance * 0.90f) + (camera.to_distance * 0.05f) + ((camera.distance + centrid_distance) * 0.05f);
        camera.to_distance = (camera.to_distance * 0.99f) + (35.0f * 0.01f);  // Gradually return camera to default zoom...

        camera.rotation_xy[XX] += camera.rotation_add_xy[XX] >> 4;
        camera.rotation_add_xy[XX] -= camera.rotation_add_xy[XX] >> 4;
        camera.rotation_xy[YY] += camera.rotation_add_xy[YY] >> 4;
        camera.rotation_add_xy[YY] -= camera.rotation_add_xy[YY] >> 4;
    }



    // Limit camera rotation
    if (((Sint16) camera.rotation_xy[YY]) < MIN_CAMERA_Y)
    {
        camera.rotation_xy[YY] = MIN_CAMERA_Y;
    }

    if (camera.rotation_xy[YY] > MAX_CAMERA_Y)
    {
        camera.rotation_xy[YY] = MAX_CAMERA_Y;
    }


    // Do screen shake effect...
    if (screen_shake_timer > 0)
    {
        offset_xyz[XX] = (g_rand.table[((screen_shake_timer>>2)+0) % g_rand.size] - 128) * screen_shake_amount;
        offset_xyz[YY] = (g_rand.table[((screen_shake_timer>>2)+1) % g_rand.size] - 128) * screen_shake_amount;
        offset_xyz[ZZ] = (g_rand.table[((screen_shake_timer>>2)+2) % g_rand.size] - 128) * screen_shake_amount;
        shake_modifier = SIN((screen_shake_timer & 3) * 0.7854f);
        offset_xyz[XX] *= shake_modifier;
        offset_xyz[YY] *= shake_modifier;
        offset_xyz[ZZ] *= shake_modifier;

        camera.target_xyz[XX] += offset_xyz[XX];
        camera.target_xyz[YY] += offset_xyz[YY];
        camera.target_xyz[ZZ] += offset_xyz[ZZ];

        screen_shake_timer -= main_frame_skip;
    }








    // !!!BAD!!!
    // !!!BAD!!!  Use look up table...
    // !!!BAD!!!
    angle_xy[XX] = camera.rotation_xy[XX] * UINT16_TO_RAD;
    angle_xy[YY] = camera.rotation_xy[YY] * UINT16_TO_RAD;
    camera.xyz[XX] = SIN(angle_xy[XX]);
    camera.xyz[YY] = COS(angle_xy[XX]);

    camera.xyz[ZZ] = COS(angle_xy[YY]);
    camera.xyz[XX] *= camera.xyz[ZZ];
    camera.xyz[YY] *= camera.xyz[ZZ];
    camera.xyz[ZZ] = SIN(angle_xy[YY]);

    camera.xyz[XX] *= camera.distance;
    camera.xyz[YY] *= camera.distance;
    camera.xyz[ZZ] *= camera.distance;

    camera.xyz[XX] += camera.target_xyz[XX];
    camera.xyz[YY] += camera.target_xyz[YY];
    camera.xyz[ZZ] += camera.target_xyz[ZZ];

//sprintf(DEBUG_STRING, "%3.6f, %3.6f, %3.6f", camera.target_xyz[XX], camera.target_xyz[YY], camera.target_xyz[ZZ]);
}

//-----------------------------------------------------------------------------------------------
void display_look_at(float* lilcam_xyz, float* liltarg_xyz)
{
    float distance, x, y, z;


    // Front
    glLoadMatrixf(initial_camera_matrix);
    x = lilcam_xyz[XX] - liltarg_xyz[XX];
    y = lilcam_xyz[YY] - liltarg_xyz[YY];
    z = lilcam_xyz[ZZ] - liltarg_xyz[ZZ];
    distance = SQRT(x * x + y * y + z * z);

    if (distance < 0.0001) distance = 0.0001f;

    rotate_camera_matrix[1] = x / distance;
    rotate_camera_matrix[5] = y / distance;
    rotate_camera_matrix[9] = z / distance;

    // Side
    x = rotate_camera_matrix[1];
    y = rotate_camera_matrix[5];
    distance = SQRT(x * x + y * y);

    if (distance < 0.0001) {  y = 1.0f;  distance = 1.0f; }

    rotate_camera_matrix[0] = y / distance;
    rotate_camera_matrix[4] = -x / distance;
    rotate_camera_matrix[8] = 0;

    // Up
    rotate_camera_matrix[2] = rotate_camera_matrix[4] * rotate_camera_matrix[9];
    rotate_camera_matrix[6] = -rotate_camera_matrix[0] * rotate_camera_matrix[9];
    rotate_camera_matrix[10] = rotate_camera_matrix[0] * rotate_camera_matrix[5] - rotate_camera_matrix[4] * rotate_camera_matrix[1];
    glMultMatrixf(rotate_camera_matrix);
    glTranslatef(-lilcam_xyz[XX], -lilcam_xyz[YY], -lilcam_xyz[ZZ]);


    // Stuff for cartoon lighting
    rotate_enviro_matrix[0] = rotate_camera_matrix[0] * 0.5f;
    rotate_enviro_matrix[1] = rotate_camera_matrix[4] * 0.5f;
    rotate_enviro_matrix[2] = rotate_camera_matrix[8] * 0.5f;
    rotate_enviro_matrix[3] = rotate_camera_matrix[2] * -0.5f;
    rotate_enviro_matrix[4] = rotate_camera_matrix[6] * -0.5f;
    rotate_enviro_matrix[5] = rotate_camera_matrix[10] * -0.5f;


    // Remember where the camera is lookin'
    camera.fore_xyz[XX] = -rotate_camera_matrix[1];
    camera.fore_xyz[YY] = -rotate_camera_matrix[5];
    camera.fore_xyz[ZZ] = -rotate_camera_matrix[9];
    camera.side_xyz[XX] = rotate_camera_matrix[0];
    camera.side_xyz[YY] = rotate_camera_matrix[4];
    camera.side_xyz[ZZ] = rotate_camera_matrix[8];


    // Figure out the onscreen point matrix...
    glGetFloatv(GL_MODELVIEW_MATRIX, modelview_matrix);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glMultMatrixf(modelview_matrix);
    glGetFloatv(GL_PROJECTION_MATRIX, onscreen_matrix);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}

//-----------------------------------------------------------------------------------------------
Uint32 display_get_texture(char* filename)
{
    // <ZZ> Helper function.  Returns GL texture name for a given file...  Returns 0
    //      if the file can't be found...
    Uint8* data;
    SDF_PHEADER pheader;

    pheader = sdf_archive_find_filetype(filename, SDF_FILE_IS_RGB);

    if (pheader)
    {
        data = sdf_file_get_data(pheader);
        return (DEREF( Uint32, data + 2 ));
    }

    return 0;
}

//-----------------------------------------------------------------------------------------------
void display_kanji_setup(void)
{
    // <ZZ> This function finds the kanji file for us...
    SDF_PHEADER kanji_header;

    kanji_data   = NULL;
    kanji_header = sdf_archive_find_filetype("KANJI", SDF_FILE_IS_DAT);

    if (kanji_header)
    {
        kanji_data = sdf_file_get_data(kanji_header);
    }
}

//-----------------------------------------------------------------------------------------------
void camera_setup(CAMERA * pc)
{
    pc->rotation_xy[XX] = 0;
    pc->rotation_xy[YY] = MIN_CAMERA_Y;

    pc->rotation_add_xy[XX] =
        pc->rotation_add_xy[YY] = 0;

    pc->to_rotation_xy[XX] = 0;
    pc->to_rotation_xy[YY] = MIN_CAMERA_Y;

    pc->distance    = 30.0f;
    pc->to_distance = 30.0f;
};
