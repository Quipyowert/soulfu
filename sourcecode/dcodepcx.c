// <ZZ> This file contains functions to convert PCX files to RGB
//  **  decode_pcx              - The main function to do a PCX conversion

#include "soulfu_config.h"
#include "sdf_archive.h"
#include "logfile.h"

#include "soulfu_endian.inl"


Uint8 pcx_palette[256*3];   // Palette data in 24-bit format
//Uint8 pcx_palette[256*2];   // Palette data in 555 format
//-----------------------------------------------------------------------------------------------
SDF_PHEADER decode_pcx(SDF_PHEADER pcx_header, const char* filename)
{
    // <ZZ> This function decompresses a pcx file that has been stored in memory.  Index is a
    //      pointer to the start of the file's pcx_header in the sdf_archive, and can be gotten from
    //      sdf_archive_find_index_from_filename.  If the function works okay, it should create a new RGB file in the
    //      pcx_header and return ktrue.  It might also delete the original compressed file to save
    //      space, but that's a compile time option.  If it fails it should return kfalse, or
    //      it might decide to crash.
    Uint8* pcx_data;      // Compressed
    Uint32 pcx_size;      // Compressed

    SDF_PHEADER rgb_header = NULL;
    Uint8*      rgb_data;   // Decompressed
    Uint32      rgb_size;   // Decompressed

    Uint8* read;
    Uint8* write;
    Uint16 output_width;
    Uint16 output_height;
    Uint16 bytes_per_line;
    int i, j;
    int x, y;
    Uint8 red, green, blue; //, hi, lo;


    // Log what we're doing
    log_info(1, "Decoding %s.JPG to %s.RGB", filename, filename);


    // Find the location of the file pcx_data, and its pcx_size...
    pcx_data   = sdf_file_get_data(pcx_header);
    pcx_size   = sdf_file_get_size(pcx_header);
    rgb_header = NULL;

    // Make sure we have room in the pcx_header for a new file
    if (sdf_archive_free_file_count() <= 0)
    {
        log_error(0, "No room left to add RGB file, program must be restarted");
        goto decode_pcx_fail;
    }

    // Read the header info and make sure it's the right type of file...
    if (*(pcx_data + 1) != 5 || *(pcx_data + 3) != 8 || *(pcx_data + 65) != 1)
    {
        log_error(0, "PCX file is incorrect version");
        goto decode_pcx_fail;
    }

    if (*(pcx_data + pcx_size - 769) != 12)
    {
        log_error(0, "PCX file has corrupt palette pcx_data");
        goto decode_pcx_fail;
    }


    // Read the x and y sizes
    output_width = *(pcx_data + 9);  output_width = output_width << 8;  output_width += *(pcx_data + 8);  output_width++;
    output_height = *(pcx_data + 11);  output_height = output_height << 8;  output_height += *(pcx_data + 10);  output_height++;
    bytes_per_line = *(pcx_data + 67);  bytes_per_line = bytes_per_line << 8;  bytes_per_line += *(pcx_data + 66);


    // Read the palette pcx_data
    read = pcx_data + pcx_size - 768;
    write = pcx_palette;
    repeat(i, 256)
    {
        red = *(read);  read++;
        green = *(read);  read++;
        blue = *(read);  read++;

        *write = red;  write++;
        *write = green;  write++;
        *write = blue;  write++;

//        convert_24bit_to_16bit(red, green, blue, hi, lo);
//        *write = hi;  write++;
//        *write = lo;  write++;
    }


    // Allocate memory for the new file...  2 byte flags, 4 byte texture, 2 byte x, 2 byte y, x*y*3 bytes for pcx_data
//    rgb_size = (2*(output_width)*(output_height)) + 10;
    rgb_size = (3 * (output_width) * (output_height)) + 10;

    rgb_header = sdf_archive_create_header(NULL, filename, rgb_size, SDF_FILE_IS_RGB);

    if (NULL == rgb_header)
    {
        log_error(0, "Couldn't create RGB file");
        goto decode_pcx_fail;
    }

    rgb_data = sdf_file_get_data(rgb_header);


    // Write the texture info and file dimensions to the first 10 bytes
    endian_write_mem_int16(rgb_data, (Uint16) 0);   rgb_data += 2;
    endian_write_mem_int32(rgb_data, (Uint32) 0);   rgb_data += 4;
    endian_write_mem_int16(rgb_data, (Uint16) output_width);   rgb_data += 2;
    endian_write_mem_int16(rgb_data, (Uint16) output_height);  rgb_data += 2;

    // Decompress the file
    read = pcx_data + 128;
    repeat(y, output_height)
    {
        x = 0;
//        write = rgb_data + (y*2*output_width);
        write = rgb_data + (y * 3 * output_width);

        while (x < bytes_per_line)
        {
            j = *read;  read++;
            i = 1;

            if ((j & 0xC0) == 0xC0)
            {
                i = (j & 0x3F);
                j = *read;  read++;
            }

            while (i > 0)
            {
                if (x < output_width)
                {
                    *write = pcx_palette[j*3];
                    write++;
                    *write = pcx_palette[(j*3)+1];
                    write++;
                    *write = pcx_palette[(j*3)+2];
                    write++;
                }

                x++;
                i--;
            }
        }
    }

    // Decide if we should get rid of the compressed file or not...
#ifndef KEEP_COMPRESSED_FILES
    sdf_archive_delete_header(pcx_header);
    sdf_archive_set_save(kfalse);
#endif

    return rgb_header;

decode_pcx_fail:

    if (NULL != rgb_header)
    {
        sdf_archive_delete_header(rgb_header);
    }

    return NULL;
}

//-----------------------------------------------------------------------------------------------

