// Interface between the SDF archive, SoulFu, and the PXSS compiler

#include "dcodesrc.h"
#include "pxss_lib.h"

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
Sint8 sf_stage_compile(Uint8 stage, Uint8 mask, float loadin_min, float loadin_max)
{
    // <ZZ> This function performs one of the three compilation stages for every SRC file we've
    //      got.  If it hits any errors, it returns kfalse, otherwise it returns ktrue.
    //      mask tells us if we want to recompile all files (SDF_FLAG_ALL) or only the ones
    //      that've been updated (SDF_FLAG_WAS_UPDATED).
    int i;
    Sint8 sofarsogood;

    bool_t draw_loadin;
    draw_loadin = loadin_max > loadin_min;

    if (pxss_compiler_error) return kfalse;


    // Go through each file in the pxss_header
    log_info(0, "Performing stage %d of compilation...", stage);
    sofarsogood = ktrue;
    repeat(i, sdf_archive_get_num_files())
    {
        SDF_PHEADER pheader;
        Uint8       file_type;
        Uint8       file_flags;
        char       *file_name;

        pheader    = sdf_archive_get_header(i);
        file_type  = sdf_file_get_type(pheader);
        file_flags = sdf_file_get_flags(pheader);
        file_name  = sdf_file_get_name(pheader);

#if !defined(_SOULFU_LIB)
        UI_INTERRUPT(draw_loadin, i, loadin_min, loadin_max);
#endif

        // Check the type of file to decompress, then hand off to a subroutine...
        if (0 == (file_flags & mask) && (mask != SDF_FLAG_ALL)) continue;

        if (stage == PXSS_HEADERIZE && file_type == SDF_FILE_IS_SRC)
        {
            sofarsogood &= sf_headerize(pheader, file_name) ? 1 : 0;
        }
        else if (stage == PXSS_COMPILERIZE && file_type == SDF_FILE_IS_SRC)
        {
            sofarsogood &= sf_compilerize(pheader, file_name) ? 1 : 0;
        }
        else if (stage == PXSS_FUNCTIONIZE && file_type == SDF_FILE_IS_RUN)
        {
            sofarsogood &= sf_loadize(pheader, file_name) ? 1 : 0;
        }
    }

    if (!sofarsogood)
    {
        log_info(0, "Had trouble with at least one of the files...");
        pxss_compiler_error = ktrue;  // for displaying error message
    }
    else
    {
        log_info(0, "Completed stage %d of compilation...", stage);
    }

    return sofarsogood;
}

//-----------------------------------------------------------------------------------------------
bool_t sf_headerize(SDF_PHEADER src_header, char * filename)
{
    SDF_STREAM src_stream;
    SDF_STREAM int_stream;

    SDF_PHEADER run_header = NULL;
    SDF_PHEADER int_header = NULL;

    // Log what we're doing
    log_info(1, "Generating header file for %s.SRC", filename);

    // Just like sdf_open...
    sdf_stream_open_header(&src_stream, src_header);

    // initialize the output stream
    sdf_stream_open_mem(&int_stream, subbuffer, BUFFERSIZE);

    if ( !pxss_headerize(&src_stream, &int_stream, filename) )
    {
        goto sf_headerize_fail;
    };

    // delete any old RUN file that exists
    run_header = sdf_archive_find_filetype(filename, SDF_FILE_IS_RUN);

    if (NULL != run_header)
    {
        sdf_archive_delete_header(run_header);
    }

    int_header = sdf_archive_create_header(int_stream.read_beg, filename, CAST(size_t, int_stream.read - int_stream.read_beg), SDF_FILE_IS_INT);

    if (NULL == int_header)
    {
        log_error(0, "Not enough memory to decompress");
        goto sf_headerize_fail;
    }

    return ktrue;

sf_headerize_fail:
    // delete any old RUN file that exists
    run_header = sdf_archive_find_filetype(filename, SDF_FILE_IS_RUN);

    if (NULL != run_header)
    {
        sdf_archive_delete_header(run_header);
    }

    log_error(0, "Found error headerizing %s.SRC", filename);
    pxss_file_found_error  = ktrue;
    return kfalse;
};

//-----------------------------------------------------------------------------------------------
bool_t sf_compilerize(SDF_PHEADER src_header, const char* filename)
{
    SDF_STREAM compile_stream;
    SDF_STREAM src_stream;

    SDF_PHEADER pxss_run_header = NULL;     // Compiled data

    SDF_PHEADER int_header = NULL;  // Original, Headerized data
    Uint8*      int_data;    // Original, Headerized data
    Uint32      int_size;    // Original, Headerized data

    // initialize the output stream
    sdf_stream_open_mem(&compile_stream, subbuffer, BUFFERSIZE);

    // Just like sdf_open...
    sdf_stream_open_header(&src_stream, src_header);

    // Copy all of the int_data from the header (that we already compiled)...
    int_header = sdf_archive_find_filetype(filename, SDF_FILE_IS_INT);

    if (int_header == NULL)
    {
        // Can't proceed without a header...
        log_error(0, "Header file for %s.SRC wasn't generated, can't compile", filename);

        pxss_file_found_error = ktrue;
        goto sf_compilerize_fail;
    }

    int_data = sdf_file_get_data(int_header);
    int_size = sdf_file_get_size(int_header);

    memcpy(compile_stream.read_beg, int_data, int_size);
    compile_stream.read = compile_stream.read_beg + int_size;

    if ( !pxss_compilerize(&src_stream, &compile_stream, filename) )
    {
        goto sf_compilerize_fail;
    }

    pxss_run_header = sdf_archive_create_header(compile_stream.read_beg, filename, CAST(size_t, compile_stream.read - compile_stream.read_beg), SDF_FILE_IS_RUN);

    if (NULL == pxss_run_header)
    {
        log_error(0, "Could not create RUN file", filename);
        goto sf_compilerize_fail;
    }

    // delete the intermediate file
    sdf_archive_delete_header(int_header);


    //fprintf(stdout, "========================================\n");

    return ktrue;

sf_compilerize_fail:

    pxss_compiler_error = ktrue;

    log_error(0, "Found error compilerizing %s.SRC", filename);

    // Delete the headerized file, since we had trouble...
    sdf_archive_delete_header(int_header);

    if (NULL != pxss_run_header)
    {
        sdf_archive_delete_header(pxss_run_header);
    };

    //fprintf(stdout, "========================================\n");

    return kfalse;
};

//-----------------------------------------------------------------------------------------------
bool_t sf_loadize(SDF_PHEADER run_header, const char* filename)
{
    SDF_STREAM run_stream;

    // Log what we're doing
#ifdef DEVTOOL
    log_info(1, "Functionizing %s.RUN", filename);
#endif

    // Figure out where our run_stream.read is, and where it stops
    sdf_stream_open_header(&run_stream, run_header);

    if (!pxss_loadize(&run_stream, filename))
    {
        goto sf_loadize_fail;
    }

    return ktrue;

sf_loadize_fail:

    pxss_compiler_error = ktrue;
    log_error(0, "Found error functionizing %s.RUN", filename);

    // Delete the RUN file if there are any linking errors...
    sdf_archive_delete_header(run_header);
    return kfalse;
};


bool_t sf_defines_setup(void)
{
    SDF_STREAM loc_stream;

    if (NULL == sdf_stream_open_filename(&loc_stream, "DEFINE.TXT"))
    {
        log_error(0, "DEFINE.TXT could not be found in the database");
        return kfalse;
    };

    return pxss_defines_setup(& loc_stream);
}
