// <ZZ> This file contains functions to convert JPEG files to RGB
//  **  my_error_mgr            - A structure for the JPEG thing's error handling (ignore)
//  **  my_error_exit           - A function for the JPEG thing's error handling (ignore)
//  **  put_scanline_someplace  - Fills in the RGB file with data, Runs line by line (helper)
//  **  decode_jpg              - The main function to do a JPG conversion
//
//  <bremac> correct the jpeg decoder so that it will decode from one memory stream into another
//           eliminates the need for a custom jpeg library.

#include "soulfu_config.h"
#include "sdf_archive.h"
#include "logfile.h"

#include "soulfu_endian.inl"

#include <jpeglib.h>
#include <setjmp.h>

Uint8 file_is_a_heightmap;
//-----------------------------------------------------------------------------------------------
struct my_error_mgr
{
    // <ZZ> Helper for the JPEG library.
    struct jpeg_error_mgr pub;
    jmp_buf setjmp_buffer;
};
typedef struct my_error_mgr* my_error_ptr;

//-----------------------------------------------------------------------------------------------
METHODDEF(void) my_error_exit (j_common_ptr cinfo)
{
    // <ZZ> Helper for the JPEG library.
    my_error_ptr myerr = (my_error_ptr) cinfo->err;
    log_error(0, "Had a problem decompressing JPEG file");
    longjmp(myerr->setjmp_buffer, 1);
}

//-----------------------------------------------------------------------------------------------
void put_scanline_someplace(JSAMPLE* read, Uint8* newdata, int samples, int bytes_per_sample)
{
    // <ZZ> This function sticks a decoded scanline (at read) into a location (at newdata).  This
    //      is a helper for the JPEG decode thing.
    int i;
    Uint8 r, g, b;
//    Uint8 hi, lo;

    // Are we doing a heightmap?
    if (file_is_a_heightmap)
    {
        // Yup...  We'll want to leave everything as an 8-bit greyscale...
        if (bytes_per_sample == 3)
        {
            repeat(i, samples)
            {
                g = *read;  read += 3;
                *newdata = g;   newdata++;
            }
        }
        else
        {
            repeat(i, samples)
            {
                g = *read;  read++;
                *newdata = g;   newdata++;
            }
        }
    }
    else
    {
        // Is the image color or greyscale?
        if (bytes_per_sample == 3)
        {
            // Do the color loop
            repeat(i, samples)
            {
                r = *read;  read++;
                g = *read;  read++;
                b = *read;  read++;
//                convert_24bit_to_16bit(r, g, b, hi, lo);
//                *newdata = hi;   newdata++;
//                *newdata = lo;   newdata++;
                *newdata = r;   newdata++;
                *newdata = g;   newdata++;
                *newdata = b;   newdata++;
            }
        }
        else
        {
            // Do the greyscale loop
            repeat(i, samples)
            {
                g = *read;  read++;
                *newdata = g;   newdata++;
                *newdata = g;   newdata++;
                *newdata = g;   newdata++;
//                convert_24bit_to_16bit(g, g, g, hi, lo);
//                *newdata = hi;   newdata++;
//                *newdata = lo;   newdata++;
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------
/* written by bremac */
// A blank function, for when we need a pointer, but nothing to do.
void jpegsrc_nil(j_decompress_ptr cinfo)
{
}

//-----------------------------------------------------------------------------------------------
/* written by bremac */
// We can't refill the buffer, since we stick the whole JPEG in.
boolean jpegsrc_fill(j_decompress_ptr cinfo)
{
    return kfalse;
}

//-----------------------------------------------------------------------------------------------
/* written by bremac */
void jpegsrc_skip(j_decompress_ptr cinfo, long num_bytes)
{
    Sint32 size = cinfo->src->bytes_in_buffer - num_bytes;

    if (size < 0)
    {
        cinfo->src->bytes_in_buffer = 0;
        cinfo->src->next_input_byte = NULL;
    }
    else
    {
        cinfo->src->bytes_in_buffer -= num_bytes;
        cinfo->src->next_input_byte += num_bytes;
    }
}

//-----------------------------------------------------------------------------------------------
SDF_PHEADER decode_jpg(SDF_PHEADER jpg_header, const char* filename)
{
    // <ZZ> This function decompresses a jpg file that has been stored in memory.  Index is a
    //      pointer to the start of the file's jpg_header in the sdf_archive, and can be gotten from
    //      sdf_archive_find_index_from_filename.  If the function works okay, it should create a new RGB file in the
    //      jpg_header and return ktrue.  It might also delete the original compressed file to save
    //      space, but that's a compile time option.  If it fails it should return kfalse, or
    //      it might decide to crash.

    struct jpeg_decompress_struct cinfo;
    struct jpeg_source_mgr srcmgr;
    struct my_error_mgr jerr;

    JSAMPARRAY buffer;      // Output row buffer
    int row_stride;         // physical row width in output buffer

    Uint8* jpg_data;      // Compressed
    Uint32 jpg_size;      // Compressed

    SDF_PHEADER rgb_header = NULL;
    Uint8*      rgb_data;   // Decompressed
    Uint32      rgb_size;   // Decompressed


    // Log what we're doing
    file_is_a_heightmap = kfalse;

    log_info(1, "Decoding %s.JPG to %s.RGB", filename, filename);

    if (filename[0] == 'H' && filename[1] == 'I' && filename[2] == 'T' && filename[3] == 'E')
    {
        file_is_a_heightmap = ktrue;
    }


    // Find the location of the file jpg_data, and its jpg_size...
    jpg_data = sdf_file_get_data(jpg_header);
    jpg_size = sdf_file_get_size(jpg_header);

    // Make sure we have room in the pcx_header for a new file
    if (sdf_archive_free_file_count() <= 0)
    {
        log_error(0, "No room left to add RGB file, program must be restarted");
        goto decode_jpg_fail;
    }

    rgb_data = NULL;


    // Setup the custom error handler
    cinfo.err = jpeg_std_error(&jerr.pub);
    jerr.pub.error_exit = my_error_exit;

    if (setjmp(jerr.setjmp_buffer))
    {
        goto decode_jpg_fail;
    }


    // Decompress the jpg_data
    jpeg_create_decompress(&cinfo);

    /* bremac - begin*/
    cinfo.src = &srcmgr; // Use the source manager created on the stack.
    cinfo.src->bytes_in_buffer = jpg_size;
    cinfo.src->next_input_byte = jpg_data;
    cinfo.src->init_source = jpegsrc_nil;
    cinfo.src->fill_input_buffer = jpegsrc_fill;
    cinfo.src->skip_input_data = jpegsrc_skip;
    cinfo.src->resync_to_restart = jpeg_resync_to_restart;
    cinfo.src->term_source = jpegsrc_nil;
    /* bremac - end */

    jpeg_read_header(&cinfo, ktrue);
    jpeg_start_decompress(&cinfo);

    // Make sure we have room in the jpg_header for a new file
    if (sdf_archive_free_file_count() <= 0)
    {
        log_error(0, "No room left to add file, program must be restarted");
        goto decode_jpg_fail;
    }



    // Allocate memory for the new file...  2 byte flags, 4 byte texture, 2 byte x, 2 byte y, x*y*3 bytes for jpg_data (x*y bytes for heightmap)
    rgb_size = 10;

    if (file_is_a_heightmap)
    {
        rgb_size += ((cinfo.output_width) * (cinfo.output_height));
    }
    else
    {
//        rgb_size += (2*(cinfo.output_width)*(cinfo.output_height));
        rgb_size += (3 * (cinfo.output_width) * (cinfo.output_height));
    }

    rgb_header = sdf_archive_create_header(NULL, filename, rgb_size, SDF_FILE_IS_RGB);

    if (NULL == rgb_header)
    {
        log_error(0, "Couldn't create RGB file");
        goto decode_jpg_fail;
    }

    rgb_data = sdf_file_get_data(rgb_header);

    // Write the texture info and file dimensions to the first 10 bytes
    endian_write_mem_int16(rgb_data, (Uint16) 0);   rgb_data += 2;
    endian_write_mem_int32(rgb_data, (Uint32) 0);   rgb_data += 4;
    endian_write_mem_int16(rgb_data, (Uint16) cinfo.output_width);   rgb_data += 2;
    endian_write_mem_int16(rgb_data, (Uint16) cinfo.output_height);  rgb_data += 2;


    // Continue decompressing
    row_stride = cinfo.output_width * cinfo.output_components;
    buffer = (*cinfo.mem->alloc_sarray) ((j_common_ptr) & cinfo, JPOOL_IMAGE, row_stride, 1);

    while (cinfo.output_scanline < cinfo.output_height)
    {
        jpeg_read_scanlines(&cinfo, buffer, 1);
        put_scanline_someplace(buffer[0], rgb_data, cinfo.output_width, cinfo.output_components);

        if (file_is_a_heightmap)
        {
            rgb_data += cinfo.output_width;  // 1 byte per sample in the finished jpg_data
        }
        else
        {
//            rgb_data += (cinfo.output_width<<1);  // 2 bytes per sample in the finished jpg_data
            rgb_data += (cinfo.output_width * 3);  // 3 bytes per sample in the finished jpg_data
        }
    }

    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);


    // Decide if we should get rid of the compressed file or not...
    // Decide if we should get rid of the compressed file or not...
#ifndef KEEP_COMPRESSED_FILES
    sdf_archive_delete_header(jpg_header);
    sdf_archive_set_save(kfalse);
#endif

    return rgb_header;

decode_jpg_fail:

    jpeg_destroy_decompress(&cinfo);

    if (NULL != rgb_header)
    {
        sdf_archive_delete_header(rgb_header);
    }

    return NULL;
}

//-----------------------------------------------------------------------------------------------

