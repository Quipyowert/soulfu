# SoulFu #

This is a modified version of the source code for [Aaron Bishop](http://www.aaronbishopgames.com/)'s Secret of Ultimate Legendary Fantasy Unleashed game.  Note that it has a custom and confusing license which forces the user to do cumbersome shit every time they start playing the game, if they want to play the full version.

## Compiling SoulFu ##

Start by preparing the dependencies (this will be simplified further in future):

1. Ensure you have Visual Studio 2013 (perhaps community edition) installed.
1. Ensure that you have the EditorConfig extension installed.
    1. Run Visual Studio.
    1. Select the menu option: Tools -> Extensions and Updates.
    1. Select the Online entry from the left-hand side list.
    1. Search for and install EditorConfig.
    1. Exit Visual Studio.
1. Open a DOS command prompt and ensure you are in the top level directory.
1. Execute `cd build`
1. Execute `build.bat`
1. Copy `SDL.dll` from within `dependencies\SDL-1.2.14` to the top level directory.
1. Copy `SDL_net.dll` from within `dependencies\SDL_net-1.2.7` to the top level directory.
1. Open `SoulFu.sln`
1. Select the `Debug DEVTOOL` configuration, which will generate the datafile once it completes.
1. Debug and the game should start.
