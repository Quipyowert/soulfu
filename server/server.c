/* Soulfu_server */

/*  A first crack at writing a simple server */

#include <SDL_types.h>

#include "server.h"
#include "sdf_archive.h"
#include "gameseed.h"
#include "network.h"
#include "random.h"
#include "logfile.h"

#include "soulfu.h"

#include "network.h"

#include <SDL_net.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <assert.h>

/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/

#define MAX_INDEX 256
#define VERBOSE

/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/

#pragma pack(push,1)
struct s_sv_char_info
{
    Uint8 index;     // 1 byte    Local index number
    Uint8 script;    // 1 byte    Script index (script must be found in NETLIST.DAT, or character isn't sent...)
    Uint8 pos_mod;   // 1 byte    Position modifiers (top 2 bits are for x pos) (mid 2 bits are for y pos) (low 4 bits are for z pos above floor)
    Uint8 pos_x;     // 1 byte    X position (with modifier should range from 0-1023)
    Uint8 pos_y;     // 1 byte    Y position (with modifier should range from 0-1023)
    Uint8 facing;    // 1 byte    Facing (should range from 0-255)
    Uint8 action;    // 1 byte    Action (top bit is used to flag high-data characters - CHAR_FULL_NETWORK or riding a mount...)
    Uint8 misc;      // 1 byte    Miscellaneous (top 2 bits are team) (then 1 bit for poison) (then 1 bit for petrify) (then 1 bit for low alpha) (then 1 bit for enchant_deflect) (then 1 bit for enchant_haste) (then 1 bit if enchanted in any way other than deflect & haste)
    Uint8 EqLeft;    // 1 byte    EqLeft
    Uint8 EqRight;   // 1 byte    EqRight
    Uint8 EqCol01;   // 1 byte    EqCol01

    // If character is a high-data character
    Uint8   Eqcol23;    // 1 byte       Eqcol23
    Uint8   EqSpec1;    // 1 byte       EqSpec1
    Uint8   EqSpec2;    // 1 byte       EqSpec2
    Uint8   EqHelm;     // 1 byte       EqHelm
    Uint8   EqBody;     // 1 byte       EqBody
    Uint8   EqLegs;     // 1 byte       EqLegs
    Uint8   chr_class;  // 1 byte       Character class
    Uint8   mount;      // 1 byte       Local index number of mount (same as index of character if mount is invalid)
}  SET_PACKED();
typedef struct s_sv_char_info sv_char_info_t;
#pragma pack(pop)

struct s_sv_client
{
    int active;
    TCPsocket sock_tcp;
    UDPsocket sock_udp;

    IPaddress client;
    Uint32    guid;
    Uint16    net_seed;

    sv_char_info_t * info_list[MAX_INDEX];
};
typedef struct s_sv_client sv_client_t;

/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/

time_t guid_seed  = ~0;
int    my_guid    = ~0;
static char * login_string;


static sv_client_t      sv_client_list[MAX_REMOTE];
static SDLNet_SocketSet sv_client_socket_set = NULL;
static UDPsocket        sv_client_socket_udp;


////create a hash table
static sv_char_info_t   sv_info[MAX_REMOTE];
static sv_char_info_t * sv_info_stack[MAX_REMOTE];
static Uint32           sv_info_stack_last;

/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/
void sv_add_connection(void);

/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/
/* Initialize the channels */
void sv_initialize()
{
    int i;

    for (i = 0; i < MAX_REMOTE; i++)
    {
        memset(&sv_client_list[i], 0, sizeof(sv_client_t));
        sv_info_stack[i] = &sv_info[i];
        sv_info_stack_last = i;
    };

    generate_game_seed();

    if (!sdf_archive_load("datafile.sdf")) { log_message(0, "ERROR:  sdf_load() failed"); exit(1); }

    if (!random_setup(game_seed))          { log_message(0, "ERROR:  random_setup() failed");  exit(1); }
};

/*---------------------------------------------------------------------------------------------*/
sv_char_info_t * sv_pop_char_info()
{
    sv_char_info_t * retval = NULL;

    //error trap
    if (sv_info_stack_last < 0) return retval;

    //grab one
    retval = sv_info_stack[sv_info_stack_last];
    sv_info_stack_last--;

    //construct
    memset(retval, 0, sizeof(sv_char_info_t));

    return retval;
};

/*---------------------------------------------------------------------------------------------*/
void sv_push_char_info(sv_char_info_t * pinfo)
{
    //error trap
    if (NULL == pinfo || sv_info_stack_last >= MAX_REMOTE) assert(kfalse);

    //grab one
    sv_info_stack_last++;
    sv_info_stack[sv_info_stack_last] = pinfo;
};


/*---------------------------------------------------------------------------------------------*/
Sint32 sv_make_guid() { return ((rand() << 16) | rand()); };

/*---------------------------------------------------------------------------------------------*/
/* Send an acknowledgement message to a new client */
void sv_SendHandshake(int which)
{
    PACKET loc_packet;
    sv_client_list[which].net_seed = net_seed;
    packet_begin(&loc_packet, my_guid, PACKET_TYPE_SERVER_HANDSHAKE);
    packet_add_uint32(&loc_packet,  sv_make_guid() );     // tell the client their guid
    packet_add_uint16(&loc_packet,  net_seed );      // synchronize the encryption seed
    packet_add_string(&loc_packet,  login_string );          // send a login message to the client
    packet_end(&loc_packet);

    network_transmit_packet_tcp(&loc_packet, &net_seed, sv_client_list[which].sock_tcp);
};

/*---------------------------------------------------------------------------------------------*/
/* Send a "new client" notification */
//void SendCharUpdate(int to, int about)
//{
//  packet_begin(my_guid, PACKET_TYPE_PLAYER_UPDATE);
//
//  count = 0;
//  for(i=0;i<MAX_INDEX;i++)
//  {
//    if(NULL!=sv_client_list[about].info_list[i]) count++;
//  };
//
//  packet_add_data(&sv_client_list[about].info, sizeof(sv_char_info_t));
//  packet_add_data(&sv_client_list[about].info, sizeof(sv_char_info_t));
//  packet_end();
//  network_transmit_packet_tcp(sv_client_list[to].sock_tcp);
//}

/*---------------------------------------------------------------------------------------------*/
void sv_HandleClient(int which)
{
    int i;
    Uint8  tmp8;
    Uint16 tmp16;
    Uint32 tmp32;
    char   tmps[256];
    PACKET loc_packet;

    /* Has the connection been closed? */
    if ( network_receive_packet_tcp(&loc_packet, sv_client_list[which].sock_tcp) )
    {
#ifdef DEBUG
        fprintf(stderr, "Closing socket %d (was%s active)\n",
            which, sv_client_list[which].active ? "" : " not");
#endif

        SDLNet_TCP_DelSocket(local_socket_set, sv_client_list[which].sock_tcp);
        SDLNet_TCP_Close(sv_client_list[which].sock_tcp);
        sv_client_list[which].sock_tcp = NULL;
    }
    else
    {
        Sint8 echo = ktrue;

        switch (loc_packet.data.type)
        {
        case PACKET_TYPE_NULL: echo = kfalse; break;

        case PACKET_TYPE_CHAT:
            packet_read_uint32(&loc_packet, &tmp32);
            packet_read_uint08(&loc_packet, &tmp8);
            tmps[0] = 0x00;
            packet_read_string(&loc_packet, tmps);
            fprintf(stdout, "Packet from guid %08X\n", tmp32);
            fprintf(stdout, "\tChat Message from %d(\"%s\")\n", which, tmps);
            break;

        case PACKET_TYPE_I_AM_HERE:
            packet_read_uint32(&loc_packet, &tmp32);
            packet_read_uint16(&loc_packet, &tmp16);
            fprintf(stdout, "Packet from guid %08X\n", tmp32);
            fprintf(stdout, "\tPlayer %d is in room %d\n", which, tmp16);
            break;

        case PACKET_TYPE_ROOM_UPDATE:
            packet_read_uint32(&loc_packet, &tmp32);
            fprintf(stdout, "Packet from guid %08X\n", tmp32);
            fprintf(stdout, "\tRoom update from player %d\n", which);
            packet_read_uint16(&loc_packet, &tmp16);
            fprintf(stdout, "\t\tRoom = %d\n", tmp16);
            break;

        case PACKET_TYPE_PLAYER_UPDATE:
            packet_read_uint32(&loc_packet, &tmp32);
            fprintf(stdout, "Packet from guid %08X\n", tmp32);
            packet_read_uint32(&loc_packet, &tmp32);
            fprintf(stdout, "\tPlayers received from %d: %d\n", which, tmp32);
            //grab the player's info from the client and store it locally
            //for(i=0; i<MAX_REMOTE; i++)
            //{
            //  if(sv_client_list[i].guid == tmp32) break;
            //};

            //if(MAX_REMOTE!=i)
            //{
            //  Uint8 * info = &packet_buffer[packet_readpos];
            //  Uint8 index  = info[0];

            //  if(sv_client_list[i].info_list[index] == NULL)
            //  {
            //    sv_client_list[i].info_list[index] = sv_pop_char_info();
            //  };

            //  if(sv_client_list[i].info_list[index] != NULL)
            //  {
            //    memcpy(sv_client_list[i].info_list[index], info, sizeof(sv_char_info_t));
            //  };
            //};

            //echo = kfalse;
            break;

        case PACKET_TYPE_I_WANNA_PLAY:
            packet_read_uint32(&loc_packet, &tmp32);
            fprintf(stdout, "Packet from guid %08X\n", tmp32);

            //packet_read_unsigned_char(tmp8);
            fprintf(stdout, "\tSigning on connection %d\n", which);

            sv_SendHandshake(which);

            //if(!sv_client_list[which].updated)
            //{
            //  //require the player to send updated info
            //  sv_SendHandshake(which);
            //}
            //else if(tmp8)
            //{
            //  //send updated info to the player
            //  for ( i=0; i<MAX_REMOTE; ++i )
            //  {
            //    if ( sv_client_list[i].active )
            //      SendCharUpdate(which, i);
            //  }
            //};
            echo = kfalse;
            break;

        default:
            echo = kfalse;
            /* Unknown packet type?? */;
        }

        if (echo)
        {
            //nothing fancy, just echo the packet verbatim at the moment
            for (i = 0; i < MAX_REMOTE; i++)
            {
                if (i == which) continue;

                if (!sv_client_list[which].active) continue;

                network_transmit_packet_tcp(&loc_packet, &net_seed, sv_client_list[i].sock_tcp);
            };
        }
    }
}

/*---------------------------------------------------------------------------------------------*/
void sv_cleanup(void)
{
    if ( local_socket_tcp != NULL )
    {
        SDLNet_TCP_Close(local_socket_tcp);
        local_socket_tcp = NULL;
    }

    if ( local_socket_set != NULL )
    {
        SDLNet_FreeSocketSet(local_socket_set);
        local_socket_set = NULL;
    }

    SDLNet_Quit();
}

/*---------------------------------------------------------------------------------------------*/
void sv_listen()
{
    int i;

    /* Wait for events */
    SDLNet_CheckSockets(local_socket_set, ~0);

    /* Check for new connections */
    if ( SDLNet_SocketReady(local_socket_tcp) )
    {
        sv_add_connection();
    }

    /* Check for events on existing sv_client_list */
    for ( i = 0; i < MAX_REMOTE; ++i )
    {
        if ( SDLNet_SocketReady(sv_client_list[i].sock_tcp) )
        {
            sv_HandleClient(i);
        }
    }
}

/*---------------------------------------------------------------------------------------------*/
main(int argc, char *argv[])
{
    IPaddress serverIP;

    /* Initialize SDL */
    if ( SDL_Init(0) < 0 )
    {
        fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
        exit(1);
    }

    atexit(SDL_Quit);

    /* Initialize the network */
    if ( SDLNet_Init() < 0 )
    {
        fprintf(stderr, "Couldn't initialize net: %s\n",
            SDLNet_GetError());
        exit(1);
    }

    atexit(sv_cleanup);

    // Set up the global identifiers.
    // Use the random number generator to generate them. hehehehehe
    guid_seed = time( NULL );
    srand( (Uint32)guid_seed );
    my_guid = sv_make_guid();
    login_string = "hi";

    //initialize the data
    sv_initialize();

    /* Allocate the socket set */
    local_socket_set = SDLNet_AllocSocketSet(MAX_REMOTE + 1);

    if ( local_socket_set == NULL )
    {
        fprintf(stderr, "Couldn't create socket set: %s\n",
            SDLNet_GetError());
        exit(2);
    }

    /* create the UDP channel */
    local_socket_udp = SDLNet_UDP_Open(UDP_PORT_SERVER);

    if (!local_socket_udp)
    {
        fprintf(stderr, "ERROR:  Couldn't open a UDP port on %04X", UDP_PORT_SERVER );
        exit(3);
    }

    SDLNet_UDP_AddSocket(local_socket_set, local_socket_udp);

    /* Create the server IP socket */
    SDLNet_ResolveHost(&serverIP, NULL, TCPIP_PORT_SERVER);
    local_socket_tcp = SDLNet_TCP_Open(&serverIP);

    if ( local_socket_tcp == NULL )
    {
        fprintf(stderr, "Couldn't create server socket: %s\n", SDLNet_GetError());
        exit(2);
    }

    SDLNet_TCP_AddSocket(local_socket_set, local_socket_tcp);

    printf("SoulFu Server\n");
    printf("\tGUID: 0x%08X\n", my_guid);
    printf("\tServer IP: 0x%08X, Port: 0x%04X\n",
        (SDL_BYTEORDER == SDL_LIL_ENDIAN) ? SDL_Swap32(serverIP.host) : serverIP.host,
        (SDL_BYTEORDER == SDL_LIL_ENDIAN) ? SDL_Swap16(serverIP.port) : serverIP.port );

    /* Loop, waiting for network events */
    for (;;)
    {
        sv_listen();

        SDL_Delay(10);
    }

    exit(0);
}

/*---------------------------------------------------------------------------------------------*/
void sv_kill_connection(int which)
{
    PACKET    loc_packet;

    /* Kick them out.. */
    packet_begin(&loc_packet, my_guid, PACKET_TYPE_CLOSE_CONNECTION);
    packet_end(&loc_packet);
    network_transmit_packet_tcp(&loc_packet, &net_seed, sv_client_list[which].sock_tcp);

    SDLNet_TCP_DelSocket(local_socket_set, sv_client_list[which].sock_tcp);
    SDLNet_TCP_Close(sv_client_list[which].sock_tcp);
#ifdef VERBOSE
    fprintf(stdout, "Killed inactive socket %d\n", which);
#endif
}

/*---------------------------------------------------------------------------------------------*/
void sv_refuse_connection( TCPsocket newsock, int which )
{
    PACKET    loc_packet;

    /* No more room: refuse the connection. */
    packet_begin(&loc_packet, my_guid, PACKET_TYPE_CLOSE_CONNECTION);
    packet_end(&loc_packet);
    network_transmit_packet_tcp(&loc_packet, &net_seed, sv_client_list[which].sock_tcp);

    SDLNet_TCP_Close(newsock);
#ifdef VERBOSE
    fprintf(stderr, "Connection refused -- chat room full\n");
#endif
}

/*---------------------------------------------------------------------------------------------*/
void sv_add_connection(void)
{
    TCPsocket newsock;
    int which;

    newsock = SDLNet_TCP_Accept(local_socket_tcp);

    if ( newsock == NULL ) return;

    /* Look for unconnected player slot */
    for ( which = 0; which < MAX_REMOTE; ++which )
    {
        if ( NULL == sv_client_list[which].sock_tcp ) break;
    }

    /* Failing that, look for an in-active slot */
    if ( which == MAX_REMOTE )
    {
        /* Look for inactive person slot */
        for ( which = 0; which < MAX_REMOTE; ++which )
        {
            if ( sv_client_list[which].sock_tcp && !sv_client_list[which].active )
            {
                sv_kill_connection(which);
                break;
            }
        }
    }

    if ( which == MAX_REMOTE )
    {
        sv_refuse_connection(newsock, which);
    }
    else
    {
        /* Add socket as an inactive person */
        sv_client_list[which].sock_tcp = newsock;
        sv_client_list[which].client = *SDLNet_TCP_GetPeerAddress(newsock);

        if ( SDLNet_TCP_AddSocket(local_socket_set, sv_client_list[which].sock_tcp) > 0)
        {
            sv_SendHandshake(which);
        };

#ifdef VERBOSE
        fprintf(stderr, "New inactive socket %d\n", which);

#endif
    }
}
