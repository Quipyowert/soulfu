//    SoulFu_export - Utility to test the compilation of soulfu .SRC files
//    Copyright (C) 2007 Benjamin Birdsey
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    web:   http://www.samssite.co.nr

#include <ctype.h>

#include "soulfu_common.h"
#include "soulfu_config.h"
#include "sdf_archive.h"
#include "dcodesrc.h"
#include "logfile.h"
#include "pxss_compile.h"

char *find_file_name( char *file_name )
{
    if (NULL == file_name) return NULL;

    return strrchr(file_name, SLASH_CHR);
};

void display_usage( char *executable_path )
{
    char * executable_name = find_file_name( executable_path );

    if (NULL != executable_name)
    {
        fprintf( stdout, "Usage: %s file1.src [file2.src ...]\n", executable_name );
        fprintf( stdout, "\t\tdir:\tDirectory from which the data files will be read" );
    };
}

int main( int argc, char **argv )
{
    char * fname;
    int i;

    if ( argc < 2 )
    {
        display_usage( argv[ 0 ] );
        return 1;
    }

    if (!sdf_archive_create(0))
    {
        fprintf( stderr, "Couldn't initialize the compiler.\n" );
        return 1;
    }

    // initialize the log file
    log_setup();

    for (i = 1; i < argc; i++)
    {
        fname = find_file_name( argv[i] );
        sdf_archive_add_file(fname, argv[i]);
    };

    if (sdf_archive_get_num_files() == 0)
    {
        fprintf( stderr, "No files loaded into the compiler.\n" );
        return 1;
    }
    else
    {
        // Register the unload function to be called on exit
        atexit(sdf_archive_unload);
    };

    // go through all compilation stages for the script files in the archive
    for (i = PXSS_HEADERIZE; i <= PXSS_FUNCTIONIZE; i++)
    {
        sf_stage_compile(i, SDF_FLAG_ALL, 0.0f, 0.0f);
    };

    return 0;
}
