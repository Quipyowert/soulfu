//    SoulFu_export - Utility to export files SoulFu archives (*.sdf)
//    Copyright (C) 2007 Poobah
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    web:   http://www.samssite.co.nr

#include "soulfu_common.h"
#include "soulfu_config.h"
#include "sdf_archive.h"
#include "logfile.h"

char string0[256], string1[256], string2[256];


char *find_file_name( char *file_name )
{
    if (NULL == file_name) return NULL;

    return strrchr(file_name, SLASH_CHR);
};


void display_usage( char *executable_path )
{
    char * executable_name = find_file_name( executable_path );

    if (NULL != executable_name)
    {
        printf( "Usage: %s dir\n", executable_name );
        printf( "\t\tdir:\tDirectory into which the data files will be extracted" );
    };
}


int main( int argc, char **argv )
{
    unsigned char filename[9];
    unsigned char filetype;
    int i;

    if ( argc != 2 )
    {
        display_usage( argv[ 0 ] );
        return 1;
    }

    // initialize the log file
    log_setup();

    if (!sdf_archive_load("datafile.sdf"))
    {
        printf( "Couldn't load the datafile.\n" );
        return 1;
    }

    // Register the unload function to be called on exit
    atexit(sdf_archive_unload);

    repeat(i, sdf_archive_get_num_files())
    {
        SDF_PHEADER pheader = sdf_archive_get_header(i);

        sdf_file_get_file_info(pheader, filename, &filetype);
        sprintf(string0, "%s%s%s.%s", argv[ 1 ], SLASH_STR, filename, sdf_file_get_extension(pheader));

        if (!sdf_file_export_file(pheader, string0))
            printf( "The file \"%s\" couldn't be written.\n", string0 );
    }

    return 0;
}