//    SoulFu_export - Utility to import files into a SoulFu archive (*.sdf)
//    Copyright (C) 2007 Poobah
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    web:   http://www.samssite.co.nr

#include "soulfu_config.h"
#include "sdf_archive.h"
#include "logfile.h"
#include "..\pxss\pxss_compile.h"
#include "script_extensions.h"

extern Sint8 sf_stage_compile(Uint8 stage, Uint8 mask, float loadin_min, float loadin_max);

static char string0[256], string1[256], string2[256];
static char blah[256], *blah_ptr;

char *find_file_name( char *file_name )
{
    if (NULL == file_name) return NULL;

    return strrchr(file_name, SLASH_CHR);
};

void display_usage( char *executable_path )
{
    char * executable_name = find_file_name( executable_path );

    if (NULL != executable_name)
    {
        printf( "Usage: %s dir\n", executable_name );
        printf( "\t\tdir:\tDirectory from which the data files will be read" );
        printf( "Should be run from the SoulFu root directory" );
    };
}

int main( int argc, char **argv )
{
    FILE * file;

    if ( argc != 2 )
    {
        display_usage( argv[ 0 ] );
        return 1;
    }

    // initialize the log file
    log_setup();

    //if(!sdf_archive_load("datafile.sdf"))
    {
        if (!sdf_archive_create(0))
        {
            fprintf( stderr, "Couldn't initiate compiler.\n" );
            return 1;
        }
    };

    // Register the unload function to be called on exit
    atexit(sdf_archive_unload);

#ifdef WIN32
    //dump the filenames to a text file in "bare format"
    sprintf(string0, "dir /b %s%s*.* > %s%s%s", argv[1], SLASH_STR, argv[1], SLASH_STR, "fnames.txt" );
#else
    sprintf(string0, "ls %s%s*.* > %s%s%s", argv[1], SLASH_STR, argv[1], SLASH_STR, "fnames.txt" );
    //printf("%s\n", string0);
#endif

    system( string0 );

    //check to see if the file exists
    sprintf(string0, "%s%s%s", argv[1], SLASH_STR, "fnames.txt" );
    file = fopen(string0, "r");

    if (NULL == file)
    {
        printf( "Couldn't open the directory listing \"%s\".\n", string0 );
        return 1;
    };

    //replace the .sdf archive files with the new data
    while ( !feof(file) )
    {
        int i;

        if (NULL == fgets(string1, SDL_arraysize(string1)-1, file)) break;

        for (i = 0; i < 255; i++)
        {
            switch (string1[i])
            {
                case 0x0A:
                case 0x0D:
                    string1[i] = 0x00;
                    break;
            }

            if (0x00 == string1[i]) break;
        };

        if (0x00 == string1[0]) break;

        snprintf(string2, SDL_arraysize(string2), "%s%s%s", argv[1], SLASH_STR, string1);
#if defined(_MSC_VER)
        if (!sdf_archive_add_file(string1, string2))
        {
            printf( "The file \"%s\" couldn't be read.\n", string1 );
        }
#else
        blah_ptr = strrchr(string1, SLASH_CHR);
        if( NULL == blah_ptr )
        {
            blah_ptr = string1;
        }
        else
        {
            blah_ptr++;
        }

        strncpy( blah, blah_ptr, SDL_arraysize(blah) );
        if (!sdf_archive_add_file(blah, string1 ))
        {
            printf( "The file \"%s\" couldn't be read.\n", string1 );
        }
#endif
    }

    fclose(file);

    // Compile the source code.

    if (!buffer_setup()) { log_error(0, "buffer_setup() failed");  exit(1); }

    // initialize the scripting system
    sf_extensions_setup( &g_sf_scr );

    log_info(0, "sf_stage_compile(PXSS_HEADERIZE)");
    sf_stage_compile(PXSS_HEADERIZE, SDF_FLAG_ALL, 0.0f, 0.14f);

    log_info(0, "sf_stage_compile(PXSS_COMPILERIZE)");
    sf_stage_compile(PXSS_COMPILERIZE, SDF_FLAG_ALL, 0.14f, 0.28f);

    // tell the archive that it can be saved
    sdf_archive_set_save(ktrue);

    //save the .sdf archive in the sub-directory
    sprintf(string0, "%s%s%s", argv[1], SLASH_STR, "datafile.sdf" );
    sdf_archive_save(string0);

    return 0;
}
