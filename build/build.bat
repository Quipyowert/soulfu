<# : 
@echo off
setlocal EnableDelayedExpansion

REM Note: If a BAT script calls something else, which modifies environment variables
REM       then when that something else exits, the BAT script will still have the
REM       same variables it already had - as it gets them at startup and by direct
REM       execution.

REM Divert to the internal setup code, it will return to the user setup.
goto internal_setup

REM --- User event handler functions -----------------------------------------

:user_setup

set LINKS[0]=https://www.opengl.org/registry/api/GL/glext.h
set LINKS[1]=http://www.ijg.org/files/jpegsr9a.zip
set LINKS[2]=http://downloads.xiph.org/releases/ogg/libogg-1.3.2.zip
set LINKS[3]=http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.4.zip
set LINKS[4]=http://www.libsdl.org/release/SDL-1.2.14.zip
set LINKS[5]=http://www.libsdl.org/projects/SDL_net/release/SDL_net-1.2.7.zip

set INCLUDE_PATH=%DEPENDENCY_PATH%\include
if not exist %INCLUDE_PATH% (
    mkdir %INCLUDE_PATH%
)

REM Process the user data, calling event functions when applicable.
goto return_from_user_setup

:user_zip_extracted
REM variable: %V_FILE% - The relative file name of the archive.
REM variable: %DEPENDENCY_PATH% - The absolute directory the archive is located in, and will be extracted within.
REM variable: %EXTRACT_DIRNAME% - The relative directory name the archive contains as it's sole top level entry.
REM variable: %V_EXTRACT_SKIPPED% - 'yes' or 'no', depending on whether the archive was already extracted.
cd !DEPENDENCY_PATH!

if "!V_FILE!" EQU "jpegsr9a.zip" (
    if "%V_EXTRACT_SKIPPED%" equ "no" (
        if exist libjpeg.lib del libjpeg.lib libjpeg_d.lib
    )
    if not exist libjpeg.lib (
        cd !EXTRACT_DIRNAME!
        if exist jconfig.h del jconfig.h
        copy jconfig.vc jconfig.h
        if exist win32.mak del win32.mak

        xcopy /Q /Y !DEPENDENCY_PATH!\!EXTRACT_DIRNAME!\*.h %INCLUDE_PATH%\

        echo cc=cl.exe > win32.mak
        echo cflags=-c /MTd >> win32.mak
        call nmake /nologo -f makefile.vc clean
        call nmake /nologo -f makefile.vc libjpeg.lib
        copy libjpeg.lib !DEPENDENCY_PATH!\libjpeg_d.lib

        echo cc=cl.exe > win32.mak
        echo cflags=-c /Ox /MT >> win32.mak
        call nmake /nologo -f makefile.vc clean
        call nmake /nologo nodebug=1 -f makefile.vc libjpeg.lib
        copy libjpeg.lib !DEPENDENCY_PATH!\libjpeg.lib

        goto return_from_user_zip_extracted
    )
) else if "!V_FILE!" EQU "libogg-1.3.2.zip" (
    if "%V_EXTRACT_SKIPPED%" equ "no" (
        if exist libogg.lib del libogg.lib libogg.pdb libogg_d.lib libogg_d.pdb
    )
    if not exist libogg.lib (
        REM Note that libvorbis looks for a specific version of libogg at the same directory level to link
        REM against.  Currently that is 1.2.0, but if the libogg directory is ambiguously 'libogg' it
        REM will find it, and include and link from it if necessary.
        if exist libogg rmdir /S /Q libogg
        xcopy /I /E !EXTRACT_DIRNAME! libogg

        REM Include paths include via an 'ogg' subdirectory.
        if not exist %INCLUDE_PATH%\ogg (
            mkdir %INCLUDE_PATH%\ogg
            xcopy /Q /Y !DEPENDENCY_PATH!\libogg\include\ogg\*.h %INCLUDE_PATH%\ogg\
        )
        
        cd !DEPENDENCY_PATH!\libogg
        REM At this time, the latest solution is for VS2010, so we need to use that after upgrading it.
        find /c "Format Version 11.00" win32\VS2010\libogg_static.sln 1>nul && echo Upgrading libogg from Visual Studio 2010
        find /c "Format Version 11.00" win32\VS2010\libogg_static.sln 1>nul && start /wait devenv.exe /Upgrade win32\VS2010\libogg_static.sln
        msbuild win32\VS2010\libogg_static.sln /p:Configuration=Debug /p:Platform=Win32
        copy win32\VS2010\Win32\Debug\libogg_static.lib !DEPENDENCY_PATH!\libogg_d.lib
        copy win32\VS2010\Win32\Debug\vc120.pdb !DEPENDENCY_PATH!\libogg_d.pdb
        msbuild win32\VS2010\libogg_static.sln /p:Configuration=Release /p:Platform=Win32
        copy win32\VS2010\Win32\Release\libogg_static.lib !DEPENDENCY_PATH!\libogg.lib

        goto return_from_user_zip_extracted
    )
) else if "!V_FILE!" EQU "libvorbis-1.3.4.zip" (
    if "%V_EXTRACT_SKIPPED%" equ "no" (
        if exist libvorbis.lib del libvorbis.lib libvorbis.pdb libvorbis_d.lib libvorbis_d.pdb
    )
    if not exist libvorbis.lib (        
        REM Include paths include via an 'vorbis' subdirectory? They do it for 'ogg'.  Who knows.
        if not exist %INCLUDE_PATH%\vorbis (
            mkdir %INCLUDE_PATH%\vorbis
            xcopy /Q /Y !DEPENDENCY_PATH!\!EXTRACT_DIRNAME!\include\vorbis\*.h %INCLUDE_PATH%\vorbis\
        )
    
        cd !DEPENDENCY_PATH!\!EXTRACT_DIRNAME!
        REM At this time, the latest solution is for VS2010, so we need to use that after upgrading it.
        find /c "Format Version 11.00" win32\VS2010\vorbis_static.sln 1>nul && echo Upgrading !EXTRACT_DIRNAME! from Visual Studio 2010
        find /c "Format Version 11.00" win32\VS2010\vorbis_static.sln 1>nul && start /wait devenv.exe /Upgrade win32\VS2010\vorbis_static.sln
        msbuild win32\VS2010\vorbis_static.sln /p:Configuration=Debug /p:Platform=Win32 /t:libvorbis_static
        copy win32\VS2010\Win32\Debug\libvorbis_static.lib !DEPENDENCY_PATH!\libvorbis_d.lib
        copy win32\VS2010\libvorbis\Win32\Debug\vc120.pdb !DEPENDENCY_PATH!\libvorbis_d.pdb
        msbuild win32\VS2010\vorbis_static.sln /p:Configuration=Release /p:Platform=Win32 /t:libvorbis_static
        copy win32\VS2010\Win32\Release\libvorbis_static.lib !DEPENDENCY_PATH!\libvorbis.lib
        copy win32\VS2010\libvorbis\Win32\Release\vc120.pdb !DEPENDENCY_PATH!\libvorbis.pdb

        goto return_from_user_zip_extracted
    )
) else if "!V_FILE!" EQU "SDL-1.2.14.zip" (
    if "%V_EXTRACT_SKIPPED%" equ "no" (
        if exist SDL.dll del SDL.dll SDL.lib SDL_d.lib SDL_d.dll SDL_d.pdb SDLmain.lib SDLmain.pdb
    )
    
    REM Used by SDL_net.
    set SDL_EXTRACT_DIRNAME=!EXTRACT_DIRNAME!
    
    if not exist SDL.dll (        
        cd !DEPENDENCY_PATH!\!EXTRACT_DIRNAME!
        if not exist VisualC (
            echo Decompressing: %EXTRACT_DIRNAME%\VisualC.zip
            set fn=Archive-Extract
            set fnp0=!DEPENDENCY_PATH!\!EXTRACT_DIRNAME!\VisualC.zip
            set fnp1=!DEPENDENCY_PATH!\!EXTRACT_DIRNAME!
            more %BUILD_PATH%%0 | powershell -c - > nul
        )

        if not exist %INCLUDE_PATH%\sdl (
            mkdir %INCLUDE_PATH%\sdl
            xcopy /Q /Y !DEPENDENCY_PATH!\!EXTRACT_DIRNAME!\include\*.h %INCLUDE_PATH%\sdl\
        )
    
        find /c "Format Version 9.00" VisualC\SDL.sln 1>nul && echo Upgrading !EXTRACT_DIRNAME! from Visual Studio 2005
        find /c "Format Version 9.00" VisualC\SDL.sln 1>nul && start /wait devenv.exe /Upgrade VisualC\SDL.sln

        msbuild VisualC\SDL.sln /p:Configuration=Debug /p:Platform=Win32 /t:SDL,SDLmain
        copy VisualC\SDL\Debug\SDL.dll !DEPENDENCY_PATH!\SDL_d.dll
        copy VisualC\SDL\Debug\SDL.lib !DEPENDENCY_PATH!\SDL_d.lib
        copy VisualC\SDL\Debug\SDL.pdb !DEPENDENCY_PATH!\SDL_d.pdb
        copy VisualC\SDLmain\Debug\SDLmain.lib !DEPENDENCY_PATH!\SDLmain_d.lib

        msbuild VisualC\SDL.sln /p:Configuration=Release /p:Platform=Win32 /t:SDL,SDLmain
        copy VisualC\SDL\Release\SDL.dll !DEPENDENCY_PATH!\SDL.dll
        copy VisualC\SDL\Release\SDL.lib !DEPENDENCY_PATH!\SDL.lib
        copy VisualC\SDLmain\Release\SDLmain.lib !DEPENDENCY_PATH!\SDLmain.lib
        copy VisualC\SDLmain\Release\vc120.pdb !DEPENDENCY_PATH!\SDLmain.pdb

        goto return_from_user_zip_extracted
    )
) else if "!V_FILE!" EQU "SDL_net-1.2.7.zip" (
    if "%V_EXTRACT_SKIPPED%" equ "no" (
        if exist SDL_net.dll del SDL_net.dll SDL_net.lib SDL_net_d.lib SDL_net_d.dll SDL_net_d.pdb
    )
    
    if not exist SDL_net.dll (
        cd !DEPENDENCY_PATH!\!EXTRACT_DIRNAME!
        if not exist VisualC (
            echo Decompressing: %EXTRACT_DIRNAME%\VisualC.zip
            set fn=Archive-Extract
            set fnp0=!DEPENDENCY_PATH!\!EXTRACT_DIRNAME!\VisualC.zip
            set fnp1=!DEPENDENCY_PATH!\!EXTRACT_DIRNAME!
            more %BUILD_PATH%%0 | powershell -c - > nul
        )

        if not exist %INCLUDE_PATH%\sdl_net (
            mkdir %INCLUDE_PATH%\sdl_net
            echo "COPYING SSS" !DEPENDENCY_PATH!\!EXTRACT_DIRNAME!\*.h %INCLUDE_PATH%\sdl_net\
            xcopy /Q /Y !DEPENDENCY_PATH!\!EXTRACT_DIRNAME!\*.h %INCLUDE_PATH%\sdl_net\
        )

        xcopy /Q /Y !DEPENDENCY_PATH!\!SDL_EXTRACT_DIRNAME!\include\*.h .

        find /c "Format Version 9.00" VisualC\SDL_net.sln 1>nul && echo Upgrading !EXTRACT_DIRNAME! from Visual Studio 2005
        find /c "Format Version 9.00" VisualC\SDL_net.sln 1>nul && start /wait devenv.exe /Upgrade VisualC\SDL_net.sln

        copy /B /Y !DEPENDENCY_PATH!\sdl_d.lib VisualC\sdl.lib
        msbuild VisualC\SDL_net.sln /p:Configuration=Debug /p:Platform=Win32 /t:SDL_net
        copy VisualC\Debug\SDL_net.dll !DEPENDENCY_PATH!\SDL_net_d.dll
        copy VisualC\Debug\SDL_net.lib !DEPENDENCY_PATH!\SDL_net_d.lib
        copy VisualC\Debug\SDL_net.pdb !DEPENDENCY_PATH!\SDL_net_d.pdb

        copy /B /Y !DEPENDENCY_PATH!\sdl.lib VisualC\sdl.lib
        msbuild VisualC\SDL_net.sln /p:Configuration=Release /p:Platform=Win32 /t:SDL_net
        copy VisualC\Release\SDL_net.dll !DEPENDENCY_PATH!\SDL_net.dll
        copy VisualC\Release\SDL_net.lib !DEPENDENCY_PATH!\SDL_net.lib

        goto return_from_user_zip_extracted
    )
) else (
    echo ERROR.. !V_FILE! not handled by user who wrote the build amendments.

    goto return_from_user_zip_extracted
)

echo MSG: Already built.
goto return_from_user_zip_extracted

REM User function: user_teardown
REM Description: Called as a final step before the script exits.
:user_teardown

if not exist %INCLUDE_PATH%\glext.h copy /B /Y !DEPENDENCY_PATH!\glext.h %INCLUDE_PATH%\

goto return_from_user_teardown

REM --- Script defined values ------------------------------------------------

:internal_setup

REM Ensure that we have a properly set up developer console with access to things like msbuild and devenv.
if not exist "%VS120COMNTOOLS%VsDevCmd.bat" (
    echo You do not appear to have Visual Studio 2013 installed.
    echo The community edition is free, download it and install it.
    pause & exit /b
)
if "%VisualStudioVersion%" EQU "" %VS120COMNTOOLS%VsDevCmd.bat
if "%VisualStudioVersion%" NEQ "12.0" (
    echo Your console window has already run the setup for Visual Studio %VisualStudioVersion%.
    echo Open a fresh window and run this script there.  It will run the correct setup.
    pause & exit /b
)

set BUILD_PATH=%~dp0
set BUILD_SCRIPT_FILENAME=%~nx0
set DEPENDENCY_DIRNAME=dependencies
set DEPENDENCY_PATH=%BUILD_PATH%%DEPENDENCY_DIRNAME%

if not exist %DEPENDENCY_PATH% mkdir %DEPENDENCY_PATH%
cd %DEPENDENCY_PATH%

REM --- Script defined values ------------------------------------------------

set FILES[0]=
set V_EXTRACT_DIRNAMES[0]=

goto user_setup
 
REM --- FUNCTION: Map lines to local filenames -------------------------------
:return_from_user_setup

REM Iterate over the links.
SET /A IDX=0
:loop1processlinks
set V_LINK=!LINKS[%IDX%]!
if "!V_LINK!" EQU "" goto downloadlinkedfiles

:loop2processlink
if "!V_LINK!" EQU "" goto loop0continue
REM Select the substring up to the first path separator.
for /f "delims=/" %%M in ("!V_LINK!") do set SUBSTRING=%%M

:loop3strip
REM Skip until the next path separator.
set CHAR=!V_LINK:~0,1!
set V_LINK=!V_LINK:~1!
if "!V_LINK!" EQU "" goto foundfile
if "!CHAR!" NEQ "/" goto loop3strip
goto loop2processlink

:foundfile
REM We have the trailing string after the last path separator, or the file name.
set FILES[%IDX%]=!SUBSTRING!
goto loop2processlink

:loop0continue
set /A IDX=!IDX!+1
goto loop1processlinks

REM --- DOWNLOAD DEPENDENCIES THAT HAVE NOT ALREADY BEEN DOWNLOADED ----------

:downloadlinkedfiles

set /A IDX=0
:loop_downloadlinkedfiles
set V_LINK=!LINKS[%IDX%]!
set V_FILE=!FILES[%IDX%]!
if "!V_LINK!" EQU "" goto processlinkedfiles

if not exist !V_FILE! (
    REM Download it through powershell (puts up console download progress GUI).
    echo Downloading: !V_FILE!
    powershell -c "Start-BitsTransfer -source !V_LINK!"
) else (
    echo Downloading: !V_FILE! [skipped]
)

set /A IDX=!IDX!+1
goto loop_downloadlinkedfiles

REM --- PROCESS  DEPENDENCIES THAT HAVE NOT ALREADY BEEN PROCESSED  ----------

:processlinkedfiles

set /A IDX_PLF=0
:loop_processlinkedfiles
set V_LINK=!LINKS[%IDX_PLF%]!
set V_FILE=!FILES[%IDX_PLF%]!
if "!V_LINK!" EQU "" goto internal_teardown

if "%V_FILE:~-4%" EQU ".zip" (    
    echo Decompressing: %V_FILE%
    set fn=Archive-Extract
    set fnp0=%DEPENDENCY_PATH%\%V_FILE%
    set fnp1=%DEPENDENCY_PATH%
    set EXTRACT_DIRNAME=
    set V_EXTRACT_DIRNAMES[%IDX_PLF%]=
    set V_EXTRACT_SKIPPED=no
    
    for /F "usebackq tokens=*" %%i in (`more %BUILD_PATH%%0 ^| powershell -c -`) do (
        set vi=%%i
        if "!vi:~0,12!" EQU "EXTRACTPATH:" (
            set EXTRACT_DIRNAME=!vi:~13!
            set V_EXTRACT_DIRNAMES[%IDX_PLF%]=!EXTRACT_DIRNAME!
        ) else if "!vi:~0,4!" EQU "MSG:" (
            set V_EXTRACT_MSG=!vi:~5!
            if "!V_EXTRACT_MSG!" EQU "Already extracted." set V_EXTRACT_SKIPPED=yes
            echo !vi!
        ) else (
            echo Unexpected result: !vi!
        )
    )

    goto user_zip_extracted
:return_from_user_zip_extracted
    REM Restore this just in case.  Is it necessary?
    REM cd !DEPENDENCY_PATH!
    REM This is here to get the label directly preceding it to parse correctly.
)

set /A IDX_PLF=!IDX_PLF!+1
goto loop_processlinkedfiles

REM --- Everything is done, exit back to the user ----------------------------

:internal_teardown
REM Now that processing is done, allow the user to do some steps before exiting.
goto user_teardown

:return_from_user_teardown
REM Leave the user in the directory they were in to begin with.
cd %BUILD_PATH%

REM endlocal: Ensure environment variables are left the same as when the script started.
REM exit /b:  Exit the script, but do not close any DOS window it was run from within.
endlocal & exit /b
#>

function Archive-Extract([string]$zipFilePath, [string]$destinationPath) {
    # This will get added when paths are joined, and path comparison will need it to be absent.
    $destinationPath = $destinationPath.TrimEnd("\");

    [Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem') > $null;
    $zipfile = [IO.Compression.ZipFile]::OpenRead($zipFilePath);

    # Determine how many top level entries there are.
    $ziplevel0files = @{};
    $zipfile.Entries | foreach {
        $s = ($_.FullName.TrimEnd("/") -split "/")[0];
        if ($ziplevel0files.ContainsKey($s)) {
            $ziplevel0files[$s] = $ziplevel0files[$s] + 1;
        } else {
            $ziplevel0files[$s] = 0;
        }
    }

    if ($ziplevel0files.count -ne 1) {
        Write-Host "MSG: Zip archives are (at this time) expected to contain one top-level directory, and all content within it.";
        return 1; # Failure
    }

    $zipDirName = $ziplevel0files.Keys[0]
    $zipDirPath = Join-Path -Path $destinationPath -ChildPath $zipDirName;
    Write-Host "EXTRACTPATH: $zipDirName";
    if (Test-Path -LiteralPath $zipDirPath) {
        Write-Host "MSG: Already extracted.";
        return 2; # Failure
    }
    
    $zipfile.Entries | foreach {
        $extractFilePath = Join-Path -Path $destinationPath -ChildPath $_.FullName;
        $extractFileDirPath = Split-Path -Parent $extractFilePath;
        # Skip the top-level directory everything comes under.
        if ($extractFileDirPath -ne $destinationPath) {
            if (-not (Test-Path -LiteralPath $extractFileDirPath -PathType Container)) {
                New-Item -Path $extractFileDirPath -Type Directory | Out-Null;
            }

            # Sometimes a directory comes after a file within the directory (the latter causes it to be created implicitly above).
            if (-not $extractFilePath.EndsWith("\")) {
                try {
                    [IO.Compression.ZipFileExtensions]::ExtractToFile($_, $extractFilePath, $true);
                } catch {
                    Write-Host "MSG: Failed to extract file:" $extractFilePath;
                    return 3; # Failure
                }
            }
        }
    }
    return 0; # Success
}

# Anything that calls should execute the powershell and set the parameters.

$fn = (Get-ChildItem Env:fn).Value
$archivepath = (Get-ChildItem Env:fnp0).Value;
$destinationpath = (Get-ChildItem Env:fnp1).Value;
$err = & $fn $archivepath $destinationpath;
exit $err
