SoulFu Dependencies:

  SDL        - www.libsdl.org
  SDL_net    - www.libsdl.org
  OpenGL     - www.opengl.org   (OpenGL is bundled with all Win32 systems after Win95 and before Vista)
  libjpeg    - www.ijg.org      (At present, a modified libjpeg must be used. It is included in the libs dir)
  libogg     - www.vorbis.com
  libvorbis  - www.vorbis.com